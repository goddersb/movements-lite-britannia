﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTreasuryManagement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvBalance = New System.Windows.Forms.DataGridView()
        Me.cmdSubmitMoves = New System.Windows.Forms.Button()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.lblRequestStatus = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.cboBroker = New System.Windows.Forms.ComboBox()
        Me.cboCurrency = New System.Windows.Forms.ComboBox()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvMoves = New System.Windows.Forms.DataGridView()
        Me.lblHasMoney = New System.Windows.Forms.Label()
        Me.cboBalType = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkHasMoney = New System.Windows.Forms.CheckBox()
        Me.dtpTransDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.timerCheckTreasuryStatus = New System.Windows.Forms.DataGridView()
        Me.timerMoveStatus = New System.Windows.Forms.Timer(Me.components)
        Me.txtFileName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdGetFileStatus = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkTreasuryAuto = New System.Windows.Forms.CheckBox()
        Me.timerGetTreasuryStatus = New System.Windows.Forms.Timer(Me.components)
        CType(Me.dgvBalance, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvMoves, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.timerCheckTreasuryStatus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvBalance
        '
        Me.dgvBalance.AllowUserToAddRows = False
        Me.dgvBalance.AllowUserToDeleteRows = False
        Me.dgvBalance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBalance.Location = New System.Drawing.Point(4, 167)
        Me.dgvBalance.Name = "dgvBalance"
        Me.dgvBalance.Size = New System.Drawing.Size(771, 503)
        Me.dgvBalance.TabIndex = 0
        '
        'cmdSubmitMoves
        '
        Me.cmdSubmitMoves.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSubmitMoves.Location = New System.Drawing.Point(615, 30)
        Me.cmdSubmitMoves.Name = "cmdSubmitMoves"
        Me.cmdSubmitMoves.Size = New System.Drawing.Size(96, 53)
        Me.cmdSubmitMoves.TabIndex = 2
        Me.cmdSubmitMoves.Text = "Submit Moves"
        Me.cmdSubmitMoves.UseVisualStyleBackColor = True
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Location = New System.Drawing.Point(665, 19)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(91, 44)
        Me.cmdRefresh.TabIndex = 3
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'lblRequestStatus
        '
        Me.lblRequestStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRequestStatus.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.lblRequestStatus.Location = New System.Drawing.Point(6, 16)
        Me.lblRequestStatus.Name = "lblRequestStatus"
        Me.lblRequestStatus.Size = New System.Drawing.Size(603, 80)
        Me.lblRequestStatus.TabIndex = 4
        '
        'cboPortfolio
        '
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(78, 73)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(578, 21)
        Me.cboPortfolio.TabIndex = 5
        '
        'cboBroker
        '
        Me.cboBroker.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboBroker.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBroker.FormattingEnabled = True
        Me.cboBroker.Location = New System.Drawing.Point(78, 46)
        Me.cboBroker.Name = "cboBroker"
        Me.cboBroker.Size = New System.Drawing.Size(578, 21)
        Me.cboBroker.TabIndex = 6
        '
        'cboCurrency
        '
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(78, 19)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(66, 21)
        Me.cboCurrency.TabIndex = 7
        '
        'cmdClear
        '
        Me.cmdClear.Location = New System.Drawing.Point(665, 71)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(91, 25)
        Me.cmdClear.TabIndex = 8
        Me.cmdClear.Text = "Clear"
        Me.cmdClear.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(4, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 15)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Currency:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(4, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 15)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Broker:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(4, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 15)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Portfolio:"
        '
        'dgvMoves
        '
        Me.dgvMoves.AllowUserToAddRows = False
        Me.dgvMoves.AllowUserToDeleteRows = False
        Me.dgvMoves.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMoves.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMoves.Location = New System.Drawing.Point(781, 167)
        Me.dgvMoves.MultiSelect = False
        Me.dgvMoves.Name = "dgvMoves"
        Me.dgvMoves.ReadOnly = True
        Me.dgvMoves.Size = New System.Drawing.Size(717, 235)
        Me.dgvMoves.TabIndex = 1
        '
        'lblHasMoney
        '
        Me.lblHasMoney.AutoSize = True
        Me.lblHasMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHasMoney.Location = New System.Drawing.Point(427, 23)
        Me.lblHasMoney.Name = "lblHasMoney"
        Me.lblHasMoney.Size = New System.Drawing.Size(66, 15)
        Me.lblHasMoney.TabIndex = 15
        Me.lblHasMoney.Text = "Bal Type:"
        '
        'cboBalType
        '
        Me.cboBalType.FormattingEnabled = True
        Me.cboBalType.Location = New System.Drawing.Point(499, 21)
        Me.cboBalType.Name = "cboBalType"
        Me.cboBalType.Size = New System.Drawing.Size(61, 21)
        Me.cboBalType.TabIndex = 14
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkHasMoney)
        Me.GroupBox1.Controls.Add(Me.dtpTransDate)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblHasMoney)
        Me.GroupBox1.Controls.Add(Me.cboBalType)
        Me.GroupBox1.Controls.Add(Me.cmdClear)
        Me.GroupBox1.Controls.Add(Me.cboCurrency)
        Me.GroupBox1.Controls.Add(Me.cboBroker)
        Me.GroupBox1.Controls.Add(Me.cboPortfolio)
        Me.GroupBox1.Controls.Add(Me.cmdRefresh)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(770, 103)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Criteria"
        '
        'chkHasMoney
        '
        Me.chkHasMoney.AutoSize = True
        Me.chkHasMoney.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkHasMoney.Location = New System.Drawing.Point(566, 24)
        Me.chkHasMoney.Name = "chkHasMoney"
        Me.chkHasMoney.Size = New System.Drawing.Size(89, 17)
        Me.chkHasMoney.TabIndex = 18
        Me.chkHasMoney.Text = "Has Money"
        Me.chkHasMoney.UseVisualStyleBackColor = True
        '
        'dtpTransDate
        '
        Me.dtpTransDate.Location = New System.Drawing.Point(247, 20)
        Me.dtpTransDate.Name = "dtpTransDate"
        Me.dtpTransDate.Size = New System.Drawing.Size(150, 20)
        Me.dtpTransDate.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(163, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Trans Date:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.lblRequestStatus)
        Me.GroupBox2.Controls.Add(Me.cmdSubmitMoves)
        Me.GroupBox2.Location = New System.Drawing.Point(781, 58)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(717, 103)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Treasury Request"
        '
        'timerCheckTreasuryStatus
        '
        Me.timerCheckTreasuryStatus.AllowUserToAddRows = False
        Me.timerCheckTreasuryStatus.AllowUserToDeleteRows = False
        Me.timerCheckTreasuryStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.timerCheckTreasuryStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.timerCheckTreasuryStatus.Location = New System.Drawing.Point(781, 472)
        Me.timerCheckTreasuryStatus.MultiSelect = False
        Me.timerCheckTreasuryStatus.Name = "timerCheckTreasuryStatus"
        Me.timerCheckTreasuryStatus.ReadOnly = True
        Me.timerCheckTreasuryStatus.Size = New System.Drawing.Size(717, 198)
        Me.timerCheckTreasuryStatus.TabIndex = 19
        '
        'timerMoveStatus
        '
        Me.timerMoveStatus.Interval = 1000
        '
        'txtFileName
        '
        Me.txtFileName.Location = New System.Drawing.Point(69, 26)
        Me.txtFileName.Name = "txtFileName"
        Me.txtFileName.Size = New System.Drawing.Size(348, 20)
        Me.txtFileName.TabIndex = 20
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "File Name:"
        '
        'cmdGetFileStatus
        '
        Me.cmdGetFileStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdGetFileStatus.Location = New System.Drawing.Point(615, 19)
        Me.cmdGetFileStatus.Name = "cmdGetFileStatus"
        Me.cmdGetFileStatus.Size = New System.Drawing.Size(96, 33)
        Me.cmdGetFileStatus.TabIndex = 2
        Me.cmdGetFileStatus.Text = "Get File Status"
        Me.cmdGetFileStatus.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.cmdGetFileStatus)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.txtFileName)
        Me.GroupBox3.Location = New System.Drawing.Point(781, 408)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(717, 58)
        Me.GroupBox3.TabIndex = 19
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Treasury Request Status"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label7.Location = New System.Drawing.Point(4, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(437, 46)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Treasury Management"
        '
        'chkTreasuryAuto
        '
        Me.chkTreasuryAuto.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkTreasuryAuto.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkTreasuryAuto.AutoSize = True
        Me.chkTreasuryAuto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTreasuryAuto.Location = New System.Drawing.Point(1313, 18)
        Me.chkTreasuryAuto.Name = "chkTreasuryAuto"
        Me.chkTreasuryAuto.Size = New System.Drawing.Size(185, 34)
        Me.chkTreasuryAuto.TabIndex = 39
        Me.chkTreasuryAuto.Text = "Auto Treasury On"
        Me.chkTreasuryAuto.UseVisualStyleBackColor = True
        '
        'timerGetTreasuryStatus
        '
        Me.timerGetTreasuryStatus.Interval = 3000
        '
        'FrmTreasuryManagement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1499, 682)
        Me.Controls.Add(Me.chkTreasuryAuto)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.timerCheckTreasuryStatus)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvMoves)
        Me.Controls.Add(Me.dgvBalance)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmTreasuryManagement"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Treasury Management"
        CType(Me.dgvBalance, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvMoves, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.timerCheckTreasuryStatus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvBalance As DataGridView
    Friend WithEvents cmdSubmitMoves As Button
    Friend WithEvents cmdRefresh As Button
    Friend WithEvents lblRequestStatus As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents cboBroker As ComboBox
    Friend WithEvents cboCurrency As ComboBox
    Friend WithEvents cmdClear As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents dgvMoves As DataGridView
    Friend WithEvents lblHasMoney As Label
    Friend WithEvents cboBalType As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents timerCheckTreasuryStatus As DataGridView
    Friend WithEvents timerMoveStatus As Timer
    Friend WithEvents txtFileName As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cmdGetFileStatus As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents chkHasMoney As CheckBox
    Friend WithEvents dtpTransDate As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents chkTreasuryAuto As CheckBox
    Friend WithEvents timerGetTreasuryStatus As Timer
End Class
