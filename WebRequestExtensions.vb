﻿Imports System.Net
Imports System.Runtime.CompilerServices
Module WebRequestExtensions
    <Extension()>
    Function GetResponseWithoutException(ByVal request As WebRequest) As WebResponse
        If request Is Nothing Then
            Throw New ArgumentNullException("request")
        End If

        Try
            Return request.GetResponse()
        Catch e As WebException

            If e.Response Is Nothing Then
                Throw
            End If

            Return e.Response
        End Try
    End Function
End Module