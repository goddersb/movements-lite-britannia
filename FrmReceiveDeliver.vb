﻿Public Class FrmReceiveDeliver
    Const ReceiveFree As String = "Receive Free"
    Const DeliverFree As String = "Deliver Free"
    Const ReceiveFreeMIFID As String = "Receive Free from 3rd Party MiFID2"
    Const DeliverFreeMIFID As String = "Deliver Free to 3rd Party MiFID2"

    Private Function ValidatedForm() As Boolean
        ValidatedForm = True
        If cboRPortfolio.Text = "" Then
            MsgBox("Please enter portfolio", vbExclamation, "Enter Portfolio")
            ValidatedForm = False
        ElseIf CboRCCY.Text = "" Then
            MsgBox("Please enter Currency", vbExclamation, "Enter Currency")
            ValidatedForm = False
        ElseIf Not IsNumeric(txtRAmount.Text) Then
            MsgBox("Please enter a valid amount", vbExclamation, "Enter valid amount")
            ValidatedForm = False
        ElseIf ((CboRInstrument.Text = "" And cboRISIN.Text = "") Or (Not IsNumeric(CboRInstrument.SelectedValue) And Not IsNumeric(cboRISIN.SelectedValue))) Then
            MsgBox("Please enter either an instrument or an ISIN", vbExclamation, "Enter Instrument")
            ValidatedForm = False
        ElseIf cboRBroker.Text = "" Then
            MsgBox("Please enter a Broker", vbExclamation, "Enter Broker")
            ValidatedForm = False
            'ElseIf cboRClearer.Text = "" Then
            '   MsgBox("Please enter a clearer", vbExclamation, "Enter Clearer")
            '  ValidatedForm = False
            'ElseIf cboRDelivAgent.Text = "" Then
            '   MsgBox("Please enter a Receive/Delivery Agent", vbExclamation, "Enter Receive/Delivery Agent")
            '  ValidatedForm = False
            'ElseIf txtRDelivAgentAccountNo.Text = "" Then
            '   MsgBox("Please enter a Receive/Delivery Agent's Account Number", vbExclamation, "Enter Receive/Delivery Agent's Account Number")
            '  ValidatedForm = False
            'ElseIf cboRPlaceSettle.Text = "" Then
            '   MsgBox("Please enter a Place of Settlement", vbExclamation, "Enter place of settlement")
            '  ValidatedForm = False
        End If
    End Function

    Private Sub ClearRForm()
        RRB1.Checked = True
        cboRType.Text = ""
        cboRPortfolio.Text = ""
        CboRInstrument.Text = ""
        cboRISIN.Text = ""
        dtRTransDate.Value = Now
        dtRSettleDate.Value = Now
        txtRAmount.Text = ""
        CboRCCY.Text = ""
        cboRBroker.Text = ""
        cboRCashAcc.Text = ""
        cboRClearer.Text = ""
        cboRDelivAgent.Text = ""
        txtRDelivAgentAccountNo.Text = ""
        cboRPlaceSettle.Text = ""
        txtRInstructions.Text = ""
        ChkRSendIMS.Checked = True
    End Sub

    Private Sub EditRD()
        If ClsRD.RecType = 1 Then
            cboRType.Text = ReceiveFree
        ElseIf ClsRD.RecType = 2 Then
            cboRType.Text = DeliverFree
        ElseIf ClsRD.RecType = 3 Then
            cboRType.Text = ReceiveFreeMIFID
        ElseIf ClsRD.RecType = 4 Then
            cboRType.Text = DeliverFreeMIFID
        End If

        PopulateComboBoxes()
        Call PopulateCbo(cboRPortfolio, "Select pf_Code,Portfolio from vwDolfinPaymentPortfolio order by Portfolio")
        cboRPortfolio.SelectedValue = ClsRD.PortfolioCode
        cboRPortfolio.Text = ClsRD.PortfolioName
        If IsNumeric(cboRPortfolio.SelectedValue) Then
            Call PopulateCbo(CboRCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboRPortfolio.SelectedValue & " group by CR_Code, CR_Name1 order by CR_Name1")
        End If
        CboRCCY.SelectedValue = ClsRD.CCYCode
        PopulateRInstAndCashAccount()
        CboRInstrument.SelectedValue = ClsRD.InstrCode
        cboRBroker.SelectedValue = ClsRD.BrokerCode
        If IsNumeric(cboRBroker.SelectedValue) Then
            Call PopulateCbo(cboRClearer, "Select ClearerCode,ClearerName from vwDolfinPaymentBrokerClearer where isnull(ClearerSwiftAddress,'')<> '' and isnull(ClearerNo,'')<> '' and BrokerCode = " & cboRBroker.SelectedValue & " group by ClearerCode,ClearerName")
        End If
        cboRCashAcc.SelectedValue = ClsRD.CashAccountCode
        cboRClearer.SelectedValue = ClsRD.ClearerCode
        dtRTransDate.Value = ClsRD.TransDate
        dtRSettleDate.Value = ClsRD.SettleDate
        cboRDelivAgent.SelectedValue = ClsRD.Agent
        txtRDelivAgentAccountNo.Text = ClsRD.AgentAccountNo
        txtRAmount.Text = ClsRD.Amount
        cboRPlaceSettle.SelectedValue = ClsRD.PlaceofSettlement
        txtRInstructions.Text = ClsRD.Instructions
        ChkRSendIMS.Checked = ClsRD.SendIMS
        ChkRSendMiniOMS.Checked = ClsRD.SendMiniOMS
        ChangeformRD(False)
    End Sub

    Public Sub ChangeformRD(ByVal varAdd As Boolean)
        If varAdd Then
            cmdRDRefresh.Text = "Refresh Transactions"
            cmdAddRD.Text = "Add Transaction"
            'txtID.BackColor = Color.LightSteelBlue
            Me.BackColor = Color.LightSteelBlue
            GrpTransType.BackColor = Color.LightSteelBlue
            RDBox.BackColor = Color.LightSteelBlue
            TabPageReceive.BackColor = Color.LightSteelBlue
            TabPageRDPreview.BackColor = Color.LightSteelBlue
        Else
            cmdAddRD.Text = "Click to confirm edit"
            Me.BackColor = Color.FromKnownColor(KnownColor.Control)
            GrpTransType.BackColor = Color.FromKnownColor(KnownColor.Control)
            RDBox.BackColor = Color.FromKnownColor(KnownColor.Control)
            TabPageReceive.BackColor = Color.FromKnownColor(KnownColor.Control)
            TabPageRDPreview.BackColor = Color.FromKnownColor(KnownColor.Control)
        End If
    End Sub

    Private Sub AddPrice()
        Dim TCode As Integer
        Dim SecName As String
        Dim FromDate As Date
        Dim ToDate As Date

        ' Set the date range for the price
        FromDate = DateAdd(DateInterval.Day, -11, Now)
        ToDate = DateAdd(DateInterval.Day, 0, Now)

        ' Get the TCode
        If CboRInstrument.Text = "" And cboRISIN.Text <> "" Then
            TCode = cboRISIN.SelectedValue
        ElseIf CboRInstrument.Text <> "" Then
            TCode = CboRInstrument.SelectedValue
        End If

        ' Get the ticker
        SecName = ClsIMS.GetSQLItem("SELECT T_ShortCut1 + ' ' + dbo.DolfinPaymentGetBloombergYellowKey(T_BLOOMBERGTYPE) FROM dbo.Titles T WHERE  1 = 1 AND T_Code = " & CStr(TCode))
        'SecName = SDataReader.GetString("SecName")
        ' Import the price
        If Not SecName Is Nothing Then
            ClsRD.ImportBbgPrices(SecName, FromDate, ToDate, ClsIMS.GetNewOpenConnection)
        End If
    End Sub

    Private Sub FrmReceiveDeliver_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadForm()
    End Sub

    Private Sub PopulateComboBoxes()
        cboRType.Items.Clear()
        cboRType.Items.Add(ReceiveFree)
        cboRType.Items.Add(DeliverFree)
        cboRType.Items.Add(ReceiveFreeMIFID)
        cboRType.Items.Add(DeliverFreeMIFID)
        cboRType.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRType.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRCashAcc.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRCashAcc.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRClearer.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRClearer.AutoCompleteSource = AutoCompleteSource.ListItems
        CboRInstrument.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboRInstrument.AutoCompleteSource = AutoCompleteSource.ListItems
        RRB1.Checked = True
        cboRISIN.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRISIN.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRBroker, "Select BrokerCode, BrokerName from vwDolfinPaymentBrokerClearer where isnull(BrokerSwiftAddress,'')<>'' and isnull(BrokerAccountNo,'')<>'' group by BrokerCode, BrokerName order by BrokerName")
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRPortfolio, "Select pf_Code,Portfolio from vwDolfinPaymentPortfolio where branchCode = " & ClsIMS.UserLocationCode & " order by Portfolio")
        cboRPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(CboRCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & ClsIMS.UserLocationCode & " group by CR_Code, CR_Name1 order by CR_Name1")
        CboRCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboRCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRDelivAgent, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboRDelivAgent.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRDelivAgent.AutoCompleteSource = AutoCompleteSource.ListItems
        Call PopulateCbo(cboRPlaceSettle, "select SC_SwiftCode, isnull(SC_BankName,'') + '      (' + isnull(SC_SwiftCode,'') + ')' from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_SwiftCode,'')<>'' and isnull(SC_BankName,'')<>'' Order by SC_BankName")
        cboRPlaceSettle.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRPlaceSettle.AutoCompleteSource = AutoCompleteSource.ListItems
        cboRBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboRBroker.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub LoadForm()
        ModRMS.DoubleBuffered(dgvRD, True)
        PopulateComboBoxes()
        If CboFilterStatus.Items.Count = 0 Then
            ClsIMS.PopulatecbostatusTypes(CboFilterStatus)
        End If
        If cboPayCheck.Items.Count = 0 Then
            ClsIMS.PopulatecboPayCheck(cboPayCheck)
        End If
        cboPayCheck.SelectedValue = -1

        lblRMsg.Text = "Please Note:" & vbNewLine & "1. Only instruments with ISIN populated in IMS are available to be selected" & vbNewLine &
        "2. Only broker acccounts set up with a clearer, SWIFT Address and broker account number can be selected"
        RefreshGridsRD()
        ClsRD.ClearClass()
    End Sub

    Private Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub


    Private Sub PopulateRClassFromForm()
        If cboRType.Text = ReceiveFree Then
            ClsRD.RecType = 1
        ElseIf cboRType.Text = deliverFree Then
            ClsRD.RecType = 2
        ElseIf cboRType.Text = ReceiveFreemifid Then
            ClsRD.RecType = 3
        ElseIf cboRType.Text = deliverFreemifid Then
            ClsRD.RecType = 4
        End If
        ClsRD.IMSTrCode = 0
        ClsRD.PortfolioCode = cboRPortfolio.SelectedValue
        ClsRD.CCYCode = CboRCCY.SelectedValue
        If CboRInstrument.Text <> "" Then
            ClsRD.InstrCode = CboRInstrument.SelectedValue
        Else
            ClsRD.InstrCode = cboRISIN.SelectedValue
        End If
        ClsRD.BrokerCode = cboRBroker.SelectedValue
        ClsRD.CashAccountCode = cboRCashAcc.SelectedValue
        ClsRD.ClearerCode = cboRClearer.SelectedValue
        ClsRD.TransDate = dtRTransDate.Value
        ClsRD.SettleDate = dtRSettleDate.Value
        ClsRD.Agent = cboRDelivAgent.SelectedValue
        ClsRD.AgentAccountNo = txtRDelivAgentAccountNo.Text
        ClsRD.PlaceofSettlement = cboRPlaceSettle.SelectedValue
        ClsRD.Instructions = txtRInstructions.Text
        ClsRD.Amount = txtRAmount.Text
        ClsRD.SendIMS = ChkRSendIMS.Checked
        ClsRD.SendMiniOMS = ChkRSendMiniOMS.Checked
    End Sub


    Private Sub cmdAddRD_Click(sender As Object, e As EventArgs) Handles cmdAddRD.Click
        Dim varBody As String = ""
        Dim varID As Double

        If ValidatedForm() Then
            PopulateRClassFromForm()
            If ClsRD.SelectedTransactionID <> 0 Then
                ClsIMS.UpdatePaymentTableRD(False)
                ChangeformRD(True)
            Else
                varID = ClsIMS.UpdatePaymentTableRD(True)
                'ClsRD.SendReceiveDeliverEmailBody(True, varBody, dgvFOP, varRow, ClsRD.PortfolioName, varID, varNotes)
                'ClsPayAuth.SendReceiveDeliverEmail(varBody, ClsRD.Email, IIf(cboPayType.SelectedValue = 15, True, False), varID)
            End If
        End If
        AddPrice()
        ClsRD.ClearClass()
        txtRAmount.Text = ""
        RefreshGridsRD()
    End Sub

    Public Sub PopulatePreviewSWIFT()
        Dim Lst As New Dictionary(Of String, String)
        Dim PaymentTypeID As Integer = 0

        Lst.Add("TransDate", dtRTransDate.Value.Date)
        Lst.Add("SettleDate", dtRSettleDate.Value.Date)
        Lst.Add("Amount", IIf(IsNumeric(txtRAmount.Text), txtRAmount.Text, 0))
        If CboRInstrument.Text = "" And cboRISIN.Text <> "" Then
            Lst.Add("InstrCode", cboRISIN.SelectedValue)
        ElseIf CboRInstrument.Text <> "" Then
            Lst.Add("InstrCode", CboRInstrument.SelectedValue)
        End If

        Lst.Add("BrokerCode", IIf(cboRBroker.Text <> "", cboRBroker.SelectedValue, 0))
        Lst.Add("CashAccCode", IIf(cboRCashAcc.Text <> "", cboRCashAcc.SelectedValue, 0))
        Lst.Add("ClearerCode", IIf(cboRClearer.Text <> "", cboRClearer.SelectedValue, 0))
        Lst.Add("Agent", IIf(cboRDelivAgent.Text <> "", cboRDelivAgent.SelectedValue, 0))
        Lst.Add("AgentAccountNo", IIf(txtRDelivAgentAccountNo.Text <> "", txtRDelivAgentAccountNo.Text, ""))
        Lst.Add("PlaceofSettlement", IIf(cboRPlaceSettle.SelectedValue <> "", cboRPlaceSettle.SelectedValue, ""))
        Lst.Add("Instructions", IIf(txtRInstructions.Text <> "", txtRInstructions.Text, ""))

        If cboRType.Text = ReceiveFree Then
            Lst.Add("OrderRef", "RECEIV0000000")
            txtSWIFT.Text = ClsIMS.GetSwiftFileData540(Lst)
        Else
            Lst.Add("OrderRef", "DELIVE0000000")
            txtSWIFT.Text = ClsIMS.GetSwiftFileData542(Lst)
        End If

    End Sub

    Public Function GetFilterWhereClause() As String
        GetFilterWhereClause = ""
        If CboFilterStatus.Text <> "" Then
            GetFilterWhereClause = "where (status = '" & CboFilterStatus.Text & "')"
        End If
    End Function

    Private Sub cmdRDRefresh_Click(sender As Object, e As EventArgs) Handles cmdRDRefresh.Click
        RefreshGridsRD()
    End Sub

    Private Sub CboRInstrument_Leave(sender As Object, e As EventArgs) Handles CboRInstrument.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub cboRISIN_Leave(sender As Object, e As EventArgs) Handles cboRISIN.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub dtRTransDate_Leave(sender As Object, e As EventArgs) Handles dtRTransDate.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub dtRSettleDate_Leave(sender As Object, e As EventArgs) Handles dtRSettleDate.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub txtRAmount_Leave(sender As Object, e As EventArgs) Handles txtRAmount.Leave
        If IsNumeric(txtRAmount.Text) Then
            txtRAmount.Text = CDbl(txtRAmount.Text).ToString("#,##0.00")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub RRB1_CheckedChanged(sender As Object, e As EventArgs) Handles RRB1.CheckedChanged
        cboRISIN.Text = ""
        CboRInstrument.Enabled = True
        cboRISIN.Enabled = False
    End Sub

    Private Sub RRB2_CheckedChanged(sender As Object, e As EventArgs) Handles RRB2.CheckedChanged
        CboRInstrument.Text = ""
        CboRInstrument.Enabled = False
        cboRISIN.Enabled = True
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        Dim ExportFilesList As New Dictionary(Of String, String)

        Dim RunAllSwifts As String = ClsIMS.GetSQLItem("Select * from vwDolfinPaymentAllowSwifts")
        If RunAllSwifts = "True" Or RunAllSwifts = "1" Then
            Try
                Dim FullFilePathXL As String = ClsIMS.GetFilePath("IMSExportXL") & "IMSXLRDF" & Now.ToString("_yyMMdd_HHmmssfff") & ".xlsx"
                ClsRD.CreateTransactionsXLRD(FullFilePathXL)

                Using reader As SqlClient.SqlDataReader = ClsIMS.GetSQLData("select * from vwDolfinPaymentReadySwiftReceiveDeliver")
                    If reader.HasRows Then
                        If ClsIMS.LiveDB() Then
                            ClsIMS.RunSQLJob(cSwiftJobNameRD)
                        Else
                            ClsIMS.RunSQLJob(cSwiftJobNameRD & " UAT")
                        End If

                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "Submit receive/deliver transactions files", 0)
                        MsgBox("Swift files Exported" & vbNewLine & vbNewLine & "Exported XL file can be found: " & vbNewLine & FullFilePathXL, vbInformation, "Exported")
                        ClsRD.ClearClass()
                        RefreshGridsRD()
                    Else
                        MsgBox("no files found to send. If you are expecting files to be send, please contact systems support for assistance.", vbExclamation, "No Files")
                    End If
                End Using

            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in Submit receive/deliver")
            End Try
        Else
            MsgBox("The block swift option is currently activated. No Swifts will be sent until the block has been removed from the system settings.", vbCritical, "No Swifts will be sent")
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, "FrmReceiveDeliver: cmdSubmit_Click RunAllSwifts in Systems settings is false. No Swifts will be sent", 0)
        End If
    End Sub

    Private Sub dgvRD_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvRD.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvRD, e.RowIndex)
        End If
    End Sub

    Private Sub FormatRDGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            dgv.Columns("ID").Width = 60
            dgv.Columns("Status").Width = 120
            dgv.Columns("SwiftAck").Width = 60
            dgv.Columns("ConfAck").Width = 60
            dgv.Columns("StatusAck").Width = 60
            dgv.Columns("IMSAck").Width = 60
            dgv.Columns("ReceiveDeliver").Width = 100
            dgv.Columns("OrderRef").Width = 100
            dgv.Columns("IMSCode").Width = 100
            dgv.Columns("Portfolio").Width = 150
            dgv.Columns("CCYName").Width = 60
            dgv.Columns("InstName").Width = 100
            dgv.Columns("TransDate").Width = 80
            dgv.Columns("SettleDate").Width = 80
            dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgv.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)
            dgv.Columns("BrokerName").Width = 100
            dgv.Columns("CashAccountName").Width = 100
            dgv.Columns("ClearerName").Width = 100
            dgv.Columns("Agent").Width = 100
            dgv.Columns("AgentAccountNo").Width = 100
            dgv.Columns("SwiftRet").Width = 60
            dgv.Columns("SwiftFile").Width = 100
            dgv.Columns("SwiftStatus").Width = 100
            dgv.Columns("Username").Width = 100
            dgv.Columns("Created").Width = 100
            dgv.Columns("Modified").Width = 100
            dgv.Columns("SentTime").Width = 100
            dgv.Columns("ReceivedTime").Width = 100
            dgv.Columns("ConfReceivedTime").Width = 100
            dgv.Columns("StatusReceivedTime").Width = 100
            dgv.Columns("IMSSentTime").Width = 100
            dgv.Columns("SendIMS").Width = 60
        End If
    End Sub

    Private Sub RefreshGridsRD()
        Dim SendIMS As Boolean = False, CheckSwiftAck As Boolean = False, CheckSwiftPaid As Boolean = False
        Cursor = Cursors.WaitCursor

        ClsIMS.UpdatePendingMovementsRD()
        ClsIMS.RefreshRDGrid(dgvRD, GetFilterWhereClause())

        FormatRDGrid(dgvRD)
        Cursor = Cursors.Default
    End Sub

    Private Sub TimerRefreshRD_Tick(sender As Object, e As EventArgs) Handles TimerRefreshRD.Tick
        RefreshGridsRD()
    End Sub

    Private Sub dgvRD_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRD.RowHeaderMouseDoubleClick
        If ClsRD.SelectedTransactionID = 0 Then
            ClsIMS.GetReaderItemIntoRDClass(dgvRD.Rows(e.RowIndex).Cells(0).Value)
            FrmActionRD.ShowDialog()
            If ClsRD.SelectedAuthoriseOption = 1 Then
                EditRD()
            Else
                ClsRD.ClearClass()
            End If
            RefreshGridsRD()
        Else
            MsgBox("You are currently in edit mode. Please save your current transaction or create a new one to authorise other transactions", vbInformation, "Complete save transaction")
        End If
    End Sub

    Private Sub cboRClearer_Leave(sender As Object, e As EventArgs) Handles cboRClearer.Leave
        PopulatePreviewSWIFT()
    End Sub

    Private Sub cboRBroker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRBroker.SelectedIndexChanged
        If IsNumeric(cboRBroker.SelectedValue) Then
            Call PopulateCbo(cboRClearer, "Select ClearerCode,ClearerName from vwDolfinPaymentBrokerClearer where isnull(ClearerSwiftAddress,'')<> '' and isnull(ClearerNo,'')<> '' and BrokerCode = " & cboRBroker.SelectedValue & " group by ClearerCode,ClearerName")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub cboRDelivAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRDelivAgent.SelectedIndexChanged
        If Not cboRDelivAgent.SelectedValue Is Nothing Then
            If cboRDelivAgent.ValueMember <> "" Then
                txtRDelivAgentAccountNo.Text = ClsIMS.GetSQLItem("select top 1 left(ClearerNo,charindex('/',ClearerNo)) from vwDolfinPaymentBrokerClearer where ClearerSwiftAddress = '" & cboRDelivAgent.SelectedValue & "' and charindex('/',ClearerNo)<>0 
                                                            union select 'XXXXXXXXXXXX/XXXXXXXXXXXXXXXXX' from vwDolfinPaymentSwiftCodes where SC_CountryCode = 'RU' and SC_SwiftCode = '" & cboRDelivAgent.SelectedValue & "'")
                If InStr(txtRDelivAgentAccountNo.Text, "X", vbTextCompare) <> 0 Then
                    cboRPlaceSettle.SelectedValue = "NADCRUMM"
                End If
            End If
        End If
    End Sub

    Private Sub PopulateRInstAndCashAccount()
        If IsNumeric(cboRPortfolio.SelectedValue) And IsNumeric(CboRCCY.SelectedValue) Then
            Call PopulateCbo(CboRInstrument, "Select T_Code,T_Name1 from vwDolfinPaymentInstruments where isnull(T_ISIN,'')<>'' and t_currency = " & CboRCCY.SelectedValue & " order by T_Name1")
            Call PopulateCbo(cboRISIN, "Select T_Code,T_ISIN from vwDolfinPaymentInstruments where isnull(T_ISIN,'')<>'' and t_currency = " & CboRCCY.SelectedValue & " order by T_ISIN")
            Call PopulateCbo(cboRCashAcc, "select b_code,b_name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboRPortfolio.SelectedValue & " and cr_code = " & CboRCCY.SelectedValue & " group by b_code,b_name1")
            PopulatePreviewSWIFT()
        End If
    End Sub

    Private Sub CboFilterStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboFilterStatus.SelectedIndexChanged
        Cursor = Cursors.WaitCursor
        If CboFilterStatus.Text = "" Then
            ClsIMS.RefreshRDGrid(dgvRD)
        Else
            ClsIMS.RefreshRDGrid(dgvRD, GetFilterWhereClause())
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub cboRPortfolio_Leave(sender As Object, e As EventArgs) Handles cboRPortfolio.Leave
        PopulateRInstAndCashAccount()
    End Sub

    Private Sub CboRCCY_Leave(sender As Object, e As EventArgs) Handles CboRCCY.Leave
        PopulateRInstAndCashAccount()
    End Sub

    Private Sub CboRCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboRCCY.SelectedIndexChanged
        CboRInstrument.Text = ""
        cboRISIN.Text = ""
        cboRCashAcc.Text = ""
    End Sub

    Private Sub cmdNewRD_Click(sender As Object, e As EventArgs) Handles cmdNewRD.Click
        If ClsRD.SelectedTransactionID <> 0 Then
            Dim varResponse = MsgBox("Click OK to cancel editing this transaction?", vbCritical + vbOKCancel, "transaction not saving")
            If varResponse = vbOK Then
                ChangeformRD(True)
                ClsRD.ClearClass()
                ClearRForm()
            End If
        Else
            ClsRD.ClearClass()
            ClearRForm()
        End If
    End Sub

    Private Sub ChkRSendIMS_CheckedChanged(sender As Object, e As EventArgs) Handles ChkRSendIMS.CheckedChanged
        ChkRSendMiniOMS.Checked = False
    End Sub

    Private Sub ChkRSendMiniOMS_CheckedChanged(sender As Object, e As EventArgs) Handles ChkRSendMiniOMS.CheckedChanged
        ChkRSendIMS.Checked = False
    End Sub
End Class