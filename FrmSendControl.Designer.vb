﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSendControl
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ChkIMS = New System.Windows.Forms.CheckBox()
        Me.ChkSwift = New System.Windows.Forms.CheckBox()
        Me.txtAuthID = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ChkIMS)
        Me.GroupBox2.Controls.Add(Me.ChkSwift)
        Me.GroupBox2.Controls.Add(Me.txtAuthID)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(194, 75)
        Me.GroupBox2.TabIndex = 51
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options"
        '
        'ChkIMS
        '
        Me.ChkIMS.AutoSize = True
        Me.ChkIMS.Location = New System.Drawing.Point(115, 42)
        Me.ChkIMS.Name = "ChkIMS"
        Me.ChkIMS.Size = New System.Drawing.Size(73, 17)
        Me.ChkIMS.TabIndex = 41
        Me.ChkIMS.Text = "Send IMS"
        Me.ChkIMS.UseVisualStyleBackColor = True
        '
        'ChkSwift
        '
        Me.ChkSwift.AutoSize = True
        Me.ChkSwift.Location = New System.Drawing.Point(115, 19)
        Me.ChkSwift.Name = "ChkSwift"
        Me.ChkSwift.Size = New System.Drawing.Size(77, 17)
        Me.ChkSwift.TabIndex = 40
        Me.ChkSwift.Text = "Send Swift"
        Me.ChkSwift.UseVisualStyleBackColor = True
        '
        'txtAuthID
        '
        Me.txtAuthID.AcceptsReturn = True
        Me.txtAuthID.AcceptsTab = True
        Me.txtAuthID.BackColor = System.Drawing.SystemColors.Control
        Me.txtAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAuthID.Enabled = False
        Me.txtAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAuthID.Location = New System.Drawing.Point(6, 19)
        Me.txtAuthID.Multiline = True
        Me.txtAuthID.Name = "txtAuthID"
        Me.txtAuthID.Size = New System.Drawing.Size(86, 40)
        Me.txtAuthID.TabIndex = 49
        Me.txtAuthID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'FrmSendControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(216, 97)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmSendControl"
        Me.Text = "Send Control"
        Me.TopMost = True
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ChkIMS As CheckBox
    Friend WithEvents ChkSwift As CheckBox
    Friend WithEvents txtAuthID As TextBox
End Class
