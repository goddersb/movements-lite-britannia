﻿Public Class FrmUsersPassword
    Private Sub FrmUsersPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PictureBoxUser.Image = ImageListUser.Images(0)
        CmdCancel.Image = ImageListButton.Images(0)
        cmdLogin.Image = ImageListButton.Images(1)
        txtUser.Text = ClsIMS.FullUserName
        If ClsIMS.LiveDB() Then
            Me.Text = "FLOW Login " & ClsIMS.FLOWVersion
        Else
            Me.Text = "FLOW Login UAT " & ClsIMS.FLOWVersion
        End If
    End Sub

    Private Sub CmdCancel_Click(sender As Object, e As EventArgs) Handles CmdCancel.Click
        Me.Close()
    End Sub

    Private Sub RunLogin()
        If txtResetPassword.Text <> "" Then
            If txtResetPassword.Text <> "password" And Len(txtResetPassword.Text) > 6 Then
                ClsIMS.ResetUserLogin(txtUser.Text, txtResetPassword.Text)
                Dim varResponse As Integer = ClsIMS.CheckCanUserLogin(txtUser.Text, txtResetPassword.Text)
                If varResponse = 5 Then
                    Me.Close()
                ElseIf varResponse = 4 Then
                    MsgBox("Your password is incorrect. Please try again or contact systems support to reset it", vbExclamation, "Incorrect pasword")
                ElseIf varResponse = 3 Then
                    MsgBox("Please enter another valid password - greater than 6 characters", vbExclamation, "Good password required")
                ElseIf varResponse = 2 Then
                    MsgBox("The user " & txtUser.Text & " does not have movements application permission" & vbNewLine & "Please ask a movements administrator to give you access", vbCritical, "No application permission to use")
                ElseIf varResponse = 1 Then
                    MsgBox("The user " & txtUser.Text & " does not have database permission to use this system. Please get systems support to give you database access", vbCritical, "No database permissions to use")
                End If
            Else
                MsgBox("Please enter another valid password - greater than 6 characters", vbExclamation, "Good password required")
            End If
        Else
            Dim varResponse As Integer = ClsIMS.CheckCanUserLogin(txtUser.Text, txtPassword.Text)
            If varResponse = 5 Then
                Me.Close()
            ElseIf varResponse = 4 Then
                MsgBox("Your password is incorrect. Please try again or contact systems support to reset it", vbExclamation, "Incorrect pasword")
            ElseIf varResponse = 3 Then
                Dim GoodPassword As Boolean = False
                lblResetPassword.Visible = True
                txtResetPassword.Visible = True
                txtPassword.Enabled = False
                MsgBox("Please reset your password - greater than 6 characters", vbExclamation, "Good password required")
            ElseIf varResponse = 2 Then
                MsgBox("The user " & txtUser.Text & " does not have movements application permission" & vbNewLine & "Please ask a movements administrator to give you access", vbCritical, "No application permission to use")
            ElseIf varResponse = 1 Then
                MsgBox("The user " & txtUser.Text & " does not have database permission" & vbNewLine & "Please get systems support to give you database access", vbCritical, "No database permissions to use")
            End If
        End If
    End Sub

    Private Sub cmdLogin_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click
        RunLogin()
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            RunLogin()
        End If
    End Sub
End Class