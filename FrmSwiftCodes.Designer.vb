﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSwiftCodes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSwiftCodes))
        Me.dgvSWIFT = New System.Windows.Forms.DataGridView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.ChkAllSwifts = New System.Windows.Forms.CheckBox()
        Me.ChknonValidSC = New System.Windows.Forms.CheckBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboSC = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboSCBankName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BeneficiaryBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.cmdSWIFT = New System.Windows.Forms.Button()
        Me.txtLookCountryCode = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtLookBranch = New System.Windows.Forms.TextBox()
        Me.txtLookCountry = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtLookBankName = New System.Windows.Forms.TextBox()
        Me.lblBenBankName = New System.Windows.Forms.Label()
        Me.PicLookSwift = New System.Windows.Forms.PictureBox()
        Me.lblBenBankSwift = New System.Windows.Forms.Label()
        Me.txtLookSWIFT = New System.Windows.Forms.TextBox()
        Me.txtLookPostCode = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress1 = New System.Windows.Forms.Label()
        Me.txtLookCity = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress2 = New System.Windows.Forms.Label()
        Me.txtLookAddress1 = New System.Windows.Forms.TextBox()
        Me.lblBenBankZip = New System.Windows.Forms.Label()
        Me.ImageListTickCross = New System.Windows.Forms.ImageList(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgvSWIFT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewGroupBox.SuspendLayout()
        Me.BeneficiaryBankGroupBox.SuspendLayout()
        CType(Me.PicLookSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvSWIFT
        '
        Me.dgvSWIFT.AllowUserToOrderColumns = True
        Me.dgvSWIFT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSWIFT.Location = New System.Drawing.Point(12, 155)
        Me.dgvSWIFT.Name = "dgvSWIFT"
        Me.dgvSWIFT.Size = New System.Drawing.Size(1453, 645)
        Me.dgvSWIFT.TabIndex = 44
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(139, 24)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = "SWIFT Codes"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.ChkAllSwifts)
        Me.ViewGroupBox.Controls.Add(Me.ChknonValidSC)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboSC)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboSCBankName)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 50)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(553, 99)
        Me.ViewGroupBox.TabIndex = 42
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Criteria"
        '
        'ChkAllSwifts
        '
        Me.ChkAllSwifts.AutoSize = True
        Me.ChkAllSwifts.Location = New System.Drawing.Point(216, 54)
        Me.ChkAllSwifts.Name = "ChkAllSwifts"
        Me.ChkAllSwifts.Size = New System.Drawing.Size(113, 17)
        Me.ChkAllSwifts.TabIndex = 59
        Me.ChkAllSwifts.Text = "Inc. all swift codes"
        Me.ChkAllSwifts.UseVisualStyleBackColor = True
        '
        'ChknonValidSC
        '
        Me.ChknonValidSC.AutoSize = True
        Me.ChknonValidSC.Location = New System.Drawing.Point(216, 76)
        Me.ChknonValidSC.Name = "ChknonValidSC"
        Me.ChknonValidSC.Size = New System.Drawing.Size(149, 17)
        Me.ChknonValidSC.TabIndex = 57
        Me.ChknonValidSC.Text = "Inc. non valid Swift Codes"
        Me.ChknonValidSC.UseVisualStyleBackColor = True
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(495, 20)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 54
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(19, 54)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "Swift Code"
        '
        'cboSC
        '
        Me.cboSC.FormattingEnabled = True
        Me.cboSC.Location = New System.Drawing.Point(83, 51)
        Me.cboSC.Name = "cboSC"
        Me.cboSC.Size = New System.Drawing.Size(115, 21)
        Me.cboSC.TabIndex = 42
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(377, 54)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 29
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(377, 20)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 32)
        Me.CmdFilter.TabIndex = 31
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Bank Name"
        '
        'cboSCBankName
        '
        Me.cboSCBankName.FormattingEnabled = True
        Me.cboSCBankName.Location = New System.Drawing.Point(83, 24)
        Me.cboSCBankName.Name = "cboSCBankName"
        Me.cboSCBankName.Size = New System.Drawing.Size(282, 21)
        Me.cboSCBankName.TabIndex = 29
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(9, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(557, 13)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "Enter new SWIFT details manually at the bottom of the grid or automatically downl" &
    "oad via the SWIFT lookup function"
        '
        'BeneficiaryBankGroupBox
        '
        Me.BeneficiaryBankGroupBox.BackColor = System.Drawing.Color.AntiqueWhite
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.cmdSWIFT)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookCountryCode)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.Label6)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.Label5)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookBranch)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookCountry)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.Label4)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.PicLookSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookSWIFT)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookPostCode)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookCity)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress2)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtLookAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankZip)
        Me.BeneficiaryBankGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.BeneficiaryBankGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeneficiaryBankGroupBox.Location = New System.Drawing.Point(908, 12)
        Me.BeneficiaryBankGroupBox.Name = "BeneficiaryBankGroupBox"
        Me.BeneficiaryBankGroupBox.Size = New System.Drawing.Size(557, 137)
        Me.BeneficiaryBankGroupBox.TabIndex = 67
        Me.BeneficiaryBankGroupBox.TabStop = False
        Me.BeneficiaryBankGroupBox.Text = "SWIFT Lookup"
        '
        'cmdSWIFT
        '
        Me.cmdSWIFT.Location = New System.Drawing.Point(409, 17)
        Me.cmdSWIFT.Name = "cmdSWIFT"
        Me.cmdSWIFT.Size = New System.Drawing.Size(133, 23)
        Me.cmdSWIFT.TabIndex = 73
        Me.cmdSWIFT.Text = "Add SWIFT Details"
        Me.cmdSWIFT.UseVisualStyleBackColor = True
        '
        'txtLookCountryCode
        '
        Me.txtLookCountryCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookCountryCode.Location = New System.Drawing.Point(231, 102)
        Me.txtLookCountryCode.Name = "txtLookCountryCode"
        Me.txtLookCountryCode.Size = New System.Drawing.Size(78, 20)
        Me.txtLookCountryCode.TabIndex = 71
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(174, 105)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 13)
        Me.Label6.TabIndex = 72
        Me.Label6.Text = "Cty Code:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(325, 53)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Branch:"
        '
        'txtLookBranch
        '
        Me.txtLookBranch.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookBranch.Location = New System.Drawing.Point(375, 50)
        Me.txtLookBranch.Name = "txtLookBranch"
        Me.txtLookBranch.Size = New System.Drawing.Size(167, 20)
        Me.txtLookBranch.TabIndex = 68
        '
        'txtLookCountry
        '
        Me.txtLookCountry.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookCountry.Location = New System.Drawing.Point(375, 102)
        Me.txtLookCountry.Name = "txtLookCountry"
        Me.txtLookCountry.Size = New System.Drawing.Size(167, 20)
        Me.txtLookCountry.TabIndex = 67
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Bank Name:"
        '
        'txtLookBankName
        '
        Me.txtLookBankName.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookBankName.Location = New System.Drawing.Point(80, 50)
        Me.txtLookBankName.Name = "txtLookBankName"
        Me.txtLookBankName.Size = New System.Drawing.Size(230, 20)
        Me.txtLookBankName.TabIndex = 31
        '
        'lblBenBankName
        '
        Me.lblBenBankName.AutoSize = True
        Me.lblBenBankName.Location = New System.Drawing.Point(14, 22)
        Me.lblBenBankName.Name = "lblBenBankName"
        Me.lblBenBankName.Size = New System.Drawing.Size(170, 13)
        Me.lblBenBankName.TabIndex = 34
        Me.lblBenBankName.Text = "Type SWIFT Code here to lookup:"
        '
        'PicLookSwift
        '
        Me.PicLookSwift.Location = New System.Drawing.Point(369, 23)
        Me.PicLookSwift.Name = "PicLookSwift"
        Me.PicLookSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicLookSwift.TabIndex = 64
        Me.PicLookSwift.TabStop = False
        '
        'lblBenBankSwift
        '
        Me.lblBenBankSwift.AutoSize = True
        Me.lblBenBankSwift.Location = New System.Drawing.Point(323, 105)
        Me.lblBenBankSwift.Name = "lblBenBankSwift"
        Me.lblBenBankSwift.Size = New System.Drawing.Size(46, 13)
        Me.lblBenBankSwift.TabIndex = 58
        Me.lblBenBankSwift.Text = "Country:"
        '
        'txtLookSWIFT
        '
        Me.txtLookSWIFT.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookSWIFT.Location = New System.Drawing.Point(190, 20)
        Me.txtLookSWIFT.Name = "txtLookSWIFT"
        Me.txtLookSWIFT.Size = New System.Drawing.Size(173, 20)
        Me.txtLookSWIFT.TabIndex = 30
        '
        'txtLookPostCode
        '
        Me.txtLookPostCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookPostCode.Location = New System.Drawing.Point(80, 102)
        Me.txtLookPostCode.Name = "txtLookPostCode"
        Me.txtLookPostCode.Size = New System.Drawing.Size(78, 20)
        Me.txtLookPostCode.TabIndex = 28
        '
        'lblBenBankAddress1
        '
        Me.lblBenBankAddress1.AutoSize = True
        Me.lblBenBankAddress1.Location = New System.Drawing.Point(17, 79)
        Me.lblBenBankAddress1.Name = "lblBenBankAddress1"
        Me.lblBenBankAddress1.Size = New System.Drawing.Size(57, 13)
        Me.lblBenBankAddress1.TabIndex = 61
        Me.lblBenBankAddress1.Text = "Address 1:"
        '
        'txtLookCity
        '
        Me.txtLookCity.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookCity.Location = New System.Drawing.Point(375, 76)
        Me.txtLookCity.Name = "txtLookCity"
        Me.txtLookCity.Size = New System.Drawing.Size(167, 20)
        Me.txtLookCity.TabIndex = 27
        '
        'lblBenBankAddress2
        '
        Me.lblBenBankAddress2.AutoSize = True
        Me.lblBenBankAddress2.Location = New System.Drawing.Point(342, 79)
        Me.lblBenBankAddress2.Name = "lblBenBankAddress2"
        Me.lblBenBankAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblBenBankAddress2.TabIndex = 62
        Me.lblBenBankAddress2.Text = "City:"
        '
        'txtLookAddress1
        '
        Me.txtLookAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtLookAddress1.Location = New System.Drawing.Point(80, 76)
        Me.txtLookAddress1.Name = "txtLookAddress1"
        Me.txtLookAddress1.Size = New System.Drawing.Size(230, 20)
        Me.txtLookAddress1.TabIndex = 26
        '
        'lblBenBankZip
        '
        Me.lblBenBankZip.AutoSize = True
        Me.lblBenBankZip.Location = New System.Drawing.Point(23, 105)
        Me.lblBenBankZip.Name = "lblBenBankZip"
        Me.lblBenBankZip.Size = New System.Drawing.Size(51, 13)
        Me.lblBenBankZip.TabIndex = 63
        Me.lblBenBankZip.Text = "Post/Zip:"
        '
        'ImageListTickCross
        '
        Me.ImageListTickCross.ImageStream = CType(resources.GetObject("ImageListTickCross.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListTickCross.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListTickCross.Images.SetKeyName(0, "tick (1)16.png")
        Me.ImageListTickCross.Images.SetKeyName(1, "button_cancel16.png")
        '
        'FrmSwiftCodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1477, 812)
        Me.Controls.Add(Me.BeneficiaryBankGroupBox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvSWIFT)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmSwiftCodes"
        Me.Text = "SWIFT Codes"
        CType(Me.dgvSWIFT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        Me.BeneficiaryBankGroupBox.ResumeLayout(False)
        Me.BeneficiaryBankGroupBox.PerformLayout()
        CType(Me.PicLookSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvSWIFT As DataGridView
    Friend WithEvents Label13 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents ChknonValidSC As CheckBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents cboSC As ComboBox
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CmdFilter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboSCBankName As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents BeneficiaryBankGroupBox As GroupBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtLookBranch As TextBox
    Friend WithEvents txtLookCountry As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtLookBankName As TextBox
    Friend WithEvents lblBenBankName As Label
    Friend WithEvents PicLookSwift As PictureBox
    Friend WithEvents lblBenBankSwift As Label
    Friend WithEvents txtLookSWIFT As TextBox
    Friend WithEvents txtLookPostCode As TextBox
    Friend WithEvents lblBenBankAddress1 As Label
    Friend WithEvents txtLookCity As TextBox
    Friend WithEvents lblBenBankAddress2 As Label
    Friend WithEvents txtLookAddress1 As TextBox
    Friend WithEvents lblBenBankZip As Label
    Friend WithEvents txtLookCountryCode As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cmdSWIFT As Button
    Friend WithEvents ImageListTickCross As ImageList
    Friend WithEvents ChkAllSwifts As CheckBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
