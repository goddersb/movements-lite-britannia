﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCashTranComment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.tabCtrlUpdate = New System.Windows.Forms.TabControl()
        Me.tbUpdateComment = New System.Windows.Forms.TabPage()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtComment = New System.Windows.Forms.RichTextBox()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.dgvTransaction = New System.Windows.Forms.DataGridView()
        Me.grpFilter = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdClear = New System.Windows.Forms.Button()
        Me.cmdFilter = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker()
        Me.dtpFromDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDocNo = New System.Windows.Forms.TextBox()
        Me.tbApproveComment = New System.Windows.Forms.TabPage()
        Me.dgvRequest = New System.Windows.Forms.DataGridView()
        Me.dtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.cboRecordType = New System.Windows.Forms.ComboBox()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.tabCtrlUpdate.SuspendLayout()
        Me.tbUpdateComment.SuspendLayout()
        CType(Me.dgvTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFilter.SuspendLayout()
        Me.tbApproveComment.SuspendLayout()
        CType(Me.dgvRequest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabCtrlUpdate
        '
        Me.tabCtrlUpdate.Controls.Add(Me.tbUpdateComment)
        Me.tabCtrlUpdate.Controls.Add(Me.tbApproveComment)
        Me.tabCtrlUpdate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabCtrlUpdate.Location = New System.Drawing.Point(0, 0)
        Me.tabCtrlUpdate.Name = "tabCtrlUpdate"
        Me.tabCtrlUpdate.SelectedIndex = 0
        Me.tabCtrlUpdate.Size = New System.Drawing.Size(1124, 578)
        Me.tabCtrlUpdate.TabIndex = 2
        '
        'tbUpdateComment
        '
        Me.tbUpdateComment.BackColor = System.Drawing.Color.OldLace
        Me.tbUpdateComment.Controls.Add(Me.lblTitle)
        Me.tbUpdateComment.Controls.Add(Me.Label5)
        Me.tbUpdateComment.Controls.Add(Me.txtComment)
        Me.tbUpdateComment.Controls.Add(Me.cmdCancel)
        Me.tbUpdateComment.Controls.Add(Me.cmdSubmit)
        Me.tbUpdateComment.Controls.Add(Me.dgvTransaction)
        Me.tbUpdateComment.Controls.Add(Me.grpFilter)
        Me.tbUpdateComment.Location = New System.Drawing.Point(4, 22)
        Me.tbUpdateComment.Name = "tbUpdateComment"
        Me.tbUpdateComment.Padding = New System.Windows.Forms.Padding(3)
        Me.tbUpdateComment.Size = New System.Drawing.Size(1116, 552)
        Me.tbUpdateComment.TabIndex = 0
        Me.tbUpdateComment.Text = "Update Comment"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblTitle.Location = New System.Drawing.Point(8, 19)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(277, 24)
        Me.lblTitle.TabIndex = 13
        Me.lblTitle.Text = "Cash Transaction Comments"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 171)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "New Comment:"
        '
        'txtComment
        '
        Me.txtComment.Location = New System.Drawing.Point(94, 161)
        Me.txtComment.Name = "txtComment"
        Me.txtComment.Size = New System.Drawing.Size(712, 31)
        Me.txtComment.TabIndex = 12
        Me.txtComment.Text = ""
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(955, 161)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(140, 32)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(809, 161)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(140, 32)
        Me.cmdSubmit.TabIndex = 10
        Me.cmdSubmit.Text = "Submit"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'dgvTransaction
        '
        Me.dgvTransaction.AllowUserToAddRows = False
        Me.dgvTransaction.AllowUserToDeleteRows = False
        Me.dgvTransaction.AllowUserToOrderColumns = True
        Me.dgvTransaction.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTransaction.Location = New System.Drawing.Point(3, 199)
        Me.dgvTransaction.MultiSelect = False
        Me.dgvTransaction.Name = "dgvTransaction"
        Me.dgvTransaction.ReadOnly = True
        Me.dgvTransaction.Size = New System.Drawing.Size(1145, 350)
        Me.dgvTransaction.TabIndex = 8
        '
        'grpFilter
        '
        Me.grpFilter.Controls.Add(Me.Label6)
        Me.grpFilter.Controls.Add(Me.cmdClear)
        Me.grpFilter.Controls.Add(Me.cmdFilter)
        Me.grpFilter.Controls.Add(Me.Label3)
        Me.grpFilter.Controls.Add(Me.Label4)
        Me.grpFilter.Controls.Add(Me.dtpToDate)
        Me.grpFilter.Controls.Add(Me.dtpFromDate)
        Me.grpFilter.Controls.Add(Me.Label2)
        Me.grpFilter.Controls.Add(Me.cboPortfolio)
        Me.grpFilter.Controls.Add(Me.Label1)
        Me.grpFilter.Controls.Add(Me.txtDocNo)
        Me.grpFilter.Location = New System.Drawing.Point(6, 55)
        Me.grpFilter.Name = "grpFilter"
        Me.grpFilter.Size = New System.Drawing.Size(1086, 100)
        Me.grpFilter.TabIndex = 9
        Me.grpFilter.TabStop = False
        Me.grpFilter.Text = "Filter"
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(839, 39)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(225, 39)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "* Please note new comment are subject to approval.  If changes are urgent please " &
    "make sure you contact the approver immediately."
        '
        'cmdClear
        '
        Me.cmdClear.Location = New System.Drawing.Point(679, 71)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(122, 23)
        Me.cmdClear.TabIndex = 9
        Me.cmdClear.Text = "Clear"
        Me.cmdClear.UseVisualStyleBackColor = True
        '
        'cmdFilter
        '
        Me.cmdFilter.Location = New System.Drawing.Point(679, 33)
        Me.cmdFilter.Name = "cmdFilter"
        Me.cmdFilter.Size = New System.Drawing.Size(122, 23)
        Me.cmdFilter.TabIndex = 8
        Me.cmdFilter.Text = "Filter"
        Me.cmdFilter.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(434, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "End Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(201, 59)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Start Date:"
        '
        'dtpToDate
        '
        Me.dtpToDate.Location = New System.Drawing.Point(437, 74)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(226, 20)
        Me.dtpToDate.TabIndex = 5
        '
        'dtpFromDate
        '
        Me.dtpFromDate.Location = New System.Drawing.Point(204, 75)
        Me.dtpFromDate.Name = "dtpFromDate"
        Me.dtpFromDate.Size = New System.Drawing.Size(227, 20)
        Me.dtpFromDate.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(200, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Portfolio:"
        '
        'cboPortfolio
        '
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(203, 35)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(460, 21)
        Me.cboPortfolio.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Doc No:"
        '
        'txtDocNo
        '
        Me.txtDocNo.Location = New System.Drawing.Point(3, 36)
        Me.txtDocNo.Name = "txtDocNo"
        Me.txtDocNo.Size = New System.Drawing.Size(185, 20)
        Me.txtDocNo.TabIndex = 0
        '
        'tbApproveComment
        '
        Me.tbApproveComment.BackColor = System.Drawing.Color.OldLace
        Me.tbApproveComment.Controls.Add(Me.dgvRequest)
        Me.tbApproveComment.Controls.Add(Me.dtpEndDate)
        Me.tbApproveComment.Controls.Add(Me.dtpStartDate)
        Me.tbApproveComment.Controls.Add(Me.cboRecordType)
        Me.tbApproveComment.Controls.Add(Me.cmdRefresh)
        Me.tbApproveComment.Location = New System.Drawing.Point(4, 22)
        Me.tbApproveComment.Name = "tbApproveComment"
        Me.tbApproveComment.Padding = New System.Windows.Forms.Padding(3)
        Me.tbApproveComment.Size = New System.Drawing.Size(1116, 552)
        Me.tbApproveComment.TabIndex = 1
        Me.tbApproveComment.Text = "Approve Comment"
        '
        'dgvRequest
        '
        Me.dgvRequest.AllowUserToAddRows = False
        Me.dgvRequest.AllowUserToDeleteRows = False
        Me.dgvRequest.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRequest.Location = New System.Drawing.Point(10, 60)
        Me.dgvRequest.Name = "dgvRequest"
        Me.dgvRequest.ReadOnly = True
        Me.dgvRequest.Size = New System.Drawing.Size(1078, 425)
        Me.dgvRequest.TabIndex = 1
        '
        'dtpEndDate
        '
        Me.dtpEndDate.Location = New System.Drawing.Point(465, 22)
        Me.dtpEndDate.Name = "dtpEndDate"
        Me.dtpEndDate.Size = New System.Drawing.Size(226, 20)
        Me.dtpEndDate.TabIndex = 7
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Location = New System.Drawing.Point(232, 22)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(227, 20)
        Me.dtpStartDate.TabIndex = 6
        '
        'cboRecordType
        '
        Me.cboRecordType.FormattingEnabled = True
        Me.cboRecordType.Location = New System.Drawing.Point(11, 21)
        Me.cboRecordType.Name = "cboRecordType"
        Me.cboRecordType.Size = New System.Drawing.Size(215, 21)
        Me.cboRecordType.TabIndex = 3
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Location = New System.Drawing.Point(697, 21)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(132, 23)
        Me.cmdRefresh.TabIndex = 2
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'FrmCashTranComment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1124, 578)
        Me.Controls.Add(Me.tabCtrlUpdate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCashTranComment"
        Me.Text = "Cash Transaction Comment"
        Me.tabCtrlUpdate.ResumeLayout(False)
        Me.tbUpdateComment.ResumeLayout(False)
        Me.tbUpdateComment.PerformLayout()
        CType(Me.dgvTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFilter.ResumeLayout(False)
        Me.grpFilter.PerformLayout()
        Me.tbApproveComment.ResumeLayout(False)
        CType(Me.dgvRequest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents tabCtrlUpdate As TabControl
    Friend WithEvents tbUpdateComment As TabPage
    Friend WithEvents Label5 As Label
    Friend WithEvents txtComment As RichTextBox
    Friend WithEvents cmdCancel As Button
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents dgvTransaction As DataGridView
    Friend WithEvents grpFilter As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cmdClear As Button
    Friend WithEvents cmdFilter As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents dtpToDate As DateTimePicker
    Friend WithEvents dtpFromDate As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDocNo As TextBox
    Friend WithEvents tbApproveComment As TabPage
    Friend WithEvents dgvRequest As DataGridView
    Friend WithEvents dtpEndDate As DateTimePicker
    Friend WithEvents dtpStartDate As DateTimePicker
    Friend WithEvents cboRecordType As ComboBox
    Friend WithEvents cmdRefresh As Button
    Friend WithEvents lblTitle As Label
End Class
