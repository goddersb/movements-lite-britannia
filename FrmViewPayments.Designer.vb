﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmViewPayments
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmViewPayments))
        Me.dgvViewCash = New System.Windows.Forms.DataGridView()
        Me.cboViewStatus = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboPayCheck = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.ChkViewRefresh = New System.Windows.Forms.CheckBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cboViewBenRef = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cboViewOrderRef = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.cboViewPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboViewID = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboViewAuthorisedBy = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboViewEnteredBy = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboViewBenName = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboViewBenAccount = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboViewCCY = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboViewPayType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboViewOrderName = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboViewOrderAccount = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboViewSettleDate = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboViewTransDate = New System.Windows.Forms.ComboBox()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cmdViewSubmit = New System.Windows.Forms.Button()
        Me.TimerViewRefresh = New System.Windows.Forms.Timer(Me.components)
        CType(Me.dgvViewCash, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvViewCash
        '
        Me.dgvViewCash.AllowUserToAddRows = False
        Me.dgvViewCash.AllowUserToDeleteRows = False
        Me.dgvViewCash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvViewCash.Location = New System.Drawing.Point(12, 150)
        Me.dgvViewCash.Name = "dgvViewCash"
        Me.dgvViewCash.ReadOnly = True
        Me.dgvViewCash.Size = New System.Drawing.Size(1516, 618)
        Me.dgvViewCash.TabIndex = 19
        '
        'cboViewStatus
        '
        Me.cboViewStatus.FormattingEnabled = True
        Me.cboViewStatus.Location = New System.Drawing.Point(266, 19)
        Me.cboViewStatus.Name = "cboViewStatus"
        Me.cboViewStatus.Size = New System.Drawing.Size(164, 21)
        Me.cboViewStatus.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(196, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Status Type"
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(1247, 76)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 17
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.cboPayCheck)
        Me.ViewGroupBox.Controls.Add(Me.Label23)
        Me.ViewGroupBox.Controls.Add(Me.ChkViewRefresh)
        Me.ViewGroupBox.Controls.Add(Me.Label15)
        Me.ViewGroupBox.Controls.Add(Me.cboViewBenRef)
        Me.ViewGroupBox.Controls.Add(Me.Label16)
        Me.ViewGroupBox.Controls.Add(Me.cboViewOrderRef)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.cboViewPortfolio)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboViewID)
        Me.ViewGroupBox.Controls.Add(Me.Label14)
        Me.ViewGroupBox.Controls.Add(Me.Label11)
        Me.ViewGroupBox.Controls.Add(Me.cboViewAuthorisedBy)
        Me.ViewGroupBox.Controls.Add(Me.Label12)
        Me.ViewGroupBox.Controls.Add(Me.cboViewEnteredBy)
        Me.ViewGroupBox.Controls.Add(Me.Label9)
        Me.ViewGroupBox.Controls.Add(Me.cboViewBenName)
        Me.ViewGroupBox.Controls.Add(Me.Label10)
        Me.ViewGroupBox.Controls.Add(Me.cboViewBenAccount)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboViewCCY)
        Me.ViewGroupBox.Controls.Add(Me.Label7)
        Me.ViewGroupBox.Controls.Add(Me.cboViewPayType)
        Me.ViewGroupBox.Controls.Add(Me.Label5)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.cboViewOrderName)
        Me.ViewGroupBox.Controls.Add(Me.Label6)
        Me.ViewGroupBox.Controls.Add(Me.cboViewOrderAccount)
        Me.ViewGroupBox.Controls.Add(Me.Label4)
        Me.ViewGroupBox.Controls.Add(Me.cboViewSettleDate)
        Me.ViewGroupBox.Controls.Add(Me.Label3)
        Me.ViewGroupBox.Controls.Add(Me.cboViewTransDate)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.cboViewStatus)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 36)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(1516, 108)
        Me.ViewGroupBox.TabIndex = 30
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Movements Criteria"
        '
        'cboPayCheck
        '
        Me.cboPayCheck.FormattingEnabled = True
        Me.cboPayCheck.Location = New System.Drawing.Point(1372, 17)
        Me.cboPayCheck.Name = "cboPayCheck"
        Me.cboPayCheck.Size = New System.Drawing.Size(138, 21)
        Me.cboPayCheck.TabIndex = 68
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(1269, 19)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(81, 13)
        Me.Label23.TabIndex = 67
        Me.Label23.Text = "Payment Period"
        '
        'ChkViewRefresh
        '
        Me.ChkViewRefresh.AutoSize = True
        Me.ChkViewRefresh.Checked = True
        Me.ChkViewRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkViewRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkViewRefresh.Location = New System.Drawing.Point(1365, 45)
        Me.ChkViewRefresh.Name = "ChkViewRefresh"
        Me.ChkViewRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkViewRefresh.TabIndex = 16
        Me.ChkViewRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkViewRefresh.UseVisualStyleBackColor = True
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(10, 76)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(46, 13)
        Me.Label15.TabIndex = 58
        Me.Label15.Text = "Ben Ref"
        '
        'cboViewBenRef
        '
        Me.cboViewBenRef.FormattingEnabled = True
        Me.cboViewBenRef.Location = New System.Drawing.Point(62, 73)
        Me.cboViewBenRef.Name = "cboViewBenRef"
        Me.cboViewBenRef.Size = New System.Drawing.Size(127, 21)
        Me.cboViewBenRef.TabIndex = 10
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(3, 49)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(53, 13)
        Me.Label16.TabIndex = 56
        Me.Label16.Text = "Order Ref"
        '
        'cboViewOrderRef
        '
        Me.cboViewOrderRef.FormattingEnabled = True
        Me.cboViewOrderRef.Location = New System.Drawing.Point(62, 46)
        Me.cboViewOrderRef.Name = "cboViewOrderRef"
        Me.cboViewOrderRef.Size = New System.Drawing.Size(127, 21)
        Me.cboViewOrderRef.TabIndex = 6
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1466, 62)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 18
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'cboViewPortfolio
        '
        Me.cboViewPortfolio.FormattingEnabled = True
        Me.cboViewPortfolio.Location = New System.Drawing.Point(819, 46)
        Me.cboViewPortfolio.Name = "cboViewPortfolio"
        Me.cboViewPortfolio.Size = New System.Drawing.Size(422, 21)
        Me.cboViewPortfolio.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(768, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Portfolio"
        '
        'cboViewID
        '
        Me.cboViewID.FormattingEnabled = True
        Me.cboViewID.Location = New System.Drawing.Point(62, 19)
        Me.cboViewID.Name = "cboViewID"
        Me.cboViewID.Size = New System.Drawing.Size(69, 21)
        Me.cboViewID.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(38, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(18, 13)
        Me.Label14.TabIndex = 53
        Me.Label14.Text = "ID"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(994, 76)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 13)
        Me.Label11.TabIndex = 51
        Me.Label11.Text = "Authorised By"
        '
        'cboViewAuthorisedBy
        '
        Me.cboViewAuthorisedBy.FormattingEnabled = True
        Me.cboViewAuthorisedBy.Location = New System.Drawing.Point(1072, 73)
        Me.cboViewAuthorisedBy.Name = "cboViewAuthorisedBy"
        Me.cboViewAuthorisedBy.Size = New System.Drawing.Size(169, 21)
        Me.cboViewAuthorisedBy.TabIndex = 14
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(754, 76)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 13)
        Me.Label12.TabIndex = 49
        Me.Label12.Text = "Entered By"
        '
        'cboViewEnteredBy
        '
        Me.cboViewEnteredBy.FormattingEnabled = True
        Me.cboViewEnteredBy.Location = New System.Drawing.Point(819, 73)
        Me.cboViewEnteredBy.Name = "cboViewEnteredBy"
        Me.cboViewEnteredBy.Size = New System.Drawing.Size(169, 21)
        Me.cboViewEnteredBy.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(475, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 47
        Me.Label9.Text = "Ben Name"
        '
        'cboViewBenName
        '
        Me.cboViewBenName.FormattingEnabled = True
        Me.cboViewBenName.Location = New System.Drawing.Point(538, 73)
        Me.cboViewBenName.Name = "cboViewBenName"
        Me.cboViewBenName.Size = New System.Drawing.Size(197, 21)
        Me.cboViewBenName.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(212, 76)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 13)
        Me.Label10.TabIndex = 45
        Me.Label10.Text = "Ben Acc"
        '
        'cboViewBenAccount
        '
        Me.cboViewBenAccount.FormattingEnabled = True
        Me.cboViewBenAccount.Location = New System.Drawing.Point(266, 73)
        Me.cboViewBenAccount.Name = "cboViewBenAccount"
        Me.cboViewBenAccount.Size = New System.Drawing.Size(197, 21)
        Me.cboViewBenAccount.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(785, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "CCY"
        '
        'cboViewCCY
        '
        Me.cboViewCCY.FormattingEnabled = True
        Me.cboViewCCY.Location = New System.Drawing.Point(819, 19)
        Me.cboViewCCY.Name = "cboViewCCY"
        Me.cboViewCCY.Size = New System.Drawing.Size(55, 21)
        Me.cboViewCCY.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(475, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "Movement"
        '
        'cboViewPayType
        '
        Me.cboViewPayType.FormattingEnabled = True
        Me.cboViewPayType.Location = New System.Drawing.Point(538, 19)
        Me.cboViewPayType.Name = "cboViewPayType"
        Me.cboViewPayType.Size = New System.Drawing.Size(164, 21)
        Me.cboViewPayType.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(499, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 39
        Me.Label5.Text = "Order"
        '
        'cboViewOrderName
        '
        Me.cboViewOrderName.FormattingEnabled = True
        Me.cboViewOrderName.Location = New System.Drawing.Point(538, 46)
        Me.cboViewOrderName.Name = "cboViewOrderName"
        Me.cboViewOrderName.Size = New System.Drawing.Size(197, 21)
        Me.cboViewOrderName.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(205, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 37
        Me.Label6.Text = "Order Acc"
        '
        'cboViewOrderAccount
        '
        Me.cboViewOrderAccount.FormattingEnabled = True
        Me.cboViewOrderAccount.Location = New System.Drawing.Point(266, 46)
        Me.cboViewOrderAccount.Name = "cboViewOrderAccount"
        Me.cboViewOrderAccount.Size = New System.Drawing.Size(198, 21)
        Me.cboViewOrderAccount.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1081, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Settle Date"
        '
        'cboViewSettleDate
        '
        Me.cboViewSettleDate.FormattingEnabled = True
        Me.cboViewSettleDate.Location = New System.Drawing.Point(1147, 19)
        Me.cboViewSettleDate.Name = "cboViewSettleDate"
        Me.cboViewSettleDate.Size = New System.Drawing.Size(94, 21)
        Me.cboViewSettleDate.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(910, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Trans Date"
        '
        'cboViewTransDate
        '
        Me.cboViewTransDate.FormattingEnabled = True
        Me.cboViewTransDate.Location = New System.Drawing.Point(976, 19)
        Me.cboViewTransDate.Name = "cboViewTransDate"
        Me.cboViewTransDate.Size = New System.Drawing.Size(94, 21)
        Me.cboViewTransDate.TabIndex = 4
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(1247, 41)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 32)
        Me.CmdFilter.TabIndex = 15
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(169, 24)
        Me.Label13.TabIndex = 39
        Me.Label13.Text = "View Movements"
        '
        'cmdViewSubmit
        '
        Me.cmdViewSubmit.Location = New System.Drawing.Point(1371, 774)
        Me.cmdViewSubmit.Name = "cmdViewSubmit"
        Me.cmdViewSubmit.Size = New System.Drawing.Size(157, 24)
        Me.cmdViewSubmit.TabIndex = 20
        Me.cmdViewSubmit.Text = "Submit transactions"
        Me.cmdViewSubmit.UseVisualStyleBackColor = True
        '
        'TimerViewRefresh
        '
        Me.TimerViewRefresh.Interval = 60000
        '
        'FrmViewPayments
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1536, 803)
        Me.Controls.Add(Me.cmdViewSubmit)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.Controls.Add(Me.dgvViewCash)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmViewPayments"
        Me.Text = "View Movements"
        CType(Me.dgvViewCash, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvViewCash As System.Windows.Forms.DataGridView
    Friend WithEvents cboViewStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CmdRefreshView As System.Windows.Forms.Button
    Friend WithEvents ViewGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboViewSettleDate As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboViewTransDate As System.Windows.Forms.ComboBox
    Friend WithEvents CmdFilter As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboViewPortfolio As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboViewOrderName As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboViewOrderAccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboViewBenName As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboViewBenAccount As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboViewCCY As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboViewPayType As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboViewAuthorisedBy As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboViewEnteredBy As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmdViewSubmit As System.Windows.Forms.Button
    Friend WithEvents cboViewID As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents TimerViewRefresh As Timer
    Friend WithEvents ChkViewRefresh As CheckBox
    Friend WithEvents Label15 As Label
    Friend WithEvents cboViewBenRef As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cboViewOrderRef As ComboBox
    Friend WithEvents cboPayCheck As ComboBox
    Friend WithEvents Label23 As Label
End Class
