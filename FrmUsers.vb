﻿Public Class FrmUsers
    Dim UsersConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataUsername As New SqlClient.SqlDataAdapter
    Dim cboUsernameColumn = New DataGridViewComboBoxColumn
    Dim ds As New DataSet

    Private Sub UsersLoadGrid(Optional ByVal varFilter As String = "")
        Dim varSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If varFilter = "" Then
                varSQL = "With RoleMembers(member_principal_id, role_principal_id) AS ( " &
                                "Select rm1.member_principal_id,rm1.role_principal_id  From sys.database_role_members rm1 (NOLOCK) " &
                                "UNION ALL " &
                                "Select d.member_principal_id,rm.role_principal_id From sys.database_role_members rm (NOLOCK) " &
                                "INNER Join RoleMembers AS d On rm.member_principal_id = d.role_principal_id ) " &
                                    "Select distinct u.* " &
                                    "From RoleMembers drm " &
                                    "Join sys.database_principals rp on (drm.role_principal_id = rp.principal_id) " &
                                    "Join sys.database_principals mp on (drm.member_principal_id = mp.principal_id) " &
                                    "join vwDolfinPaymentUsers u on mp.name = u.U_Username " &
                                    "Where rp.name = 'DolfinPaymentsRole' order by U_Username"
            Else
                varSQL = "With RoleMembers(member_principal_id, role_principal_id) AS ( " &
                                "Select rm1.member_principal_id,rm1.role_principal_id  From sys.database_role_members rm1 (NOLOCK) " &
                                "UNION ALL " &
                                "Select d.member_principal_id,rm.role_principal_id From sys.database_role_members rm (NOLOCK) " &
                                "INNER Join RoleMembers AS d On rm.member_principal_id = d.role_principal_id ) " &
                                    "Select distinct u.* " &
                                    "From RoleMembers drm " &
                                    "Join sys.database_principals rp on (drm.role_principal_id = rp.principal_id) " &
                                    "Join sys.database_principals mp on (drm.member_principal_id = mp.principal_id) " &
                                    "join vwDolfinPaymentUsers u on mp.name = u.U_Username " &
                                    "Where rp.name = 'DolfinPaymentsRole' and U_Username in (" & varFilter & ") order by U_Username"
            End If

            dgvUsers.DataSource = Nothing
            UsersConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, UsersConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentUsers")
            dgvUsers.DataSource = ds.Tables("vwDolfinPaymentUsers")

            Dim varColSQL As String = "With RoleMembers(member_principal_id, role_principal_id) AS ( " &
                                "Select rm1.member_principal_id,rm1.role_principal_id  From sys.database_role_members rm1 (NOLOCK) " &
                                "UNION ALL " &
                                "Select d.member_principal_id,rm.role_principal_id From sys.database_role_members rm (NOLOCK) " &
                                "INNER Join RoleMembers AS d On rm.member_principal_id = d.role_principal_id ) " &
                                    "Select distinct mp.name As U_Username " &
                                    "From RoleMembers drm " &
                                    "Join sys.database_principals rp on (drm.role_principal_id = rp.principal_id) " &
                                    "Join sys.database_principals mp on (drm.member_principal_id = mp.principal_id) " &
                                    "left join vwDolfinPaymentUsers u on mp.name = u.U_Username " &
                                    "Where rp.name = 'DolfinPaymentsRole' order by mp.name"

            dgvdataUsername = New SqlClient.SqlDataAdapter(varColSQL, UsersConn)
            dgvdataUsername.Fill(ds, "UserNameTable")
            cboUsernameColumn = New DataGridViewComboBoxColumn
            cboUsernameColumn.HeaderText = "Username"
            cboUsernameColumn.DataPropertyName = "U_Username"
            cboUsernameColumn.DataSource = ds.Tables("UserNameTable")
            cboUsernameColumn.ValueMember = ds.Tables("UserNameTable").Columns(0).ColumnName
            cboUsernameColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboUsernameColumn.FlatStyle = FlatStyle.Flat
            dgvUsers.Columns.Insert(0, cboUsernameColumn)


            dgvUsers.Columns(0).Width = 300
            dgvUsers.Columns(cUserLogin).Width = 100
            dgvUsers.Columns(cUserLogin).HeaderText = "Users"
            dgvUsers.Columns(cUserPayments).Width = 100
            dgvUsers.Columns(cUserPayments).HeaderText = "Movements"
            dgvUsers.Columns(cUserAuthorise).Width = 100
            dgvUsers.Columns(cUserAuthorise).HeaderText = "Authorise Movements"
            dgvUsers.Columns(cUserAuthorise3rdPartyPayments).Width = 100
            dgvUsers.Columns(cUserAuthorise3rdPartyPayments).HeaderText = "Authorise 3rd Party"
            dgvUsers.Columns(cUserAuthoriseMyPayments).Width = 100
            dgvUsers.Columns(cUserAuthoriseMyPayments).HeaderText = "Authorise My Payments"
            dgvUsers.Columns(cUserEdit).Width = 100
            dgvUsers.Columns(cUserEdit).HeaderText = "Edit Movements"
            dgvUsers.Columns(cUserDelete).Width = 100
            dgvUsers.Columns(cUserDelete).HeaderText = "Delete Movements"
            dgvUsers.Columns(cUserSubmit).Width = 100
            dgvUsers.Columns(cUserSubmit).HeaderText = "Submit Movements"
            dgvUsers.Columns(cUserCutoff).Width = 100
            dgvUsers.Columns(cUserCutoff).HeaderText = "Cut Offs"
            dgvUsers.Columns(cUserTransferFees).Width = 100
            dgvUsers.Columns(cUserTransferFees).HeaderText = "Transfer Fees"
            dgvUsers.Columns(cUserAudit).Width = 100
            dgvUsers.Columns(cUserAudit).HeaderText = "Audit Log"
            dgvUsers.Columns(cUserOmnibus).Width = 100
            dgvUsers.Columns(cUserOmnibus).HeaderText = "Treasury"
            dgvUsers.Columns(cUserOmnibusDeposits).Width = 100
            dgvUsers.Columns(cUserOmnibusDeposits).HeaderText = "Treasury Term Deposits"
            dgvUsers.Columns(cUserTemplates).Width = 100
            dgvUsers.Columns(cUserTemplates).HeaderText = "Templates"
            dgvUsers.Columns(cUserBalances).Width = 100
            dgvUsers.Columns(cUserBalances).HeaderText = "Balances"
            dgvUsers.Columns(cUserSwift).Width = 100
            dgvUsers.Columns(cUserSwift).HeaderText = "Swift"
            dgvUsers.Columns(cUserSwiftIntermediary).Width = 100
            dgvUsers.Columns(cUserSwiftIntermediary).HeaderText = "Swift Intermediary"
            dgvUsers.Columns(cUserSettings).Width = 100
            dgvUsers.Columns(cUserSettings).HeaderText = "System Settings"
            dgvUsers.Columns(cUserRD).Width = 100
            dgvUsers.Columns(cUserRD).HeaderText = "Receive/Deliver"
            dgvUsers.Columns(cUserAboveThreshold).Width = 100
            dgvUsers.Columns(cUserAboveThreshold).HeaderText = "Above Threshold"
            dgvUsers.Columns(cUserAutoSwift).Width = 100
            dgvUsers.Columns(cUserAutoSwift).HeaderText = "Auto Swift"
            dgvUsers.Columns(cUserSwiftRead).Width = 100
            dgvUsers.Columns(cUserSwiftRead).HeaderText = "Swift Read"
            dgvUsers.Columns(cUserConfirmReceipts).Width = 100
            dgvUsers.Columns(cUserConfirmReceipts).HeaderText = "Confirm Receipts"
            dgvUsers.Columns(cUserFees).Width = 100
            dgvUsers.Columns(cUserFees).HeaderText = "Fees"
            dgvUsers.Columns(cUserCA).Width = 100
            dgvUsers.Columns(cUserCA).HeaderText = "Corp Action"
            dgvUsers.Columns(cUserAuthoriseAccountMgt).Width = 100
            dgvUsers.Columns(cUserAuthoriseAccountMgt).HeaderText = "Authorise Account Mgt"
            dgvUsers.Columns(cUserAuthoriseAccountMgtLevel2).Width = 100
            dgvUsers.Columns(cUserAuthoriseAccountMgtLevel2).HeaderText = "Authorise Account Mgt Level 2"
            dgvUsers.Columns(cUserAuthoriseCompliance).Width = 100
            dgvUsers.Columns(cUserAuthoriseCompliance).HeaderText = "Authorise Compliance"
            dgvUsers.Columns(cUserAuthoriseComplianceLevel2).Width = 100
            dgvUsers.Columns(cUserAuthoriseComplianceLevel2).HeaderText = "Authorise Compliance Level 2"
            dgvUsers.Columns(cUserAuthoriseFeeSchedules).Width = 100
            dgvUsers.Columns(cUserAuthoriseFeeSchedules).HeaderText = "Authorise Fee Schedules"
            dgvUsers.Columns(cUserRA).Width = 100
            dgvUsers.Columns(cUserRA).HeaderText = "Risk Assessment"
            dgvUsers.Columns(cUserC).Width = 100
            dgvUsers.Columns(cUserC).HeaderText = "Comments"
            dgvUsers.Columns(cUserAC).Width = 100
            dgvUsers.Columns(cUserAC).HeaderText = "Authorise Comments"
            dgvUsers.Columns(cUserPG).Width = 100
            dgvUsers.Columns(cUserPG).HeaderText = "Password Generator"
            dgvUsers.Columns(cUserFeesPaid).Width = 100
            dgvUsers.Columns(cUserFeesPaid).HeaderText = "Fees Paid"
            dgvUsers.Columns(cUserAuthoriseFeesPaid).Width = 100
            dgvUsers.Columns(cUserAuthoriseFeesPaid).HeaderText = "Authorise Fees Paid"
            dgvUsers.Columns(cUserPayFeesPaid).Width = 100
            dgvUsers.Columns(cUserPayFeesPaid).HeaderText = "Pay Fees Paid"
            dgvUsers.Columns(cUserTreasuryMgt).Width = 100
            dgvUsers.Columns(cUserTreasuryMgt).HeaderText = "Treasury Mgt"
            dgvUsers.Columns(cUserRec).Width = 100
            dgvUsers.Columns(cUserRec).HeaderText = "Rec"
            dgvUsers.Columns(cUserBGMRec).Width = 100
            dgvUsers.Columns(cUserBGMRec).HeaderText = "BGM Rec"
            dgvUsers.Columns(cUserSwiftAccStatement).Width = 100
            dgvUsers.Columns(cUserSwiftAccStatement).HeaderText = "Swift Acc Statement"
            dgvUsers.Columns(cUserBatchProcessing).Width = 100
            dgvUsers.Columns(cUserBatchProcessing).HeaderText = "Batch Processing"
            dgvUsers.Columns(cUserComplianceWatchList).Width = 100
            dgvUsers.Columns(cUserComplianceWatchList).HeaderText = "Compliance Watchlist"
            dgvUsers.Columns(cUserComplianceWatchList).Width = 100
            dgvUsers.Columns(cUserComplianceWatchList).HeaderText = "Compliance Watchlist"
            dgvUsers.Columns(cUserCASS).Width = 100
            dgvUsers.Columns(cUserCASS).HeaderText = "CASS Accounts"
            dgvUsers.Columns(cUserClientEmailAddresses).Width = 100
            dgvUsers.Columns(cUserClientEmailAddresses).HeaderText = "Client Emails"
            dgvUsers.Columns(cUserClientStatementGenerator).Width = 100
            dgvUsers.Columns(cUserClientStatementGenerator).HeaderText = "Client Bal Check"
            dgvUsers.Columns(cUserClientSendQtr).Width = 100
            dgvUsers.Columns(cUserClientSendQtr).HeaderText = "Client Send Qtr"
            dgvUsers.Columns(cUserAuthorisePaymentsTeam).Width = 100
            dgvUsers.Columns(cUserAuthorisePaymentsTeam).HeaderText = "Authorise Payments Team"
            dgvUsers.Columns(cUserGPP).Width = 100
            dgvUsers.Columns(cUserGPP).HeaderText = "GPP Map"

            dgvUsers.Columns("U_Location").HeaderText = "Location"
            dgvUsers.Columns("U_Department").HeaderText = "Department"
            dgvUsers.Columns("U_EmailAddress").HeaderText = "Email Address"
            dgvUsers.Columns("U_UserModifiedBy").Width = 250
            dgvUsers.Columns("U_UserModifiedBy").HeaderText = "Modified by"
            dgvUsers.Columns("U_UserModifiedTime").Width = 100
            dgvUsers.Columns("U_UserModifiedTime").HeaderText = "Modified Time"
            dgvUsers.Columns("U_OptAutoRefresh").Visible = False
            dgvUsers.Columns("U_OptInactivity").Visible = False
            dgvUsers.Columns("U_OptGridSort").Visible = False
            dgvUsers.Columns("U_Username").Visible = False
            dgvUsers.Columns("U_Username").Frozen = True

            dgvUsers.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells

            dgvUsers.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvUsers.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Problem With viewing users grid")
        End Try
    End Sub

    Private Sub FrmUsers_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        UsersLoadGrid()
    End Sub

    Private Sub FrmUsers_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If UsersConn.State = ConnectionState.Open Then
            UsersConn.Close()
        End If
    End Sub

    Private Sub FrmUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UsersConn = ClsIMS.GetNewOpenConnection
    End Sub


    Private Sub dgvUsers_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvUsers.RowLeave
        Dim varUsername As String = ""

        If dgvUsers.EndEdit Then
            Try
                varUsername = IIf(IsDBNull(dgvUsers.Rows(e.RowIndex).Cells(0).Value), "", dgvUsers.Rows(e.RowIndex).Cells(0).Value)

                If InStr(varUsername, "\", vbTextCompare) <> 0 Then
                    Dim CheckUsername As String = ClsIMS.GetSQLItem("Select U_Username from vwDolfinPaymentUsers where U_Username = '" & varUsername & "'")
                    If CheckUsername = "" Then
                        ClsIMS.ExecuteString(UsersConn, "insert into vwDolfinPaymentUsers(U_Username, U_UserModifiedBy) values ('" & varUsername & "','" & ClsIMS.FullUserName & "')")
                        ClsIMS.ResetUserLogin(varUsername, "")
                    End If

                    Dim varSQL As String = "update vwDolfinPaymentUsers set "

                    Dim varCell As Integer = 2
                    AddFieldSQL(varSQL, e.RowIndex, varCell, "U_LoginPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 1, "U_UsersPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 2, "U_AuthorisePayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 3, "U_AuthoriseMyPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 4, "U_Authorise3rdPartyPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 5, "U_EditPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 6, "U_DeletePayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 7, "U_SubmitPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 8, "U_AuthoriseCutoffTimesPayments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 9, "U_TransferFees")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 10, "U_AuditLog")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 11, "U_OmnibusSwitch")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 12, "U_OmnibusSwitchTermDeposits")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 13, "U_Templates")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 14, "U_Balances")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 15, "U_Swift")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 16, "U_SwiftIntermediary")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 17, "U_SystemSettings")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 18, "U_ReceiveDeliver")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 19, "U_AuthoriseAboveThreshold")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 20, "U_AutoSwift")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 21, "U_SwiftRead")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 22, "U_ConfirmReceipts")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 23, "U_Fees")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 24, "U_CorpAction")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 25, "U_AuthoriseAccountMgt")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 26, "U_AuthoriseAccountMgtLevel2")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 27, "U_AuthoriseCompliance")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 28, "U_AuthoriseComplianceLevel2")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 29, "U_AuthoriseFeeSchedules")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 30, "U_RiskAssessment")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 31, "U_Comments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 32, "U_AuthoriseComments")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 33, "U_PasswordGenerator")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 34, "U_FeesPaid")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 35, "U_AuthoriseFeesPaid")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 36, "U_PayFeesPaid")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 37, "U_TreasuryMgt")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 38, "U_Rec")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 39, "U_BGMRec")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 40, "U_SwiftAccStatement")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 41, "U_BatchProcessing")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 42, "U_ComplianceWatchlist")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 43, "U_CASSAccounts")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 44, "U_ClientEmail")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 45, "U_ClientBalCheck")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 46, "U_ClientSendQtr")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 47, "U_AuthorisePaymentsTeam")
                    AddFieldSQL(varSQL, e.RowIndex, varCell + 48, "U_GPP")

                    varSQL = varSQL & "U_Location = '" & dgvUsers.Rows(e.RowIndex).Cells("U_Location").Value & "',U_EmailAddress = '" & dgvUsers.Rows(e.RowIndex).Cells("U_EmailAddress").Value & "',U_Department = '" & dgvUsers.Rows(e.RowIndex).Cells("U_Department").Value & "',U_UserModifiedBy = '" & ClsIMS.FullUserName & "',U_userModifiedTime=getdate() where u_username = '" & varUsername & "'"
                    ClsIMS.ExecuteString(UsersConn, varSQL)

                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameUsers, varSQL)
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Problem with updating username: " & varUsername)
            End Try
        End If
    End Sub

    Public Sub AddFieldSQL(ByRef varSQL As String, ByVal varRow As Integer, ByVal varCellValue As Integer, ByVal FieldName As String)
        If IsDBNull(dgvUsers.Rows(varRow).Cells(varCellValue).Value) Then
            varSQL = varSQL & FieldName & " = 0,"
        Else
            varSQL = varSQL & FieldName & " = " & IIf(dgvUsers.Rows(varRow).Cells(varCellValue).Value = True, 1, 0) & ","
        End If
    End Sub


    Private Sub dgvUsers_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvUsers.UserDeletingRow
        Dim varUsername As String = ""
        Try
            varUsername = dgvUsers.SelectedRows(0).Cells(0).Value
            Dim varResponse As Integer = MsgBox("Are you sure you want to delete user " & varUsername & "?", vbYesNo, "Are you sure")
            If vbYes Then
                ClsIMS.ExecuteString(UsersConn, "delete from vwDolfinPaymentUsers where U_Username = '" & varUsername & "'")
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameUsers, "user deleted - delete from vwDolfinPaymentUsers where U_Username = '" & varUsername & "')")
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Problem with deleting username: " & varUsername)
        End Try
    End Sub

End Class