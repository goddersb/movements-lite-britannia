﻿Imports Microsoft.Office

Public Class FrmPaymentRequest
    Private Const cNewTemplateName As String = "<New Template>"
    Private Const cPaymentSSIName As String = "PaymentSSI"
    Private Const cDefaultFilePath As String = "" '"\\EgnyteDrive\DolfinGroup\Shared\Dolfin\Business Development\Sales\Clients\Private\"
    Private Const cVolopaTemplateName As String = "Volopa Financial Services - GBP"
    Private Const cDefaultStartSearch As String = "https://www.google.com/search?q=("""""""
    Private Const cDefaultSearchA As String = """"""")%20AND%20((money%20laundering)%20OR%20(terrorist%20financing)%20OR%20(fraud)%20OR%20(tax%20evasion)%20OR%20(bribery)%20OR%20(corruption)%20OR%20(tax)%20OR%20(embezzlement))"
    Private Const cDefaultSearchB As String = """"""")%20AND%20((conviction)%20OR%20(criminal)%20OR%20(drug%20trafficking)%20OR%20(human%20trafficking)%20OR%20(crime)%20OR%20(prison))"
    Private AllowFee As Boolean = False
    Private cFirmMMPortfolioCode As Integer = 498
    Const ReceiveFree As String = "Receive Free"
    Const DeliverFree As String = "Deliver Free"

    Private Sub TransferFee()
        If IsNumeric(cboTemFindPortfolio.SelectedValue) Then
            cboTemFeeCCY.SelectedValue = cboTemFindCCY.SelectedValue
            If AllowFee Then
                If (ClsIMS.UserLocationCode = 42 And cboTemFeeCCY.SelectedValue = 6) Or (ClsIMS.UserLocationCode = 47 And cboTemFeeCCY.SelectedValue = 17) Then
                    Dim varFee As String = ClsIMS.GetSQLItem("select convert(nvarchar(20),TF_Fee) + ',' + TF_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & cboTemFindPortfolio.SelectedValue)
                    If varFee <> "" Then
                        ClsPay.PortfolioFeeCCY = Strings.Right(varFee, Len(varFee) - InStr(1, varFee, ",", vbTextCompare))
                        If IsNumeric(Strings.Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1)) Then
                            ClsPay.PortfolioFee = CInt(Strings.Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1))
                        End If
                        If Not IsNumeric(cboFeeType.SelectedValue) Then
                            MsgBox("PLEASE NOTE: This client will incur an external transfer fee of " & ClsPay.PortfolioFee & " " & ClsPay.PortfolioFeeCCY & " per transaction", vbInformation, "Transaction Fee")
                        End If
                    End If
                Else
                    Dim varIntFee As String = ClsIMS.GetSQLItem("select convert(nvarchar(20),TF_Int_Fee) + ',' + TF_Int_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & cboTemFindPortfolio.SelectedValue)
                    If varIntFee <> "" Then
                        ClsPay.PortfolioFeeCCYInt = Strings.Right(varIntFee, Len(varIntFee) - InStr(1, varIntFee, ",", vbTextCompare))
                        If IsNumeric(Strings.Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1)) Then
                            ClsPay.PortfolioFeeInt = CInt(Strings.Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1))
                        End If
                        If Not IsNumeric(cboFeeType.SelectedValue) Then
                            MsgBox("PLEASE NOTE: This client will incur an external international transfer fee of " & ClsPay.PortfolioFeeInt & " " & ClsPay.PortfolioFeeCCYInt & " per transaction", vbInformation, "Transaction Fee")
                        End If
                    End If
                End If

                If Not IsNumeric(cboFeeType.SelectedValue) Then
                    cboFeeType.SelectedValue = 0
                End If
            ElseIf cboPayType.SelectedValue = 1 Or cboPayType.SelectedValue = 3 Then
                If Not IsNumeric(cboFeeType.SelectedValue) Then
                    cboFeeType.SelectedValue = 1
                End If
            End If

            If cboPayType.SelectedValue <> 7 Then
                If Not IsNumeric(txtPRAmount.Text) Then
                    txtPRAmount.Text = 0
                End If
                If Not IsNumeric(txtPRAmountFee.Text) Then
                    txtPRAmountFee.Text = 0
                End If
                If cboPayType.SelectedValue = 8 Then
                    txtRef.Text = ClsIMS.GetSQLItem("Select VolopaToken from customers c inner join cuporel on c.cust_id = cupo_custid inner join DolfinPaymentPassword p on p.cust_id = c.cust_id where cupo_pfcode = " & cboTemFindPortfolio.SelectedValue)
                    If Not IsNumeric(txtRef.Text) Then
                        MsgBox("This client does not have a Volopa token ID associated with their account", vbExclamation)
                        Dim NewPG = New FrmPasswordGenerator
                        NewPG.ShowDialog()
                        txtRef.Text = ClsIMS.GetSQLItem("Select VolopaToken from customers c inner join cuporel on c.cust_id = cupo_custid inner join DolfinPaymentPassword p on p.cust_id = c.cust_id where cupo_pfcode = " & cboTemFindPortfolio.SelectedValue)
                    End If
                End If
                If AllowFee Then
                    FeeVisible(True)
                End If
            End If
            CalcPostBalance()
            CalcFeeBalance()
            CalcEndBalance()
        End If
        PopulateSearch()
    End Sub

    Public Sub SaveSwiftBankDetails(ByVal LookCity As String, ByVal LookBranch As String, ByVal LookAddress1 As String,
             ByVal LookPostCode As String, ByVal LookCountry As String, ByVal LookCountryCode As String)

        If cboLookSWIFT.Text <> "" Then
            If ValidSwift(cboLookSWIFT.Text) And txtLookBankName.Text <> "" Then
                Dim varSQL As String = "insert into DolfinPaymentSwiftCodes(SC_SwiftCode,SC_BankName,SC_City,SC_Branch,SC_Address1,SC_PostCode,SC_Country,SC_CountryCode,SC_LastModifiedDate,SC_Source) values ("
                varSQL = varSQL & "'" & UCase(cboLookSWIFT.Text) & "',"
                varSQL = varSQL & "'" & UCase(txtLookBankName.Text) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookCity, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookBranch, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookAddress1, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookPostCode, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookCountry, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(LookCountryCode, "'", "")) & "',"
                varSQL = varSQL & "getdate(),'USER')"

                ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, varSQL)
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodes, varSQL)
            End If
        End If
    End Sub

    Public Sub CheckSwiftCode()
        Dim varSwiftDetails As Dictionary(Of String, String)

        If cboLookSWIFT.Text <> "" Then
            If ValidSwift(cboLookSWIFT.Text) Then
                Dim varBankName As String = ClsIMS.GetSQLItem("Select SC_BankName from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & cboLookSWIFT.Text & "'")
                If varBankName = "" Then
                    varSwiftDetails = GetSwiftDetails(cboLookSWIFT.Text)
                    If Not varSwiftDetails Is Nothing Then
                        If varSwiftDetails.Count > 0 Then
                            txtLookBankName.Text = varSwiftDetails.Item("bank").ToString
                            SaveSwiftBankDetails(varSwiftDetails.Item("city").ToString, varSwiftDetails.Item("branch").ToString,
                            varSwiftDetails.Item("address").ToString, varSwiftDetails.Item("postcode").ToString,
                            varSwiftDetails.Item("country").ToString, varSwiftDetails.Item("countrycode").ToString)
                            cboLookBankCountry.Text = ClsIMS.GetSQLItem("Select SC_Country from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & cboLookSWIFT.Text & "'")
                        Else
                            MsgBox("This SWIFT code has not been found. Please enter a valid SWIFT code for retrieveing bank details", vbCritical, "Enter all details")
                        End If
                    Else
                        MsgBox("This SWIFT code is not valid. Please enter a valid SWIFT code for retrieveing bank details", vbCritical, "Enter all details")
                    End If
                Else
                    Dim varCountry As String = ClsIMS.GetSQLItem("Select SC_Country from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & cboLookSWIFT.Text & "'")
                    txtLookBankName.Text = varBankName
                    cboLookBankCountry.Text = varCountry
                End If
            Else
                MsgBox("This SWIFT code is not valid. Please enter a valid SWIFT code for retrieveing bank details", vbCritical, "Enter all details")
            End If
        End If
    End Sub

    Public Sub EditPaymentRequest()
        cboPayType.SelectedValue = ClsPR.PayRequestType
        If ClsPR.PayRequestType = 2 Or ClsPR.PayRequestType = 4 Or ClsPR.PayRequestType = 6 Then
            txtBenBusinessName.Text = ClsPR.BeneficiaryName
        End If

        ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccountAll order by Portfolio")
        cboTemFindPortfolio.SelectedValue = ClsPR.PortfolioCode
        ClsIMS.PopulateComboboxWithData(cboTemFindCCY, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccountAll where pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
        cboTemFindCCY.SelectedValue = ClsPR.CCYCode
        ClsIMS.PopulateComboboxWithData(cboTemBenPayPortfolio, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccountAll order by Portfolio")

        If Not IsNothing(ClsPR.BeneficiaryPortfolioCode) Then
            If ClsPR.BeneficiaryPortfolioCode <> 0 Then
                cboTemBenPayPortfolio.SelectedValue = ClsPR.BeneficiaryPortfolioCode
            End If
        End If
        'resetting portfolio cbo, to fix
        'ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'" & cNewTemplateName & "' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & cboTemFindPortfolio.SelectedValue & " and Pa_CCYCode = " & cboTemFindCCY.SelectedValue & " group by Pa_TemplateName order by Pa_TemplateName")

        If ClsPR.PayRequestType = 1 Or ClsPR.PayRequestType = 3 Then
            'ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'" & cNewTemplateName & "' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & cboTemFindPortfolio.SelectedValue & " and Pa_CCYCode = " & cboTemFindCCY.SelectedValue & " group by Pa_TemplateName order by Pa_TemplateName")
            'cboTemFindName.Text = ClsPR.TemplateName
            txtPRAmount.Text = ClsPR.Amount
            txtPRRate.Text = ClsPR.PayRequestRate
            txtRef.Text = ClsPR.Reference
        ElseIf ClsPR.PayRequestType = 13 Then
            cboMMPortfolio.SelectedValue = ClsPR.PortfolioCode
            cboMMPortfolio.Text = ClsPR.PortfolioName
            cboMMBuySell.SelectedValue = ClsPR.MMBuySell
            txtMMAmount.Text = ClsPR.Amount
            ClsIMS.PopulateComboboxWithData(cboMMCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccountAll where pf_code = " & cboMMPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            cboMMCCY.SelectedValue = ClsPR.CCYCode
            ClsIMS.PopulateComboboxWithData(cboMMInstrument, "Select T_Code,T_Name1 FROM Titles WHERE (dbo.Titles.T_Type = 4) AND (dbo.Titles.T_Name1 LIKE '%LIQ%') and T_Currency = " & cboMMCCY.SelectedValue & " group by T_Code,T_Name1 order by T_Name1")
            cboMMInstrument.SelectedValue = ClsPR.MMInstrumentCode
            cboMMHeldType.SelectedValue = ClsPR.MMDurationType
            dtHeldStartDate.Value = ClsPR.MMStartDate
            dtHeldEndDate.Value = ClsPR.MMEndDate
        Else
            cboBenType.SelectedValue = ClsPR.BeneficiaryTypeCode
            cboBenType.Text = ClsPR.BeneficiaryType
            cbobenrelationship.SelectedValue = ClsPR.RelationshipCode
            cbobenrelationship.Text = ClsPR.Relationship
            txtBenBusinessName.Text = ClsPR.BusinessName
            txtbenfirstname.Text = ClsPR.Firstname
            txtbenmiddlename.Text = ClsPR.Middlename
            txtbensurname.Text = ClsPR.Surname
            txtbenaddress1.Text = ClsPR.Address
            txtbencity.Text = ClsPR.City
            txtbencounty.Text = ClsPR.County
            txtbenpostcode.Text = ClsPR.Postcode
            cboBenCountry.SelectedValue = ClsPR.CountryCode
            cboBenCountry.Text = ClsPR.Country
            ClsPayAuth.PaymentFilePath = ClsPR.FilePath
            ChkKYCsanctions.Checked = ClsPR.CheckedSanctions
            ChkKYCPEP.Checked = ClsPR.CheckedPEP
            ChkKYCCDD.Checked = ClsPR.CheckedCDD
            txtKYCIssuesDisclosed.Text = ClsPR.IssuesDisclosed
            txtLookBankName.Text = ClsPR.CheckedBankName
            cboLookBankCountry.Text = ClsPR.CheckedBankCountry
            If IsNumeric(Replace(ClsPR.CheckedSwiftSortCode, "-", "")) And Len(Replace(ClsPR.CheckedSwiftSortCode, "-", "")) = 6 And Len(ClsPR.CheckedAccountNo) = 8 Then
                txtLookSortCode.Text = ClsPR.CheckedSwiftSortCode
                txtLookAccountNo.Text = ClsPR.CheckedAccountNo
            Else
                cboLookSWIFT.Text = ClsPR.CheckedSwiftSortCode
            End If
            cboBenType.SelectedValue = ClsPR.BeneficiaryTypeCode
            CboTemBenSubBankName.Text = ClsPR.BeneficiarySubBankName
            txtTemSubBankSwift.Text = ClsPR.BeneficiarySubBankSwift
            txtTemSubBankIBAN.Text = ClsPR.BeneficiarySubBankIBAN
            CboTemBenBankName.Text = ClsPR.BeneficiaryBankName
            txtTemBankSwift.Text = ClsPR.BeneficiaryBankSwift
            txtTemBankIBAN.Text = ClsPR.BeneficiaryBankIBAN
            txtPRAmount.Text = ClsPR.Amount
            txtPRRate.Text = ClsPR.PayRequestRate
            txtRef.Text = ClsPR.Reference
        End If
        txtEmail.Text = ClsPR.PayRequestEmail
        txtEmail.ReadOnly = False

        If ClsPR.ManualPay Then
            lblFinancePay.Visible = True
        Else
            lblFinancePay.Visible = False
        End If

        If ClsPR.PayRequestRate <> 0 Then
            cboFeeType.SelectedValue = 1
            cboFeeType.Visible = True
        End If

        If ClsPR.SelectedAuthoriseOption = 6 Then
            'GrpRequest.Enabled = False
            DisableGroups()
            txtEmail.ReadOnly = True
            CmdPRSend.Enabled = False
            TemplateEnabled(False)
        ElseIf ClsPR.SelectedAuthoriseOption = 7 Then
            GrpPRCriteria.Enabled = True
            CmdPRSend.Enabled = True
            ClsPayAuth.BeneficiaryTemplateName = ""
            ClsPayAuth.BeneficiaryAddressID = 0
            ClsPayAuth.BeneficiarySubBankAddressID = 0
            ClsPayAuth.BeneficiaryBankAddressID = 0
            ClsPay.BeneficiaryTemplateName = ""
            ClsPay.BeneficiaryAddressID = 0
            ClsPay.BeneficiarySubBankAddressID = 0
            ClsPay.BeneficiaryBankAddressID = 0
            ClsPay.PaymentFilePath = ClsPayAuth.PaymentFilePath
            ClsPay.TempPaymentFilePath = ClsPayAuth.TempPaymentFilePath
            ClsPay.PaymentFilePathRemote = ClsPayAuth.PaymentFilePathRemote
            ClsPayAuth.PaymentFilePath = ""
            ClsIMS.PopulateComboboxWithData(cboTemFeeCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccountAll where pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            BalanceTitlesVisible(True)
            CalcBalance()
            TransferFee()

            'If IO.Directory.Exists(ClsPay.PaymentFilePath) Then
            '    Dim varFiles As String() = IO.Directory.GetFiles(ClsPay.PaymentFilePath)
            '    For Each vFile In varFiles
            '        If Not (InStr(1, vFile, "Search", vbTextCompare) <> 0 And InStr(1, vFile, ".html", vbTextCompare) <> 0) Then
            '            IO.File.Copy(vFile, ClsPayAuth.PaymentFilePath & Dir(vFile))
            '        End If
            '    Next
            'End If
        End If
        txtPaymentFilesPath.Text = ClsPayAuth.PaymentFilePath
        PopulateSearch()
    End Sub

    Public Sub DisableGroups()
        GrpBankDetails.Enabled = False
        GrpKYC.Enabled = False
        GrpLookup.Enabled = False
        GrpSearch.Enabled = False
    End Sub

    Private Sub cboTemFindCCY_Leave(sender As Object, e As EventArgs) Handles cboTemFindCCY.Leave
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) Then
            If cboPayType.SelectedValue = 10 Or cboPayType.SelectedValue = 11 Then
                ClsIMS.PopulateComboboxWithData(cboTemBenPayPortfolio, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & IIf(cboPayType.SelectedValue = 10, "42", "47") & " and cr_code = " & cboTemFindCCY.SelectedValue & " order by Portfolio")
            Else
                ClsIMS.PopulateComboboxWithData(cboTemBenPayPortfolio, "Select 0 As pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and cr_code = " & cboTemFindCCY.SelectedValue & " order by Portfolio")
            End If
            If cboPayType.SelectedValue <> 1 Then
                cboTemFindName.Enabled = True
                cboTemFindName.Text = ""
                ClearTemplateDetails()
                If cboPayType.SelectedValue = 8 Then
                    ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 1 as ID,Pa_TemplateName from vwDolfinPaymentAddress where Pa_TemplateName = '" & cVolopaTemplateName & "' and pa_templateSelected = 2")
                    cboTemFindName.SelectedValue = 1
                    DisableGroups()
                Else
                    ClsIMS.PopulateComboboxWithData(cboTemFindName, "Select 0 As ID, '' as Pa_TemplateName union select 1 as ID,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & cboTemFindPortfolio.SelectedValue & " and Pa_CCYCode = " & cboTemFindCCY.SelectedValue & " group by Pa_TemplateName order by Pa_TemplateName")
                End If
                If cboPayType.SelectedValue = 4 Then
                    Dim varBenType As String = ClsIMS.GetSQLItem("Select isnull(cust_category,0) From vwDolfinPaymentClientPortfolio where pf_code = " & cboTemFindPortfolio.SelectedValue)
                    varBenType = IIf(Not IsNumeric(varBenType), 0, varBenType)
                    If varBenType = 0 Then
                        cboBenType.Text = "Individual"
                        cboBenType.SelectedValue = 0
                        txtbenfirstname.Text = ClsIMS.GetSQLItem("Select PF_FName2 from vwDolfinPaymentClientPortfolio where pf_code = " & cboTemFindPortfolio.SelectedValue)
                        txtbensurname.Text = ClsIMS.GetSQLItem("Select PF_FName1 from vwDolfinPaymentClientPortfolio where pf_code = " & cboTemFindPortfolio.SelectedValue)
                    Else
                        cboBenType.Text = "Business"
                        cboBenType.SelectedValue = 1
                        cbobenrelationship.SelectedValue = 3
                        txtBenBusinessName.Text = ClsIMS.GetSQLItem("Select PF_FName1 from vwDolfinPaymentClientPortfolio where pf_code = " & cboTemFindPortfolio.SelectedValue)
                    End If
                    cboBenCountry.Text = ClsIMS.GetSQLItem("select top 1 ctry_name from [dbo].[DolfinPaymentRARangeCountry] where ctry_name like '%' + (Select Cust_Residency_Country from vwDolfinPaymentClientPortfolio where pf_code = " & cboTemFindPortfolio.SelectedValue & ") + '%'")
                    cbobenrelationship.SelectedValue = 3
                    cbobenrelationship.Text = "Client's own name account"
                ElseIf cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 6 Then
                    cboBenType.Text = "Business"
                    cboBenType.SelectedValue = 1
                    cbobenrelationship.SelectedValue = 5
                End If
            End If

            BalanceTitlesVisible(True)
            CalcBalance()
            TransferFee()
        End If
        CreateTempPaymentDirectory()
    End Sub

    Public Sub BalanceTitlesVisible(ByVal varVisible As Boolean)
        lblBalancetitle.Visible = varVisible
        lblBalance.Visible = varVisible
        lblBalanceCCY.Visible = varVisible
        lblPostBalancetitle.Visible = varVisible
        lblPostBalance.Visible = varVisible
        lblPostBalanceCCY.Visible = varVisible
        lblNewBalancetitle.Visible = varVisible
        lblNewBalance.Visible = varVisible
        LblNewBalanceCCY.Visible = varVisible
        If Not varVisible Then
            lblBalance.Text = ""
            lblPostBalance.Text = ""
            lblNewBalance.Text = ""
        End If
    End Sub

    Public Sub ColourBalance(ByVal Bal As Label)
        If IsNumeric(Bal.Text) Then
            If CDbl(Bal.Text) >= 0 Then
                Bal.ForeColor = Color.FromKnownColor(KnownColor.HotTrack)
            Else
                Bal.ForeColor = Color.DarkRed
            End If
        End If
    End Sub

    Private Sub cboTemFindPortfolio_Leave(sender As Object, e As EventArgs) Handles cboTemFindPortfolio.Leave
        ClearTemplateDetails()
        cboTemFindCCY.Text = ""
        If cboPayType.SelectedValue <> 1 Then
            txtBenBusinessName.Text = ""
        End If
        If IsNumeric(cboTemFindPortfolio.SelectedValue) Then
            If cboPayType.SelectedValue = 8 Then
                ClsIMS.PopulateComboboxWithData(cboTemFindCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where cr_code = 6 and pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            Else
                ClsIMS.PopulateComboboxWithData(cboTemFindCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            End If
            ClsIMS.PopulateComboboxWithData(cboTemFeeCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
        End If
        CreateTempPaymentDirectory()
    End Sub

    Private Sub FrmPaymentRequest_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboPayType, "Select prt_Code,prt_description from vwDolfinPaymentRequestType where PRT_Active = 1 order by prt_description")
        ClsIMS.PopulateComboboxWithData(CboTemBenSubBankName, "Select '' as SC_SwiftCode,'' as SC_BankName union select SC_SwiftCode,SC_BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName order by SC_BankName", False)
        ClsIMS.PopulateComboboxWithData(CboTemBenBankName, "select '' as SC_SwiftCode,'' as SC_BankName union select SC_SwiftCode,SC_BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName order by SC_BankName", False)
        ClsIMS.PopulateComboboxWithData(cboBenType, "Select BeneficiaryTypeID, BeneficiaryType from vwDolfinPaymentBeneficiaryType order by BeneficiaryType")
        ClsIMS.PopulateComboboxWithData(cbobenrelationship, "SELECT pty_id, pty_description from DolfinPaymentRARangePayType group by pty_id, pty_description")
        ClsIMS.PopulateComboboxWithData(cboBenCountry, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ClsIMS.PopulateComboboxWithData(cboLookBankCountry, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ClsIMS.PopulateComboboxWithData(cboLookSWIFT, "Select 0,SC_SwiftCode from vwDolfinPaymentSwiftCodes where sc_countryid<>232 order by SC_SwiftCode")
        ClsIMS.PopulateComboboxWithData(cboFeeType, "Select 0,'Std Fee' union Select 1,'Rate'")
        txtFilePath.Text = cDefaultFilePath
        FolderBrowserDialog1.SelectedPath = cDefaultFilePath

        If ClsPR.ID = 0 Then
            ClsPayAuth = New ClsPayment
            'ClsPayAuth.GetTempDirectory(ClsIMS.GetFilePath("Default"))
        Else
            EditPaymentRequest()
        End If

        ExtractAssociatedIconEx1()
    End Sub

    Private Sub CmdPRSend_Click(sender As Object, e As EventArgs) Handles CmdPRSend.Click
        Dim varRef As String = ""
        ExtractAssociatedIconEx1()
        If ValidRequest() Then
            'varRef is comments for user as is without removal of special characters, spaces etc - this goes on the statement
            'txtRef.Text is comments stripped of special characters so it can be used as the reference for the swift

            varRef = txtRef.Text
            txtRef.Text = System.Text.RegularExpressions.Regex.Replace(txtRef.Text, "[^A-Za-z0-9\-/]", "")
            If cboPayType.SelectedValue <> 13 And cboPayType.SelectedValue <> 14 And cboPayType.SelectedValue <> 15 Then
                ClsPayAuth.SelectedPaymentID = -1
                If cboBenType.SelectedValue = 0 Then
                    ClsPayAuth.PaymentRequestedBeneficiaryName = txtbenfirstname.Text & " " & txtbenmiddlename.Text & " " & txtbensurname.Text
                Else
                    ClsPayAuth.PaymentRequestedBeneficiaryName = txtBenBusinessName.Text
                End If

                If cboPayType.SelectedValue = 1 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan)
                    ClsPayAuth.Comments = "Payment to " & cboTemBenPayPortfolio.Text & " for Loan Agreement No " & varRef
                ElseIf cboPayType.SelectedValue = 3 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort)
                    ClsPayAuth.Comments = "Payment from " & cboTemFindPortfolio.Text & " to " & cboTemBenPayPortfolio.Text & " for " & txtPRAmount.Text & " " & cboTemFindCCY.Text & " " & IIf(varRef <> "", varRef, "")
                ElseIf cboPayType.SelectedValue = 2 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)
                    ClsPayAuth.Comments = "Payment to " & ClsPayAuth.PaymentRequestedBeneficiaryName & " for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                ElseIf cboPayType.SelectedValue = 4 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut)
                    If cboTemFindName.Text = "" Then
                        ClsPayAuth.Comments = "Own funds transfer"
                    Else
                        ClsPayAuth.Comments = "Own funds transfer to " & IIf(cbobenrelationship.SelectedValue = 0, txtbenfirstname.Text & " " & txtbenmiddlename.Text & " " & txtbensurname.Text, txtBenBusinessName.Text) & IIf(CboTemBenBankName.Text <> "", " at " & CboTemBenBankName.Text, "")
                    End If
                ElseIf cboPayType.SelectedValue = 6 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)
                    ClsPayAuth.Comments = "Payment to " & ClsPayAuth.PaymentRequestedBeneficiaryName & " for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                ElseIf cboPayType.SelectedValue = 7 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
                    ClsPayAuth.OtherID = cPayDescInOtherDolfinFee
                    ClsPayAuth.PortfolioIsFee = True
                    ClsPayAuth.SendSwift = True
                    ClsPayAuth.Comments = "Payment to Dolfin Firm (" & varRef & ") for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                ElseIf cboPayType.SelectedValue = 8 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)
                    ClsPayAuth.Comments = "Payment to " & ClsPayAuth.PaymentRequestedBeneficiaryName & " for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                ElseIf cboPayType.SelectedValue = 9 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
                    ClsPayAuth.OtherID = cPayDescInOtherPayVolopaFee
                    ClsPayAuth.PortfolioIsFee = True
                    ClsPayAuth.Comments = "Payment to Dolfin Firm Volopa Fee (" & varRef & ") for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                ElseIf cboPayType.SelectedValue = 10 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon)
                    ClsPayAuth.Comments = "Own fund transfer from " & cboTemFindPortfolio.Text & " (Malta) to " & cboTemBenPayPortfolio.Text & " (LDN) " & varRef
                ElseIf cboPayType.SelectedValue = 11 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta)
                    ClsPayAuth.Comments = "Own fund transfer from " & cboTemFindPortfolio.Text & " (LDN) to " & cboTemBenPayPortfolio.Text & " (Malta) " & varRef
                ElseIf cboPayType.SelectedValue = 12 Then
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
                    ClsPayAuth.OtherID = cPayDescInOtherPayVolopaAnnualFee
                    ClsPayAuth.PortfolioIsFee = True
                    ClsPayAuth.Comments = "Payment to Dolfin Firm Volopa Annual Fee (" & varRef & ") for " & txtPRAmount.Text & " " & cboTemFindCCY.Text
                End If
                ClsPayAuth.OrderMessageTypeID = 78
                ClsPayAuth.PortfolioCode = cboTemFindPortfolio.SelectedValue
                ClsPayAuth.Portfolio = cboTemFindPortfolio.Text
                ClsPayAuth.TransDate = Now.Date
                ClsPayAuth.SettleDate = Now.Date
                ClsPayAuth.Amount = txtPRAmount.Text
                ClsPayAuth.CCYCode = cboTemFindCCY.SelectedValue
                ClsPayAuth.CCY = cboTemFindCCY.Text
                ClsPayAuth.OrderRef = ""

                ClsPayAuth.BeneficiaryCCYCode = cboTemFindCCY.SelectedValue
                ClsPayAuth.BeneficiaryCCY = cboTemFindCCY.Text

                ClsPayAuth.PaymentRequested = True
                ClsPayAuth.PaymentRequestedEmail = txtEmail.Text & vbNewLine & vbNewLine & "Request from: " & ClsIMS.FullUserName
                ClsPayAuth.PaymentRequestedFrom = ClsIMS.FullUserName
                ClsPayAuth.PaymentRequestedRate = IIf(Not IsNumeric(txtPRRate.Text) Or txtPRRate.Text = "", 0, txtPRRate.Text)
                ClsPayAuth.PaymentRequestedType = cboPayType.SelectedValue
                ClsPayAuth.OrderOther = txtRef.Text

                If cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8 Then
                    ClsPayAuth.PaymentRequestedKYCFolder = txtFilePath.Text
                    ClsPayAuth.BeneficiaryTemplateName = cboTemFindPortfolio.Text & " - " & cboTemFindCCY.Text & " - " & ClsPayAuth.PaymentRequestedBeneficiaryName & " (" & Now.ToString("dd-MM-yyyy hh:mm") & ")"

                    PopulateBeneficiaryDetails()

                    If cboPayType.SelectedValue = 8 Then
                        ClsPayAuth.SentApproveEmailCompliance = 3
                        ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAccountMgtLevel2)
                    Else
                        ClsPayAuth.SentApproveEmailCompliance = 1
                        ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam)
                    End If
                ElseIf cboPayType.SelectedValue = 1 Or cboPayType.SelectedValue = 3 Then
                    ClsPayAuth.SendSwift = False
                    ClsPayAuth.SendIMS = True
                    ClsPayAuth.BeneficiaryPortfolioCode = cboTemBenPayPortfolio.SelectedValue
                    ClsPayAuth.BeneficiaryPortfolio = cboTemBenPayPortfolio.Text
                    ClsPayAuth.PaymentRequestedBeneficiaryName = ""
                    ClsPayAuth.BeneficiaryName = ""
                    ClsPayAuth.SentApproveEmailCompliance = 1
                    ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam)
                ElseIf cboPayType.SelectedValue = 4 Then
                    ClsPayAuth.BeneficiaryTemplateName = cboTemFindName.Text
                    PopulateBeneficiaryDetails()
                    ClsPayAuth.SentApproveEmailCompliance = 1 'temp change to 1 from 0 so it goes to payents team
                    ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth)
                ElseIf cboPayType.SelectedValue = 10 Or cboPayType.SelectedValue = 11 Then
                    ClsPayAuth.SendSwift = True
                    ClsPayAuth.SendIMS = True
                    ClsPayAuth.BeneficiaryPortfolioCode = cboTemBenPayPortfolio.SelectedValue
                    ClsPayAuth.BeneficiaryPortfolio = cboTemBenPayPortfolio.Text
                    ClsPayAuth.PaymentRequestedBeneficiaryName = ""
                    ClsPayAuth.BeneficiaryName = ""
                    ClsPayAuth.SentApproveEmailCompliance = 1
                    ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingAuth)
                End If

                If IsNumeric(cboFeeType.SelectedValue) Then
                    If cboFeeType.SelectedValue = 0 Then
                        If cboTemFeeCCY.SelectedValue = 6 Then
                            ClsPayAuth.PortfolioFee = IIf(Not IsNumeric(txtPRAmountFee.Text), 0, txtPRAmountFee.Text)
                            ClsPayAuth.PortfolioFeeCCY = cboTemFeeCCY.Text
                        Else
                            ClsPayAuth.PortfolioFeeInt = IIf(Not IsNumeric(txtPRAmountFee.Text), 0, txtPRAmountFee.Text)
                            ClsPayAuth.PortfolioFeeCCYInt = cboTemFeeCCY.Text
                        End If
                    ElseIf cboFeeType.SelectedValue = 1 Then
                        If cboTemFeeCCY.SelectedValue = 6 Then
                            ClsPayAuth.PortfolioFee = IIf(Not IsNumeric(txtPRAmountFee.Text), 0, txtPRAmountFee.Text)
                            ClsPayAuth.PortfolioFeeCCY = cboTemFeeCCY.Text
                        Else
                            ClsPayAuth.PortfolioFeeInt = IIf(Not IsNumeric(txtPRAmountFee.Text), 0, txtPRAmountFee.Text)
                            ClsPayAuth.PortfolioFeeCCYInt = cboTemFeeCCY.Text
                        End If
                    End If
                    ClsPayAuth.PortfolioFeeAccountCode = ClsIMS.GetPortfolioFeeAccountCode(cboTemFindPortfolio.SelectedValue, cboTemFeeCCY.SelectedValue)
                    ClsPayAuth.PortfolioFeeRMSAccountCode = ClsIMS.GetPortfolioFeeRMSAccountCode(cboTemFeeCCY.SelectedValue)
                End If

                If ClsPayAuth.SendSwift And ClsIMS.IsAboveThreshold(ClsPayAuth.Amount, ClsPayAuth.CCYCode) Then
                    ClsPayAuth.AboveThreshold = True
                Else
                    ClsPayAuth.AboveThreshold = False
                End If
            End If

            If cboPayType.SelectedValue = 14 Or cboPayType.SelectedValue = 15 Then
                If CreateFOP() Then
                    ClsPay = New ClsPayment
                    ClsPayAuth = New ClsPayment
                    Me.Close()
                End If
            Else 'If cboPayType.SelectedValue = 1 Or cboPayType.SelectedValue = 3 Or cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 8 Or cboPayType.SelectedValue = 10 Or cboPayType.SelectedValue = 11 Or cboPayType.SelectedValue = 13 Then
                If cboPayType.SelectedValue = 13 Then
                    CreateMM()
                End If

                FrmEmail.ShowDialog()

                ClsPay = New ClsPayment
                ClsPayAuth = New ClsPayment
                Me.Close()
                'Else
                '    Dim TmpFilePath As String = ClsPayAuth.PaymentFilePath
                '    ClsPay = ClsPayAuth
                '    ClsIMS.UpdatePaymentTable(0, ClsPayAuth.SelectedPaymentID)

                '    ClsPay.PaymentFilePath = TmpFilePath
                '    ClsPayAuth.ConfirmDirectory()
                '    ClsPayAuth.CopyPaymentFiles(ClsPayAuth.TempPaymentFilePath, ClsPayAuth.PaymentFilePath)
                '    MsgBox("The payment has been booked", MsgBoxStyle.Information, "Payment booked")

                '    ClsPay = New ClsPayment
                '    ClsPayAuth = New ClsPayment
                '    Me.Close()
            End If


        End If
    End Sub

    Public Function CreateMM() As Boolean
        ClsPayAuth.SelectedPaymentID = -1
        ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
        If cboMMBuySell.SelectedValue = 0 Then
            ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketBuy
        Else
            ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketSell
        End If
        ClsPayAuth.Comments = "Money Markets booking for " & cboMMPortfolio.Text
        ClsPayAuth.SendSwift = False
        ClsPayAuth.SendIMS = True

        ClsPayAuth.OrderMessageTypeID = 78
        ClsPayAuth.PortfolioCode = cboMMPortfolio.SelectedValue
        ClsPayAuth.Portfolio = cboMMPortfolio.Text
        ClsPayAuth.TransDate = Now.Date
        ClsPayAuth.SettleDate = Now.Date
        ClsPayAuth.Amount = txtMMAmount.Text
        ClsPayAuth.CCYCode = cboMMCCY.SelectedValue
        ClsPayAuth.CCY = cboMMCCY.Text
        ClsPayAuth.BeneficiaryCCYCode = cboMMCCY.SelectedValue
        ClsPayAuth.BeneficiaryCCY = cboMMCCY.Text

        ClsPayAuth.PaymentRequested = True
        ClsPayAuth.PaymentRequestedEmail = txtEmail.Text & vbNewLine & vbNewLine & "Request from: " & ClsIMS.FullUserName
        ClsPayAuth.PaymentRequestedFrom = ClsIMS.FullUserName
        ClsPayAuth.PaymentRequestedRate = 0
        ClsPayAuth.PaymentRequestedType = cboPayType.SelectedValue

        ClsPayAuth.PaymentRequestedMMBuySell = cboMMBuySell.SelectedValue
        ClsPayAuth.PaymentRequestedMMInstrumentCode = cboMMInstrument.SelectedValue
        ClsPayAuth.PaymentRequestedMMInstrument = cboMMInstrument.Text
        ClsPayAuth.PaymentRequestedMMDurationType = cboMMHeldType.SelectedValue
        ClsPayAuth.PaymentRequestedMMStartDate = dtHeldStartDate.Value
        ClsPayAuth.PaymentRequestedMMEndDate = dtHeldEndDate.Value

        If ClsIMS.HasMMHoldings(cFirmMMPortfolioCode, cboMMInstrument.SelectedValue, cboMMCCY.SelectedValue, txtMMAmount.Text) Then
            ClsPayAuth.PaymentRequestedMMFirmMove = True
        Else
            ClsPayAuth.PaymentRequestedMMFirmMove = False
        End If
        ClsPayAuth.PortfolioFeeAccountCode = ClsIMS.GetPortfolioFeeAccountCode(cboMMPortfolio.SelectedValue, cboMMCCY.SelectedValue)
        ClsPayAuth.PortfolioFeeRMSAccountCode = ClsIMS.GetPortfolioFeeRMSAccountCode(cboMMCCY.SelectedValue)
        CreateMM = True
    End Function

    Public Function CreateFOP() As Boolean
        Dim varBody As String = ""
        Dim varID As Double
        Dim varValues As Boolean = False

        CreateFOP = False
        ClsRD.SendReceiveDeliverEmailBodyTitle(varBody)
        For varRow As Integer = 0 To dgvFOP.RowCount - 1
            If Not IsNothing(dgvFOP.Rows(varRow).Cells(0).Value) And Not IsNothing(dgvFOP.Rows(varRow).Cells(1).Value) And
                   Not IsNothing(dgvFOP.Rows(varRow).Cells(2).Value) And Not IsNothing(dgvFOP.Rows(varRow).Cells(3).Value) And
                   Not IsNothing(dgvFOP.Rows(varRow).Cells(4).Value) And Not IsNothing(dgvFOP.Rows(varRow).Cells(5).Value) Then
                ClsRD = New ClsReceiveDeliver
                ClsRD.PortfolioCode = cboFOPPortfolio.SelectedValue
                ClsRD.PortfolioName = cboFOPPortfolio.Text
                ClsRD.Email = txtEmail.Text
                Dim varNotes As String = ""
                ClsRD.GetRDGridIntoClass(dgvFOP, varRow)
                If ClsRD.InstrCode = 0 Then
                    varNotes = "New Instrument: This instrument for this CCY has not been found in IMS and will need to be setup"
                End If

                varID = ClsIMS.UpdatePaymentTableRD(True)
                ClsRD.SelectedTransactionID = varID
                ClsRD.ConfirmDirectoryFOP()
                ClsRD.SendReceiveDeliverEmailBody(True, varBody, dgvFOP, varRow, ClsRD.PortfolioName, varID, varNotes)
                varValues = True
            End If
        Next

        If varValues Then
            ClsIMS.ActionMovementRD(8, varID, ClsIMS.FullUserName)
            ClsEmail.SendReceiveDeliverEmail(varBody, ClsRD.Email, IIf((cboPayType.SelectedValue = 15) Or (cboPayType.SelectedValue = 14 And (ClsRD.RecType = 2 Or ClsRD.RecType = 4)), True, False), varID)
            MsgBox("The free of payment tranactions have been booked and the emails have been sent", MsgBoxStyle.Information, "FOP booked")
            CreateFOP = True
        Else
            MsgBox("No free of payments tranactions have been booked. Please check your bookings are completed then try again", MsgBoxStyle.Information, "No FOPs booked")
        End If
    End Function

    Private Sub PopulateBeneficiaryDetails()
        ClsPayAuth.OrderOther = txtRef.Text
        ClsPayAuth.BeneficiaryTypeID = cboBenType.SelectedValue
        ClsPayAuth.BeneficiaryName = txtBenBusinessName.Text
        ClsPayAuth.BeneficiaryFirstName = txtbenfirstname.Text
        ClsPayAuth.BeneficiaryMiddleName = txtbenmiddlename.Text
        ClsPayAuth.BeneficiarySurname = txtbensurname.Text
        ClsPayAuth.BeneficiaryRelationshipID = cbobenrelationship.SelectedValue
        ClsPayAuth.BeneficiaryAddress1 = txtbenaddress1.Text
        ClsPayAuth.BeneficiaryAddress2 = txtbencity.Text
        ClsPayAuth.BeneficiaryCounty = txtbencounty.Text
        ClsPayAuth.BeneficiaryZip = txtbenpostcode.Text
        ClsPayAuth.BeneficiaryCountryID = cboBenCountry.SelectedValue

        If txtLookSortCode.Text = "" Then
            'ClsPayAuth.BeneficiaryBankName = CboTemBenBankName.Text
            ClsPayAuth.BeneficiarySwift = cboLookSWIFT.Text
            ClsPayAuth.BeneficiaryIBAN = txtTemBankIBAN.Text
        Else
            ClsPayAuth.BeneficiarySwift = Strings.Left(Replace(txtLookSortCode.Text, "-", ""), 6)
            ClsPayAuth.BeneficiaryIBAN = txtLookAccountNo.Text
        End If

        ClsPayAuth.BeneficiarySubBankName = CboTemBenSubBankName.Text
        ClsPayAuth.BeneficiarySubBankSwift = txtTemSubBankSwift.Text
        ClsPayAuth.BeneficiarySubBankIBAN = txtTemSubBankIBAN.Text

        ClsPayAuth.PaymentRequestedCheckedSantions = ChkKYCsanctions.Checked
        ClsPayAuth.PaymentRequestedCheckedPEP = ChkKYCPEP.Checked
        ClsPayAuth.PaymentRequestedCheckedCDD = ChkKYCCDD.Checked

        ClsPayAuth.PaymentRequestedIssuesDisclosed = txtKYCIssuesDisclosed.Text
        ClsPayAuth.PaymentRequestedCheckedBankName = txtLookBankName.Text
        ClsPayAuth.PaymentRequestedCheckedBankCountryID = cboLookBankCountry.SelectedValue
        ClsPayAuth.PaymentRequestedCheckedBankCountry = cboLookBankCountry.Text

        ClsPayAuth.SendSwift = True
        ClsPayAuth.SendIMS = True
    End Sub

    Private Function PopulatedAllFOP() As String
        PopulatedAllFOP = ""
        For i As Integer = 0 To dgvFOP.RowCount - 1
            If IsNumeric(dgvFOP.Rows(i).Cells(0).Value) Then
                If Not IsDate(dgvFOP.Rows(i).Cells(1).Value) Then
                    PopulatedAllFOP = "Please populate transaction date: row " & i + 1
                ElseIf Not IsDate(dgvFOP.Rows(i).Cells(2).Value) Then
                    PopulatedAllFOP = "Please populate settle date: row " & i + 1
                ElseIf CDate(dgvFOP.Rows(i).Cells(2).Value) < CDate(dgvFOP.Rows(i).Cells(1).Value) Then
                    PopulatedAllFOP = "Please confirm the transaction date is equal or earlier than the settle date: row " & i + 1
                ElseIf dgvFOP.Rows(i).Cells(3).Value = "" Then
                    PopulatedAllFOP = "Please confirm ISIN is populated: row " & i + 1
                ElseIf Not IsNumeric(dgvFOP.Rows(i).Cells(4).Value) Then
                    PopulatedAllFOP = "Please enter a numerical value for the amount: row " & i + 1
                ElseIf dgvFOP.Rows(i).Cells(5).Value = "" Then
                    PopulatedAllFOP = "Please populate CCY: row " & i + 1
                End If
            End If
            If PopulatedAllFOP <> "" Then
                Exit For
            End If
        Next
    End Function

    Private Function ValidRequest() As Boolean
        Dim varResponse As Integer

        ValidRequest = True

        If cboPayType.SelectedValue = 13 Then
            If Not IsNumeric(cboMMPortfolio.SelectedValue) Then
                MsgBox("Please enter a portfolio for request", vbExclamation)
                ValidRequest = False
            End If
        ElseIf cboPayType.SelectedValue = 14 Or cboPayType.SelectedValue = 15 Then
            If Not IsNumeric(cboFOPPortfolio.SelectedValue) Then
                MsgBox("Please enter a portfolio for request", vbExclamation)
                ValidRequest = False
            ElseIf LVFiles1.Items.Count < 1 Then
                MsgBox("Please enter a free of payment SSI email or file attachment instructions", vbExclamation)
                ValidRequest = False
            Else
                Dim Msg As String = PopulatedAllFOP()
                If Msg <> "" Then
                    MsgBox(Msg, vbExclamation)
                    ValidRequest = False
                End If
            End If
        Else
            If cboPayType.Text = "" Then
                MsgBox("Please enter a payment type for request", vbExclamation)
                ValidRequest = False
            ElseIf Not IsNumeric(cboTemFindPortfolio.SelectedValue) Then
                MsgBox("Please enter a portfolio for request", vbExclamation)
                ValidRequest = False
            ElseIf Not IsNumeric(cboTemFindCCY.SelectedValue) Then
                MsgBox("Please enter a currency for request", vbExclamation)
                ValidRequest = False
            ElseIf Not IsNumeric(txtPRAmount.Text) Then
                MsgBox("Please enter a valid amount for request", vbExclamation)
                ValidRequest = False
            ElseIf cboPayType.SelectedValue = 1 Then
                If cboTemBenPayPortfolio.Text = "" Then
                    MsgBox("Please enter a valid beneficiary portfolio to receive loan", vbExclamation)
                    ValidRequest = False
                ElseIf txtPRRate.Text = "" Then
                    MsgBox("Please enter a rate in percentage for loan", vbExclamation)
                    ValidRequest = False
                End If
            End If

            If txtPRAmountFee.Visible Then
                If Not IsNumeric(cboTemFeeCCY.SelectedValue) Then
                    MsgBox("Please enter a CCY for the fee", vbExclamation)
                    ValidRequest = False
                End If
            End If


            If ValidRequest And cboPayType.SelectedValue = 2 Then
                If Not ChkKYCsanctions.Checked Then
                    varResponse = MsgBox("The sanctions check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                    If varResponse = vbNo Then
                        ValidRequest = False
                    End If
                End If
                If Not ChkKYCPEP.Checked Then
                    varResponse = MsgBox("The PEP check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                    If varResponse = vbNo Then
                        ValidRequest = False
                    End If
                End If
                If Not ChkKYCCDD.Checked Then
                    varResponse = MsgBox("The CDD check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                    If varResponse = vbNo Then
                        ValidRequest = False
                    End If
                End If
            End If

            If ValidRequest And (cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 6) Then
                'If cboTemFindCCY.SelectedValue = 6 And txtbenaddress1.Text = "" Then
                'varResponse = MsgBox("If you want this GBP payment to be send via faster payments, the address line needs to be completed." & vbnewline & "Do you want to enter the address?", vbQuestion + vbYesNo)
                'If varResponse = vbyes Then
                'ValidRequest = False
                'End If
                'ElseIf cboTemFindCCY.SelectedValue = 6 And txtbencity.Text = "" Then
                'varResponse = MsgBox("If you want this GBP payment to be send via faster payments, city needs to be completed." & vbnewline & "Do you want to enter the city?", vbQuestion + vbYesNo)
                'If varResponse = vbNo Then
                'ValidRequest = False
                'End If

                If txtLookBankName.Text = "" Then
                    MsgBox("This payment requires a bank name. Please validate the bank via swift code, sort code & account number or enter bank name manually", vbExclamation)
                    ValidRequest = False
                ElseIf cboLookBankCountry.Text = "" Then
                    MsgBox("This payment requires a bank country. Please validate the bank via swift code, sort code & account number or enter bank name manually", vbExclamation)
                    ValidRequest = False
                ElseIf cboBenType.Text = "" Then
                    MsgBox("Please enter a beneficiary type for request", vbExclamation)
                    ValidRequest = False
                ElseIf cbobenrelationship.Text = "" Then
                    MsgBox("Please enter a beneficiary relationship type for request", vbExclamation)
                    ValidRequest = False
                ElseIf cboBenCountry.Text = "" Then
                    MsgBox("Please enter a beneficiary country for request", vbExclamation)
                    ValidRequest = False
                ElseIf IsNumeric(cboBenType.SelectedValue) Then
                    If cboBenType.SelectedValue = 0 Then
                        If txtbenfirstname.Text = "" Then
                            MsgBox("Please enter a valid beneficiary first name", vbExclamation)
                            ValidRequest = False
                        ElseIf txtbensurname.Text = "" Then
                            MsgBox("Please enter a valid beneficiary surname", vbExclamation)
                            ValidRequest = False
                        End If
                    ElseIf txtBenBusinessName.Text = "" Then
                        MsgBox("Please enter a valid beneficiary business name", vbExclamation)
                        ValidRequest = False
                    End If
                End If
            ElseIf ValidRequest And (cboPayType.SelectedValue = 4) Then
                If txtLookBankName.Text = "" Then
                    MsgBox("This payment requires a bank name. Please validate the bank via swift code, sort code & account number or enter bank name manually", vbExclamation)
                    ValidRequest = False
                ElseIf cboLookBankCountry.Text = "" Then
                    MsgBox("This payment requires a bank country. Please validate the bank via swift code, sort code & account number or enter bank name manually", vbExclamation)
                    ValidRequest = False
                End If
            End If


            If ValidRequest And cboPayType.SelectedValue = 6 Then
                If Not CBool(ClsIMS.HasFunds(cboTemFindPortfolio.SelectedValue, cboTemFindCCY.SelectedValue, CDbl(txtPRAmount.Text))) Then
                    MsgBox("This client does not have the funds available for payment . Please check with ops for funding allocation" & vbNewLine & "(note: Brokers inc ABN, Barc, BONY, Lloyds and Metro)", vbExclamation)
                    ValidRequest = False
                End If
            End If


            If ValidRequest And cboPayType.SelectedValue = 8 Then
                If Not CBool(ClsIMS.HasFunds(cboTemFindPortfolio.SelectedValue, cboTemFindCCY.SelectedValue, CDbl(txtPRAmount.Text) + CDbl(txtPRAmountFee.Text))) Then
                    MsgBox("This client does not have the funds available for payment. Please check with ops for funding allocation" & vbNewLine & "(note: Brokers inc ABN, Barc, BONY, Lloyds and Metro)", vbExclamation)
                    ValidRequest = False
                ElseIf Not CBool(ClsIMS.VolopaInLimits(cboTemFindPortfolio.SelectedValue, cboTemFindCCY.SelectedValue, txtPRAmount.Text)) Then
                    MsgBox("This top-up is breaching the minimum/maximim allocation for this client. Please check the client linits for top-ups", vbExclamation)
                    ValidRequest = False
                ElseIf Not IsNumeric(txtRef.Text) Then
                    MsgBox("This client does not have a Volopa token ID associated with their account", vbExclamation)
                    ValidRequest = False
                    Dim NewPG = New FrmPasswordGenerator
                    NewPG.ShowDialog()
                    txtRef.Text = ClsIMS.GetSQLItem("Select VolopaToken from customers c inner join cuporel on c.cust_id = cupo_custid inner join DolfinPaymentPassword p on p.cust_id = c.cust_id where cupo_pfcode = " & cboTemFindPortfolio.SelectedValue)
                End If
            End If

            If ValidRequest And (cboPayType.SelectedValue = 2 Or cboPayType.SelectedValue = 4 Or cboPayType.SelectedValue = 6) Then
                If txtRef.Text = "" Then
                    MsgBox("Please enter a payment reference for external payments", vbExclamation)
                    ValidRequest = False
                End If
                If LVFiles1.Items.Count < 1 Then
                    MsgBox("Please enter a payment SSI email or file attachment", vbExclamation)
                    ValidRequest = False
                End If
                If Not ChkCallback.Checked Then
                    If Not IsNumeric(cboTemFindName.SelectedValue) Then
                        varResponse = MsgBox("Call back has not been ticked as completed." & vbNewLine & vbNewLine & "You are required to use a template for this payment or confirm you have called back the client to confirm the new payment details by ticking the call back checkbox", vbInformation)
                        ValidRequest = False
                    ElseIf cboTemFindName.SelectedValue = 0 Then
                        varResponse = MsgBox("Call back has not been ticked as completed." & vbNewLine & vbNewLine & "You are required to use a template for this payment or confirm you have called back the client to confirm the new payment details by ticking the call back checkbox", vbInformation)
                        ValidRequest = False
                    End If
                End If
            End If

            If ValidRequest And (cboPayType.SelectedValue <> 6 Or cboPayType.SelectedValue <> 8) Then
                If CDbl(lblBalance.Text) < 0 Then
                    varResponse = MsgBox("This client does not have the funds available for payment. Do you want to continue the request?", vbQuestion + vbYesNo)
                    If varResponse = vbNo Then
                        ValidRequest = False
                    End If
                End If
                If cboFeeType.Visible Then
                    If CDbl(lblFeeBalance.Text) < 0 Then
                        varResponse = MsgBox("This client does not have the funds available for the payment fee. Do you want to continue the request?", vbQuestion + vbYesNo)
                        If varResponse = vbNo Then
                            ValidRequest = False
                        End If
                    End If
                End If
            End If
        End If
    End Function

    Public Sub ExtractAssociatedIconEx1()
        Try
            LVFiles1.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If IO.Directory.Exists(ClsPayAuth.TempPaymentFilePath) Then

                LVFiles1.SmallImageList = ImageFileList
                LVFiles1.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New IO.DirectoryInfo(ClsPayAuth.TempPaymentFilePath)

                Dim item As ListViewItem
                LVFiles1.BeginUpdate()
                Dim file As IO.FileInfo
                For Each file In dir.GetFiles("*.*")
                    ' Set a default icon for the file.
                    Dim iconForFile As Icon = SystemIcons.WinLogo
                    item = New ListViewItem(file.Name, 1)

                    ' Check to see if the image collection contains an image
                    ' for this extension, using the extension as a key.
                    If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                        ' If not, add the image to the image list.
                        'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                        ImageFileList.Images.Add(file.Extension, iconForFile)
                    End If
                    item.ImageKey = file.Extension
                    If Not (InStr(1, item.Text, "Search", vbTextCompare) <> 0 And InStr(1, item.Text, ".html", vbTextCompare) <> 0) Then
                        LVFiles1.Items.Add(item)
                    End If

                Next file
                LVFiles1.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx - Payment Request")
        End Try
    End Sub


    Private Sub LVFiles1_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles LVFiles1.ItemMouseHover
        LVFiles1.ContextMenuStrip = ContextMenuStripFiles1
    End Sub

    Private Sub LVFiles_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles1_DragEnter(sender As Object, e As DragEventArgs) Handles LVFiles1.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles_DragLeave(sender As Object, e As EventArgs)
        Cursor = Cursors.Default
    End Sub

    Private Sub LVFiles1_DragLeave(sender As Object, e As EventArgs) Handles LVFiles1.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub LVFiles1_DragDrop(sender As Object, e As DragEventArgs) Handles LVFiles1.DragDrop
        If IO.Directory.Exists(ClsPayAuth.TempPaymentFilePath) Then
            Dim varFiles As String() = IO.Directory.GetFiles(ClsPayAuth.TempPaymentFilePath, cPaymentSSIName & "*.*")

            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    If Not IO.File.Exists(ClsPayAuth.TempPaymentFilePath & cPaymentSSIName & varFiles.Length & IO.Path.GetExtension(MyFiles(i))) Then
                        IO.File.Copy(MyFiles(i), ClsPayAuth.TempPaymentFilePath & cPaymentSSIName & varFiles.Length & IO.Path.GetExtension(MyFiles(i)))
                    End If
                Next
                ExtractAssociatedIconEx1()
            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Interop.Outlook.MailItem) Then
                                If MsgCount < 2 Then
                                    mi.SaveAs(ClsPayAuth.TempPaymentFilePath & cPaymentSSIName & IIf(varFiles.Length = 0, "", varFiles.Length) & ".msg")
                                    MsgCount = MsgCount + 1
                                End If
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx1()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub


    Private Sub cboTemFindName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemFindName.SelectedIndexChanged
        Dim Lst
        ClearTemplateDetails()
        If IsNumeric(cboTemFindName.SelectedValue) Then
            If cboTemFindName.Text <> "" And cboTemFindName.SelectedValue <> 0 Then
                If cboTemFindName.Text = cNewTemplateName Then
                    cboPayType.SelectedValue = 2
                    TemplateEnabled(False)
                Else
                    If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) Then
                        If cboPayType.SelectedValue = 8 Then
                            Lst = ClsIMS.GetDataToList("select Pa_BeneficiaryTypeID,Pa_BeneficiaryType,Pa_Name,Pa_Firstname,Pa_Middlename,Pa_Surname,Pa_Swift,Pa_IBAN,Pa_Address1,Pa_Address2,Pa_County,Pa_Zip,Pa_CountryID,Pa_Country,Pa_RelationshipID,Pa_Relationship from vwDolfinPaymentTemplates where Pa_TemplateName = '" & cVolopaTemplateName & "' and Pa_TemplateSelected = 2")
                        Else
                            Lst = ClsIMS.GetDataToList("Select Pa_BeneficiaryTypeID, Pa_BeneficiaryType, Pa_Name, Pa_Firstname, Pa_Middlename, Pa_Surname, Pa_Swift, Pa_IBAN, Pa_Address1, Pa_Address2, Pa_County, Pa_Zip, Pa_CountryID, Pa_Country, Pa_RelationshipID, Pa_Relationship from vwDolfinPaymentTemplates where Pa_TemplateName = '" & cboTemFindName.Text & "' and Pa_TemplateSelected = 2 and Pa_PortfolioCode = " & cboTemFindPortfolio.SelectedValue & " and Pa_CCYCode = " & cboTemFindCCY.SelectedValue)
                        End If

                        If Not Lst Is Nothing Then
                            cboBenType.SelectedValue = IIf(Lst.Item("Pa_BeneficiaryTypeID") = "", 0, Lst.Item("Pa_BeneficiaryTypeID"))
                            cboBenType.Text = Lst.Item("Pa_BeneficiaryType").ToString
                            cbobenrelationship.SelectedValue = IIf(Lst.Item("Pa_RelationshipID") = "", 5, Lst.Item("Pa_RelationshipID"))
                            cbobenrelationship.Text = Lst.Item("Pa_Relationship").ToString
                            If cboBenType.SelectedValue = 0 Then
                                txtbenfirstname.Text = Lst.Item("Pa_Firstname").ToString
                                txtbenmiddlename.Text = Lst.Item("Pa_Middlename").ToString
                                txtbensurname.Text = Lst.Item("Pa_Surname").ToString
                            Else
                                txtBenBusinessName.Text = Lst.Item("Pa_Name").ToString
                            End If

                            txtbenaddress1.Text = Lst.Item("Pa_Address1").ToString
                            txtbencity.Text = Lst.Item("Pa_Address2").ToString
                            txtbencounty.Text = Lst.Item("Pa_County").ToString
                            txtbenpostcode.Text = Lst.Item("Pa_Zip").ToString
                            cboBenCountry.SelectedValue = IIf(Lst.Item("Pa_CountryID") = "", 0, Lst.Item("Pa_CountryID"))
                            cboBenCountry.Text = Lst.Item("Pa_Country").ToString
                            txtTemBankSwift.Text = Lst.Item("Pa_Swift").ToString
                            txtTemBankIBAN.Text = Lst.Item("Pa_IBAN").ToString
                        End If

                        Lst = ClsIMS.GetDataToList("select Pa_Name,Pa_Swift,Pa_IBAN,Pa_CountryID,Pa_Country from vwDolfinPaymentTemplates where Pa_TemplateName = '" & cboTemFindName.Text & "' and Pa_TemplateSelected = 3")
                        If Not Lst Is Nothing Then
                            CboTemBenSubBankName.Text = Lst.Item("Pa_Name").ToString
                            txtTemSubBankSwift.Text = Lst.Item("Pa_Swift").ToString
                            txtTemSubBankIBAN.Text = Lst.Item("Pa_IBAN").ToString
                            txtTemSubBankCtry.Text = Lst.Item("Pa_Country").ToString
                        End If

                        Lst = ClsIMS.GetDataToList("select Pa_Name,Pa_Swift,Pa_IBAN,Pa_CountryID,Pa_Country from vwDolfinPaymentTemplates where Pa_TemplateName = '" & cboTemFindName.Text & "' and Pa_TemplateSelected = 4")
                        If Not Lst Is Nothing Then
                            CboTemBenBankName.Text = Lst.Item("Pa_Name").ToString
                            txtTemBankSwift.Text = IIf(txtTemBankSwift.Text = "", Lst.Item("Pa_Swift").ToString, txtTemBankSwift.Text)
                            txtTemBankIBAN.Text = IIf(txtTemBankIBAN.Text = "", Lst.Item("Pa_IBAN").ToString, txtTemBankIBAN.Text)
                            txtTemBankCtry.Text = Lst.Item("Pa_Country").ToString
                            txtLookBankName.Text = IIf(txtLookBankName.Text = "", Lst.Item("Pa_Name").ToString, txtLookBankName.Text)
                            cboLookBankCountry.Text = IIf(cboLookBankCountry.Text = "", Lst.Item("Pa_Country").ToString, cboLookBankCountry.Text)
                        End If

                        If IsNumeric(Replace(txtTemBankSwift.Text, "-", "")) And Len(Replace(txtTemBankSwift.Text, "-", "")) = 6 And Len(txtTemBankIBAN.Text) = 8 Then
                            txtLookSortCode.Text = txtTemBankSwift.Text
                            txtLookAccountNo.Text = txtTemBankIBAN.Text
                        Else
                            cboLookSWIFT.Text = txtTemBankSwift.Text
                        End If
                        ValidateBank()

                        TemplateEnabled(False)

                        If cboPayType.SelectedValue = 4 Or cboPayType.SelectedValue = 6 And (cboBenType.Text = "" Or cbobenrelationship.Text = "" Or cboBenCountry.Text = "" Or txtTemBankIBAN.Text = "" Or txtTemBankSwift.Text = "") Then
                            MsgBox("This payment template is not valid as it is missing the minimum payment details criteria. Either edit the template and try again or enter new 3rd Party Payment/owns funds transfer details", vbExclamation)
                        End If
                    End If
                End If
            Else
                cboLookSWIFT.Text = ""
                txtLookAccountNo.Text = ""
                txtLookAccountNo.Text = ""
                txtLookBankName.Text = ""
                cboLookBankCountry.Text = ""
                TemplateEnabled(True)
            End If
        End If
    End Sub

    Public Sub FeeVisible(ByVal varVisible As Boolean)
        cboFeeType.Visible = varVisible
        lblTemFeeCCY.Visible = varVisible
        cboTemFeeCCY.Visible = varVisible
        If varVisible = False Then
            cboFeeType.SelectedValue = 0
            txtPRRate.Visible = varVisible
        End If
        lblPRAmountFee.Visible = varVisible
        txtPRAmountFee.Visible = varVisible
        lblFeeBalancetitle.Visible = varVisible
        lblFeeBalance.Visible = varVisible
        lblFeeBalanceCCY.Visible = varVisible
    End Sub

    Public Sub MMFeeVisible(ByVal varVisible As Boolean)
        lblMMBalancetitle.Visible = varVisible
        lblMMBalance.Visible = varVisible
        lblMMBalanceCCY.Visible = varVisible
        lblMMPostBalancetitle.Visible = varVisible
        lblMMPostBalance.Visible = varVisible
        lblMMPostBalanceCCY.Visible = varVisible
        lblMMInstrumentBalancetitle.Visible = varVisible
        lblMMInstrumentBalance.Visible = varVisible
        lblMMInstrumentBalanceCCY.Visible = varVisible
        lblMMNewBalancetitle.Visible = varVisible
        lblMMNewBalance.Visible = varVisible
        LblMMNewBalanceCCY.Visible = varVisible
        If Not varVisible Then
            lblMMBalance.Text = ""
            lblMMPostBalance.Text = ""
            lblMMInstrumentBalance.Text = ""
            lblMMNewBalance.Text = ""
        End If
    End Sub

    Public Sub TemplateEnabled(ByVal varEnabled As Boolean)
        cboBenType.Enabled = varEnabled
        cbobenrelationship.Enabled = varEnabled
        txtBenBusinessName.Enabled = varEnabled
        txtbenfirstname.Enabled = varEnabled
        txtbenmiddlename.Enabled = varEnabled
        txtbensurname.Enabled = varEnabled
        txtbenaddress1.Enabled = varEnabled
        txtbencity.Enabled = varEnabled
        txtbencounty.Enabled = varEnabled
        txtbenpostcode.Enabled = varEnabled
        cboBenCountry.Enabled = varEnabled
        CboTemBenSubBankName.Enabled = varEnabled
        txtTemSubBankSwift.Enabled = varEnabled
        txtTemSubBankIBAN.Enabled = varEnabled
        txtTemSubBankCtry.Enabled = varEnabled
        'CboTemBenBankName.Enabled = varEnabled
        'txtTemBankSwift.Enabled = varEnabled
        txtTemBankIBAN.Enabled = varEnabled
        'txtTemBankCtry.Enabled = varEnabled
    End Sub

    Public Sub ClearTemplateDetails()
        cboBenType.Text = ""
        cbobenrelationship.Text = ""
        txtBenBusinessName.Text = ""
        txtbenfirstname.Text = ""
        txtbenmiddlename.Text = ""
        txtbensurname.Text = ""
        txtbenaddress1.Text = ""
        txtbencity.Text = ""
        txtbencounty.Text = ""
        txtbenpostcode.Text = ""
        cboBenCountry.Text = ""
        CboTemBenSubBankName.Text = ""
        txtTemSubBankSwift.Text = ""
        txtTemSubBankIBAN.Text = ""
        txtTemSubBankCtry.Text = ""
        CboTemBenBankName.Text = ""
        txtTemBankSwift.Text = ""
        txtTemBankIBAN.Text = ""
        txtTemBankCtry.Text = ""
    End Sub


    Private Sub txtTemBankIBAN_Leave(sender As Object, e As EventArgs) Handles txtTemBankIBAN.Leave
        txtTemBankIBAN.Text = Replace(txtTemBankIBAN.Text, " ", "")
    End Sub

    Private Sub CboTemBenBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboTemBenBankName.SelectedIndexChanged
        If CboTemBenBankName.SelectedValue.ToString <> "System.Data.DataRowView" Then
            If CboTemBenBankName.SelectedValue.ToString <> "" Then
                Dim Lst = ClsIMS.GetDataToList("Select SC_SwiftCode, SC_Country from vwDolfinPaymentSwiftCodes where SC_BankName = '" & CboTemBenBankName.Text & "'")
                If Not Lst Is Nothing Then
                    txtTemBankSwift.Text = Lst.Item("SC_SwiftCode").ToString
                    txtTemBankCtry.Text = Lst.Item("SC_Country").ToString
                End If
            Else
                txtTemBankSwift.Text = ""
                txtTemBankIBAN.Text = ""
                txtTemBankCtry.Text = ""
            End If
        End If
    End Sub

    Private Sub cboPayType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPayType.SelectedIndexChanged
        cboTemFindPortfolio.Text = ""
        cboTemFindCCY.Text = ""
        ChkKYCsanctions.Checked = False
        ChkKYCPEP.Checked = False
        ChkKYCCDD.Checked = False
        TabPayment.Enabled = True
        txtRef.Visible = True
        FeeVisible(False)
        BalanceTitlesVisible(False)
        TabRequest.SelectedTab = TabRequestPayment
        If IsNumeric(cboPayType.SelectedValue) Then
            If cboPayType.SelectedValue = 1 Then 'client loan
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
                lblTemBenPayPortfolio.Visible = True
                cboTemBenPayPortfolio.Visible = True
                cboTemFindName.Enabled = False
                lblRef.Text = "Loan No:"
                txtRef.Text = Now.Date.ToString("yyyymmdd") & " dd <dd/mm/yyyy>"
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 2 Then '3rd party payment
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) group by pf_Code,Portfolio order by Portfolio")
                ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'" & cNewTemplateName & "' as Pa_TemplateName")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = False
                lblRef.Text = "Swift Ref:"
                txtRef.Text = ""
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(True)
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 3 Then 'client to client payment
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
                lblTemBenPayPortfolio.Visible = True
                cboTemBenPayPortfolio.Visible = True
                cboTemFindName.Enabled = False
                txtRef.Visible = True
                txtRef.Text = ""
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 4 Then 'own funds transfer
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = True
                lblRef.Text = "Swift Ref:"
                txtRef.Text = "OwnFundsTransfer"
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(False)
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 6 Then 'payment services
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_shortcut1 in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'" & cNewTemplateName & "' as Pa_TemplateName")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = False
                lblRef.Text = "Swift Ref:"
                txtRef.Text = ""
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(True)
                ChkKYCsanctions.Checked = True
                ChkKYCPEP.Checked = True
                ChkKYCCDD.Checked = True
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 7 Then 'dolfin pay
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class in (1,25,37,39) order by Portfolio")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = True
                lblRef.Text = "Swift Ref:"
                txtRef.Text = ""
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = False
            ElseIf cboPayType.SelectedValue = 8 Then 'dolfin volopa card top-up
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class In (1,25,37,39) and cr_code = 6 order by Portfolio")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = True
                lblRef.Text = "Volopa ID:"
                txtRef.Enabled = False
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(True)
                ChkKYCsanctions.Checked = True
                ChkKYCPEP.Checked = True
                ChkKYCCDD.Checked = True
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 9 Then '= Volopa Top Up Fee
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class In (1,25,37,39) and cr_code = 6 order by Portfolio")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = True
                lblRef.Text = "Swift Ref:"
                txtRef.Enabled = False
                txtRef.Text = "Volopa top up fee"
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = False
            ElseIf cboPayType.SelectedValue = 10 Then 'malta to Ldn
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = 47 and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                lblTemBenPayPortfolio.Visible = True
                cboTemBenPayPortfolio.Visible = True
                cboTemFindName.Enabled = False
                lblRef.Text = "Swift Ref:"
                txtRef.Enabled = True
                txtRef.Visible = True
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 11 Then 'Ldn to Malta
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = 42 and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                lblTemBenPayPortfolio.Visible = True
                cboTemBenPayPortfolio.Visible = True
                cboTemFindName.Enabled = False
                lblRef.Text = "Swift Ref:"
                txtRef.Enabled = True
                txtRef.Visible = True
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = True
            ElseIf cboPayType.SelectedValue = 12 Then 'volopa annual pay
                ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_class In (1,25,37,39) and cr_code = 6 order by Portfolio")
                cboTemFindName.Text = ""
                lblTemBenPayPortfolio.Visible = False
                cboTemBenPayPortfolio.Visible = False
                cboTemFindName.Enabled = True
                lblRef.Text = "Swift Ref:"
                txtRef.Enabled = False
                txtRef.Text = "Volopa annual fee"
                cboTemBenPayPortfolio.SelectedValue = 0
                cboTemBenPayPortfolio.Text = ""
                txtBenBusinessName.Text = ""
                TemplateEnabled(False)
                TabPayment.Enabled = False
                AllowFee = False
            ElseIf cboPayType.SelectedValue = 13 Then 'Money Markets request
                ClsIMS.PopulateComboboxWithData(cboMMPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                ClsIMS.PopulateComboboxWithData(cboMMBuySell, "Select 0, 'Buy' union Select 1, 'Sell'")
                ClsIMS.PopulateComboboxWithData(cboMMHeldType, "Select 0, 'Open' union Select 1, 'Range'")
                TabRequest.SelectedTab = TabRequestMM
                TemplateEnabled(False)
                TabPayment.Enabled = False
            ElseIf cboPayType.SelectedValue = 14 Or cboPayType.SelectedValue = 15 Then 'FOP request - 15 = 3rd party
                ClsIMS.PopulateComboboxWithData(cboFOPPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and pf_active = 1 and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                TabRequest.SelectedTab = TabRequestFOP
                TemplateEnabled(False)
                TabPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub cmdValidateBank_Click(sender As Object, e As EventArgs) Handles cmdValidateBank.Click
        ValidateBank()
    End Sub

    Public Sub ValidateBank()
        txtLookBankName.Text = ""
        cboLookBankCountry.Text = ""
        If cboLookSWIFT.Text <> "" Then
            CheckSwiftCode()
            If txtLookBankName.Text <> "" And cboLookBankCountry.Text <> "" Then
                PopulateBankDetails()
            End If
        ElseIf txtLookAccountNo.Text <> "" And txtLookAccountNo.Text <> "" Then
            Dim varBankName As String = ""
            If CheckBankNameDetails(Replace(txtLookSortCode.Text, "-", ""), txtLookAccountNo.Text, varBankName) Then
                txtLookBankName.Text = varBankName
                cboLookBankCountry.Text = "UNITED KINGDOM"
            Else
                MsgBox(varBankName, vbCritical)
            End If
        ElseIf (txtLookSortCode.Text = "" And txtLookAccountNo.Text <> "") Or (txtLookSortCode.Text <> "" And txtLookAccountNo.Text = "") Then
            MsgBox("Please enter both SortCode and Account number for GBP Bank lookup", vbInformation)
        End If
    End Sub

    Private Sub PopulateBankDetails()
        Dim Lst = ClsIMS.GetDataToList("Select SC_SwiftCode, SC_BankName, SC_Country from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & cboLookSWIFT.Text & "'")
        If Not Lst Is Nothing Then
            CboTemBenBankName.Text = Lst.Item("SC_BankName").ToString
            txtTemBankSwift.Text = Lst.Item("SC_SwiftCode").ToString
            txtTemBankCtry.Text = Lst.Item("SC_Country").ToString
        End If
    End Sub


    Private Sub cboBenType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBenType.SelectedIndexChanged
        If IsNumeric(cboBenType.SelectedValue) Then
            If cboBenType.SelectedValue = 0 Then
                txtBenBusinessName.Enabled = False
                txtBenBusinessName.Text = ""
                txtbenfirstname.Enabled = True
                txtbenmiddlename.Enabled = True
                txtbensurname.Enabled = True
            Else
                txtBenBusinessName.Enabled = True
                txtbenfirstname.Enabled = False
                txtbenfirstname.Text = ""
                txtbenmiddlename.Enabled = False
                txtbenmiddlename.Text = ""
                txtbensurname.Enabled = False
                txtbensurname.Text = ""
                cbobenrelationship.SelectedValue = 5
            End If
        End If
    End Sub

    Private Sub CboTemBenSubBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboTemBenSubBankName.SelectedIndexChanged
        If CboTemBenSubBankName.SelectedValue.ToString <> "System.Data.DataRowView" Then
            If CboTemBenSubBankName.SelectedValue.ToString <> "" Then
                Dim Lst = ClsIMS.GetDataToList("Select SC_SwiftCode, SC_Country from vwDolfinPaymentSwiftCodes where SC_BankName = '" & CboTemBenSubBankName.Text & "'")
                If Not Lst Is Nothing Then
                    txtTemSubBankSwift.Text = Lst.Item("SC_SwiftCode").ToString
                    txtTemSubBankCtry.Text = Lst.Item("SC_Country").ToString
                End If
            Else
                txtTemSubBankSwift.Text = ""
                txtTemSubBankIBAN.Text = ""
                txtTemSubBankCtry.Text = ""
            End If
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        If LVFiles1.SelectedItems.Count > 0 Then
            ShellExecute(ClsPayAuth.TempPaymentFilePath & LVFiles1.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
        If LVFiles1.SelectedItems.Count > 0 Then
            IO.File.Delete(ClsPayAuth.TempPaymentFilePath & LVFiles1.SelectedItems(0).Text)
        End If
        ExtractAssociatedIconEx1()
    End Sub

    Private Sub ToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem3.Click
        ExtractAssociatedIconEx1()
    End Sub

    Private Sub LVFiles1_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles LVFiles1.MouseDoubleClick
        If LVFiles1.SelectedItems.Count > 0 Then
            ShellExecute(ClsPayAuth.TempPaymentFilePath & LVFiles1.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub cboTemFeeCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemFeeCCY.SelectedIndexChanged
        CalcFeeBalance()
    End Sub

    Private Sub CalcBalance()
        lblBalance.Text = GetBalance(IIf(cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8, False, True), cboTemFindPortfolio.SelectedValue, cboTemFindCCY.SelectedValue).ToString("#,##0.00")
        ColourBalance(lblBalance)
        lblBalanceCCY.Text = cboTemFindCCY.Text
    End Sub

    Private Sub CalcBalanceMM()
        If IsNumeric(cboMMPortfolio.SelectedValue) And IsNumeric(cboMMCCY.SelectedValue) Then
            lblMMBalance.Text = GetBalance(True, cboMMPortfolio.SelectedValue, cboMMCCY.SelectedValue).ToString("#,##0.00")
            lblMMPostBalance.Text = (lblMMBalance.Text - (CDbl(txtMMAmount.Text))).ToString("#,##0.00")
            lblMMNewBalance.Text = (lblMMBalance.Text - (CDbl(txtMMAmount.Text))).ToString("#,##0.00")
            ColourBalance(lblMMBalance)
            ColourBalance(lblMMPostBalance)
            ColourBalance(lblMMNewBalance)
            lblMMBalanceCCY.Text = cboMMCCY.Text
            lblMMPostBalanceCCY.Text = cboMMCCY.Text
            LblMMNewBalanceCCY.Text = cboMMCCY.Text

            If IsNumeric(cboMMInstrument.SelectedValue) Then
                lblMMInstrumentBalance.Text = GetMMInstrumentBalance(cboMMPortfolio.SelectedValue, cboMMCCY.SelectedValue, cboMMInstrument.SelectedValue).ToString("#,##0.00")
                ColourBalance(lblMMInstrumentBalance)
                lblMMInstrumentBalanceCCY.Text = cboMMCCY.Text
            End If
        End If

    End Sub

    Private Sub CalcFeeBalance()
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFeeCCY.SelectedValue) And IsNumeric(txtPRAmountFee.Text) And IsNumeric(cboFeeType.SelectedValue) Then
            If cboFeeType.SelectedValue = 0 Then
                If IsNumeric(txtPRAmount.Text) Then
                    If ClsIMS.UserLocationCode = 42 Then
                        If cboTemFindCCY.SelectedValue = 6 Then
                            If ClsPay.PortfolioFee <> 0 Then
                                txtPRAmountFee.Text = ClsPay.PortfolioFee * ClsIMS.GetCCYRate(Now.Date, "GBP", cboTemFeeCCY.Text)
                            End If
                        Else
                            If ClsPay.PortfolioFeeInt <> 0 Then
                                txtPRAmountFee.Text = ClsPay.PortfolioFeeInt * ClsIMS.GetCCYRate(Now.Date, "GBP", cboTemFeeCCY.Text)
                            End If
                        End If
                        lblFeeBalanceCCY.Text = cboTemFeeCCY.Text
                    Else
                        If cboTemFindCCY.SelectedValue = 17 Then
                            If ClsPay.PortfolioFee <> 0 Then
                                txtPRAmountFee.Text = ClsPay.PortfolioFee * ClsIMS.GetCCYRate(Now.Date, ClsPay.PortfolioFeeCCY, cboTemFeeCCY.Text)
                                lblFeeBalanceCCY.Text = ClsPay.PortfolioFeeCCY
                            End If
                        Else
                            If ClsPay.PortfolioFeeInt <> 0 Then
                                txtPRAmountFee.Text = ClsPay.PortfolioFeeInt * ClsIMS.GetCCYRate(Now.Date, ClsPay.PortfolioFeeCCYInt, cboTemFeeCCY.Text)
                                lblFeeBalanceCCY.Text = ClsPay.PortfolioFeeCCYInt
                            End If
                        End If
                    End If

                    'txtPRAmountFee.Text = txtPRAmountFee.Text * ClsIMS.GetCCYRate(Now.Date, "GBP", cboTemFeeCCY.Text)
                End If
                If cboTemFeeCCY.Text <> cboTemFindCCY.Text Then
                    lblFeeBalance.Text = (CDbl(GetBalance(IIf(cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8, False, True), cboTemFindPortfolio.SelectedValue, cboTemFeeCCY.SelectedValue)) - CDbl(txtPRAmountFee.Text)).ToString("#,##0.00")
                Else
                    lblFeeBalance.Text = (CDbl(GetBalance(IIf(cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8, False, True), cboTemFindPortfolio.SelectedValue, cboTemFeeCCY.SelectedValue)) - CDbl(txtPRAmountFee.Text) - CDbl(txtPRAmount.Text)).ToString("#,##0.00")
                End If
            ElseIf IsNumeric(txtPRAmount.Text) And IsNumeric(txtPRRate.Text) Then
                If cboTemFeeCCY.Text <> cboTemFindCCY.Text Then
                    txtPRAmountFee.Text = (CDbl(txtPRAmount.Text) * CDbl(txtPRRate.Text) * 0.01 * ClsIMS.GetCCYRate(Now.Date, cboTemFindCCY.Text, cboTemFeeCCY.Text)).ToString("#,##0.00")
                    lblFeeBalance.Text = (CDbl(GetBalance(IIf(cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8, False, True), cboTemFindPortfolio.SelectedValue, cboTemFeeCCY.SelectedValue)) - CDbl(txtPRAmountFee.Text)).ToString("#,##0.00")
                Else
                    txtPRAmountFee.Text = (CDbl(txtPRAmount.Text) * CDbl(txtPRRate.Text) * 0.01 * ClsIMS.GetCCYRate(Now.Date, cboTemFindCCY.Text, cboTemFeeCCY.Text)).ToString("#,##0.00")
                    lblFeeBalance.Text = (CDbl(GetBalance(IIf(cboPayType.SelectedValue = 6 Or cboPayType.SelectedValue = 8, False, True), cboTemFindPortfolio.SelectedValue, cboTemFeeCCY.SelectedValue)) - CDbl(txtPRAmountFee.Text) - CDbl(txtPRAmount.Text)).ToString("#,##0.00")
                End If
            End If
        End If

        ColourBalance(lblFeeBalance)
        'lblFeeBalanceCCY.Text = cboTemFeeCCY.Text
        CalcEndBalance()
    End Sub

    Private Sub CalcPostBalance()
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) And IsNumeric(lblBalance.Text) And IsNumeric(txtPRAmount.Text) Then
            lblPostBalance.Text = (lblBalance.Text - (CDbl(txtPRAmount.Text))).ToString("#,##0.00")
            ColourBalance(lblPostBalance)
            lblPostBalanceCCY.Text = cboTemFindCCY.Text
            CalcEndBalance()
        End If
    End Sub

    Private Sub CalcEndBalance()
        Dim varFoundOrderAccountCCY As String = "", varFoundOrderAccount As String = "", varFoundOrderAccountBalance As Double = 0, varFoundOrderAccountFee As Double = 0, varFoundOrderAccountConvertedFee As Double = 0, varExchangeRate As Double = 0

        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) And IsNumeric(lblBalance.Text) And IsNumeric(txtPRAmount.Text) Then
            ClsPay.PortfolioCode = cboTemFindPortfolio.SelectedValue
            ClsPay.CCYCode = cboTemFeeCCY.SelectedValue
            varFoundOrderAccount = 0
            ClsPay.Amount = txtPRAmount.Text
            ClsIMS.GetTransferFeeAccount(varFoundOrderAccountCCY, CStr(ClsPay.OrderAccountCode), varFoundOrderAccountBalance, varFoundOrderAccountFee, varFoundOrderAccountConvertedFee, varExchangeRate)


            If cboTemFindCCY.SelectedValue = cboTemFeeCCY.SelectedValue Then
                lblNewBalance.Text = (lblBalance.Text - (CDbl(txtPRAmount.Text) + CDbl(IIf(txtPRAmountFee.Text = "", 0, txtPRAmountFee.Text)))).ToString("#,##0.00")
            Else
                lblNewBalance.Text = (lblBalance.Text - (CDbl(txtPRAmount.Text))).ToString("#,##0.00")
            End If
            ColourBalance(lblNewBalance)
            LblNewBalanceCCY.Text = cboTemFindCCY.Text
        End If
    End Sub

    Private Sub txtPRAmount_Leave(sender As Object, e As EventArgs) Handles txtPRAmount.Leave
        If IsNumeric(txtPRAmount.Text) Then
            txtPRAmount.Text = CDbl(txtPRAmount.Text).ToString("#,##0.00")
            CalcPostBalance()
        End If
        CreateTempPaymentDirectory()
    End Sub

    Private Sub txtPRAmountFee_Leave(sender As Object, e As EventArgs) Handles txtPRAmountFee.Leave
        If IsNumeric(txtPRAmountFee.Text) Then
            txtPRAmountFee.Text = CDbl(txtPRAmountFee.Text).ToString("#,##0.00")
            CalcFeeBalance()
        End If
    End Sub

    Private Sub cboFeeType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFeeType.SelectedIndexChanged
        If IsNumeric(cboFeeType.SelectedValue) Then
            If cboFeeType.SelectedValue = 0 Then
                txtPRRate.Visible = False
            Else
                txtPRRate.Visible = True
            End If
        End If
    End Sub

    Private Sub txtPRRate_Leave(sender As Object, e As EventArgs) Handles txtPRRate.Leave
        CalcFeeBalance()
    End Sub

    Private Sub txtRef_Leave(sender As Object, e As EventArgs) Handles txtRef.Leave
        If Len(txtRef.Text) > 150 Then
            MsgBox("Please restrict your reference to 150 characters and remove all spaces and special characters e.g. /,.'")
        End If
    End Sub

    Private Sub PopulateSearch()
        If IsNumeric(cboBenType.SelectedValue) Then
            If cboBenType.SelectedValue = 0 Then
                txtPRGoogleSearch.Text = cDefaultStartSearch & txtbenfirstname.Text & """"""" AROUND(2) """"""" & txtbensurname.Text & cDefaultSearchA
                txtPRGoogleSearch2.Text = cDefaultStartSearch & txtbenfirstname.Text & """"""" AROUND(2) """"""" & txtbensurname.Text & cDefaultSearchB
            Else
                txtPRGoogleSearch.Text = cDefaultStartSearch & Replace(Trim(Replace(Replace(Replace(Replace(Replace(Replace(Replace(UCase(txtBenBusinessName.Text), "&", ""), """", ""), "LIMITED", ""), "LTD", ""), "LLP", ""), "LLC", ""), "PLC", "")), " ", """"""" AROUND(2) """"""") & cDefaultSearchA
                txtPRGoogleSearch2.Text = cDefaultStartSearch & Replace(Trim(Replace(Replace(Replace(Replace(Replace(Replace(Replace(UCase(txtBenBusinessName.Text), "&", ""), """", ""), "LIMITED", ""), "LTD", ""), "LLP", ""), "LLC", ""), "PLC", "")), " ", """"""" AROUND(2) """"""") & cDefaultSearchB
            End If
        End If
    End Sub

    Private Sub cmdPRSearch_Click(sender As Object, e As EventArgs) Handles cmdPRSearch.Click
        RunSearch(1)
    End Sub

    Private Sub RunSearch(ByVal Runtype As Integer)
        Dim webAddress As String = ""
        If Runtype = 1 Then
            webAddress = txtPRGoogleSearch.Text
            Process.Start(webAddress)
        Else
            webAddress = txtPRGoogleSearch2.Text
            Process.Start(webAddress)
        End If
    End Sub

    Private Sub txtBenBusinessName_Leave(sender As Object, e As EventArgs) Handles txtBenBusinessName.Leave
        PopulateSearch()
    End Sub

    Private Sub txtbensurname_Leave(sender As Object, e As EventArgs) Handles txtbensurname.Leave
        PopulateSearch()
    End Sub

    Private Sub cmdPRSearch2_Click(sender As Object, e As EventArgs) Handles cmdPRSearch2.Click
        RunSearch(2)
    End Sub

    Private Sub cmdBrowse_Click(sender As Object, e As EventArgs) Handles cmdBrowse.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            txtFilePath.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub cboMMPortfolio_Leave(sender As Object, e As EventArgs) Handles cboMMPortfolio.Leave
        If IsNumeric(cboMMPortfolio.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboMMCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboMMPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
        End If
    End Sub

    Private Sub cboMMCCY_Leave(sender As Object, e As EventArgs) Handles cboMMCCY.Leave
        If IsNumeric(cboMMCCY.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboMMInstrument, "Select T_Code,T_Name1 FROM Titles WHERE (dbo.Titles.T_Type = 4) AND (dbo.Titles.T_Name1 LIKE '%LIQ%') and T_Currency = " & cboMMCCY.SelectedValue & " group by T_Code,T_Name1 order by T_Name1")
            MMFeeVisible(True)
            CalcBalanceMM()
        End If
    End Sub

    Private Sub cboMMHeldType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMMHeldType.SelectedIndexChanged
        If IsNumeric(cboMMHeldType.SelectedValue) Then
            If cboMMHeldType.SelectedValue = 0 Then
                lblHeldEndDate.Visible = False
                dtHeldEndDate.Visible = False
            Else
                lblHeldEndDate.Visible = True
                dtHeldEndDate.Visible = True
            End If
        End If
    End Sub


    Private Sub txtMMAmount_Leave(sender As Object, e As EventArgs) Handles txtMMAmount.Leave
        If IsNumeric(txtMMAmount.Text) Then
            txtMMAmount.Text = CDbl(txtMMAmount.Text).ToString("#,##0.00")
            CalcBalanceMM()
        End If
    End Sub

    Private Sub cboMMInstrument_Leave(sender As Object, e As EventArgs) Handles cboMMInstrument.Leave
        CalcBalanceMM()
    End Sub

    Public Sub PopulateFOPDGV()
        Dim TransConn As New SqlClient.SqlConnection
        Dim dgvdata As New SqlClient.SqlDataAdapter
        Dim ds As New DataSet
        Dim cboColumn As New DataGridViewComboBoxColumn
        Dim txtTBColumn = New DataGridViewTextBoxColumn
        Dim varSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor

            dgvFOP.DataSource = Nothing
            TransConn = ClsIMS.GetNewOpenConnection
            ds = New DataSet
            dgvFOP.AutoGenerateColumns = True

            For i As Integer = 0 To dgvFOP.ColumnCount - 1
                dgvFOP.Columns.RemoveAt(dgvFOP.ColumnCount - 1)
            Next

            If cboPayType.SelectedValue = 14 Then
                varSQL = "Select 1 as DRCode,'" & ReceiveFree & "' as DR union Select 2 as DRCode,'" & DeliverFree & "' as DR"
            ElseIf cboPayType.SelectedValue = 15 Then
                varSQL = "Select 3 as DRCode,'" & ReceiveFree & "' as DR union Select 4 as DRCode,'" & DeliverFree & "' as DR"
            End If

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TransConn)
            dgvdata.Fill(ds, "DR")
            cboColumn = New DataGridViewComboBoxColumn
            cboColumn.HeaderText = "Type"
            cboColumn.DataSource = ds.Tables("DR")
            cboColumn.ValueMember = ds.Tables("DR").Columns(0).ColumnName
            cboColumn.DisplayMember = ds.Tables("DR").Columns(1).ColumnName
            cboColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboColumn.FlatStyle = FlatStyle.Flat
            dgvFOP.Columns.Insert(0, cboColumn)

            txtTBColumn = New DataGridViewTextBoxColumn
            txtTBColumn.HeaderText = "Trans Date"
            dgvFOP.Columns.Insert(1, txtTBColumn)

            txtTBColumn = New DataGridViewTextBoxColumn
            txtTBColumn.HeaderText = "Settle Date"
            dgvFOP.Columns.Insert(2, txtTBColumn)

            txtTBColumn = New DataGridViewTextBoxColumn
            txtTBColumn.HeaderText = "ISIN"
            dgvFOP.Columns.Insert(3, txtTBColumn)

            txtTBColumn = New DataGridViewTextBoxColumn
            txtTBColumn.HeaderText = "Amount"
            dgvFOP.Columns.Insert(4, txtTBColumn)

            varSQL = "Select CR_Code as CCYCode,CR_Name1 as CCY from vwDolfinpaymentSelectaccount where pf_code = " & cboFOPPortfolio.SelectedValue & " group by CR_Code,CR_Name1 Order by CR_Name1"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TransConn)
            dgvdata.Fill(ds, "CCY")
            cboColumn = New DataGridViewComboBoxColumn
            cboColumn.HeaderText = "CCY"
            cboColumn.DataSource = ds.Tables("CCY")
            'cboColumn.ValueMember = ds.Tables("CCY").Columns(0).ColumnName
            cboColumn.DisplayMember = ds.Tables("CCY").Columns(1).ColumnName
            cboColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboColumn.FlatStyle = FlatStyle.Flat
            dgvFOP.Columns.Insert(5, cboColumn)

            dgvFOP.Columns(0).Width = 120
            dgvFOP.Columns(1).Width = 80
            dgvFOP.Columns(2).Width = 80
            dgvFOP.Columns(3).Width = 110
            dgvFOP.Columns(4).Width = 110
            dgvFOP.Columns(5).Width = 60
            dgvFOP.Rows.Add()
            dgvFOP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvFOP.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvFOP.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing transfer fees grid")
        End Try
    End Sub

    Private Sub dgvFOP_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFOP.CellEnter
        If e.ColumnIndex = 1 Or e.ColumnIndex = 2 Then
            dgvFOP.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = Now.Date.ToString("dd/MM/yyyy")
        End If
    End Sub

    Private Sub dgvFOP_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvFOP.CellValidating
        If (e.ColumnIndex = 1 Or e.ColumnIndex = 2) And Not IsDate(e.FormattedValue) And Len(e.FormattedValue) > 0 Then
            MsgBox("Please enter a valid date (dd/mm/yyyy)", vbCritical, "Valid date required")
            e.Cancel = True
        ElseIf e.ColumnIndex = 4 And Not IsNumeric(e.FormattedValue) And Len(e.FormattedValue) > 0 Then
            MsgBox("Please enter a numerical amount", vbCritical, "Valid amount required")
            e.Cancel = True
        End If
    End Sub

    Private Sub cboFOPPortfolio_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboFOPPortfolio.SelectedValueChanged
        If IsNumeric(cboFOPPortfolio.SelectedValue) Then
            If cboFOPPortfolio.SelectedValue <> 0 Then
                PopulateFOPDGV()
            End If
        End If
    End Sub

    Private Sub dgvFOP_CellLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFOP.CellLeave
        If dgvFOP.Rows(e.RowIndex).Cells.Count > 3 Then
            If e.ColumnIndex = 3 Then
                If Not IsNothing(dgvFOP.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue) Then
                    If dgvFOP.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue <> "" Then
                        If dgvFOP.Rows(e.RowIndex).Cells(0).Value = 2 And IsNumeric(cboFOPPortfolio.SelectedValue) Then
                            Dim Lst As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select Quantity,CCY,cast(CCYCode as int) as CCYCode from DolfinPaymentGetPosition(" & cboFOPPortfolio.SelectedValue & ",'" & dgvFOP.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue & "')")
                            If Not Lst Is Nothing Then
                                dgvFOP.Rows(e.RowIndex).Cells(4).Value = CDbl(Lst("Quantity")).ToString("0.00")
                                dgvFOP.Rows(e.RowIndex).Cells(5).Value = Lst("CCY").ToString
                                'Dim Cmb As New DataGridViewComboBoxCell()
                                'Cmb = CType(Me.dgvFOP(5, e.RowIndex), DataGridViewComboBoxCell)
                                'Cmb.Value = Lst("CCY")
                            Else
                                MsgBox("This instrument has not been found for delivery. Please enter a valid ISIN with a position in IMS", vbCritical, "Instrument not found")
                            End If
                        End If

                    End If
                End If
            End If

            If Not IsNothing(dgvFOP.Rows(e.RowIndex).Cells(3).EditedFormattedValue) Then
                If dgvFOP.Rows(e.RowIndex).Cells(3).EditedFormattedValue <> "" Then
                    If ClsIMS.IsISINRestricted(dgvFOP.Rows(e.RowIndex).Cells(3).EditedFormattedValue) > 0 Then
                        MsgBox("You need to email the GCC for approval to perform a trade or FOP on this security", vbCritical, "Instrument is restricted")
                        For varCol As Integer = 0 To dgvFOP.ColumnCount - 1
                            dgvFOP.Rows(e.RowIndex).Cells(varCol).Value = Nothing
                        Next
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub CreateTempPaymentDirectory()
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) And IsNumeric(txtPRAmount.Text) Then
            If CDbl(txtPRAmount.Text) > 0 Then
                ClsPayAuth.TempPaymentFilePath = ClsIMS.GetFilePath("TempPaymentFiles") & cboTemFindPortfolio.Text & "\"
                txtPaymentFilesPath.Text = ClsPayAuth.TempPaymentFilePath
                If Not IO.Directory.Exists(ClsPayAuth.TempPaymentFilePath) Then
                    IO.Directory.CreateDirectory(ClsPayAuth.TempPaymentFilePath)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "Created directory at " & ClsPayAuth.TempPaymentFilePath)
                End If
                MsgBox("The directory for attaching payment details has been created here: " & ClsPayAuth.TempPaymentFilePath & vbNewLine & vbNewLine & "Once files are in the directory, you can attach them to the payment using this form", vbInformation)
            End If
        End If
        ExtractAssociatedIconEx1()
    End Sub

    Private Sub GetPaymentFiles()
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) And IsNumeric(txtPRAmount.Text) Then
            ExtractAssociatedIconEx1()
        Else
            MsgBox("Please complete form before attempting to attach files to the payment")
        End If
    End Sub

    Private Sub CmdFilePayment_Click(sender As Object, e As EventArgs) Handles CmdFilePayment.Click
        GetPaymentFiles()
    End Sub

    Private Sub CmdAddFiles_Click(sender As Object, e As EventArgs)
        Dim varFullFilePath As String = ""
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then

            LVFiles1.Items.Add(OpenFileDialog1.FileName)
            ExtractAssociatedIconEx1()
        End If
    End Sub

    Private Sub CmdBrowseFiles_Click(sender As Object, e As EventArgs) Handles CmdBrowseFiles.Click
        ExtractAssociatedIconEx1()
        ShellExecute(txtPaymentFilesPath.Text)
        ExtractAssociatedIconEx1()
    End Sub
End Class
