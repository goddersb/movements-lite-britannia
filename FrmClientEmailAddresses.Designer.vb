﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmClientEmailAddresses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientEmailAddresses))
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.dgvRptsCsvConfig = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CmdExportRptConfigToExcel = New System.Windows.Forms.Button()
        Me.cmdRefreshCsvConfigView = New System.Windows.Forms.Button()
        Me.cmdCsvFilter = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboFullName = New System.Windows.Forms.ComboBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dgvClientEmailAddresses = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.CmdExportEmailsToExcel = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboShortcut = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshEmailView = New System.Windows.Forms.Button()
        Me.CmdEmailFilter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboPortfolioName = New System.Windows.Forms.ComboBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvPDFs = New System.Windows.Forms.DataGridView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.CmdExportRptSchedule = New System.Windows.Forms.Button()
        Me.cmdRefreshPdfView = New System.Windows.Forms.Button()
        Me.cmdPdfFilter = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboPDFPortfolioName = New System.Windows.Forms.ComboBox()
        Me.ImageListTickCross = New System.Windows.Forms.ImageList(Me.components)
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvRptsCsvConfig, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvClientEmailAddresses, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewGroupBox.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.dgvPDFs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.lblpaymentTitle)
        Me.TabPage2.Controls.Add(Me.dgvRptsCsvConfig)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(2)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(2)
        Me.TabPage2.Size = New System.Drawing.Size(1718, 836)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Client CSV Reports"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.Maroon
        Me.Label13.Location = New System.Drawing.Point(654, 70)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(394, 13)
        Me.Label13.TabIndex = 118
        Me.Label13.Text = "These email adresses are used for automated client csv exports (Reports Exporter)" &
    ""
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(8, 11)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(188, 24)
        Me.lblpaymentTitle.TabIndex = 115
        Me.lblpaymentTitle.Text = "Client CSV Reports"
        '
        'dgvRptsCsvConfig
        '
        Me.dgvRptsCsvConfig.AllowUserToOrderColumns = True
        Me.dgvRptsCsvConfig.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvRptsCsvConfig.ColumnHeadersHeight = 29
        Me.dgvRptsCsvConfig.Location = New System.Drawing.Point(12, 143)
        Me.dgvRptsCsvConfig.Name = "dgvRptsCsvConfig"
        Me.dgvRptsCsvConfig.RowHeadersWidth = 51
        Me.dgvRptsCsvConfig.Size = New System.Drawing.Size(1698, 688)
        Me.dgvRptsCsvConfig.TabIndex = 74
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Maroon
        Me.Label3.Location = New System.Drawing.Point(654, 47)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(416, 13)
        Me.Label3.TabIndex = 73
        Me.Label3.Text = "Enter email addresses in the grid below, multiple emails can be separated by semi" &
    "-colon"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CmdExportRptConfigToExcel)
        Me.GroupBox1.Controls.Add(Me.cmdRefreshCsvConfigView)
        Me.GroupBox1.Controls.Add(Me.cmdCsvFilter)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cboFullName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(636, 99)
        Me.GroupBox1.TabIndex = 72
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Filter Criteria"
        '
        'CmdExportRptConfigToExcel
        '
        Me.CmdExportRptConfigToExcel.Image = CType(resources.GetObject("CmdExportRptConfigToExcel.Image"), System.Drawing.Image)
        Me.CmdExportRptConfigToExcel.Location = New System.Drawing.Point(587, 19)
        Me.CmdExportRptConfigToExcel.Name = "CmdExportRptConfigToExcel"
        Me.CmdExportRptConfigToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportRptConfigToExcel.TabIndex = 54
        Me.CmdExportRptConfigToExcel.UseVisualStyleBackColor = True
        '
        'cmdRefreshCsvConfigView
        '
        Me.cmdRefreshCsvConfigView.Location = New System.Drawing.Point(469, 53)
        Me.cmdRefreshCsvConfigView.Name = "cmdRefreshCsvConfigView"
        Me.cmdRefreshCsvConfigView.Size = New System.Drawing.Size(112, 23)
        Me.cmdRefreshCsvConfigView.TabIndex = 29
        Me.cmdRefreshCsvConfigView.Text = "Reset Filter"
        Me.cmdRefreshCsvConfigView.UseVisualStyleBackColor = True
        '
        'cmdCsvFilter
        '
        Me.cmdCsvFilter.Location = New System.Drawing.Point(469, 19)
        Me.cmdCsvFilter.Name = "cmdCsvFilter"
        Me.cmdCsvFilter.Size = New System.Drawing.Size(112, 32)
        Me.cmdCsvFilter.TabIndex = 31
        Me.cmdCsvFilter.Text = "Filter Data Grid"
        Me.cmdCsvFilter.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Client Name"
        '
        'cboFullName
        '
        Me.cboFullName.FormattingEnabled = True
        Me.cboFullName.Location = New System.Drawing.Point(84, 24)
        Me.cboFullName.Name = "cboFullName"
        Me.cboFullName.Size = New System.Drawing.Size(379, 21)
        Me.cboFullName.TabIndex = 29
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.dgvClientEmailAddresses)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.ViewGroupBox)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(2)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(2)
        Me.TabPage1.Size = New System.Drawing.Size(1718, 836)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Client Email Addresses"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.Maroon
        Me.Label12.Location = New System.Drawing.Point(706, 81)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(634, 13)
        Me.Label12.TabIndex = 117
        Me.Label12.Text = "These email adresses are used for automated client reports (Reports Exporter/FLOW" &
    ") and client Order/Trade confirmation (Mini-OMS)"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label11.Location = New System.Drawing.Point(8, 11)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(226, 24)
        Me.Label11.TabIndex = 116
        Me.Label11.Text = "Client Email Addresses"
        '
        'dgvClientEmailAddresses
        '
        Me.dgvClientEmailAddresses.AllowUserToOrderColumns = True
        Me.dgvClientEmailAddresses.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvClientEmailAddresses.ColumnHeadersHeight = 29
        Me.dgvClientEmailAddresses.Location = New System.Drawing.Point(12, 143)
        Me.dgvClientEmailAddresses.Name = "dgvClientEmailAddresses"
        Me.dgvClientEmailAddresses.RowHeadersWidth = 51
        Me.dgvClientEmailAddresses.Size = New System.Drawing.Size(1382, 685)
        Me.dgvClientEmailAddresses.TabIndex = 70
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(706, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(416, 13)
        Me.Label1.TabIndex = 71
        Me.Label1.Text = "Enter email addresses in the grid below, multiple emails can be separated by semi" &
    "-colon"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.CmdExportEmailsToExcel)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboShortcut)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshEmailView)
        Me.ViewGroupBox.Controls.Add(Me.CmdEmailFilter)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboPortfolioName)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 38)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(688, 99)
        Me.ViewGroupBox.TabIndex = 68
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Criteria"
        '
        'CmdExportEmailsToExcel
        '
        Me.CmdExportEmailsToExcel.Image = CType(resources.GetObject("CmdExportEmailsToExcel.Image"), System.Drawing.Image)
        Me.CmdExportEmailsToExcel.Location = New System.Drawing.Point(635, 24)
        Me.CmdExportEmailsToExcel.Name = "CmdExportEmailsToExcel"
        Me.CmdExportEmailsToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportEmailsToExcel.TabIndex = 54
        Me.CmdExportEmailsToExcel.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(51, 50)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(47, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "Shortcut"
        '
        'cboShortcut
        '
        Me.cboShortcut.FormattingEnabled = True
        Me.cboShortcut.Location = New System.Drawing.Point(115, 48)
        Me.cboShortcut.Name = "cboShortcut"
        Me.cboShortcut.Size = New System.Drawing.Size(115, 21)
        Me.cboShortcut.TabIndex = 42
        '
        'CmdRefreshEmailView
        '
        Me.CmdRefreshEmailView.Location = New System.Drawing.Point(517, 58)
        Me.CmdRefreshEmailView.Name = "CmdRefreshEmailView"
        Me.CmdRefreshEmailView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshEmailView.TabIndex = 29
        Me.CmdRefreshEmailView.Text = "Reset Filter"
        Me.CmdRefreshEmailView.UseVisualStyleBackColor = True
        '
        'CmdEmailFilter
        '
        Me.CmdEmailFilter.Location = New System.Drawing.Point(517, 24)
        Me.CmdEmailFilter.Name = "CmdEmailFilter"
        Me.CmdEmailFilter.Size = New System.Drawing.Size(112, 32)
        Me.CmdEmailFilter.TabIndex = 31
        Me.CmdEmailFilter.Text = "Filter Data Grid"
        Me.CmdEmailFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Portfolio Name"
        '
        'cboPortfolioName
        '
        Me.cboPortfolioName.FormattingEnabled = True
        Me.cboPortfolioName.Location = New System.Drawing.Point(115, 24)
        Me.cboPortfolioName.Name = "cboPortfolioName"
        Me.cboPortfolioName.Size = New System.Drawing.Size(396, 21)
        Me.cboPortfolioName.TabIndex = 29
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1726, 862)
        Me.TabControl1.TabIndex = 72
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Controls.Add(Me.dgvPDFs)
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(2)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1718, 836)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Client PDF Reports"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Maroon
        Me.Label14.Location = New System.Drawing.Point(655, 88)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(372, 13)
        Me.Label14.TabIndex = 118
        Me.Label14.Text = "These email adresses are used for automated client reports (Reports Exporter)"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.Maroon
        Me.Label7.Location = New System.Drawing.Point(655, 66)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(416, 13)
        Me.Label7.TabIndex = 117
        Me.Label7.Text = "Enter email addresses in the grid below, multiple emails can be separated by semi" &
    "-colon"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label6.Location = New System.Drawing.Point(8, 12)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(187, 24)
        Me.Label6.TabIndex = 116
        Me.Label6.Text = "Client PDF Reports"
        '
        'dgvPDFs
        '
        Me.dgvPDFs.AllowUserToOrderColumns = True
        Me.dgvPDFs.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvPDFs.ColumnHeadersHeight = 29
        Me.dgvPDFs.Location = New System.Drawing.Point(7, 140)
        Me.dgvPDFs.Name = "dgvPDFs"
        Me.dgvPDFs.RowHeadersWidth = 51
        Me.dgvPDFs.Size = New System.Drawing.Size(1703, 688)
        Me.dgvPDFs.TabIndex = 75
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button4)
        Me.GroupBox4.Controls.Add(Me.Button5)
        Me.GroupBox4.Controls.Add(Me.Button6)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.ComboBox2)
        Me.GroupBox4.Location = New System.Drawing.Point(1522, 916)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(553, 99)
        Me.GroupBox4.TabIndex = 80
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Filter Criteria"
        '
        'Button4
        '
        Me.Button4.Image = CType(resources.GetObject("Button4.Image"), System.Drawing.Image)
        Me.Button4.Location = New System.Drawing.Point(495, 20)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(39, 37)
        Me.Button4.TabIndex = 54
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(377, 54)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(112, 23)
        Me.Button5.TabIndex = 29
        Me.Button5.Text = "Reset Filter"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(377, 20)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(112, 32)
        Me.Button6.TabIndex = 31
        Me.Button6.Text = "Filter Data Grid"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 27)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Portfolio"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(115, 24)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(251, 21)
        Me.ComboBox2.TabIndex = 29
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.CmdExportRptSchedule)
        Me.GroupBox5.Controls.Add(Me.cmdRefreshPdfView)
        Me.GroupBox5.Controls.Add(Me.cmdPdfFilter)
        Me.GroupBox5.Controls.Add(Me.Label10)
        Me.GroupBox5.Controls.Add(Me.cboPDFPortfolioName)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 47)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(625, 87)
        Me.GroupBox5.TabIndex = 73
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Filter Criteria"
        '
        'CmdExportRptSchedule
        '
        Me.CmdExportRptSchedule.Image = CType(resources.GetObject("CmdExportRptSchedule.Image"), System.Drawing.Image)
        Me.CmdExportRptSchedule.Location = New System.Drawing.Point(576, 19)
        Me.CmdExportRptSchedule.Name = "CmdExportRptSchedule"
        Me.CmdExportRptSchedule.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportRptSchedule.TabIndex = 54
        Me.CmdExportRptSchedule.UseVisualStyleBackColor = True
        '
        'cmdRefreshPdfView
        '
        Me.cmdRefreshPdfView.Location = New System.Drawing.Point(458, 53)
        Me.cmdRefreshPdfView.Name = "cmdRefreshPdfView"
        Me.cmdRefreshPdfView.Size = New System.Drawing.Size(112, 23)
        Me.cmdRefreshPdfView.TabIndex = 29
        Me.cmdRefreshPdfView.Text = "Reset Filter"
        Me.cmdRefreshPdfView.UseVisualStyleBackColor = True
        '
        'cmdPdfFilter
        '
        Me.cmdPdfFilter.Location = New System.Drawing.Point(458, 19)
        Me.cmdPdfFilter.Name = "cmdPdfFilter"
        Me.cmdPdfFilter.Size = New System.Drawing.Size(112, 32)
        Me.cmdPdfFilter.TabIndex = 31
        Me.cmdPdfFilter.Text = "Filter Data Grid"
        Me.cmdPdfFilter.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 27)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Portfolio"
        '
        'cboPDFPortfolioName
        '
        Me.cboPDFPortfolioName.FormattingEnabled = True
        Me.cboPDFPortfolioName.Location = New System.Drawing.Point(65, 24)
        Me.cboPDFPortfolioName.Name = "cboPDFPortfolioName"
        Me.cboPDFPortfolioName.Size = New System.Drawing.Size(378, 21)
        Me.cboPDFPortfolioName.TabIndex = 29
        '
        'ImageListTickCross
        '
        Me.ImageListTickCross.ImageStream = CType(resources.GetObject("ImageListTickCross.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListTickCross.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListTickCross.Images.SetKeyName(0, "tick (1)16.png")
        Me.ImageListTickCross.Images.SetKeyName(1, "button_cancel16.png")
        '
        'FrmClientEmailAddresses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1726, 862)
        Me.Controls.Add(Me.TabControl1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "FrmClientEmailAddresses"
        Me.Text = "Client Email and Report details"
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvRptsCsvConfig, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvClientEmailAddresses, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.dgvPDFs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents dgvClientEmailAddresses As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents CmdExportEmailsToExcel As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents cboShortcut As ComboBox
    Friend WithEvents CmdRefreshEmailView As Button
    Friend WithEvents CmdEmailFilter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboPortfolioName As ComboBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents ImageListTickCross As ImageList
    Friend WithEvents dgvRptsCsvConfig As DataGridView
    Friend WithEvents Label3 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents CmdExportRptConfigToExcel As Button
    Friend WithEvents cmdRefreshCsvConfigView As Button
    Friend WithEvents cmdCsvFilter As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents cboFullName As ComboBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents CmdExportRptSchedule As Button
    Friend WithEvents cmdRefreshPdfView As Button
    Friend WithEvents cmdPdfFilter As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents cboPDFPortfolioName As ComboBox
    Friend WithEvents dgvPDFs As DataGridView
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label14 As Label
End Class
