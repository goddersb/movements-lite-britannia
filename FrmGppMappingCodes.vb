﻿Public Class FrmGppMappingCodes

    Dim SICConn As SqlClient.SqlConnection
    Dim dgvdata As SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim cboBrokerColumn As DataGridViewComboBoxColumn
    Dim cboClearerColumn As DataGridViewComboBoxColumn
    Dim cboCcyColumn As DataGridViewComboBoxColumn

    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub FrmGppMappingCodes_Load(sender As Object, e As EventArgs) Handles Me.Load

        ModRMS.DoubleBuffered(dgvGppMappingCodes, True)
        ClsIMS.PopulateComboboxWithData(cboBroker, "SELECT 1, B_Name1 from vwDolfinPaymentBrokers group by B_Name1 order by B_Name1")
        cboBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBroker.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboCCY, "SELECT 1, CR_Name1 from vwDolfinPaymentCurrencies group by CR_Name1 order by CR_Name1")
        cboBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBroker.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboClearer, "SELECT 1, B_Name1 from vwDolfinPaymentBrokers where B_Class = 9 group by B_Name1 order by B_Name1")
        cboBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBroker.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboClearerCode, "SELECT DISTINCT 1, GPP_ClrCode FROM vwDolfinPaymentGPPCodes ORDER BY GPP_ClrCode ASC ")
        cboClearerCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboClearerCode.AutoCompleteSource = AutoCompleteSource.ListItems

        LoadGridView()

    End Sub

    Private Sub LoadGridView(Optional ByVal varFilterWhereClause As String = "")

        Dim strSQL As String = ""
        Dim strColSql As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If varFilterWhereClause = "" Then
                strSQL = "SELECT BrokerName, ClearerName, CCY,  GPP_ClrCode, GPP_ShortCode, GPP_AccountNo, GPP_ID, GPP_BrCode, GPP_BrClrCode, GPP_CCYCode FROM vwDolfinPaymentGPPCodes ORDER BY BrokerName, ClearerName ASC"
            Else
                strSQL = $"SELECT BrokerName, ClearerName, CCY, GPP_ClrCode, GPP_ShortCode, GPP_AccountNo, GPP_ID, GPP_BrCode, GPP_BrClrCode, GPP_CCYCode FROM vwDolfinPaymentGPPCodes {varFilterWhereClause} ORDER BY BrokerName, ClearerName ASC"
            End If

            dgvGppMappingCodes.DataSource = Nothing
            SICConn = ClsIMS.GetNewOpenConnection

            'Broker Combo Box.
            strColSql = "SELECT B_Name1 AS [BrokerName] FROM BROKERS WHERE B_Class <> 9 ORDER BY B_Name1 ASC"
            dgvdata = New SqlClient.SqlDataAdapter(strColSql, SICConn)
            dgvdata.Fill(ds, "BrokerName")
            cboBrokerColumn = New DataGridViewComboBoxColumn
            cboBrokerColumn.HeaderText = "Broker Name"
            cboBrokerColumn.DataPropertyName = "BrokerName"
            cboBrokerColumn.DataSource = ds.Tables("BrokerName")
            cboBrokerColumn.ValueMember = ds.Tables("BrokerName").Columns(0).ColumnName
            cboBrokerColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboBrokerColumn.FlatStyle = FlatStyle.Flat
            dgvGppMappingCodes.Columns.Insert(0, cboBrokerColumn)

            'Clearer Combo Box.
            strColSql = "SELECT B_Name1 AS [ClearerName] FROM BROKERS WHERE B_Class = 9 ORDER BY B_Name1 ASC"
            dgvdata = New SqlClient.SqlDataAdapter(strColSql, SICConn)
            dgvdata.Fill(ds, "ClearerName")
            cboClearerColumn = New DataGridViewComboBoxColumn
            cboClearerColumn.HeaderText = "Clearer Name"
            cboClearerColumn.DataPropertyName = "ClearerName"
            cboClearerColumn.DataSource = ds.Tables("ClearerName")
            cboClearerColumn.ValueMember = ds.Tables("ClearerName").Columns(0).ColumnName
            cboClearerColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboClearerColumn.FlatStyle = FlatStyle.Flat
            dgvGppMappingCodes.Columns.Insert(1, cboClearerColumn)

            'CCY Combo Box.
            strColSql = "Select CR_Name1 as CCY FROM vwDolfinPaymentCurrencies"
            dgvdata = New SqlClient.SqlDataAdapter(strColSql, SICConn)
            dgvdata.Fill(ds, "vwDolfinPaymentCurrencies")
            cboCcyColumn = New DataGridViewComboBoxColumn
            cboCcyColumn.HeaderText = " GPP CCY"
            cboCcyColumn.DataPropertyName = "CCY"
            cboCcyColumn.DataSource = ds.Tables("vwDolfinPaymentCurrencies")
            cboCcyColumn.ValueMember = ds.Tables("vwDolfinPaymentCurrencies").Columns(0).ColumnName
            cboCcyColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCcyColumn.FlatStyle = FlatStyle.Flat
            dgvGppMappingCodes.Columns.Insert(2, cboCcyColumn)

            dgvdata = New SqlClient.SqlDataAdapter(strSQL, SICConn)
            ds = New DataSet
            dgvdata.Fill(ds, "GppMappingCodes")
            dgvGppMappingCodes.AutoGenerateColumns = True
            dgvGppMappingCodes.DataSource = ds.Tables("GppMappingCodes")

            dgvGppMappingCodes.Columns(0).Width = 220
            dgvGppMappingCodes.Columns(1).Width = 120
            dgvGppMappingCodes.Columns("GPP_ClrCode").Width = 110
            dgvGppMappingCodes.Columns("GPP_ClrCode").HeaderText = "GPP Clearer Code"
            dgvGppMappingCodes.Columns("GPP_ShortCode").Width = 120
            dgvGppMappingCodes.Columns("GPP_ShortCode").HeaderText = "GPP Short Code"
            dgvGppMappingCodes.Columns("GPP_AccountNo").Width = 140
            dgvGppMappingCodes.Columns("GPP_AccountNo").HeaderText = "GPP Account No"
            dgvGppMappingCodes.Columns(6).Visible = False
            dgvGppMappingCodes.Columns(7).Visible = False
            dgvGppMappingCodes.Columns(8).Visible = False
            dgvGppMappingCodes.Columns(9).Visible = False

            dgvGppMappingCodes.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvGppMappingCodes.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameGPP, Err.Description & " - Problem With viewing gpp mapping codes grid")
        End Try

    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click

        cboBroker.Text = ""
        cboClearer.Text = ""
        cboCCY.Text = ""
        cboClearerCode.Text = ""
        LoadGridView()

    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click

        Dim varWhereClauseFilter As String = "where"

        Cursor = Cursors.WaitCursor

        If cboBroker.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & $" and BrokerName ='{cboBroker.Text}'"
        End If

        If cboClearer.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & $" and ClearerName ='{cboClearer.Text}'"
        End If

        If cboCCY.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & $" and CCY ='{cboCCY.Text}'"
        End If

        If cboClearerCode.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & $" and GPP_ClrCode ='{cboClearerCode.Text}'"
        End If

        If varWhereClauseFilter <> "where" Then
            LoadGridView(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            LoadGridView()
        End If
        Cursor = Cursors.Default

    End Sub

    Private Sub dgvGppMappingCodes_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvGppMappingCodes.RowLeave

        Dim intBrokerId As Integer
        Dim intClearerId As Integer
        Dim intCcyId As Integer
        Dim intGppId As Integer

        Dim strShortCode As String
        Dim strAccountNo As String
        Dim strCCY As String
        Dim strBrokerName As String
        Dim strClearerName As String
        Dim strClearerCode As String

        If dgvGppMappingCodes.EndEdit Then
            Try
                intBrokerId = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_BrCode").Value), 0, dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_BrCode").Value)
                intClearerId = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_BrClrCode").Value), 0, dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_BrClrCode").Value)
                intCcyId = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_CCYCode").Value), 0, dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_CCYCode").Value)
                intGppId = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ID").Value), 0, dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ID").Value)

                strShortCode = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ShortCode").Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ShortCode").Value)
                strAccountNo = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_AccountNo").Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_AccountNo").Value)
                strClearerCode = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ClrCode").Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells("GPP_ClrCode").Value)
                strBrokerName = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells(0).Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells(0).Value)
                strClearerName = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells(1).Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells(1).Value)
                strCCY = IIf(IsDBNull(dgvGppMappingCodes.Rows(e.RowIndex).Cells(2).Value), "", dgvGppMappingCodes.Rows(e.RowIndex).Cells(2).Value)

                If strShortCode <> "" And strAccountNo <> "" And strCCY <> "" And strClearerCode <> "" Then
                    If intGppId = 0 And intBrokerId = 0 And intClearerId = 0 And intCcyId = 0 Then
                        Dim strCheckID As String = ClsIMS.GetSQLItem($"SELECT GPP_ID FROM vwDolfinPaymentGPPCodes WHERE BrokerName = '{strBrokerName}' And ClearerName = '{strClearerName}' and CCY = '{strCCY}'")
                        If strCheckID = "" Then

                            Dim strSql As String = "INSERT INTO vwDolfinPaymentGPPCodes (GPP_BrCode, GPP_BrClrCode, GPP_CCYCode, GPP_ShortCode, GPP_AccountNo, GPP_ClrCode) VALUES (" &
                                $"(SELECT B_Code FROM BROKERS WHERE B_Name1 = '{strBrokerName}'), " &
                                $"(SELECT B_Code FROM BROKERS WHERE B_Name1 = '{strClearerName}'), " &
                                $"(SELECT CR_Code from vwdolfinPaymentcurrencies WHERE CR_Name1 = '{strCCY}'), '{strShortCode}', '{strAccountNo}', '{strClearerCode}')"
                            ClsIMS.ExecuteString(SICConn, strSql)
                            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameGPP, strSql)
                        End If
                    Else
                        Dim strSQL As String = "UPDATE vwDolfinPaymentGPPCodes SET " &
                            $"GPP_BrCode = (SELECT B_Code FROM BROKERS WHERE B_Name1 = '{strBrokerName}'), " &
                            $"GPP_BrClrCode = (SELECT B_Code FROM BROKERS WHERE B_Name1 = '{strClearerName}'), " &
                            $"GPP_CCYCode = (SELECT CR_Code from vwdolfinPaymentcurrencies WHERE CR_Name1 = '{strCCY}'), " &
                            $"GPP_ClrCode = '{strClearerCode}'," &
                            $"GPP_ShortCode = '{strShortCode}', " &
                            $"GPP_AccountNo = '{strAccountNo}' " &
                            $"WHERE GPP_ID = {intGppId}"
                        ClsIMS.ExecuteString(SICConn, strSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameGPP, strSQL)
                    End If
                End If
            Catch ex As SqlClient.SqlException
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameGPP, Err.Description & $" - Problem with updating GPP Mapping for: {intGppId}.")
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameGPP, Err.Description & $" - Problem with updating GPP Mapping for: {intGppId}.")
            End Try
        End If

    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvGppMappingCodes, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvGppMappingCodes_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvGppMappingCodes.UserDeletingRow

        Dim intGppId As Integer = 0
        Try
            intGppId = IIf(IsDBNull(dgvGppMappingCodes.SelectedRows(0).Cells("GPP_ID").Value), 0, dgvGppMappingCodes.SelectedRows(0).Cells("GPP_ID").Value)
            Dim varResponse As Integer = MsgBox($"Are you sure you want to delete GPP Mapping Code {Chr(13)}" & IIf(IsDBNull(dgvGppMappingCodes.SelectedRows(0).Cells(0).Value), 0, dgvGppMappingCodes.SelectedRows(0).Cells(0).Value) & "?", vbYesNo + vbQuestion, "Are you sure")
            If varResponse = vbYes Then
                ClsIMS.ExecuteString(SICConn, $"DELETE DolfinPaymentGPPCodes WHERE GPP_ID = {intGppId}")
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameGPP, $"user deleted - delete vwDolfinPaymentGPPCodes GPP_ID {intGppId}.")
            Else
                e.Cancel = True
            End If
            dgvGppMappingCodes.Refresh()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameGPP, Err.Description & $" - Problem with deleting vwDolfinPaymentGPPCodes varid {intGppId}.")
        End Try

    End Sub

    Private Sub FrmGppMappingCodes_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed

        If SICConn.State = ConnectionState.Open Then
            SICConn.Close()
        End If

    End Sub

End Class