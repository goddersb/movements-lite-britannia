﻿Public Class FrmTreasuryAllocation
    Dim TAConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataTitles As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim TotalRow As New DataGridViewRow
    Dim CompletedCellEdit As Boolean = False
    Dim varCurrentRow As Integer = 0

    Private Sub TALoadGrid()
        Dim varSQL As String = ""
        Dim dgvColumnHeaderStyle As New DataGridViewCellStyle()
        dgvColumnHeaderStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Dim Boldstyle As New DataGridViewCellStyle()

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "Select 0,0,0 from vwDolfinPaymentSelectAccountTreasury where BrokerA = '12ghfgvfhg'"

            dgvTATitles.DataSource = Nothing
            TAConn = ClsIMS.GetNewOpenConnection
            dgvdataTitles = New SqlClient.SqlDataAdapter(varSQL, TAConn)
            ds = New DataSet
            dgvdataTitles.Fill(ds, "Titles")
            dgvTATitles.AutoGenerateColumns = True
            dgvTATitles.DataSource = ds.Tables("Titles")

            dgvTATitles.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised

            dgvTATitles.ColumnHeadersDefaultCellStyle = dgvColumnHeaderStyle
            dgvTATitles.Columns(0).Width = 400
            dgvTATitles.Columns(0).HeaderText = ""
            dgvTATitles.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATitles.Columns(1).Width = 300
            dgvTATitles.Columns(1).HeaderText = CboTAInstA.Text
            dgvTATitles.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATitles.Columns(2).Width = 300
            dgvTATitles.Columns(2).HeaderText = CboTAInstB.Text
            dgvTATitles.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable

            varSQL = "exec DolfinPaymentSelectAccountByDate '" & dtTransDate.Value.ToString("yyyy-MM-dd") & "'," & CboTAInstA.SelectedValue & "," & CboTAInstB.SelectedValue & "," & cboTACCY.SelectedValue & ", '" & ClsIMS.UserLocation & "'"

            dgvTA.DataSource = Nothing
            TAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TAConn)
            ds = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(ds, "vwDolfinPaymentSelectAccountTreasury")
            dgvTA.AutoGenerateColumns = True
            dgvTA.DataSource = ds.Tables("vwDolfinPaymentSelectAccountTreasury")

            TotalRow = New DataGridViewRow

            dgvTA.Columns(0).HeaderText = "Portfolio"
            dgvTA.Columns(0).Width = 400
            dgvTA.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(0).ReadOnly = True
            dgvTA.Columns(1).HeaderText = "Current Balance"
            dgvTA.Columns(1).Width = 100
            dgvTA.Columns(1).ReadOnly = True
            dgvTA.Columns(1).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(2).HeaderText = "Movement"
            dgvTA.Columns(2).Width = 100
            dgvTA.Columns(2).DefaultCellStyle.ForeColor = Color.Blue
            dgvTA.Columns(2).DefaultCellStyle.BackColor = Color.LightYellow
            dgvTA.Columns(2).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(3).HeaderText = "New Balance"
            dgvTA.Columns(3).Width = 100
            dgvTA.Columns(3).ReadOnly = True
            dgvTA.Columns(3).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(4).HeaderText = "Current Balance"
            dgvTA.Columns(4).Width = 100
            dgvTA.Columns(4).ReadOnly = True
            dgvTA.Columns(4).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(5).HeaderText = "Movement"
            dgvTA.Columns(5).Width = 100
            dgvTA.Columns(5).DefaultCellStyle.ForeColor = Color.Blue
            dgvTA.Columns(5).ReadOnly = True
            dgvTA.Columns(5).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(6).HeaderText = "New Balance"
            dgvTA.Columns(6).Width = 100
            dgvTA.Columns(6).ReadOnly = True
            dgvTA.Columns(6).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTA.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTA.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTA.Columns(7).Visible = False
            dgvTA.Columns(8).Visible = False
            dgvTA.Columns(9).Visible = False
            dgvTA.Columns(10).Visible = False
            dgvTA.Columns(11).Visible = False
            dgvTA.Columns(12).Visible = False
            dgvTA.Columns(13).Visible = False
            dgvTA.Columns(14).Visible = False
            dgvTA.Columns(15).Visible = False
            dgvTA.Columns(16).Visible = False
            dgvTA.Columns(17).Visible = False
            dgvTA.Columns(18).Visible = False
            dgvTA.Columns(19).Visible = False
            dgvTA.Columns(20).Visible = False
            dgvTA.Columns(21).Visible = False
            dgvTA.Columns(22).Visible = False
            dgvTA.Columns(23).Visible = False
            dgvTA.Columns(24).Visible = False
            dgvTA.Columns(25).Visible = False
            dgvTA.Columns(26).Visible = False
            dgvTA.Columns(27).Visible = False
            dgvTA.Columns(28).Visible = False

            Boldstyle.Font = New Font(dgvTA.Font, FontStyle.Bold)
            dgvTA.Rows(dgvTA.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray

            dgvTA.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTA.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub RunGrid()
        ClearResults()
        If cboTACCY.Text = "" Or CboTAInstA.Text = "" Or CboTAInstB.Text = "" Then
            MsgBox("Please complete all criteria to run treasury options", vbInformation, "Complete Criteria")
        ElseIf CboTAInstA.Text = CboTAInstB.Text Then
            MsgBox("Please select different institutions to run treasury options", vbInformation, "Different Institutions Required")
        Else
            TALoadGrid()
        End If
    End Sub

    Private Sub CmdTARefreshgrid_Click(sender As Object, e As EventArgs) Handles CmdTARefreshgrid.Click
        TALoadGrid()
    End Sub

    Private Sub FrmTreasuryAllocation_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        ClearResults()
        If TAConn.State = ConnectionState.Open Then
            TAConn.Close()
        End If
    End Sub

    Private Sub dgvTA_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTA.CellEndEdit
        Dim varNum As Double
        Dim varBalA As Double, varBalB As Double

        If Not IsDBNull(dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
            If dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value <> "" Then
                varNum = BasicCalculator(dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                If varNum <> 0 Then
                    dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = CDbl(varNum).ToString("#,##0.00")
                    varBalA = dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex - 1).Value
                    varBalB = dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 2).Value
                    If ValidAmount(varNum, CDbl(varBalA), CDbl(varBalB), e.ColumnIndex, e.RowIndex) Then
                        dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = (CDbl(varBalA) + varNum).ToString("#,##0.00")
                        dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = (-varNum).ToString("#,##0.00")
                        dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 4).Value = (CDbl(varBalB) + -varNum).ToString("#,##0.00")
                    Else
                        dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
                        dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
                    End If
                Else
                    dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
                    dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
                End If
            End If
        Else
            dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
            dgvTA.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
        End If
        Refreshtotals(e.ColumnIndex, e.RowIndex)
        CompletedCellEdit = True
    End Sub

    Private Sub Refreshtotals(ByVal vColNum As Integer, vRowNum As Integer)
        If IsDBNull(dgvTA.Rows(vRowNum).Cells(vColNum).Value) Then
            dgvTA.Rows(vRowNum).Cells(vColNum + 1).Value = dgvTA.Rows(vRowNum).Cells(vColNum - 1).Value
            dgvTA.Rows(vRowNum).Cells(vColNum + 4).Value = dgvTA.Rows(vRowNum).Cells(vColNum + 2).Value
        ElseIf dgvTA.Rows(vRowNum).Cells(vColNum).Value = "" Or dgvTA.Rows(vRowNum).Cells(vColNum).Value = "0" Then
            dgvTA.Rows(vRowNum).Cells(vColNum + 1).Value = dgvTA.Rows(vRowNum).Cells(vColNum - 1).Value
            dgvTA.Rows(vRowNum).Cells(vColNum + 4).Value = dgvTA.Rows(vRowNum).Cells(vColNum + 2).Value
        End If

        dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(vColNum).Value = SumColumnTotal(vColNum).ToString("#,##0.00")
        dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(vColNum + 1).Value = SumColumnTotal(vColNum + 1).ToString("#,##0.00")
        dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(vColNum + 3).Value = SumColumnTotal(vColNum + 3).ToString("#,##0.00")
        dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(vColNum + 4).Value = SumColumnTotal(vColNum + 4).ToString("#,##0.00")
    End Sub
    Private Function SumColumnTotal(ByVal vColnum As Integer) As Double
        SumColumnTotal = 0
        For i As Integer = 0 To Me.dgvTA.Rows.Count - 2
            If IsNumeric(dgvTA.Rows(i).Cells(vColnum).Value) Then
                SumColumnTotal = SumColumnTotal + dgvTA.Rows(i).Cells(vColnum).Value
            End If
        Next
    End Function

    Private Function ValidAmount(ByVal varNum As Double, varBalA As Double, varBalB As Double, vColNum As Integer, vRowNum As Integer) As Boolean
        ValidAmount = True
        If varNum > 0 Then
            If varBalB < varNum Then
                ValidAmount = False
                MsgBox("Amount is greater than the balance available in institution B", vbCritical, "Not enough in balance B")
            ElseIf Not IsEnoughCashInAccounts(math.Abs(varNum), vColNum - 1, vRowNum) Then
                ValidAmount = False
                MsgBox("There is not enough cash in the alternative accounts to do this transfer", vbCritical, "Cannot transfer")
            End If
        ElseIf varNum < 0 Then
            If varBalA < Math.Abs(varNum) Then
                ValidAmount = False
                MsgBox("Amount is greater than the balance available in institution A", vbCritical, "Not enough in balance A")
            ElseIf Not IsEnoughCashInAccounts(math.Abs(varNum), vColNum + 2, vRowNum) Then
                ValidAmount = False
                MsgBox("There is not enough cash in the alternative accounts to do this transfer", vbCritical, "Cannot transfer")
            End If
        Else
            ValidAmount = False
        End If
    End Function

    Private Function IsEnoughCashInAccounts(ByVal vCash As Double, ByVal vColnum As Integer, ByVal vRow As Integer) As Boolean
        Dim vSumColumnTotal As Double = 0

        IsEnoughCashInAccounts = False
        For i As Integer = 0 To Me.dgvTA.Rows.Count - 2
            If IsNumeric(dgvTA.Rows(i).Cells(vColnum).Value) Then
                If i <> vRow And dgvTA.Rows(i).Cells(vColnum).Value > 0 Then
                    vSumColumnTotal = vSumColumnTotal + dgvTA.Rows(i).Cells(vColnum).Value
                End If
            End If
        Next

        If vCash <= vSumColumnTotal Then
            IsEnoughCashInAccounts = True
        End If
    End Function

    Public Sub ClearResults()
        TimerGetResults.Enabled = False
        txtTAFileName.Text = ""
        txtTAStatus.Text = ""
        txtTAMessage.Text = ""
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        Dim Msg As Integer = vbYes

        If ValidMovements() Then
            If dtTransDate.Value.Date <> Now.Date Then
                Msg = MsgBox("The movements will be pre-dated in IMS for " & dtTransDate.Value.Date & vbNewLine & "Is this correct?", vbYesNo + vbQuestion, "Are you sure")
            End If

            If Msg = vbYes Then
                ClearResults()
                ClsPayTreasury = ClsPay
                ClsPayTreasury.CreateTreasuryTransactionsXL(dgvTA, dtTransDate.Value.Date)
                ClsPayTreasury.SubmitTreasuryTransactions(dgvTA, dtTransDate.Value.Date)
                TimerGetResults.Enabled = True
                If Not ClsPayTreasury.PaymentFilePath Is Nothing Then
                    If Dir(ClsPayTreasury.PaymentFilePath) <> "" Then
                        txtTAFileName.Text = Dir(ClsPayTreasury.PaymentFilePath)
                        txtTAStatus.Text = "Sent"
                        txtTAStatus.ForeColor = Color.Black
                        txtTAFileName.ForeColor = Color.Black
                        txtTAMessage.ForeColor = Color.Black
                    End If
                End If
                ClsPayTreasury = Nothing
            End If
        End If
    End Sub

    Private Function PopulateResults(ByRef varErrors As Integer, ByRef varErrorDesc As String) As Boolean
        PopulateResults = False

        If txtTAFileName.Text <> "" Then
            If ClsIMS.GetTAStatus(txtTAFileName.Text, varErrors, varErrorDesc) Then
                If varErrors = 0 Then
                    txtTAStatus.Text = ClsIMS.GetStatusType(cStatusSwiftAck)
                    txtTAStatus.ForeColor = Color.DarkGreen
                    txtTAFileName.ForeColor = Color.DarkGreen
                    txtTAMessage.ForeColor = Color.DarkGreen
                Else
                    txtTAStatus.Text = ClsIMS.GetStatusType(cStatusIMSErr)
                    txtTAStatus.ForeColor = Color.DarkRed
                    txtTAFileName.ForeColor = Color.DarkRed
                    txtTAMessage.ForeColor = Color.DarkRed
                End If
                txtTAMessage.Text = varErrorDesc
                PopulateResults = True
            End If
        End If
    End Function

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        ClearResults()
        cboTACCY.SelectedValue = -1
        CboTAInstA.SelectedValue = -1
        CboTAInstB.SelectedValue = -1
        TALoadGrid()
    End Sub

    Private Function ValidMovements() As Boolean
        ValidMovements = True
        If dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(1).Value <> dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(3).Value Then
            ValidMovements = False
            MsgBox("Total Amounts do not balance for institution A", vbCritical, "Amounts do not balance")
        ElseIf dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(4).Value <> dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(6).Value Then
            ValidMovements = False
            MsgBox("Total Amounts do not balance for institution B", vbCritical, "Amounts do not balance")
        ElseIf dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(2).Value <> dgvTA.Rows(dgvTA.Rows.Count - 1).Cells(5).Value Then
            ValidMovements = False
            MsgBox("Movement Amounts do not balance", vbCritical, "Amounts do not balance")
        End If
    End Function

    Private Sub FrmTreasuryAllocation_Load(sender As Object, e As EventArgs) Handles Me.Load
        ClsIMS.PopulateFormCombo(cboTACCY)
        cboTACCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTACCY.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateFormCombo(CboTAInstA)
        CboTAInstA.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboTAInstA.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateFormCombo(CboTAInstB)
        CboTAInstB.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboTAInstB.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvTA, 7)
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvTA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dgvTA.KeyPress
        Dim Characters As String = ChrW(Keys.Tab)
        Dim Characters2 As String = ChrW(Keys.Enter)

        If InStr(Characters, e.KeyChar) = 1 Then
            If dgvTA.CurrentRow.Index = dgvTA.Rows.Count - 2 Then
                dgvTA.CurrentCell = dgvTA.Rows(0).Cells(2)
            Else
                dgvTA.CurrentCell = dgvTA.Rows(dgvTA.CurrentRow.Index + 1).Cells(2)
            End If
        ElseIf InStr(Characters2, e.KeyChar) = 1 Then
            If dgvTA.CurrentRow.Index = dgvTA.Rows.Count - 1 Then
                dgvTA.CurrentCell = dgvTA.Rows(0).Cells(2)
            Else
                dgvTA.CurrentCell = dgvTA.Rows(dgvTA.CurrentRow.Index).Cells(2)
            End If
        End If
    End Sub

    Private Sub dgvTA_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTA.SelectionChanged
        If CompletedCellEdit Then
            MoveCell()
        End If
        dgvTA.Columns(3).Selected = False
    End Sub

    Private Sub MoveCell()
        CompletedCellEdit = False
        If dgvTA.CurrentRow.Index = dgvTA.Rows.Count - 1 Then
            dgvTA.CurrentCell = dgvTA.Rows(0).Cells(2)
        Else
            dgvTA.CurrentCell = dgvTA.Rows(dgvTA.CurrentRow.Index).Cells(2)
        End If
    End Sub

    Private Sub TimerGetResults_Tick(sender As Object, e As EventArgs) Handles TimerGetResults.Tick
        Dim varErrors As Integer = 0
        Dim varErrorDesc As String = ""

        Cursor = Cursors.WaitCursor
        If PopulateResults(varErrors, varErrorDesc) Then
            TimerGetResults.Enabled = False
            If varErrors <> 0 Then
                MsgBox("There has been a problem inporting the transactions into IFT" & vbNewLine & "It is possible some of the transactions were successful" & vbNewLine & vbNewLine & "Please contact systems support with the filename: " & txtTAFileName.Text, MsgBoxStyle.Critical)
            Else
                TALoadGrid()
            End If
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub dtTransDate_ValueChanged(sender As Object, e As EventArgs) Handles dtTransDate.ValueChanged
        RunGrid()
    End Sub

    Private Sub FrmTreasuryAllocation_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        ClsPayTreasury.ClearClass()
    End Sub

End Class