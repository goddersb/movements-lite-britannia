﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRASettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.dgvPayResults = New System.Windows.Forms.DataGridView()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.dgvPayTypes = New System.Windows.Forms.DataGridView()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.dgvPayRangesCumulative = New System.Windows.Forms.DataGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.dgvPayRanges = New System.Windows.Forms.DataGridView()
        Me.dgvCCY = New System.Windows.Forms.DataGridView()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgvBank = New System.Windows.Forms.DataGridView()
        Me.dgvRecipient = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvPEP = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvCtry = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.dgvBenTypes = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.dgvPayResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.dgvPayTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.dgvPayRangesCumulative, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvPayRanges, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCCY, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRecipient, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvPEP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvCtry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox9.SuspendLayout()
        CType(Me.dgvBenTypes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Controls.Add(Me.GroupBox8)
        Me.GroupBox1.Controls.Add(Me.GroupBox7)
        Me.GroupBox1.Controls.Add(Me.GroupBox6)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 55)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1649, 823)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Settings Criteria"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.dgvPayResults)
        Me.GroupBox8.Location = New System.Drawing.Point(745, 591)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(897, 224)
        Me.GroupBox8.TabIndex = 6
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Category 8 (Payment Results Details)"
        '
        'dgvPayResults
        '
        Me.dgvPayResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayResults.Location = New System.Drawing.Point(6, 19)
        Me.dgvPayResults.Name = "dgvPayResults"
        Me.dgvPayResults.RowHeadersVisible = False
        Me.dgvPayResults.Size = New System.Drawing.Size(885, 199)
        Me.dgvPayResults.TabIndex = 3
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.dgvPayTypes)
        Me.GroupBox7.Location = New System.Drawing.Point(1197, 403)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(446, 182)
        Me.GroupBox7.TabIndex = 5
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Category 6 (Payment Type Details)"
        '
        'dgvPayTypes
        '
        Me.dgvPayTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayTypes.Location = New System.Drawing.Point(6, 19)
        Me.dgvPayTypes.Name = "dgvPayTypes"
        Me.dgvPayTypes.RowHeadersVisible = False
        Me.dgvPayTypes.Size = New System.Drawing.Size(434, 157)
        Me.dgvPayTypes.TabIndex = 3
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.dgvPayRangesCumulative)
        Me.GroupBox6.Location = New System.Drawing.Point(1197, 20)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(446, 377)
        Me.GroupBox6.TabIndex = 4
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Category 5 (Cumulative Payment Details)"
        '
        'dgvPayRangesCumulative
        '
        Me.dgvPayRangesCumulative.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayRangesCumulative.Location = New System.Drawing.Point(6, 19)
        Me.dgvPayRangesCumulative.Name = "dgvPayRangesCumulative"
        Me.dgvPayRangesCumulative.RowHeadersVisible = False
        Me.dgvPayRangesCumulative.Size = New System.Drawing.Size(434, 352)
        Me.dgvPayRangesCumulative.TabIndex = 3
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dgvPayRanges)
        Me.GroupBox5.Controls.Add(Me.dgvCCY)
        Me.GroupBox5.Location = New System.Drawing.Point(745, 20)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(446, 565)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Category 4 (Currency && Payment Range Details)"
        '
        'dgvPayRanges
        '
        Me.dgvPayRanges.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayRanges.Location = New System.Drawing.Point(6, 188)
        Me.dgvPayRanges.Name = "dgvPayRanges"
        Me.dgvPayRanges.RowHeadersVisible = False
        Me.dgvPayRanges.Size = New System.Drawing.Size(434, 371)
        Me.dgvPayRanges.TabIndex = 3
        '
        'dgvCCY
        '
        Me.dgvCCY.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCCY.Location = New System.Drawing.Point(6, 19)
        Me.dgvCCY.Name = "dgvCCY"
        Me.dgvCCY.RowHeadersVisible = False
        Me.dgvCCY.Size = New System.Drawing.Size(434, 157)
        Me.dgvCCY.TabIndex = 3
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvBank)
        Me.GroupBox4.Controls.Add(Me.dgvRecipient)
        Me.GroupBox4.Location = New System.Drawing.Point(325, 208)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(414, 377)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Category 3 (Recipient && Bank details)"
        '
        'dgvBank
        '
        Me.dgvBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBank.Location = New System.Drawing.Point(6, 207)
        Me.dgvBank.Name = "dgvBank"
        Me.dgvBank.RowHeadersVisible = False
        Me.dgvBank.Size = New System.Drawing.Size(399, 164)
        Me.dgvBank.TabIndex = 4
        '
        'dgvRecipient
        '
        Me.dgvRecipient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRecipient.Location = New System.Drawing.Point(8, 19)
        Me.dgvRecipient.Name = "dgvRecipient"
        Me.dgvRecipient.RowHeadersVisible = False
        Me.dgvRecipient.Size = New System.Drawing.Size(397, 170)
        Me.dgvRecipient.TabIndex = 3
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvPEP)
        Me.GroupBox3.Location = New System.Drawing.Point(325, 20)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(414, 182)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Category 2 (PEP Details)"
        '
        'dgvPEP
        '
        Me.dgvPEP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPEP.Location = New System.Drawing.Point(6, 19)
        Me.dgvPEP.Name = "dgvPEP"
        Me.dgvPEP.RowHeadersVisible = False
        Me.dgvPEP.Size = New System.Drawing.Size(399, 157)
        Me.dgvPEP.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvCtry)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 20)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(312, 795)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Category 1 (Country Details)"
        '
        'dgvCtry
        '
        Me.dgvCtry.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCtry.Location = New System.Drawing.Point(6, 19)
        Me.dgvCtry.Name = "dgvCtry"
        Me.dgvCtry.RowHeadersVisible = False
        Me.dgvCtry.Size = New System.Drawing.Size(297, 770)
        Me.dgvCtry.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(14, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(247, 24)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Risk Assessment Settings"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.dgvBenTypes)
        Me.GroupBox9.Location = New System.Drawing.Point(332, 591)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(407, 224)
        Me.GroupBox9.TabIndex = 7
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Category 7 (Beneficiary Types && tollerances)"
        '
        'dgvBenTypes
        '
        Me.dgvBenTypes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBenTypes.Location = New System.Drawing.Point(6, 19)
        Me.dgvBenTypes.Name = "dgvBenTypes"
        Me.dgvBenTypes.RowHeadersVisible = False
        Me.dgvBenTypes.Size = New System.Drawing.Size(392, 199)
        Me.dgvBenTypes.TabIndex = 3
        '
        'FrmRASettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1666, 882)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmRASettings"
        Me.Text = "Risk Assessment Settings"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.dgvPayResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        CType(Me.dgvPayTypes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.dgvPayRangesCumulative, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvPayRanges, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCCY, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvBank, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRecipient, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvPEP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvCtry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.dgvBenTypes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents dgvPayRanges As DataGridView
    Friend WithEvents dgvCCY As DataGridView
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents dgvBank As DataGridView
    Friend WithEvents dgvRecipient As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents dgvPEP As DataGridView
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvCtry As DataGridView
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents dgvPayTypes As DataGridView
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents dgvPayRangesCumulative As DataGridView
    Friend WithEvents GroupBox8 As GroupBox
    Friend WithEvents dgvPayResults As DataGridView
    Friend WithEvents GroupBox9 As GroupBox
    Friend WithEvents dgvBenTypes As DataGridView
End Class
