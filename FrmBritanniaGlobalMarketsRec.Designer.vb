﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmBritanniaGlobalMarketsRec
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBritanniaGlobalMarketsRec))
        Me.dgvRec = New System.Windows.Forms.DataGridView()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.GrpFSDetails = New System.Windows.Forms.GroupBox()
        Me.CmdExportEmailsToExcel = New System.Windows.Forms.Button()
        Me.dtRunDate = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ChkReCalcRec = New System.Windows.Forms.CheckBox()
        Me.cmdRunRec = New System.Windows.Forms.Button()
        Me.mnuSelect = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuMatch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUnMatch = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpFSDetails.SuspendLayout()
        Me.mnuSelect.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvRec
        '
        Me.dgvRec.AllowUserToAddRows = False
        Me.dgvRec.AllowUserToDeleteRows = False
        Me.dgvRec.AllowUserToResizeRows = False
        Me.dgvRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRec.Location = New System.Drawing.Point(12, 121)
        Me.dgvRec.Name = "dgvRec"
        Me.dgvRec.RowHeadersWidth = 51
        Me.dgvRec.Size = New System.Drawing.Size(1604, 719)
        Me.dgvRec.TabIndex = 112
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(19, 9)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(373, 24)
        Me.lblpaymentTitle.TabIndex = 114
        Me.lblpaymentTitle.Text = "Britannia Global Markets Reconciliation"
        '
        'GrpFSDetails
        '
        Me.GrpFSDetails.BackColor = System.Drawing.Color.OldLace
        Me.GrpFSDetails.Controls.Add(Me.CmdExportEmailsToExcel)
        Me.GrpFSDetails.Controls.Add(Me.dtRunDate)
        Me.GrpFSDetails.Controls.Add(Me.Label11)
        Me.GrpFSDetails.Controls.Add(Me.ChkReCalcRec)
        Me.GrpFSDetails.Controls.Add(Me.cmdRunRec)
        Me.GrpFSDetails.Location = New System.Drawing.Point(12, 36)
        Me.GrpFSDetails.Name = "GrpFSDetails"
        Me.GrpFSDetails.Size = New System.Drawing.Size(420, 79)
        Me.GrpFSDetails.TabIndex = 113
        Me.GrpFSDetails.TabStop = False
        Me.GrpFSDetails.Text = "Reconcilliation Selection Criteria"
        '
        'CmdExportEmailsToExcel
        '
        Me.CmdExportEmailsToExcel.Image = CType(resources.GetObject("CmdExportEmailsToExcel.Image"), System.Drawing.Image)
        Me.CmdExportEmailsToExcel.Location = New System.Drawing.Point(364, 15)
        Me.CmdExportEmailsToExcel.Name = "CmdExportEmailsToExcel"
        Me.CmdExportEmailsToExcel.Size = New System.Drawing.Size(46, 52)
        Me.CmdExportEmailsToExcel.TabIndex = 118
        Me.CmdExportEmailsToExcel.UseVisualStyleBackColor = True
        '
        'dtRunDate
        '
        Me.dtRunDate.Location = New System.Drawing.Point(75, 20)
        Me.dtRunDate.Name = "dtRunDate"
        Me.dtRunDate.Size = New System.Drawing.Size(124, 20)
        Me.dtRunDate.TabIndex = 117
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 13)
        Me.Label11.TabIndex = 116
        Me.Label11.Text = "Run Date:"
        '
        'ChkReCalcRec
        '
        Me.ChkReCalcRec.AutoSize = True
        Me.ChkReCalcRec.Location = New System.Drawing.Point(75, 50)
        Me.ChkReCalcRec.Name = "ChkReCalcRec"
        Me.ChkReCalcRec.Size = New System.Drawing.Size(139, 17)
        Me.ChkReCalcRec.TabIndex = 114
        Me.ChkReCalcRec.Text = "ReCalc Reconcilliation?"
        Me.ChkReCalcRec.UseVisualStyleBackColor = True
        '
        'cmdRunRec
        '
        Me.cmdRunRec.Location = New System.Drawing.Point(218, 19)
        Me.cmdRunRec.Name = "cmdRunRec"
        Me.cmdRunRec.Size = New System.Drawing.Size(131, 23)
        Me.cmdRunRec.TabIndex = 113
        Me.cmdRunRec.Text = "Run Reconcilliation"
        Me.cmdRunRec.UseVisualStyleBackColor = True
        '
        'mnuSelect
        '
        Me.mnuSelect.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.mnuSelect.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMatch, Me.mnuUnMatch})
        Me.mnuSelect.Name = "mnuSelect"
        Me.mnuSelect.Size = New System.Drawing.Size(203, 48)
        '
        'mnuMatch
        '
        Me.mnuMatch.Name = "mnuMatch"
        Me.mnuMatch.Size = New System.Drawing.Size(202, 22)
        Me.mnuMatch.Text = "&Match Selected Items"
        '
        'mnuUnMatch
        '
        Me.mnuUnMatch.Name = "mnuUnMatch"
        Me.mnuUnMatch.Size = New System.Drawing.Size(202, 22)
        Me.mnuUnMatch.Text = "&Unmatch Selected Items"
        '
        'FrmBritanniaGlobalMarketsRec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1443, 852)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.GrpFSDetails)
        Me.Controls.Add(Me.dgvRec)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmBritanniaGlobalMarketsRec"
        Me.Text = "FrmBritanniaGlobalMarkets Reconcilliation"
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpFSDetails.ResumeLayout(False)
        Me.GrpFSDetails.PerformLayout()
        Me.mnuSelect.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvRec As DataGridView
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents GrpFSDetails As GroupBox
    Friend WithEvents cmdRunRec As Button
    Friend WithEvents ChkReCalcRec As CheckBox
    Friend WithEvents mnuSelect As ContextMenuStrip
    Friend WithEvents mnuMatch As ToolStripMenuItem
    Friend WithEvents mnuUnMatch As ToolStripMenuItem
    Friend WithEvents dtRunDate As DateTimePicker
    Friend WithEvents Label11 As Label
    Friend WithEvents CmdExportEmailsToExcel As Button
End Class
