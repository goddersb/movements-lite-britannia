﻿Imports Microsoft.Office
Imports System.Threading
Imports System.Globalization

Public Class ClsFeesPaid
    Private varID As Double = 0
    Private varPortfolioCode As Double = 0
    Private varPortfolioName As String = ""
    Private varCCYCode As Double = 0
    Private varCCYName As String = ""
    Private varStartDate As Date = Now
    Private varEndDate As Date = DateAdd(DateInterval.Day, +1, Now)
    Private varType As Integer = 0
    Private varTypeName As String = ""
    Private varReference As String = ""
    Private varAccountName As String = ""
    Private varLastPaymentAmount As Double = 0
    Private varDestination As String = ""
    Private varPortfolioCodeRedirect As Double = 0
    Private varPortfolioNameRedirect As String = ""


    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property

    Property StartDate() As Date
        Get
            Return varStartDate
        End Get
        Set(value As Date)
            varStartDate = value
        End Set
    End Property

    Property EndDate() As Date
        Get
            Return varEndDate
        End Get
        Set(value As Date)
            varEndDate = value
        End Set
    End Property

    Property Type() As Integer
        Get
            Return varType
        End Get
        Set(value As Integer)
            varType = value
        End Set
    End Property

    Property TypeName() As String
        Get
            Return varTypeName
        End Get
        Set(value As String)
            varTypeName = value
        End Set
    End Property

    Property Reference() As String
        Get
            Return varReference
        End Get
        Set(value As String)
            varReference = value
        End Set
    End Property

    Property AccountName() As String
        Get
            Return varAccountName
        End Get
        Set(value As String)
            varAccountName = value
        End Set
    End Property

    Property LastPaymentAmount() As Double
        Get
            Return varLastPaymentAmount
        End Get
        Set(value As Double)
            varLastPaymentAmount = value
        End Set
    End Property

    Property Destination() As String
        Get
            Return varDestination
        End Get
        Set(value As String)
            varDestination = value
        End Set
    End Property

    Property PortfolioCodeRedirect() As Double
        Get
            Return varPortfolioCodeRedirect
        End Get
        Set(value As Double)
            varPortfolioCodeRedirect = value
        End Set
    End Property

    Property PortfolioNameRedirect() As String
        Get
            Return varPortfolioNameRedirect
        End Get
        Set(value As String)
            varPortfolioNameRedirect = value
        End Set
    End Property

    Public Function CreateMultreesExportXL(ByVal dgv As DataGridView) As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim j As Integer

        Try
            Dim FullFilePathTemplateXL As String = ClsIMS.GetFilePath("Templates") & "MultreesUpload.xlsm"
            Dim FullFilePathXL As String = "c:\Temp\MultreesUpload.xlsm"

            Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
            FullFilePathTemplateXL = System.IO.Path.Combine(ClsIMS.GetFilePath("Templates"), "MultreesUpload.xlsm")

            xlApp = New Interop.Excel.Application
            xlApp.ScreenUpdating = False
            'xlApp.Visible = True
            xlWorkBook = xlApp.Workbooks.Open(FullFilePathTemplateXL)
            xlWorkSheet = xlWorkBook.Sheets("Input")


            j = 4
            If dgv.RowCount > 0 Then
                For i As Integer = 0 To dgv.RowCount - 1
                    If i <> 0 Then
                        xlWorkSheet.Rows(j).copy
                        xlWorkSheet.Rows(j + 1).select
                        xlWorkSheet.Paste()
                    End If
                    xlWorkSheet.Cells(j, 1) = "Fee"
                    xlWorkSheet.Cells(j, 2) = dgv.Rows(i).Cells("CCY Chargeable").Value
                    xlWorkSheet.Cells(j, 3) = Now.Date.ToString("dd/MM/yyyy")
                    xlWorkSheet.Cells(j, 4) = "Debit"
                    xlWorkSheet.Cells(j, 5) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_AccountNo").Value), "", dgv.Rows(i).Cells("Mul_AccountNo").Value)
                    xlWorkSheet.Cells(j, 6) = dgv.Rows(i).Cells("Payment Amount").Value
                    xlWorkSheet.Cells(j, 7) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_PfShortcut1").Value), "", dgv.Rows(i).Cells("Mul_PfShortcut1").Value)
                    xlWorkSheet.Cells(j, 8) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_PfShortcut1").Value), "", dgv.Rows(i).Cells("Mul_PfShortcut1").Value)
                    j = j + 1
                    xlWorkSheet.Rows(j).copy
                    xlWorkSheet.Rows(j + 1).select
                    xlWorkSheet.Paste()
                    xlWorkSheet.Cells(j, 1) = "Fee"
                    xlWorkSheet.Cells(j, 2) = dgv.Rows(i).Cells("CCY Chargeable").Value
                    xlWorkSheet.Cells(j, 3) = Now.Date.ToString("dd/MM/yyyy")
                    xlWorkSheet.Cells(j, 4) = "Credit"
                    xlWorkSheet.Cells(j, 5) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_AccountNo").Value), "", dgv.Rows(i).Cells("Mul_AccountNo").Value)
                    xlWorkSheet.Cells(j, 6) = dgv.Rows(i).Cells("Payment Amount").Value
                    xlWorkSheet.Cells(j, 7) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_PfShortcut1").Value), "", dgv.Rows(i).Cells("Mul_PfShortcut1").Value)
                    xlWorkSheet.Cells(j, 8) = IIf(IsDBNull(dgv.Rows(i).Cells("Mul_PfShortcut1").Value), "", dgv.Rows(i).Cells("Mul_PfShortcut1").Value)
                    j = j + 1
                Next
            End If
            xlWorkSheet.SaveAs(FullFilePathXL)
            xlApp.ScreenUpdating = True
            xlWorkBook.Close()
            xlApp.Quit()
            CreateMultreesExportXL = True
        Catch ex As Exception
            CreateMultreesExportXL = False
            xlApp.ScreenUpdating = True
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in CreateMultreesExportXL")
        End Try
    End Function
End Class
