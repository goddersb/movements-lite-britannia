﻿Public Class FrmCorpActionTerms
    Dim CAConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataCCY As New SqlClient.SqlDataAdapter
    Dim CmdSwift As DataGridViewButtonColumn
    Dim ds As New DataSet
    Dim cboCCYColumn As New DataGridViewComboBoxColumn

    Private Sub LoadGridCATerms()
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsTermsLinks where [CA ID] = " & ClsCA.ID

            dgvCATerms.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsTermsLinks")
            dgvCATerms.AutoGenerateColumns = True
            dgvCATerms.DataSource = ds.Tables("vwDolfinPaymentCorporateActionsTermsLinks")

            dgvCATerms.Columns("CA ID").Visible = False
            dgvCATerms.Columns("TermNumber").Visible = False
            dgvCATerms.Columns("reference").Visible = False
            dgvCATerms.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCATerms.Refresh()


            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsTermsSelected where SR_ID = " & ClsCA.ID & " And t_isin = '" & ClsCA.ISIN & "'"

            dgvCAPortfolio.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsTermsSelected")
            dgvCAPortfolio.AutoGenerateColumns = True
            dgvCAPortfolio.DataSource = ds.Tables("vwDolfinPaymentCorporateActionsTermsSelected")

            dgvCAPortfolio.Columns("pf_code").Visible = False
            dgvCAPortfolio.Columns("t_isin").Visible = False
            dgvCAPortfolio.Columns("TermNo").Visible = False
            dgvCAPortfolio.Columns("SRCA_TermNumber").Visible = False
            dgvCAPortfolio.Columns("T_Code").Visible = False
            dgvCAPortfolio.Columns("sr_id").Visible = False
            dgvCAPortfolio.Columns("portfolio").Width = 250
            dgvCAPortfolio.Columns("portfolio").ReadOnly = True
            dgvCAPortfolio.Columns("pf_shortcut1").ReadOnly = True
            dgvCAPortfolio.Columns("quantity").ReadOnly = True

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select TermNo FROM vwDolfinPaymentCorporateActionsTermsLinks where [CA ID] = " & ClsCA.ID, CAConn)
            dgvdataCCY.Fill(ds, "Terms")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "Term Option"
            cboCCYColumn.DataPropertyName = "TermNo"
            cboCCYColumn.DataSource = ds.Tables("Terms")
            cboCCYColumn.ValueMember = ds.Tables("Terms").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            dgvCAPortfolio.Columns.Insert(0, cboCCYColumn)
            dgvCAPortfolio.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCAPortfolio.Refresh()

            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsInformationLinks where [CA ID] = " & ClsCA.ID

            dgvCAInfo.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsInformationLinks")
            dgvCAInfo.AutoGenerateColumns = True
            dgvCAInfo.DataSource = ds.Tables("vwDolfinPaymentCorporateActionsInformationLinks")
            dgvCAInfo.Columns("CA ID").Visible = False
            dgvCAInfo.Columns("sr_filename").Visible = False
            dgvCAInfo.Columns("description").Width = 2000

            Dim CmdSwift As New DataGridViewButtonColumn()
            CmdSwift.HeaderText = "View Swift"
            CmdSwift.Text = "View Swift"
            CmdSwift.Name = "CmdSwift"
            CmdSwift.UseColumnTextForButtonValue = True
            dgvCAInfo.Columns.Insert(0, CmdSwift)

            dgvCAInfo.DefaultCellStyle.WrapMode = DataGridViewTriState.True
            dgvCAInfo.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCAInfo.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCorpAction, Err.Description & " - Problem with viewing corporate actions grid")
            Cursor = Cursors.Default
            dgvCATerms.DataSource = Nothing
            dgvCAPortfolio.DataSource = Nothing
            dgvCAInfo.DataSource = Nothing
        End Try
    End Sub


    Private Sub FrmCorpActionTerms_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvCATerms, True)
        ModRMS.DoubleBuffered(dgvCAPortfolio, True)
        ModRMS.DoubleBuffered(dgvCAInfo, True)
        txtCAName.Text = ClsCA.InstrumentName
        txtCAISIN.Text = ClsCA.ISIN
        txtCAEvent.Text = ClsCA.CAEvent
        txtCAMV.Text = ClsCA.MV
        txtCACustodian.Text = ClsCA.Custodian
        txtCACustodianDeadline.Text = IIf(ClsCA.CustodianDeadline = "2001-01-01", "", ClsCA.CustodianDeadline).ToString
        txtCADolfinDeadline.Text = IIf(ClsCA.DolfinDeadline = "2001-01-01", "", ClsCA.DolfinDeadline).ToString
        txtCAExDate.Text = IIf(ClsCA.ExDate = "2001-01-01", "", ClsCA.ExDate).ToString
        txtCARecDate.Text = IIf(ClsCA.RecDate = "2001-01-01", "", ClsCA.RecDate).ToString
        txtCASettledPosition.Text = Format(ClsCA.SettledPosition, "#,##0.00")
        Dim varIMSPosition As String = ClsIMS.GetSQLItem("SELECT sum(quantity) FROM vwDolfinPaymentCorporateActionsTermsSelected where SR_ID = " & ClsCA.ID & " And t_isin = '" & ClsCA.ISIN & "'")
        If IsNumeric(varIMSPosition) Then
            txtCAIMSPosition.Text = Format(CDbl(varIMSPosition), "#,##0.00")
            txtCAPositionDifference.Text = Format(ClsCA.SettledPosition - CDbl(varIMSPosition), "#,##0.00")
            If (ClsCA.SettledPosition - CDbl(varIMSPosition)) <> 0 Then
                txtCAPositionDifference.BackColor = Color.Red
            End If
        Else
            txtCAIMSPosition.Text = 0
            txtCAPositionDifference.Text = Format(ClsCA.SettledPosition, "#,##0.00")
            If ClsCA.SettledPosition <> 0 Then
                txtCAPositionDifference.BackColor = Color.Red
            End If
        End If


        LblElected.Text = "Last elected by: "
        LoadGridCATerms()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Dim varFullFilePath As String = ""
        Cursor = Cursors.WaitCursor
        If TabTerms.SelectedIndex = 0 Then
            ExportGridToExcelFromForm(dgvCATerms, 0)
        ElseIf TabTerms.SelectedIndex = 1 Then
            ExportGridToExcelFromForm(dgvCAPortfolio, 0)
        ElseIf TabTerms.SelectedIndex = 2 Then
            ExportGridToExcelFromForm(dgvCAInfo, 0)
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvCAInfo_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCAInfo.CellClick
        If e.RowIndex >= 0 And e.ColumnIndex = 0 Then
            If Not IsDBNull(dgvCAInfo.Rows(e.RowIndex).Cells("sr_filename").Value) Then
                ClsIMS.OpenSwiftFile(dgvCAInfo.Rows(e.RowIndex).Cells("sr_filename").Value)
            End If
        End If
    End Sub

    Private Sub dgvCAPortfolio_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCAPortfolio.CellValueChanged
        Dim varTerm As String = dgvCAPortfolio.Rows(e.RowIndex).Cells(5).Value
        Dim varID As String = dgvCAPortfolio.Rows(e.RowIndex).Cells(6).Value
        Dim varPortfolioCode As String = dgvCAPortfolio.Rows(e.RowIndex).Cells(0).Value
        Dim varPortfolioName As String = dgvCAPortfolio.Rows(e.RowIndex).Cells(2).Value

        ClsIMS.ExecuteString(CAConn, "update DolfinPaymentSwiftReadCorpActionTermsSelected set SRCA_Term = '" & varTerm & "', SRCA_TermNumber = " & varTerm & ",LastModifiedBy = '" & ClsIMS.FullUserName & "',LastModifiedDate = getdate() where SR_ID = " & varID & " and PortfolioCode = " & varPortfolioCode)
        Dim varResponse As Integer = MsgBox("Option " & varTerm & " has been selected for " & varPortfolioName, vbInformation)
    End Sub
End Class