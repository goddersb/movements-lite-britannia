﻿Imports System.ComponentModel

Public Class FrmPasswordGenerator
    Private Const cMaxPassword As Integer = 10
    Private Const cAnnualFee As Double = 150
    Private varNew As Boolean = True

    Private Sub ClearForm()
        varNew = True
        txtPassword.Text = ""
        txtPassword1.Text = ""
        txtPassword2.Text = ""
        txtPassword3.Text = ""
        txtPassword4.Text = ""
        txtPassword5.Text = ""
        txtPassword6.Text = ""
        txtPassword7.Text = ""
        txtPassword8.Text = ""
        txtPassword9.Text = ""
        txtPassword10.Text = ""
        txtNewPassword.Text = ""
        cboQ1.Text = ""
        txtAns1.Text = ""
        cboQ2.Text = ""
        txtAns2.Text = ""
        cboQ3.Text = ""
        txtAns3.Text = ""
        cboTier.SelectedValue = 4
        txtVolopaToken.Text = ""
        dtStartDate.Value = Now
        ChkAnnualFee.Checked = True
        txtAnnualFee.Text = cAnnualFee
    End Sub

    Private Sub LoadClientPassword()
        If IsNumeric(cboClient.SelectedValue) Then
            ClearForm()
            EnableGroups(True)
            Dim lstDetails As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select cust_code,cust_Birthdate,cust_ContTel,cust_Contemail,Password,Q1,A1,Q2,A2,Q3,A3,VolopaToken,isnull(VolopaTier,4) as VolopaTier,VolopaStartDate,VolopaAnnualFee from vwDolfinPaymentClientPortfolio c inner join dolfinpaymentpassword p on c.cust_id = p.cust_id where c.cust_id = " & cboClient.SelectedValue)
            If Not lstDetails Is Nothing Then
                varNew = False
                txtCode.Text = lstDetails.Item("cust_code").ToString
                If IsDate(lstDetails.Item("cust_Birthdate")) Then
                    txtDOB.Text = CDate(lstDetails.Item("cust_Birthdate")).ToString("dd-MMM-yyyy")
                End If
                txtNo.Text = lstDetails.Item("cust_ContTel").ToString
                txtEmail.Text = lstDetails.Item("cust_Contemail").ToString
                txtPassword.Text = lstDetails.Item("Password").ToString
                txtPassword1.Text = Mid(lstDetails.Item("Password").ToString, 1, 1)
                txtPassword1.Text = Mid(lstDetails.Item("Password").ToString, 1, 1)
                txtPassword2.Text = Mid(lstDetails.Item("Password").ToString, 2, 1)
                txtPassword3.Text = Mid(lstDetails.Item("Password").ToString, 3, 1)
                txtPassword4.Text = Mid(lstDetails.Item("Password").ToString, 4, 1)
                txtPassword5.Text = Mid(lstDetails.Item("Password").ToString, 5, 1)
                txtPassword6.Text = Mid(lstDetails.Item("Password").ToString, 6, 1)
                txtPassword7.Text = Mid(lstDetails.Item("Password").ToString, 7, 1)
                txtPassword8.Text = Mid(lstDetails.Item("Password").ToString, 8, 1)
                txtPassword9.Text = Mid(lstDetails.Item("Password").ToString, 9, 1)
                txtPassword10.Text = Mid(lstDetails.Item("Password").ToString, 10, 1)
                cboQ1.Text = lstDetails.Item("Q1").ToString
                txtAns1.Text = lstDetails.Item("A1").ToString
                cboQ2.Text = lstDetails.Item("Q2").ToString
                txtAns2.Text = lstDetails.Item("A2").ToString
                cboQ3.Text = lstDetails.Item("Q3").ToString
                txtAns3.Text = lstDetails.Item("A3").ToString
                txtVolopaToken.Text = lstDetails.Item("VolopaToken").ToString
                cboTier.SelectedValue = lstDetails.Item("VolopaTier")
                dtStartDate.Value = IIf(lstDetails.Item("VolopaStartDate") = "", Now, lstDetails.Item("VolopaStartDate"))
                If IsNumeric(lstDetails.Item("VolopaAnnualFee")) Then
                    ChkAnnualFee.Checked = True
                    txtAnnualFee.Text = CInt(lstDetails.Item("VolopaAnnualFee"))
                End If
            End If
        End If
    End Sub

    Private Sub EnableGroups(ByVal varEnabled As Boolean)
        GrpPassword.Enabled = varEnabled
        GrpQuestions.Enabled = varEnabled
        GrpVolopa.Enabled = varEnabled
    End Sub

    Private Sub FrmPasswordGenerator_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboClient, "Select Cust_ID,Name from vwDolfinPaymentClientPortfolio where branchCode = " & ClsIMS.UserLocationCode & " group by Cust_ID,Name order by Name")
        ClsIMS.PopulateComboboxWithData(cboQ1, "Select ID,PasswordQuestion from DolfinPaymentPasswordQuestions order by PasswordQuestion")
        ClsIMS.PopulateComboboxWithData(cboQ2, "Select ID,PasswordQuestion from DolfinPaymentPasswordQuestions order by PasswordQuestion")
        ClsIMS.PopulateComboboxWithData(cboQ3, "Select ID,PasswordQuestion from DolfinPaymentPasswordQuestions order by PasswordQuestion")
        ClsIMS.PopulateComboboxWithData(cboTier, "Select VolopaTier, 'Tier ' + cast(VolopaTier as nvarchar) as Tier from DolfinPaymentVolopaTier")
        ClearForm()

        Try
            If IsFormOpen(NewPaymentRequest) Then
                cboClient.SelectedValue = ClsIMS.GetSQLItem("Select cust_id from customers c inner join cuporel on c.cust_id = cupo_custid where cupo_pfcode = " & NewPaymentRequest.cboTemFindPortfolio.SelectedValue)
                EnableGroups(True)
            ElseIf IsFormOpen(NewConfirmReceipt) Then
                If NewConfirmReceipt.dgvPR.SelectedCells.Count > 0 Then
                    cboClient.SelectedValue = NewConfirmReceipt.dgvPR.Rows(NewConfirmReceipt.dgvPR.SelectedCells.Item(0).RowIndex).Cells("P_CustomerId").Value
                    EnableGroups(True)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CmdGen_Click(sender As Object, e As EventArgs) Handles CmdGen.Click
        On Error Resume Next
        Dim Letters As New List(Of Integer)
        'add ASCII Codes for Numbers
        For i As Integer = 48 To 57
            Letters.Add(i)
        Next
        'Upper Case
        For i As Integer = 65 To 90
            Letters.Add(i)
        Next
        'Lower Case
        'For i As Integer = 97 To 122
        'Letters.Add(i)
        'Next
        'Special Characters
        'For i As Integer = 33 To 42
        'Letters.Add(i)
        'Next
        'Select 8 to 12 random integers from number of items in Letters
        'Convert those random integers to characters and
        'Add each to string to display in txtPassword.text

        Dim Rnd As New Random
        Dim SB As New System.Text.StringBuilder
        Dim Temp As Integer
        For count As Integer = 1 To cMaxPassword 'Val(txtLenPass.Text)
            Temp = Rnd.Next(0, Letters.Count)
            SB.Append(Chr(Letters(Temp)))
        Next
        txtNewPassword.Text = UCase(SB.ToString)
        PopulatePasswordCharacters()
    End Sub

    Public Sub PopulatePasswordCharacters()
        txtPassword1.Text = Mid(txtNewPassword.Text, 1, 1)
        txtPassword1.Text = Mid(txtNewPassword.Text, 1, 1)
        txtPassword2.Text = Mid(txtNewPassword.Text, 2, 1)
        txtPassword3.Text = Mid(txtNewPassword.Text, 3, 1)
        txtPassword4.Text = Mid(txtNewPassword.Text, 4, 1)
        txtPassword5.Text = Mid(txtNewPassword.Text, 5, 1)
        txtPassword6.Text = Mid(txtNewPassword.Text, 6, 1)
        txtPassword7.Text = Mid(txtNewPassword.Text, 7, 1)
        txtPassword8.Text = Mid(txtNewPassword.Text, 8, 1)
        txtPassword9.Text = Mid(txtNewPassword.Text, 9, 1)
        txtPassword10.Text = Mid(txtNewPassword.Text, 10, 1)
    End Sub


    Private Sub CmdSave_Click(sender As Object, e As EventArgs) Handles CmdSave.Click
        Dim vbResponse As Integer = vbNo
        If IsNumeric(cboClient.SelectedValue) Then
            If ValidateSecurity() Then
                If txtNewPassword.Text <> "" Then
                    vbResponse = MsgBox("Are you sure you want to replace the current password with the new password?", vbQuestion + vbYesNo, "Please confirm password change")
                End If

                ClsIMS.SaveGeneratedPassword(varNew, cboClient.SelectedValue, Strings.Left(IIf(vbResponse = vbYes, txtNewPassword.Text, txtPassword.Text), cMaxPassword), Strings.Left(cboQ1.Text, 250),
                                                 Strings.Left(txtAns1.Text, 250), Strings.Left(cboQ2.Text, 250), Strings.Left(txtAns2.Text, 250), Strings.Left(cboQ3.Text, 250),
                                                 Strings.Left(txtAns3.Text, 250), IIf(IsNumeric(txtVolopaToken.Text), txtVolopaToken.Text, 0), cboTier.SelectedValue, dtStartDate.Value, ChkAnnualFee.Checked, IIf(Not IsNumeric(txtAnnualFee.Text), 0, txtAnnualFee.Text))
                MsgBox("Client details saved", vbInformation)
                LoadClientPassword()
            End If
        End If
    End Sub

    Public Function ValidateSecurity() As Boolean
        ValidateSecurity = True
        If txtPassword.Text = "" And txtNewPassword.Text = "" Then
            MsgBox("Please generate a new password for this client", vbInformation)
            ValidateSecurity = False
            'ElseIf cboQ1.Text = "" Then
            '   MsgBox("Please select a question 1 for this client", vbInformation)
            '  ValidateSecurity = False
            'ElseIf txtAns1.Text = "" Then
            '   MsgBox("Please select an answer 1 for this client", vbInformation)
            '  ValidateSecurity = False
            'ElseIf cboQ2.Text = "" Then
            '   MsgBox("Please select a question 2 for this client", vbInformation)
            '  ValidateSecurity = False
            'ElseIf txtAns2.Text = "" Then
            '    MsgBox("Please select an answer 2 for this client", vbInformation)
            '   ValidateSecurity = False
            'ElseIf cboQ3.Text = "" Then
            '   MsgBox("Please select a question 3 for this client", vbInformation)
            '  ValidateSecurity = False
            'ElseIf txtAns3.Text = "" Then
            '   MsgBox("Please select an answer 3 for this client", vbInformation)
            '  ValidateSecurity = False
        End If
    End Function


    Private Sub cboClient_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClient.SelectedIndexChanged
        LoadClientPassword()
    End Sub

    Private Sub cboQ2_Validating(sender As Object, e As CancelEventArgs) Handles cboQ2.Validating
        If cboQ2.Text <> "" And (cboQ2.Text = cboQ1.Text Or cboQ2.Text = cboQ3.Text) Then
            cboQ2.Text = ""
            e.Cancel = True
        End If
    End Sub

    Private Sub cboQ3_Validating(sender As Object, e As CancelEventArgs) Handles cboQ3.Validating
        If cboQ3.Text <> "" And (cboQ3.Text = cboQ1.Text Or cboQ3.Text = cboQ2.Text) Then
            cboQ3.Text = ""
            e.Cancel = True
        End If
    End Sub

    Private Sub cboQ1_Validating(sender As Object, e As CancelEventArgs) Handles cboQ1.Validating
        If cboQ1.Text <> "" And (cboQ1.Text = cboQ2.Text Or cboQ1.Text = cboQ3.Text) Then
            cboQ1.Text = ""
            e.Cancel = True
        End If
    End Sub

    Private Sub cboTier_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTier.SelectedIndexChanged
        If IsNumeric(cboTier.SelectedValue) Then
            Dim lstDetails As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select VolopaMaxLoadpd,VolopaMaxLoadpm,VolopaMaxLoadpa from DolfinPaymentVolopaTier where VolopaTier = " & cboTier.SelectedValue)
            If Not lstDetails Is Nothing Then
                txtMaxpD.Text = CDbl(lstDetails.Item("VolopaMaxLoadpd")).ToString("#,##0.00")
                txtMaxpM.Text = CDbl(lstDetails.Item("VolopaMaxLoadpm")).ToString("#,##0.00")
                txtMaxpY.Text = CDbl(lstDetails.Item("VolopaMaxLoadpa")).ToString("#,##0.00")
            End If
        End If
    End Sub

    Private Sub txtNewPassword_TextChanged(sender As Object, e As EventArgs) Handles txtNewPassword.TextChanged
        PopulatePasswordCharacters()
    End Sub

    Private Sub ChkAnnualFee_CheckedChanged(sender As Object, e As EventArgs) Handles ChkAnnualFee.CheckedChanged
        If ChkAnnualFee.Checked Then
            txtAnnualFee.Text = cAnnualFee
            txtAnnualFee.Enabled = True
        Else
            txtAnnualFee.Text = ""
            txtAnnualFee.Enabled = False
        End If
    End Sub
End Class