﻿Public Class FrmGridView

    Private Sub FormatAuditTrailGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            If ClsGV.GridOption = 0 Or ClsGV.GridOption = 4 Then
                dgv.Columns(0).Width = 100
                dgv.Columns(1).Width = 100
                dgv.Columns(2).Width = 100
                dgv.Columns(3).Width = 100
                dgv.Columns(4).Width = 600
                dgv.Columns(5).Width = 150
                dgv.Columns(6).Width = 150
            End If
        End If
    End Sub

    Private Sub FrmGridView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cursor = Cursors.WaitCursor
        If ClsGV.GridOption = 0 Then
            lbltitle.Text = "Audit Trail for Payment " & ClsGV.ID
            ClsIMS.RefreshGridAuditTrail(ClsGV.GridOption, dgvView, "Where A_AuditTransactionID = " & ClsGV.ID)
        ElseIf ClsGV.GridOption = 1 Then
            lbltitle.Text = "Audit Trail for Management Fees Breakdown " & ClsGV.ID
            ClsIMS.RefreshGridAuditTrail(ClsGV.GridOption, dgvView, "'" & ClsGV.Date1.ToString("yyyy-MM-dd") & "','" & ClsGV.Date2.ToString("yyyy-MM-dd") & "'," & ClsGV.ID & "," & ClsGV.ID2)
        ElseIf ClsGV.GridOption = 2 Then
            lbltitle.Text = "Audit Trail for Custody Fees Breakdown " & ClsGV.ID
            ClsIMS.RefreshGridAuditTrail(ClsGV.GridOption, dgvView, "'" & ClsGV.Date1.ToString("yyyy-MM-dd") & "','" & ClsGV.Date2.ToString("yyyy-MM-dd") & "'," & ClsGV.ID & "," & ClsGV.ID2)
        ElseIf ClsGV.GridOption = 3 Then
            lbltitle.Text = "Fees Payment Audit Trail " & ClsGV.ID2
            ClsIMS.RefreshGridAuditTrail(ClsGV.GridOption, dgvView, "Where Fee_PortfolioCode = " & ClsGV.ID)
        ElseIf ClsGV.GridOption = 4 Then
            lbltitle.Text = "Incoming Funds Audit Trail " & ClsGV.ID
            ClsIMS.RefreshGridAuditTrail(ClsGV.GridOption, dgvView, CStr(ClsGV.ID))
        End If
        FormatAuditTrailGrid(dgvView)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvView, 0)
        Cursor = Cursors.Default
    End Sub
End Class