﻿Public Class FrmFeesPaidOption
    Private Sub CreateFeeFXMovement()
        Dim varID As Double = 0

        ClsPay = New ClsPayment
        ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
        ClsPay.OrderMessageTypeID = 78
        If ClsFP.PortfolioCodeRedirect <> 0 Then
            ClsPay.PortfolioCode = ClsFP.PortfolioCodeRedirect
            ClsPay.Portfolio = ClsFP.PortfolioNameRedirect
        Else
            ClsPay.PortfolioCode = ClsFP.PortfolioCode
            ClsPay.Portfolio = ClsFP.PortfolioName
        End If
        ClsPay.CCYCode = ClsIMS.GetSQLItem("Select cr_code from vwdolfinpaymentcurrencies where cr_name1 = '" & ClsFP.CCYName & "'")
        Dim Lst As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select * from DolfinPaymentGetPortfolioFeeAccountCodeFXDetails(" & ClsPay.PortfolioCode & "," & ClsPay.CCYCode & "," & ClsFP.LastPaymentAmount & ") order by OrderCol")
        If Not Lst Is Nothing Then
            ClsPay.ExchangeRate = Lst.Item("ExchangeRate").ToString
            ClsPay.Amount = CDbl(ClsFP.LastPaymentAmount * ClsPay.ExchangeRate).ToString("0.00")
            ClsPay.OrderAccountCode = Lst.Item("t_code").ToString
            ClsPay.BeneficiaryPortfolioCode = IIf(ClsIMS.UserLocationCode = 47, 757, 120)
            ClsPay.BeneficiaryAccountCode = ClsIMS.GetPortfolioFeeRMSAccountCode(Lst.Item("cr_code").ToString)
            ClsPay.BeneficiaryCCYCode = Lst.Item("cr_code").ToString
            ClsPay.Comments = ClsFP.TypeName & " from " & ClsPay.Portfolio & " to Britannia Global Markets Ltd for " & ClsPay.Amount & " " & Lst.Item("cr_name1").ToString & " (FX - " & ClsFP.CCYName & "/" & Lst.Item("cr_name1").ToString & " @ " & ClsPay.ExchangeRate & ")"
            ClsPay.OtherID = IIf(ClsFP.Type = 0, cPayDescInOtherManagementFeeFX, cPayDescInOtherCustodyFeeFX)
            ClsPay.IMSRef = ClsFP.CCYName & " " & ClsFP.LastPaymentAmount
            ClsPay.CCYCode = Lst.Item("cr_code").ToString

            ClsPay.GetTempDirectory(ClsIMS.GetFilePath("Default"))

            ClsIMS.FullUserName = ClsIMS.FullUserName & " - CreateFeeFXMovement"
            ClsIMS.UpdatePaymentTable(1, varID)
            ClsIMS.AddFeeHistoryPayments(ClsFP.ID, varID, ClsPay.Amount, "FX")
            ClsIMS.FullUserName = Environment.UserDomainName & "\" & Environment.UserName
            ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "update dolfinpaymentfeehistory set fee_paymentid = " & varID & " where fee_id = " & ClsFP.ID)
            ClsPay.ConfirmDirectory()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "Added clear owed fees movement for portfolio: " & ClsPay.Portfolio & " - CreateOwedFeesMovement", varID)
            MsgBox("fees fx movements has been booked - id: " & varID & ". please check movements form for authorisation.", vbInformation, "Movement booked")
        Else
            MsgBox("An alternative currency was not found in order to FX the payment. Please check balances for this portfolio", vbExclamation, "No alternative balance found")
        End If
    End Sub

    Private Sub CreateOwedFeesMovement(ByVal varAmount As Double)
        Dim varID As Double = 0

        ClsPay = New ClsPayment
        ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)
        ClsPay.OrderMessageTypeID = 78
        ClsPay.PortfolioCode = ClsFP.PortfolioCode
        ClsPay.Portfolio = ClsFP.PortfolioName
        ClsPay.Amount = varAmount
        ClsPay.CCYCode = ClsFP.CCYCode
        ClsPay.OrderAccountCode = ClsIMS.GetPortfolioFeeAccountCode(ClsPay.PortfolioCode, ClsPay.CCYCode)
        ClsPay.BeneficiaryPortfolioCode = IIf(ClsIMS.UserLocationCode = 47, 757, 120)
        ClsPay.BeneficiaryAccountCode = ClsIMS.GetPortfolioFeeRMSAccountCode(ClsPay.CCYCode)
        ClsPay.BeneficiaryCCYCode = ClsFP.CCYCode
        ClsPay.Comments = ClsFP.TypeName & " (Clear Client Owed Fees) from " & ClsPay.Portfolio & " to Britannia Global Markets Ltd for " & varAmount & " " & ClsFP.CCYName
        ClsPay.OtherID = IIf(ClsFP.Type = 0, cPayDescInOtherManagementFeeClearClientOwedFees, cPayDescInOtherCustodyFeeClearClientOwedFees)
        ClsPay.IMSRef = ClsFP.Reference

        ClsPay.GetTempDirectory(ClsIMS.GetFilePath("Default"))

        ClsIMS.UpdatePaymentTable(1, varID)
        ClsIMS.AddFeeHistoryPayments(ClsFP.ID, varID, varAmount, "Client Owed Fees")
        ClsPay.ConfirmDirectory()
        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "Added clear owed fees movement for portfolio: " & ClsPay.Portfolio & " - CreateOwedFeesMovement", varID)
        MsgBox("Clear owed fees movement has been booked - id: " & varID & ". please check movements form for authorisation.", vbInformation, "Movement booked")
    End Sub


    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        If Rb1.Checked Then
            ClsGV.GridOption = ClsFP.Type + 1
            ClsGV.ID = ClsFP.PortfolioCode
            ClsGV.ID2 = ClsFP.CCYCode
            ClsGV.Date1 = ClsFP.StartDate
            ClsGV.Date2 = ClsFP.EndDate
            Dim FrmBreakdownGridView As New FrmGridView
            FrmBreakdownGridView.ShowDialog()
        ElseIf Rb2.Checked Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, "U_PayFeesPaid") Then
                Dim varAmount As Double = InputBox("Please enter amount to clear client owed fee for " & ClsFP.PortfolioName, "Clear Client Owed Fees")
                If IsNumeric(varAmount) Then
                    CreateOwedFeesMovement(varAmount)
                Else
                    MsgBox("A valid numerical amount is required to clear the fee", vbCritical, "Valid Amount required")
                End If
            Else
                MsgBox("You do not have permission to pay fees. Please contact systems support for permissions", vbCritical, "Not authorised")
            End If
        ElseIf Rb3.Checked Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, "U_PayFeesPaid") Then
                CreateFeeFXMovement()
            Else
                MsgBox("You do not have permission to pay fees. Please contact systems support for permissions", vbCritical, "Not authorised")
            End If
        ElseIf Rb4.Checked Then
            ClsGV.GridOption = 3
            ClsGV.ID = ClsFP.PortfolioCode
            ClsGV.ID2 = ClsFP.CCYCode
            ClsGV.Date1 = ClsFP.StartDate
            ClsGV.Date2 = ClsFP.EndDate
            Dim FrmFeePaymentsGridView As New FrmGridView
            FrmFeePaymentsGridView.ShowDialog()
        End If
    End Sub

    Private Sub FrmFeesPaidOption_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Rb1.Checked = True
        If ClsFP.Reference = "CRM" Then
            Rb2.Enabled = False
            Rb3.Enabled = False
        ElseIf InStr(1, ClsFP.AccountName, "Client Owed", vbTextCompare) = 0 Then
            Rb2.Enabled = False
        End If
        txtFPOPortfolio.Text = ClsFP.PortfolioName
        txtFPOCCY.Text = ClsFP.CCYName
        txtFPOID.Text = ClsFP.ID
        txtFPOType.Text = ClsFP.TypeName
        txtFPOStartDate.Text = ClsFP.StartDate.ToString("dd-MMM-yyyy")
        txtFPOSEndDate.Text = ClsFP.EndDate.ToString("dd-MMM-yyyy")
        txtFPOref.Text = ClsFP.Reference
        txtFPOAccount.Text = ClsFP.AccountName
        txtFPOAmt.Text = ClsFP.LastPaymentAmount
        txtFPODestination.Text = ClsFP.Destination
        txtFPOPortfolioRedirect.Text = ClsFP.PortfolioNameRedirect
    End Sub
End Class