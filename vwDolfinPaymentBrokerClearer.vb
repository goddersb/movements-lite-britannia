'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class vwDolfinPaymentBrokerClearer
    Public Property BrokerCode As Integer
    Public Property BrokerName As String
    Public Property BrokerSwiftAddress As String
    Public Property BrokerAccountNo As String
    Public Property ClearerCode As Integer
    Public Property ClearerName As String
    Public Property ClearerSwiftAddress As String
    Public Property ClearerNo As String

End Class
