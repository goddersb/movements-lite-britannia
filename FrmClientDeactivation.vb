﻿Imports System.Data.SqlClient

Public Class FrmClientDeactivation

    Private Sub LoadWatchList(CustomerId As Integer)
        'TODO: Gridview
        Dim SqlString As String = ""
        Dim ds As DataSet
        ' Dim dgvView As DataView

        Me.dgvWatchList.DataSource = Nothing
        Me.dgvWatchList.Rows.Clear()
        Me.dgvWatchList.Columns.Clear()

        SqlString = "EXEC dbo.DolfinPaymentGetComplianceWatchList '" + Format(dtpReportDate.Value, "yyyy-MM-dd").ToString + "',  " + CustomerId.ToString + ", '1'" '+ IIf(IsAllRequired = True, 1, 0).ToString
        Try
            ds = ClsIMS.GetTransactionData(SqlString)
            With Me.dgvWatchList
                .AutoGenerateColumns = True
                .DataSource = ds.Tables(0)
                .CurrentCell = Nothing
                dgvWatchList.Columns("SortID").Visible = False
                With .Columns("CustCode")
                    .Width = 80
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    .HeaderText = "Client Code"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                End With
                With .Columns("ClientName")
                    .Width = 200
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    .HeaderText = "Client Name"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                End With
                With .Columns("AUM")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .HeaderText = "AUM"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                End With
                With .Columns("CustRecEnteredBy")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                    .HeaderText = "Entered By"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                End With
                With .Columns("CustRecCreateDate")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .HeaderText = "Date Entered"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("NoOfDays")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    .HeaderText = "No. Of Days"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
                End With
                With .Columns("IsCustomerActive")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .HeaderText = "Is Active"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("IsComplianceApproved")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .HeaderText = "Is Approved"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("Is Externally Approved")
                    .ReadOnly = True
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .HeaderText = "Is Externally Approved"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("ComplianceAppBy")
                    .ReadOnly = True
                    .HeaderText = "Approved By"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft
                End With
                With .Columns("HasOwnKyc")
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .ReadOnly = True
                    .HeaderText = "Has KYC"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("HasKyc")
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .ReadOnly = True
                    .HeaderText = "Has Related KYC"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
                With .Columns("HasTransaction")
                    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .ReadOnly = True
                    .HeaderText = "Has Transaction"
                    .HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                End With
            End With
            ' Just make the DGV autosize
            dgvWatchList.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            'If IsAllRequired Then
            '    cmdUpdate.Enabled = False
            'Else
            '    cmdUpdate.Enabled = True
            'End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            ds = Nothing
        End Try
    End Sub

    Private Sub cmbClientList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClientList.SelectedIndexChanged
        Dim ClientStatus As String = cboClientList.Text
        Dim CustID As Integer
        If Not (cboClientList.SelectedItem Is Nothing) Then
            CustID = cboClientList.SelectedItem(0).ToString

            If ClientStatus <> "" Then
                ClientStatus = Trim(Mid(ClientStatus, InStrRev(ClientStatus, "-", -1) + 1, ClientStatus.Length))
            End If

            If ClientStatus = "Active" Then
                cmdUpdate.Text = "Deactivate"
                cmdUpdate.Enabled = True
            ElseIf ClientStatus = "Inactive" Then
                cmdUpdate.Text = "Activate"
                cmdUpdate.Enabled = True
            End If

            LoadWatchList(CustID)

            If CustID > 0 And dgvWatchList.RowCount = 1 Then
                If dgvWatchList.Rows(0).Cells("IsCustomerActive").Value = "N" And dgvWatchList.Rows(0).Cells("IsComplianceApproved").Value = "N" And dgvWatchList.Rows(0).Cells("Is Externally Approved").Value = "N" Then
                    cmdUpdate.Enabled = False
                End If
                If dgvWatchList.Rows(0).Cells("IsCustomerActive").Value = "N" And dgvWatchList.Rows(0).Cells("Is Externally Approved").Value = "N" Then
                    cmdExtApprove.Text = "External Approve"
                    cmdExtApprove.Enabled = True
                ElseIf dgvWatchList.Rows(0).Cells("Is Externally Approved").Value = "N" Then
                    cmdExtApprove.Text = "External Approve"
                    cmdExtApprove.Enabled = True
                ElseIf dgvWatchList.Rows(0).Cells("Is Externally Approved").Value = "Y" Then
                    cmdExtApprove.Text = "External Un-Approve"
                    cmdExtApprove.Enabled = True
                End If
            End If
        End If
    End Sub

    Private Sub cmdUpdate_Click(sender As Object, e As EventArgs) Handles cmdUpdate.Click
        Dim CustID As Integer
        CustID = cboClientList.SelectedItem(0).ToString
        If cmdUpdate.Text = "Deactivate" Then
            'TODO: Deactivate user And pass CustomerID to LoadWatchList so it sits right at the top
            ClsIMS.EnterClientStatus(CustID, "I")
            PopulateClientList()
            LoadWatchList(CustID)
            MessageBox.Show("Customer set to inactive!!")
        ElseIf cmdUpdate.Text = "Activate" Then
            ClsIMS.EnterClientStatus(CustID, "A")
            PopulateClientList()
            LoadWatchList(0)
            MessageBox.Show("Customer set to active!!")
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        chkIsAllClient.Checked = False
        ClearForm()
        LoadWatchList(0)
    End Sub

    Private Sub FrmClientDeactivation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClearForm()
        PopulateClientList()
        LoadWatchList(0)
    End Sub
    Private Sub PopulateClientList()
        ClsIMS.PopulateComboboxWithData(cboClientList, GetClientListQuery)
        cboClientList.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboClientList.AutoCompleteSource = AutoCompleteSource.ListItems
        cboClientList.SelectedIndex = 0
        cmdUpdate.Text = ""
        cmdExtApprove.Text = ""
        cmdUpdate.Enabled = False
        cmdExtApprove.Enabled = False
    End Sub

    Private Sub ClearForm()
        With Me
            .dtpReportDate.Value = IIf(chkIsAllClient.Checked = False, IIf(DatePart(DateInterval.Year, Now()) = 2019, CDate("2019-01-01"), DateAdd(DateInterval.Day, -180, Now())), CDate("2012-01-01"))
            .dgvWatchList.DataSource = Nothing
            .dgvWatchList.Rows.Clear()
            .dgvWatchList.Columns.Clear()
            .cmdUpdate.Enabled = False
            .cmdExtApprove.Enabled = False
            .cmdUpdate.Text = ""
            .cmdExtApprove.Text = ""
            If chkIsAllClient.Checked = False Then
                .grpComplianceDetail.Text = "Compliance Inactive Clients (last 180 days)"
            Else
                .grpComplianceDetail.Text = "Compliance Details"
            End If
        End With

    End Sub


    Private Function GetClientListQuery() As String
        Dim SqlString As String
        SqlString = "SELECT CustId = -1, FullNameWithCodeAndStatus = ''
                    UNION ALL SELECT Cust_ID, FullNameWithCodeAndStatus FROM dbo.vwDolfinPaymentClientList WHERE 1 = 1 /* AND FullName NOT LIKE '%Dolfin Sample%' */ AND Cust_ID NOT IN (185, 227, 134, 276, 362, 524, 745) AND CreationDate >= '" + IIf(chkIsAllClient.Checked = False, Format(dtpReportDate.Value, "yyyy-MM-dd"), "2012-01-01") + "' ORDER BY FullNameWithCodeAndStatus"
        GetClientListQuery = SqlString
    End Function

    Private Sub chkIsAllClient_CheckedChanged(sender As Object, e As EventArgs) Handles chkIsAllClient.CheckedChanged
        ClearForm()
        PopulateClientList()
    End Sub

    Private Sub cmdExtApprove_Click(sender As Object, e As EventArgs) Handles cmdExtApprove.Click
        Dim CustID As Integer
        CustID = cboClientList.SelectedItem(0).ToString
        If cmdExtApprove.Text = "External Un-Approve" Then
            'TODO: Deactivate user And pass CustomerID to LoadWatchList so it sits right at the top
            ClsIMS.EnterClientStatus(CustID, "EI")
            PopulateClientList()
            LoadWatchList(CustID)
            MessageBox.Show("Customer set to external un-approve!!")
        ElseIf cmdExtApprove.Text = "External Approve" Then
            ClsIMS.EnterClientStatus(CustID, "EA")
            PopulateClientList()
            LoadWatchList(0)
            MessageBox.Show("Customer set to external approve!!")
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvWatchList, 0)
        Cursor = Cursors.Default
    End Sub
End Class