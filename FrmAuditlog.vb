﻿Public Class FrmAuditlog
    Dim AuditConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet

    Private Sub AuditLoadGrid(Optional ByVal varWhereClauseFilter As String = "")
        Dim varSQL As String
        Try
            Cursor = Cursors.WaitCursor
            If varWhereClauseFilter = "" Then
                varSQL = "select top 1000 * from vwDolfinPaymentAuditLog order by A_AuditDateTime Desc"
            Else
                varSQL = "select top 1000 * from vwDolfinPaymentAuditLog " & varWhereClauseFilter & " order by A_AuditDateTime Desc"
            End If

            dgvAudit.DataSource = Nothing
            AuditConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, AuditConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentAuditLog")
            dgvAudit.AutoGenerateColumns = True
            dgvAudit.DataSource = ds.Tables("vwDolfinPaymentAuditLog")

            dgvAudit.Columns(0).Width = 100
            dgvAudit.Columns(1).Width = 100
            dgvAudit.Columns(2).Width = 100
            dgvAudit.Columns(3).Width = 100
            dgvAudit.Columns(4).Width = 600
            dgvAudit.Columns(5).Width = 150
            dgvAudit.Columns(6).Width = 150

            dgvAudit.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvAudit.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvAudit.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameAudit, Err.Description & " - Problem with viewing audit grid")
        End Try
    End Sub

    Private Sub FrmAuditlog_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If AuditConn.State = ConnectionState.Open Then
            AuditConn.Close()
        End If
    End Sub

    Private Sub FrmAuditlog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(cboAuditID, "Select top 100 0,A_AuditTransactionID from vwDolfinPaymentAuditLog group by A_AuditTransactionID order by A_AuditTransactionID desc")
        ClsIMS.PopulateComboboxWithData(cboAuditStatus, "Select top 100 0,A_AuditStatus from vwDolfinPaymentAuditLog group by A_AuditStatus order by A_AuditStatus desc")
        ClsIMS.PopulateComboboxWithData(cboAuditGroup, "Select top 100 0,A_AuditGroupName from vwDolfinPaymentAuditLog group by A_AuditGroupName order by A_AuditGroupName desc")
        ClsIMS.PopulateComboboxWithData(CboAuditUser, "Select top 100 0,A_AuditLoggedUser from vwDolfinPaymentAuditLog group by A_AuditLoggedUser order by A_AuditLoggedUser desc")
        ClsIMS.PopulateComboboxWithData(CboAuditFromDate, "Select top 100 0,cast(A_AuditDateTime as date) from vwDolfinPaymentAuditLog group by cast(A_AuditDateTime as date) order by cast(A_AuditDateTime as date) desc")
        ClsIMS.PopulateComboboxWithData(CboAuditToDate, "Select top 100 0,cast(A_AuditDateTime as date) from vwDolfinPaymentAuditLog group by cast(A_AuditDateTime as date) order by cast(A_AuditDateTime as date) desc")
        AuditLoadGrid()
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varWhereClauseFilter As String = "where"

        If cboAuditID.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and A_AuditTransactionID in ('" & cboAuditID.Text & "')"
        End If

        If cboAuditStatus.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and A_AuditStatus in ('" & cboAuditStatus.Text & "')"
        End If

        If cboAuditGroup.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and A_AuditGroupName in ('" & cboAuditGroup.Text & "')"
        End If

        If CboAuditUser.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and A_AuditLoggedUser in ('" & CboAuditUser.Text & "')"
        End If

        If CboAuditFromDate.Text <> "" Or CboAuditToDate.Text <> "" Then
            If CboAuditFromDate.Text <> "" And CboAuditToDate.Text = "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and cast(A_AuditDatetime as date) between cast('" & CDate(CboAuditFromDate.Text).ToString("yyyy-MM-dd") & "' as date) and cast(getdate() as date)"
            ElseIf CboAuditFromDate.Text = "" And CboAuditToDate.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and cast(A_AuditDatetime as date) between cast('2016-01-01' as date) and cast('" & CDate(CboAuditToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
            ElseIf CboAuditFromDate.Text <> "" And CboAuditToDate.Text <> "" Then
                If CDate(CboAuditFromDate.Text) <= CDate(CboAuditToDate.Text) Then
                    varWhereClauseFilter = varWhereClauseFilter & " and cast(A_AuditDatetime as date) between cast('" & CDate(CboAuditFromDate.Text).ToString("yyyy-MM-dd") & "' as date) and cast('" & CDate(CboAuditToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                End If
            End If
        End If

        If varWhereClauseFilter <> "where" Then
            AuditLoadGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            AuditLoadGrid()
        End If
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        AuditLoadGrid()
        cboAuditStatus.Text = ""
        cboAuditGroup.Text = ""
        CboAuditUser.Text = ""
        CboAuditFromDate.Text = ""
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvAudit, 0)
        Cursor = Cursors.Default
    End Sub
End Class