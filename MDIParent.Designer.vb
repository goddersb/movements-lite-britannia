﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MDIParent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIParent))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaymentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReceiveDeliverFreeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternalOmnibusSwitchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutomaticSwiftsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TreasuryManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatchProcessingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintPreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnalysisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewBalancesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReadSwiftToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfirmExternalReceiptsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FeesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FeesPaidToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IMSReconciliationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BritanniaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CorporateActionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CashTransactionCommentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SwiftAccountStatementsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CutOffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransferFeesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TemplatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SwiftCodesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntermediaryCodesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RiskAssessmentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComplianceWatchlistToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CASSAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GPPMappingCodesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunClientReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReleaseIMSDocNumbersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientEmailsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientStatementGeneratorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasswordGeneratorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuditLogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserDetailsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SystemSettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WindowsMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TechnicalGuideToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewBalancesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReadSwiftToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfirmExternalReceiptsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CutOffTimesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TransferFeesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewBalancesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TemplateManagementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SwiftCodesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntermediaryCodesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FeesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton11 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton19 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton9 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton22 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton13 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton14 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton18 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton20 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton26 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton12 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton16 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton21 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonInt = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButtonFee = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton10 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton15 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton23 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton24 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton28 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton25 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton27 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton17 = New System.Windows.Forms.ToolStripButton()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.TimerInActivity = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.ToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.BackColor = System.Drawing.Color.White
        Me.MenuStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenu, Me.EditMenu, Me.ViewMenu, Me.AnalysisToolStripMenuItem, Me.ToolsMenu, Me.ClientsToolStripMenuItem, Me.AdminToolStripMenuItem, Me.WindowsMenu, Me.HelpMenu})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.WindowsMenu
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip.Size = New System.Drawing.Size(1187, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.ToolStripSeparator3, Me.ToolStripSeparator4, Me.PrintToolStripMenuItem, Me.PrintPreviewToolStripMenuItem, Me.PrintSetupToolStripMenuItem, Me.ToolStripSeparator5, Me.ExitToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(37, 20)
        Me.FileMenu.Text = "&File"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaymentToolStripMenuItem, Me.ReceiveDeliverFreeToolStripMenuItem, Me.InternalOmnibusSwitchToolStripMenuItem, Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem, Me.AutomaticSwiftsToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.TreasuryManagementToolStripMenuItem, Me.BatchProcessingToolStripMenuItem})
        Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.NewToolStripMenuItem.Text = "&Movements"
        '
        'PaymentToolStripMenuItem
        '
        Me.PaymentToolStripMenuItem.Image = CType(resources.GetObject("PaymentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PaymentToolStripMenuItem.Name = "PaymentToolStripMenuItem"
        Me.PaymentToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.PaymentToolStripMenuItem.Text = "&Add Movement"
        '
        'ReceiveDeliverFreeToolStripMenuItem
        '
        Me.ReceiveDeliverFreeToolStripMenuItem.Image = CType(resources.GetObject("ReceiveDeliverFreeToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ReceiveDeliverFreeToolStripMenuItem.Name = "ReceiveDeliverFreeToolStripMenuItem"
        Me.ReceiveDeliverFreeToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.ReceiveDeliverFreeToolStripMenuItem.Text = "Receive/Deliver Free"
        '
        'InternalOmnibusSwitchToolStripMenuItem
        '
        Me.InternalOmnibusSwitchToolStripMenuItem.Image = CType(resources.GetObject("InternalOmnibusSwitchToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InternalOmnibusSwitchToolStripMenuItem.Name = "InternalOmnibusSwitchToolStripMenuItem"
        Me.InternalOmnibusSwitchToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.InternalOmnibusSwitchToolStripMenuItem.Text = "&Internal Omnibus Switch"
        '
        'InternalOmnibusSwitchTermDepositsToolStripMenuItem
        '
        Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem.Image = CType(resources.GetObject("InternalOmnibusSwitchTermDepositsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem.Name = "InternalOmnibusSwitchTermDepositsToolStripMenuItem"
        Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.InternalOmnibusSwitchTermDepositsToolStripMenuItem.Text = "Internal Omnibus Switch (Term Deposits)"
        '
        'AutomaticSwiftsToolStripMenuItem
        '
        Me.AutomaticSwiftsToolStripMenuItem.Image = CType(resources.GetObject("AutomaticSwiftsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AutomaticSwiftsToolStripMenuItem.Name = "AutomaticSwiftsToolStripMenuItem"
        Me.AutomaticSwiftsToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.AutomaticSwiftsToolStripMenuItem.Text = "&Automatic Swifts"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Image = CType(resources.GetObject("OptionsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.OptionsToolStripMenuItem.Text = "&View Movements"
        '
        'TreasuryManagementToolStripMenuItem
        '
        Me.TreasuryManagementToolStripMenuItem.Image = CType(resources.GetObject("TreasuryManagementToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TreasuryManagementToolStripMenuItem.Name = "TreasuryManagementToolStripMenuItem"
        Me.TreasuryManagementToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.TreasuryManagementToolStripMenuItem.Text = "&Treasury Management"
        '
        'BatchProcessingToolStripMenuItem
        '
        Me.BatchProcessingToolStripMenuItem.Image = CType(resources.GetObject("BatchProcessingToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BatchProcessingToolStripMenuItem.Name = "BatchProcessingToolStripMenuItem"
        Me.BatchProcessingToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.BatchProcessingToolStripMenuItem.Text = "Batch Processing"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(177, 6)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(177, 6)
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Image = CType(resources.GetObject("PrintToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PrintToolStripMenuItem.Text = "&Print"
        '
        'PrintPreviewToolStripMenuItem
        '
        Me.PrintPreviewToolStripMenuItem.Image = CType(resources.GetObject("PrintPreviewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintPreviewToolStripMenuItem.Name = "PrintPreviewToolStripMenuItem"
        Me.PrintPreviewToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PrintPreviewToolStripMenuItem.Text = "Print Pre&view"
        '
        'PrintSetupToolStripMenuItem
        '
        Me.PrintSetupToolStripMenuItem.Name = "PrintSetupToolStripMenuItem"
        Me.PrintSetupToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.PrintSetupToolStripMenuItem.Text = "Print Setup"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(177, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'EditMenu
        '
        Me.EditMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UndoToolStripMenuItem, Me.RedoToolStripMenuItem, Me.ToolStripSeparator6, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.ToolStripSeparator7, Me.SelectAllToolStripMenuItem})
        Me.EditMenu.Name = "EditMenu"
        Me.EditMenu.Size = New System.Drawing.Size(39, 20)
        Me.EditMenu.Text = "&Edit"
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Image = CType(resources.GetObject("UndoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UndoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.UndoToolStripMenuItem.Text = "&Undo"
        '
        'RedoToolStripMenuItem
        '
        Me.RedoToolStripMenuItem.Image = CType(resources.GetObject("RedoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RedoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.RedoToolStripMenuItem.Name = "RedoToolStripMenuItem"
        Me.RedoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RedoToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.RedoToolStripMenuItem.Text = "&Redo"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(161, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Image = CType(resources.GetObject("CutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CutToolStripMenuItem.Text = "Cu&t"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.CopyToolStripMenuItem.Text = "&Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.PasteToolStripMenuItem.Text = "&Paste"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(161, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select &All"
        '
        'ViewMenu
        '
        Me.ViewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolBarToolStripMenuItem, Me.StatusBarToolStripMenuItem})
        Me.ViewMenu.Name = "ViewMenu"
        Me.ViewMenu.Size = New System.Drawing.Size(44, 20)
        Me.ViewMenu.Text = "&View"
        '
        'ToolBarToolStripMenuItem
        '
        Me.ToolBarToolStripMenuItem.Checked = True
        Me.ToolBarToolStripMenuItem.CheckOnClick = True
        Me.ToolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToolBarToolStripMenuItem.Name = "ToolBarToolStripMenuItem"
        Me.ToolBarToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.ToolBarToolStripMenuItem.Text = "&Toolbar"
        '
        'StatusBarToolStripMenuItem
        '
        Me.StatusBarToolStripMenuItem.Checked = True
        Me.StatusBarToolStripMenuItem.CheckOnClick = True
        Me.StatusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StatusBarToolStripMenuItem.Name = "StatusBarToolStripMenuItem"
        Me.StatusBarToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.StatusBarToolStripMenuItem.Text = "&Status Bar"
        '
        'AnalysisToolStripMenuItem
        '
        Me.AnalysisToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewBalancesToolStripMenuItem2, Me.ReadSwiftToolStripMenuItem1, Me.ConfirmExternalReceiptsToolStripMenuItem1, Me.FeesToolStripMenuItem2, Me.FeesPaidToolStripMenuItem, Me.IMSReconciliationToolStripMenuItem, Me.BritanniaToolStripMenuItem, Me.CorporateActionsToolStripMenuItem, Me.CashTransactionCommentsToolStripMenuItem, Me.SwiftAccountStatementsToolStripMenuItem})
        Me.AnalysisToolStripMenuItem.Name = "AnalysisToolStripMenuItem"
        Me.AnalysisToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.AnalysisToolStripMenuItem.Text = "&Analysis"
        '
        'ViewBalancesToolStripMenuItem2
        '
        Me.ViewBalancesToolStripMenuItem2.Image = CType(resources.GetObject("ViewBalancesToolStripMenuItem2.Image"), System.Drawing.Image)
        Me.ViewBalancesToolStripMenuItem2.Name = "ViewBalancesToolStripMenuItem2"
        Me.ViewBalancesToolStripMenuItem2.Size = New System.Drawing.Size(270, 22)
        Me.ViewBalancesToolStripMenuItem2.Text = "&View Balances"
        '
        'ReadSwiftToolStripMenuItem1
        '
        Me.ReadSwiftToolStripMenuItem1.Image = CType(resources.GetObject("ReadSwiftToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ReadSwiftToolStripMenuItem1.Name = "ReadSwiftToolStripMenuItem1"
        Me.ReadSwiftToolStripMenuItem1.Size = New System.Drawing.Size(270, 22)
        Me.ReadSwiftToolStripMenuItem1.Text = "&Read Swift"
        '
        'ConfirmExternalReceiptsToolStripMenuItem1
        '
        Me.ConfirmExternalReceiptsToolStripMenuItem1.Image = CType(resources.GetObject("ConfirmExternalReceiptsToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ConfirmExternalReceiptsToolStripMenuItem1.Name = "ConfirmExternalReceiptsToolStripMenuItem1"
        Me.ConfirmExternalReceiptsToolStripMenuItem1.Size = New System.Drawing.Size(270, 22)
        Me.ConfirmExternalReceiptsToolStripMenuItem1.Text = "Payments Requests/External Receipts"
        '
        'FeesToolStripMenuItem2
        '
        Me.FeesToolStripMenuItem2.Image = CType(resources.GetObject("FeesToolStripMenuItem2.Image"), System.Drawing.Image)
        Me.FeesToolStripMenuItem2.Name = "FeesToolStripMenuItem2"
        Me.FeesToolStripMenuItem2.Size = New System.Drawing.Size(270, 22)
        Me.FeesToolStripMenuItem2.Text = "&Fees"
        '
        'FeesPaidToolStripMenuItem
        '
        Me.FeesPaidToolStripMenuItem.Image = CType(resources.GetObject("FeesPaidToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FeesPaidToolStripMenuItem.Name = "FeesPaidToolStripMenuItem"
        Me.FeesPaidToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.FeesPaidToolStripMenuItem.Text = "Fees Paid"
        '
        'IMSReconciliationToolStripMenuItem
        '
        Me.IMSReconciliationToolStripMenuItem.Image = CType(resources.GetObject("IMSReconciliationToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IMSReconciliationToolStripMenuItem.Name = "IMSReconciliationToolStripMenuItem"
        Me.IMSReconciliationToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.IMSReconciliationToolStripMenuItem.Text = "&IMS Reconciliation"
        '
        'BritanniaToolStripMenuItem
        '
        Me.BritanniaToolStripMenuItem.Image = CType(resources.GetObject("BritanniaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.BritanniaToolStripMenuItem.Name = "BritanniaToolStripMenuItem"
        Me.BritanniaToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.BritanniaToolStripMenuItem.Text = "Britannia Reconciliation"
        '
        'CorporateActionsToolStripMenuItem
        '
        Me.CorporateActionsToolStripMenuItem.Image = CType(resources.GetObject("CorporateActionsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CorporateActionsToolStripMenuItem.Name = "CorporateActionsToolStripMenuItem"
        Me.CorporateActionsToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.CorporateActionsToolStripMenuItem.Text = "Corporate Actions"
        '
        'CashTransactionCommentsToolStripMenuItem
        '
        Me.CashTransactionCommentsToolStripMenuItem.Image = CType(resources.GetObject("CashTransactionCommentsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CashTransactionCommentsToolStripMenuItem.Name = "CashTransactionCommentsToolStripMenuItem"
        Me.CashTransactionCommentsToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.CashTransactionCommentsToolStripMenuItem.Text = "Cash Transaction Comments"
        '
        'SwiftAccountStatementsToolStripMenuItem
        '
        Me.SwiftAccountStatementsToolStripMenuItem.Image = CType(resources.GetObject("SwiftAccountStatementsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SwiftAccountStatementsToolStripMenuItem.Name = "SwiftAccountStatementsToolStripMenuItem"
        Me.SwiftAccountStatementsToolStripMenuItem.Size = New System.Drawing.Size(270, 22)
        Me.SwiftAccountStatementsToolStripMenuItem.Text = "Swift Account Statements"
        '
        'ToolsMenu
        '
        Me.ToolsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CutOffToolStripMenuItem, Me.TransferFeesToolStripMenuItem1, Me.TemplatesToolStripMenuItem, Me.SwiftCodesToolStripMenuItem1, Me.IntermediaryCodesToolStripMenuItem1, Me.RiskAssessmentToolStripMenuItem, Me.ComplianceWatchlistToolStripMenuItem, Me.CASSAccountsToolStripMenuItem, Me.GPPMappingCodesToolStripMenuItem, Me.OptionsToolStripMenuItem2})
        Me.ToolsMenu.Name = "ToolsMenu"
        Me.ToolsMenu.Size = New System.Drawing.Size(88, 20)
        Me.ToolsMenu.Text = "&Maintenance"
        '
        'CutOffToolStripMenuItem
        '
        Me.CutOffToolStripMenuItem.Image = CType(resources.GetObject("CutOffToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutOffToolStripMenuItem.Name = "CutOffToolStripMenuItem"
        Me.CutOffToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.CutOffToolStripMenuItem.Text = "&Cut Off"
        '
        'TransferFeesToolStripMenuItem1
        '
        Me.TransferFeesToolStripMenuItem1.Image = CType(resources.GetObject("TransferFeesToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.TransferFeesToolStripMenuItem1.Name = "TransferFeesToolStripMenuItem1"
        Me.TransferFeesToolStripMenuItem1.Size = New System.Drawing.Size(190, 22)
        Me.TransferFeesToolStripMenuItem1.Text = "&Transfer Fees"
        '
        'TemplatesToolStripMenuItem
        '
        Me.TemplatesToolStripMenuItem.Image = CType(resources.GetObject("TemplatesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TemplatesToolStripMenuItem.Name = "TemplatesToolStripMenuItem"
        Me.TemplatesToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.TemplatesToolStripMenuItem.Text = "&Templates"
        '
        'SwiftCodesToolStripMenuItem1
        '
        Me.SwiftCodesToolStripMenuItem1.Image = CType(resources.GetObject("SwiftCodesToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.SwiftCodesToolStripMenuItem1.Name = "SwiftCodesToolStripMenuItem1"
        Me.SwiftCodesToolStripMenuItem1.Size = New System.Drawing.Size(190, 22)
        Me.SwiftCodesToolStripMenuItem1.Text = "&Swift Codes"
        '
        'IntermediaryCodesToolStripMenuItem1
        '
        Me.IntermediaryCodesToolStripMenuItem1.Image = CType(resources.GetObject("IntermediaryCodesToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.IntermediaryCodesToolStripMenuItem1.Name = "IntermediaryCodesToolStripMenuItem1"
        Me.IntermediaryCodesToolStripMenuItem1.Size = New System.Drawing.Size(190, 22)
        Me.IntermediaryCodesToolStripMenuItem1.Text = "Intermediary Codes"
        '
        'RiskAssessmentToolStripMenuItem
        '
        Me.RiskAssessmentToolStripMenuItem.Image = CType(resources.GetObject("RiskAssessmentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RiskAssessmentToolStripMenuItem.Name = "RiskAssessmentToolStripMenuItem"
        Me.RiskAssessmentToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.RiskAssessmentToolStripMenuItem.Text = "Risk Assessment"
        '
        'ComplianceWatchlistToolStripMenuItem
        '
        Me.ComplianceWatchlistToolStripMenuItem.Image = CType(resources.GetObject("ComplianceWatchlistToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ComplianceWatchlistToolStripMenuItem.Name = "ComplianceWatchlistToolStripMenuItem"
        Me.ComplianceWatchlistToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.ComplianceWatchlistToolStripMenuItem.Text = "Compliance Watchlist"
        '
        'CASSAccountsToolStripMenuItem
        '
        Me.CASSAccountsToolStripMenuItem.Image = CType(resources.GetObject("CASSAccountsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CASSAccountsToolStripMenuItem.Name = "CASSAccountsToolStripMenuItem"
        Me.CASSAccountsToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.CASSAccountsToolStripMenuItem.Text = "CASS Accounts"
        '
        'GPPMappingCodesToolStripMenuItem
        '
        Me.GPPMappingCodesToolStripMenuItem.Image = CType(resources.GetObject("GPPMappingCodesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.GPPMappingCodesToolStripMenuItem.Name = "GPPMappingCodesToolStripMenuItem"
        Me.GPPMappingCodesToolStripMenuItem.ShowShortcutKeys = False
        Me.GPPMappingCodesToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.GPPMappingCodesToolStripMenuItem.Text = "GPP Mapping Codes"
        '
        'OptionsToolStripMenuItem2
        '
        Me.OptionsToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RunClientReportsToolStripMenuItem, Me.ReleaseIMSDocNumbersToolStripMenuItem})
        Me.OptionsToolStripMenuItem2.Name = "OptionsToolStripMenuItem2"
        Me.OptionsToolStripMenuItem2.Size = New System.Drawing.Size(190, 22)
        Me.OptionsToolStripMenuItem2.Text = "&Options"
        '
        'RunClientReportsToolStripMenuItem
        '
        Me.RunClientReportsToolStripMenuItem.Name = "RunClientReportsToolStripMenuItem"
        Me.RunClientReportsToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.RunClientReportsToolStripMenuItem.Text = "Run Client Reports"
        '
        'ReleaseIMSDocNumbersToolStripMenuItem
        '
        Me.ReleaseIMSDocNumbersToolStripMenuItem.Name = "ReleaseIMSDocNumbersToolStripMenuItem"
        Me.ReleaseIMSDocNumbersToolStripMenuItem.Size = New System.Drawing.Size(210, 22)
        Me.ReleaseIMSDocNumbersToolStripMenuItem.Text = "Release IMS Doc numbers"
        '
        'ClientsToolStripMenuItem
        '
        Me.ClientsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientEmailsToolStripMenuItem, Me.ClientStatementGeneratorToolStripMenuItem, Me.PasswordGeneratorToolStripMenuItem})
        Me.ClientsToolStripMenuItem.Name = "ClientsToolStripMenuItem"
        Me.ClientsToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.ClientsToolStripMenuItem.Text = "&Clients"
        '
        'ClientEmailsToolStripMenuItem
        '
        Me.ClientEmailsToolStripMenuItem.Image = CType(resources.GetObject("ClientEmailsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientEmailsToolStripMenuItem.Name = "ClientEmailsToolStripMenuItem"
        Me.ClientEmailsToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.ClientEmailsToolStripMenuItem.Text = "Client Emails"
        '
        'ClientStatementGeneratorToolStripMenuItem
        '
        Me.ClientStatementGeneratorToolStripMenuItem.Image = CType(resources.GetObject("ClientStatementGeneratorToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientStatementGeneratorToolStripMenuItem.Name = "ClientStatementGeneratorToolStripMenuItem"
        Me.ClientStatementGeneratorToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.ClientStatementGeneratorToolStripMenuItem.Text = "Client Statement Generator"
        '
        'PasswordGeneratorToolStripMenuItem
        '
        Me.PasswordGeneratorToolStripMenuItem.Image = CType(resources.GetObject("PasswordGeneratorToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasswordGeneratorToolStripMenuItem.Name = "PasswordGeneratorToolStripMenuItem"
        Me.PasswordGeneratorToolStripMenuItem.Size = New System.Drawing.Size(217, 22)
        Me.PasswordGeneratorToolStripMenuItem.Text = "Client &Password Generator"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AuditLogToolStripMenuItem, Me.UserDetailsToolStripMenuItem1, Me.OptionsToolStripMenuItem1, Me.SystemSettingsToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "A&dmin"
        '
        'AuditLogToolStripMenuItem
        '
        Me.AuditLogToolStripMenuItem.Name = "AuditLogToolStripMenuItem"
        Me.AuditLogToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.AuditLogToolStripMenuItem.Text = "&Audit Log"
        '
        'UserDetailsToolStripMenuItem1
        '
        Me.UserDetailsToolStripMenuItem1.Name = "UserDetailsToolStripMenuItem1"
        Me.UserDetailsToolStripMenuItem1.Size = New System.Drawing.Size(157, 22)
        Me.UserDetailsToolStripMenuItem1.Text = "&User Details"
        '
        'OptionsToolStripMenuItem1
        '
        Me.OptionsToolStripMenuItem1.Name = "OptionsToolStripMenuItem1"
        Me.OptionsToolStripMenuItem1.Size = New System.Drawing.Size(157, 22)
        Me.OptionsToolStripMenuItem1.Text = "&User Options"
        '
        'SystemSettingsToolStripMenuItem
        '
        Me.SystemSettingsToolStripMenuItem.Name = "SystemSettingsToolStripMenuItem"
        Me.SystemSettingsToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.SystemSettingsToolStripMenuItem.Text = "&System Settings"
        '
        'WindowsMenu
        '
        Me.WindowsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CloseAllToolStripMenuItem})
        Me.WindowsMenu.Name = "WindowsMenu"
        Me.WindowsMenu.Size = New System.Drawing.Size(68, 20)
        Me.WindowsMenu.Text = "&Windows"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(120, 22)
        Me.CloseAllToolStripMenuItem.Text = "C&lose All"
        '
        'HelpMenu
        '
        Me.HelpMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IndexToolStripMenuItem, Me.TechnicalGuideToolStripMenuItem, Me.ToolStripSeparator8, Me.AboutToolStripMenuItem})
        Me.HelpMenu.Name = "HelpMenu"
        Me.HelpMenu.Size = New System.Drawing.Size(44, 20)
        Me.HelpMenu.Text = "&Help"
        '
        'IndexToolStripMenuItem
        '
        Me.IndexToolStripMenuItem.Image = CType(resources.GetObject("IndexToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IndexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.IndexToolStripMenuItem.Name = "IndexToolStripMenuItem"
        Me.IndexToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.IndexToolStripMenuItem.Text = "&User Guide"
        '
        'TechnicalGuideToolStripMenuItem
        '
        Me.TechnicalGuideToolStripMenuItem.Name = "TechnicalGuideToolStripMenuItem"
        Me.TechnicalGuideToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.TechnicalGuideToolStripMenuItem.Text = "Technical Guide"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(154, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(157, 22)
        Me.AboutToolStripMenuItem.Text = "&About ..."
        '
        'ViewBalancesToolStripMenuItem1
        '
        Me.ViewBalancesToolStripMenuItem1.Image = CType(resources.GetObject("ViewBalancesToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ViewBalancesToolStripMenuItem1.Name = "ViewBalancesToolStripMenuItem1"
        Me.ViewBalancesToolStripMenuItem1.Size = New System.Drawing.Size(209, 22)
        Me.ViewBalancesToolStripMenuItem1.Text = "View &Balances"
        '
        'ReadSwiftToolStripMenuItem
        '
        Me.ReadSwiftToolStripMenuItem.Image = CType(resources.GetObject("ReadSwiftToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ReadSwiftToolStripMenuItem.Name = "ReadSwiftToolStripMenuItem"
        Me.ReadSwiftToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.ReadSwiftToolStripMenuItem.Text = "&Read Swift"
        '
        'ConfirmExternalReceiptsToolStripMenuItem
        '
        Me.ConfirmExternalReceiptsToolStripMenuItem.Image = CType(resources.GetObject("ConfirmExternalReceiptsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConfirmExternalReceiptsToolStripMenuItem.Name = "ConfirmExternalReceiptsToolStripMenuItem"
        Me.ConfirmExternalReceiptsToolStripMenuItem.Size = New System.Drawing.Size(209, 22)
        Me.ConfirmExternalReceiptsToolStripMenuItem.Text = "&Confirm External Receipts"
        '
        'CutOffTimesToolStripMenuItem
        '
        Me.CutOffTimesToolStripMenuItem.Image = CType(resources.GetObject("CutOffTimesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutOffTimesToolStripMenuItem.Name = "CutOffTimesToolStripMenuItem"
        Me.CutOffTimesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.CutOffTimesToolStripMenuItem.Text = "&Cut Off Times"
        '
        'TransferFeesToolStripMenuItem
        '
        Me.TransferFeesToolStripMenuItem.Image = CType(resources.GetObject("TransferFeesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TransferFeesToolStripMenuItem.Name = "TransferFeesToolStripMenuItem"
        Me.TransferFeesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.TransferFeesToolStripMenuItem.Text = "&Transfer Fees"
        '
        'ViewBalancesToolStripMenuItem
        '
        Me.ViewBalancesToolStripMenuItem.Image = CType(resources.GetObject("ViewBalancesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ViewBalancesToolStripMenuItem.Name = "ViewBalancesToolStripMenuItem"
        Me.ViewBalancesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.ViewBalancesToolStripMenuItem.Text = "View &Balances"
        '
        'TemplateManagementToolStripMenuItem
        '
        Me.TemplateManagementToolStripMenuItem.Image = CType(resources.GetObject("TemplateManagementToolStripMenuItem.Image"), System.Drawing.Image)
        Me.TemplateManagementToolStripMenuItem.Name = "TemplateManagementToolStripMenuItem"
        Me.TemplateManagementToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.TemplateManagementToolStripMenuItem.Text = "Template &Management"
        '
        'SwiftCodesToolStripMenuItem
        '
        Me.SwiftCodesToolStripMenuItem.Image = CType(resources.GetObject("SwiftCodesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SwiftCodesToolStripMenuItem.Name = "SwiftCodesToolStripMenuItem"
        Me.SwiftCodesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SwiftCodesToolStripMenuItem.Text = "&SWIFT Codes"
        '
        'IntermediaryCodesToolStripMenuItem
        '
        Me.IntermediaryCodesToolStripMenuItem.Image = CType(resources.GetObject("IntermediaryCodesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IntermediaryCodesToolStripMenuItem.Name = "IntermediaryCodesToolStripMenuItem"
        Me.IntermediaryCodesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.IntermediaryCodesToolStripMenuItem.Text = "Intermediary Codes"
        '
        'FeesToolStripMenuItem
        '
        Me.FeesToolStripMenuItem.Image = CType(resources.GetObject("FeesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FeesToolStripMenuItem.Name = "FeesToolStripMenuItem"
        Me.FeesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.FeesToolStripMenuItem.Text = "Fees"
        '
        'ToolStrip
        '
        Me.ToolStrip.BackColor = System.Drawing.Color.White
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripButton11, Me.ToolStripButton1, Me.ToolStripButton6, Me.ToolStripButton19, Me.ToolStripButton9, Me.ToolStripButton3, Me.ToolStripButton22, Me.ToolStripSeparator1, Me.ToolStripButton7, Me.ToolStripButton8, Me.ToolStripButton13, Me.ToolStripButton14, Me.ToolStripButton18, Me.ToolStripButton20, Me.ToolStripButton26, Me.ToolStripButton12, Me.ToolStripButton16, Me.ToolStripButton21, Me.ToolStripSeparator2, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripButtonInt, Me.ToolStripButtonFee, Me.ToolStripButton10, Me.ToolStripButton15, Me.ToolStripButton23, Me.ToolStripButton24, Me.ToolStripButton28, Me.ToolStripSeparator9, Me.ToolStripButton25, Me.ToolStripButton27, Me.ToolStripButton17})
        Me.ToolStrip.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(1187, 25)
        Me.ToolStrip.TabIndex = 6
        Me.ToolStrip.Text = "ToolStrip"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Add Movement"
        Me.ToolStripButton2.ToolTipText = "Movement"
        '
        'ToolStripButton11
        '
        Me.ToolStripButton11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton11.Image = CType(resources.GetObject("ToolStripButton11.Image"), System.Drawing.Image)
        Me.ToolStripButton11.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton11.Name = "ToolStripButton11"
        Me.ToolStripButton11.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton11.Text = "Receive/Deliver"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "Omnibus Switch"
        Me.ToolStripButton1.ToolTipText = "Internal Omnibus Switch"
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton6.Text = "Omnibus Switch Term Deposits"
        '
        'ToolStripButton19
        '
        Me.ToolStripButton19.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton19.Image = CType(resources.GetObject("ToolStripButton19.Image"), System.Drawing.Image)
        Me.ToolStripButton19.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton19.Name = "ToolStripButton19"
        Me.ToolStripButton19.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton19.Text = "Treasury Management"
        '
        'ToolStripButton9
        '
        Me.ToolStripButton9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton9.Image = CType(resources.GetObject("ToolStripButton9.Image"), System.Drawing.Image)
        Me.ToolStripButton9.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton9.Name = "ToolStripButton9"
        Me.ToolStripButton9.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton9.Text = "Auto Swift"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "View Movements"
        Me.ToolStripButton3.ToolTipText = "View Movements"
        '
        'ToolStripButton22
        '
        Me.ToolStripButton22.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton22.Image = CType(resources.GetObject("ToolStripButton22.Image"), System.Drawing.Image)
        Me.ToolStripButton22.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton22.Name = "ToolStripButton22"
        Me.ToolStripButton22.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton22.Text = "Batch Processing"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton7.Text = "Balances"
        Me.ToolStripButton7.ToolTipText = "Balances"
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton8.Text = "Swift Read"
        '
        'ToolStripButton13
        '
        Me.ToolStripButton13.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton13.Image = CType(resources.GetObject("ToolStripButton13.Image"), System.Drawing.Image)
        Me.ToolStripButton13.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton13.Name = "ToolStripButton13"
        Me.ToolStripButton13.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton13.Text = "Payments Requests/External Receipts"
        '
        'ToolStripButton14
        '
        Me.ToolStripButton14.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton14.Image = CType(resources.GetObject("ToolStripButton14.Image"), System.Drawing.Image)
        Me.ToolStripButton14.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton14.Name = "ToolStripButton14"
        Me.ToolStripButton14.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton14.Text = "Fees"
        '
        'ToolStripButton18
        '
        Me.ToolStripButton18.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton18.Image = CType(resources.GetObject("ToolStripButton18.Image"), System.Drawing.Image)
        Me.ToolStripButton18.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton18.Name = "ToolStripButton18"
        Me.ToolStripButton18.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton18.Text = "Fees Paid"
        '
        'ToolStripButton20
        '
        Me.ToolStripButton20.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton20.Image = CType(resources.GetObject("ToolStripButton20.Image"), System.Drawing.Image)
        Me.ToolStripButton20.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton20.Name = "ToolStripButton20"
        Me.ToolStripButton20.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton20.Text = "IMS Reconciliation"
        '
        'ToolStripButton26
        '
        Me.ToolStripButton26.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton26.Image = CType(resources.GetObject("ToolStripButton26.Image"), System.Drawing.Image)
        Me.ToolStripButton26.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton26.Name = "ToolStripButton26"
        Me.ToolStripButton26.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton26.Text = "BGM Reconciliation"
        '
        'ToolStripButton12
        '
        Me.ToolStripButton12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton12.Image = CType(resources.GetObject("ToolStripButton12.Image"), System.Drawing.Image)
        Me.ToolStripButton12.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton12.Name = "ToolStripButton12"
        Me.ToolStripButton12.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton12.Text = "Corporate Actions"
        '
        'ToolStripButton16
        '
        Me.ToolStripButton16.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton16.Image = CType(resources.GetObject("ToolStripButton16.Image"), System.Drawing.Image)
        Me.ToolStripButton16.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton16.Name = "ToolStripButton16"
        Me.ToolStripButton16.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton16.Text = "Cash Transactions Comments"
        '
        'ToolStripButton21
        '
        Me.ToolStripButton21.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton21.Image = CType(resources.GetObject("ToolStripButton21.Image"), System.Drawing.Image)
        Me.ToolStripButton21.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton21.Name = "ToolStripButton21"
        Me.ToolStripButton21.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton21.Text = "SWIFT Account Statements"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "Cut Off"
        Me.ToolStripButton4.ToolTipText = "Cut Off Times"
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton5.Text = "Transfer Fees"
        Me.ToolStripButton5.ToolTipText = "Transfer Fees"
        '
        'ToolStripButtonInt
        '
        Me.ToolStripButtonInt.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonInt.Image = CType(resources.GetObject("ToolStripButtonInt.Image"), System.Drawing.Image)
        Me.ToolStripButtonInt.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonInt.Name = "ToolStripButtonInt"
        Me.ToolStripButtonInt.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonInt.Text = "Templates"
        Me.ToolStripButtonInt.ToolTipText = "Templates"
        '
        'ToolStripButtonFee
        '
        Me.ToolStripButtonFee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButtonFee.Image = CType(resources.GetObject("ToolStripButtonFee.Image"), System.Drawing.Image)
        Me.ToolStripButtonFee.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButtonFee.Name = "ToolStripButtonFee"
        Me.ToolStripButtonFee.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButtonFee.Text = "SWIFT Codes"
        '
        'ToolStripButton10
        '
        Me.ToolStripButton10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton10.Image = CType(resources.GetObject("ToolStripButton10.Image"), System.Drawing.Image)
        Me.ToolStripButton10.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton10.Name = "ToolStripButton10"
        Me.ToolStripButton10.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton10.Text = "Intermediary Codes"
        '
        'ToolStripButton15
        '
        Me.ToolStripButton15.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton15.Image = CType(resources.GetObject("ToolStripButton15.Image"), System.Drawing.Image)
        Me.ToolStripButton15.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton15.Name = "ToolStripButton15"
        Me.ToolStripButton15.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton15.Text = "Risk Assessment"
        '
        'ToolStripButton23
        '
        Me.ToolStripButton23.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton23.Image = CType(resources.GetObject("ToolStripButton23.Image"), System.Drawing.Image)
        Me.ToolStripButton23.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton23.Name = "ToolStripButton23"
        Me.ToolStripButton23.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton23.Text = "Compliance Watchlist"
        '
        'ToolStripButton24
        '
        Me.ToolStripButton24.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton24.Image = CType(resources.GetObject("ToolStripButton24.Image"), System.Drawing.Image)
        Me.ToolStripButton24.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton24.Name = "ToolStripButton24"
        Me.ToolStripButton24.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton24.Text = "CASS Accounts"
        '
        'ToolStripButton28
        '
        Me.ToolStripButton28.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton28.Image = CType(resources.GetObject("ToolStripButton28.Image"), System.Drawing.Image)
        Me.ToolStripButton28.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton28.Name = "ToolStripButton28"
        Me.ToolStripButton28.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton28.Text = "GPP Mapping Codes"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripButton25
        '
        Me.ToolStripButton25.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton25.Image = CType(resources.GetObject("ToolStripButton25.Image"), System.Drawing.Image)
        Me.ToolStripButton25.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton25.Name = "ToolStripButton25"
        Me.ToolStripButton25.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton25.Text = "ToolStripButton25"
        Me.ToolStripButton25.ToolTipText = "Client Emails"
        '
        'ToolStripButton27
        '
        Me.ToolStripButton27.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton27.Image = CType(resources.GetObject("ToolStripButton27.Image"), System.Drawing.Image)
        Me.ToolStripButton27.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton27.Name = "ToolStripButton27"
        Me.ToolStripButton27.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton27.Text = "Client Statement Generator"
        '
        'ToolStripButton17
        '
        Me.ToolStripButton17.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton17.Image = CType(resources.GetObject("ToolStripButton17.Image"), System.Drawing.Image)
        Me.ToolStripButton17.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton17.Name = "ToolStripButton17"
        Me.ToolStripButton17.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton17.Text = "Password Generator"
        '
        'StatusStrip
        '
        Me.StatusStrip.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusStrip.Location = New System.Drawing.Point(0, 680)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1187, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'TimerInActivity
        '
        Me.TimerInActivity.Interval = 120000
        '
        'MDIParent
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Azure
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1187, 702)
        Me.Controls.Add(Me.ToolStrip)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "MDIParent"
        Me.Text = "FLOW"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(false)
        Me.MenuStrip.PerformLayout
        Me.ToolStrip.ResumeLayout(false)
        Me.ToolStrip.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents HelpMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndexToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintPreviewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PrintSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents EditMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RedoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusBarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolsMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaymentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CutOffTimesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TransferFeesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserDetailsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AuditLogToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InternalOmnibusSwitchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton1 As ToolStripButton
    Friend WithEvents ToolStripButton2 As ToolStripButton
    Friend WithEvents ToolStripButton3 As ToolStripButton
    Friend WithEvents ToolStripButton4 As ToolStripButton
    Friend WithEvents ToolStripButton5 As ToolStripButton
    Friend WithEvents TemplateManagementToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButtonInt As ToolStripButton
    Friend WithEvents ViewBalancesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton7 As ToolStripButton
    Friend WithEvents TechnicalGuideToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TimerInActivity As Timer
    Friend WithEvents OptionsToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents SwiftCodesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButtonFee As ToolStripButton
    Friend WithEvents ToolStripButton10 As ToolStripButton
    Friend WithEvents IntermediaryCodesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SystemSettingsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReceiveDeliverFreeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton11 As ToolStripButton
    Friend WithEvents AutomaticSwiftsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton12 As ToolStripButton
    Friend WithEvents AnalysisToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewBalancesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ReadSwiftToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton8 As ToolStripButton
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents ConfirmExternalReceiptsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton13 As ToolStripButton
    Friend WithEvents FeesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton14 As ToolStripButton
    Friend WithEvents CutOffToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TransferFeesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents TemplatesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SwiftCodesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents IntermediaryCodesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ViewBalancesToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ReadSwiftToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ConfirmExternalReceiptsToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents InternalOmnibusSwitchTermDepositsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents FeesToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents CorporateActionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton9 As ToolStripButton
    Friend WithEvents RiskAssessmentToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton15 As ToolStripButton
    Friend WithEvents CashTransactionCommentsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton16 As ToolStripButton
    Friend WithEvents ToolStripButton17 As ToolStripButton
    Friend WithEvents ToolStripButton18 As ToolStripButton
    Friend WithEvents FeesPaidToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton19 As ToolStripButton
    Friend WithEvents TreasuryManagementToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IMSReconciliationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton20 As ToolStripButton
    Friend WithEvents SwiftAccountStatementsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton21 As ToolStripButton
    Friend WithEvents BatchProcessingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton22 As ToolStripButton
    Friend WithEvents ToolStripButton23 As ToolStripButton
    Friend WithEvents ComplianceWatchlistToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents RunClientReportsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReleaseIMSDocNumbersToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CASSAccountsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton24 As ToolStripButton
    Friend WithEvents ClientsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ClientEmailsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator9 As ToolStripSeparator
    Friend WithEvents ToolStripButton25 As ToolStripButton
    Friend WithEvents ClientStatementGeneratorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BritanniaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton26 As ToolStripButton
    Friend WithEvents ToolStripButton27 As ToolStripButton
    Friend WithEvents PasswordGeneratorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GPPMappingCodesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripButton28 As ToolStripButton
End Class
