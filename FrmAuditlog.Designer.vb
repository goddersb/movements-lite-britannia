﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmAuditlog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmAuditlog))
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboAuditStatus = New System.Windows.Forms.ComboBox()
        Me.dgvAudit = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CboAuditToDate = New System.Windows.Forms.ComboBox()
        Me.cboAuditID = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CboAuditFromDate = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboAuditUser = New System.Windows.Forms.ComboBox()
        Me.cboAuditGroup = New System.Windows.Forms.ComboBox()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        CType(Me.dgvAudit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(623, 67)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 39
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(12, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 24)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Audit Log"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(206, -14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Filter Portfolio:"
        '
        'cboAuditStatus
        '
        Me.cboAuditStatus.FormattingEnabled = True
        Me.cboAuditStatus.Location = New System.Drawing.Point(314, 23)
        Me.cboAuditStatus.Name = "cboAuditStatus"
        Me.cboAuditStatus.Size = New System.Drawing.Size(295, 21)
        Me.cboAuditStatus.TabIndex = 36
        '
        'dgvAudit
        '
        Me.dgvAudit.AllowUserToAddRows = False
        Me.dgvAudit.AllowUserToDeleteRows = False
        Me.dgvAudit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAudit.Location = New System.Drawing.Point(16, 146)
        Me.dgvAudit.Name = "dgvAudit"
        Me.dgvAudit.ReadOnly = True
        Me.dgvAudit.Size = New System.Drawing.Size(1466, 685)
        Me.dgvAudit.TabIndex = 35
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.CboAuditToDate)
        Me.GroupBox1.Controls.Add(Me.cboAuditID)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.CboAuditFromDate)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CboAuditUser)
        Me.GroupBox1.Controls.Add(Me.cboAuditGroup)
        Me.GroupBox1.Controls.Add(Me.CmdFilter)
        Me.GroupBox1.Controls.Add(Me.cboAuditStatus)
        Me.GroupBox1.Controls.Add(Me.CmdRefreshView)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 36)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(799, 104)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Audit Filter Criteria"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(260, 80)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 60
        Me.Label8.Text = "To Date:"
        '
        'CboAuditToDate
        '
        Me.CboAuditToDate.FormattingEnabled = True
        Me.CboAuditToDate.Location = New System.Drawing.Point(314, 77)
        Me.CboAuditToDate.Name = "CboAuditToDate"
        Me.CboAuditToDate.Size = New System.Drawing.Size(169, 21)
        Me.CboAuditToDate.TabIndex = 59
        '
        'cboAuditID
        '
        Me.cboAuditID.FormattingEnabled = True
        Me.cboAuditID.Location = New System.Drawing.Point(77, 23)
        Me.cboAuditID.Name = "cboAuditID"
        Me.cboAuditID.Size = New System.Drawing.Size(69, 21)
        Me.cboAuditID.TabIndex = 57
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(31, 26)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(45, 13)
        Me.Label14.TabIndex = 58
        Me.Label14.Text = "Audit ID"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(741, 24)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 56
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 47
        Me.Label6.Text = "From Date:"
        '
        'CboAuditFromDate
        '
        Me.CboAuditFromDate.FormattingEnabled = True
        Me.CboAuditFromDate.Location = New System.Drawing.Point(77, 77)
        Me.CboAuditFromDate.Name = "CboAuditFromDate"
        Me.CboAuditFromDate.Size = New System.Drawing.Size(169, 21)
        Me.CboAuditFromDate.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(253, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "Audit User:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 44
        Me.Label4.Text = "Audit Group:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(245, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Audit Status:"
        '
        'CboAuditUser
        '
        Me.CboAuditUser.FormattingEnabled = True
        Me.CboAuditUser.Location = New System.Drawing.Point(315, 50)
        Me.CboAuditUser.Name = "CboAuditUser"
        Me.CboAuditUser.Size = New System.Drawing.Size(294, 21)
        Me.CboAuditUser.TabIndex = 42
        '
        'cboAuditGroup
        '
        Me.cboAuditGroup.FormattingEnabled = True
        Me.cboAuditGroup.Location = New System.Drawing.Point(77, 50)
        Me.cboAuditGroup.Name = "cboAuditGroup"
        Me.cboAuditGroup.Size = New System.Drawing.Size(169, 21)
        Me.cboAuditGroup.TabIndex = 41
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(623, 26)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 38)
        Me.CmdFilter.TabIndex = 40
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.Maroon
        Me.Label7.Location = New System.Drawing.Point(241, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(443, 13)
        Me.Label7.TabIndex = 41
        Me.Label7.Text = "This filter only shows the top 1000 records. Please use the filter to reduce your" &
    " search criteria"
        '
        'FrmAuditlog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1494, 843)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvAudit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmAuditlog"
        Me.Text = "Audit Log"
        CType(Me.dgvAudit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CmdRefreshView As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAuditStatus As System.Windows.Forms.ComboBox
    Friend WithEvents dgvAudit As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CboAuditUser As System.Windows.Forms.ComboBox
    Friend WithEvents cboAuditGroup As System.Windows.Forms.ComboBox
    Friend WithEvents CmdFilter As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents CboAuditFromDate As System.Windows.Forms.ComboBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents cboAuditID As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents CboAuditToDate As ComboBox
End Class
