﻿Imports System.ComponentModel

Public Class FrmBatchProcessingTemplate
    Private Sub FrmBatchProcessingTemplate_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        ClsBP.TemplateID = cboTemFindName.SelectedValue
    End Sub

    Private Sub FrmBatchProcessingTemplate_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPortfolio.Text = ClsBP.PortfolioName
        txtCCY.Text = ClsBP.CCYName
        ClsIMS.PopulateComboboxWithData(cboTemFindName, "select Pa_Id as ID,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & ClsBP.PortfolioCode & " and Pa_CCYCode = " & ClsBP.CCYCode & " and isnull(Pa_TemplateName,'')<>'' group by Pa_Id,Pa_TemplateName order by ID, Pa_TemplateName")
    End Sub
End Class