﻿Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class FrmClientEmailAddresses

    Dim sqlConn As SqlConnection
    Dim dgvdata As SqlDataAdapter
    Dim ds As New DataSet
    Dim saveEmails As Boolean = False
    Dim saveRptsCsvConfig As Boolean = False
    Dim savePDFs As Boolean = False

    Private Sub FrmClientEmailAddresses_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
        LoadComboBoxForClientEmailAddress()
        LoadComboBoxForCsvConfigLoad()
        LoadComboBoxForPortfolios()
        CEMLoadGrid()

    End Sub

    Private Sub LoadComboBoxForClientEmailAddress()

        cboPortfolioName.Items.Clear()
        ModRMS.DoubleBuffered(dgvClientEmailAddresses, True)
        ClsIMS.PopulateComboboxWithData(cboPortfolioName, "Select 1, Portfolio from _Dolfin_PortfolioList Where PF_Active = 1 Order by Portfolio")
        cboPortfolioName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPortfolioName.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboShortcut, "Select 1, PF_Shortcut1 from _Dolfin_PortfolioList Where PF_Active = 1 order by PF_Shortcut1")
        cboShortcut.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboShortcut.AutoCompleteSource = AutoCompleteSource.ListItems

    End Sub

    Private Sub LoadComboBoxForPortfolios()

        cboPDFPortfolioName.DataSource = nothing
        cboPDFPortfolioName.Items.Clear()
        ModRMS.DoubleBuffered(dgvPDFs, True)
        ClsIMS.PopulateComboboxWithData(cboPDFPortfolioName, "Select RS_PFCode, Portfolio from _Dolfin_ReportSchedule rs  inner join _Dolfin_PortfolioList pl on pl.PF_Code =  rs.RS_PfCode Where pl.PF_Active = 1 Order by Portfolio")
        cboPDFPortfolioName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPDFPortfolioName.AutoCompleteSource = AutoCompleteSource.ListItems

    End Sub

    Private Sub LoadComboBoxForCsvConfigLoad()
        
        ModRMS.DoubleBuffered(dgvRptsCsvConfig, True)

        cboFullName.DataSource = nothing
        cboFullName.Items.Clear()
        ClsIMS.PopulateComboboxWithData(cboFullName, "Select 1, _Dolfin_ClientListWithDetail.FullName from _Dolfin_ClientListWithDetail inner join _Dolfin_RptCsvConfig on _Dolfin_ClientListWithDetail.Cust_ID = _Dolfin_RptCsvConfig.Cust_ID Where IsCustomerActive = 1 Order by 2")
        cboFullName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFullName.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub TabControl1_Selected(sender As Object, e As TabControlEventArgs) Handles TabControl1.Selected
        
        Cursor = Cursors.WaitCursor

        saveEmails = False
        saveRptsCsvConfig = False
        savePDFs = False

        Select Case e.TabPageIndex
            Case 0 
                CEMLoadGrid()
            Case 1
                CsvConfigLoadGrid()
            Case 2
                PDFLoadGrid()
        End Select

        Cursor = Cursors.Default

    End Sub

    Private Sub CEMLoadGrid(Optional ByVal varFilterWhereClause As String = "")

        Try
            Cursor = Cursors.WaitCursor



            Dim varSql As String
            If varFilterWhereClause = "" Then
                varSql = "Select 
                            pl.PF_code, 
                            PF_Shortcut1,
                            Portfolio, 
                            PF_AddrContact,
                            PF_AddrEmail
                            from 
                            _Dolfin_PortfolioList pl 
                            inner join [dbo].[_Dolfin_PF_ADDRESSES] addr on pl.PF_Code = addr.PF_Code 
                            where PF_Active = 1 and (ISNULL(PF_AddrEmail,'')<>'' OR ISNULL(PF_AddrContact,'')<>'' or ISNULL(PF_AddrStreet,'')<>'')
                            group by 
                            pl.PF_code, 
                            PF_Shortcut1,
                            Portfolio,
                            PF_AddrContact,
                            PF_AddrEmail
                            Order by Portfolio"
            Else
                varSql = "Select 
                            pl.PF_code, 
                            PF_Shortcut1,
                            Portfolio, 
                            PF_AddrContact,
                            PF_AddrEmail
                            from 
                            _Dolfin_PortfolioList pl 
                            inner join [dbo].[_Dolfin_PF_ADDRESSES] addr on pl.PF_Code = addr.PF_Code  " & varFilterWhereClause & " group by 
                            pl.PF_code, 
                            PF_Shortcut1,
                            Portfolio,
                            PF_AddrContact,
                            PF_AddrEmail
                            Order by Portfolio"
            End If

            dgvClientEmailAddresses.DataSource = Nothing
            sqlConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "Portfolios")
            dgvClientEmailAddresses.AutoGenerateColumns = True
            dgvClientEmailAddresses.DataSource = ds.Tables("Portfolios")

            dgvClientEmailAddresses.Columns("PF_code").Visible = False
            dgvClientEmailAddresses.Columns("PF_Shortcut1").HeaderText = "Portfolio"
            dgvClientEmailAddresses.Columns("PF_Shortcut1").Width = 200
            dgvClientEmailAddresses.Columns("PF_Shortcut1").ReadOnly = True

            dgvClientEmailAddresses.Columns("Portfolio").HeaderText = "Portfolio Name"
            dgvClientEmailAddresses.Columns("Portfolio").Width = 500
            dgvClientEmailAddresses.Columns("Portfolio").ReadOnly = True

            dgvClientEmailAddresses.Columns("PF_AddrContact").HeaderText = "Contact Name"
            dgvClientEmailAddresses.Columns("PF_AddrContact").Width = 300

            dgvClientEmailAddresses.Columns("PF_AddrEmail").HeaderText = "Email Address"
            dgvClientEmailAddresses.Columns("PF_AddrEmail").Width = 600

            dgvClientEmailAddresses.Refresh()
            Cursor = Cursors.Default

        Catch ex As Exception
            Cursor = Cursors.Default
            dgvClientEmailAddresses.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientEmailAddresses, Err.Description & " - Problem With viewing client email addresses")
        End Try
    End Sub

    Private Sub CsvConfigLoadGrid(Optional ByVal varFilterWhereClause As String = "")
          Try
            Cursor = Cursors.WaitCursor


            Dim varSql As String
            If varFilterWhereClause = "" Then
                varSql = "Select
                            _Dolfin_ClientListWithDetail.Cust_Id, 
                            _Dolfin_ClientListWithDetail.FullName, 
                            _Dolfin_RptCsvConfig.EmailBCC, 
                            _Dolfin_RptCsvConfig.EmailCC,
                            _Dolfin_RptCsvConfig.EmailTo,
                            _Dolfin_RptCsvConfig.EmailToError, 
                            _Dolfin_RptCsvConfig.IsCashTransactionReq,	
                            _Dolfin_RptCsvConfig.IsTransactionReq,	
                            _Dolfin_RptCsvConfig.IsValuationReq,
                            _Dolfin_RptCsvConfig.RFID
                            from _Dolfin_ClientListWithDetail
                            inner join _Dolfin_RptCsvConfig on _Dolfin_ClientListWithDetail.Cust_ID = _Dolfin_RptCsvConfig.Cust_ID where IsCustomerActive = 1 Order by _Dolfin_ClientListWithDetail.FullName"
            Else
                varSql = "Select _Dolfin_ClientListWithDetail.Cust_Id, 
                            _Dolfin_ClientListWithDetail.FullName, 
                            _Dolfin_RptCsvConfig.EmailBCC, 
                            _Dolfin_RptCsvConfig.EmailCC,
                            _Dolfin_RptCsvConfig.EmailTo,
                            _Dolfin_RptCsvConfig.EmailToError, 
                            _Dolfin_RptCsvConfig.IsCashTransactionReq,	
                            _Dolfin_RptCsvConfig.IsTransactionReq,	
                            _Dolfin_RptCsvConfig.IsValuationReq,
                            _Dolfin_RptCsvConfig.RFID
                            from _Dolfin_ClientListWithDetail
                            inner join _Dolfin_RptCsvConfig on _Dolfin_ClientListWithDetail.Cust_ID = _Dolfin_RptCsvConfig.Cust_ID " &  varFilterWhereClause & " Order by _Dolfin_ClientListWithDetail.FullName" 
            End If
              
            dgvRptsCsvConfig.DataSource = Nothing
            sqlConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "ClientCsvConfig")
            dgvRptsCsvConfig.AutoGenerateColumns = True
            dgvRptsCsvConfig.DataSource = ds.Tables("ClientCsvConfig")

            dgvRptsCsvConfig.Columns("Cust_Id").HeaderText = "Customer Id"
            dgvRptsCsvConfig.Columns("Cust_Id").Width = 150
            dgvRptsCsvConfig.Columns("Cust_Id").ReadOnly = True

            Dim varColSQL As String = "Select Cust_ID, FullName from _Dolfin_ClientListWithDetail Where ISCustomerActive = 1 Order by FullName"
            Dim dgvNames As New SqlDataAdapter(varColSQL, sqlConn)
            dgvNames.Fill(ds, "vwDolfinPortfolioNames")

            Dim cboPortfolioName As New DataGridViewComboBoxColumn()
            cboPortfolioName.HeaderText = "Full Name"
            cboPortfolioName.DataPropertyName = "Cust_Id"
            cboPortfolioName.DataSource = ds.Tables("vwDolfinPortfolioNames")
            cboPortfolioName.ValueMember = ds.Tables("vwDolfinPortfolioNames").Columns(0).ColumnName
            cboPortfolioName.DisplayMember = ds.Tables("vwDolfinPortfolioNames").Columns(1).ColumnName

            cboPortfolioName.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboPortfolioName.FlatStyle = FlatStyle.Flat


            varColSQL = "SELECT [RF_ID],[RF_Name] FROM [dbo].[_Dolfin_ReportFrequency]"
            dgvNames = New SqlDataAdapter(varColSQL, sqlConn)
            dgvNames.Fill(ds, "vwDolfinReportFrequencies")


            Dim cboReportFrequency As New DataGridViewComboBoxColumn()
            cboReportFrequency.HeaderText = "Report Frequency"
            cboReportFrequency.Name = "Report Frequency"
            cboReportFrequency.DataPropertyName = "RFID"
            cboReportFrequency.DataSource = ds.Tables("vwDolfinReportFrequencies")
            cboReportFrequency.ValueMember = ds.Tables("vwDolfinReportFrequencies").Columns(0).ColumnName
            cboReportFrequency.DisplayMember = ds.Tables("vwDolfinReportFrequencies").Columns(1).ColumnName

            cboReportFrequency.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboReportFrequency.FlatStyle = FlatStyle.Flat
            cboReportFrequency.DefaultCellStyle.NullValue = ""


            cboPortfolioName.DefaultCellStyle.NullValue = ""
            dgvRptsCsvConfig.Columns.Insert(1, cboPortfolioName)
            dgvRptsCsvConfig.Columns(1).Width = 400

            dgvRptsCsvConfig.Columns.Insert(2, cboReportFrequency)
            dgvRptsCsvConfig.Columns(2).Width = 100

            dgvRptsCsvConfig.Columns("FullName").HeaderText = "Full Name"
            dgvRptsCsvConfig.Columns("FullName").Visible = False
            dgvRptsCsvConfig.Columns("RFID").Visible = False

            dgvRptsCsvConfig.Columns("EmailBCC").HeaderText = "Email BCC"
            dgvRptsCsvConfig.Columns("EmailBCC").Width = 500
            
            dgvRptsCsvConfig.Columns("EmailCC").HeaderText = "Email CC"
            dgvRptsCsvConfig.Columns("EmailCC").Width = 500

            dgvRptsCsvConfig.Columns("EmailTo").HeaderText = "Email To"
            dgvRptsCsvConfig.Columns("EmailTo").Width = 500
            
            dgvRptsCsvConfig.Columns("EmailToError").HeaderText = "Email To Error"
            dgvRptsCsvConfig.Columns("EmailToError").Width = 300
            
            dgvRptsCsvConfig.Columns("IsCashTransactionReq").HeaderText = "Is Cash Transaction Required"
            dgvRptsCsvConfig.Columns("IsCashTransactionReq").Width = 200

            dgvRptsCsvConfig.Columns("IsTransactionReq").HeaderText = "Is Transaction Required"
            dgvRptsCsvConfig.Columns("IsTransactionReq").Width = 150

            dgvRptsCsvConfig.Columns("IsValuationReq").HeaderText = "Is Valuation Required"
            dgvRptsCsvConfig.Columns("IsValuationReq").Width = 150

            dgvRptsCsvConfig.Refresh()
            Cursor = Cursors.Default 
           
            saveEmails = True

        Catch ex As Exception
            Cursor = Cursors.Default
            dgvRptsCsvConfig.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientEmailAddresses, Err.Description & " - Problem With viewing client reports csv config")
        End Try
       
    End Sub

    Private Sub PDFLoadGrid(Optional ByVal varFilterWhereClause As String = "")

        Try
            Cursor = Cursors.WaitCursor

            Dim varSql As String
            If varFilterWhereClause = "" Then
                varSql = "SELECT [RS_ID],[RS_PfCode],[Portfolio],[RS_RfID],[RS_EmailTo],[RS_EmailCC],[RS_EmailBCC],[RS_WithPerformance],[RS_IsActive],RS_ReportName,RS_LastRun
                         FROM [dbo].[_Dolfin_ReportSchedule] RS INNER JOIN [dbo].[_Dolfin_PortfolioList] P ON RS.RS_PfCode = P.PF_Code
                         WHERE RS_IsActive = 1 AND PF_Active = 1 ORDER BY RS_RfID,Portfolio"
            Else
                varSql = "SELECT [RS_ID],[RS_PfCode],[Portfolio],[RS_RfID],[RS_EmailTo],[RS_EmailCC],[RS_EmailBCC],[RS_WithPerformance],[RS_IsActive],RS_ReportName,RS_LastRun
                         FROM [dbo].[_Dolfin_ReportSchedule] RS INNER JOIN [dbo].[_Dolfin_PortfolioList] P ON RS.RS_PfCode = P.PF_Code
                         WHERE RS_IsActive = 1 AND PF_Active = 1 " & varFilterWhereClause & " ORDER BY RS_RfID,Portfolio"
            End If

            dgvPDFs.DataSource = Nothing
            sqlConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "Reports")
            dgvPDFs.AutoGenerateColumns = True
            dgvPDFs.DataSource = ds.Tables("Reports")

            dgvPDFs.Columns("RS_ID").Visible = False
            dgvPDFs.Columns("RS_PfCode").Visible = False
            dgvPDFs.Columns("Portfolio").Visible = False
            dgvPDFs.Columns("RS_RfID").Visible = False
            dgvPDFs.Columns("RS_ReportName").Visible = False

            Dim varColSQL = "Select PF_Code, Portfolio from _Dolfin_PortfolioList Where PF_Active = 1 Order by Portfolio"
            Dim dgvNames As New SqlDataAdapter(varColSQL, sqlConn)
            dgvNames.Fill(ds, "vwDolfinPortfolioNames")

            Dim cboPortfolioName As New DataGridViewComboBoxColumn()
            cboPortfolioName.HeaderText = "Portfolio"
            cboPortfolioName.Name = "Portfolio"
            cboPortfolioName.DataPropertyName = "RS_PfCode"
            cboPortfolioName.DataSource = ds.Tables("vwDolfinPortfolioNames")
            cboPortfolioName.ValueMember = ds.Tables("vwDolfinPortfolioNames").Columns(0).ColumnName
            cboPortfolioName.DisplayMember = ds.Tables("vwDolfinPortfolioNames").Columns(1).ColumnName

            cboPortfolioName.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboPortfolioName.FlatStyle = FlatStyle.Flat
            cboPortfolioName.DefaultCellStyle.NullValue = ""



            varColSQL = "SELECT [RF_ID],[RF_Name] FROM [dbo].[_Dolfin_ReportFrequency]"
            dgvNames = New SqlDataAdapter(varColSQL, sqlConn)
            dgvNames.Fill(ds, "vwDolfinReportFrequencies")

            Dim cboReportFrequency As New DataGridViewComboBoxColumn()
            cboReportFrequency.HeaderText = "Report Frequency"
            cboReportFrequency.Name = "Report Frequency"
            cboReportFrequency.DataPropertyName = "RS_RfID"
            cboReportFrequency.DataSource = ds.Tables("vwDolfinReportFrequencies")
            cboReportFrequency.ValueMember = ds.Tables("vwDolfinReportFrequencies").Columns(0).ColumnName
            cboReportFrequency.DisplayMember = ds.Tables("vwDolfinReportFrequencies").Columns(1).ColumnName

            cboReportFrequency.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboReportFrequency.FlatStyle = FlatStyle.Flat
            cboReportFrequency.DefaultCellStyle.NullValue = ""


            varColSQL = "Select [Name] as RS_ReportName FROM [ReportServer].[dbo].[Catalog] WHERE left(Name, 7) = 'Client ' and left(Path,22) = '/Dolfin Reporting NEW/'"
            dgvNames = New SqlDataAdapter(varColSQL, sqlConn)
            dgvNames.Fill(ds, "vwDolfinReportName")

            Dim cboReportName As New DataGridViewComboBoxColumn()
            cboReportName.HeaderText = "Report Name"
            cboReportName.Name = "Report Name"
            cboReportName.DataPropertyName = "RS_ReportName"
            cboReportName.DataSource = ds.Tables("vwDolfinReportName")
            cboReportName.ValueMember = ds.Tables("vwDolfinReportName").Columns(0).ColumnName
            cboReportName.DisplayMember = ds.Tables("vwDolfinReportName").Columns(0).ColumnName

            cboReportName.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboReportName.FlatStyle = FlatStyle.Flat
            cboReportName.DefaultCellStyle.NullValue = ""


            dgvPDFs.Columns.Insert(0, cboPortfolioName)
            dgvPDFs.Columns(0).Width = 400

            dgvPDFs.Columns.Insert(1, cboReportFrequency)
            dgvPDFs.Columns(1).Width = 100

            dgvPDFs.Columns.Insert(2, cboReportName)
            dgvPDFs.Columns(2).Width = 200

            dgvPDFs.Columns("RS_EmailTo").HeaderText = "Email To"
            dgvPDFs.Columns("RS_EmailTo").Width = 200

            dgvPDFs.Columns("RS_EmailCC").HeaderText = "Email CC"
            dgvPDFs.Columns("RS_EmailCC").Width = 200

            dgvPDFs.Columns("RS_EmailBCC").HeaderText = "Email BC"
            dgvPDFs.Columns("RS_EmailBCC").Width = 200

            dgvPDFs.Columns("RS_WithPerformance").HeaderText = "Performance"
            dgvPDFs.Columns("RS_WithPerformance").Width = 100

            dgvPDFs.Columns("RS_IsActive").HeaderText = "Is Active"
            dgvPDFs.Columns("RS_IsActive").Width = 100

            dgvPDFs.Columns("RS_LastRun").HeaderText = "Last Run"
            dgvPDFs.Columns("RS_LastRun").Width = 150


            dgvPDFs.Refresh()
            Cursor = Cursors.Default

        Catch ex As Exception
            Cursor = Cursors.Default
            dgvPDFs.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientPDFReports, Err.Description & " - Problem With viewing client email addresses")
        End Try
    End Sub

    Private Sub dgvClientEmailAddresses_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvClientEmailAddresses.MouseDown
        saveEmails = True
    End Sub

    Private Sub dgvRptsCsvConfig_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvRptsCsvConfig.MouseDown 
        saveRptsCsvConfig = True
    End Sub

    Private Sub dgvPDFs_MouseDown(sender As Object, e As MouseEventArgs) Handles dgvPDFs.MouseDown 
        savePDFs = True
    End Sub
    
    Private Sub dgvPDFs_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPDFs.RowLeave 

        If Not savePDFs Then
            Return
        End If


        If dgvPDFs.IsCurrentRowDirty Then

            dgvPDFs.EndEdit()

            Dim cellValue As String
            cellValue = dgvPDFs.Rows(e.RowIndex).Cells(4).EditedFormattedValue

            If Not AreValidEmails(cellValue) Then
                MessageBox.Show("EmailTo value must be a valid email address or else leave blank", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            cellValue = dgvPDFs.Rows(e.RowIndex).Cells(5).EditedFormattedValue

            If Not AreValidEmails(cellValue) Then
                MessageBox.Show("Email CC value must be a valid email address or else leave blank", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            cellValue = dgvPDFs.Rows(e.RowIndex).Cells(6).EditedFormattedValue

            If Not AreValidEmails(cellValue) Then
                MessageBox.Show("Email BC value must be a valid email address or else leave blank", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim pfCode As Integer = dgvPDFs.Rows(e.RowIndex).Cells(1).Value
            Dim rfId As Integer = dgvPDFs.Rows(e.RowIndex).Cells(3).Value
            Dim emailTo As String = dgvPDFs.Rows(e.RowIndex).Cells(4).EditedFormattedValue
            Dim emailCC As String = dgvPDFs.Rows(e.RowIndex).Cells(5).EditedFormattedValue
            Dim emailBC As String = dgvPDFs.Rows(e.RowIndex).Cells(6).EditedFormattedValue
            Dim performance As Boolean = dgvPDFs.Rows(e.RowIndex).Cells(7).EditedFormattedValue
            Dim isActive As Boolean = dgvPDFs.Rows(e.RowIndex).Cells(8).EditedFormattedValue
            Dim reportname As String = dgvPDFs.Rows(e.RowIndex).Cells("RS_ReportName").EditedFormattedValue
            'valid email value so we save
            UpdatePDFs(pfCode, rfId, emailTo, emailCC, emailBC, performance, isActive, reportname)
            LoadComboBoxForPortfolios()

        End If
    End Sub

    Private Sub dgvClientEmailAddresses_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvClientEmailAddresses.RowLeave

        If Not saveEmails
            Return
        End If

        Dim headerText As String = dgvClientEmailAddresses.Columns(e.ColumnIndex).HeaderText

        If Not headerText.Equals("Email Address") And Not headerText.Equals("Contact Name") Then
            Return
        End If

        If dgvClientEmailAddresses.IsCurrentRowDirty Then

            dgvClientEmailAddresses.EndEdit()

            'Dim cellValue As String
            'cellValue = dgvClientEmailAddresses.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue



            Dim portfolioCode As Integer = dgvClientEmailAddresses.Rows(e.RowIndex).Cells("PF_code").EditedFormattedValue
            Dim ContactName As String = dgvClientEmailAddresses.Rows(e.RowIndex).Cells("PF_AddrContact").EditedFormattedValue
            Dim EmailAddress As String = dgvClientEmailAddresses.Rows(e.RowIndex).Cells("PF_AddrEmail").EditedFormattedValue

            If Not AreValidEmails(EmailAddress) Then
                MessageBox.Show("Value must be a valid email address or else leave blank", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            'valid email value so we save
            UpdateEmailAddresses(portfolioCode, ContactName, EmailAddress)
        End If

    End Sub
    
    Private Sub dgvRptsCsvConfig_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRptsCsvConfig.RowLeave

        If Not saveRptsCsvConfig
            Return
        End If

        If dgvRptsCsvConfig.IsCurrentRowDirty
            
            dgvRptsCsvConfig.EndEdit()

            Dim EmailBCC As String = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(2).EditedFormattedValue
            If EmailBCC = "" Or Not AreValidEmails(EmailBCC) Then
                MessageBox.Show("EmailBCC value must be a valid email address", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim EmailCC As String = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(3).EditedFormattedValue
            If EmailCC = "" Or Not AreValidEmails(EmailCC) Then
                MessageBox.Show("EmailCC value must be a valid email address", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim EmailTo As String = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(4).EditedFormattedValue
            If EmailTo = "" Or Not AreValidEmails(EmailTo) Then
                MessageBox.Show("EmailTo value must be a valid email address", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim EmailToError As String = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(5).EditedFormattedValue
            If EmailToError = "" Or Not AreValidEmails(EmailToError) Then
                MessageBox.Show("EmailToError value must be a valid email address", "Invalid Email", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End If

            Dim custId As Integer = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(0).EditedFormattedValue
            Dim IsCashTransactionReq As Boolean = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(6).EditedFormattedValue
            Dim IsTransactionReq As Boolean = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(7).EditedFormattedValue
            Dim IsValuationReq As Boolean = dgvRptsCsvConfig.Rows(e.RowIndex).Cells(8).EditedFormattedValue
            'valid email value so we save
            UpdateClientCsvConfig(custId, EmailBCC, EmailCC, EmailTo, EmailToError, IsCashTransactionReq, IsTransactionReq, IsValuationReq)
            LoadComboBoxForCsvConfigLoad()

        End If

    End Sub


    Public Sub UpdatePDFs(pfCode As Integer,
                          rfId As Integer,
                          emailTo As String,
                          emailCC As String,
                          emailBC As String,
                          performance As Boolean,
                          isActive As Boolean,
                          reportname As String)

        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.[DolfinPaymentUpdateRptSchedule]"
            Cmd.Parameters.Add("@PFCode", SqlDbType.Int).Value = pfCode
            Cmd.Parameters.Add("@RFId", SqlDbType.Int).Value = rfId
            Cmd.Parameters.Add("@EmailTo", SqlDbType.VarChar).Value = emailTo
            Cmd.Parameters.Add("@EmailCC", SqlDbType.VarChar).Value = emailCC
            Cmd.Parameters.Add("@EmailBCC", SqlDbType.VarChar).Value = emailBC
            Cmd.Parameters.Add("@Performance ", SqlDbType.Bit).Value = performance
            Cmd.Parameters.Add("@IsActive ", SqlDbType.Bit).Value = isActive
            Cmd.Parameters.Add("@ReportName ", SqlDbType.VarChar).Value = reportname
            Cmd.Parameters.Add("@ModifiedBy ", SqlDbType.VarChar).Value = $"RMSCAPITAL\{Environment.UserName}"
            Cmd.ExecuteNonQuery()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameClientPDFReports, $"exec dbo.DolfinPaymentUpdateRptCsvConfig {pfCode} {emailTo} {emailCC} {emailCC} {performance} {isActive} ")

        Catch ex As SqlException

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientPDFReports, $"exec dbo.DolfinPaymentUpdateRptCsvConfig {pfCode} {emailTo} {emailCC} {emailCC} {performance} {isActive} {ex.Message}")

        End Try
    End Sub

    Public Sub UpdateEmailAddresses(PortfolioCode As Integer, ContactName As String, emailAddresses As String)
        ' TODO: Add treasury move
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.DolfinPaymentUpdatePortfolioEmailAddress"
            Cmd.Parameters.Add("@PF_Code", SqlDbType.Int).Value = PortfolioCode
            Cmd.Parameters.Add("@ContactName", SqlDbType.VarChar).Value = ContactName
            Cmd.Parameters.Add("@EmailAddresses", SqlDbType.VarChar).Value = emailAddresses
            Cmd.ExecuteNonQuery()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameClientEmailAddresses, $"exec dbo.DolfinPaymentUpdatePortfolioEmailAddress {PortfolioCode} {ContactName} {emailAddresses} ")

        Catch ex As SqlException
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientEmailAddresses, $"exec dbo.DolfinPaymentUpdatePortfolioEmailAddress {PortfolioCode} {ContactName} {emailAddresses} {ex.Message}")
        End Try
    End Sub

    Public Sub UpdateClientCsvConfig(custId As Integer, 
                               emailBCC As String, 
                               emailCC As String, 
                               emailTo As String, 
                               emailToError As String, 
                               isCashTransactionReq As Boolean, 
                               isTransactionReq As Boolean, 
                               isValuationReq As Boolean)

        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.DolfinPaymentUpdateRptCsvConfig"
            Cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = custId
            Cmd.Parameters.Add("@EmailBCC ", SqlDbType.VarChar).Value = emailBCC
            Cmd.Parameters.Add("@EmailCC", SqlDbType.VarChar).Value = emailCC
            Cmd.Parameters.Add("@EmailTo", SqlDbType.VarChar).Value = emailTo
            Cmd.Parameters.Add("@EmailToError", SqlDbType.VarChar).Value = emailToError
            Cmd.Parameters.Add("@isCashTransactionReq ", SqlDbType.Bit).Value = isCashTransactionReq
            Cmd.Parameters.Add("@isTransactionReq ", SqlDbType.Bit).Value = isTransactionReq
            Cmd.Parameters.Add("@isValuationReq ", SqlDbType.Bit).Value = isValuationReq
            Cmd.Parameters.Add("@ModifiedBy ", SqlDbType.VarChar).Value = $"RMSCAPITAL\{Environment.UserName}"
            Cmd.ExecuteNonQuery()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameClientRptCsvConfig, $"exec dbo.DolfinPaymentUpdateRptCsvConfig {custId} {emailBCC} {emailCC} {emailTo} {emailToError} {isCashTransactionReq} {isTransactionReq} {isValuationReq} ")

        Catch ex As SqlException
        
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientRptCsvConfig, $"exec dbo.DolfinPaymentUpdateRptCsvConfig {custId} {emailBCC} {emailCC} {emailTo} {emailToError} {isCashTransactionReq} {isTransactionReq} {isValuationReq} {ex.Message}")
        
        End Try
    End Sub

    Private Sub CmdEmailFilter_Click(sender As Object, e As EventArgs) Handles CmdEmailFilter.Click

        Dim varWhereClauseFilter As String = "where PF_Active = 1 and (ISNULL(PF_AddrEmail,'')<>'' OR ISNULL(PF_AddrContact,'')<>'' or ISNULL(PF_AddrStreet,'')<>'') "

        Cursor = Cursors.WaitCursor

        If cboPortfolioName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and Portfolio ='" & cboPortfolioName.Text & "'"
        End If

        If cboShortcut.Text <> "" Then
            If varWhereClauseFilter.Length > 6
                varWhereClauseFilter = varWhereClauseFilter & " and "
            End If
            varWhereClauseFilter = varWhereClauseFilter & " PF_Shortcut1 ='" & cboShortcut.Text & "'"
        End If

        If varWhereClauseFilter <> "where" Then
            CEMLoadGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            CEMLoadGrid()
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub cmdCsvFilter_Click(sender As Object, e As EventArgs) Handles cmdCsvFilter.Click

        Dim varWhereClauseFilter As String = "where IsCustomerActive = 1 and"

        Cursor = Cursors.WaitCursor

        If cboFullName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " _Dolfin_ClientListWithDetail.FullName ='" & cboFullName.Text & "'"
        End If

        If varWhereClauseFilter <> "where" Then
            CsvConfigLoadGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            CsvConfigLoadGrid()
        End If
        Cursor = Cursors.Default

    End Sub

    Private Sub cmdPdfFilter_Click(sender As Object, e As EventArgs) Handles cmdPdfFilter.Click

        Dim varWhereClauseFilter = ""

        Cursor = Cursors.WaitCursor

        If cboPDFPortfolioName.Text <> "" Then
            varWhereClauseFilter = " AND RS_PFCode =" & cboPDFPortfolioName.SelectedValue
        End If

        If varWhereClauseFilter.Length > 0 Then
            PDFLoadGrid(varWhereClauseFilter)
        Else
            PDFLoadGrid()
        End If

        Cursor = Cursors.Default

    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshEmailView.Click

        cboPortfolioName.Text = ""
        cboShortcut.Text = ""
        CEMLoadGrid()

    End Sub

    Private Sub cmdRefreshCsvConfigView_Click(sender As Object, e As EventArgs) Handles cmdRefreshCsvConfigView.Click
        cboFullName.Text = ""
        CsvConfigLoadGrid()
    End Sub

    Private Sub cmdRefreshPdfView_Click(sender As Object, e As EventArgs) Handles cmdRefreshPdfView.Click 

        cboPDFPortfolioName.Text = ""
        PDFLoadGrid()

    End Sub

    Private Sub CmdExportEmailsToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportEmailsToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvClientEmailAddresses, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExportRptConfigToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportRptConfigToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvRptsCsvConfig, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExportRptScheduleToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportRptSchedule.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvPDFs, 0)
        Cursor = Cursors.Default
    End Sub
    
    #Region "commonhelpers"

    Function AreValidEmails(ByVal emailAddresses As String) As Boolean

        If emailAddresses=""
            Return True
        End If

        Dim emailsArray() As String = emailAddresses.Split(";")

        For Each email In emailsArray
            Dim validEmail As Boolean = IsValidEmail(email.Trim())
            If Not validEmail Then
                Return False
            End If
        Next

        Return True
    End Function

    Function IsValidEmail(ByVal emailAddress As String) As Boolean

        If emailAddress = "" Then
            Return True
        End If

        Dim regExPattern As String = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, regExPattern)

        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If

    End Function

    Private Sub dgvPDFs_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dgvPDFs.EditingControlShowing 


        If dgvPDFs.CurrentCell.OwningColumn.Name = "Portfolio" Then
            Dim combo = TryCast(e.Control, ComboBox)
            If combo IsNot Nothing Then
                RemoveHandler combo.SelectedIndexChanged, AddressOf combo_SelectedIndexChangedPortfolio
                AddHandler combo.SelectedIndexChanged, AddressOf combo_SelectedIndexChangedPortfolio
            End If
        End If

        If dgvPDFs.CurrentCell.OwningColumn.Name = "Report Frequency" Then
            Dim combo = TryCast(e.Control, ComboBox)
            If combo IsNot Nothing Then
                RemoveHandler combo.SelectedIndexChanged, AddressOf combo_SelectedIndexChangedReportFrequency
                AddHandler combo.SelectedIndexChanged, AddressOf combo_SelectedIndexChangedReportFrequency
            End If
        End If

    End Sub

    Private Sub dgvRptsCsvConfig_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dgvRptsCsvConfig.EditingControlShowing

        
        If dgvRptsCsvConfig.CurrentCell.ColumnIndex = 9 And e.Control.GetType() = GetType(DataGridViewComboBoxEditingControl)
            
            Dim comboBox As ComboBox = CType(e.Control, ComboBox)
            RemoveHandler  comboBox.SelectedIndexChanged, AddressOf combox_SelectedIndexChanged
            AddHandler  comboBox.SelectedIndexChanged, AddressOf combox_SelectedIndexChanged
        
        End If

    End Sub

    Private Sub combox_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim  currentCell = dgvRptsCsvConfig.CurrentCellAddress
        Dim sendingCb =  CType(sender,DataGridViewComboBoxEditingControl)

        Dim cel = CType(dgvRptsCsvConfig.Rows(currentCell.Y).Cells(0), DataGridViewTextBoxCell)
        cel.Value = sendingCb.SelectedValue

    End Sub

    Private Sub combo_SelectedIndexChangedPortfolio(ByVal sender As Object, ByVal e As EventArgs)


        If dgvPDFs.CurrentCell.OwningColumn.Name = "Portfolio" Then
            Dim cb As ComboBox = DirectCast(sender, ComboBox)
            dgvPDFs.CurrentRow.Cells(0).Value = cb.SelectedValue
        End If


    End Sub


    Private Sub combo_SelectedIndexChangedReportFrequency(ByVal sender As Object, ByVal e As EventArgs)

        If dgvPDFs.CurrentCell.OwningColumn.Name = "Report Frequency" Then
            Dim cb As ComboBox = DirectCast(sender, ComboBox)
            dgvPDFs.CurrentRow.Cells(0).Value = cb.SelectedValue
        End If

    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

#End Region

End Class