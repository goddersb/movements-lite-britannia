﻿Public Class FrmConfirmReceipt
    Dim SCConn As SqlClient.SqlConnection
    Dim dgvdata As SqlClient.SqlDataAdapter
    Dim dgvdataPortfolio As SqlClient.SqlDataAdapter
    Dim CmdConfirmReceipt As DataGridViewButtonColumn
    Dim cboPortfolioColumn As DataGridViewComboBoxColumn
    Dim ds As New DataSet

    Private Sub CERLoadGrid(ByVal varFirstLoad As Boolean)
        Dim varSQL As String = "", varColSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor

            If Not ChkShowAll.Checked Then
                varSQL = "SELECT * from vwDolfinPaymentConfirmReceiptsAll 
                            WHERE (IncomingFundsType = 'MANUAL' and (Status not in ('Cancelled','Confirm Acknowledged') or 
														(Status in ('Cancelled','Confirm Acknowledged') and cast(ModifiedTime as date)= cast(getdate() as date)))) or
						            (IncomingFundsType = 'SWIFT' and isnull(SR_IMSDocNoIn,'')='') order by ReceivedDate desc"
            Else
                varSQL = "SELECT top 1000 * from vwDolfinPaymentConfirmReceiptsAll order by ReceivedDate desc"
            End If

            dgvCER.DataSource = Nothing
            SCConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, SCConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentConfirmReceipts")
            dgvCER.AutoGenerateColumns = True
            dgvCER.DataSource = ds.Tables("vwDolfinPaymentConfirmReceipts")

            varColSQL = "Select 0 as pf_code,'' as Portfolio union Select pf_code,Portfolio from vwDolfinPaymentSelectAccount where pf_active = 1 and b_code=222 And cr_code In (Select distinct cr_code from vwDolfinPaymentSelectAccount where pf_code=288 And b_code=222) group by pf_code,Portfolio order by Portfolio"
            dgvdataPortfolio = New SqlClient.SqlDataAdapter(varColSQL, SCConn)
            dgvdataPortfolio.Fill(ds, "vwDolfinPaymentPortfolio")
            cboPortfolioColumn = New DataGridViewComboBoxColumn
            cboPortfolioColumn.HeaderText = "Portfolio"
            cboPortfolioColumn.DataPropertyName = "Pf_Code"
            cboPortfolioColumn.DataSource = ds.Tables("vwDolfinPaymentPortfolio")
            cboPortfolioColumn.ValueMember = ds.Tables("vwDolfinPaymentPortfolio").Columns(0).ColumnName
            cboPortfolioColumn.DisplayMember = ds.Tables("vwDolfinPaymentPortfolio").Columns(1).ColumnName
            cboPortfolioColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboPortfolioColumn.FlatStyle = FlatStyle.Flat

            dgvCER.Columns.Insert(3, cboPortfolioColumn)
            dgvCER.Columns(3).Width = 300

            dgvCER.Columns("Pf_Code").Visible = False
            dgvCER.Columns("SR_ID").Visible = False
            dgvCER.Columns("ReceivedDate").Visible = False
            dgvCER.Columns("PortfolioID").Visible = False
            dgvCER.Columns("AccountNo").Visible = False
            dgvCER.Columns("Portfolio").Visible = False
            dgvCER.Columns("Ref").Visible = False
            dgvCER.Columns("SR_Reference").Visible = False
            dgvCER.Columns("SR_IMSDocNoIn").Visible = False
            dgvCER.Columns("CRM_IMSDocNoOut").Visible = False
            dgvCER.Columns("CCYCode").Visible = False
            dgvCER.Columns("CRM_AuthorisedDate").Visible = False

            dgvCER.Columns("Sender").Width = 300
            dgvCER.Columns("Status").Width = 150
            dgvCER.Columns("Amount").Width = 100
            dgvCER.Columns("TradeDate").Width = 80
            dgvCER.Columns("SettleDate").Width = 80
            dgvCER.Columns("IncomingFundsType").Width = 60
            dgvCER.Columns("IncomingFundsType").HeaderText = "Type"
            dgvCER.Columns("SenderBankInstitution").Width = 300
            dgvCER.Columns("SenderBankInstitution").HeaderText = "Sender Info"
            dgvCER.Columns("AmountCCY").Width = 50
            dgvCER.Columns("AmountCCY").HeaderText = "CCY"
            dgvCER.Columns("filename").Width = 200
            dgvCER.Columns("filename").HeaderText = "Filename"
            dgvCER.Columns("Notes").Width = 400

            dgvCER.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvCER.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgvCER.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)

            dgvCER.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCER.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            'dgvCER.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameConfirmReceipts, Err.Description & " - Problem With loading confirm receipts grid")
        End Try
    End Sub

    Private Sub PRLoadGrid(ByVal varFirstLoad As Boolean, Optional ByVal varFilter As String = "")
        Dim varSQL As String = "", varColSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor

            If varFilter <> "" Then
                varSQL = "Select * from vwDolfinPaymentGridViewLitePaymentRequestAll " & varFilter
            Else
                varSQL = "Select * from vwDolfinPaymentGridViewLitePaymentRequest"
            End If

            If ClsIMS.UserOptGridSort = 0 Then
                varSQL = varSQL & " order by orderval, TransDate desc"
            Else
                varSQL = varSQL & " order by id desc, TransDate desc"
            End If

            dgvPR.DataSource = Nothing
            SCConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, SCConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentGridViewLitePaymentRequest")
            dgvPR.AutoGenerateColumns = True
            dgvPR.DataSource = ds.Tables("vwDolfinPaymentGridViewLitePaymentRequest")

            dgvPR.Columns("ID").Width = 60
            dgvPR.Columns("Status").Width = 150
            dgvPR.Columns("Portfolio").Width = 250
            dgvPR.Columns("Amount").Width = 100
            dgvPR.Columns("CCY").Width = 60
            dgvPR.Columns("TransDate").Width = 80
            dgvPR.Columns("OrderRef").Width = 100
            dgvPR.Columns("PaymentType").Width = 150
            dgvPR.Columns("PayRequestEmail").Width = 500
            dgvPR.Columns("BeneficiaryName").Width = 300
            dgvPR.Columns("EmailSent").Width = 70
            dgvPR.Columns("ManualPay").Width = 70
            dgvPR.Columns("OrderAccount").Width = 200
            dgvPR.Columns("OrderBalance").Width = 100
            dgvPR.Columns("P_CCYCode").Visible = False
            dgvPR.Columns("P_PortfolioCode").Visible = False
            dgvPR.Columns("TemplateName").Visible = False
            dgvPR.Columns("PayRequestType").Visible = False
            dgvPR.Columns("PayRequestRate").Visible = False
            dgvPR.Columns("BenPortfolio").Visible = False
            dgvPR.Columns("P_BenPortfolioCode").Visible = False
            dgvPR.Columns("P_CustomerID").Visible = False
            dgvPR.Columns("orderval").Visible = False
            dgvPR.Columns("PortfolioCode").Visible = False
            dgvPR.Columns("CCYCode").Visible = False
            dgvPR.Columns("Comments").Visible = False
            dgvPR.Columns("PortfolioFeeID").Visible = False
            dgvPR.Columns("SendSwift").Visible = False
            dgvPR.Columns("SendIMS").Visible = False
            dgvPR.Columns("IMSAcknowledged").Visible = False
            dgvPR.Columns("SwiftAcknowledged").Visible = False
            dgvPR.Columns("PaidAcknowledged").Visible = False

            Dim varDept As String = ClsIMS.GetSQLItem("Select U_Department from vwDolfinPaymentUsers where u_username = '" & ClsIMS.FullUserName & "'")

            'Can only selected ops if in ops dept or has permissions for paying fees U_PayFeesPaid
            If varDept = "Finance" Or varDept = "Operations" Then
                dgvPR.Columns("EmailSent").Visible = True
                dgvPR.Columns("ManualPay").Visible = True
                dgvPR.Columns("OrderAccount").Visible = True
                dgvPR.Columns("OrderBalance").Visible = True
            Else
                dgvPR.Columns("EmailSent").Visible = False
                dgvPR.Columns("ManualPay").Visible = False
                dgvPR.Columns("OrderAccount").Visible = False
                dgvPR.Columns("OrderBalance").Visible = False
            End If

            dgvPR.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPR.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvPR.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameConfirmReceipts, Err.Description & " - Problem with viewing payment request grid")
        End Try
    End Sub

    Private Sub FrmConfirmReceipt_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PopulateCbo(cboPRPortfolio, "Select 0 As P_PortfolioCode, '' as Portfolio union Select P_PortfolioCode,Portfolio from vwDolfinPaymentGridViewLitePaymentRequestAll where BranchCode = " & ClsIMS.UserLocationCode & " and [Statusid] not in (4) and Portfolio is not null group by P_PortfolioCode,Portfolio order by Portfolio")
        PopulateCbo(cboPRCCY, "Select 0 as P_CCYCode, '' as CCY union select P_CCYCode,CCY from vwDolfinPaymentGridViewLitePaymentRequestAll where BranchCode = " & ClsIMS.UserLocationCode & " group by P_CCYCode,CCY order by CCY")
        PopulateCbo(cboPROrderRef, "Select 0 as OrderRefID, '' as OrderRef union Select 0 as OrderRefID,OrderRef from vwDolfinPaymentGridViewLitePaymentRequestAll where BranchCode = " & ClsIMS.UserLocationCode & " group by OrderRef order by OrderRef")
        ModRMS.DoubleBuffered(dgvPR, True)
        ModRMS.DoubleBuffered(dgvCER, True)
        PRLoadGrid(True)
        CERLoadGrid(True)
        RDLoadGrid()
    End Sub

    Public Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub

    Private Sub cmdCERRefresh_Click(sender As Object, e As EventArgs) Handles cmdCERRefresh.Click
        CERLoadGrid(False)
        AssignDataLinkToFilename()
    End Sub

    Private Sub dgvCER_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCER.CellMouseClick
        If e.ColumnIndex = 4 And e.RowIndex >= 0 Then
            If Not IsDBNull(dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                If dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "FileName" Then
                    If IO.File.Exists(ClsIMS.GetFilePath("SWIFTImport") & dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                        Process.Start(ClsIMS.GetFilePath("SWIFTImport") & dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub AssignDataLinkToFilename()
        For i As Integer = 0 To dgvCER.RowCount - 1
            dgvCER(dgvCER.Columns("filename").Index, i) = New DataGridViewLinkCell
        Next
    End Sub

    Private Sub dgvCER_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCER.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvCER, e.RowIndex)
        End If
    End Sub

    Private Sub cmdNewPR_Click(sender As Object, e As EventArgs) Handles cmdNewPR.Click
        NewPaymentRequest = New FrmPaymentRequest
        ClsPR = New ClsPaymentRequest
        NewPaymentRequest.ShowDialog()
        PRLoadGrid(False)
        RDLoadGrid()
    End Sub

    Private Sub dgvPR_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvPR.CellFormatting
        If dgvPR.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "Status" Then
            FormatGridStatus(dgvPR, e.RowIndex)
            'ElseIf dgvPR.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "OrderBalance" Then
            '   If dgvPR.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusReady) Or
            '  dgvPR.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingAuth) Or
            ' dgvPR.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingReview) Then
            '     FormatGridPaymentBalance(dgvPR, e.RowIndex, e.ColumnIndex)
            'Else
            '   dgvPR.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.ForeColor = Color.Black
            'End If
        End If
    End Sub

    Private Sub dgvCER_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCER.RowHeaderMouseDoubleClick
        Try
            ClsCRM = New ClsConfirmReceipt
            ClsIMS.GetReaderItemIntoCRMClass(dgvCER.Rows(dgvCER.SelectedRows.Item(0).Index).Cells("ID").Value)
            Dim NewCRM = New FrmConfirmReceiptsOptions
            NewCRM.ShowDialog()
            If ChkRefresh.Checked Then
                CERLoadGrid(False)
                AssignDataLinkToFilename()
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvPR.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameConfirmReceipts, Err.Description & " dgvPR_RowHeaderMouseDoubleClick - Problem with viewing payment request grid")
        End Try
    End Sub

    Private Sub TimerRefresh_Tick(sender As Object, e As EventArgs) Handles TimerRefresh.Tick
        If ChkRefresh.Checked Then
            Cursor = Cursors.WaitCursor
            PRLoadGrid(False)
            CERLoadGrid(False)
            RDLoadGrid()
            AssignDataLinkToFilename()
            Cursor = Cursors.Default
        End If
    End Sub

    Private Sub dgvPR_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvPR.RowHeaderMouseDoubleClick
        Try
            ClsPR = New ClsPaymentRequest
            ClsIMS.GetReaderItemIntoPRClass(dgvPR.Rows(dgvPR.SelectedRows.Item(0).Index).Cells(0).Value)
            Dim NewPRA = New FrmPaymentrequestAuthorise
            NewPRA.ShowDialog()
            'ClsIMS.UpdatePendingMovements()
            If ChkRefresh.Checked Then
                PRLoadGrid(False)
            End If
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvPR.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameConfirmReceipts, Err.Description & " dgvPR_RowHeaderMouseDoubleClick - Problem with viewing payment request grid")
        End Try
    End Sub

    Private Sub cmdPRSearch_Click(sender As Object, e As EventArgs) Handles cmdPRSearch.Click
        Dim varWhereClauseFilter As String = ""

        Cursor = Cursors.WaitCursor
        If cboPRPortfolio.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [Portfolio] in ('" & cboPRPortfolio.Text & "')"
        End If

        If cboPRCCY.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [CCY] in ('" & cboPRCCY.Text & "')"
        End If

        If cboPROrderRef.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ([OrderRef] = '" & cboPROrderRef.Text & "' or [ID] = case when isnumeric('" & cboPROrderRef.Text & "') = 1 then '" & cboPROrderRef.Text & "' else 0 end)"
        End If

        If Strings.Left(varWhereClauseFilter, 4) = " and" Then
            PRLoadGrid(False, "where " & Strings.Right(varWhereClauseFilter, Strings.Len(varWhereClauseFilter) - 4))
        Else
            PRLoadGrid(False)
        End If

        cboPRPortfolio.Text = ""
        cboPRCCY.Text = ""
        cboPROrderRef.Text = ""
        Cursor = Cursors.Default
    End Sub

    Private Sub cmdPRRefresh_Click(sender As Object, e As EventArgs) Handles cmdPRRefresh.Click
        Cursor = Cursors.WaitCursor
        ClsIMS.UpdatePendingMovements()
        PRLoadGrid(False)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdRunPassword_Click(sender As Object, e As EventArgs) Handles CmdRunPassword.Click
        Dim NewPG = New FrmPasswordGenerator
        NewPG.ShowDialog()
    End Sub

    Private Sub CmdFOPRefresh_Click(sender As Object, e As EventArgs) Handles CmdFOPRefresh.Click
        RDLoadGrid()
    End Sub

    Private Sub RDLoadGrid()
        ClsIMS.UpdatePendingMovementsRD()
        ClsIMS.RefreshRDGrid(dgvFOP, , True)
        FormatRDGrid(dgvFOP)
    End Sub

    Private Sub FormatRDGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            dgv.Columns("ID").Width = 60
            dgv.Columns("Status").Width = 150
            dgv.Columns("ReceiveDeliver").Width = 100
            dgv.Columns("OrderRef").Width = 100
            dgv.Columns("Portfolio").Width = 225
            dgv.Columns("InstISIN").Width = 100
            dgv.Columns("InstName").Width = 150
            dgv.Columns("TransDate").Width = 80
            dgv.Columns("SettleDate").Width = 80
            dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgv.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)
            dgv.Columns("CCYName").Width = 60
        End If
    End Sub

    Private Sub dgvFOP_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvFOP.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvFOP, e.RowIndex)
        End If
    End Sub

    Private Sub dgvFOP_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvFOP.RowHeaderMouseDoubleClick
        ClsRD = New ClsReceiveDeliver
        ClsIMS.GetReaderItemIntoRDClass(dgvFOP.Rows(dgvFOP.SelectedRows.Item(0).Index).Cells(0).Value)
        Dim NewPRFO = New FrmPaymentRequestFOPOption
        NewPRFO.ShowDialog()
        If ChkRefresh.Checked Then
            RDLoadGrid()
        End If
    End Sub

    Private Sub Cmd_Click(sender As Object, e As EventArgs) Handles Cmd.Click
        Dim NewCRMAF = New FrmConfirmReceiptsAddFunds
        NewCRMAF.ShowDialog()
        If ChkRefresh.Checked Then
            CERLoadGrid(False)
            AssignDataLinkToFilename()
        End If
    End Sub

    Private Sub dgvCER_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCER.CellEndEdit
        Dim varResponse As Integer

        If dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "" And dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.DataPropertyName = "Pf_Code" Then
            If Not IsDBNull(dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                If IsNumeric(dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                    If dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value <> 0 Then
                        'If ClsIMS.GetSQLItem("select top 1 t_code from vwDolfinPaymentSelectAccount where b_code = 222 and cr_code = " & dgvCER.Rows(e.RowIndex).Cells("CCYCode").Value & " and pf_code = " & dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) <> "" Then
                        If dgvCER.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingAccountMgt) And (ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgt) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgtLevel2)) Then
                            varResponse = MsgBox("Are you sure you want to assign " & dgvCER.Rows(e.RowIndex).Cells("Amount").Value & " " & dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value & " to " & dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).FormattedValue & "?", vbYesNo + vbQuestion, "Are you sure")
                            If varResponse = vbYes Then
                                If dgvCER.Rows(e.RowIndex).Cells("IncomingFundsType").Value = "SWIFT" Then
                                    ClsIMS.ConfirmReceipt(dgvCER.Rows(e.RowIndex).Cells("ID").Value, dgvCER.Rows(e.RowIndex).Cells("Sender").Value, dgvCER.Rows(e.RowIndex).Cells("PF_Code").Value, dgvCER.Rows(e.RowIndex).Cells("TradeDate").Value,
                                      dgvCER.Rows(e.RowIndex).Cells("SettleDate").Value, dgvCER.Rows(e.RowIndex).Cells("Amount").Value, dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value)
                                Else
                                    ClsIMS.UpdateFundsPortfolio(dgvCER.Rows(e.RowIndex).Cells("ID").Value, dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                                End If
                                Dim LstRecipients As New List(Of String)
                                LstRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('paymentsteam')")
                                ClsIMS.GetReaderItemIntoCRMClass(dgvCER.Rows(e.RowIndex).Cells("ID").Value)
                                ClsEmail.SendApproveMovementEmailReceipts(LstRecipients, True)
                                CERLoadGrid(False)
                                MsgBox("Funds have been assigned and the payment team have been alerted to authorise these funds", vbInformation, "Funds Assigned")
                            Else
                                dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 0
                            End If
                        Else
                            MsgBox("You are not authorised to assign a portfolio against these incoming funds", vbExclamation, "Cannot select portfolio")
                            dgvCER.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = 0
                        End If
                        'Else
                        'MsgBox("Please request Ops to create a Lloyds account against this portfolio for this currency in IMS before selecting this portfolio for incoming funds", vbExclamation, "Cannot select portfolio")
                        'End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub CmdExcelFOP_Click(sender As Object, e As EventArgs) Handles CmdExcelFOP.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvFOP, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExcelCER_Click(sender As Object, e As EventArgs) Handles CmdExcelCER.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvCER, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExcelPR_Click(sender As Object, e As EventArgs) Handles CmdExcelPR.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvPR, 0)
        Cursor = Cursors.Default
    End Sub

End Class