﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEmail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.LstSelectRecipients = New System.Windows.Forms.ListBox()
        Me.ChkAddFiles = New System.Windows.Forms.CheckBox()
        Me.txtSelectRecipients = New System.Windows.Forms.TextBox()
        Me.lblMsg = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LstEmailRecipients = New System.Windows.Forms.ListBox()
        Me.CmdAdd = New System.Windows.Forms.Button()
        Me.CmdRemove = New System.Windows.Forms.Button()
        Me.CmdSend = New System.Windows.Forms.Button()
        Me.ChkCCMe = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblTitle.Location = New System.Drawing.Point(244, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(221, 24)
        Me.lblTitle.TabIndex = 10
        Me.lblTitle.Text = "Send Email Recipients"
        '
        'LstSelectRecipients
        '
        Me.LstSelectRecipients.FormattingEnabled = True
        Me.LstSelectRecipients.Location = New System.Drawing.Point(27, 126)
        Me.LstSelectRecipients.Name = "LstSelectRecipients"
        Me.LstSelectRecipients.Size = New System.Drawing.Size(289, 225)
        Me.LstSelectRecipients.TabIndex = 11
        '
        'ChkAddFiles
        '
        Me.ChkAddFiles.AutoSize = True
        Me.ChkAddFiles.Checked = True
        Me.ChkAddFiles.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkAddFiles.Location = New System.Drawing.Point(27, 358)
        Me.ChkAddFiles.Name = "ChkAddFiles"
        Me.ChkAddFiles.Size = New System.Drawing.Size(300, 17)
        Me.ChkAddFiles.TabIndex = 12
        Me.ChkAddFiles.Text = "Add files attached to this payment as attachment to email?"
        Me.ChkAddFiles.UseVisualStyleBackColor = True
        '
        'txtSelectRecipients
        '
        Me.txtSelectRecipients.Location = New System.Drawing.Point(27, 101)
        Me.txtSelectRecipients.Name = "txtSelectRecipients"
        Me.txtSelectRecipients.Size = New System.Drawing.Size(289, 20)
        Me.txtSelectRecipients.TabIndex = 13
        '
        'lblMsg
        '
        Me.lblMsg.AutoSize = True
        Me.lblMsg.Location = New System.Drawing.Point(24, 52)
        Me.lblMsg.Name = "lblMsg"
        Me.lblMsg.Size = New System.Drawing.Size(350, 13)
        Me.lblMsg.TabIndex = 14
        Me.lblMsg.Text = "Type email below or select user from listbox to receive authorisation email"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 85)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(116, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Selection of Recipeints"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(401, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Email Recipeints"
        '
        'LstEmailRecipients
        '
        Me.LstEmailRecipients.FormattingEnabled = True
        Me.LstEmailRecipients.Location = New System.Drawing.Point(402, 101)
        Me.LstEmailRecipients.Name = "LstEmailRecipients"
        Me.LstEmailRecipients.Size = New System.Drawing.Size(289, 251)
        Me.LstEmailRecipients.TabIndex = 17
        '
        'CmdAdd
        '
        Me.CmdAdd.Location = New System.Drawing.Point(322, 126)
        Me.CmdAdd.Name = "CmdAdd"
        Me.CmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.CmdAdd.TabIndex = 18
        Me.CmdAdd.Text = "Add >>"
        Me.CmdAdd.UseVisualStyleBackColor = True
        '
        'CmdRemove
        '
        Me.CmdRemove.Location = New System.Drawing.Point(322, 328)
        Me.CmdRemove.Name = "CmdRemove"
        Me.CmdRemove.Size = New System.Drawing.Size(75, 23)
        Me.CmdRemove.TabIndex = 19
        Me.CmdRemove.Text = "<< Remove"
        Me.CmdRemove.UseVisualStyleBackColor = True
        '
        'CmdSend
        '
        Me.CmdSend.Location = New System.Drawing.Point(513, 377)
        Me.CmdSend.Name = "CmdSend"
        Me.CmdSend.Size = New System.Drawing.Size(178, 23)
        Me.CmdSend.TabIndex = 20
        Me.CmdSend.Text = "Send Authorisation Email"
        Me.CmdSend.UseVisualStyleBackColor = True
        '
        'ChkCCMe
        '
        Me.ChkCCMe.AutoSize = True
        Me.ChkCCMe.Checked = True
        Me.ChkCCMe.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCCMe.Location = New System.Drawing.Point(27, 381)
        Me.ChkCCMe.Name = "ChkCCMe"
        Me.ChkCCMe.Size = New System.Drawing.Size(133, 17)
        Me.ChkCCMe.TabIndex = 21
        Me.ChkCCMe.Text = "CC myself for this email"
        Me.ChkCCMe.UseVisualStyleBackColor = True
        '
        'FrmEmail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(703, 410)
        Me.Controls.Add(Me.ChkCCMe)
        Me.Controls.Add(Me.CmdSend)
        Me.Controls.Add(Me.CmdRemove)
        Me.Controls.Add(Me.CmdAdd)
        Me.Controls.Add(Me.LstEmailRecipients)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.txtSelectRecipients)
        Me.Controls.Add(Me.ChkAddFiles)
        Me.Controls.Add(Me.LstSelectRecipients)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmEmail"
        Me.Text = "Email Recipients"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents LstSelectRecipients As ListBox
    Friend WithEvents ChkAddFiles As CheckBox
    Friend WithEvents txtSelectRecipients As TextBox
    Friend WithEvents lblMsg As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents LstEmailRecipients As ListBox
    Friend WithEvents CmdAdd As Button
    Friend WithEvents CmdRemove As Button
    Friend WithEvents CmdSend As Button
    Friend WithEvents ChkCCMe As CheckBox
End Class
