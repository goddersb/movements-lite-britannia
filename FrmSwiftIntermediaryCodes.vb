﻿Public Class FrmSwiftIntermediaryCodes
    Dim SICConn As SqlClient.SqlConnection
    Dim dgvdata As SqlClient.SqlDataAdapter
    Dim dgvdataBank As SqlClient.SqlDataAdapter
    Dim dgvdataIBank As SqlClient.SqlDataAdapter
    Dim cboBankColumn As DataGridViewComboBoxColumn
    Dim cboBankCCYColumn As DataGridViewComboBoxColumn
    Dim cboIBankColumn As DataGridViewComboBoxColumn
    Dim ds As New DataSet

    Private Sub SCLoadGrid(Optional ByVal varFilterWhereClause As String = "")
        Dim varSQL As String = "", varColSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If varFilterWhereClause = "" Then
                varSQL = "SELECT *, BankName + ', ' + BankCity as BankFullName, IBankName + ', ' + IBankCity as IBankFullName FROM vwDolfinPaymentSwiftCodesIntermediaryBankLinks order by BankSwiftCode,BankCCY,IBankSwiftCode"
            Else
                varSQL = "SELECT *, BankName + ', ' + BankCity as BankFullName, IBankName + ', ' + IBankCity as IBankFullName FROM vwDolfinPaymentSwiftCodesIntermediaryBankLinks " & varFilterWhereClause & " order by BankSwiftCode,BankCCY,IBankSwiftCode"
            End If

            dgvSwiftIntermediaryCodes.DataSource = Nothing
            SICConn = ClsIMS.GetNewOpenConnection

            'varColSQL = "Select SC_SwiftCode as IBankSwiftCode FROM vwDolfinPaymentSwiftCodes order by IBankSwiftCode"
            varColSQL = "Select SC_SwiftCode as IBankSwiftCode FROM vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') 
                            union Select top 5000 SC_SwiftCode as IBankSwiftCode FROM vwDolfinPaymentSwiftCodes where SC_Source in ('UPLOAD') order by IBankSwiftCode"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SICConn)
            dgvdataBank.Fill(ds, "IBank")
            cboIBankColumn = New DataGridViewComboBoxColumn
            cboIBankColumn.HeaderText = "Intermediary Bank Swift Code"
            cboIBankColumn.DataPropertyName = "IBankSwiftCode"
            cboIBankColumn.DataSource = ds.Tables("IBank")
            cboIBankColumn.ValueMember = ds.Tables("IBank").Columns(0).ColumnName
            cboIBankColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboIBankColumn.FlatStyle = FlatStyle.Flat
            dgvSwiftIntermediaryCodes.Columns.Insert(0, cboIBankColumn)

            varColSQL = "Select CR_Name1 as BankCCY FROM vwDolfinPaymentCurrencies"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SICConn)
            dgvdataBank.Fill(ds, "vwDolfinPaymentCurrencies")
            cboBankCCYColumn = New DataGridViewComboBoxColumn
            cboBankCCYColumn.HeaderText = "CCY"
            cboBankCCYColumn.DataPropertyName = "BankCCY"
            cboBankCCYColumn.DataSource = ds.Tables("vwDolfinPaymentCurrencies")
            cboBankCCYColumn.ValueMember = ds.Tables("vwDolfinPaymentCurrencies").Columns(0).ColumnName
            cboBankCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboBankCCYColumn.FlatStyle = FlatStyle.Flat
            dgvSwiftIntermediaryCodes.Columns.Insert(0, cboBankCCYColumn)

            'varColSQL = "Select SC_SwiftCode as BankSwiftCode FROM vwDolfinPaymentSwiftCodes order by BankSwiftCode"
            varColSQL = "Select SC_SwiftCode as BankSwiftCode FROM vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') 
                            union Select top 5000 SC_SwiftCode as BankSwiftCode FROM vwDolfinPaymentSwiftCodes where SC_Source in ('UPLOAD') order by BankSwiftCode"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SICConn)
            dgvdataBank.Fill(ds, "Bank")
            cboBankColumn = New DataGridViewComboBoxColumn
            cboBankColumn.HeaderText = "Bank Swift Code"
            cboBankColumn.DataPropertyName = "BankSwiftCode"
            cboBankColumn.DataSource = ds.Tables("Bank")
            cboBankColumn.ValueMember = ds.Tables("Bank").Columns(0).ColumnName
            cboBankColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboBankColumn.FlatStyle = FlatStyle.Flat
            dgvSwiftIntermediaryCodes.Columns.Insert(0, cboBankColumn)

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, SICConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentSwiftCodes")
            dgvSwiftIntermediaryCodes.AutoGenerateColumns = True
            dgvSwiftIntermediaryCodes.DataSource = ds.Tables("vwDolfinPaymentSwiftCodes")


            dgvSwiftIntermediaryCodes.Columns(0).Visible = False
            dgvSwiftIntermediaryCodes.Columns(1).Width = 140
            dgvSwiftIntermediaryCodes.Columns(2).Visible = False
            dgvSwiftIntermediaryCodes.Columns(3).Visible = False
            dgvSwiftIntermediaryCodes.Columns(4).Visible = False
            dgvSwiftIntermediaryCodes.Columns(5).Visible = False
            dgvSwiftIntermediaryCodes.Columns(6).Visible = False
            dgvSwiftIntermediaryCodes.Columns(7).Visible = False
            dgvSwiftIntermediaryCodes.Columns(8).Width = 80
            dgvSwiftIntermediaryCodes.Columns(9).Visible = False
            dgvSwiftIntermediaryCodes.Columns(10).Width = 140
            dgvSwiftIntermediaryCodes.Columns(11).Visible = False
            dgvSwiftIntermediaryCodes.Columns(12).Visible = False
            dgvSwiftIntermediaryCodes.Columns(13).Visible = False
            dgvSwiftIntermediaryCodes.Columns(14).Visible = False
            dgvSwiftIntermediaryCodes.Columns(15).Visible = False
            dgvSwiftIntermediaryCodes.Columns(16).Width = 200
            dgvSwiftIntermediaryCodes.Columns("IBankBIK").Width = 200
            dgvSwiftIntermediaryCodes.Columns("IBankBIK").HeaderText = "Intermediary Bank BIK Code"
            dgvSwiftIntermediaryCodes.Columns("IBankIBAN").Width = 200
            dgvSwiftIntermediaryCodes.Columns("IBankIBAN").HeaderText = "Intermediary Bank IBAN"
            dgvSwiftIntermediaryCodes.Columns("BankFullName").Width = 325
            dgvSwiftIntermediaryCodes.Columns("BankFullName").ReadOnly = True
            dgvSwiftIntermediaryCodes.Columns("BankFullName").HeaderText = "Bank Name"
            dgvSwiftIntermediaryCodes.Columns("IBankFullName").Width = 325
            dgvSwiftIntermediaryCodes.Columns("IBankFullName").ReadOnly = True
            dgvSwiftIntermediaryCodes.Columns("IBankFullName").HeaderText = "Intermediary Bank Name"
            dgvSwiftIntermediaryCodes.Columns("IBankINN").ReadOnly = True
            dgvSwiftIntermediaryCodes.Columns("IBankLastModifiedDate").Visible = False


            'dgvSwiftIntermediaryCodes.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvSwiftIntermediaryCodes.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvSwiftIntermediaryCodes.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSwiftCodesIntermediary, Err.Description & " - Problem With viewing swift codes grid")
        End Try
    End Sub

    Private Sub FrmSwiftCodes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvSwiftIntermediaryCodes, True)
        ClsIMS.PopulateComboboxWithData(cboSCBankName, "Select 1, BankName from vwDolfinPaymentSwiftCodesIntermediaryBankLinks group by BankName order by BankName")
        cboSCBankName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSCBankName.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboSC, "Select 1, BankSwiftCode from vwDolfinPaymentSwiftCodesIntermediaryBankLinks group by BankSwiftCode order by BankSwiftCode")
        cboSC.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSC.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboCCY, "Select 1, BankCCY from vwDolfinPaymentSwiftCodesIntermediaryBankLinks group by BankCCY order by BankCCY")
        cboSC.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSC.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboIB, "Select 1, IBankSwiftCode from vwDolfinPaymentSwiftCodesIntermediaryBankLinks group by IBankSwiftCode order by IBankSwiftCode")
        cboSC.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSC.AutoCompleteSource = AutoCompleteSource.ListItems
        SCLoadGrid()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvSwiftIntermediaryCodes, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        cboSCBankName.Text = ""
        cboSC.Text = ""
        cboIB.Text = ""
        cboCCY.Text = ""
        SCLoadGrid()
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varWhereClauseFilter As String = "where"

        Cursor = Cursors.WaitCursor

        If cboSCBankName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and BankName ='" & cboSCBankName.Text & "'"
        End If

        If cboSC.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and BankSwiftCode ='" & cboSC.Text & "'"
        End If

        If cboCCY.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and BankCCY ='" & cboCCY.Text & "'"
        End If

        If cboIB.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and IBankSwiftCode ='" & cboIB.Text & "'"
        End If

        If varWhereClauseFilter <> "where" Then
            SCLoadGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            SCLoadGrid()
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvSwiftCodes_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSwiftIntermediaryCodes.RowLeave
        Dim varBankId As Integer = 0, varIBankId As Integer = 0, varCCYCode As Integer = 0
        Dim varSwiftCode As String = "", varIBankSwiftCode As String = "", varCCY As String = "", varIBAN As String = "", varBIK As String = "", varINN As String = ""
        Dim varSwiftDetails As New Dictionary(Of String, String)

        If dgvSwiftIntermediaryCodes.EndEdit Then
            Try
                varBankId = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("BankID").Value), 0, dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("BankID").Value)
                varIBankId = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankID").Value), 0, dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankID").Value)
                varCCYCode = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("BankCCYCode").Value), 0, dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("BankCCYCode").Value)
                varSwiftCode = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(1).Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(1).Value)
                varSwiftCode = Strings.Left(Replace(Replace(varSwiftCode, " ", ""), "-", ""), 11)
                varCCY = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(8).Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(8).Value)
                varIBankSwiftCode = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(10).Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells(10).Value)
                varINN = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankINN").Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankINN").Value)
                varIBAN = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankIBAN").Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankIBAN").Value)
                varBIK = IIf(IsDBNull(dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankBIK").Value), "", dgvSwiftIntermediaryCodes.Rows(e.RowIndex).Cells("IBankBIK").Value)
                If varSwiftCode <> "" And varCCY <> "" And varIBankSwiftCode <> "" Then
                    If varIBankId = 0 And varIBankId = 0 Then
                        Dim CheckID As String = ClsIMS.GetSQLItem("Select BankID From vwDolfinPaymentSwiftCodesIntermediaryBankLinks Where BankSwiftCode = '" & varSwiftCode & "' and BankCCY = '" & varCCY & "' and IBankSwiftCode = '" & varIBankSwiftCode & "'")

                        If CheckID = "" Then
                            Dim varSQL As String = "insert into vwDolfinPaymentSwiftCodesIntermediaryBank(SC_ID,IB_CCYCode,IB_SC_ID,IB_IBAN,IB_BIK,IB_LastModifiedDate) values (" &
                                "(select SC_ID from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varSwiftCode & "')," &
                                "(select CR_Code from vwdolfinPaymentcurrencies where CR_Name1 = '" & varCCY & "')," &
                                "(select SC_ID from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varIBankSwiftCode & "')," &
                                "'" & varIBAN & "','" & varBIK & "',getdate())"
                            ClsIMS.ExecuteString(SICConn, varSQL)
                            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodesIntermediary, varSQL)
                        End If
                    Else
                        Dim varSQL As String = "update vwDolfinPaymentSwiftCodesIntermediaryBank set " &
                        "SC_ID = (select SC_ID from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varSwiftCode & "')," &
                        "IB_CCYCode = (select CR_Code from vwdolfinPaymentcurrencies where CR_Name1 = '" & varCCY & "')," &
                        "IB_SC_ID = (select SC_ID from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varIBankSwiftCode & "')," &
                        "IB_IBAN = '" & varIBAN & "'," &
                        "IB_BIK = '" & varBIK & "'," &
                        "IB_LastModifiedDate = getdate() where IB_ID = " & varIBankId
                        ClsIMS.ExecuteString(SICConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodesIntermediary, varSQL)
                    End If
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSwiftCodesIntermediary, Err.Description & " - Problem with updating SWIFT Code: " & varSwiftCode)
            End Try
        End If
    End Sub

    Private Sub FrmSwiftCodes_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If SICConn.State = ConnectionState.Open Then
            SICConn.Close()
        End If
    End Sub

    Private Sub dgvSwiftIntermediaryCodes_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvSwiftIntermediaryCodes.UserDeletingRow
        Dim varIBankID As Integer = 0
        Try
            varIBankID = IIf(IsDBNull(dgvSwiftIntermediaryCodes.SelectedRows(0).Cells("IBankID").Value), 0, dgvSwiftIntermediaryCodes.SelectedRows(0).Cells("IBankID").Value)
            Dim varResponse As Integer = MsgBox("Are you sure you want to delete SWIFT Intermediary detail " & IIf(IsDBNull(dgvSwiftIntermediaryCodes.SelectedRows(0).Cells(10).Value), 0, dgvSwiftIntermediaryCodes.SelectedRows(0).Cells(10).Value) & "?", vbYesNo + vbQuestion, "Are you sure")
            If vbYes Then
                ClsIMS.ExecuteString(SICConn, "delete from vwDolfinPaymentSwiftCodesIntermediaryBank where IB_ID = " & varIBankID)
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodesIntermediary, "user deleted - delete from vwDolfinPaymentSwiftCodes varid " & varIBankID)
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSwiftCodesIntermediary, Err.Description & " - Problem with deleting from vwDolfinPaymentSwiftCodes varid " & varIBankID)
        End Try
    End Sub
End Class