﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFeesPaidOption
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtFPOPortfolioRedirect = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtFPOType = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtFPODestination = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFPOAmt = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtFPOAccount = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtFPOref = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtFPOSEndDate = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtFPOStartDate = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtFPOPortfolio = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtFPOCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.txtFPOID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.Rb4 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(60, 399)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(135, 17)
        Me.Rb2.TabIndex = 71
        Me.Rb2.TabStop = True
        Me.Rb2.Text = "Clear Client Owed Fees"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox1.Controls.Add(Me.txtFPOPortfolioRedirect)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtFPOType)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtFPODestination)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtFPOAmt)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtFPOAccount)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtFPOref)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtFPOSEndDate)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtFPOStartDate)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtFPOPortfolio)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtFPOCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Controls.Add(Me.txtFPOID)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 37)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(438, 327)
        Me.GroupBox1.TabIndex = 70
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'txtFPOPortfolioRedirect
        '
        Me.txtFPOPortfolioRedirect.Enabled = False
        Me.txtFPOPortfolioRedirect.Location = New System.Drawing.Point(85, 292)
        Me.txtFPOPortfolioRedirect.Name = "txtFPOPortfolioRedirect"
        Me.txtFPOPortfolioRedirect.Size = New System.Drawing.Size(340, 20)
        Me.txtFPOPortfolioRedirect.TabIndex = 111
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 295)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 13)
        Me.Label9.TabIndex = 110
        Me.Label9.Text = "Pf (Redirect):"
        '
        'txtFPOType
        '
        Me.txtFPOType.Enabled = False
        Me.txtFPOType.Location = New System.Drawing.Point(85, 54)
        Me.txtFPOType.Name = "txtFPOType"
        Me.txtFPOType.Size = New System.Drawing.Size(217, 20)
        Me.txtFPOType.TabIndex = 109
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(45, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Type:"
        '
        'txtFPODestination
        '
        Me.txtFPODestination.Enabled = False
        Me.txtFPODestination.Location = New System.Drawing.Point(86, 266)
        Me.txtFPODestination.Name = "txtFPODestination"
        Me.txtFPODestination.Size = New System.Drawing.Size(216, 20)
        Me.txtFPODestination.TabIndex = 105
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(21, 269)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 13)
        Me.Label10.TabIndex = 104
        Me.Label10.Text = "Destination:"
        '
        'txtFPOAmt
        '
        Me.txtFPOAmt.Enabled = False
        Me.txtFPOAmt.Location = New System.Drawing.Point(85, 242)
        Me.txtFPOAmt.Name = "txtFPOAmt"
        Me.txtFPOAmt.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.txtFPOAmt.Size = New System.Drawing.Size(110, 20)
        Me.txtFPOAmt.TabIndex = 103
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 245)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(72, 13)
        Me.Label7.TabIndex = 102
        Me.Label7.Text = "Last Pay Amt:"
        '
        'txtFPOAccount
        '
        Me.txtFPOAccount.Enabled = False
        Me.txtFPOAccount.Location = New System.Drawing.Point(85, 216)
        Me.txtFPOAccount.Name = "txtFPOAccount"
        Me.txtFPOAccount.Size = New System.Drawing.Size(217, 20)
        Me.txtFPOAccount.TabIndex = 101
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(29, 219)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 100
        Me.Label8.Text = "Account:"
        '
        'txtFPOref
        '
        Me.txtFPOref.Enabled = False
        Me.txtFPOref.Location = New System.Drawing.Point(85, 187)
        Me.txtFPOref.Name = "txtFPOref"
        Me.txtFPOref.Size = New System.Drawing.Size(217, 20)
        Me.txtFPOref.TabIndex = 97
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(52, 190)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(27, 13)
        Me.Label3.TabIndex = 96
        Me.Label3.Text = "Ref:"
        '
        'txtFPOSEndDate
        '
        Me.txtFPOSEndDate.Enabled = False
        Me.txtFPOSEndDate.Location = New System.Drawing.Point(85, 160)
        Me.txtFPOSEndDate.Name = "txtFPOSEndDate"
        Me.txtFPOSEndDate.Size = New System.Drawing.Size(110, 20)
        Me.txtFPOSEndDate.TabIndex = 95
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(24, 163)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 94
        Me.Label6.Text = "End Date:"
        '
        'txtFPOStartDate
        '
        Me.txtFPOStartDate.Enabled = False
        Me.txtFPOStartDate.Location = New System.Drawing.Point(85, 134)
        Me.txtFPOStartDate.Name = "txtFPOStartDate"
        Me.txtFPOStartDate.Size = New System.Drawing.Size(110, 20)
        Me.txtFPOStartDate.TabIndex = 93
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 137)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "Start Date:"
        '
        'txtFPOPortfolio
        '
        Me.txtFPOPortfolio.Enabled = False
        Me.txtFPOPortfolio.Location = New System.Drawing.Point(85, 80)
        Me.txtFPOPortfolio.Name = "txtFPOPortfolio"
        Me.txtFPOPortfolio.Size = New System.Drawing.Size(340, 20)
        Me.txtFPOPortfolio.TabIndex = 86
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(31, 83)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblauthportfolio.TabIndex = 85
        Me.lblauthportfolio.Text = "Portfolio:"
        '
        'txtFPOCCY
        '
        Me.txtFPOCCY.Enabled = False
        Me.txtFPOCCY.Location = New System.Drawing.Point(85, 106)
        Me.txtFPOCCY.Name = "txtFPOCCY"
        Me.txtFPOCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtFPOCCY.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(48, 109)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "CCY:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(195, 103)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'txtFPOID
        '
        Me.txtFPOID.AcceptsReturn = True
        Me.txtFPOID.AcceptsTab = True
        Me.txtFPOID.BackColor = System.Drawing.Color.OldLace
        Me.txtFPOID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtFPOID.Enabled = False
        Me.txtFPOID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFPOID.Location = New System.Drawing.Point(24, 19)
        Me.txtFPOID.Multiline = True
        Me.txtFPOID.Name = "txtFPOID"
        Me.txtFPOID.Size = New System.Drawing.Size(103, 29)
        Me.txtFPOID.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(132, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(182, 24)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Fees Paid Options"
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Location = New System.Drawing.Point(60, 376)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(135, 17)
        Me.Rb1.TabIndex = 68
        Me.Rb1.TabStop = True
        Me.Rb1.Text = "Show Fees Breakdown"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(12, 484)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(438, 23)
        Me.cmdContinue.TabIndex = 67
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Location = New System.Drawing.Point(60, 422)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(152, 17)
        Me.Rb3.TabIndex = 72
        Me.Rb3.TabStop = True
        Me.Rb3.Text = "Pay Fee via FX transaction"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'Rb4
        '
        Me.Rb4.AutoSize = True
        Me.Rb4.Location = New System.Drawing.Point(60, 445)
        Me.Rb4.Name = "Rb4"
        Me.Rb4.Size = New System.Drawing.Size(234, 17)
        Me.Rb4.TabIndex = 73
        Me.Rb4.TabStop = True
        Me.Rb4.Text = "Show Fees Payment Audit Trail (By Portfolio)"
        Me.Rb4.UseVisualStyleBackColor = True
        '
        'FrmFeesPaidOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(462, 517)
        Me.Controls.Add(Me.Rb4)
        Me.Controls.Add(Me.Rb3)
        Me.Controls.Add(Me.Rb2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Rb1)
        Me.Controls.Add(Me.cmdContinue)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFeesPaidOption"
        Me.Text = "FrmFeesPaidOption"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtFPOSEndDate As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtFPOStartDate As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtFPOPortfolio As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtFPOCCY As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents txtFPOID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents txtFPOType As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtFPODestination As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtFPOAmt As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtFPOAccount As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtFPOref As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Rb3 As RadioButton
    Friend WithEvents txtFPOPortfolioRedirect As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Rb4 As RadioButton
End Class
