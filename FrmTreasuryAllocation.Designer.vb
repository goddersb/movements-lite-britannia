﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTreasuryAllocation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTreasuryAllocation))
        Me.dgvTA = New System.Windows.Forms.DataGridView()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CboTAInstB = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CboTAInstA = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboTACCY = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CmdTARefreshgrid = New System.Windows.Forms.Button()
        Me.dgvTATitles = New System.Windows.Forms.DataGridView()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.ViewResultsGroupBox = New System.Windows.Forms.GroupBox()
        Me.txtTAFileName = New System.Windows.Forms.TextBox()
        Me.txtTAStatus = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTAMessage = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TimerGetResults = New System.Windows.Forms.Timer(Me.components)
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtTransDate = New System.Windows.Forms.DateTimePicker()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgvTA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvTATitles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewResultsGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvTA
        '
        Me.dgvTA.AllowUserToAddRows = False
        Me.dgvTA.AllowUserToDeleteRows = False
        Me.dgvTA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTA.Location = New System.Drawing.Point(21, 146)
        Me.dgvTA.Name = "dgvTA"
        Me.dgvTA.Size = New System.Drawing.Size(1062, 614)
        Me.dgvTA.TabIndex = 27
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CboTAInstB)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.CboTAInstA)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Controls.Add(Me.cboTACCY)
        Me.ViewGroupBox.Controls.Add(Me.Label14)
        Me.ViewGroupBox.Controls.Add(Me.CmdTARefreshgrid)
        Me.ViewGroupBox.Location = New System.Drawing.Point(21, 12)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(609, 110)
        Me.ViewGroupBox.TabIndex = 31
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Movements Criteria"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(564, 17)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 4
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(446, 66)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 27)
        Me.CmdRefreshView.TabIndex = 5
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CboTAInstB
        '
        Me.CboTAInstB.FormattingEnabled = True
        Me.CboTAInstB.Location = New System.Drawing.Point(184, 55)
        Me.CboTAInstB.Name = "CboTAInstB"
        Me.CboTAInstB.Size = New System.Drawing.Size(256, 21)
        Me.CboTAInstB.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(116, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Institution B"
        '
        'CboTAInstA
        '
        Me.CboTAInstA.FormattingEnabled = True
        Me.CboTAInstA.Location = New System.Drawing.Point(184, 28)
        Me.CboTAInstA.Name = "CboTAInstA"
        Me.CboTAInstA.Size = New System.Drawing.Size(256, 21)
        Me.CboTAInstA.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(116, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "Institution A"
        '
        'cboTACCY
        '
        Me.cboTACCY.FormattingEnabled = True
        Me.cboTACCY.Location = New System.Drawing.Point(38, 30)
        Me.cboTACCY.Name = "cboTACCY"
        Me.cboTACCY.Size = New System.Drawing.Size(69, 21)
        Me.cboTACCY.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(4, 34)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(28, 13)
        Me.Label14.TabIndex = 53
        Me.Label14.Text = "CCY"
        '
        'CmdTARefreshgrid
        '
        Me.CmdTARefreshgrid.Location = New System.Drawing.Point(446, 28)
        Me.CmdTARefreshgrid.Name = "CmdTARefreshgrid"
        Me.CmdTARefreshgrid.Size = New System.Drawing.Size(112, 37)
        Me.CmdTARefreshgrid.TabIndex = 3
        Me.CmdTARefreshgrid.Text = "Filter Data Grid"
        Me.CmdTARefreshgrid.UseVisualStyleBackColor = True
        '
        'dgvTATitles
        '
        Me.dgvTATitles.AllowUserToDeleteRows = False
        Me.dgvTATitles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTATitles.Location = New System.Drawing.Point(21, 128)
        Me.dgvTATitles.Name = "dgvTATitles"
        Me.dgvTATitles.ReadOnly = True
        Me.dgvTATitles.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTATitles.Size = New System.Drawing.Size(1062, 20)
        Me.dgvTATitles.TabIndex = 32
        Me.dgvTATitles.TabStop = False
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(926, 764)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(157, 24)
        Me.cmdSubmit.TabIndex = 41
        Me.cmdSubmit.Text = "Submit transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'ViewResultsGroupBox
        '
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTAFileName)
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTAStatus)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label4)
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTAMessage)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label3)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label5)
        Me.ViewResultsGroupBox.Location = New System.Drawing.Point(636, 15)
        Me.ViewResultsGroupBox.Name = "ViewResultsGroupBox"
        Me.ViewResultsGroupBox.Size = New System.Drawing.Size(447, 107)
        Me.ViewResultsGroupBox.TabIndex = 42
        Me.ViewResultsGroupBox.TabStop = False
        Me.ViewResultsGroupBox.Text = "Export Movements Results"
        '
        'txtTAFileName
        '
        Me.txtTAFileName.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtTAFileName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTAFileName.Location = New System.Drawing.Point(65, 19)
        Me.txtTAFileName.Name = "txtTAFileName"
        Me.txtTAFileName.ReadOnly = True
        Me.txtTAFileName.Size = New System.Drawing.Size(347, 13)
        Me.txtTAFileName.TabIndex = 61
        Me.txtTAFileName.TabStop = False
        '
        'txtTAStatus
        '
        Me.txtTAStatus.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtTAStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTAStatus.Location = New System.Drawing.Point(65, 38)
        Me.txtTAStatus.Name = "txtTAStatus"
        Me.txtTAStatus.ReadOnly = True
        Me.txtTAStatus.Size = New System.Drawing.Size(347, 13)
        Me.txtTAStatus.TabIndex = 60
        Me.txtTAStatus.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 59
        Me.Label4.Text = "Status:"
        '
        'txtTAMessage
        '
        Me.txtTAMessage.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtTAMessage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTAMessage.Location = New System.Drawing.Point(65, 60)
        Me.txtTAMessage.Multiline = True
        Me.txtTAMessage.Name = "txtTAMessage"
        Me.txtTAMessage.ReadOnly = True
        Me.txtTAMessage.Size = New System.Drawing.Size(356, 41)
        Me.txtTAMessage.TabIndex = 58
        Me.txtTAMessage.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 57
        Me.Label3.Text = "Message:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "File Name:"
        '
        'TimerGetResults
        '
        Me.TimerGetResults.Interval = 5000
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(690, 770)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 13)
        Me.Label6.TabIndex = 61
        Me.Label6.Text = "Trans Date"
        '
        'dtTransDate
        '
        Me.dtTransDate.Location = New System.Drawing.Point(756, 766)
        Me.dtTransDate.Name = "dtTransDate"
        Me.dtTransDate.Size = New System.Drawing.Size(144, 20)
        Me.dtTransDate.TabIndex = 60
        '
        'FrmTreasuryAllocation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1099, 791)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dtTransDate)
        Me.Controls.Add(Me.ViewResultsGroupBox)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.dgvTATitles)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.Controls.Add(Me.dgvTA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmTreasuryAllocation"
        Me.Text = "TreasuryAllocation"
        CType(Me.dgvTA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvTATitles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewResultsGroupBox.ResumeLayout(False)
        Me.ViewResultsGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvTA As DataGridView
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents cboTACCY As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents CmdTARefreshgrid As Button
    Friend WithEvents CboTAInstB As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CboTAInstA As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents dgvTATitles As DataGridView
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents ViewResultsGroupBox As GroupBox
    Friend WithEvents txtTAFileName As TextBox
    Friend WithEvents txtTAStatus As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTAMessage As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents TimerGetResults As Timer
    Friend WithEvents Label6 As Label
    Friend WithEvents dtTransDate As DateTimePicker
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
