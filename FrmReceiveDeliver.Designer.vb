﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReceiveDeliver
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ChkCashRefresh = New System.Windows.Forms.CheckBox()
        Me.CboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.cmdRDRefresh = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.dgvRD = New System.Windows.Forms.DataGridView()
        Me.cmdAddRD = New System.Windows.Forms.Button()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.RDBox = New System.Windows.Forms.GroupBox()
        Me.cmdNewRD = New System.Windows.Forms.Button()
        Me.TabRD = New System.Windows.Forms.TabControl()
        Me.TabPageReceive = New System.Windows.Forms.TabPage()
        Me.GrpTransType = New System.Windows.Forms.GroupBox()
        Me.ChkRSendMiniOMS = New System.Windows.Forms.CheckBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboRType = New System.Windows.Forms.ComboBox()
        Me.ChkRSendIMS = New System.Windows.Forms.CheckBox()
        Me.cboRCashAcc = New System.Windows.Forms.ComboBox()
        Me.cboRClearer = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cboRPlaceSettle = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtRInstructions = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtRDelivAgentAccountNo = New System.Windows.Forms.TextBox()
        Me.cboRDelivAgent = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CboRCCY = New System.Windows.Forms.ComboBox()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.lblRMsg = New System.Windows.Forms.Label()
        Me.RRB2 = New System.Windows.Forms.RadioButton()
        Me.RRB1 = New System.Windows.Forms.RadioButton()
        Me.txtRAmount = New System.Windows.Forms.TextBox()
        Me.cboRBroker = New System.Windows.Forms.ComboBox()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtRTransDate = New System.Windows.Forms.DateTimePicker()
        Me.lblSettleDate = New System.Windows.Forms.Label()
        Me.dtRSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboRISIN = New System.Windows.Forms.ComboBox()
        Me.lblCCYAmt = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboRPortfolio = New System.Windows.Forms.ComboBox()
        Me.LblPayType = New System.Windows.Forms.Label()
        Me.CboRInstrument = New System.Windows.Forms.ComboBox()
        Me.TabPageRDPreview = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtSWIFT = New System.Windows.Forms.TextBox()
        Me.TimerRefreshRD = New System.Windows.Forms.Timer(Me.components)
        Me.cboPayCheck = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        CType(Me.dgvRD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RDBox.SuspendLayout()
        Me.TabRD.SuspendLayout()
        Me.TabPageReceive.SuspendLayout()
        Me.GrpTransType.SuspendLayout()
        Me.TabPageRDPreview.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChkCashRefresh
        '
        Me.ChkCashRefresh.AutoSize = True
        Me.ChkCashRefresh.Checked = True
        Me.ChkCashRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCashRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkCashRefresh.Location = New System.Drawing.Point(6, 706)
        Me.ChkCashRefresh.Name = "ChkCashRefresh"
        Me.ChkCashRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkCashRefresh.TabIndex = 59
        Me.ChkCashRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkCashRefresh.UseVisualStyleBackColor = True
        '
        'CboFilterStatus
        '
        Me.CboFilterStatus.FormattingEnabled = True
        Me.CboFilterStatus.Location = New System.Drawing.Point(1065, 420)
        Me.CboFilterStatus.Name = "CboFilterStatus"
        Me.CboFilterStatus.Size = New System.Drawing.Size(138, 21)
        Me.CboFilterStatus.TabIndex = 58
        '
        'cmdRDRefresh
        '
        Me.cmdRDRefresh.Location = New System.Drawing.Point(6, 420)
        Me.cmdRDRefresh.Name = "cmdRDRefresh"
        Me.cmdRDRefresh.Size = New System.Drawing.Size(267, 23)
        Me.cmdRDRefresh.TabIndex = 53
        Me.cmdRDRefresh.Text = "Refresh Transactions"
        Me.cmdRDRefresh.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(997, 423)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 57
        Me.Label10.Text = "Filter Status"
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(953, 706)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(251, 24)
        Me.cmdSubmit.TabIndex = 56
        Me.cmdSubmit.Text = "Submit Transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'dgvRD
        '
        Me.dgvRD.AllowUserToAddRows = False
        Me.dgvRD.AllowUserToDeleteRows = False
        Me.dgvRD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRD.Location = New System.Drawing.Point(7, 446)
        Me.dgvRD.MultiSelect = False
        Me.dgvRD.Name = "dgvRD"
        Me.dgvRD.ReadOnly = True
        Me.dgvRD.Size = New System.Drawing.Size(1197, 254)
        Me.dgvRD.TabIndex = 55
        '
        'cmdAddRD
        '
        Me.cmdAddRD.Location = New System.Drawing.Point(929, 311)
        Me.cmdAddRD.Name = "cmdAddRD"
        Me.cmdAddRD.Size = New System.Drawing.Size(255, 24)
        Me.cmdAddRD.TabIndex = 14
        Me.cmdAddRD.Text = "Add Transaction"
        Me.cmdAddRD.UseVisualStyleBackColor = True
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(440, 16)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(207, 24)
        Me.lblpaymentTitle.TabIndex = 61
        Me.lblpaymentTitle.Text = "Receive/Deliver Free"
        '
        'RDBox
        '
        Me.RDBox.Controls.Add(Me.cmdNewRD)
        Me.RDBox.Controls.Add(Me.TabRD)
        Me.RDBox.Controls.Add(Me.cmdAddRD)
        Me.RDBox.Location = New System.Drawing.Point(12, 43)
        Me.RDBox.Name = "RDBox"
        Me.RDBox.Size = New System.Drawing.Size(1192, 343)
        Me.RDBox.TabIndex = 62
        Me.RDBox.TabStop = False
        Me.RDBox.Text = "Receive/Deliver Details"
        '
        'cmdNewRD
        '
        Me.cmdNewRD.Location = New System.Drawing.Point(6, 311)
        Me.cmdNewRD.Name = "cmdNewRD"
        Me.cmdNewRD.Size = New System.Drawing.Size(255, 24)
        Me.cmdNewRD.TabIndex = 62
        Me.cmdNewRD.Text = "New Transaction"
        Me.cmdNewRD.UseVisualStyleBackColor = True
        '
        'TabRD
        '
        Me.TabRD.Controls.Add(Me.TabPageReceive)
        Me.TabRD.Controls.Add(Me.TabPageRDPreview)
        Me.TabRD.Location = New System.Drawing.Point(6, 19)
        Me.TabRD.Name = "TabRD"
        Me.TabRD.SelectedIndex = 0
        Me.TabRD.Size = New System.Drawing.Size(1178, 286)
        Me.TabRD.TabIndex = 61
        '
        'TabPageReceive
        '
        Me.TabPageReceive.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPageReceive.Controls.Add(Me.GrpTransType)
        Me.TabPageReceive.Location = New System.Drawing.Point(4, 22)
        Me.TabPageReceive.Name = "TabPageReceive"
        Me.TabPageReceive.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageReceive.Size = New System.Drawing.Size(1170, 260)
        Me.TabPageReceive.TabIndex = 0
        Me.TabPageReceive.Text = "Receive/Deliver Free"
        '
        'GrpTransType
        '
        Me.GrpTransType.Controls.Add(Me.ChkRSendMiniOMS)
        Me.GrpTransType.Controls.Add(Me.Label27)
        Me.GrpTransType.Controls.Add(Me.cboRType)
        Me.GrpTransType.Controls.Add(Me.ChkRSendIMS)
        Me.GrpTransType.Controls.Add(Me.cboRCashAcc)
        Me.GrpTransType.Controls.Add(Me.cboRClearer)
        Me.GrpTransType.Controls.Add(Me.Label25)
        Me.GrpTransType.Controls.Add(Me.cboRPlaceSettle)
        Me.GrpTransType.Controls.Add(Me.Label16)
        Me.GrpTransType.Controls.Add(Me.txtRInstructions)
        Me.GrpTransType.Controls.Add(Me.Label15)
        Me.GrpTransType.Controls.Add(Me.txtRDelivAgentAccountNo)
        Me.GrpTransType.Controls.Add(Me.cboRDelivAgent)
        Me.GrpTransType.Controls.Add(Me.Label6)
        Me.GrpTransType.Controls.Add(Me.Label7)
        Me.GrpTransType.Controls.Add(Me.Label8)
        Me.GrpTransType.Controls.Add(Me.CboRCCY)
        Me.GrpTransType.Controls.Add(Me.lblCCY)
        Me.GrpTransType.Controls.Add(Me.lblRMsg)
        Me.GrpTransType.Controls.Add(Me.RRB2)
        Me.GrpTransType.Controls.Add(Me.RRB1)
        Me.GrpTransType.Controls.Add(Me.txtRAmount)
        Me.GrpTransType.Controls.Add(Me.cboRBroker)
        Me.GrpTransType.Controls.Add(Me.lblTransDate)
        Me.GrpTransType.Controls.Add(Me.dtRTransDate)
        Me.GrpTransType.Controls.Add(Me.lblSettleDate)
        Me.GrpTransType.Controls.Add(Me.dtRSettleDate)
        Me.GrpTransType.Controls.Add(Me.Label4)
        Me.GrpTransType.Controls.Add(Me.Label3)
        Me.GrpTransType.Controls.Add(Me.cboRISIN)
        Me.GrpTransType.Controls.Add(Me.lblCCYAmt)
        Me.GrpTransType.Controls.Add(Me.lblportfoliofee)
        Me.GrpTransType.Controls.Add(Me.Label1)
        Me.GrpTransType.Controls.Add(Me.cboRPortfolio)
        Me.GrpTransType.Controls.Add(Me.LblPayType)
        Me.GrpTransType.Controls.Add(Me.CboRInstrument)
        Me.GrpTransType.Location = New System.Drawing.Point(6, 6)
        Me.GrpTransType.Name = "GrpTransType"
        Me.GrpTransType.Size = New System.Drawing.Size(1158, 244)
        Me.GrpTransType.TabIndex = 42
        Me.GrpTransType.TabStop = False
        Me.GrpTransType.Text = "Receive/Deliver Free Details"
        '
        'ChkRSendMiniOMS
        '
        Me.ChkRSendMiniOMS.AutoSize = True
        Me.ChkRSendMiniOMS.Location = New System.Drawing.Point(914, 103)
        Me.ChkRSendMiniOMS.Name = "ChkRSendMiniOMS"
        Me.ChkRSendMiniOMS.Size = New System.Drawing.Size(197, 17)
        Me.ChkRSendMiniOMS.TabIndex = 86
        Me.ChkRSendMiniOMS.Text = "Send To Mini-OMS (After Confirmed)"
        Me.ChkRSendMiniOMS.UseVisualStyleBackColor = True
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(49, 53)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(34, 13)
        Me.Label27.TabIndex = 85
        Me.Label27.Text = "Type:"
        '
        'cboRType
        '
        Me.cboRType.FormattingEnabled = True
        Me.cboRType.Location = New System.Drawing.Point(89, 50)
        Me.cboRType.Name = "cboRType"
        Me.cboRType.Size = New System.Drawing.Size(463, 21)
        Me.cboRType.TabIndex = 0
        '
        'ChkRSendIMS
        '
        Me.ChkRSendIMS.AutoSize = True
        Me.ChkRSendIMS.Checked = True
        Me.ChkRSendIMS.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkRSendIMS.Location = New System.Drawing.Point(914, 80)
        Me.ChkRSendIMS.Name = "ChkRSendIMS"
        Me.ChkRSendIMS.Size = New System.Drawing.Size(170, 17)
        Me.ChkRSendIMS.TabIndex = 4
        Me.ChkRSendIMS.Text = "Send To IMS (After Confirmed)"
        Me.ChkRSendIMS.UseVisualStyleBackColor = True
        '
        'cboRCashAcc
        '
        Me.cboRCashAcc.FormattingEnabled = True
        Me.cboRCashAcc.Location = New System.Drawing.Point(357, 188)
        Me.cboRCashAcc.Name = "cboRCashAcc"
        Me.cboRCashAcc.Size = New System.Drawing.Size(195, 21)
        Me.cboRCashAcc.TabIndex = 11
        '
        'cboRClearer
        '
        Me.cboRClearer.FormattingEnabled = True
        Me.cboRClearer.Location = New System.Drawing.Point(89, 215)
        Me.cboRClearer.Name = "cboRClearer"
        Me.cboRClearer.Size = New System.Drawing.Size(463, 21)
        Me.cboRClearer.TabIndex = 12
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(295, 191)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(56, 13)
        Me.Label25.TabIndex = 83
        Me.Label25.Text = "Cash Acc:"
        '
        'cboRPlaceSettle
        '
        Me.cboRPlaceSettle.FormattingEnabled = True
        Me.cboRPlaceSettle.Location = New System.Drawing.Point(653, 210)
        Me.cboRPlaceSettle.Name = "cboRPlaceSettle"
        Me.cboRPlaceSettle.Size = New System.Drawing.Size(458, 21)
        Me.cboRPlaceSettle.TabIndex = 15
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(568, 212)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(79, 13)
        Me.Label16.TabIndex = 82
        Me.Label16.Text = "Place of Settle:"
        '
        'txtRInstructions
        '
        Me.txtRInstructions.Location = New System.Drawing.Point(89, 159)
        Me.txtRInstructions.Name = "txtRInstructions"
        Me.txtRInstructions.Size = New System.Drawing.Size(463, 20)
        Me.txtRInstructions.TabIndex = 9
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(5, 162)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(80, 13)
        Me.Label15.TabIndex = 80
        Me.Label15.Text = "Instructions/ID:"
        '
        'txtRDelivAgentAccountNo
        '
        Me.txtRDelivAgentAccountNo.Location = New System.Drawing.Point(653, 184)
        Me.txtRDelivAgentAccountNo.Name = "txtRDelivAgentAccountNo"
        Me.txtRDelivAgentAccountNo.Size = New System.Drawing.Size(458, 20)
        Me.txtRDelivAgentAccountNo.TabIndex = 14
        '
        'cboRDelivAgent
        '
        Me.cboRDelivAgent.FormattingEnabled = True
        Me.cboRDelivAgent.Location = New System.Drawing.Point(653, 156)
        Me.cboRDelivAgent.Name = "cboRDelivAgent"
        Me.cboRDelivAgent.Size = New System.Drawing.Size(458, 21)
        Me.cboRDelivAgent.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(563, 187)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(84, 13)
        Me.Label6.TabIndex = 76
        Me.Label6.Text = "Del Agt Acc No:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(645, 175)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 15)
        Me.Label7.TabIndex = 75
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(582, 159)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 13)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Deliv Agent:"
        '
        'CboRCCY
        '
        Me.CboRCCY.FormattingEnabled = True
        Me.CboRCCY.Location = New System.Drawing.Point(808, 78)
        Me.CboRCCY.Name = "CboRCCY"
        Me.CboRCCY.Size = New System.Drawing.Size(81, 21)
        Me.CboRCCY.TabIndex = 3
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(771, 82)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 73
        Me.lblCCY.Text = "CCY:"
        '
        'lblRMsg
        '
        Me.lblRMsg.AutoSize = True
        Me.lblRMsg.ForeColor = System.Drawing.Color.DarkRed
        Me.lblRMsg.Location = New System.Drawing.Point(563, 15)
        Me.lblRMsg.Name = "lblRMsg"
        Me.lblRMsg.Size = New System.Drawing.Size(39, 13)
        Me.lblRMsg.TabIndex = 71
        Me.lblRMsg.Text = "Label2"
        '
        'RRB2
        '
        Me.RRB2.AutoSize = True
        Me.RRB2.Location = New System.Drawing.Point(37, 132)
        Me.RRB2.Name = "RRB2"
        Me.RRB2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RRB2.Size = New System.Drawing.Size(46, 17)
        Me.RRB2.TabIndex = 6
        Me.RRB2.TabStop = True
        Me.RRB2.Text = "ISIN"
        Me.RRB2.UseVisualStyleBackColor = True
        '
        'RRB1
        '
        Me.RRB1.AutoSize = True
        Me.RRB1.Location = New System.Drawing.Point(9, 105)
        Me.RRB1.Name = "RRB1"
        Me.RRB1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.RRB1.Size = New System.Drawing.Size(74, 17)
        Me.RRB1.TabIndex = 4
        Me.RRB1.TabStop = True
        Me.RRB1.Text = "Instrument"
        Me.RRB1.UseVisualStyleBackColor = True
        '
        'txtRAmount
        '
        Me.txtRAmount.Location = New System.Drawing.Point(655, 79)
        Me.txtRAmount.Name = "txtRAmount"
        Me.txtRAmount.Size = New System.Drawing.Size(103, 20)
        Me.txtRAmount.TabIndex = 2
        '
        'cboRBroker
        '
        Me.cboRBroker.FormattingEnabled = True
        Me.cboRBroker.Location = New System.Drawing.Point(89, 188)
        Me.cboRBroker.Name = "cboRBroker"
        Me.cboRBroker.Size = New System.Drawing.Size(195, 21)
        Me.cboRBroker.TabIndex = 10
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(586, 106)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(63, 13)
        Me.lblTransDate.TabIndex = 66
        Me.lblTransDate.Text = "Trans Date:"
        '
        'dtRTransDate
        '
        Me.dtRTransDate.Location = New System.Drawing.Point(655, 103)
        Me.dtRTransDate.Name = "dtRTransDate"
        Me.dtRTransDate.Size = New System.Drawing.Size(128, 20)
        Me.dtRTransDate.TabIndex = 7
        '
        'lblSettleDate
        '
        Me.lblSettleDate.AutoSize = True
        Me.lblSettleDate.Location = New System.Drawing.Point(586, 133)
        Me.lblSettleDate.Name = "lblSettleDate"
        Me.lblSettleDate.Size = New System.Drawing.Size(63, 13)
        Me.lblSettleDate.TabIndex = 65
        Me.lblSettleDate.Text = "Settle Date:"
        '
        'dtRSettleDate
        '
        Me.dtRSettleDate.Location = New System.Drawing.Point(655, 129)
        Me.dtRSettleDate.Name = "dtRSettleDate"
        Me.dtRSettleDate.Size = New System.Drawing.Size(128, 20)
        Me.dtRSettleDate.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(603, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "Amount:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(40, 218)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "Clearer:"
        '
        'cboRISIN
        '
        Me.cboRISIN.Enabled = False
        Me.cboRISIN.FormattingEnabled = True
        Me.cboRISIN.Location = New System.Drawing.Point(89, 131)
        Me.cboRISIN.Name = "cboRISIN"
        Me.cboRISIN.Size = New System.Drawing.Size(463, 21)
        Me.cboRISIN.TabIndex = 6
        '
        'lblCCYAmt
        '
        Me.lblCCYAmt.AutoSize = True
        Me.lblCCYAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCYAmt.ForeColor = System.Drawing.Color.Black
        Me.lblCCYAmt.Location = New System.Drawing.Point(1035, 104)
        Me.lblCCYAmt.Name = "lblCCYAmt"
        Me.lblCCYAmt.Size = New System.Drawing.Size(0, 15)
        Me.lblCCYAmt.TabIndex = 52
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Location = New System.Drawing.Point(306, 80)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 39
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Portfolio:"
        '
        'cboRPortfolio
        '
        Me.cboRPortfolio.FormattingEnabled = True
        Me.cboRPortfolio.Location = New System.Drawing.Point(89, 77)
        Me.cboRPortfolio.Name = "cboRPortfolio"
        Me.cboRPortfolio.Size = New System.Drawing.Size(463, 21)
        Me.cboRPortfolio.TabIndex = 1
        '
        'LblPayType
        '
        Me.LblPayType.AutoSize = True
        Me.LblPayType.Location = New System.Drawing.Point(32, 191)
        Me.LblPayType.Name = "LblPayType"
        Me.LblPayType.Size = New System.Drawing.Size(51, 13)
        Me.LblPayType.TabIndex = 6
        Me.LblPayType.Text = "Brkr Acc:"
        '
        'CboRInstrument
        '
        Me.CboRInstrument.FormattingEnabled = True
        Me.CboRInstrument.Location = New System.Drawing.Point(89, 104)
        Me.CboRInstrument.Name = "CboRInstrument"
        Me.CboRInstrument.Size = New System.Drawing.Size(463, 21)
        Me.CboRInstrument.TabIndex = 5
        '
        'TabPageRDPreview
        '
        Me.TabPageRDPreview.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPageRDPreview.Controls.Add(Me.GroupBox3)
        Me.TabPageRDPreview.Location = New System.Drawing.Point(4, 22)
        Me.TabPageRDPreview.Name = "TabPageRDPreview"
        Me.TabPageRDPreview.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageRDPreview.Size = New System.Drawing.Size(1129, 260)
        Me.TabPageRDPreview.TabIndex = 1
        Me.TabPageRDPreview.Text = "Preview"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtSWIFT)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1028, 212)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "SWIFT Export File Contents"
        '
        'txtSWIFT
        '
        Me.txtSWIFT.AcceptsReturn = True
        Me.txtSWIFT.AcceptsTab = True
        Me.txtSWIFT.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.txtSWIFT.Location = New System.Drawing.Point(6, 19)
        Me.txtSWIFT.Multiline = True
        Me.txtSWIFT.Name = "txtSWIFT"
        Me.txtSWIFT.ReadOnly = True
        Me.txtSWIFT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSWIFT.Size = New System.Drawing.Size(1016, 184)
        Me.txtSWIFT.TabIndex = 34
        '
        'TimerRefreshRD
        '
        Me.TimerRefreshRD.Interval = 120000
        '
        'cboPayCheck
        '
        Me.cboPayCheck.FormattingEnabled = True
        Me.cboPayCheck.Location = New System.Drawing.Point(853, 420)
        Me.cboPayCheck.Name = "cboPayCheck"
        Me.cboPayCheck.Size = New System.Drawing.Size(138, 21)
        Me.cboPayCheck.TabIndex = 64
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(786, 423)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(61, 13)
        Me.Label23.TabIndex = 63
        Me.Label23.Text = "FOP Period"
        '
        'FrmReceiveDeliver
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1216, 730)
        Me.Controls.Add(Me.cboPayCheck)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.RDBox)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.ChkCashRefresh)
        Me.Controls.Add(Me.CboFilterStatus)
        Me.Controls.Add(Me.cmdRDRefresh)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.dgvRD)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmReceiveDeliver"
        Me.Text = "Receive/Deliver"
        CType(Me.dgvRD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RDBox.ResumeLayout(False)
        Me.TabRD.ResumeLayout(False)
        Me.TabPageReceive.ResumeLayout(False)
        Me.GrpTransType.ResumeLayout(False)
        Me.GrpTransType.PerformLayout()
        Me.TabPageRDPreview.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChkCashRefresh As CheckBox
    Friend WithEvents CboFilterStatus As ComboBox
    Friend WithEvents cmdRDRefresh As Button
    Friend WithEvents Label10 As Label
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents dgvRD As DataGridView
    Friend WithEvents cmdAddRD As Button
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents RDBox As GroupBox
    Friend WithEvents TimerRefreshRD As Timer
    Friend WithEvents cboPayCheck As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents TabRD As TabControl
    Friend WithEvents TabPageReceive As TabPage
    Friend WithEvents GrpTransType As GroupBox
    Friend WithEvents Label27 As Label
    Friend WithEvents cboRType As ComboBox
    Friend WithEvents ChkRSendIMS As CheckBox
    Friend WithEvents cboRCashAcc As ComboBox
    Friend WithEvents cboRClearer As ComboBox
    Friend WithEvents Label25 As Label
    Friend WithEvents cboRPlaceSettle As ComboBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtRInstructions As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents txtRDelivAgentAccountNo As TextBox
    Friend WithEvents cboRDelivAgent As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents CboRCCY As ComboBox
    Friend WithEvents lblCCY As Label
    Friend WithEvents lblRMsg As Label
    Friend WithEvents RRB2 As RadioButton
    Friend WithEvents RRB1 As RadioButton
    Friend WithEvents txtRAmount As TextBox
    Friend WithEvents cboRBroker As ComboBox
    Friend WithEvents lblTransDate As Label
    Friend WithEvents dtRTransDate As DateTimePicker
    Friend WithEvents lblSettleDate As Label
    Friend WithEvents dtRSettleDate As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cboRISIN As ComboBox
    Friend WithEvents lblCCYAmt As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboRPortfolio As ComboBox
    Friend WithEvents LblPayType As Label
    Friend WithEvents CboRInstrument As ComboBox
    Friend WithEvents TabPageRDPreview As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtSWIFT As TextBox
    Friend WithEvents cmdNewRD As Button
    Friend WithEvents ChkRSendMiniOMS As CheckBox
End Class
