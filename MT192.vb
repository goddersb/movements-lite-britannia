﻿Public Class MT192

    Public Property ClientSwiftCode As String
    Public Property OrderRef As String
    Public Property OrderRelatedRef As String
    Public Property OrderRelatedDate As String
    Public Property OrderSwift As String
    Public Property Message As String
    Public Property SwiftUrgent As Boolean
    Public Property Email As String
    Public Property Ftp As Boolean?

End Class