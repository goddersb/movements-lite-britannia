﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmFees
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFees))
        Me.GrpSelect = New System.Windows.Forms.GroupBox()
        Me.ChkRedirectFee = New System.Windows.Forms.CheckBox()
        Me.ChkTemplateName = New System.Windows.Forms.CheckBox()
        Me.cboPortfolioRedirect = New System.Windows.Forms.ComboBox()
        Me.cboTemplateName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CboCCY = New System.Windows.Forms.ComboBox()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.GrpFees = New System.Windows.Forms.GroupBox()
        Me.TabMgt = New System.Windows.Forms.TabControl()
        Me.TabPageMgt = New System.Windows.Forms.TabPage()
        Me.ChkMgtFees = New System.Windows.Forms.CheckBox()
        Me.GrpMgtFees = New System.Windows.Forms.GroupBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtMgtMaintMin = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cboMGTCCYEqv = New System.Windows.Forms.ComboBox()
        Me.dgvMgtRange = New System.Windows.Forms.DataGridView()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboMGTChargeableCCY = New System.Windows.Forms.ComboBox()
        Me.txtMgtID = New System.Windows.Forms.TextBox()
        Me.TabExceptions = New System.Windows.Forms.TabControl()
        Me.TabExceptionGroup = New System.Windows.Forms.TabPage()
        Me.dgvMgtExcGrp = New System.Windows.Forms.DataGridView()
        Me.TabException = New System.Windows.Forms.TabPage()
        Me.dgvMgtExcIns = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMgtMax = New System.Windows.Forms.TextBox()
        Me.txtMgtMin = New System.Windows.Forms.TextBox()
        Me.cboMgtType = New System.Windows.Forms.ComboBox()
        Me.TabPageCustody = New System.Windows.Forms.TabPage()
        Me.ChkCF = New System.Windows.Forms.CheckBox()
        Me.GrpCF = New System.Windows.Forms.GroupBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtCFMaintMin = New System.Windows.Forms.TextBox()
        Me.TabCFCustody = New System.Windows.Forms.TabControl()
        Me.TabCFRanges = New System.Windows.Forms.TabPage()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cboCFCCYEqv = New System.Windows.Forms.ComboBox()
        Me.dgvCFRange = New System.Windows.Forms.DataGridView()
        Me.TabCFSafeKeeping = New System.Windows.Forms.TabPage()
        Me.dgvCFSK = New System.Windows.Forms.DataGridView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtCFMax = New System.Windows.Forms.TextBox()
        Me.txtCFMin = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboCFType = New System.Windows.Forms.ComboBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabCFCustodyExceptions = New System.Windows.Forms.TabControl()
        Me.TabCFExceptionsGroup = New System.Windows.Forms.TabPage()
        Me.dgvCFExcGrp = New System.Windows.Forms.DataGridView()
        Me.TabCFExceptions = New System.Windows.Forms.TabPage()
        Me.dgvCFExcIns = New System.Windows.Forms.DataGridView()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.cboCFChargeableCCY = New System.Windows.Forms.ComboBox()
        Me.TabPageTransactions = New System.Windows.Forms.TabPage()
        Me.ChkTF = New System.Windows.Forms.CheckBox()
        Me.GrpTF = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ChkCoverBrokerage = New System.Windows.Forms.CheckBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtTFFOPFee = New System.Windows.Forms.TextBox()
        Me.cboTFFOPFeeCCY = New System.Windows.Forms.ComboBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtTFTransFee = New System.Windows.Forms.TextBox()
        Me.cboTFTransFeeCCY = New System.Windows.Forms.ComboBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtTFTicketFee = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtTFMax = New System.Windows.Forms.TextBox()
        Me.cboTFTicketFeeCCY = New System.Windows.Forms.ComboBox()
        Me.txtTFMin = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TabFIGovt = New System.Windows.Forms.TabControl()
        Me.TabTrans = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboTFCCYEqv = New System.Windows.Forms.ComboBox()
        Me.dgvTFRange = New System.Windows.Forms.DataGridView()
        Me.TabTransSK = New System.Windows.Forms.TabPage()
        Me.dgvTFSK = New System.Windows.Forms.DataGridView()
        Me.TabTransSKFixed = New System.Windows.Forms.TabPage()
        Me.dgvTFSKFixed = New System.Windows.Forms.DataGridView()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.cboTFType = New System.Windows.Forms.ComboBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TabPageTransfers = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvTrans = New System.Windows.Forms.DataGridView()
        Me.TabPageDocs = New System.Windows.Forms.TabPage()
        Me.GrpFiles = New System.Windows.Forms.GroupBox()
        Me.TreeViewFiles = New System.Windows.Forms.TreeView()
        Me.LVFiles = New System.Windows.Forms.ListView()
        Me.TabPageEAMClients = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.dgvEAMClients = New System.Windows.Forms.DataGridView()
        Me.GrpFSDetails = New System.Windows.Forms.GroupBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.cboFSIsEAM = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cboFSPayFreq = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.DtFSStart = New System.Windows.Forms.DateTimePicker()
        Me.DtFSEnd = New System.Windows.Forms.DateTimePicker()
        Me.cboFSDestination = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.CmdSaveAll = New System.Windows.Forms.Button()
        Me.ImageListFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.ContextMenuStripFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblshortcut = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblRM2 = New System.Windows.Forms.Label()
        Me.lblRM1 = New System.Windows.Forms.Label()
        Me.lblClientName = New System.Windows.Forms.Label()
        Me.GrpSelect.SuspendLayout()
        Me.GrpFees.SuspendLayout()
        Me.TabMgt.SuspendLayout()
        Me.TabPageMgt.SuspendLayout()
        Me.GrpMgtFees.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvMgtRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabExceptions.SuspendLayout()
        Me.TabExceptionGroup.SuspendLayout()
        CType(Me.dgvMgtExcGrp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabException.SuspendLayout()
        CType(Me.dgvMgtExcIns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageCustody.SuspendLayout()
        Me.GrpCF.SuspendLayout()
        Me.TabCFCustody.SuspendLayout()
        Me.TabCFRanges.SuspendLayout()
        CType(Me.dgvCFRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCFSafeKeeping.SuspendLayout()
        CType(Me.dgvCFSK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCFCustodyExceptions.SuspendLayout()
        Me.TabCFExceptionsGroup.SuspendLayout()
        CType(Me.dgvCFExcGrp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCFExceptions.SuspendLayout()
        CType(Me.dgvCFExcIns, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageTransactions.SuspendLayout()
        Me.GrpTF.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabFIGovt.SuspendLayout()
        Me.TabTrans.SuspendLayout()
        CType(Me.dgvTFRange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTransSK.SuspendLayout()
        CType(Me.dgvTFSK, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabTransSKFixed.SuspendLayout()
        CType(Me.dgvTFSKFixed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageTransfers.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageDocs.SuspendLayout()
        Me.GrpFiles.SuspendLayout()
        Me.TabPageEAMClients.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvEAMClients, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpFSDetails.SuspendLayout()
        Me.ContextMenuStripFiles.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrpSelect
        '
        Me.GrpSelect.Controls.Add(Me.ChkRedirectFee)
        Me.GrpSelect.Controls.Add(Me.ChkTemplateName)
        Me.GrpSelect.Controls.Add(Me.cboPortfolioRedirect)
        Me.GrpSelect.Controls.Add(Me.cboTemplateName)
        Me.GrpSelect.Controls.Add(Me.Label1)
        Me.GrpSelect.Controls.Add(Me.CboCCY)
        Me.GrpSelect.Controls.Add(Me.lblCCY)
        Me.GrpSelect.Controls.Add(Me.cboPortfolio)
        Me.GrpSelect.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrpSelect.Location = New System.Drawing.Point(24, 36)
        Me.GrpSelect.Name = "GrpSelect"
        Me.GrpSelect.Size = New System.Drawing.Size(686, 110)
        Me.GrpSelect.TabIndex = 0
        Me.GrpSelect.TabStop = False
        Me.GrpSelect.Text = "Selection Criteria"
        '
        'ChkRedirectFee
        '
        Me.ChkRedirectFee.AutoSize = True
        Me.ChkRedirectFee.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkRedirectFee.Location = New System.Drawing.Point(31, 81)
        Me.ChkRedirectFee.Name = "ChkRedirectFee"
        Me.ChkRedirectFee.Size = New System.Drawing.Size(90, 17)
        Me.ChkRedirectFee.TabIndex = 103
        Me.ChkRedirectFee.Text = "Redirect Fee:"
        Me.ChkRedirectFee.UseVisualStyleBackColor = True
        '
        'ChkTemplateName
        '
        Me.ChkTemplateName.AutoSize = True
        Me.ChkTemplateName.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkTemplateName.Location = New System.Drawing.Point(18, 51)
        Me.ChkTemplateName.Name = "ChkTemplateName"
        Me.ChkTemplateName.Size = New System.Drawing.Size(104, 17)
        Me.ChkTemplateName.TabIndex = 88
        Me.ChkTemplateName.Text = "Template Name:"
        Me.ChkTemplateName.UseVisualStyleBackColor = True
        '
        'cboPortfolioRedirect
        '
        Me.cboPortfolioRedirect.Enabled = False
        Me.cboPortfolioRedirect.FormattingEnabled = True
        Me.cboPortfolioRedirect.Location = New System.Drawing.Point(127, 79)
        Me.cboPortfolioRedirect.Name = "cboPortfolioRedirect"
        Me.cboPortfolioRedirect.Size = New System.Drawing.Size(408, 21)
        Me.cboPortfolioRedirect.TabIndex = 102
        '
        'cboTemplateName
        '
        Me.cboTemplateName.Enabled = False
        Me.cboTemplateName.FormattingEnabled = True
        Me.cboTemplateName.Location = New System.Drawing.Point(128, 49)
        Me.cboTemplateName.Name = "cboTemplateName"
        Me.cboTemplateName.Size = New System.Drawing.Size(407, 21)
        Me.cboTemplateName.TabIndex = 87
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(74, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 79
        Me.Label1.Text = "Portfolio:"
        '
        'CboCCY
        '
        Me.CboCCY.Enabled = False
        Me.CboCCY.FormattingEnabled = True
        Me.CboCCY.Location = New System.Drawing.Point(585, 20)
        Me.CboCCY.Name = "CboCCY"
        Me.CboCCY.Size = New System.Drawing.Size(81, 21)
        Me.CboCCY.TabIndex = 76
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(548, 24)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 78
        Me.lblCCY.Text = "CCY:"
        '
        'cboPortfolio
        '
        Me.cboPortfolio.Enabled = False
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(128, 20)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(407, 21)
        Me.cboPortfolio.TabIndex = 74
        '
        'GrpFees
        '
        Me.GrpFees.Controls.Add(Me.TabMgt)
        Me.GrpFees.Controls.Add(Me.GrpFSDetails)
        Me.GrpFees.Location = New System.Drawing.Point(24, 152)
        Me.GrpFees.Name = "GrpFees"
        Me.GrpFees.Size = New System.Drawing.Size(1040, 571)
        Me.GrpFees.TabIndex = 1
        Me.GrpFees.TabStop = False
        Me.GrpFees.Text = "Fee Details"
        '
        'TabMgt
        '
        Me.TabMgt.Controls.Add(Me.TabPageMgt)
        Me.TabMgt.Controls.Add(Me.TabPageCustody)
        Me.TabMgt.Controls.Add(Me.TabPageTransactions)
        Me.TabMgt.Controls.Add(Me.TabPageTransfers)
        Me.TabMgt.Controls.Add(Me.TabPageDocs)
        Me.TabMgt.Controls.Add(Me.TabPageEAMClients)
        Me.TabMgt.Location = New System.Drawing.Point(6, 103)
        Me.TabMgt.Name = "TabMgt"
        Me.TabMgt.SelectedIndex = 0
        Me.TabMgt.Size = New System.Drawing.Size(1024, 468)
        Me.TabMgt.TabIndex = 0
        '
        'TabPageMgt
        '
        Me.TabPageMgt.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageMgt.Controls.Add(Me.ChkMgtFees)
        Me.TabPageMgt.Controls.Add(Me.GrpMgtFees)
        Me.TabPageMgt.Location = New System.Drawing.Point(4, 22)
        Me.TabPageMgt.Name = "TabPageMgt"
        Me.TabPageMgt.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageMgt.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageMgt.TabIndex = 0
        Me.TabPageMgt.Text = "Mgt Fees"
        '
        'ChkMgtFees
        '
        Me.ChkMgtFees.AutoSize = True
        Me.ChkMgtFees.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.ChkMgtFees.Location = New System.Drawing.Point(13, 15)
        Me.ChkMgtFees.Name = "ChkMgtFees"
        Me.ChkMgtFees.Size = New System.Drawing.Size(120, 17)
        Me.ChkMgtFees.TabIndex = 83
        Me.ChkMgtFees.Text = "Management Fees?"
        Me.ChkMgtFees.UseVisualStyleBackColor = True
        '
        'GrpMgtFees
        '
        Me.GrpMgtFees.Controls.Add(Me.Label32)
        Me.GrpMgtFees.Controls.Add(Me.txtMgtMaintMin)
        Me.GrpMgtFees.Controls.Add(Me.GroupBox1)
        Me.GrpMgtFees.Controls.Add(Me.Label22)
        Me.GrpMgtFees.Controls.Add(Me.Label8)
        Me.GrpMgtFees.Controls.Add(Me.cboMGTChargeableCCY)
        Me.GrpMgtFees.Controls.Add(Me.txtMgtID)
        Me.GrpMgtFees.Controls.Add(Me.TabExceptions)
        Me.GrpMgtFees.Controls.Add(Me.Label10)
        Me.GrpMgtFees.Controls.Add(Me.Label9)
        Me.GrpMgtFees.Controls.Add(Me.Label4)
        Me.GrpMgtFees.Controls.Add(Me.txtMgtMax)
        Me.GrpMgtFees.Controls.Add(Me.txtMgtMin)
        Me.GrpMgtFees.Controls.Add(Me.cboMgtType)
        Me.GrpMgtFees.Location = New System.Drawing.Point(7, 38)
        Me.GrpMgtFees.Name = "GrpMgtFees"
        Me.GrpMgtFees.Size = New System.Drawing.Size(1003, 398)
        Me.GrpMgtFees.TabIndex = 82
        Me.GrpMgtFees.TabStop = False
        Me.GrpMgtFees.Text = "Management Fee Details"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(769, 26)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(96, 13)
        Me.Label32.TabIndex = 101
        Me.Label32.Text = "Min Maint. Charge:"
        '
        'txtMgtMaintMin
        '
        Me.txtMgtMaintMin.Location = New System.Drawing.Point(871, 23)
        Me.txtMgtMaintMin.Name = "txtMgtMaintMin"
        Me.txtMgtMaintMin.Size = New System.Drawing.Size(99, 20)
        Me.txtMgtMaintMin.TabIndex = 100
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.cboMGTCCYEqv)
        Me.GroupBox1.Controls.Add(Me.dgvMgtRange)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 65)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(494, 309)
        Me.GroupBox1.TabIndex = 99
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enter Ranges"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(273, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(119, 13)
        Me.Label16.TabIndex = 112
        Me.Label16.Text = "Range CCY Equivalent:"
        '
        'cboMGTCCYEqv
        '
        Me.cboMGTCCYEqv.FormattingEnabled = True
        Me.cboMGTCCYEqv.Location = New System.Drawing.Point(398, 13)
        Me.cboMGTCCYEqv.Name = "cboMGTCCYEqv"
        Me.cboMGTCCYEqv.Size = New System.Drawing.Size(90, 21)
        Me.cboMGTCCYEqv.TabIndex = 111
        '
        'dgvMgtRange
        '
        Me.dgvMgtRange.AllowUserToResizeColumns = False
        Me.dgvMgtRange.AllowUserToResizeRows = False
        Me.dgvMgtRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMgtRange.Location = New System.Drawing.Point(9, 37)
        Me.dgvMgtRange.MultiSelect = False
        Me.dgvMgtRange.Name = "dgvMgtRange"
        Me.dgvMgtRange.Size = New System.Drawing.Size(479, 260)
        Me.dgvMgtRange.TabIndex = 109
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(16, 26)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(88, 13)
        Me.Label22.TabIndex = 98
        Me.Label22.Text = "Chargeable CCY:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Maroon
        Me.Label8.Location = New System.Drawing.Point(626, 57)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(368, 13)
        Me.Label8.TabIndex = 93
        Me.Label8.Text = "Select Exceptions (Derivatives are already excluded from management fees):"
        '
        'cboMGTChargeableCCY
        '
        Me.cboMGTChargeableCCY.Enabled = False
        Me.cboMGTChargeableCCY.FormattingEnabled = True
        Me.cboMGTChargeableCCY.Location = New System.Drawing.Point(110, 22)
        Me.cboMGTChargeableCCY.Name = "cboMGTChargeableCCY"
        Me.cboMGTChargeableCCY.Size = New System.Drawing.Size(90, 21)
        Me.cboMGTChargeableCCY.TabIndex = 97
        '
        'txtMgtID
        '
        Me.txtMgtID.AcceptsReturn = True
        Me.txtMgtID.AcceptsTab = True
        Me.txtMgtID.BackColor = System.Drawing.Color.Linen
        Me.txtMgtID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMgtID.Enabled = False
        Me.txtMgtID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMgtID.Location = New System.Drawing.Point(6, 42)
        Me.txtMgtID.Multiline = True
        Me.txtMgtID.Name = "txtMgtID"
        Me.txtMgtID.Size = New System.Drawing.Size(27, 28)
        Me.txtMgtID.TabIndex = 51
        Me.txtMgtID.Visible = False
        '
        'TabExceptions
        '
        Me.TabExceptions.Controls.Add(Me.TabExceptionGroup)
        Me.TabExceptions.Controls.Add(Me.TabException)
        Me.TabExceptions.Location = New System.Drawing.Point(509, 75)
        Me.TabExceptions.Name = "TabExceptions"
        Me.TabExceptions.SelectedIndex = 0
        Me.TabExceptions.Size = New System.Drawing.Size(481, 299)
        Me.TabExceptions.TabIndex = 96
        '
        'TabExceptionGroup
        '
        Me.TabExceptionGroup.BackColor = System.Drawing.Color.Linen
        Me.TabExceptionGroup.Controls.Add(Me.dgvMgtExcGrp)
        Me.TabExceptionGroup.Location = New System.Drawing.Point(4, 22)
        Me.TabExceptionGroup.Name = "TabExceptionGroup"
        Me.TabExceptionGroup.Padding = New System.Windows.Forms.Padding(3)
        Me.TabExceptionGroup.Size = New System.Drawing.Size(473, 273)
        Me.TabExceptionGroup.TabIndex = 0
        Me.TabExceptionGroup.Text = "Instrument Group (Exceptions)"
        '
        'dgvMgtExcGrp
        '
        Me.dgvMgtExcGrp.AllowUserToResizeColumns = False
        Me.dgvMgtExcGrp.AllowUserToResizeRows = False
        Me.dgvMgtExcGrp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMgtExcGrp.Location = New System.Drawing.Point(6, 6)
        Me.dgvMgtExcGrp.MultiSelect = False
        Me.dgvMgtExcGrp.Name = "dgvMgtExcGrp"
        Me.dgvMgtExcGrp.Size = New System.Drawing.Size(461, 260)
        Me.dgvMgtExcGrp.TabIndex = 86
        '
        'TabException
        '
        Me.TabException.BackColor = System.Drawing.Color.Linen
        Me.TabException.Controls.Add(Me.dgvMgtExcIns)
        Me.TabException.Location = New System.Drawing.Point(4, 22)
        Me.TabException.Name = "TabException"
        Me.TabException.Padding = New System.Windows.Forms.Padding(3)
        Me.TabException.Size = New System.Drawing.Size(473, 273)
        Me.TabException.TabIndex = 1
        Me.TabException.Text = "Instrument (Exceptions)"
        '
        'dgvMgtExcIns
        '
        Me.dgvMgtExcIns.AllowUserToResizeColumns = False
        Me.dgvMgtExcIns.AllowUserToResizeRows = False
        Me.dgvMgtExcIns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMgtExcIns.Location = New System.Drawing.Point(6, 6)
        Me.dgvMgtExcIns.MultiSelect = False
        Me.dgvMgtExcIns.Name = "dgvMgtExcIns"
        Me.dgvMgtExcIns.Size = New System.Drawing.Size(461, 260)
        Me.dgvMgtExcIns.TabIndex = 87
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(591, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 95
        Me.Label10.Text = "Max Charge:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(416, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(64, 13)
        Me.Label9.TabIndex = 94
        Me.Label9.Text = "Min Charge:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(209, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(34, 13)
        Me.Label4.TabIndex = 89
        Me.Label4.Text = "Type:"
        '
        'txtMgtMax
        '
        Me.txtMgtMax.Location = New System.Drawing.Point(664, 23)
        Me.txtMgtMax.Name = "txtMgtMax"
        Me.txtMgtMax.Size = New System.Drawing.Size(99, 20)
        Me.txtMgtMax.TabIndex = 85
        '
        'txtMgtMin
        '
        Me.txtMgtMin.Location = New System.Drawing.Point(486, 23)
        Me.txtMgtMin.Name = "txtMgtMin"
        Me.txtMgtMin.Size = New System.Drawing.Size(99, 20)
        Me.txtMgtMin.TabIndex = 84
        '
        'cboMgtType
        '
        Me.cboMgtType.FormattingEnabled = True
        Me.cboMgtType.Location = New System.Drawing.Point(249, 22)
        Me.cboMgtType.Name = "cboMgtType"
        Me.cboMgtType.Size = New System.Drawing.Size(161, 21)
        Me.cboMgtType.TabIndex = 81
        '
        'TabPageCustody
        '
        Me.TabPageCustody.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageCustody.Controls.Add(Me.ChkCF)
        Me.TabPageCustody.Controls.Add(Me.GrpCF)
        Me.TabPageCustody.Location = New System.Drawing.Point(4, 22)
        Me.TabPageCustody.Name = "TabPageCustody"
        Me.TabPageCustody.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageCustody.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageCustody.TabIndex = 1
        Me.TabPageCustody.Text = "Custody Fees"
        '
        'ChkCF
        '
        Me.ChkCF.AutoSize = True
        Me.ChkCF.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.ChkCF.Location = New System.Drawing.Point(13, 15)
        Me.ChkCF.Name = "ChkCF"
        Me.ChkCF.Size = New System.Drawing.Size(96, 17)
        Me.ChkCF.TabIndex = 87
        Me.ChkCF.Tag = "CF"
        Me.ChkCF.Text = "Custody Fees?"
        Me.ChkCF.UseVisualStyleBackColor = True
        '
        'GrpCF
        '
        Me.GrpCF.Controls.Add(Me.Label34)
        Me.GrpCF.Controls.Add(Me.txtCFMaintMin)
        Me.GrpCF.Controls.Add(Me.TabCFCustody)
        Me.GrpCF.Controls.Add(Me.Label2)
        Me.GrpCF.Controls.Add(Me.Label15)
        Me.GrpCF.Controls.Add(Me.txtCFMax)
        Me.GrpCF.Controls.Add(Me.txtCFMin)
        Me.GrpCF.Controls.Add(Me.Label13)
        Me.GrpCF.Controls.Add(Me.cboCFType)
        Me.GrpCF.Controls.Add(Me.TextBox1)
        Me.GrpCF.Controls.Add(Me.TabCFCustodyExceptions)
        Me.GrpCF.Controls.Add(Me.Label19)
        Me.GrpCF.Controls.Add(Me.cboCFChargeableCCY)
        Me.GrpCF.Location = New System.Drawing.Point(7, 38)
        Me.GrpCF.Name = "GrpCF"
        Me.GrpCF.Size = New System.Drawing.Size(1000, 387)
        Me.GrpCF.TabIndex = 86
        Me.GrpCF.TabStop = False
        Me.GrpCF.Text = "Custody Fee Details"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(769, 26)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(96, 13)
        Me.Label34.TabIndex = 113
        Me.Label34.Text = "Min Maint. Charge:"
        '
        'txtCFMaintMin
        '
        Me.txtCFMaintMin.Location = New System.Drawing.Point(871, 23)
        Me.txtCFMaintMin.Name = "txtCFMaintMin"
        Me.txtCFMaintMin.Size = New System.Drawing.Size(99, 20)
        Me.txtCFMaintMin.TabIndex = 112
        '
        'TabCFCustody
        '
        Me.TabCFCustody.Controls.Add(Me.TabCFRanges)
        Me.TabCFCustody.Controls.Add(Me.TabCFSafeKeeping)
        Me.TabCFCustody.Location = New System.Drawing.Point(7, 53)
        Me.TabCFCustody.Name = "TabCFCustody"
        Me.TabCFCustody.SelectedIndex = 0
        Me.TabCFCustody.Size = New System.Drawing.Size(496, 320)
        Me.TabCFCustody.TabIndex = 111
        '
        'TabCFRanges
        '
        Me.TabCFRanges.BackColor = System.Drawing.Color.FloralWhite
        Me.TabCFRanges.Controls.Add(Me.Label23)
        Me.TabCFRanges.Controls.Add(Me.cboCFCCYEqv)
        Me.TabCFRanges.Controls.Add(Me.dgvCFRange)
        Me.TabCFRanges.Location = New System.Drawing.Point(4, 22)
        Me.TabCFRanges.Name = "TabCFRanges"
        Me.TabCFRanges.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCFRanges.Size = New System.Drawing.Size(488, 294)
        Me.TabCFRanges.TabIndex = 0
        Me.TabCFRanges.Text = "Fee Ranges"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(267, 9)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(119, 13)
        Me.Label23.TabIndex = 110
        Me.Label23.Text = "Range CCY Equivalent:"
        '
        'cboCFCCYEqv
        '
        Me.cboCFCCYEqv.FormattingEnabled = True
        Me.cboCFCCYEqv.Location = New System.Drawing.Point(392, 6)
        Me.cboCFCCYEqv.Name = "cboCFCCYEqv"
        Me.cboCFCCYEqv.Size = New System.Drawing.Size(90, 21)
        Me.cboCFCCYEqv.TabIndex = 109
        '
        'dgvCFRange
        '
        Me.dgvCFRange.AllowUserToResizeColumns = False
        Me.dgvCFRange.AllowUserToResizeRows = False
        Me.dgvCFRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCFRange.Location = New System.Drawing.Point(6, 27)
        Me.dgvCFRange.MultiSelect = False
        Me.dgvCFRange.Name = "dgvCFRange"
        Me.dgvCFRange.Size = New System.Drawing.Size(476, 260)
        Me.dgvCFRange.TabIndex = 87
        Me.dgvCFRange.Tag = "CF"
        '
        'TabCFSafeKeeping
        '
        Me.TabCFSafeKeeping.Controls.Add(Me.dgvCFSK)
        Me.TabCFSafeKeeping.Location = New System.Drawing.Point(4, 22)
        Me.TabCFSafeKeeping.Name = "TabCFSafeKeeping"
        Me.TabCFSafeKeeping.Padding = New System.Windows.Forms.Padding(3)
        Me.TabCFSafeKeeping.Size = New System.Drawing.Size(488, 294)
        Me.TabCFSafeKeeping.TabIndex = 1
        Me.TabCFSafeKeeping.Text = "Safe Keeping"
        Me.TabCFSafeKeeping.UseVisualStyleBackColor = True
        '
        'dgvCFSK
        '
        Me.dgvCFSK.AllowUserToResizeColumns = False
        Me.dgvCFSK.AllowUserToResizeRows = False
        Me.dgvCFSK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCFSK.Location = New System.Drawing.Point(6, 6)
        Me.dgvCFSK.MultiSelect = False
        Me.dgvCFSK.Name = "dgvCFSK"
        Me.dgvCFSK.Size = New System.Drawing.Size(476, 281)
        Me.dgvCFSK.TabIndex = 86
        Me.dgvCFSK.Tag = "CF"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(591, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 110
        Me.Label2.Text = "Max Charge:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(416, 26)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 13)
        Me.Label15.TabIndex = 109
        Me.Label15.Text = "Min Charge:"
        '
        'txtCFMax
        '
        Me.txtCFMax.Location = New System.Drawing.Point(664, 22)
        Me.txtCFMax.Name = "txtCFMax"
        Me.txtCFMax.Size = New System.Drawing.Size(99, 20)
        Me.txtCFMax.TabIndex = 108
        Me.txtCFMax.Tag = "CF"
        '
        'txtCFMin
        '
        Me.txtCFMin.Location = New System.Drawing.Point(486, 23)
        Me.txtCFMin.Name = "txtCFMin"
        Me.txtCFMin.Size = New System.Drawing.Size(99, 20)
        Me.txtCFMin.TabIndex = 107
        Me.txtCFMin.Tag = "CF"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(209, 26)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(34, 13)
        Me.Label13.TabIndex = 98
        Me.Label13.Text = "Type:"
        '
        'cboCFType
        '
        Me.cboCFType.FormattingEnabled = True
        Me.cboCFType.Location = New System.Drawing.Point(249, 22)
        Me.cboCFType.Name = "cboCFType"
        Me.cboCFType.Size = New System.Drawing.Size(161, 21)
        Me.cboCFType.TabIndex = 97
        Me.cboCFType.Tag = "CF"
        '
        'TextBox1
        '
        Me.TextBox1.AcceptsReturn = True
        Me.TextBox1.AcceptsTab = True
        Me.TextBox1.BackColor = System.Drawing.Color.Linen
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(6, 19)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(10, 28)
        Me.TextBox1.TabIndex = 51
        Me.TextBox1.Visible = False
        '
        'TabCFCustodyExceptions
        '
        Me.TabCFCustodyExceptions.Controls.Add(Me.TabCFExceptionsGroup)
        Me.TabCFCustodyExceptions.Controls.Add(Me.TabCFExceptions)
        Me.TabCFCustodyExceptions.Location = New System.Drawing.Point(505, 74)
        Me.TabCFCustodyExceptions.Name = "TabCFCustodyExceptions"
        Me.TabCFCustodyExceptions.SelectedIndex = 0
        Me.TabCFCustodyExceptions.Size = New System.Drawing.Size(489, 299)
        Me.TabCFCustodyExceptions.TabIndex = 96
        '
        'TabCFExceptionsGroup
        '
        Me.TabCFExceptionsGroup.Controls.Add(Me.dgvCFExcGrp)
        Me.TabCFExceptionsGroup.Location = New System.Drawing.Point(4, 22)
        Me.TabCFExceptionsGroup.Name = "TabCFExceptionsGroup"
        Me.TabCFExceptionsGroup.Size = New System.Drawing.Size(481, 273)
        Me.TabCFExceptionsGroup.TabIndex = 1
        Me.TabCFExceptionsGroup.Text = "Instrument Group (Exceptions)"
        Me.TabCFExceptionsGroup.UseVisualStyleBackColor = True
        '
        'dgvCFExcGrp
        '
        Me.dgvCFExcGrp.AllowUserToResizeColumns = False
        Me.dgvCFExcGrp.AllowUserToResizeRows = False
        Me.dgvCFExcGrp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCFExcGrp.Location = New System.Drawing.Point(3, 6)
        Me.dgvCFExcGrp.MultiSelect = False
        Me.dgvCFExcGrp.Name = "dgvCFExcGrp"
        Me.dgvCFExcGrp.Size = New System.Drawing.Size(470, 260)
        Me.dgvCFExcGrp.TabIndex = 87
        Me.dgvCFExcGrp.Tag = "CF"
        '
        'TabCFExceptions
        '
        Me.TabCFExceptions.Controls.Add(Me.dgvCFExcIns)
        Me.TabCFExceptions.Location = New System.Drawing.Point(4, 22)
        Me.TabCFExceptions.Name = "TabCFExceptions"
        Me.TabCFExceptions.Size = New System.Drawing.Size(481, 273)
        Me.TabCFExceptions.TabIndex = 2
        Me.TabCFExceptions.Text = "Instrument (Exceptions)"
        Me.TabCFExceptions.UseVisualStyleBackColor = True
        '
        'dgvCFExcIns
        '
        Me.dgvCFExcIns.AllowUserToResizeColumns = False
        Me.dgvCFExcIns.AllowUserToResizeRows = False
        Me.dgvCFExcIns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCFExcIns.Location = New System.Drawing.Point(3, 6)
        Me.dgvCFExcIns.MultiSelect = False
        Me.dgvCFExcIns.Name = "dgvCFExcIns"
        Me.dgvCFExcIns.Size = New System.Drawing.Size(470, 260)
        Me.dgvCFExcIns.TabIndex = 88
        Me.dgvCFExcIns.Tag = "CF"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(16, 26)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 13)
        Me.Label19.TabIndex = 89
        Me.Label19.Text = "Chargeable CCY:"
        '
        'cboCFChargeableCCY
        '
        Me.cboCFChargeableCCY.Enabled = False
        Me.cboCFChargeableCCY.FormattingEnabled = True
        Me.cboCFChargeableCCY.Location = New System.Drawing.Point(110, 22)
        Me.cboCFChargeableCCY.Name = "cboCFChargeableCCY"
        Me.cboCFChargeableCCY.Size = New System.Drawing.Size(90, 21)
        Me.cboCFChargeableCCY.TabIndex = 81
        Me.cboCFChargeableCCY.Tag = "CF"
        '
        'TabPageTransactions
        '
        Me.TabPageTransactions.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageTransactions.Controls.Add(Me.ChkTF)
        Me.TabPageTransactions.Controls.Add(Me.GrpTF)
        Me.TabPageTransactions.Location = New System.Drawing.Point(4, 22)
        Me.TabPageTransactions.Name = "TabPageTransactions"
        Me.TabPageTransactions.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageTransactions.TabIndex = 2
        Me.TabPageTransactions.Text = "Transaction Fees"
        '
        'ChkTF
        '
        Me.ChkTF.AutoSize = True
        Me.ChkTF.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.ChkTF.Location = New System.Drawing.Point(13, 15)
        Me.ChkTF.Name = "ChkTF"
        Me.ChkTF.Size = New System.Drawing.Size(114, 17)
        Me.ChkTF.TabIndex = 91
        Me.ChkTF.Text = "Transaction Fees?"
        Me.ChkTF.UseVisualStyleBackColor = True
        '
        'GrpTF
        '
        Me.GrpTF.Controls.Add(Me.GroupBox3)
        Me.GrpTF.Controls.Add(Me.TabFIGovt)
        Me.GrpTF.Controls.Add(Me.Label17)
        Me.GrpTF.Controls.Add(Me.cboTFType)
        Me.GrpTF.Controls.Add(Me.TextBox2)
        Me.GrpTF.Location = New System.Drawing.Point(7, 38)
        Me.GrpTF.Name = "GrpTF"
        Me.GrpTF.Size = New System.Drawing.Size(1000, 401)
        Me.GrpTF.TabIndex = 90
        Me.GrpTF.TabStop = False
        Me.GrpTF.Text = "Transaction Fee Details"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ChkCoverBrokerage)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txtTFFOPFee)
        Me.GroupBox3.Controls.Add(Me.cboTFFOPFeeCCY)
        Me.GroupBox3.Controls.Add(Me.Label33)
        Me.GroupBox3.Controls.Add(Me.Label28)
        Me.GroupBox3.Controls.Add(Me.txtTFTransFee)
        Me.GroupBox3.Controls.Add(Me.cboTFTransFeeCCY)
        Me.GroupBox3.Controls.Add(Me.Label30)
        Me.GroupBox3.Controls.Add(Me.Label27)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.txtTFTicketFee)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.txtTFMax)
        Me.GroupBox3.Controls.Add(Me.cboTFTicketFeeCCY)
        Me.GroupBox3.Controls.Add(Me.txtTFMin)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Location = New System.Drawing.Point(568, 75)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(426, 296)
        Me.GroupBox3.TabIndex = 109
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Default Ticket Fees && Charges"
        '
        'ChkCoverBrokerage
        '
        Me.ChkCoverBrokerage.AutoSize = True
        Me.ChkCoverBrokerage.Location = New System.Drawing.Point(156, 140)
        Me.ChkCoverBrokerage.Name = "ChkCoverBrokerage"
        Me.ChkCoverBrokerage.Size = New System.Drawing.Size(169, 17)
        Me.ChkCoverBrokerage.TabIndex = 125
        Me.ChkCoverBrokerage.Text = "Dolfin to cover brokerage fees"
        Me.ChkCoverBrokerage.UseVisualStyleBackColor = True
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(98, 108)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(52, 13)
        Me.Label31.TabIndex = 124
        Me.Label31.Text = "FOP Fee:"
        '
        'txtTFFOPFee
        '
        Me.txtTFFOPFee.Location = New System.Drawing.Point(156, 105)
        Me.txtTFFOPFee.Name = "txtTFFOPFee"
        Me.txtTFFOPFee.Size = New System.Drawing.Size(50, 20)
        Me.txtTFFOPFee.TabIndex = 123
        '
        'cboTFFOPFeeCCY
        '
        Me.cboTFFOPFeeCCY.FormattingEnabled = True
        Me.cboTFFOPFeeCCY.Location = New System.Drawing.Point(347, 104)
        Me.cboTFFOPFeeCCY.Name = "cboTFFOPFeeCCY"
        Me.cboTFFOPFeeCCY.Size = New System.Drawing.Size(70, 21)
        Me.cboTFFOPFeeCCY.TabIndex = 120
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(265, 107)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(76, 13)
        Me.Label33.TabIndex = 121
        Me.Label33.Text = "FOP Fee CCY:"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(61, 82)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(89, 13)
        Me.Label28.TabIndex = 119
        Me.Label28.Text = "Fixed Ticket Fee:"
        '
        'txtTFTransFee
        '
        Me.txtTFTransFee.Location = New System.Drawing.Point(156, 79)
        Me.txtTFTransFee.Name = "txtTFTransFee"
        Me.txtTFTransFee.Size = New System.Drawing.Size(50, 20)
        Me.txtTFTransFee.TabIndex = 118
        '
        'cboTFTransFeeCCY
        '
        Me.cboTFTransFeeCCY.FormattingEnabled = True
        Me.cboTFTransFeeCCY.Location = New System.Drawing.Point(347, 78)
        Me.cboTFTransFeeCCY.Name = "cboTFTransFeeCCY"
        Me.cboTFTransFeeCCY.Size = New System.Drawing.Size(70, 21)
        Me.cboTFTransFeeCCY.TabIndex = 115
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(259, 82)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(80, 13)
        Me.Label30.TabIndex = 116
        Me.Label30.Text = "Fixed Fee CCY:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(42, 56)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(108, 13)
        Me.Label27.TabIndex = 114
        Me.Label27.Text = "Ticket Fee (Basis Pt):"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(217, 29)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(67, 13)
        Me.Label25.TabIndex = 113
        Me.Label25.Text = "Max Charge:"
        '
        'txtTFTicketFee
        '
        Me.txtTFTicketFee.Location = New System.Drawing.Point(156, 53)
        Me.txtTFTicketFee.Name = "txtTFTicketFee"
        Me.txtTFTicketFee.Size = New System.Drawing.Size(50, 20)
        Me.txtTFTicketFee.TabIndex = 110
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(7, 29)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(64, 13)
        Me.Label26.TabIndex = 112
        Me.Label26.Text = "Min Charge:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(207, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(25, 13)
        Me.Label24.TabIndex = 109
        Me.Label24.Text = "Bps"
        '
        'txtTFMax
        '
        Me.txtTFMax.Location = New System.Drawing.Point(290, 26)
        Me.txtTFMax.Name = "txtTFMax"
        Me.txtTFMax.Size = New System.Drawing.Size(128, 20)
        Me.txtTFMax.TabIndex = 111
        '
        'cboTFTicketFeeCCY
        '
        Me.cboTFTicketFeeCCY.FormattingEnabled = True
        Me.cboTFTicketFeeCCY.Location = New System.Drawing.Point(347, 52)
        Me.cboTFTicketFeeCCY.Name = "cboTFTicketFeeCCY"
        Me.cboTFTicketFeeCCY.Size = New System.Drawing.Size(70, 21)
        Me.cboTFTicketFeeCCY.TabIndex = 107
        '
        'txtTFMin
        '
        Me.txtTFMin.Location = New System.Drawing.Point(77, 26)
        Me.txtTFMin.Name = "txtTFMin"
        Me.txtTFMin.Size = New System.Drawing.Size(128, 20)
        Me.txtTFMin.TabIndex = 110
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(256, 56)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(85, 13)
        Me.Label21.TabIndex = 108
        Me.Label21.Text = "Ticket Fee CCY:"
        '
        'TabFIGovt
        '
        Me.TabFIGovt.Controls.Add(Me.TabTrans)
        Me.TabFIGovt.Controls.Add(Me.TabTransSK)
        Me.TabFIGovt.Controls.Add(Me.TabTransSKFixed)
        Me.TabFIGovt.Location = New System.Drawing.Point(7, 53)
        Me.TabFIGovt.Name = "TabFIGovt"
        Me.TabFIGovt.SelectedIndex = 0
        Me.TabFIGovt.Size = New System.Drawing.Size(555, 318)
        Me.TabFIGovt.TabIndex = 106
        '
        'TabTrans
        '
        Me.TabTrans.Controls.Add(Me.Label7)
        Me.TabTrans.Controls.Add(Me.cboTFCCYEqv)
        Me.TabTrans.Controls.Add(Me.dgvTFRange)
        Me.TabTrans.Location = New System.Drawing.Point(4, 22)
        Me.TabTrans.Name = "TabTrans"
        Me.TabTrans.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTrans.Size = New System.Drawing.Size(547, 292)
        Me.TabTrans.TabIndex = 0
        Me.TabTrans.Text = "Fee Ranges"
        Me.TabTrans.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(326, 6)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(119, 13)
        Me.Label7.TabIndex = 112
        Me.Label7.Text = "Range CCY Equivalent:"
        '
        'cboTFCCYEqv
        '
        Me.cboTFCCYEqv.FormattingEnabled = True
        Me.cboTFCCYEqv.Location = New System.Drawing.Point(451, 3)
        Me.cboTFCCYEqv.Name = "cboTFCCYEqv"
        Me.cboTFCCYEqv.Size = New System.Drawing.Size(90, 21)
        Me.cboTFCCYEqv.TabIndex = 111
        '
        'dgvTFRange
        '
        Me.dgvTFRange.AllowUserToResizeColumns = False
        Me.dgvTFRange.AllowUserToResizeRows = False
        Me.dgvTFRange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTFRange.Location = New System.Drawing.Point(6, 26)
        Me.dgvTFRange.MultiSelect = False
        Me.dgvTFRange.Name = "dgvTFRange"
        Me.dgvTFRange.Size = New System.Drawing.Size(535, 260)
        Me.dgvTFRange.TabIndex = 100
        '
        'TabTransSK
        '
        Me.TabTransSK.Controls.Add(Me.dgvTFSK)
        Me.TabTransSK.Location = New System.Drawing.Point(4, 22)
        Me.TabTransSK.Name = "TabTransSK"
        Me.TabTransSK.Size = New System.Drawing.Size(547, 292)
        Me.TabTransSK.TabIndex = 1
        Me.TabTransSK.Text = "SafeKeeping (Variable)"
        Me.TabTransSK.UseVisualStyleBackColor = True
        '
        'dgvTFSK
        '
        Me.dgvTFSK.AllowUserToResizeColumns = False
        Me.dgvTFSK.AllowUserToResizeRows = False
        Me.dgvTFSK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTFSK.Location = New System.Drawing.Point(6, 5)
        Me.dgvTFSK.MultiSelect = False
        Me.dgvTFSK.Name = "dgvTFSK"
        Me.dgvTFSK.Size = New System.Drawing.Size(535, 283)
        Me.dgvTFSK.TabIndex = 101
        '
        'TabTransSKFixed
        '
        Me.TabTransSKFixed.Controls.Add(Me.dgvTFSKFixed)
        Me.TabTransSKFixed.Location = New System.Drawing.Point(4, 22)
        Me.TabTransSKFixed.Name = "TabTransSKFixed"
        Me.TabTransSKFixed.Size = New System.Drawing.Size(547, 292)
        Me.TabTransSKFixed.TabIndex = 2
        Me.TabTransSKFixed.Text = "SafeKeeping (Fixed)"
        Me.TabTransSKFixed.UseVisualStyleBackColor = True
        '
        'dgvTFSKFixed
        '
        Me.dgvTFSKFixed.AllowUserToResizeColumns = False
        Me.dgvTFSKFixed.AllowUserToResizeRows = False
        Me.dgvTFSKFixed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTFSKFixed.Location = New System.Drawing.Point(6, 5)
        Me.dgvTFSKFixed.MultiSelect = False
        Me.dgvTFSKFixed.Name = "dgvTFSKFixed"
        Me.dgvTFSKFixed.Size = New System.Drawing.Size(535, 283)
        Me.dgvTFSKFixed.TabIndex = 102
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(25, 28)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 13)
        Me.Label17.TabIndex = 104
        Me.Label17.Text = "Type:"
        '
        'cboTFType
        '
        Me.cboTFType.FormattingEnabled = True
        Me.cboTFType.Location = New System.Drawing.Point(65, 24)
        Me.cboTFType.Name = "cboTFType"
        Me.cboTFType.Size = New System.Drawing.Size(161, 21)
        Me.cboTFType.TabIndex = 103
        '
        'TextBox2
        '
        Me.TextBox2.AcceptsReturn = True
        Me.TextBox2.AcceptsTab = True
        Me.TextBox2.BackColor = System.Drawing.Color.Linen
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.Enabled = False
        Me.TextBox2.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(3, 12)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(12, 28)
        Me.TextBox2.TabIndex = 51
        Me.TextBox2.Visible = False
        '
        'TabPageTransfers
        '
        Me.TabPageTransfers.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageTransfers.Controls.Add(Me.GroupBox2)
        Me.TabPageTransfers.Location = New System.Drawing.Point(4, 22)
        Me.TabPageTransfers.Name = "TabPageTransfers"
        Me.TabPageTransfers.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageTransfers.TabIndex = 3
        Me.TabPageTransfers.Text = "Transfer Fees"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvTrans)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 38)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1000, 401)
        Me.GroupBox2.TabIndex = 30
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Transaction Fees"
        '
        'dgvTrans
        '
        Me.dgvTrans.AllowUserToAddRows = False
        Me.dgvTrans.AllowUserToDeleteRows = False
        Me.dgvTrans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTrans.Location = New System.Drawing.Point(15, 29)
        Me.dgvTrans.Name = "dgvTrans"
        Me.dgvTrans.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTrans.Size = New System.Drawing.Size(841, 114)
        Me.dgvTrans.TabIndex = 28
        '
        'TabPageDocs
        '
        Me.TabPageDocs.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageDocs.Controls.Add(Me.GrpFiles)
        Me.TabPageDocs.Location = New System.Drawing.Point(4, 22)
        Me.TabPageDocs.Name = "TabPageDocs"
        Me.TabPageDocs.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageDocs.TabIndex = 4
        Me.TabPageDocs.Text = "Documentation"
        '
        'GrpFiles
        '
        Me.GrpFiles.Controls.Add(Me.TreeViewFiles)
        Me.GrpFiles.Controls.Add(Me.LVFiles)
        Me.GrpFiles.Location = New System.Drawing.Point(8, 14)
        Me.GrpFiles.Name = "GrpFiles"
        Me.GrpFiles.Size = New System.Drawing.Size(1004, 395)
        Me.GrpFiles.TabIndex = 9
        Me.GrpFiles.TabStop = False
        Me.GrpFiles.Text = "Files associated with these fees are stored here (drag && drop to add, right clic" &
    "k for file menu):"
        '
        'TreeViewFiles
        '
        Me.TreeViewFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.TreeViewFiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeViewFiles.Location = New System.Drawing.Point(15, 19)
        Me.TreeViewFiles.Name = "TreeViewFiles"
        Me.TreeViewFiles.Size = New System.Drawing.Size(375, 365)
        Me.TreeViewFiles.TabIndex = 7
        '
        'LVFiles
        '
        Me.LVFiles.AllowDrop = True
        Me.LVFiles.BackColor = System.Drawing.SystemColors.Window
        Me.LVFiles.BackgroundImage = CType(resources.GetObject("LVFiles.BackgroundImage"), System.Drawing.Image)
        Me.LVFiles.BackgroundImageTiled = True
        Me.LVFiles.FullRowSelect = True
        Me.LVFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LVFiles.Location = New System.Drawing.Point(396, 19)
        Me.LVFiles.MultiSelect = False
        Me.LVFiles.Name = "LVFiles"
        Me.LVFiles.Size = New System.Drawing.Size(602, 365)
        Me.LVFiles.TabIndex = 6
        Me.LVFiles.UseCompatibleStateImageBehavior = False
        '
        'TabPageEAMClients
        '
        Me.TabPageEAMClients.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPageEAMClients.Controls.Add(Me.GroupBox5)
        Me.TabPageEAMClients.Location = New System.Drawing.Point(4, 22)
        Me.TabPageEAMClients.Name = "TabPageEAMClients"
        Me.TabPageEAMClients.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageEAMClients.Size = New System.Drawing.Size(1016, 442)
        Me.TabPageEAMClients.TabIndex = 5
        Me.TabPageEAMClients.Text = "EAM Clients"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dgvEAMClients)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 21)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(1000, 401)
        Me.GroupBox5.TabIndex = 31
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "EAM Clients"
        '
        'dgvEAMClients
        '
        Me.dgvEAMClients.AllowUserToAddRows = False
        Me.dgvEAMClients.AllowUserToDeleteRows = False
        Me.dgvEAMClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEAMClients.Location = New System.Drawing.Point(15, 29)
        Me.dgvEAMClients.Name = "dgvEAMClients"
        Me.dgvEAMClients.Size = New System.Drawing.Size(979, 366)
        Me.dgvEAMClients.TabIndex = 28
        '
        'GrpFSDetails
        '
        Me.GrpFSDetails.Controls.Add(Me.Label29)
        Me.GrpFSDetails.Controls.Add(Me.cboFSIsEAM)
        Me.GrpFSDetails.Controls.Add(Me.Label20)
        Me.GrpFSDetails.Controls.Add(Me.cboFSPayFreq)
        Me.GrpFSDetails.Controls.Add(Me.Label18)
        Me.GrpFSDetails.Controls.Add(Me.DtFSStart)
        Me.GrpFSDetails.Controls.Add(Me.DtFSEnd)
        Me.GrpFSDetails.Controls.Add(Me.cboFSDestination)
        Me.GrpFSDetails.Controls.Add(Me.Label5)
        Me.GrpFSDetails.Controls.Add(Me.Label6)
        Me.GrpFSDetails.Location = New System.Drawing.Point(10, 19)
        Me.GrpFSDetails.Name = "GrpFSDetails"
        Me.GrpFSDetails.Size = New System.Drawing.Size(1016, 79)
        Me.GrpFSDetails.TabIndex = 12
        Me.GrpFSDetails.TabStop = False
        Me.GrpFSDetails.Text = "Fee Schedule Details"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(534, 50)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(50, 13)
        Me.Label29.TabIndex = 97
        Me.Label29.Text = "Is EAM?:"
        '
        'cboFSIsEAM
        '
        Me.cboFSIsEAM.FormattingEnabled = True
        Me.cboFSIsEAM.Location = New System.Drawing.Point(590, 47)
        Me.cboFSIsEAM.Name = "cboFSIsEAM"
        Me.cboFSIsEAM.Size = New System.Drawing.Size(62, 21)
        Me.cboFSIsEAM.TabIndex = 96
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(262, 50)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(81, 13)
        Me.Label20.TabIndex = 95
        Me.Label20.Text = "Pay Frequency:"
        '
        'cboFSPayFreq
        '
        Me.cboFSPayFreq.FormattingEnabled = True
        Me.cboFSPayFreq.Location = New System.Drawing.Point(349, 47)
        Me.cboFSPayFreq.Name = "cboFSPayFreq"
        Me.cboFSPayFreq.Size = New System.Drawing.Size(161, 21)
        Me.cboFSPayFreq.TabIndex = 94
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(11, 50)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(63, 13)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "Destination:"
        '
        'DtFSStart
        '
        Me.DtFSStart.Location = New System.Drawing.Point(80, 19)
        Me.DtFSStart.Name = "DtFSStart"
        Me.DtFSStart.Size = New System.Drawing.Size(161, 20)
        Me.DtFSStart.TabIndex = 82
        '
        'DtFSEnd
        '
        Me.DtFSEnd.Location = New System.Drawing.Point(349, 19)
        Me.DtFSEnd.Name = "DtFSEnd"
        Me.DtFSEnd.Size = New System.Drawing.Size(161, 20)
        Me.DtFSEnd.TabIndex = 83
        '
        'cboFSDestination
        '
        Me.cboFSDestination.FormattingEnabled = True
        Me.cboFSDestination.Location = New System.Drawing.Point(80, 47)
        Me.cboFSDestination.Name = "cboFSDestination"
        Me.cboFSDestination.Size = New System.Drawing.Size(161, 21)
        Me.cboFSDestination.TabIndex = 92
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 13)
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Start Date:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(288, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "End Date:"
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(30, 9)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(57, 24)
        Me.lblpaymentTitle.TabIndex = 8
        Me.lblpaymentTitle.Text = "Fees"
        '
        'CmdSaveAll
        '
        Me.CmdSaveAll.Location = New System.Drawing.Point(810, 729)
        Me.CmdSaveAll.Name = "CmdSaveAll"
        Me.CmdSaveAll.Size = New System.Drawing.Size(254, 23)
        Me.CmdSaveAll.TabIndex = 9
        Me.CmdSaveAll.Text = "Save All Fees"
        Me.CmdSaveAll.UseVisualStyleBackColor = True
        '
        'ImageListFiles
        '
        Me.ImageListFiles.ImageStream = CType(resources.GetObject("ImageListFiles.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListFiles.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListFiles.Images.SetKeyName(0, "computer.jpg")
        Me.ImageListFiles.Images.SetKeyName(1, "download3.jpg")
        '
        'ContextMenuStripFiles
        '
        Me.ContextMenuStripFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenFileToolStripMenuItem, Me.RemoveFileToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStripFiles.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles.Size = New System.Drawing.Size(139, 70)
        '
        'OpenFileToolStripMenuItem
        '
        Me.OpenFileToolStripMenuItem.Name = "OpenFileToolStripMenuItem"
        Me.OpenFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.OpenFileToolStripMenuItem.Text = "&Open File"
        '
        'RemoveFileToolStripMenuItem
        '
        Me.RemoveFileToolStripMenuItem.Name = "RemoveFileToolStripMenuItem"
        Me.RemoveFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RemoveFileToolStripMenuItem.Text = "&Remove File"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.lblshortcut)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.lblRM2)
        Me.GroupBox4.Controls.Add(Me.lblRM1)
        Me.GroupBox4.Controls.Add(Me.lblClientName)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(719, 36)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(345, 109)
        Me.GroupBox4.TabIndex = 10
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Portfolio Information"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(28, 41)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 12)
        Me.Label14.TabIndex = 84
        Me.Label14.Text = "Portfolio Code:"
        '
        'lblshortcut
        '
        Me.lblshortcut.AutoSize = True
        Me.lblshortcut.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblshortcut.ForeColor = System.Drawing.Color.Navy
        Me.lblshortcut.Location = New System.Drawing.Point(97, 41)
        Me.lblshortcut.Name = "lblshortcut"
        Me.lblshortcut.Size = New System.Drawing.Size(0, 13)
        Me.lblshortcut.TabIndex = 83
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(11, 83)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(85, 12)
        Me.Label12.TabIndex = 82
        Me.Label12.Text = "Relationship Mgr 2:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(11, 61)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(85, 12)
        Me.Label11.TabIndex = 81
        Me.Label11.Text = "Relationship Mgr 1:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(37, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 80
        Me.Label3.Text = "Client Name:"
        '
        'lblRM2
        '
        Me.lblRM2.AutoSize = True
        Me.lblRM2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRM2.ForeColor = System.Drawing.Color.Navy
        Me.lblRM2.Location = New System.Drawing.Point(97, 83)
        Me.lblRM2.Name = "lblRM2"
        Me.lblRM2.Size = New System.Drawing.Size(0, 13)
        Me.lblRM2.TabIndex = 2
        '
        'lblRM1
        '
        Me.lblRM1.AutoSize = True
        Me.lblRM1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRM1.ForeColor = System.Drawing.Color.Navy
        Me.lblRM1.Location = New System.Drawing.Point(97, 61)
        Me.lblRM1.Name = "lblRM1"
        Me.lblRM1.Size = New System.Drawing.Size(0, 13)
        Me.lblRM1.TabIndex = 1
        '
        'lblClientName
        '
        Me.lblClientName.AutoSize = True
        Me.lblClientName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClientName.ForeColor = System.Drawing.Color.Navy
        Me.lblClientName.Location = New System.Drawing.Point(97, 20)
        Me.lblClientName.Name = "lblClientName"
        Me.lblClientName.Size = New System.Drawing.Size(0, 13)
        Me.lblClientName.TabIndex = 0
        '
        'FrmFees
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1076, 762)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.CmdSaveAll)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.GrpFees)
        Me.Controls.Add(Me.GrpSelect)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFees"
        Me.Text = "Fees"
        Me.GrpSelect.ResumeLayout(False)
        Me.GrpSelect.PerformLayout()
        Me.GrpFees.ResumeLayout(False)
        Me.TabMgt.ResumeLayout(False)
        Me.TabPageMgt.ResumeLayout(False)
        Me.TabPageMgt.PerformLayout()
        Me.GrpMgtFees.ResumeLayout(False)
        Me.GrpMgtFees.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvMgtRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabExceptions.ResumeLayout(False)
        Me.TabExceptionGroup.ResumeLayout(False)
        CType(Me.dgvMgtExcGrp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabException.ResumeLayout(False)
        CType(Me.dgvMgtExcIns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageCustody.ResumeLayout(False)
        Me.TabPageCustody.PerformLayout()
        Me.GrpCF.ResumeLayout(False)
        Me.GrpCF.PerformLayout()
        Me.TabCFCustody.ResumeLayout(False)
        Me.TabCFRanges.ResumeLayout(False)
        Me.TabCFRanges.PerformLayout()
        CType(Me.dgvCFRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCFSafeKeeping.ResumeLayout(False)
        CType(Me.dgvCFSK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCFCustodyExceptions.ResumeLayout(False)
        Me.TabCFExceptionsGroup.ResumeLayout(False)
        CType(Me.dgvCFExcGrp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCFExceptions.ResumeLayout(False)
        CType(Me.dgvCFExcIns, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageTransactions.ResumeLayout(False)
        Me.TabPageTransactions.PerformLayout()
        Me.GrpTF.ResumeLayout(False)
        Me.GrpTF.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabFIGovt.ResumeLayout(False)
        Me.TabTrans.ResumeLayout(False)
        Me.TabTrans.PerformLayout()
        CType(Me.dgvTFRange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTransSK.ResumeLayout(False)
        CType(Me.dgvTFSK, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabTransSKFixed.ResumeLayout(False)
        CType(Me.dgvTFSKFixed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageTransfers.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvTrans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageDocs.ResumeLayout(False)
        Me.GrpFiles.ResumeLayout(False)
        Me.TabPageEAMClients.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvEAMClients, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpFSDetails.ResumeLayout(False)
        Me.GrpFSDetails.PerformLayout()
        Me.ContextMenuStripFiles.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GrpSelect As GroupBox
    Friend WithEvents GrpFees As GroupBox
    Friend WithEvents TabMgt As TabControl
    Friend WithEvents TabPageMgt As TabPage
    Friend WithEvents TabPageCustody As TabPage
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents CboCCY As ComboBox
    Friend WithEvents lblCCY As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TabPageTransactions As TabPage
    Friend WithEvents TabPageTransfers As TabPage
    Friend WithEvents TabPageDocs As TabPage
    Friend WithEvents GrpMgtFees As GroupBox
    Friend WithEvents txtMgtMax As TextBox
    Friend WithEvents txtMgtMin As TextBox
    Friend WithEvents cboMgtType As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents dgvMgtExcGrp As DataGridView
    Friend WithEvents TabExceptions As TabControl
    Friend WithEvents TabExceptionGroup As TabPage
    Friend WithEvents TabException As TabPage
    Friend WithEvents dgvMgtExcIns As DataGridView
    Friend WithEvents CmdSaveAll As Button
    Friend WithEvents txtMgtID As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvTrans As DataGridView
    Friend WithEvents GrpFiles As GroupBox
    Friend WithEvents TreeViewFiles As TreeView
    Friend WithEvents LVFiles As ListView
    Friend WithEvents ImageListFiles As ImageList
    Friend WithEvents ContextMenuStripFiles As ContextMenuStrip
    Friend WithEvents OpenFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents lblRM2 As Label
    Friend WithEvents lblRM1 As Label
    Friend WithEvents lblClientName As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents ChkMgtFees As CheckBox
    Friend WithEvents ChkCF As CheckBox
    Friend WithEvents GrpCF As GroupBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TabCFCustodyExceptions As TabControl
    Friend WithEvents dgvCFSK As DataGridView
    Friend WithEvents Label19 As Label
    Friend WithEvents dgvCFRange As DataGridView
    Friend WithEvents cboCFChargeableCCY As ComboBox
    Friend WithEvents ChkTF As CheckBox
    Friend WithEvents GrpTF As GroupBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TabCFExceptionsGroup As TabPage
    Friend WithEvents TabCFExceptions As TabPage
    Friend WithEvents dgvCFExcGrp As DataGridView
    Friend WithEvents dgvCFExcIns As DataGridView
    Friend WithEvents Label13 As Label
    Friend WithEvents cboCFType As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents lblshortcut As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents cboTFType As ComboBox
    Friend WithEvents dgvTFRange As DataGridView
    Friend WithEvents Label22 As Label
    Friend WithEvents cboMGTChargeableCCY As ComboBox
    Friend WithEvents cboTemplateName As ComboBox
    Friend WithEvents ChkTemplateName As CheckBox
    Friend WithEvents TabFIGovt As TabControl
    Friend WithEvents TabTrans As TabPage
    Friend WithEvents Label2 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents txtCFMax As TextBox
    Friend WithEvents txtCFMin As TextBox
    Friend WithEvents GrpFSDetails As GroupBox
    Friend WithEvents Label20 As Label
    Friend WithEvents cboFSPayFreq As ComboBox
    Friend WithEvents Label18 As Label
    Friend WithEvents DtFSStart As DateTimePicker
    Friend WithEvents DtFSEnd As DateTimePicker
    Friend WithEvents cboFSDestination As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TabCFCustody As TabControl
    Friend WithEvents TabCFRanges As TabPage
    Friend WithEvents TabCFSafeKeeping As TabPage
    Friend WithEvents Label23 As Label
    Friend WithEvents cboCFCCYEqv As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents cboMGTCCYEqv As ComboBox
    Friend WithEvents dgvMgtRange As DataGridView
    Friend WithEvents Label7 As Label
    Friend WithEvents cboTFCCYEqv As ComboBox
    Friend WithEvents TabTransSK As TabPage
    Friend WithEvents dgvTFSK As DataGridView
    Friend WithEvents TabTransSKFixed As TabPage
    Friend WithEvents dgvTFSKFixed As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtTFTicketFee As TextBox
    Friend WithEvents Label24 As Label
    Friend WithEvents cboTFTicketFeeCCY As ComboBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents txtTFMax As TextBox
    Friend WithEvents txtTFMin As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents ChkCoverBrokerage As CheckBox
    Friend WithEvents Label31 As Label
    Friend WithEvents txtTFFOPFee As TextBox
    Friend WithEvents cboTFFOPFeeCCY As ComboBox
    Friend WithEvents Label33 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents txtTFTransFee As TextBox
    Friend WithEvents cboTFTransFeeCCY As ComboBox
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents cboFSIsEAM As ComboBox
    Friend WithEvents TabPageEAMClients As TabPage
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents dgvEAMClients As DataGridView
    Friend WithEvents cboPortfolioRedirect As ComboBox
    Friend WithEvents ChkRedirectFee As CheckBox
    Friend WithEvents Label32 As Label
    Friend WithEvents txtMgtMaintMin As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents txtCFMaintMin As TextBox
End Class
