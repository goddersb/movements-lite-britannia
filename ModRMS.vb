﻿Imports System.Net
Imports System.Xml
Imports System.Reflection
Imports Microsoft.Office
Imports System.IO

Module ModRMS
    Public Const cEndAmt As Double = 999999999
    Public Const cSwiftJobName As String = "Dolfin Payment CreateSwiftFile"
    Public Const cSwiftJobNameRD As String = "Dolfin Payment CreateSwiftFileRD"
    Public Const cSwiftJobNameSwift As String = "Dolfin Payment CreateSwiftFileSwift"
    Public Const cSwiftJobNameBatch As String = "Dolfin Payment CreateSwiftFile BatchReady"

    Public Const cPayDescInPort As Integer = 1
    Public Const cPayDescIn As Integer = 2
    Public Const cPayDescExClientIn As Integer = 3
    Public Const cPayDescExClientOut As Integer = 4
    Public Const cPayDescExOut As Integer = 5
    Public Const cPayDescExIn As Integer = 6
    Public Const cPayDescInOther As Integer = 7
    Public Const cPayDescReceieveFree As Integer = 8
    Public Const cPayDescDeliverFree As Integer = 9
    Public Const cPayDescMutualFund As Integer = 10
    Public Const cPayDescInPortLoan As Integer = 11
    Public Const cPayDescInMaltaLondon As Integer = 12
    Public Const cPayDescInLondonMalta As Integer = 13
    Public Const cPayDescExVolopaOut As Integer = 14
    Public Const cPayDescExReturnFunds As Integer = 15

    Public Const cPayDescInOtherCovChg As Integer = 1
    Public Const cPayDescInOtherComFee As Integer = 2
    Public Const cPayDescInOtherPayFee As Integer = 3
    Public Const cPayDescInOtherComBenOnly As Integer = 4
    Public Const cPayDescInOtherIntTra As Integer = 5
    Public Const cPayDescInOtherComOrdOnly As Integer = 6
    Public Const cPayDescInOtherCovChgOrdOnly As Integer = 7
    Public Const cPayDescInOtherCovChgBenOnly As Integer = 8
    Public Const cPayDescInOtherNoticetoReceive As Integer = 9
    Public Const cPayDescInOtherGlobalCustodyFee As Integer = 10
    Public Const cPayDescInOtherCustodyFee As Integer = 11
    Public Const cPayDescInOtherManagementFee As Integer = 12
    Public Const cPayDescInOtherBankCharges As Integer = 13
    Public Const cPayDescInOtherBankChargesCovChg As Integer = 14
    Public Const cPayDescInOtherFXCommission As Integer = 15
    Public Const cPayDescInOtherBankInterestCreditOrdOnly As Integer = 16
    Public Const cPayDescInOtherBankInterestDebitOrdOnly As Integer = 17
    Public Const cPayDescInOtherBankInterestCreditBenOnly As Integer = 18
    Public Const cPayDescInOtherBankInterestDebitBenOnly As Integer = 19
    Public Const cPayDescInOtherPayServicesFee As Integer = 20
    Public Const cPayDescInOtherFOPFee As Integer = 21
    Public Const cPayDescInOtherFreeText999 As Integer = 22
    Public Const cPayDescInOtherDolfinFee As Integer = 23
    Public Const cPayDescInOtherStatementRequest As Integer = 24
    Public Const cPayDescInOtherFXInternalOrdOnly As Integer = 25
    Public Const cPayDescInOtherFXInternalBenOnly As Integer = 26
    Public Const cPayDescInOtherPayVolopaFee As Integer = 27
    Public Const cPayDescInOtherFreeText199 As Integer = 28
    Public Const cPayDescInOtherCancelPayment192 As Integer = 29
    Public Const cPayDescInOtherPayVolopaAnnualFee As Integer = 30
    Public Const cPayDescInOtherCustodyFeeBenOnly As Integer = 31
    Public Const cPayDescInOtherManagementFeeBenOnly As Integer = 32
    Public Const cPayDescInOtherCustodyFeeClearClientOwedFees As Integer = 33
    Public Const cPayDescInOtherManagementFeeClearClientOwedFees As Integer = 34
    Public Const cPayDescInOtherFXBuy As Integer = 35
    Public Const cPayDescInOtherFXSell As Integer = 36
    Public Const cPayDescInOtherCustodyFeeFX As Integer = 37
    Public Const cPayDescInOtherManagementFeeFX As Integer = 38
    Public Const cPayDescInOtherMoneyMarketBuy As Integer = 39
    Public Const cPayDescInOtherMoneyMarketSell As Integer = 40
    Public Const cPayDescInOtherFreeText599 As Integer = 41
    Public Const cPayDescInOtherCustodyFeeReversal As Integer = 42
    Public Const cPayDescInOtherManagementFeeReversal As Integer = 43
    Public Const cPayDescInOtherAccountClosingCustodyFee As Integer = 44
    Public Const cPayDescInOtherAccountClosingManagementFee As Integer = 45
    Public Const cPayDescInOtherClientMoneySweep As Integer = 46

    Public Const cStatusReady As Integer = 1
    Public Const cStatusPendingAuth As Integer = 2
    Public Const cStatusPendingReview As Integer = 3
    Public Const cStatusCancelled As Integer = 4
    Public Const cStatusSent As Integer = 5
    Public Const cStatusSwiftAck As Integer = 6
    Public Const cStatusSwiftRej As Integer = 7
    Public Const cStatusSwiftErr As Integer = 8
    Public Const cStatusIMSAck As Integer = 9
    Public Const cStatusIMSErr As Integer = 10
    Public Const cStatusAwaitingSwift As Integer = 11
    Public Const cStatusAwaitingIMS As Integer = 12
    Public Const cStatusPaidAck As Integer = 13
    Public Const cStatusCustodianConfirmAcknowledged As Integer = 14
    Public Const cStatusCustodianMatched As Integer = 15
    Public Const cStatusCustodianNotMatched As Integer = 16
    Public Const cStatusCustodianPending As Integer = 17
    Public Const cStatusCustodianRejected As Integer = 18
    Public Const cStatusCustodianCancelled As Integer = 19
    Public Const cStatusCustodianCancelling As Integer = 20
    Public Const cStatusActive As Integer = 21
    Public Const cStatusNewEvent As Integer = 22
    Public Const cStatusExDate As Integer = 23
    Public Const cStatusPayDate As Integer = 24
    Public Const cStatusDolfinDeadlineDate As Integer = 25
    Public Const cStatusCustodianDeadlineDate As Integer = 26
    Public Const cStatusCustodianDeadlineDateMinusOne As Integer = 27
    Public Const cStatusArchive As Integer = 28
    Public Const cStatusPendingCompliance As Integer = 29
    Public Const cStatusComplianceRejected As Integer = 30
    Public Const cStatusAwaitingPayDate As Integer = 31
    Public Const cStatusExpired As Integer = 32
    Public Const cStatusPendingAccountMgt As Integer = 33
    Public Const cStatusPendingComplianceLevel2 As Integer = 34
    Public Const cStatusPaymentError As Integer = 35
    Public Const cStatusAllocatingFunds As Integer = 36
    Public Const cStatusGenerateInvoice As Integer = 37
    Public Const cStatusNoMatch As Integer = 38
    Public Const cStatusFullMatch As Integer = 39
    Public Const cStatusManualMatch As Integer = 40
    Public Const cStatusReviewMatch As Integer = 41
    Public Const cStatusMiniOMSAcknowledged As Integer = 42
    Public Const cStatusAutoReady As Integer = 43
    Public Const cStatusPendingAccountMgtLevel2 As Integer = 44
    Public Const cStatusPricingInstrument As Integer = 45
    Public Const cStatusAboveThreshold As Integer = 46
    Public Const cStatusSWIFTNoIMSMatch As Integer = 47
    Public Const cStatusSWIFTIMSMatched As Integer = 48
    Public Const cStatusIMSNoSWIFTMatch As Integer = 49
    Public Const cStatusResendToIMS As Integer = 50
    Public Const cStatusPendingPaymentsTeam As Integer = 51
    Public Const cStatusPaymentsTeamRejected As Integer = 52
    Public Const cStatusBatchReady As Integer = 53

    Public Const cAuditStatusSuccess As Integer = 1
    Public Const cAuditStatusBlocked As Integer = 2
    Public Const cAuditStatusError As Integer = 3
    Public Const cAuditStatusWarning As Integer = 4

    Public Const cAuditGroupNameViewMovements As Integer = 1
    Public Const cAuditGroupNameCashMovements As Integer = 2
    Public Const cAuditGroupNameCashMovementsFiles As Integer = 3
    Public Const cAuditGroupNameCutOff As Integer = 4
    Public Const cAuditGroupNameTransferFees As Integer = 5
    Public Const cAuditGroupNameTemplates As Integer = 6
    Public Const cAuditGroupNameBalances As Integer = 7
    Public Const cAuditGroupNameUsers As Integer = 8
    Public Const cAuditGroupNameAudit As Integer = 9
    Public Const cAuditGroupNameOmnibusSwitch As Integer = 10
    Public Const cAuditGroupNameSwiftCodes As Integer = 11
    Public Const cAuditGroupNameSwiftCodesIntermediary As Integer = 12
    Public Const cAuditGroupNameSystemSettings As Integer = 13
    Public Const cAuditGroupNameEmail As Integer = 14
    Public Const cAuditGroupNameReceiveDeliver As Integer = 15
    Public Const cAuditGroupNameViewAllSwifts As Integer = 16
    Public Const cAuditGroupNameReadSwifts As Integer = 17
    Public Const cAuditGroupNameConfirmReceipts As Integer = 18
    Public Const cAuditGroupNameFees As Integer = 19
    Public Const cAuditGroupNameCorpAction As Integer = 20
    Public Const cAuditGroupNamePaymentRequest As Integer = 21
    Public Const cAuditGroupNameRiskAssessment As Integer = 22
    Public Const cAuditGroupNameConnectivity As Integer = 23
    Public Const cAuditGroupNameReconciliation As Integer = 24
    Public Const cAuditGroupNameTreasuryBalancing As Integer = 25
    Public Const cAuditGroupNameBatchProcessing As Integer = 26
    Public Const cAuditGroupNameVolopa As Integer = 27
    Public Const cAuditGroupNameComplainceWatchList As Integer = 28
    Public Const cAuditGroupNameFOPPriceUpdate As Integer = 29
    Public Const cAuditGroupNameEditSwift As Integer = 30
    Public Const cAuditGroupNameCASSAccounts As Integer = 31
    Public Const cAuditGroupNameClientEmailAddresses As Integer = 32
    Public Const cAuditGroupNameClientRptCsvConfig As Integer = 33
    Public Const cAuditGroupNameClientStatementGenerator As Integer = 34
    Public Const cAuditGroupNameClientPDFReports As Integer = 35
    Public Const cAuditGroupNameBGMRec As Integer = 36
    Public Const cAuditGroupNameClientSendQtr As Integer = 37
    Public Const cAuditGroupNameWeeklyTradeFiles As Integer = 38
    Public Const cAuditGroupNameRestrictionList As Integer = 39
    Public Const cAuditGroupNameGPP As Integer = 40

    Public Const cUserLogin As String = "U_LoginPayments"
    Public Const cUserPayments As String = "U_UsersPayments"
    Public Const cUserAuthorise As String = "U_AuthorisePayments"
    Public Const cUserAuthoriseMyPayments As String = "U_AuthoriseMyPayments"
    Public Const cUserAuthorise3rdPartyPayments As String = "U_Authorise3rdPartyPayments"
    Public Const cUserEdit As String = "U_EditPayments"
    Public Const cUserDelete As String = "U_DeletePayments"
    Public Const cUserSubmit As String = "U_SubmitPayments"
    Public Const cUserCutoff As String = "U_AuthoriseCutoffTimesPayments"
    Public Const cUserTransferFees As String = "U_TransferFees"
    Public Const cUserAudit As String = "U_AuditLog"
    Public Const cUserOmnibus As String = "U_OmnibusSwitch"
    Public Const cUserOmnibusDeposits As String = "U_OmnibusSwitchTermDeposits"
    Public Const cUserTemplates As String = "U_Templates"
    Public Const cUserBalances As String = "U_Balances"
    Public Const cUserSwift As String = "U_Swift"
    Public Const cUserSwiftIntermediary As String = "U_SwiftIntermediary"
    Public Const cUserSettings As String = "U_SystemSettings"
    Public Const cUserRD As String = "U_ReceiveDeliver"
    Public Const cUserAboveThreshold As String = "U_AuthoriseAboveThreshold"
    Public Const cUserAutoSwift As String = "U_AutoSwift"
    Public Const cUserSwiftRead As String = "U_SwiftRead"
    Public Const cUserConfirmReceipts As String = "U_ConfirmReceipts"
    Public Const cUserFees As String = "U_Fees"
    Public Const cUserCA As String = "U_CorpAction"
    Public Const cUserAuthoriseAccountMgt As String = "U_AuthoriseAccountMgt"
    Public Const cUserAuthoriseAccountMgtLevel2 As String = "U_AuthoriseAccountMgtLevel2"
    Public Const cUserAuthoriseCompliance As String = "U_AuthoriseCompliance"
    Public Const cUserAuthoriseComplianceLevel2 As String = "U_AuthoriseComplianceLevel2"
    Public Const cUserAuthoriseFeeSchedules As String = "U_AuthoriseFeeSchedules"
    Public Const cUserRA As String = "U_RiskAssessment"
    Public Const cUserC As String = "U_Comments"
    Public Const cUserAC As String = "U_AuthoriseComments"
    Public Const cUserPG As String = "U_PasswordGenerator"
    Public Const cUserFeesPaid As String = "U_FeesPaid"
    Public Const cUserAuthoriseFeesPaid As String = "U_AuthoriseFeesPaid"
    Public Const cUserPayFeesPaid As String = "U_PayFeesPaid"
    Public Const cUserTreasuryMgt As String = "U_TreasuryMgt"
    Public Const cUserRec As String = "U_Rec"
    Public Const cUserSwiftAccStatement As String = "U_SwiftAccStatement"
    Public Const cUserBatchProcessing As String = "U_BatchProcessing"
    Public Const cUserComplianceWatchList As String = "U_ComplianceWatchlist"
    Public Const cUserCASS As String = "U_CASSAccounts"
    Public Const cUserClientEmailAddresses As String = "U_ClientEmail"
    Public Const cUserClientStatementGenerator As String = "U_ClientBalCheck"
    Public Const cUserClientSendQtr As String = "U_ClientSendQtr"
    Public Const cUserAuthorisePaymentsTeam As String = "U_AuthorisePaymentsTeam"
    Public Const cUserBGMRec As String = "U_BGMRec"
    Public Const cUserGPP As String = "U_GPP"

    Public Const cSwiftFileId As String = "{108:"
    Public Const cSwiftFileId2 As String = ":21:"
    Public Const cSwiftFileId3 As String = ":61:"
    Public Const cSwiftFileId4 As String = ":20C::RELA//"
    Public Const cSwiftFileAck As String = "{451:0}"
    Public Const cSwiftFileNoAck As String = "{451:1}"
    Public Const cSwiftFileError As String = "{405:"
    Public Const cSwiftFileType As String = "{2:"
    Public Const cSwiftStatusCode As String = ":25D::"
    Public Const cSwiftReasonCode As String = ":24B::"

    Public ClsIMS As New ClsIMSData
    Public ClsPay As New ClsPayment
    Public ClsPayAuth As New ClsPayment
    Public ClsPayTreasury As New ClsPayment
    Public ClsRD As New ClsReceiveDeliver
    Public ClsFS As New ClsFeeSchedule
    Public ClsMgtFees As New ClsMgtFee
    Public ClsCF As New ClsCustodyFee
    Public ClsTF As New ClsTransactionFee
    Public ClsPR As New ClsPaymentRequest
    Public ClsCA As New ClsCorpAction
    Public ClsSearch As New ClsC6Search
    Public ClsGV As New ClsGridView
    Public ClsFP As New ClsFeesPaid
    Public ClsBP As New ClsBatchProcessing
    Public ClsCRM As New ClsConfirmReceipt
    Public ClsEmail As New ClsEmailExchange

    Public NewPaymentForm As FrmPayment
    Public NewViewPay As FrmViewPayments
    Public NewCutoff As FrmCutOff
    Public NewTF As FrmTransferFees
    Public NewAbout As FrmAbout
    Public NewTA As FrmTreasuryAllocation
    Public NewTATD As FrmTreasuryAllocationTermDeposits
    Public NewUsers As FrmUsers
    Public NewUsersLogin As FrmUsersPassword
    Public NewAudit As FrmAuditlog
    Public NewTemplate As FrmTemplate
    Public NewBal As FrmBalances
    Public NewOpt As FrmOptions
    Public NewSC As FrmSwiftCodes
    Public NewSIC As FrmSwiftIntermediaryCodes
    Public NewSS As FrmSystemSettings
    Public RDForm As FrmReceiveDeliver
    Public ASForm As FrmAllSwifts
    Public NewSwiftRead As FrmSwiftRead
    Public NewConfirmReceipt As FrmConfirmReceipt
    Public NewFeeSchedule As FrmFeeSchedules
    Public NewCA As FrmCorpAction
    Public NewCAT As FrmCorpActionTerms
    Public NewRA As FrmRASettings
    Public NewPG As FrmPasswordGenerator
    Public NewC As FrmCashTranComment
    Public NewFeesPaid As FrmFeesPaid
    Public NewTresMgt As FrmTreasuryManagement
    Public NewRec As FrmRec
    Public NewAccStatement As FrmSwiftStatements
    Public NewPaymentRequest As FrmPaymentRequest
    Public NewBP As FrmBatchProcessing
    Public NewBPTemplate As FrmBatchProcessingTemplate
    Public NewCWL As FrmClientDeactivation
    Public NewCASS As FrmCASS
    Public NewSendControl As FrmSendControl
    Public NewCEA As FrmClientEmailAddresses
    Public NewCSG As FrmClientStatementGenerator
    Public NewBGMR As FrmBritanniaGlobalMarketsRec
    Public NewCRM As FrmConfirmReceiptsOptions
    Public NewGPP As FrmGppMappingCodes


    Public Function HTMLDecode(ByVal sText As String) As String
        sText = Replace(sText, "&quot;", Chr(34))
        sText = Replace(sText, "&lt;", Chr(60))
        sText = Replace(sText, "&gt;", Chr(62))
        sText = Replace(sText, "&amp;", Chr(38))
        sText = Replace(sText, "&nbsp;", Chr(32))

        Dim regEx2 As New System.Text.RegularExpressions.Regex("£$&#(\d+);<>€", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        Dim matches As System.Text.RegularExpressions.MatchCollection = regEx2.Matches(sText)

        'Iterate over matches
        For Each match1 As System.Text.RegularExpressions.Match In matches
            'For each unicode match, replace the whole match, with the ChrW of the digits.

            sText = Replace(sText, match1.Value, ChrW(match1.Groups(0).Index))
        Next

        HTMLDecode = sText
    End Function

    Public Sub DoubleBuffered(ByVal dgv As DataGridView, ByVal setting As Boolean)
        Dim dgvType As Type = dgv.[GetType]()
        Dim pi As PropertyInfo = dgvType.GetProperty(“DoubleBuffered”, BindingFlags.Instance Or BindingFlags.NonPublic)
        pi.SetValue(dgv, setting, Nothing)
    End Sub

    Public Function ConvertTimeZone(ByVal varTimeZone As String) As DateTimeOffset
        Dim varDateTime As DateTime = CDate(DateTime.UtcNow)
        Dim tz As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(varTimeZone)
        Dim utcOffset As New DateTimeOffset(varDateTime, TimeSpan.Zero)
        ConvertTimeZone = utcOffset.ToOffset(tz.GetUtcOffset(utcOffset))
    End Function

    Public Function BasicCalculator(ByVal varExpression As String) As Double
        Dim varCalc As String = "", varLeftExpr As String = "", varRightExpr As String = ""

        BasicCalculator = 0
        Try
            If Not IsNumeric(varExpression) Then
                varExpression = Replace(Replace(Replace(Replace(varExpression, "=", ""), "(", ""), ")", ""), "\", "/")
                If InStr(1, varExpression, "+", vbTextCompare) <> 0 Then
                    Dim varPos As Integer = InStr(1, varExpression, "+", vbTextCompare)
                    varCalc = Strings.Mid(varExpression, varPos, 1)
                    varLeftExpr = Strings.Left(varExpression, varPos - 1)
                    varRightExpr = Strings.Right(varExpression, Len(varExpression) - varPos)
                    If IsNumeric(varLeftExpr) And IsNumeric(varRightExpr) Then
                        BasicCalculator = CDbl(varLeftExpr) + CDbl(varRightExpr)
                    End If
                ElseIf InStr(1, varExpression, "-", vbTextCompare) <> 0 Then
                    Dim varPos As Integer = InStr(1, varExpression, "-", vbTextCompare)
                    varCalc = Strings.Mid(varExpression, varPos, 1)
                    varLeftExpr = Strings.Left(varExpression, varPos - 1)
                    varRightExpr = Strings.Right(varExpression, Len(varExpression) - varPos)
                    If IsNumeric(varLeftExpr) And IsNumeric(varRightExpr) Then
                        BasicCalculator = CDbl(varLeftExpr) - CDbl(varRightExpr)
                    End If
                ElseIf InStr(1, varExpression, "*", vbTextCompare) <> 0 Then
                    Dim varPos As Integer = InStr(1, varExpression, "*", vbTextCompare)
                    varCalc = Strings.Mid(varExpression, varPos, 1)
                    varLeftExpr = Strings.Left(varExpression, varPos - 1)
                    varRightExpr = Strings.Right(varExpression, Len(varExpression) - varPos)
                    If IsNumeric(varLeftExpr) And IsNumeric(varRightExpr) Then
                        BasicCalculator = CDbl(varLeftExpr) * CDbl(varRightExpr)
                    End If
                ElseIf InStr(1, varExpression, "/", vbTextCompare) <> 0 Then
                    Dim varPos As Integer = InStr(1, varExpression, "/", vbTextCompare)
                    varCalc = Strings.Mid(varExpression, varPos, 1)
                    varLeftExpr = Strings.Left(varExpression, varPos - 1)
                    varRightExpr = Strings.Right(varExpression, Len(varExpression) - varPos)
                    If IsNumeric(varLeftExpr) And IsNumeric(varRightExpr) Then
                        BasicCalculator = CDbl(varLeftExpr) / CDbl(varRightExpr)
                    End If
                End If
            Else
                BasicCalculator = CDbl(varExpression)
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Sub FormatGridStatus(ByRef dgv As DataGridView, ByRef varRow As Integer, Optional ByRef varCol As Integer = 1)
        Try
            If Not IsDBNull(dgv.Rows(varRow).Cells(varCol).Value) Then
                If dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusReady) Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusActive) Or
                    InStr(1, dgv.Rows(varRow).Cells(varCol).Value, "Match on") <> 0 Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAutoReady) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusBatchReady) Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSWIFTIMSMatched) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGreen
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingAuth) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianPending) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAwaitingPayDate) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPricingInstrument) Or
                    InStr(1, dgv.Rows(varRow).Cells(varCol).Value, "Manual Match") <> 0 Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGoldenrod
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingAccountMgt) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingAccountMgtLevel2) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingCompliance) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingComplianceLevel2) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusIMSNoSWIFTMatch) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Goldenrod
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPendingReview) Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusReviewMatch) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkViolet
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCancelled) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianCancelled) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Black
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSent) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusExDate) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPayDate) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkBlue
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSwiftAck) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.MediumSeaGreen
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusIMSAck) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianConfirmAcknowledged) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkSeaGreen
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSwiftErr) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSwiftRej) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusIMSErr) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianNotMatched) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianRejected) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusDolfinDeadlineDate) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianDeadlineDate) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianDeadlineDateMinusOne) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusComplianceRejected) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPaymentsTeamRejected) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusNewEvent) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusGenerateInvoice) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusExpired) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusSWIFTNoIMSMatch) Or
                    InStr(1, dgv.Rows(varRow).Cells(varCol).Value, "No Match ") <> 0 Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkRed
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAwaitingIMS) Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAwaitingSwift) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Chocolate
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusPaidAck) Or
                    dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusCustodianMatched) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGray
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusArchive) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Gray
                ElseIf dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAllocatingFunds) Or dgv.Rows(varRow).Cells(varCol).Value = ClsIMS.GetStatusType(cStatusAboveThreshold) Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkKhaki
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub FormatGridPaymentBalance(ByRef dgv As DataGridView, ByRef varRow As Integer, ByRef varCol As Integer)
        If Not IsDBNull(dgv.Rows(varRow).Cells(varCol).Value) Then
            If IsNumeric(dgv.Rows(varRow).Cells(varCol).Value) Then
                If CDbl(dgv.Rows(varRow).Cells(varCol).Value) >= 0 Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGreen
                ElseIf Not IsDBNull(dgv.Rows(varRow).Cells("PortfolioCode").Value) And Not IsDBNull(dgv.Rows(varRow).Cells("CCYCode").Value) Then
                    'If ClsIMS.GetPortfolioCCYBalance(dgv.Rows(varRow).Cells("PortfolioCode").Value, dgv.Rows(varRow).Cells("CCYCode").Value) <= 0 Then
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkRed
                    'Else
                    'dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkGoldenrod
                    'End If
                Else
                    dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.DarkRed
                End If
            Else
                dgv.Rows(varRow).Cells(varCol).Style.ForeColor = Color.Black
            End If
        End If
    End Sub

    Public Sub FormatGridPaymentBalanceColour(ByRef dgv As DataGridView, ByRef varBalanceColumnName As String)
        For varRow = 0 To dgv.Rows.Count - 1
            If dgv.Rows(varRow).Cells("Status").Value = ClsIMS.GetStatusType(cStatusReady) Or
              dgv.Rows(varRow).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingAuth) Or
                dgv.Rows(varRow).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingReview) Then
                If Not IsDBNull(dgv.Rows(varRow).Cells(varBalanceColumnName).Value) Then
                    If IsNumeric(dgv.Rows(varRow).Cells(varBalanceColumnName).Value) Then
                        If CDbl(dgv.Rows(varRow).Cells(varBalanceColumnName).Value) >= 0 Then
                            dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.DarkGreen
                            dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.Empty
                        ElseIf Not IsDBNull(dgv.Rows(varRow).Cells("PortfolioCode").Value) And Not IsDBNull(dgv.Rows(varRow).Cells("CCYCode").Value) Then
                            If ClsIMS.GetPortfolioCCYBalance(dgv.Rows(varRow).Cells("PortfolioCode").Value, dgv.Rows(varRow).Cells("CCYCode").Value) <= 0 Then
                                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.White
                                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.DarkRed
                            Else
                                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.DarkRed
                                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.Empty
                            End If
                        Else
                            dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.Black
                            dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.Empty
                        End If
                    Else
                        dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.Black
                        dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.Empty
                    End If
                End If
            Else
                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.ForeColor = Color.Black
                dgv.Rows(varRow).Cells(varBalanceColumnName).Style.BackColor = Color.Empty
            End If
        Next
    End Sub

    Public Function GetValueFromBloomberg(ByVal Ticker As String, ByVal Field As String, Optional ByVal ReturnError As Boolean = False) As String
        Dim url As String = "http://10.1.15.96:35111/?columns=" & Field & "&tickers=" & Replace(Ticker, " ", "%20")
        Dim varwebclient As New WebClient()
        Dim JSONString As String = ""
        Dim varResult As String = ""

        JSONString = varwebclient.DownloadString(url)

        If InStr(1, JSONString, "error", vbTextCompare) <> 0 Or JSONString = "" Then
            If InStr(1, JSONString, "error", vbTextCompare) <> 0 Or InStr(1, JSONString, Field, vbTextCompare) = 0 Then
                If ReturnError Then
                    varResult = JSONString
                Else
                    varResult = ""
                End If
            ElseIf JSONString <> "" Then
                varResult = Right(JSONString, Len(JSONString) - InStr(1, JSONString, Field, vbTextCompare) - Len(Field))
                varResult = Replace(varResult, ":", "")
                varResult = Replace(varResult, """", "")
                varResult = Replace(varResult, "}", "")
                varResult = Replace(varResult, "{", "")
                varResult = Replace(varResult, Field, "")
            End If
        ElseIf JSONString <> "" Then
            varResult = Right(JSONString, Len(JSONString) - InStr(1, JSONString, Field, vbTextCompare) - Len(Field))
            varResult = Replace(varResult, ":", "")
            varResult = Replace(varResult, """", "")
            varResult = Replace(varResult, "}", "")
            varResult = Replace(varResult, "{", "")
            varResult = Replace(varResult, Field, "")
        End If

        varResult = Replace(varResult, Chr(10), "")
        varResult = Replace(varResult, Chr(13), "")
        GetValueFromBloomberg = Trim(varResult)
    End Function

    Public Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub

    Public Function GetSwiftDetails(ByVal varSwiftCode As String) As Dictionary(Of String, String)
        Dim url As String = "https://api.bank.codes/swift/?format=xml&api_key=6eeb5030334cd714cada24a7815fb3d1&swift=" & varSwiftCode
        Dim xDoc As New XmlDocument()
        Dim varwebclient As New WebClient()
        Dim Lst As New Dictionary(Of String, String)
        Dim xmldoc As String = ""
        Dim elemList As XmlNodeList

        GetSwiftDetails = Nothing
        Try
            xmldoc = varwebclient.DownloadString(url)
            xDoc.LoadXml(xmldoc)

            elemList = xDoc.GetElementsByTagName("valid")
            If elemList(0).InnerXml = "true" Then
                elemList = xDoc.GetElementsByTagName("swift")
                Lst.Add("swift", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("bank")
                Lst.Add("bank", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("city")
                Lst.Add("city", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("branch")
                Lst.Add("branch", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("address")
                Lst.Add("address", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("postcode")
                Lst.Add("postcode", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("country")
                Lst.Add("country", elemList(0).InnerXml)
                elemList = xDoc.GetElementsByTagName("countrycode")
                Lst.Add("countrycode", elemList(0).InnerXml)
                GetSwiftDetails = Lst
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function CheckBankNameDetails(ByRef varSortCode As String, ByRef varAccountNo As String, ByRef varBankName As String) As Boolean
        CheckBankNameDetails = True
        If IsNumeric(varSortCode) And Len(varSortCode) = 6 And Len(varAccountNo) = 8 Then
            varBankName = GetBankNameDetails(varSortCode, varAccountNo)
            If varBankName <> "" Then
                If varBankName = "exceeded" Then
                    varBankName = "You have exceeded the API number of checks allowed (2 in any 24 hours period). Please check manually via https://www.sortcodes.co.uk/"
                    CheckBankNameDetails = False
                End If
            Else
                varBankName = "This sortcode and account number provided have not proved to be a valid bank account within the UK"
                CheckBankNameDetails = False
            End If
        Else
            varBankName = "Please enter a valid UK sortcode and UK bank account number"
            CheckBankNameDetails = False
        End If
    End Function

    Public Function GetBankNameDetails(ByVal varSortCode As String, ByVal varAccountNo As String) As String
        Dim url As String = "https://www.bankaccountchecker.com/listener.php?key=60f239fa2d99544b9f7260c742419338&password=Bluel10n%&output=xml&type=uk&sortcode=" & varSortCode & "&bankaccount=" & varAccountNo
        Dim xDoc As New XmlDocument()
        Dim varwebclient As New WebClient()
        Dim Lst As New Dictionary(Of String, String)
        Dim xmldoc As String = ""
        Dim elemList As XmlNodeList

        GetBankNameDetails = ""
        Try
            xmldoc = varwebclient.DownloadString(url)
            xDoc.LoadXml(xmldoc)

            elemList = xDoc.GetElementsByTagName("resultDescription")
            If InStr(1, elemList(0).InnerXml, "Sortcode and Bank Account are valid", vbTextCompare) <> 0 Then
                elemList = xDoc.GetElementsByTagName("institution")
                GetBankNameDetails = elemList(0).InnerXml
            ElseIf InStr(1, elemList(0).InnerXml, "You have exceeded the number of checks allowed", vbTextCompare) <> 0 Then
                GetBankNameDetails = "exceeded"
            End If
        Catch ex As Exception

        End Try
    End Function


    Public Function ValidSwift(ByVal varSwift As String) As Boolean
        ValidSwift = True
        If Len(varSwift) <> 8 And Len(varSwift) <> 11 Then
            ValidSwift = False
        End If
        If Not Char.IsLetter(Strings.Left(varSwift, 6)) Then
            ValidSwift = False
        End If
        If InStr(1, varSwift, " ", vbTextCompare) <> 0 Then
            ValidSwift = False
        End If
    End Function

    Public Function ValidIBAN(ByVal varIBAN As String) As Boolean
        '1. check string length per country
        '2. move 4 initial characters to end of string
        '3. Replace each letter in the string with two digits, thereby expanding the string, where A = 10, B = 11, ..., Z = 35
        '4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97
        '5. If the remainder is 1, the check digit test is passed and the IBAN might be valid.

        Try
            ValidIBAN = True
            Dim varFirstLetter As String = UCase(Strings.Left(varIBAN, 1))
            Dim varSecondLetter As String = UCase(Strings.Mid(varIBAN, 2, 1))
            Dim JoinLetter As String = varFirstLetter & varSecondLetter

            If InStr(1, varIBAN, " ", vbTextCompare) <> 0 Then
                ValidIBAN = False
            ElseIf Len(varIBAN) < 15 Or Len(varIBAN) > 32 Then
                ValidIBAN = False
            ElseIf Strings.Asc(varFirstLetter) < 65 Or Strings.Asc(varFirstLetter) > 90 Then
                ValidIBAN = False
            ElseIf Strings.Asc(varSecondLetter) < 65 Or Strings.Asc(varSecondLetter) > 90 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AX" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AL" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AD" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AT" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BE" And Len(varIBAN) <> 16 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BA" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BG" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "HR" And Len(varIBAN) <> 21 Then
                ValidIBAN = False
            ElseIf JoinLetter = "CY" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "CZ" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "DK" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "EE" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "FO" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "FI" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "FR" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "DE" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GI" And Len(varIBAN) <> 23 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GR" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GL" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "HU" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "IS" And Len(varIBAN) <> 26 Then
                ValidIBAN = False
            ElseIf JoinLetter = "IE" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "IT" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "XK" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LV" And Len(varIBAN) <> 21 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LI" And Len(varIBAN) <> 21 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LT" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LU" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MK" And Len(varIBAN) <> 19 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MT" And Len(varIBAN) <> 31 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MD" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MC" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "ME" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "NL" And Len(varIBAN) <> 18 Then
                ValidIBAN = False
            ElseIf JoinLetter = "NO" And Len(varIBAN) <> 15 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PL" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PT" And Len(varIBAN) <> 25 Then
                ValidIBAN = False
            ElseIf JoinLetter = "RO" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "SM" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "RS" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "SK" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "SI" And Len(varIBAN) <> 19 Then
                ValidIBAN = False
            ElseIf JoinLetter = "ES" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "SE" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "CH" And Len(varIBAN) <> 21 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GB" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AZ" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BH" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BR" And Len(varIBAN) <> 29 Then
                ValidIBAN = False
            ElseIf JoinLetter = "VG" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "CR" And Len(varIBAN) <> 21 Then
                ValidIBAN = False
            ElseIf JoinLetter = "DO" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GF" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PF" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "TF" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GE" And Len(varIBAN) <> 22 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GP" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "GU" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "IL" And Len(varIBAN) <> 23 Then
                ValidIBAN = False
            ElseIf JoinLetter = "JO" And Len(varIBAN) <> 30 Then
                ValidIBAN = False
            ElseIf JoinLetter = "KZ" And Len(varIBAN) <> 20 Then
                ValidIBAN = False
            ElseIf JoinLetter = "KW" And Len(varIBAN) <> 30 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LB" And Len(varIBAN) <> 28 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MQ" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MR" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MU" And Len(varIBAN) <> 30 Then
                ValidIBAN = False
            ElseIf JoinLetter = "YT" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "NC" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PK" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PS" And Len(varIBAN) <> 29 Then
                ValidIBAN = False
            ElseIf JoinLetter = "QA" And Len(varIBAN) <> 29 Then
                ValidIBAN = False
            ElseIf JoinLetter = "RE" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "BL" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "LC" And Len(varIBAN) <> 32 Then
                ValidIBAN = False
            ElseIf JoinLetter = "MF" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "PM" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf JoinLetter = "ST" And Len(varIBAN) <> 25 Then
                ValidIBAN = False
            ElseIf JoinLetter = "SA" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "TL" And Len(varIBAN) <> 23 Then
                ValidIBAN = False
            ElseIf JoinLetter = "TN" And Len(varIBAN) <> 24 Then
                ValidIBAN = False
            ElseIf JoinLetter = "TR" And Len(varIBAN) <> 26 Then
                ValidIBAN = False
            ElseIf JoinLetter = "UA" And Len(varIBAN) <> 29 Then
                ValidIBAN = False
            ElseIf JoinLetter = "AE" And Len(varIBAN) <> 23 Then
                ValidIBAN = False
            ElseIf JoinLetter = "WF" And Len(varIBAN) <> 27 Then
                ValidIBAN = False
            ElseIf Not ValidIBANConvertAndConfirm(varIBAN) Then
                ValidIBAN = False
            End If
        Catch ex As Exception
            ValidIBAN = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - ValidIBAN")
        End Try
    End Function

    Private Function ValidIBANConvertAndConfirm(ByVal varIBAN As String) As Boolean
        Dim varIBANR As String
        Dim ReplaceChr As String
        Dim ReplaceBy As String
        Dim LeftOver As Long
        Dim digit As Integer

        ValidIBANConvertAndConfirm = False

        varIBAN = Replace(UCase(varIBAN), " ", "")
        varIBANR = Strings.Right(varIBAN, Len(varIBAN) - 4) & Strings.Left(varIBAN, 4)
        For Nr = 10 To 35
            ReplaceChr = Chr(Nr + 55)
            ReplaceBy = Trim(Str(Nr))
            varIBANR = Replace(varIBANR, ReplaceChr, ReplaceBy)
        Next Nr

        'Loop through the IBAN, as it is too long to calculate at one go
        LeftOver = 0
        For i = 1 To Len(varIBANR)
            digit = CInt(Mid(varIBANR, i, 1))
            LeftOver = (10 * LeftOver + digit) Mod 97
        Next

        If LeftOver = 1 Then
            ValidIBANConvertAndConfirm = True
        End If
    End Function

    Public Sub LabelTextAndPoint(lbl As Label, varText As String, x As Integer, y As Integer)
        lbl.Text = varText
        lbl.Location = New Point(x, y)
    End Sub

    Public Sub ShellExecute(ByVal File As String)
        Dim myProcess As New Process
        Try
            myProcess.StartInfo.FileName = File
            myProcess.StartInfo.UseShellExecute = True
            myProcess.StartInfo.RedirectStandardOutput = False
            myProcess.Start()
            myProcess.Dispose()
        Catch ex As Exception
            MsgBox("Unable to open file")
            myProcess = Nothing
        End Try
    End Sub

    Public Sub PopulateBatchTable(ByRef TblBP As DataTable)
        AddDataColumn(TblBP, "P_ID", "System.Int32")
        AddDataColumn(TblBP, "P_PaymentType", "System.String")
        AddDataColumn(TblBP, "P_PortfolioCode", "System.Int32")
        AddDataColumn(TblBP, "P_Portfolio", "System.String")
        AddDataColumn(TblBP, "P_TransDate", "System.DateTime")
        AddDataColumn(TblBP, "P_SettleDate", "System.DateTime")
        AddDataColumn(TblBP, "P_Amount", "System.Double")
        AddDataColumn(TblBP, "P_CCYCode", "System.Int32")
        AddDataColumn(TblBP, "P_CCY", "System.String")
        AddDataColumn(TblBP, "P_OrderBrokerCode", "System.Int32")
        AddDataColumn(TblBP, "P_OrderAccountCode", "System.Int32")
        AddDataColumn(TblBP, "P_OrderAccount", "System.String")
        AddDataColumn(TblBP, "P_OrderName", "System.String")
        AddDataColumn(TblBP, "P_OrderSwift", "System.String")
        AddDataColumn(TblBP, "P_OrderIBAN", "System.String")
        AddDataColumn(TblBP, "P_OrderVO_ID", "System.Int32")
        AddDataColumn(TblBP, "P_OrderOther", "System.String")
        AddDataColumn(TblBP, "P_BenTypeID", "System.Int32")
        AddDataColumn(TblBP, "P_BenType", "System.String")
        AddDataColumn(TblBP, "P_BenName", "System.String")
        AddDataColumn(TblBP, "P_BenFirstname", "System.String")
        AddDataColumn(TblBP, "P_BenMiddlename", "System.String")
        AddDataColumn(TblBP, "P_BenSurname", "System.String")
        AddDataColumn(TblBP, "P_BenCountryID", "System.Int32")
        AddDataColumn(TblBP, "P_BenCountry", "System.String")
        AddDataColumn(TblBP, "P_BenSwift", "System.String")
        AddDataColumn(TblBP, "P_BenIBAN", "System.String")
        AddDataColumn(TblBP, "P_BenBIK", "System.String")
        AddDataColumn(TblBP, "P_BenINN", "System.String")
        AddDataColumn(TblBP, "P_BenRelationshipID", "System.Int32")
        AddDataColumn(TblBP, "P_BenRelationship", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankName", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankCountryID", "System.Int32")
        AddDataColumn(TblBP, "P_BenSubBankCountry", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankSwift", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankIBAN", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankBIK", "System.String")
        AddDataColumn(TblBP, "P_BenSubBankINN", "System.String")
        AddDataColumn(TblBP, "P_RequestTypeID", "System.Int32")
        AddDataColumn(TblBP, "P_RequestType", "System.String")
        AddDataColumn(TblBP, "P_RequestRate", "System.Double")
        AddDataColumn(TblBP, "P_RequestFee", "System.Double")
        AddDataColumn(TblBP, "P_RequestFeeCCY", "System.String")
        AddDataColumn(TblBP, "P_RequestKYCFolder", "System.String")
        AddDataColumn(TblBP, "P_RequestCheckedBankName", "System.String")
        AddDataColumn(TblBP, "P_RequestCheckedBankCountryID", "System.Int32")
        AddDataColumn(TblBP, "P_RequestCheckedBankCountry", "System.String")
        AddDataColumn(TblBP, "P_RequestCheckedSanctions", "System.Boolean")
        AddDataColumn(TblBP, "P_RequestCheckedPEP", "System.Boolean")
        AddDataColumn(TblBP, "P_RequestCheckedCDD", "System.Boolean")
        AddDataColumn(TblBP, "P_RequestIssuesDisclosed", "System.String")
        AddDataColumn(TblBP, "P_AddFee", "System.Boolean")
        AddDataColumn(TblBP, "P_Comments", "System.String")
        AddDataColumn(TblBP, "P_Username", "System.String")
    End Sub


    Public Sub AddDataColumn(ByRef Tbl As DataTable, ByRef varName As String, ByRef varType As String)
        Dim newCol As New DataColumn(varName)
        newCol.DataType = System.Type.GetType(varType)
        Tbl.Columns.Add(newCol)
    End Sub

    Public Function IsFormOpen(ByVal frm As Form) As Boolean
        If Application.OpenForms.OfType(Of Form).Contains(frm) Then
            Return True
        Else
            Return False
        End If
    End Function

    'Public Sub DisplayExcelFile()
    '    Dim xlsApp As Microsoft.Office.Interop.Excel.Application = Nothing
    '    Dim xlsWorkBooks As Microsoft.Office.Interop.Excel.Workbooks = Nothing
    '    Dim xlsWB As Microsoft.Office.Interop.Excel.Workbook = Nothing
    '    Dim varRow As Integer
    '    Dim varFilePath As String = "C:\Temp\Volopa\"
    '    Try

    '        xlsApp = New Microsoft.Office.Interop.Excel.Application
    '        xlsWorkBooks = xlsApp.Workbooks
    '        xlsWB = xlsWorkBooks.Open(varFilePath & "VolopaPayments.xlsx")

    '        Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("Select P_Amount,FirstName,Surname,VolopaToken from vwDolfinPaymentReadyIMSVolopa")
    '        Dim varStartRow As Integer = 6

    '        If Not Lst Is Nothing Then
    '            For varRow = 0 To Lst.Count - 1 Step 4
    '                xlsWB.Sheets("Daily").range("B" & varStartRow + (varRow / 4)) = Now().ToString("dd/MM/yyyy")
    '                xlsWB.Sheets("Daily").range("C" & varStartRow + (varRow / 4)) = Lst(varRow)
    '                xlsWB.Sheets("Daily").range("D" & varStartRow + (varRow / 4)) = Lst(varRow + 1)
    '                xlsWB.Sheets("Daily").range("E" & varStartRow + (varRow / 4)) = Lst(varRow + 2)
    '                xlsWB.Sheets("Daily").range("F" & varStartRow + (varRow / 4)) = Lst(varRow + 3)
    '                xlsWB.Sheets("Daily").range("G" & varStartRow + (varRow / 4)) = "Dolfin"
    '            Next
    '            xlsWB.SaveAs(varFilePath & Now.Date.ToString("yyyyMMdd") & "xlsx")
    '        End If

    '    Catch ex As Exception

    '    Finally

    '        xlsWB.Close()
    '        xlsWB = Nothing
    '        xlsApp.Quit()
    '        xlsApp = Nothing

    '    End Try

    'End Sub

    Public Function GetBalance(ByVal varPortfolioBalance As Boolean, ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As Double
        Dim varGetBalance As String = ""
        GetBalance = 0

        If varPortfolioBalance Then
            varGetBalance = ClsIMS.GetSQLItem("select sum(convert(money, isnull(BalanceUnblockedIncPending,0))) from vwDolfinPaymentSelectAccount where b_name1 not like '%Berkeley%' and pf_code = " & varPortfolioCode & " And cr_code = " & varCCYCode)
        Else
            varGetBalance = ClsIMS.GetSQLItem("select sum(convert(money, isnull(BalanceUnblockedIncPending,0))) from vwDolfinPaymentSelectAccount where b_name1 not like '%Berkeley%' and pf_code in (select cupo_pfcode from cuporel where cupo_Custid = (select Cupo_CustID from cuporel where Cupo_PFCode = " & varPortfolioCode & ")) And cr_code = " & varCCYCode)
        End If

        If IsNumeric(varGetBalance) Then
            GetBalance = CDbl(varGetBalance)
        End If
    End Function

    Public Function GetMMInstrumentBalance(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double, ByVal varInstID As Double) As Double
        Dim varGetBalance As String = ""
        GetMMInstrumentBalance = 0
        varGetBalance = ClsIMS.GetSQLItem("select sum(convert(money, isnull(BalanceUnblockedIncPending,0))) from vwDolfinPaymentSelectAccount where t_code = " & varInstID & " and pf_code = " & varPortfolioCode & " and cr_code = " & varCCYCode)

        If IsNumeric(varGetBalance) Then
            GetMMInstrumentBalance = CDbl(varGetBalance)
        End If
    End Function

    Public Sub ColorBalanceTextBox(ByVal BalTxtbox As TextBox)
        If IsNumeric(BalTxtbox.Text) Then
            If CDbl(BalTxtbox.Text) >= 0 Then
                BalTxtbox.ForeColor = Color.FromKnownColor(KnownColor.HotTrack)
            Else
                BalTxtbox.ForeColor = Color.DarkRed
            End If
        End If
    End Sub

    Public Sub ColorRadio(ByVal Rb As RadioButton)
        If Rb.Checked Then
            Rb.ForeColor = Color.DarkGreen
        Else
            Rb.ForeColor = Color.Black
        End If
    End Sub

    Public Sub ColorCheckbox(ByVal Chk As CheckBox)
        If Chk.Checked Then
            Chk.ForeColor = Color.DarkGreen
        Else
            Chk.ForeColor = Color.DarkRed
        End If
    End Sub

    Public Sub GetOlympicVal()
        Dim createstring As String = "http://10.226.10.5:8081/olympic-native/api/CLI"
        Dim request2 As HttpWebRequest = DirectCast(HttpWebRequest.Create(createstring), HttpWebRequest)
        request2.Method = "GET"
        request2.Headers("Authorization") = "10000"
        request2.ContentType = "application/json"
        Dim response2 As WebResponse = request2.GetResponse()
        Dim dataStream2 As Stream = response2.GetResponseStream()
        Dim reader2 As New StreamReader(dataStream2)
        Dim responseFromServer2 As String = reader2.ReadToEnd()
        reader2.Close()
        dataStream2.Close()
        response2.Close()
    End Sub

    Public Sub SendClientReports()
        Dim varResponse As Integer
        varResponse = MsgBox("Are you sure you want to send all of the Client CSV Files?", vbQuestion + vbYesNo)
        If varResponse = vbYes Then
            varResponse = MsgBox("Would you like to send the CSV files direct to the clients?", vbQuestion + vbYesNo)
            If varResponse = vbYes Then
                ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "exec dbo.DolfinPaymentRunAllIMSClientCSV 1")
            Else
                ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "exec dbo.DolfinPaymentRunAllIMSClientCSV 0")
            End If
        End If
    End Sub

    Public Sub ReleaseDocNumbers()
        Dim varResponse As Integer
        varResponse = MsgBox("Are you sure you want to release the IMS doc numbers?", vbQuestion + vbYesNo)
        If varResponse = vbYes Then
            ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "update docnumbers set doc_locks = 0 where isnull(doc_locks,0) = 1")
            varResponse = MsgBox("IMS doc numbers have been released. please check IMS again.", vbInformation)
        End If
    End Sub

    Public Function GetNewTempDirectory(ByVal FilePath As String) As String
        Dim result As String = ""
        GetNewTempDirectory = ""
        Try
            result = Path.GetRandomFileName()

            If Not Directory.Exists(FilePath & result) Then
                Directory.CreateDirectory(FilePath & result)
                GetNewTempDirectory = FilePath & result & "\"
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - GetNewTempDirectory: " & result)
        End Try
    End Function

    Public Sub ConfirmCorrectDirectoryCreated(ByVal TempDirectory As String, ByVal CorrectDirectory As String)
        If Not Directory.Exists(CorrectDirectory) Then
            Directory.Move(TempDirectory, CorrectDirectory)
        End If
    End Sub

    Public Sub MapDrive(ByVal DriveLetter As String, ByVal UNCPath As String, ByVal strUsername As String, ByVal strPassword As String)
        Dim p As New Process()
        p.StartInfo.FileName = "net.exe"
        p.StartInfo.Arguments = " use " & DriveLetter & ": " & UNCPath & " " & strPassword & " /USER:" & strUsername
        p.StartInfo.CreateNoWindow = True
        p.Start()
        p.WaitForExit()
    End Sub

    Public Sub ExportGridToExcelFromForm(ByVal dgv As DataGridView, Optional varMAXColumns As Integer = 0)
        Dim varFullFilePath As String = ""
        Dim varSaveFileDialog As New SaveFileDialog
        varSaveFileDialog.Filter = "Excel Files (*.xlsx)|*.xlsx"
        If varSaveFileDialog.ShowDialog() = DialogResult.OK Then
            varFullFilePath = varSaveFileDialog.FileName

            If varMAXColumns <> 0 Then
                ClsPay.ExportGridToExcel(dgv, varMAXColumns, varFullFilePath)
            Else
                ClsPay.ExportGridToExcel(dgv, , varFullFilePath)
            End If
        End If
    End Sub


    Public Sub tester()
        Dim varThreshold As String = "RMSFGB2LAXX"
        Dim varBusinessName As String = "INGBPLPWXXX"
        Dim varPEP As String = "CUSFEE0091782"
        Dim varPrevSanc As String = "CUSFEE0091781"
        Dim varCurrSanc As String = "A Message goes here."
        Dim varLaw As String = "false"
        Dim varFinReg As String = "david.harper@dashrosolutions.com"

        Dim request As HttpWebRequest = DirectCast(HttpWebRequest.Create("http://51.141.32.246:10010/api/v1/Swift/MT199?clientId=5003"), HttpWebRequest)
        request.Method = "POST"

        Dim postData As String = "{ ""clientSwiftCode"":" & IIf(varBusinessName = "", "null", """" & varBusinessName & """") &
            ",""brokerSwiftCode"":" & IIf(varBusinessName = "", "null", """" & varBusinessName & """") &
            ",""orderRef"":" & IIf(varPEP = "", "null", """" & varPEP & """") &
            ",""orderRelatedRef"":" & IIf(varPrevSanc = "", "null", """" & varPrevSanc & """") &
            ",""message"":" & IIf(varCurrSanc = "", "null", """" & varCurrSanc & """") &
            ",""swiftUrgent"":" & varLaw.ToString().ToLower &
            ",""email"":" & IIf(varFinReg = "", "null", """" & varFinReg & """") &
            "}"
        Diagnostics.Debug.WriteLine(postData)
        'Dim postData As String = "Threshold"
        Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(postData)
        'request.Headers("apiKey") = "2729be39-e243-4fab-bc80-7b12e4258a05"
        'request.Headers("apiKey") = "1c57a4a1-c1c0-4003-9abf-39efd3b3e5dd"
        request.Headers("apiKey") = "f05ce2c3-a744-4792-a19a-6a718facc79e"
        request.ContentType = "application/json"
        request.ContentLength = byteArray.Length
        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim origResponse As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)

        Dim response As WebResponse = request.GetResponse()
        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
        dataStream = response.GetResponseStream()
        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        reader.Close()
        dataStream.Close()
        response.Close()


    End Sub

End Module

