﻿Public Class FrmSwiftRead
    Dim SRConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As DataSet

    Private Sub RefreshSRGrid(Optional ByVal varWhereClauseFilter As String = "")
        LoadSRGrid(varWhereClauseFilter)
        FormatSRGrid(dgvSwiftRead)
    End Sub


    Private Sub LoadSRGrid(Optional ByVal varWhereClauseFilter As String = "")
        Dim varSQL As String
        Try
            Cursor = Cursors.WaitCursor
            lbllastread.Text = "Last time swifts read from files: " & ClsIMS.GetSQLItem("select FE_UserModifiedTime from [DolfinPaymentFileExport] where FE_ID = 4")

            If cboDetailSelect.Text = "Detail" Then
                If varWhereClauseFilter = "" Then
                    varSQL = "select top 1000 * from vwDolfinPaymentSwiftReadDetail order by SRA_FileNameDate desc"
                Else
                    varSQL = "select top 1000 * from vwDolfinPaymentSwiftReadDetail " & varWhereClauseFilter & " order by SRA_FileNameDate Desc"
                End If
            Else
                If varWhereClauseFilter = "" Then
                    varSQL = "select top 1000 * from vwDolfinPaymentSwiftRead order by SR_FileDateTime desc"
                Else
                    varSQL = "select top 1000 * from vwDolfinPaymentSwiftRead " & varWhereClauseFilter & " order by SR_FileDateTime Desc"
                End If
            End If

            dgvSwiftRead.DataSource = Nothing
            SRConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, SRConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentSwiftRead")
            dgvSwiftRead.AutoGenerateColumns = True
            dgvSwiftRead.DataSource = ds.Tables("vwDolfinPaymentSwiftRead")

            dgvSwiftRead.Columns(0).Width = 100
            dgvSwiftRead.Columns(1).Width = 100
            dgvSwiftRead.Columns(2).Width = 100
            dgvSwiftRead.Columns(3).Width = 100
            dgvSwiftRead.Columns(4).Width = 100
            dgvSwiftRead.Columns(5).Width = 100
            dgvSwiftRead.Columns(6).Width = 100

            'dgvSwiftRead.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvSwiftRead.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvSwiftRead.DataSource = Nothing
        End Try
    End Sub

    Private Sub FormatSRGrid(ByRef dgv As DataGridView)
        For i As Integer = 0 To dgv.RowCount - 1
            If cboDetailSelect.Text = "Detail" Then
                dgv(dgv.Columns("SRA_FileName").Index, i) = New DataGridViewLinkCell
            Else
                dgv(dgv.Columns("SR_FileName").Index, i) = New DataGridViewLinkCell
            End If
        Next
    End Sub

    Private Sub FrmSwiftRead_Load(sender As Object, e As EventArgs) Handles Me.Load
        ClsIMS.PopulateComboboxWithData(CboFromDate, "Select 0,cast(SR_FileNameDate as date) from vwDolfinPaymentSwiftRead group by cast(SR_FileNameDate as date) order by cast(SR_FileNameDate as date) desc")
        ClsIMS.PopulateComboboxWithData(CboToDate, "Select 0,cast(SR_FileNameDate as date) from vwDolfinPaymentSwiftRead group by cast(SR_FileNameDate as date) order by cast(SR_FileNameDate as date) desc")
        ClsIMS.PopulateComboboxWithData(cboType, "Select 0,SR_SwiftType from vwDolfinPaymentSwiftRead group by SR_SwiftType order by SR_SwiftType")
        ClsIMS.PopulateComboboxWithData(cboSwiftCode, "Select 0,SR_Swiftcode from vwDolfinPaymentSwiftRead group by SR_Swiftcode order by SR_Swiftcode")
        ClsIMS.PopulateComboboxWithData(cboBroker, "Select 0,sc_BankName from vwDolfinPaymentSwiftRead sr inner join DolfinPaymentSwiftCodes sc on sr.sr_swiftcode = sc.sc_swiftcode group by sc_BankName order by sc_BankName")
        ClsIMS.PopulateComboboxWithData(cboAccountNo, "Select top 5000 0,SR_AccountNo from vwDolfinPaymentSwiftRead where isnull(SR_AccountNo,'')<>'' group by SR_AccountNo,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(CboUserRef, "Select top 5000 0,SR_SwiftUserReference from vwDolfinPaymentSwiftRead where isnull(SR_SwiftUserReference,'')<>'' group by SR_SwiftUserReference,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(cboSwiftRef, "Select top 5000 0,SR_Reference from vwDolfinPaymentSwiftRead where isnull(SR_Reference,'')<>'' group by SR_Reference,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(cboRelatedref, "Select top 5000 0,SR_RelatedReference from vwDolfinPaymentSwiftRead where isnull(SR_RelatedReference,'')<>'' group by SR_RelatedReference,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(cboLinkedRef, "Select top 5000 0,SR_LinkReference from vwDolfinPaymentSwiftRead where isnull(SR_LinkReference,'')<>'' and SR_LinkReference<>'NONREF' group by SR_LinkReference,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(CboFileName, "Select top 5000 0,SR_FileName from vwDolfinPaymentSwiftRead where isnull(SR_FileName,'')<>'' group by SR_FileName,SR_filedatetime order by SR_filedatetime desc")
        ClsIMS.PopulateComboboxWithData(CboISIN, "Select 0,SR_ISIN from vwDolfinPaymentSwiftRead group by SR_ISIN order by SR_ISIN")
        ClsIMS.PopulateComboboxWithData(CboAmt, "Select 0,SR_Amount from vwDolfinPaymentSwiftRead group by SR_Amount order by SR_Amount")
        ClsIMS.PopulateComboboxWithData(CboAmtCCY, "Select 0,SR_AmountCCY from vwDolfinPaymentSwiftRead group by SR_AmountCCY order by SR_AmountCCY")
        cboDetailSelect.Items.Add("Default")
        cboDetailSelect.Items.Add("Detail")

        ModRMS.DoubleBuffered(dgvSwiftRead, True)
    End Sub

    Private Sub FrmSwiftRead_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If SRConn.State = ConnectionState.Open Then
            SRConn.Close()
        End If
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varWhereClauseFilter As String = "where"

        If cboDetailSelect.Text = "Detail" Then
            If cboType.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sra_swifttype in ('" & cboType.Text & "')"
            End If

            If cboSwiftCode.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sra_swiftcode in ('" & cboSwiftCode.Text & "')"
            End If

            If cboBroker.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sc_BankName in ('" & cboBroker.Text & "')"
            End If

            If cboAccountNo.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_AccountNo in ('" & cboAccountNo.Text & "')"
            End If

            If CboUserRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_SwiftUserReference in ('" & CboUserRef.Text & "')"
            End If

            If cboSwiftRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_Reference in ('" & cboSwiftRef.Text & "')"
            End If

            If cboRelatedref.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_RelatedReference in ('" & cboRelatedref.Text & "')"
            End If

            If cboLinkedRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_LinkReference in ('" & cboLinkedRef.Text & "')"
            End If

            If CboFileName.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_filename in ('" & CboFileName.Text & "')"
            End If

            If CboISIN.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_ISIN in ('" & CboISIN.Text & "')"
            End If

            If CboAmt.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_Amount in ('" & CboAmt.Text & "')"
            End If

            If CboAmtCCY.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SRA_AmountCCY in ('" & CboAmtCCY.Text & "')"
            End If

            If CboFromDate.Text <> "" Or CboToDate.Text <> "" Then
                If CboFromDate.Text <> "" And CboToDate.Text = "" Then
                    varWhereClauseFilter = varWhereClauseFilter & " and cast(sra_filenamedate as date) = cast('" & CDate(CboFromDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                ElseIf CboFromDate.Text = "" And CboToDate.Text <> "" Then
                    varWhereClauseFilter = varWhereClauseFilter & " and cast(sra_filenamedate as date) = cast('" & CDate(CboToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                ElseIf CboFromDate.Text <> "" And CboToDate.Text <> "" Then
                    If CDate(CboFromDate.Text) <= CDate(CboToDate.Text) Then
                        varWhereClauseFilter = varWhereClauseFilter & " and cast(sra_filenamedate as date) between cast('" & CDate(CboFromDate.Text).ToString("yyyy-MM-dd") & "' as date) and cast('" & CDate(CboToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                    End If
                End If
            End If
        Else
            If cboType.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sr_swifttype in ('" & cboType.Text & "')"
            End If

            If cboSwiftCode.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sr_swiftcode in ('" & cboSwiftCode.Text & "')"
            End If

            If cboBroker.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and sc_BankName in ('" & cboBroker.Text & "')"
            End If

            If cboAccountNo.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_AccountNo in ('" & cboAccountNo.Text & "')"
            End If

            If CboUserRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_SwiftUserReference in ('" & CboUserRef.Text & "')"
            End If

            If cboSwiftRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_Reference in ('" & cboSwiftRef.Text & "')"
            End If

            If cboRelatedref.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_RelatedReference in ('" & cboRelatedref.Text & "')"
            End If

            If cboLinkedRef.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_LinkReference in ('" & cboLinkedRef.Text & "')"
            End If

            If CboFileName.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_filename in ('" & CboFileName.Text & "')"
            End If

            If CboISIN.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_ISIN in ('" & CboISIN.Text & "')"
            End If

            If CboAmt.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_Amount in ('" & CboAmt.Text & "')"
            End If

            If CboAmtCCY.Text <> "" Then
                varWhereClauseFilter = varWhereClauseFilter & " and SR_AmountCCY in ('" & CboAmtCCY.Text & "')"
            End If

            If CboFromDate.Text <> "" Or CboToDate.Text <> "" Then
                If CboFromDate.Text <> "" And CboToDate.Text = "" Then
                    varWhereClauseFilter = varWhereClauseFilter & " and cast(sr_filenamedate as date) = cast('" & CDate(CboFromDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                ElseIf CboFromDate.Text = "" And CboToDate.Text <> "" Then
                    varWhereClauseFilter = varWhereClauseFilter & " and cast(sr_filenamedate as date) = cast('" & CDate(CboToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                ElseIf CboFromDate.Text <> "" And CboToDate.Text <> "" Then
                    If CDate(CboFromDate.Text) <= CDate(CboToDate.Text) Then
                        varWhereClauseFilter = varWhereClauseFilter & " and cast(sr_filenamedate as date) between cast('" & CDate(CboFromDate.Text).ToString("yyyy-MM-dd") & "' as date) and cast('" & CDate(CboToDate.Text).ToString("yyyy-MM-dd") & "' as date)"
                    End If
                End If
            End If
        End If

        If varWhereClauseFilter <> "where" Then
            RefreshSRGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            RefreshSRGrid()
        End If
    End Sub

    Private Sub dgvSwiftRead_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvSwiftRead.CellMouseClick
        If e.RowIndex >= 0 And e.ColumnIndex >= 0 Then
            If Not IsDBNull(dgvSwiftRead.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                If Strings.Right(dgvSwiftRead.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, 4) = ".fin" Then
                    ClsIMS.OpenSwiftFile(dgvSwiftRead.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                End If
            End If
        End If
    End Sub

    Private Sub FrmSwiftRead_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        RefreshSRGrid()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvSwiftRead, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        cboType.Text = ""
        cboSwiftCode.Text = ""
        cboBroker.Text = ""
        cboAccountNo.Text = ""
        CboUserRef.Text = ""
        cboSwiftRef.Text = ""
        cboRelatedref.Text = ""
        cboLinkedRef.Text = ""
        CboFileName.Text = ""
        CboISIN.Text = ""
        CboAmt.Text = ""
        CboAmtCCY.Text = ""
        CboFromDate.Text = ""
        CboToDate.Text = ""
    End Sub
End Class
