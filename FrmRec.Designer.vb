﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRec
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRec))
        Me.dgvRec = New System.Windows.Forms.DataGridView()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.GrpFSDetails = New System.Windows.Forms.GroupBox()
        Me.dtFromDate = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtToDate = New System.Windows.Forms.DateTimePicker()
        Me.ChkReCalcRec = New System.Windows.Forms.CheckBox()
        Me.cmdRunRec = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.mnuSelect = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuMatch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUnMatch = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBoxFilter = New System.Windows.Forms.GroupBox()
        Me.LblTotals = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboFilterDate = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboFilterStatusDesc = New System.Windows.Forms.ComboBox()
        Me.cboFilterBankType = New System.Windows.Forms.ComboBox()
        Me.cboFilterIMSType = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboFilterCCY = New System.Windows.Forms.ComboBox()
        Me.cboFilterISIN = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboFilterAccountName = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboFilterAccountNo = New System.Windows.Forms.ComboBox()
        Me.cmdClearFilter = New System.Windows.Forms.Button()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboFilterBroker = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpFSDetails.SuspendLayout()
        Me.mnuSelect.SuspendLayout()
        Me.GroupBoxFilter.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvRec
        '
        Me.dgvRec.AllowUserToAddRows = False
        Me.dgvRec.AllowUserToDeleteRows = False
        Me.dgvRec.AllowUserToResizeRows = False
        Me.dgvRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRec.Location = New System.Drawing.Point(12, 121)
        Me.dgvRec.Name = "dgvRec"
        Me.dgvRec.Size = New System.Drawing.Size(1604, 719)
        Me.dgvRec.TabIndex = 112
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(19, 9)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(188, 24)
        Me.lblpaymentTitle.TabIndex = 114
        Me.lblpaymentTitle.Text = "IMS Reconcilliation"
        '
        'GrpFSDetails
        '
        Me.GrpFSDetails.BackColor = System.Drawing.Color.OldLace
        Me.GrpFSDetails.Controls.Add(Me.dtFromDate)
        Me.GrpFSDetails.Controls.Add(Me.Label11)
        Me.GrpFSDetails.Controls.Add(Me.dtToDate)
        Me.GrpFSDetails.Controls.Add(Me.ChkReCalcRec)
        Me.GrpFSDetails.Controls.Add(Me.cmdRunRec)
        Me.GrpFSDetails.Controls.Add(Me.Label18)
        Me.GrpFSDetails.Location = New System.Drawing.Point(12, 36)
        Me.GrpFSDetails.Name = "GrpFSDetails"
        Me.GrpFSDetails.Size = New System.Drawing.Size(398, 79)
        Me.GrpFSDetails.TabIndex = 113
        Me.GrpFSDetails.TabStop = False
        Me.GrpFSDetails.Text = "Reconcilliation Selection Criteria"
        '
        'dtFromDate
        '
        Me.dtFromDate.Location = New System.Drawing.Point(75, 20)
        Me.dtFromDate.Name = "dtFromDate"
        Me.dtFromDate.Size = New System.Drawing.Size(124, 20)
        Me.dtFromDate.TabIndex = 117
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 23)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 13)
        Me.Label11.TabIndex = 116
        Me.Label11.Text = "From Date:"
        '
        'dtToDate
        '
        Me.dtToDate.Location = New System.Drawing.Point(260, 19)
        Me.dtToDate.Name = "dtToDate"
        Me.dtToDate.Size = New System.Drawing.Size(129, 20)
        Me.dtToDate.TabIndex = 115
        '
        'ChkReCalcRec
        '
        Me.ChkReCalcRec.AutoSize = True
        Me.ChkReCalcRec.Location = New System.Drawing.Point(75, 50)
        Me.ChkReCalcRec.Name = "ChkReCalcRec"
        Me.ChkReCalcRec.Size = New System.Drawing.Size(139, 17)
        Me.ChkReCalcRec.TabIndex = 114
        Me.ChkReCalcRec.Text = "ReCalc Reconcilliation?"
        Me.ChkReCalcRec.UseVisualStyleBackColor = True
        '
        'cmdRunRec
        '
        Me.cmdRunRec.Location = New System.Drawing.Point(261, 50)
        Me.cmdRunRec.Name = "cmdRunRec"
        Me.cmdRunRec.Size = New System.Drawing.Size(131, 23)
        Me.cmdRunRec.TabIndex = 113
        Me.cmdRunRec.Text = "Run Reconcilliation"
        Me.cmdRunRec.UseVisualStyleBackColor = True
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(205, 23)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(49, 13)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "To Date:"
        '
        'mnuSelect
        '
        Me.mnuSelect.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMatch, Me.mnuUnMatch})
        Me.mnuSelect.Name = "mnuSelect"
        Me.mnuSelect.Size = New System.Drawing.Size(203, 48)
        '
        'mnuMatch
        '
        Me.mnuMatch.Name = "mnuMatch"
        Me.mnuMatch.Size = New System.Drawing.Size(202, 22)
        Me.mnuMatch.Text = "&Match Selected Items"
        '
        'mnuUnMatch
        '
        Me.mnuUnMatch.Name = "mnuUnMatch"
        Me.mnuUnMatch.Size = New System.Drawing.Size(202, 22)
        Me.mnuUnMatch.Text = "&Unmatch Selected Items"
        '
        'GroupBoxFilter
        '
        Me.GroupBoxFilter.BackColor = System.Drawing.Color.OldLace
        Me.GroupBoxFilter.Controls.Add(Me.LblTotals)
        Me.GroupBoxFilter.Controls.Add(Me.Label10)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterDate)
        Me.GroupBoxFilter.Controls.Add(Me.Label9)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterStatusDesc)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterBankType)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterIMSType)
        Me.GroupBoxFilter.Controls.Add(Me.Label6)
        Me.GroupBoxFilter.Controls.Add(Me.Label8)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterCCY)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterISIN)
        Me.GroupBoxFilter.Controls.Add(Me.Label1)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterAccountName)
        Me.GroupBoxFilter.Controls.Add(Me.Label3)
        Me.GroupBoxFilter.Controls.Add(Me.Label4)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterAccountNo)
        Me.GroupBoxFilter.Controls.Add(Me.cmdClearFilter)
        Me.GroupBoxFilter.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBoxFilter.Controls.Add(Me.Label2)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterBroker)
        Me.GroupBoxFilter.Controls.Add(Me.Label7)
        Me.GroupBoxFilter.Controls.Add(Me.Label5)
        Me.GroupBoxFilter.Controls.Add(Me.cboFilterStatus)
        Me.GroupBoxFilter.Controls.Add(Me.CmdFilter)
        Me.GroupBoxFilter.Location = New System.Drawing.Point(436, 12)
        Me.GroupBoxFilter.Name = "GroupBoxFilter"
        Me.GroupBoxFilter.Size = New System.Drawing.Size(1180, 103)
        Me.GroupBoxFilter.TabIndex = 116
        Me.GroupBoxFilter.TabStop = False
        Me.GroupBoxFilter.Text = "Filter Grid Criteria"
        Me.GroupBoxFilter.Visible = False
        '
        'LblTotals
        '
        Me.LblTotals.AutoSize = True
        Me.LblTotals.Location = New System.Drawing.Point(1081, 84)
        Me.LblTotals.Name = "LblTotals"
        Me.LblTotals.Size = New System.Drawing.Size(45, 13)
        Me.LblTotals.TabIndex = 144
        Me.LblTotals.Text = "Label12"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 49)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 13)
        Me.Label10.TabIndex = 143
        Me.Label10.Text = "Date:"
        '
        'cboFilterDate
        '
        Me.cboFilterDate.FormattingEnabled = True
        Me.cboFilterDate.Location = New System.Drawing.Point(53, 46)
        Me.cboFilterDate.Name = "cboFilterDate"
        Me.cboFilterDate.Size = New System.Drawing.Size(118, 21)
        Me.cboFilterDate.TabIndex = 142
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(186, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 141
        Me.Label9.Text = "Desc:"
        '
        'cboFilterStatusDesc
        '
        Me.cboFilterStatusDesc.FormattingEnabled = True
        Me.cboFilterStatusDesc.Location = New System.Drawing.Point(224, 19)
        Me.cboFilterStatusDesc.Name = "cboFilterStatusDesc"
        Me.cboFilterStatusDesc.Size = New System.Drawing.Size(190, 21)
        Me.cboFilterStatusDesc.TabIndex = 140
        '
        'cboFilterBankType
        '
        Me.cboFilterBankType.FormattingEnabled = True
        Me.cboFilterBankType.Location = New System.Drawing.Point(490, 74)
        Me.cboFilterBankType.Name = "cboFilterBankType"
        Me.cboFilterBankType.Size = New System.Drawing.Size(82, 21)
        Me.cboFilterBankType.TabIndex = 139
        '
        'cboFilterIMSType
        '
        Me.cboFilterIMSType.FormattingEnabled = True
        Me.cboFilterIMSType.Location = New System.Drawing.Point(640, 74)
        Me.cboFilterIMSType.Name = "cboFilterIMSType"
        Me.cboFilterIMSType.Size = New System.Drawing.Size(179, 21)
        Me.cboFilterIMSType.TabIndex = 137
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(578, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 138
        Me.Label6.Text = "IMS Type:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(422, 78)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 136
        Me.Label8.Text = "Bank Type:"
        '
        'cboFilterCCY
        '
        Me.cboFilterCCY.FormattingEnabled = True
        Me.cboFilterCCY.Location = New System.Drawing.Point(53, 74)
        Me.cboFilterCCY.Name = "cboFilterCCY"
        Me.cboFilterCCY.Size = New System.Drawing.Size(118, 21)
        Me.cboFilterCCY.TabIndex = 135
        '
        'cboFilterISIN
        '
        Me.cboFilterISIN.FormattingEnabled = True
        Me.cboFilterISIN.Location = New System.Drawing.Point(224, 74)
        Me.cboFilterISIN.Name = "cboFilterISIN"
        Me.cboFilterISIN.Size = New System.Drawing.Size(190, 21)
        Me.cboFilterISIN.TabIndex = 133
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(187, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 134
        Me.Label1.Text = "ISIN:"
        '
        'cboFilterAccountName
        '
        Me.cboFilterAccountName.FormattingEnabled = True
        Me.cboFilterAccountName.Location = New System.Drawing.Point(490, 46)
        Me.cboFilterAccountName.Name = "cboFilterAccountName"
        Me.cboFilterAccountName.Size = New System.Drawing.Size(329, 21)
        Me.cboFilterAccountName.TabIndex = 131
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(424, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 132
        Me.Label3.Text = "Acc Name:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(438, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 130
        Me.Label4.Text = "Acc No:"
        '
        'cboFilterAccountNo
        '
        Me.cboFilterAccountNo.FormattingEnabled = True
        Me.cboFilterAccountNo.Location = New System.Drawing.Point(490, 19)
        Me.cboFilterAccountNo.Name = "cboFilterAccountNo"
        Me.cboFilterAccountNo.Size = New System.Drawing.Size(329, 21)
        Me.cboFilterAccountNo.TabIndex = 129
        '
        'cmdClearFilter
        '
        Me.cmdClearFilter.Location = New System.Drawing.Point(839, 72)
        Me.cmdClearFilter.Name = "cmdClearFilter"
        Me.cmdClearFilter.Size = New System.Drawing.Size(127, 23)
        Me.cmdClearFilter.TabIndex = 128
        Me.cmdClearFilter.Text = "Clear Filter"
        Me.cmdClearFilter.UseVisualStyleBackColor = True
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1130, 14)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(44, 45)
        Me.CmdExportToExcel.TabIndex = 116
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 125
        Me.Label2.Text = "CCY:"
        '
        'cboFilterBroker
        '
        Me.cboFilterBroker.FormattingEnabled = True
        Me.cboFilterBroker.Location = New System.Drawing.Point(224, 45)
        Me.cboFilterBroker.Name = "cboFilterBroker"
        Me.cboFilterBroker.Size = New System.Drawing.Size(190, 21)
        Me.cboFilterBroker.TabIndex = 122
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(177, 50)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 123
        Me.Label7.Text = "Broker:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 117
        Me.Label5.Text = "Status:"
        '
        'cboFilterStatus
        '
        Me.cboFilterStatus.FormattingEnabled = True
        Me.cboFilterStatus.Location = New System.Drawing.Point(53, 18)
        Me.cboFilterStatus.Name = "cboFilterStatus"
        Me.cboFilterStatus.Size = New System.Drawing.Size(118, 21)
        Me.cboFilterStatus.TabIndex = 116
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(839, 18)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(127, 45)
        Me.CmdFilter.TabIndex = 113
        Me.CmdFilter.Text = "Filter"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'FrmRec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1633, 852)
        Me.Controls.Add(Me.GroupBoxFilter)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.GrpFSDetails)
        Me.Controls.Add(Me.dgvRec)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmRec"
        Me.Text = "IMS Reconcilliation"
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpFSDetails.ResumeLayout(False)
        Me.GrpFSDetails.PerformLayout()
        Me.mnuSelect.ResumeLayout(False)
        Me.GroupBoxFilter.ResumeLayout(False)
        Me.GroupBoxFilter.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvRec As DataGridView
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents GrpFSDetails As GroupBox
    Friend WithEvents cmdRunRec As Button
    Friend WithEvents Label18 As Label
    Friend WithEvents ChkReCalcRec As CheckBox
    Friend WithEvents dtToDate As DateTimePicker
    Friend WithEvents mnuSelect As ContextMenuStrip
    Friend WithEvents mnuMatch As ToolStripMenuItem
    Friend WithEvents GroupBoxFilter As GroupBox
    Friend WithEvents cmdClearFilter As Button
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboFilterBroker As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents cboFilterStatus As ComboBox
    Friend WithEvents CmdFilter As Button
    Friend WithEvents cboFilterISIN As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboFilterAccountName As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents cboFilterAccountNo As ComboBox
    Friend WithEvents cboFilterCCY As ComboBox
    Friend WithEvents mnuUnMatch As ToolStripMenuItem
    Friend WithEvents cboFilterBankType As ComboBox
    Friend WithEvents cboFilterIMSType As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents cboFilterStatusDesc As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents cboFilterDate As ComboBox
    Friend WithEvents dtFromDate As DateTimePicker
    Friend WithEvents Label11 As Label
    Friend WithEvents LblTotals As Label
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
