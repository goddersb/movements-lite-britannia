﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPayment))
        Me.cmdAddCash = New System.Windows.Forms.Button()
        Me.dgvCash = New System.Windows.Forms.DataGridView()
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.GrpTransType = New System.Windows.Forms.GroupBox()
        Me.lblExchRate = New System.Windows.Forms.Label()
        Me.txtExchRate = New System.Windows.Forms.TextBox()
        Me.ChkAutoComments = New System.Windows.Forms.CheckBox()
        Me.lblCCYAmt = New System.Windows.Forms.Label()
        Me.lblExchangeRate = New System.Windows.Forms.Label()
        Me.cboBenCCY = New System.Windows.Forms.ComboBox()
        Me.lblBenCCY = New System.Windows.Forms.Label()
        Me.PicPayType = New System.Windows.Forms.PictureBox()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.lblportfolio = New System.Windows.Forms.Label()
        Me.CboPortfolio = New System.Windows.Forms.ComboBox()
        Me.CboCCY = New System.Windows.Forms.ComboBox()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.txtComments = New System.Windows.Forms.TextBox()
        Me.lblcomments = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.lblTransDate = New System.Windows.Forms.Label()
        Me.dtTransDate = New System.Windows.Forms.DateTimePicker()
        Me.lblSettleDate = New System.Windows.Forms.Label()
        Me.dtSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.LblPayType = New System.Windows.Forms.Label()
        Me.CboPayType = New System.Windows.Forms.ComboBox()
        Me.lblOrderBalancePostInc = New System.Windows.Forms.Label()
        Me.txtOrderBalanceInc = New System.Windows.Forms.TextBox()
        Me.lblOrderAccount = New System.Windows.Forms.Label()
        Me.BeneficiaryGroupBox = New System.Windows.Forms.GroupBox()
        Me.TabBeneficiary = New System.Windows.Forms.TabControl()
        Me.MainTab = New System.Windows.Forms.TabPage()
        Me.ChkBenTemplate = New System.Windows.Forms.CheckBox()
        Me.ChkBenTemplateEdit = New System.Windows.Forms.CheckBox()
        Me.cboBenType = New System.Windows.Forms.ComboBox()
        Me.lblbenType = New System.Windows.Forms.Label()
        Me.cboBenCountry = New System.Windows.Forms.ComboBox()
        Me.lblBenCountry = New System.Windows.Forms.Label()
        Me.lblBenCounty = New System.Windows.Forms.Label()
        Me.txtbencounty = New System.Windows.Forms.TextBox()
        Me.txtBenZip = New System.Windows.Forms.TextBox()
        Me.txtBenAddress1 = New System.Windows.Forms.TextBox()
        Me.txtBenAddress2 = New System.Windows.Forms.TextBox()
        Me.txtbensurname = New System.Windows.Forms.TextBox()
        Me.txtbenmiddlename = New System.Windows.Forms.TextBox()
        Me.txtbenfirstname = New System.Windows.Forms.TextBox()
        Me.txtBenRef = New System.Windows.Forms.TextBox()
        Me.txtBenBalancePostInc = New System.Windows.Forms.TextBox()
        Me.txtBenBalanceInc = New System.Windows.Forms.TextBox()
        Me.txtBenBalance = New System.Windows.Forms.TextBox()
        Me.lblBenAddress1 = New System.Windows.Forms.Label()
        Me.lblBenAddress2 = New System.Windows.Forms.Label()
        Me.lblBenZip = New System.Windows.Forms.Label()
        Me.cbobenrelationship = New System.Windows.Forms.ComboBox()
        Me.lblBenRelationship = New System.Windows.Forms.Label()
        Me.lblBenSurname = New System.Windows.Forms.Label()
        Me.lblBenMiddlename = New System.Windows.Forms.Label()
        Me.lblBenFirstname = New System.Windows.Forms.Label()
        Me.lblBenBalancePostInc = New System.Windows.Forms.Label()
        Me.lblBenBalanceInc = New System.Windows.Forms.Label()
        Me.lblBenName = New System.Windows.Forms.Label()
        Me.lblBenRef = New System.Windows.Forms.Label()
        Me.CboBenName = New System.Windows.Forms.ComboBox()
        Me.CboBenAccount = New System.Windows.Forms.ComboBox()
        Me.lblBenAccount = New System.Windows.Forms.Label()
        Me.lblBenBalance = New System.Windows.Forms.Label()
        Me.BankDetailsTab = New System.Windows.Forms.TabPage()
        Me.lblbenmsg3 = New System.Windows.Forms.LinkLabel()
        Me.lblbenmsg2 = New System.Windows.Forms.LinkLabel()
        Me.lblbenmsg1 = New System.Windows.Forms.LinkLabel()
        Me.txtBenBIK = New System.Windows.Forms.TextBox()
        Me.txtBenINN = New System.Windows.Forms.TextBox()
        Me.txtBenSwift = New System.Windows.Forms.TextBox()
        Me.txtBenIBAN = New System.Windows.Forms.TextBox()
        Me.txtBenOther1 = New System.Windows.Forms.TextBox()
        Me.lblbenBIK = New System.Windows.Forms.Label()
        Me.lblBenSwift = New System.Windows.Forms.Label()
        Me.lblBenIBAN = New System.Windows.Forms.Label()
        Me.lblbenINN = New System.Windows.Forms.Label()
        Me.ChkBenSendRef = New System.Windows.Forms.CheckBox()
        Me.ChkBenUrgent = New System.Windows.Forms.CheckBox()
        Me.lblBenOther1 = New System.Windows.Forms.Label()
        Me.PicBenSwift = New System.Windows.Forms.PictureBox()
        Me.PicBenIBAN = New System.Windows.Forms.PictureBox()
        Me.OrderGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboOrderCountry = New System.Windows.Forms.ComboBox()
        Me.lblOrderCountry = New System.Windows.Forms.Label()
        Me.txtIMSRef = New System.Windows.Forms.TextBox()
        Me.lblIMSRef = New System.Windows.Forms.Label()
        Me.lblOrderVOCode = New System.Windows.Forms.Label()
        Me.cboOrderVOCode = New System.Windows.Forms.ComboBox()
        Me.lblordermsg8 = New System.Windows.Forms.LinkLabel()
        Me.txtOrderBalancePostInc = New System.Windows.Forms.TextBox()
        Me.lblOrderBalanceInc = New System.Windows.Forms.Label()
        Me.lblordermsg7 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg6 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg5 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg4 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg3 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg2 = New System.Windows.Forms.LinkLabel()
        Me.lblordermsg1 = New System.Windows.Forms.LinkLabel()
        Me.PicOrderIBAN = New System.Windows.Forms.PictureBox()
        Me.PicOrderSwift = New System.Windows.Forms.PictureBox()
        Me.lblOrderBalance = New System.Windows.Forms.Label()
        Me.txtOrderBalance = New System.Windows.Forms.TextBox()
        Me.CboOrderAccount = New System.Windows.Forms.ComboBox()
        Me.txtOrderOther1 = New System.Windows.Forms.TextBox()
        Me.txtOrderIBAN = New System.Windows.Forms.TextBox()
        Me.txtOrderSwift = New System.Windows.Forms.TextBox()
        Me.txtOrderZip = New System.Windows.Forms.TextBox()
        Me.txtOrderAddress2 = New System.Windows.Forms.TextBox()
        Me.txtOrderAddress1 = New System.Windows.Forms.TextBox()
        Me.lblOrderZip = New System.Windows.Forms.Label()
        Me.lblOrderAddress2 = New System.Windows.Forms.Label()
        Me.lblOrderAddress1 = New System.Windows.Forms.Label()
        Me.lblOrderOther1 = New System.Windows.Forms.Label()
        Me.lblOrderIBAN = New System.Windows.Forms.Label()
        Me.lblOrderSwift = New System.Windows.Forms.Label()
        Me.CboOrderName = New System.Windows.Forms.ComboBox()
        Me.txtOrderRef = New System.Windows.Forms.TextBox()
        Me.lblorderref = New System.Windows.Forms.Label()
        Me.lblOrderName = New System.Windows.Forms.Label()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.TabMovement = New System.Windows.Forms.TabControl()
        Me.OrderTab = New System.Windows.Forms.TabPage()
        Me.BeneficiaryTab = New System.Windows.Forms.TabPage()
        Me.BeneficiaryGroupBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.BeneficiarySubBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboBenSubBankCountry = New System.Windows.Forms.ComboBox()
        Me.txtBenSubbankINN = New System.Windows.Forms.TextBox()
        Me.txtBenSubBankBIK = New System.Windows.Forms.TextBox()
        Me.lblBenSubBankBIK = New System.Windows.Forms.Label()
        Me.lblBenSubBankINN = New System.Windows.Forms.Label()
        Me.ChkBenSubBank = New System.Windows.Forms.CheckBox()
        Me.txtBenSubBankOther1 = New System.Windows.Forms.TextBox()
        Me.PicBenSubBankIBAN = New System.Windows.Forms.PictureBox()
        Me.PicBenSubBankSwift = New System.Windows.Forms.PictureBox()
        Me.CboBenSubBankName = New System.Windows.Forms.ComboBox()
        Me.lblBenSubBankSwift = New System.Windows.Forms.Label()
        Me.txtBenSubBankIBAN = New System.Windows.Forms.TextBox()
        Me.lblBenSubBankIBAN = New System.Windows.Forms.Label()
        Me.txtBenSubBankSwift = New System.Windows.Forms.TextBox()
        Me.lblBenSubBankOther1 = New System.Windows.Forms.Label()
        Me.txtBenSubBankZip = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtBenSubBankAddress2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtBenSubBankAddress1 = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.BeneficiaryBankGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboBenBankCountry = New System.Windows.Forms.ComboBox()
        Me.txtBenBankINN = New System.Windows.Forms.TextBox()
        Me.txtBenBankBIK = New System.Windows.Forms.TextBox()
        Me.lblBenBankBIK = New System.Windows.Forms.Label()
        Me.lblBenBankINN = New System.Windows.Forms.Label()
        Me.txtBenBankOther1 = New System.Windows.Forms.TextBox()
        Me.PicBenBankIBAN = New System.Windows.Forms.PictureBox()
        Me.lblBenBankName = New System.Windows.Forms.Label()
        Me.PicBenBankSwift = New System.Windows.Forms.PictureBox()
        Me.CboBenBankName = New System.Windows.Forms.ComboBox()
        Me.lblBenBankSwift = New System.Windows.Forms.Label()
        Me.txtBenBankIBAN = New System.Windows.Forms.TextBox()
        Me.lblBenBankIBAN = New System.Windows.Forms.Label()
        Me.txtBenBankSwift = New System.Windows.Forms.TextBox()
        Me.lblBenBankOther1 = New System.Windows.Forms.Label()
        Me.txtBenBankZip = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress1 = New System.Windows.Forms.Label()
        Me.txtBenBankAddress2 = New System.Windows.Forms.TextBox()
        Me.lblBenBankAddress2 = New System.Windows.Forms.Label()
        Me.txtBenBankAddress1 = New System.Windows.Forms.TextBox()
        Me.lblBenBankZip = New System.Windows.Forms.Label()
        Me.FileTab = New System.Windows.Forms.TabPage()
        Me.GrpFiles = New System.Windows.Forms.GroupBox()
        Me.TreeViewFiles = New System.Windows.Forms.TreeView()
        Me.ImageListFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.LVFiles = New System.Windows.Forms.ListView()
        Me.HistoryTab = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvHistory = New System.Windows.Forms.DataGridView()
        Me.PreviewTab = New System.Windows.Forms.TabPage()
        Me.GroupSWIFT = New System.Windows.Forms.GroupBox()
        Me.txtSWIFT = New System.Windows.Forms.TextBox()
        Me.RequestTab = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GrpSendMessage = New System.Windows.Forms.GroupBox()
        Me.txtRelatedRef = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboSendTo = New System.Windows.Forms.ComboBox()
        Me.txtEmailRequest = New System.Windows.Forms.TextBox()
        Me.MovementToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.TimerBroker = New System.Windows.Forms.Timer(Me.components)
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.ImageListTickCross = New System.Windows.Forms.ImageList(Me.components)
        Me.ContextMenuStripFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.CboFilterStatus = New System.Windows.Forms.ComboBox()
        Me.ChkCashRefresh = New System.Windows.Forms.CheckBox()
        Me.cboPayCheck = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.cboFilterType = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboFilterLocation = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFilterID = New System.Windows.Forms.TextBox()
        Me.ChkShowHidden = New System.Windows.Forms.CheckBox()
        Me.CmdSubmitBatch = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgvCash, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpTransType.SuspendLayout()
        CType(Me.PicPayType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryGroupBox.SuspendLayout()
        Me.TabBeneficiary.SuspendLayout()
        Me.MainTab.SuspendLayout()
        Me.BankDetailsTab.SuspendLayout()
        CType(Me.PicBenSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBenIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.OrderGroupBox.SuspendLayout()
        CType(Me.PicOrderIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicOrderSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabMovement.SuspendLayout()
        Me.OrderTab.SuspendLayout()
        Me.BeneficiaryTab.SuspendLayout()
        Me.BeneficiaryGroupBankGroupBox.SuspendLayout()
        Me.BeneficiarySubBankGroupBox.SuspendLayout()
        CType(Me.PicBenSubBankIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBenSubBankSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BeneficiaryBankGroupBox.SuspendLayout()
        CType(Me.PicBenBankIBAN, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicBenBankSwift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FileTab.SuspendLayout()
        Me.GrpFiles.SuspendLayout()
        Me.HistoryTab.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PreviewTab.SuspendLayout()
        Me.GroupSWIFT.SuspendLayout()
        Me.RequestTab.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GrpSendMessage.SuspendLayout()
        Me.ContextMenuStripFiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdAddCash
        '
        Me.cmdAddCash.Location = New System.Drawing.Point(11, 535)
        Me.cmdAddCash.Name = "cmdAddCash"
        Me.cmdAddCash.Size = New System.Drawing.Size(246, 24)
        Me.cmdAddCash.TabIndex = 34
        Me.cmdAddCash.Text = "Add Movement"
        Me.cmdAddCash.UseVisualStyleBackColor = True
        '
        'dgvCash
        '
        Me.dgvCash.AllowUserToAddRows = False
        Me.dgvCash.AllowUserToDeleteRows = False
        Me.dgvCash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCash.Location = New System.Drawing.Point(11, 562)
        Me.dgvCash.MultiSelect = False
        Me.dgvCash.Name = "dgvCash"
        Me.dgvCash.ReadOnly = True
        Me.dgvCash.Size = New System.Drawing.Size(1571, 279)
        Me.dgvCash.TabIndex = 35
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(15, 9)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(170, 24)
        Me.lblpaymentTitle.TabIndex = 7
        Me.lblpaymentTitle.Text = "Cash Movements"
        '
        'GrpTransType
        '
        Me.GrpTransType.Controls.Add(Me.lblExchRate)
        Me.GrpTransType.Controls.Add(Me.txtExchRate)
        Me.GrpTransType.Controls.Add(Me.ChkAutoComments)
        Me.GrpTransType.Controls.Add(Me.lblCCYAmt)
        Me.GrpTransType.Controls.Add(Me.lblExchangeRate)
        Me.GrpTransType.Controls.Add(Me.cboBenCCY)
        Me.GrpTransType.Controls.Add(Me.lblBenCCY)
        Me.GrpTransType.Controls.Add(Me.PicPayType)
        Me.GrpTransType.Controls.Add(Me.lblportfoliofee)
        Me.GrpTransType.Controls.Add(Me.lblportfolio)
        Me.GrpTransType.Controls.Add(Me.CboPortfolio)
        Me.GrpTransType.Controls.Add(Me.CboCCY)
        Me.GrpTransType.Controls.Add(Me.lblCCY)
        Me.GrpTransType.Controls.Add(Me.txtComments)
        Me.GrpTransType.Controls.Add(Me.lblcomments)
        Me.GrpTransType.Controls.Add(Me.lblAmount)
        Me.GrpTransType.Controls.Add(Me.txtAmount)
        Me.GrpTransType.Controls.Add(Me.lblTransDate)
        Me.GrpTransType.Controls.Add(Me.dtTransDate)
        Me.GrpTransType.Controls.Add(Me.lblSettleDate)
        Me.GrpTransType.Controls.Add(Me.dtSettleDate)
        Me.GrpTransType.Controls.Add(Me.LblPayType)
        Me.GrpTransType.Controls.Add(Me.CboPayType)
        Me.GrpTransType.Location = New System.Drawing.Point(12, 36)
        Me.GrpTransType.Name = "GrpTransType"
        Me.GrpTransType.Size = New System.Drawing.Size(1570, 115)
        Me.GrpTransType.TabIndex = 9
        Me.GrpTransType.TabStop = False
        Me.GrpTransType.Text = "Movement Details"
        '
        'lblExchRate
        '
        Me.lblExchRate.AutoSize = True
        Me.lblExchRate.Location = New System.Drawing.Point(551, 58)
        Me.lblExchRate.Name = "lblExchRate"
        Me.lblExchRate.Size = New System.Drawing.Size(33, 13)
        Me.lblExchRate.TabIndex = 55
        Me.lblExchRate.Text = "Rate:"
        Me.lblExchRate.Visible = False
        '
        'txtExchRate
        '
        Me.txtExchRate.Location = New System.Drawing.Point(590, 55)
        Me.txtExchRate.Name = "txtExchRate"
        Me.txtExchRate.Size = New System.Drawing.Size(146, 20)
        Me.txtExchRate.TabIndex = 54
        Me.txtExchRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtExchRate.Visible = False
        '
        'ChkAutoComments
        '
        Me.ChkAutoComments.AutoSize = True
        Me.ChkAutoComments.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkAutoComments.Checked = True
        Me.ChkAutoComments.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkAutoComments.Location = New System.Drawing.Point(1464, 11)
        Me.ChkAutoComments.Name = "ChkAutoComments"
        Me.ChkAutoComments.Size = New System.Drawing.Size(100, 17)
        Me.ChkAutoComments.TabIndex = 53
        Me.ChkAutoComments.Text = "Auto Comments"
        Me.ChkAutoComments.UseVisualStyleBackColor = True
        '
        'lblCCYAmt
        '
        Me.lblCCYAmt.AutoSize = True
        Me.lblCCYAmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCCYAmt.ForeColor = System.Drawing.Color.Black
        Me.lblCCYAmt.Location = New System.Drawing.Point(523, 58)
        Me.lblCCYAmt.Name = "lblCCYAmt"
        Me.lblCCYAmt.Size = New System.Drawing.Size(0, 15)
        Me.lblCCYAmt.TabIndex = 52
        '
        'lblExchangeRate
        '
        Me.lblExchangeRate.AutoSize = True
        Me.lblExchangeRate.ForeColor = System.Drawing.Color.Maroon
        Me.lblExchangeRate.Location = New System.Drawing.Point(596, 85)
        Me.lblExchangeRate.Name = "lblExchangeRate"
        Me.lblExchangeRate.Size = New System.Drawing.Size(116, 13)
        Me.lblExchangeRate.TabIndex = 51
        Me.lblExchangeRate.Text = "@ 1.0 Exchange Rate "
        Me.lblExchangeRate.Visible = False
        '
        'cboBenCCY
        '
        Me.cboBenCCY.FormattingEnabled = True
        Me.cboBenCCY.Location = New System.Drawing.Point(247, 55)
        Me.cboBenCCY.Name = "cboBenCCY"
        Me.cboBenCCY.Size = New System.Drawing.Size(84, 21)
        Me.cboBenCCY.TabIndex = 49
        Me.cboBenCCY.Visible = False
        '
        'lblBenCCY
        '
        Me.lblBenCCY.AutoSize = True
        Me.lblBenCCY.Location = New System.Drawing.Point(198, 58)
        Me.lblBenCCY.Name = "lblBenCCY"
        Me.lblBenCCY.Size = New System.Drawing.Size(43, 13)
        Me.lblBenCCY.TabIndex = 50
        Me.lblBenCCY.Text = "to CCY:"
        Me.lblBenCCY.Visible = False
        '
        'PicPayType
        '
        Me.PicPayType.Location = New System.Drawing.Point(337, 29)
        Me.PicPayType.Name = "PicPayType"
        Me.PicPayType.Size = New System.Drawing.Size(17, 18)
        Me.PicPayType.TabIndex = 48
        Me.PicPayType.TabStop = False
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Location = New System.Drawing.Point(305, 13)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 39
        '
        'lblportfolio
        '
        Me.lblportfolio.AutoSize = True
        Me.lblportfolio.Location = New System.Drawing.Point(361, 34)
        Me.lblportfolio.Name = "lblportfolio"
        Me.lblportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblportfolio.TabIndex = 38
        Me.lblportfolio.Text = "Portfolio:"
        '
        'CboPortfolio
        '
        Me.CboPortfolio.FormattingEnabled = True
        Me.CboPortfolio.Location = New System.Drawing.Point(415, 29)
        Me.CboPortfolio.Name = "CboPortfolio"
        Me.CboPortfolio.Size = New System.Drawing.Size(321, 21)
        Me.CboPortfolio.TabIndex = 1
        '
        'CboCCY
        '
        Me.CboCCY.FormattingEnabled = True
        Me.CboCCY.Location = New System.Drawing.Point(79, 55)
        Me.CboCCY.Name = "CboCCY"
        Me.CboCCY.Size = New System.Drawing.Size(84, 21)
        Me.CboCCY.TabIndex = 2
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(42, 58)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 31
        Me.lblCCY.Text = "CCY:"
        '
        'txtComments
        '
        Me.txtComments.AcceptsReturn = True
        Me.txtComments.AcceptsTab = True
        Me.txtComments.Location = New System.Drawing.Point(751, 28)
        Me.txtComments.Multiline = True
        Me.txtComments.Name = "txtComments"
        Me.txtComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtComments.Size = New System.Drawing.Size(813, 70)
        Me.txtComments.TabIndex = 32
        '
        'lblcomments
        '
        Me.lblcomments.AutoSize = True
        Me.lblcomments.Location = New System.Drawing.Point(748, 12)
        Me.lblcomments.Name = "lblcomments"
        Me.lblcomments.Size = New System.Drawing.Size(59, 13)
        Me.lblcomments.TabIndex = 16
        Me.lblcomments.Text = "Comments:"
        '
        'lblAmount
        '
        Me.lblAmount.AutoSize = True
        Me.lblAmount.Location = New System.Drawing.Point(363, 58)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(46, 13)
        Me.lblAmount.TabIndex = 15
        Me.lblAmount.Text = "Amount:"
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(415, 55)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(106, 20)
        Me.txtAmount.TabIndex = 3
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTransDate
        '
        Me.lblTransDate.AutoSize = True
        Me.lblTransDate.Location = New System.Drawing.Point(9, 82)
        Me.lblTransDate.Name = "lblTransDate"
        Me.lblTransDate.Size = New System.Drawing.Size(63, 13)
        Me.lblTransDate.TabIndex = 10
        Me.lblTransDate.Text = "Trans Date:"
        '
        'dtTransDate
        '
        Me.dtTransDate.Location = New System.Drawing.Point(79, 82)
        Me.dtTransDate.Name = "dtTransDate"
        Me.dtTransDate.Size = New System.Drawing.Size(128, 20)
        Me.dtTransDate.TabIndex = 4
        '
        'lblSettleDate
        '
        Me.lblSettleDate.AutoSize = True
        Me.lblSettleDate.Location = New System.Drawing.Point(346, 85)
        Me.lblSettleDate.Name = "lblSettleDate"
        Me.lblSettleDate.Size = New System.Drawing.Size(63, 13)
        Me.lblSettleDate.TabIndex = 8
        Me.lblSettleDate.Text = "Settle Date:"
        '
        'dtSettleDate
        '
        Me.dtSettleDate.Location = New System.Drawing.Point(415, 82)
        Me.dtSettleDate.Name = "dtSettleDate"
        Me.dtSettleDate.Size = New System.Drawing.Size(128, 20)
        Me.dtSettleDate.TabIndex = 5
        '
        'LblPayType
        '
        Me.LblPayType.AutoSize = True
        Me.LblPayType.Location = New System.Drawing.Point(9, 31)
        Me.LblPayType.Name = "LblPayType"
        Me.LblPayType.Size = New System.Drawing.Size(64, 13)
        Me.LblPayType.TabIndex = 6
        Me.LblPayType.Text = "Move Type:"
        '
        'CboPayType
        '
        Me.CboPayType.FormattingEnabled = True
        Me.CboPayType.Location = New System.Drawing.Point(79, 28)
        Me.CboPayType.Name = "CboPayType"
        Me.CboPayType.Size = New System.Drawing.Size(252, 21)
        Me.CboPayType.TabIndex = 0
        '
        'lblOrderBalancePostInc
        '
        Me.lblOrderBalancePostInc.AutoSize = True
        Me.lblOrderBalancePostInc.Location = New System.Drawing.Point(256, 246)
        Me.lblOrderBalancePostInc.Name = "lblOrderBalancePostInc"
        Me.lblOrderBalancePostInc.Size = New System.Drawing.Size(140, 13)
        Me.lblOrderBalancePostInc.TabIndex = 50
        Me.lblOrderBalancePostInc.Text = "Post Balance (inc. pending):"
        '
        'txtOrderBalanceInc
        '
        Me.txtOrderBalanceInc.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtOrderBalanceInc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOrderBalanceInc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBalanceInc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtOrderBalanceInc.Location = New System.Drawing.Point(402, 224)
        Me.txtOrderBalanceInc.Name = "txtOrderBalanceInc"
        Me.txtOrderBalanceInc.ReadOnly = True
        Me.txtOrderBalanceInc.Size = New System.Drawing.Size(101, 13)
        Me.txtOrderBalanceInc.TabIndex = 49
        Me.txtOrderBalanceInc.TabStop = False
        Me.txtOrderBalanceInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOrderAccount
        '
        Me.lblOrderAccount.AutoSize = True
        Me.lblOrderAccount.Location = New System.Drawing.Point(11, 44)
        Me.lblOrderAccount.Name = "lblOrderAccount"
        Me.lblOrderAccount.Size = New System.Drawing.Size(50, 13)
        Me.lblOrderAccount.TabIndex = 33
        Me.lblOrderAccount.Text = "Account:"
        '
        'BeneficiaryGroupBox
        '
        Me.BeneficiaryGroupBox.Controls.Add(Me.TabBeneficiary)
        Me.BeneficiaryGroupBox.Location = New System.Drawing.Point(18, 7)
        Me.BeneficiaryGroupBox.Name = "BeneficiaryGroupBox"
        Me.BeneficiaryGroupBox.Size = New System.Drawing.Size(526, 332)
        Me.BeneficiaryGroupBox.TabIndex = 10
        Me.BeneficiaryGroupBox.TabStop = False
        Me.BeneficiaryGroupBox.Text = "Beneficiary Details"
        '
        'TabBeneficiary
        '
        Me.TabBeneficiary.Controls.Add(Me.MainTab)
        Me.TabBeneficiary.Controls.Add(Me.BankDetailsTab)
        Me.TabBeneficiary.Location = New System.Drawing.Point(6, 19)
        Me.TabBeneficiary.Name = "TabBeneficiary"
        Me.TabBeneficiary.SelectedIndex = 0
        Me.TabBeneficiary.Size = New System.Drawing.Size(514, 307)
        Me.TabBeneficiary.TabIndex = 67
        '
        'MainTab
        '
        Me.MainTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.MainTab.Controls.Add(Me.ChkBenTemplate)
        Me.MainTab.Controls.Add(Me.ChkBenTemplateEdit)
        Me.MainTab.Controls.Add(Me.cboBenType)
        Me.MainTab.Controls.Add(Me.lblbenType)
        Me.MainTab.Controls.Add(Me.cboBenCountry)
        Me.MainTab.Controls.Add(Me.lblBenCountry)
        Me.MainTab.Controls.Add(Me.lblBenCounty)
        Me.MainTab.Controls.Add(Me.txtbencounty)
        Me.MainTab.Controls.Add(Me.txtBenZip)
        Me.MainTab.Controls.Add(Me.txtBenAddress1)
        Me.MainTab.Controls.Add(Me.txtBenAddress2)
        Me.MainTab.Controls.Add(Me.txtbensurname)
        Me.MainTab.Controls.Add(Me.txtbenmiddlename)
        Me.MainTab.Controls.Add(Me.txtbenfirstname)
        Me.MainTab.Controls.Add(Me.txtBenRef)
        Me.MainTab.Controls.Add(Me.txtBenBalancePostInc)
        Me.MainTab.Controls.Add(Me.txtBenBalanceInc)
        Me.MainTab.Controls.Add(Me.txtBenBalance)
        Me.MainTab.Controls.Add(Me.lblBenAddress1)
        Me.MainTab.Controls.Add(Me.lblBenAddress2)
        Me.MainTab.Controls.Add(Me.lblBenZip)
        Me.MainTab.Controls.Add(Me.cbobenrelationship)
        Me.MainTab.Controls.Add(Me.lblBenRelationship)
        Me.MainTab.Controls.Add(Me.lblBenSurname)
        Me.MainTab.Controls.Add(Me.lblBenMiddlename)
        Me.MainTab.Controls.Add(Me.lblBenFirstname)
        Me.MainTab.Controls.Add(Me.lblBenBalancePostInc)
        Me.MainTab.Controls.Add(Me.lblBenBalanceInc)
        Me.MainTab.Controls.Add(Me.lblBenName)
        Me.MainTab.Controls.Add(Me.lblBenRef)
        Me.MainTab.Controls.Add(Me.CboBenName)
        Me.MainTab.Controls.Add(Me.CboBenAccount)
        Me.MainTab.Controls.Add(Me.lblBenAccount)
        Me.MainTab.Controls.Add(Me.lblBenBalance)
        Me.MainTab.Location = New System.Drawing.Point(4, 22)
        Me.MainTab.Name = "MainTab"
        Me.MainTab.Padding = New System.Windows.Forms.Padding(3)
        Me.MainTab.Size = New System.Drawing.Size(506, 281)
        Me.MainTab.TabIndex = 4
        Me.MainTab.Text = "Main"
        '
        'ChkBenTemplate
        '
        Me.ChkBenTemplate.AutoSize = True
        Me.ChkBenTemplate.Location = New System.Drawing.Point(65, 6)
        Me.ChkBenTemplate.Name = "ChkBenTemplate"
        Me.ChkBenTemplate.Size = New System.Drawing.Size(205, 17)
        Me.ChkBenTemplate.TabIndex = 152
        Me.ChkBenTemplate.Text = "Tick to save as a beneficiary template"
        Me.ChkBenTemplate.UseVisualStyleBackColor = True
        Me.ChkBenTemplate.Visible = False
        '
        'ChkBenTemplateEdit
        '
        Me.ChkBenTemplateEdit.AutoSize = True
        Me.ChkBenTemplateEdit.Location = New System.Drawing.Point(294, 6)
        Me.ChkBenTemplateEdit.Name = "ChkBenTemplateEdit"
        Me.ChkBenTemplateEdit.Size = New System.Drawing.Size(173, 17)
        Me.ChkBenTemplateEdit.TabIndex = 153
        Me.ChkBenTemplateEdit.Text = "Edit populated template details "
        Me.ChkBenTemplateEdit.UseVisualStyleBackColor = True
        Me.ChkBenTemplateEdit.Visible = False
        '
        'cboBenType
        '
        Me.cboBenType.BackColor = System.Drawing.Color.White
        Me.cboBenType.FormattingEnabled = True
        Me.cboBenType.Location = New System.Drawing.Point(65, 86)
        Me.cboBenType.Name = "cboBenType"
        Me.cboBenType.Size = New System.Drawing.Size(184, 21)
        Me.cboBenType.TabIndex = 150
        Me.cboBenType.Tag = "ControlAddressBen"
        '
        'lblbenType
        '
        Me.lblbenType.AutoSize = True
        Me.lblbenType.Location = New System.Drawing.Point(25, 92)
        Me.lblbenType.Name = "lblbenType"
        Me.lblbenType.Size = New System.Drawing.Size(34, 13)
        Me.lblbenType.TabIndex = 151
        Me.lblbenType.Text = "Type:"
        '
        'cboBenCountry
        '
        Me.cboBenCountry.BackColor = System.Drawing.Color.White
        Me.cboBenCountry.FormattingEnabled = True
        Me.cboBenCountry.Location = New System.Drawing.Point(65, 220)
        Me.cboBenCountry.Name = "cboBenCountry"
        Me.cboBenCountry.Size = New System.Drawing.Size(174, 21)
        Me.cboBenCountry.TabIndex = 148
        Me.cboBenCountry.Tag = "ControlAddressBen"
        '
        'lblBenCountry
        '
        Me.lblBenCountry.AutoSize = True
        Me.lblBenCountry.Location = New System.Drawing.Point(13, 223)
        Me.lblBenCountry.Name = "lblBenCountry"
        Me.lblBenCountry.Size = New System.Drawing.Size(46, 13)
        Me.lblBenCountry.TabIndex = 149
        Me.lblBenCountry.Text = "Country:"
        '
        'lblBenCounty
        '
        Me.lblBenCounty.AutoSize = True
        Me.lblBenCounty.Location = New System.Drawing.Point(16, 196)
        Me.lblBenCounty.Name = "lblBenCounty"
        Me.lblBenCounty.Size = New System.Drawing.Size(43, 13)
        Me.lblBenCounty.TabIndex = 147
        Me.lblBenCounty.Text = "County:"
        '
        'txtbencounty
        '
        Me.txtbencounty.Location = New System.Drawing.Point(65, 193)
        Me.txtbencounty.Name = "txtbencounty"
        Me.txtbencounty.Size = New System.Drawing.Size(173, 20)
        Me.txtbencounty.TabIndex = 146
        Me.txtbencounty.Tag = "ControlAddressBen"
        '
        'txtBenZip
        '
        Me.txtBenZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenZip.Location = New System.Drawing.Point(302, 193)
        Me.txtBenZip.Name = "txtBenZip"
        Me.txtBenZip.Size = New System.Drawing.Size(78, 20)
        Me.txtBenZip.TabIndex = 142
        Me.txtBenZip.Tag = "ControlAddressBen"
        '
        'txtBenAddress1
        '
        Me.txtBenAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenAddress1.Location = New System.Drawing.Point(65, 167)
        Me.txtBenAddress1.Name = "txtBenAddress1"
        Me.txtBenAddress1.Size = New System.Drawing.Size(173, 20)
        Me.txtBenAddress1.TabIndex = 140
        Me.txtBenAddress1.Tag = "ControlAddressBen"
        '
        'txtBenAddress2
        '
        Me.txtBenAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenAddress2.Location = New System.Drawing.Point(302, 167)
        Me.txtBenAddress2.Name = "txtBenAddress2"
        Me.txtBenAddress2.Size = New System.Drawing.Size(197, 20)
        Me.txtBenAddress2.TabIndex = 141
        Me.txtBenAddress2.Tag = "ControlAddressBen"
        '
        'txtbensurname
        '
        Me.txtbensurname.Location = New System.Drawing.Point(65, 142)
        Me.txtbensurname.Name = "txtbensurname"
        Me.txtbensurname.Size = New System.Drawing.Size(434, 20)
        Me.txtbensurname.TabIndex = 137
        Me.txtbensurname.Tag = "ControlAddressBen"
        '
        'txtbenmiddlename
        '
        Me.txtbenmiddlename.Location = New System.Drawing.Point(328, 116)
        Me.txtbenmiddlename.Name = "txtbenmiddlename"
        Me.txtbenmiddlename.Size = New System.Drawing.Size(171, 20)
        Me.txtbenmiddlename.TabIndex = 135
        Me.txtbenmiddlename.Tag = "ControlAddressBen"
        '
        'txtbenfirstname
        '
        Me.txtbenfirstname.Location = New System.Drawing.Point(65, 116)
        Me.txtbenfirstname.Name = "txtbenfirstname"
        Me.txtbenfirstname.Size = New System.Drawing.Size(184, 20)
        Me.txtbenfirstname.TabIndex = 133
        Me.txtbenfirstname.Tag = "ControlAddressBen"
        '
        'txtBenRef
        '
        Me.txtBenRef.Enabled = False
        Me.txtBenRef.Location = New System.Drawing.Point(370, 58)
        Me.txtBenRef.Name = "txtBenRef"
        Me.txtBenRef.Size = New System.Drawing.Size(129, 20)
        Me.txtBenRef.TabIndex = 18
        '
        'txtBenBalancePostInc
        '
        Me.txtBenBalancePostInc.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtBenBalancePostInc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBenBalancePostInc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenBalancePostInc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtBenBalancePostInc.Location = New System.Drawing.Point(397, 257)
        Me.txtBenBalancePostInc.Name = "txtBenBalancePostInc"
        Me.txtBenBalancePostInc.ReadOnly = True
        Me.txtBenBalancePostInc.Size = New System.Drawing.Size(101, 13)
        Me.txtBenBalancePostInc.TabIndex = 79
        Me.txtBenBalancePostInc.TabStop = False
        Me.txtBenBalancePostInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBenBalanceInc
        '
        Me.txtBenBalanceInc.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtBenBalanceInc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBenBalanceInc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenBalanceInc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtBenBalanceInc.Location = New System.Drawing.Point(397, 232)
        Me.txtBenBalanceInc.Name = "txtBenBalanceInc"
        Me.txtBenBalanceInc.ReadOnly = True
        Me.txtBenBalanceInc.Size = New System.Drawing.Size(101, 13)
        Me.txtBenBalanceInc.TabIndex = 81
        Me.txtBenBalanceInc.TabStop = False
        Me.txtBenBalanceInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBenBalance
        '
        Me.txtBenBalance.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtBenBalance.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBenBalance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBenBalance.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtBenBalance.Location = New System.Drawing.Point(398, 35)
        Me.txtBenBalance.Name = "txtBenBalance"
        Me.txtBenBalance.ReadOnly = True
        Me.txtBenBalance.Size = New System.Drawing.Size(101, 13)
        Me.txtBenBalance.TabIndex = 66
        Me.txtBenBalance.TabStop = False
        Me.txtBenBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBenAddress1
        '
        Me.lblBenAddress1.AutoSize = True
        Me.lblBenAddress1.Location = New System.Drawing.Point(2, 170)
        Me.lblBenAddress1.Name = "lblBenAddress1"
        Me.lblBenAddress1.Size = New System.Drawing.Size(57, 13)
        Me.lblBenAddress1.TabIndex = 143
        Me.lblBenAddress1.Text = "Address 1:"
        '
        'lblBenAddress2
        '
        Me.lblBenAddress2.AutoSize = True
        Me.lblBenAddress2.Location = New System.Drawing.Point(269, 170)
        Me.lblBenAddress2.Name = "lblBenAddress2"
        Me.lblBenAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblBenAddress2.TabIndex = 144
        Me.lblBenAddress2.Text = "City:"
        '
        'lblBenZip
        '
        Me.lblBenZip.AutoSize = True
        Me.lblBenZip.Location = New System.Drawing.Point(243, 196)
        Me.lblBenZip.Name = "lblBenZip"
        Me.lblBenZip.Size = New System.Drawing.Size(51, 13)
        Me.lblBenZip.TabIndex = 145
        Me.lblBenZip.Text = "Post/Zip:"
        '
        'cbobenrelationship
        '
        Me.cbobenrelationship.BackColor = System.Drawing.Color.White
        Me.cbobenrelationship.FormattingEnabled = True
        Me.cbobenrelationship.Location = New System.Drawing.Point(328, 86)
        Me.cbobenrelationship.Name = "cbobenrelationship"
        Me.cbobenrelationship.Size = New System.Drawing.Size(173, 21)
        Me.cbobenrelationship.TabIndex = 138
        Me.cbobenrelationship.Tag = "ControlAddressBen"
        '
        'lblBenRelationship
        '
        Me.lblBenRelationship.AutoSize = True
        Me.lblBenRelationship.Location = New System.Drawing.Point(254, 89)
        Me.lblBenRelationship.Name = "lblBenRelationship"
        Me.lblBenRelationship.Size = New System.Drawing.Size(68, 13)
        Me.lblBenRelationship.TabIndex = 139
        Me.lblBenRelationship.Text = "Relationship:"
        '
        'lblBenSurname
        '
        Me.lblBenSurname.AutoSize = True
        Me.lblBenSurname.Location = New System.Drawing.Point(7, 145)
        Me.lblBenSurname.Name = "lblBenSurname"
        Me.lblBenSurname.Size = New System.Drawing.Size(52, 13)
        Me.lblBenSurname.TabIndex = 136
        Me.lblBenSurname.Text = "Surname:"
        '
        'lblBenMiddlename
        '
        Me.lblBenMiddlename.AutoSize = True
        Me.lblBenMiddlename.Location = New System.Drawing.Point(250, 119)
        Me.lblBenMiddlename.Name = "lblBenMiddlename"
        Me.lblBenMiddlename.Size = New System.Drawing.Size(72, 13)
        Me.lblBenMiddlename.TabIndex = 134
        Me.lblBenMiddlename.Text = "Middle Name:"
        '
        'lblBenFirstname
        '
        Me.lblBenFirstname.AutoSize = True
        Me.lblBenFirstname.Location = New System.Drawing.Point(2, 118)
        Me.lblBenFirstname.Name = "lblBenFirstname"
        Me.lblBenFirstname.Size = New System.Drawing.Size(57, 13)
        Me.lblBenFirstname.TabIndex = 132
        Me.lblBenFirstname.Text = "First Name"
        '
        'lblBenBalancePostInc
        '
        Me.lblBenBalancePostInc.AutoSize = True
        Me.lblBenBalancePostInc.Location = New System.Drawing.Point(252, 256)
        Me.lblBenBalancePostInc.Name = "lblBenBalancePostInc"
        Me.lblBenBalancePostInc.Size = New System.Drawing.Size(140, 13)
        Me.lblBenBalancePostInc.TabIndex = 80
        Me.lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
        '
        'lblBenBalanceInc
        '
        Me.lblBenBalanceInc.AutoSize = True
        Me.lblBenBalanceInc.Location = New System.Drawing.Point(276, 232)
        Me.lblBenBalanceInc.Name = "lblBenBalanceInc"
        Me.lblBenBalanceInc.Size = New System.Drawing.Size(116, 13)
        Me.lblBenBalanceInc.TabIndex = 82
        Me.lblBenBalanceInc.Text = "Balance (inc. pending):"
        '
        'lblBenName
        '
        Me.lblBenName.AutoSize = True
        Me.lblBenName.Location = New System.Drawing.Point(9, 60)
        Me.lblBenName.Name = "lblBenName"
        Me.lblBenName.Size = New System.Drawing.Size(38, 13)
        Me.lblBenName.TabIndex = 34
        Me.lblBenName.Text = "Name:"
        '
        'lblBenRef
        '
        Me.lblBenRef.AutoSize = True
        Me.lblBenRef.Location = New System.Drawing.Point(343, 61)
        Me.lblBenRef.Name = "lblBenRef"
        Me.lblBenRef.Size = New System.Drawing.Size(27, 13)
        Me.lblBenRef.TabIndex = 35
        Me.lblBenRef.Text = "Ref:"
        '
        'CboBenName
        '
        Me.CboBenName.FormattingEnabled = True
        Me.CboBenName.Location = New System.Drawing.Point(65, 57)
        Me.CboBenName.Name = "CboBenName"
        Me.CboBenName.Size = New System.Drawing.Size(262, 21)
        Me.CboBenName.TabIndex = 17
        Me.CboBenName.Tag = ""
        '
        'CboBenAccount
        '
        Me.CboBenAccount.FormattingEnabled = True
        Me.CboBenAccount.Location = New System.Drawing.Point(65, 29)
        Me.CboBenAccount.Name = "CboBenAccount"
        Me.CboBenAccount.Size = New System.Drawing.Size(262, 21)
        Me.CboBenAccount.TabIndex = 16
        '
        'lblBenAccount
        '
        Me.lblBenAccount.AutoSize = True
        Me.lblBenAccount.Location = New System.Drawing.Point(9, 32)
        Me.lblBenAccount.Name = "lblBenAccount"
        Me.lblBenAccount.Size = New System.Drawing.Size(50, 13)
        Me.lblBenAccount.TabIndex = 65
        Me.lblBenAccount.Text = "Account:"
        '
        'lblBenBalance
        '
        Me.lblBenBalance.AutoSize = True
        Me.lblBenBalance.Location = New System.Drawing.Point(343, 35)
        Me.lblBenBalance.Name = "lblBenBalance"
        Me.lblBenBalance.Size = New System.Drawing.Size(49, 13)
        Me.lblBenBalance.TabIndex = 67
        Me.lblBenBalance.Text = "Balance:"
        '
        'BankDetailsTab
        '
        Me.BankDetailsTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.BankDetailsTab.Controls.Add(Me.lblbenmsg3)
        Me.BankDetailsTab.Controls.Add(Me.lblbenmsg2)
        Me.BankDetailsTab.Controls.Add(Me.lblbenmsg1)
        Me.BankDetailsTab.Controls.Add(Me.txtBenBIK)
        Me.BankDetailsTab.Controls.Add(Me.txtBenINN)
        Me.BankDetailsTab.Controls.Add(Me.txtBenSwift)
        Me.BankDetailsTab.Controls.Add(Me.txtBenIBAN)
        Me.BankDetailsTab.Controls.Add(Me.txtBenOther1)
        Me.BankDetailsTab.Controls.Add(Me.lblbenBIK)
        Me.BankDetailsTab.Controls.Add(Me.lblBenSwift)
        Me.BankDetailsTab.Controls.Add(Me.lblBenIBAN)
        Me.BankDetailsTab.Controls.Add(Me.lblbenINN)
        Me.BankDetailsTab.Controls.Add(Me.ChkBenSendRef)
        Me.BankDetailsTab.Controls.Add(Me.ChkBenUrgent)
        Me.BankDetailsTab.Controls.Add(Me.lblBenOther1)
        Me.BankDetailsTab.Controls.Add(Me.PicBenSwift)
        Me.BankDetailsTab.Controls.Add(Me.PicBenIBAN)
        Me.BankDetailsTab.Location = New System.Drawing.Point(4, 22)
        Me.BankDetailsTab.Name = "BankDetailsTab"
        Me.BankDetailsTab.Size = New System.Drawing.Size(506, 281)
        Me.BankDetailsTab.TabIndex = 2
        Me.BankDetailsTab.Text = "Bank Details"
        '
        'lblbenmsg3
        '
        Me.lblbenmsg3.AutoSize = True
        Me.lblbenmsg3.ForeColor = System.Drawing.Color.Black
        Me.lblbenmsg3.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblbenmsg3.Location = New System.Drawing.Point(7, 173)
        Me.lblbenmsg3.Name = "lblbenmsg3"
        Me.lblbenmsg3.Size = New System.Drawing.Size(91, 13)
        Me.lblbenmsg3.TabIndex = 90
        Me.lblbenmsg3.Text = "Beneficiary Msg 3"
        '
        'lblbenmsg2
        '
        Me.lblbenmsg2.AutoSize = True
        Me.lblbenmsg2.ForeColor = System.Drawing.Color.Black
        Me.lblbenmsg2.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblbenmsg2.Location = New System.Drawing.Point(7, 148)
        Me.lblbenmsg2.Name = "lblbenmsg2"
        Me.lblbenmsg2.Size = New System.Drawing.Size(91, 13)
        Me.lblbenmsg2.TabIndex = 89
        Me.lblbenmsg2.Text = "Beneficiary Msg 2"
        '
        'lblbenmsg1
        '
        Me.lblbenmsg1.AutoSize = True
        Me.lblbenmsg1.ForeColor = System.Drawing.Color.Black
        Me.lblbenmsg1.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblbenmsg1.Location = New System.Drawing.Point(7, 124)
        Me.lblbenmsg1.Name = "lblbenmsg1"
        Me.lblbenmsg1.Size = New System.Drawing.Size(91, 13)
        Me.lblbenmsg1.TabIndex = 88
        Me.lblbenmsg1.Text = "Beneficiary Msg 1"
        '
        'txtBenBIK
        '
        Me.txtBenBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBIK.Location = New System.Drawing.Point(59, 83)
        Me.txtBenBIK.Name = "txtBenBIK"
        Me.txtBenBIK.Size = New System.Drawing.Size(187, 20)
        Me.txtBenBIK.TabIndex = 86
        Me.txtBenBIK.Tag = "ControlAddressIntermediary"
        Me.txtBenBIK.Visible = False
        '
        'txtBenINN
        '
        Me.txtBenINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenINN.Location = New System.Drawing.Point(296, 83)
        Me.txtBenINN.Name = "txtBenINN"
        Me.txtBenINN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenINN.TabIndex = 85
        Me.txtBenINN.Tag = "ControlAddressIntermediary"
        '
        'txtBenSwift
        '
        Me.txtBenSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSwift.Location = New System.Drawing.Point(296, 9)
        Me.txtBenSwift.Name = "txtBenSwift"
        Me.txtBenSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtBenSwift.TabIndex = 22
        Me.txtBenSwift.Tag = "ControlAddressBen"
        '
        'txtBenIBAN
        '
        Me.txtBenIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenIBAN.Location = New System.Drawing.Point(296, 34)
        Me.txtBenIBAN.Name = "txtBenIBAN"
        Me.txtBenIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenIBAN.TabIndex = 23
        Me.txtBenIBAN.Tag = "ControlAddressBen"
        '
        'txtBenOther1
        '
        Me.txtBenOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenOther1.Location = New System.Drawing.Point(296, 59)
        Me.txtBenOther1.Name = "txtBenOther1"
        Me.txtBenOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtBenOther1.TabIndex = 24
        Me.txtBenOther1.Tag = "ControlAddressBen"
        '
        'lblbenBIK
        '
        Me.lblbenBIK.AutoSize = True
        Me.lblbenBIK.Location = New System.Drawing.Point(2, 88)
        Me.lblbenBIK.Name = "lblbenBIK"
        Me.lblbenBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblbenBIK.TabIndex = 87
        Me.lblbenBIK.Text = "BIK.ACC:"
        Me.lblbenBIK.Visible = False
        '
        'lblBenSwift
        '
        Me.lblBenSwift.AutoSize = True
        Me.lblBenSwift.Location = New System.Drawing.Point(255, 12)
        Me.lblBenSwift.Name = "lblBenSwift"
        Me.lblBenSwift.Size = New System.Drawing.Size(33, 13)
        Me.lblBenSwift.TabIndex = 58
        Me.lblBenSwift.Text = "Swift:"
        '
        'lblBenIBAN
        '
        Me.lblBenIBAN.AutoSize = True
        Me.lblBenIBAN.Location = New System.Drawing.Point(255, 37)
        Me.lblBenIBAN.Name = "lblBenIBAN"
        Me.lblBenIBAN.Size = New System.Drawing.Size(35, 13)
        Me.lblBenIBAN.TabIndex = 59
        Me.lblBenIBAN.Text = "IBAN:"
        '
        'lblbenINN
        '
        Me.lblbenINN.AutoSize = True
        Me.lblbenINN.Location = New System.Drawing.Point(262, 88)
        Me.lblbenINN.Name = "lblbenINN"
        Me.lblbenINN.Size = New System.Drawing.Size(29, 13)
        Me.lblbenINN.TabIndex = 84
        Me.lblbenINN.Text = "INN:"
        Me.lblbenINN.Visible = False
        '
        'ChkBenSendRef
        '
        Me.ChkBenSendRef.AutoSize = True
        Me.ChkBenSendRef.Location = New System.Drawing.Point(10, 34)
        Me.ChkBenSendRef.Name = "ChkBenSendRef"
        Me.ChkBenSendRef.Size = New System.Drawing.Size(222, 17)
        Me.ChkBenSendRef.TabIndex = 78
        Me.ChkBenSendRef.Text = "Tick to send our ref to 3rd party in SWIFT"
        Me.ChkBenSendRef.UseVisualStyleBackColor = True
        Me.ChkBenSendRef.Visible = False
        '
        'ChkBenUrgent
        '
        Me.ChkBenUrgent.AutoSize = True
        Me.ChkBenUrgent.Location = New System.Drawing.Point(10, 12)
        Me.ChkBenUrgent.Name = "ChkBenUrgent"
        Me.ChkBenUrgent.Size = New System.Drawing.Size(162, 17)
        Me.ChkBenUrgent.TabIndex = 77
        Me.ChkBenUrgent.Text = "Tick to send SWIFT urgently"
        Me.ChkBenUrgent.UseVisualStyleBackColor = True
        Me.ChkBenUrgent.Visible = False
        '
        'lblBenOther1
        '
        Me.lblBenOther1.AutoSize = True
        Me.lblBenOther1.Location = New System.Drawing.Point(254, 62)
        Me.lblBenOther1.Name = "lblBenOther1"
        Me.lblBenOther1.Size = New System.Drawing.Size(36, 13)
        Me.lblBenOther1.TabIndex = 60
        Me.lblBenOther1.Text = "Other:"
        '
        'PicBenSwift
        '
        Me.PicBenSwift.Location = New System.Drawing.Point(475, 12)
        Me.PicBenSwift.Name = "PicBenSwift"
        Me.PicBenSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicBenSwift.TabIndex = 71
        Me.PicBenSwift.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenSwift, "X verifies that this swift has not been found in IMS. You can still import with a" &
        " valid BIC code not found in IMS")
        '
        'PicBenIBAN
        '
        Me.PicBenIBAN.Location = New System.Drawing.Point(475, 35)
        Me.PicBenIBAN.Name = "PicBenIBAN"
        Me.PicBenIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicBenIBAN.TabIndex = 72
        Me.PicBenIBAN.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenIBAN, "X verifies that this is not a valid IBAN. You can still import if you add an acco" &
        "unt number")
        '
        'OrderGroupBox
        '
        Me.OrderGroupBox.Controls.Add(Me.cboOrderCountry)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderCountry)
        Me.OrderGroupBox.Controls.Add(Me.txtIMSRef)
        Me.OrderGroupBox.Controls.Add(Me.lblIMSRef)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderVOCode)
        Me.OrderGroupBox.Controls.Add(Me.cboOrderVOCode)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg8)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderBalancePostInc)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderBalanceInc)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderBalancePostInc)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderBalanceInc)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg7)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg6)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg5)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg4)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg3)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg2)
        Me.OrderGroupBox.Controls.Add(Me.lblordermsg1)
        Me.OrderGroupBox.Controls.Add(Me.PicOrderIBAN)
        Me.OrderGroupBox.Controls.Add(Me.PicOrderSwift)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderBalance)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderBalance)
        Me.OrderGroupBox.Controls.Add(Me.CboOrderAccount)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderOther1)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderAccount)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderIBAN)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderSwift)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderZip)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderAddress2)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderAddress1)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderZip)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderAddress2)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderAddress1)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderOther1)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderIBAN)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderSwift)
        Me.OrderGroupBox.Controls.Add(Me.CboOrderName)
        Me.OrderGroupBox.Controls.Add(Me.txtOrderRef)
        Me.OrderGroupBox.Controls.Add(Me.lblorderref)
        Me.OrderGroupBox.Controls.Add(Me.lblOrderName)
        Me.OrderGroupBox.Location = New System.Drawing.Point(18, 7)
        Me.OrderGroupBox.Name = "OrderGroupBox"
        Me.OrderGroupBox.Size = New System.Drawing.Size(1063, 325)
        Me.OrderGroupBox.TabIndex = 11
        Me.OrderGroupBox.TabStop = False
        Me.OrderGroupBox.Text = "Order Details"
        '
        'cboOrderCountry
        '
        Me.cboOrderCountry.BackColor = System.Drawing.Color.White
        Me.cboOrderCountry.FormattingEnabled = True
        Me.cboOrderCountry.Location = New System.Drawing.Point(67, 181)
        Me.cboOrderCountry.Name = "cboOrderCountry"
        Me.cboOrderCountry.Size = New System.Drawing.Size(174, 21)
        Me.cboOrderCountry.TabIndex = 150
        Me.cboOrderCountry.Tag = "ControlAddress"
        '
        'lblOrderCountry
        '
        Me.lblOrderCountry.AutoSize = True
        Me.lblOrderCountry.Location = New System.Drawing.Point(15, 184)
        Me.lblOrderCountry.Name = "lblOrderCountry"
        Me.lblOrderCountry.Size = New System.Drawing.Size(46, 13)
        Me.lblOrderCountry.TabIndex = 151
        Me.lblOrderCountry.Text = "Country:"
        '
        'txtIMSRef
        '
        Me.txtIMSRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtIMSRef.Location = New System.Drawing.Point(316, 182)
        Me.txtIMSRef.Name = "txtIMSRef"
        Me.txtIMSRef.Size = New System.Drawing.Size(173, 20)
        Me.txtIMSRef.TabIndex = 88
        Me.txtIMSRef.Visible = False
        '
        'lblIMSRef
        '
        Me.lblIMSRef.AutoSize = True
        Me.lblIMSRef.Location = New System.Drawing.Point(243, 185)
        Me.lblIMSRef.Name = "lblIMSRef"
        Me.lblIMSRef.Size = New System.Drawing.Size(67, 13)
        Me.lblIMSRef.TabIndex = 89
        Me.lblIMSRef.Text = "Related Ref:"
        Me.lblIMSRef.Visible = False
        '
        'lblOrderVOCode
        '
        Me.lblOrderVOCode.AutoSize = True
        Me.lblOrderVOCode.Location = New System.Drawing.Point(10, 211)
        Me.lblOrderVOCode.Name = "lblOrderVOCode"
        Me.lblOrderVOCode.Size = New System.Drawing.Size(53, 13)
        Me.lblOrderVOCode.TabIndex = 87
        Me.lblOrderVOCode.Text = "VO Code:"
        Me.lblOrderVOCode.Visible = False
        '
        'cboOrderVOCode
        '
        Me.cboOrderVOCode.FormattingEnabled = True
        Me.cboOrderVOCode.Location = New System.Drawing.Point(68, 208)
        Me.cboOrderVOCode.Name = "cboOrderVOCode"
        Me.cboOrderVOCode.Size = New System.Drawing.Size(78, 21)
        Me.cboOrderVOCode.TabIndex = 86
        Me.cboOrderVOCode.Tag = "ControlAddressIntermediary"
        Me.cboOrderVOCode.Visible = False
        '
        'lblordermsg8
        '
        Me.lblordermsg8.AutoSize = True
        Me.lblordermsg8.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg8.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg8.Location = New System.Drawing.Point(539, 210)
        Me.lblordermsg8.Name = "lblordermsg8"
        Me.lblordermsg8.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg8.TabIndex = 59
        Me.lblordermsg8.Text = "Order Msg 8"
        '
        'txtOrderBalancePostInc
        '
        Me.txtOrderBalancePostInc.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtOrderBalancePostInc.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOrderBalancePostInc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBalancePostInc.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtOrderBalancePostInc.Location = New System.Drawing.Point(402, 246)
        Me.txtOrderBalancePostInc.Name = "txtOrderBalancePostInc"
        Me.txtOrderBalancePostInc.ReadOnly = True
        Me.txtOrderBalancePostInc.Size = New System.Drawing.Size(101, 13)
        Me.txtOrderBalancePostInc.TabIndex = 58
        Me.txtOrderBalancePostInc.TabStop = False
        Me.txtOrderBalancePostInc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOrderBalanceInc
        '
        Me.lblOrderBalanceInc.AutoSize = True
        Me.lblOrderBalanceInc.Location = New System.Drawing.Point(256, 224)
        Me.lblOrderBalanceInc.Name = "lblOrderBalanceInc"
        Me.lblOrderBalanceInc.Size = New System.Drawing.Size(116, 13)
        Me.lblOrderBalanceInc.TabIndex = 57
        Me.lblOrderBalanceInc.Text = "Balance (inc. pending):"
        '
        'lblordermsg7
        '
        Me.lblordermsg7.AutoSize = True
        Me.lblordermsg7.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg7.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg7.Location = New System.Drawing.Point(539, 184)
        Me.lblordermsg7.Name = "lblordermsg7"
        Me.lblordermsg7.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg7.TabIndex = 56
        Me.lblordermsg7.Text = "Order Msg 7"
        '
        'lblordermsg6
        '
        Me.lblordermsg6.AutoSize = True
        Me.lblordermsg6.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg6.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg6.Location = New System.Drawing.Point(539, 156)
        Me.lblordermsg6.Name = "lblordermsg6"
        Me.lblordermsg6.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg6.TabIndex = 55
        Me.lblordermsg6.Text = "Order Msg 6"
        '
        'lblordermsg5
        '
        Me.lblordermsg5.AutoSize = True
        Me.lblordermsg5.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg5.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg5.Location = New System.Drawing.Point(539, 128)
        Me.lblordermsg5.Name = "lblordermsg5"
        Me.lblordermsg5.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg5.TabIndex = 54
        Me.lblordermsg5.Text = "Order Msg 5"
        '
        'lblordermsg4
        '
        Me.lblordermsg4.AutoSize = True
        Me.lblordermsg4.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg4.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg4.Location = New System.Drawing.Point(539, 100)
        Me.lblordermsg4.Name = "lblordermsg4"
        Me.lblordermsg4.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg4.TabIndex = 53
        Me.lblordermsg4.Text = "Order Msg 4"
        '
        'lblordermsg3
        '
        Me.lblordermsg3.AutoSize = True
        Me.lblordermsg3.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg3.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg3.Location = New System.Drawing.Point(539, 72)
        Me.lblordermsg3.Name = "lblordermsg3"
        Me.lblordermsg3.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg3.TabIndex = 52
        Me.lblordermsg3.Text = "Order Msg 3"
        '
        'lblordermsg2
        '
        Me.lblordermsg2.AutoSize = True
        Me.lblordermsg2.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg2.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg2.Location = New System.Drawing.Point(539, 44)
        Me.lblordermsg2.Name = "lblordermsg2"
        Me.lblordermsg2.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg2.TabIndex = 51
        Me.lblordermsg2.Text = "Order Msg 2"
        '
        'lblordermsg1
        '
        Me.lblordermsg1.AutoSize = True
        Me.lblordermsg1.ForeColor = System.Drawing.Color.Black
        Me.lblordermsg1.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        Me.lblordermsg1.Location = New System.Drawing.Point(539, 16)
        Me.lblordermsg1.Name = "lblordermsg1"
        Me.lblordermsg1.Size = New System.Drawing.Size(65, 13)
        Me.lblordermsg1.TabIndex = 50
        Me.lblordermsg1.Text = "Order Msg 1"
        '
        'PicOrderIBAN
        '
        Me.PicOrderIBAN.Location = New System.Drawing.Point(495, 132)
        Me.PicOrderIBAN.Name = "PicOrderIBAN"
        Me.PicOrderIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicOrderIBAN.TabIndex = 48
        Me.PicOrderIBAN.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicOrderIBAN, "X verifies that this is not a valid IBAN. You can still import if you add an acco" &
        "unt number")
        '
        'PicOrderSwift
        '
        Me.PicOrderSwift.Location = New System.Drawing.Point(495, 109)
        Me.PicOrderSwift.Name = "PicOrderSwift"
        Me.PicOrderSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicOrderSwift.TabIndex = 47
        Me.PicOrderSwift.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicOrderSwift, "X verifies that this swift has not been found in IMS. You can still import with a" &
        " valid BIC code not found in IMS")
        '
        'lblOrderBalance
        '
        Me.lblOrderBalance.AutoSize = True
        Me.lblOrderBalance.Location = New System.Drawing.Point(352, 47)
        Me.lblOrderBalance.Name = "lblOrderBalance"
        Me.lblOrderBalance.Size = New System.Drawing.Size(49, 13)
        Me.lblOrderBalance.TabIndex = 36
        Me.lblOrderBalance.Text = "Balance:"
        '
        'txtOrderBalance
        '
        Me.txtOrderBalance.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtOrderBalance.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtOrderBalance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrderBalance.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtOrderBalance.Location = New System.Drawing.Point(407, 47)
        Me.txtOrderBalance.Name = "txtOrderBalance"
        Me.txtOrderBalance.ReadOnly = True
        Me.txtOrderBalance.Size = New System.Drawing.Size(101, 13)
        Me.txtOrderBalance.TabIndex = 35
        Me.txtOrderBalance.TabStop = False
        Me.txtOrderBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CboOrderAccount
        '
        Me.CboOrderAccount.FormattingEnabled = True
        Me.CboOrderAccount.Location = New System.Drawing.Point(67, 41)
        Me.CboOrderAccount.Name = "CboOrderAccount"
        Me.CboOrderAccount.Size = New System.Drawing.Size(262, 21)
        Me.CboOrderAccount.TabIndex = 7
        '
        'txtOrderOther1
        '
        Me.txtOrderOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderOther1.Location = New System.Drawing.Point(316, 156)
        Me.txtOrderOther1.Name = "txtOrderOther1"
        Me.txtOrderOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtOrderOther1.TabIndex = 15
        Me.txtOrderOther1.Tag = "ControlAddress"
        '
        'txtOrderIBAN
        '
        Me.txtOrderIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderIBAN.Location = New System.Drawing.Point(316, 131)
        Me.txtOrderIBAN.Name = "txtOrderIBAN"
        Me.txtOrderIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtOrderIBAN.TabIndex = 14
        Me.txtOrderIBAN.Tag = "ControlAddress"
        '
        'txtOrderSwift
        '
        Me.txtOrderSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderSwift.Location = New System.Drawing.Point(316, 106)
        Me.txtOrderSwift.Name = "txtOrderSwift"
        Me.txtOrderSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtOrderSwift.TabIndex = 13
        Me.txtOrderSwift.Tag = "ControlAddress"
        '
        'txtOrderZip
        '
        Me.txtOrderZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderZip.Location = New System.Drawing.Point(68, 156)
        Me.txtOrderZip.Name = "txtOrderZip"
        Me.txtOrderZip.Size = New System.Drawing.Size(78, 20)
        Me.txtOrderZip.TabIndex = 12
        Me.txtOrderZip.Tag = "ControlAddress"
        '
        'txtOrderAddress2
        '
        Me.txtOrderAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderAddress2.Location = New System.Drawing.Point(68, 130)
        Me.txtOrderAddress2.Name = "txtOrderAddress2"
        Me.txtOrderAddress2.Size = New System.Drawing.Size(173, 20)
        Me.txtOrderAddress2.TabIndex = 11
        Me.txtOrderAddress2.Tag = "ControlAddress"
        '
        'txtOrderAddress1
        '
        Me.txtOrderAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderAddress1.Location = New System.Drawing.Point(68, 106)
        Me.txtOrderAddress1.Name = "txtOrderAddress1"
        Me.txtOrderAddress1.Size = New System.Drawing.Size(173, 20)
        Me.txtOrderAddress1.TabIndex = 10
        Me.txtOrderAddress1.Tag = "ControlAddress"
        '
        'lblOrderZip
        '
        Me.lblOrderZip.AutoSize = True
        Me.lblOrderZip.Location = New System.Drawing.Point(11, 159)
        Me.lblOrderZip.Name = "lblOrderZip"
        Me.lblOrderZip.Size = New System.Drawing.Size(51, 13)
        Me.lblOrderZip.TabIndex = 33
        Me.lblOrderZip.Text = "Post/Zip:"
        '
        'lblOrderAddress2
        '
        Me.lblOrderAddress2.AutoSize = True
        Me.lblOrderAddress2.Location = New System.Drawing.Point(35, 134)
        Me.lblOrderAddress2.Name = "lblOrderAddress2"
        Me.lblOrderAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblOrderAddress2.TabIndex = 32
        Me.lblOrderAddress2.Text = "City:"
        '
        'lblOrderAddress1
        '
        Me.lblOrderAddress1.AutoSize = True
        Me.lblOrderAddress1.Location = New System.Drawing.Point(11, 109)
        Me.lblOrderAddress1.Name = "lblOrderAddress1"
        Me.lblOrderAddress1.Size = New System.Drawing.Size(57, 13)
        Me.lblOrderAddress1.TabIndex = 31
        Me.lblOrderAddress1.Text = "Address 1:"
        '
        'lblOrderOther1
        '
        Me.lblOrderOther1.AutoSize = True
        Me.lblOrderOther1.Location = New System.Drawing.Point(274, 159)
        Me.lblOrderOther1.Name = "lblOrderOther1"
        Me.lblOrderOther1.Size = New System.Drawing.Size(36, 13)
        Me.lblOrderOther1.TabIndex = 25
        Me.lblOrderOther1.Text = "Other:"
        '
        'lblOrderIBAN
        '
        Me.lblOrderIBAN.AutoSize = True
        Me.lblOrderIBAN.Location = New System.Drawing.Point(275, 134)
        Me.lblOrderIBAN.Name = "lblOrderIBAN"
        Me.lblOrderIBAN.Size = New System.Drawing.Size(35, 13)
        Me.lblOrderIBAN.TabIndex = 23
        Me.lblOrderIBAN.Text = "IBAN:"
        '
        'lblOrderSwift
        '
        Me.lblOrderSwift.AutoSize = True
        Me.lblOrderSwift.Location = New System.Drawing.Point(275, 109)
        Me.lblOrderSwift.Name = "lblOrderSwift"
        Me.lblOrderSwift.Size = New System.Drawing.Size(33, 13)
        Me.lblOrderSwift.TabIndex = 21
        Me.lblOrderSwift.Text = "Swift:"
        '
        'CboOrderName
        '
        Me.CboOrderName.FormattingEnabled = True
        Me.CboOrderName.Location = New System.Drawing.Point(67, 69)
        Me.CboOrderName.Name = "CboOrderName"
        Me.CboOrderName.Size = New System.Drawing.Size(262, 21)
        Me.CboOrderName.TabIndex = 8
        '
        'txtOrderRef
        '
        Me.txtOrderRef.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrderRef.Enabled = False
        Me.txtOrderRef.Location = New System.Drawing.Point(379, 70)
        Me.txtOrderRef.Name = "txtOrderRef"
        Me.txtOrderRef.Size = New System.Drawing.Size(129, 20)
        Me.txtOrderRef.TabIndex = 9
        '
        'lblorderref
        '
        Me.lblorderref.AutoSize = True
        Me.lblorderref.Location = New System.Drawing.Point(352, 73)
        Me.lblorderref.Name = "lblorderref"
        Me.lblorderref.Size = New System.Drawing.Size(27, 13)
        Me.lblorderref.TabIndex = 15
        Me.lblorderref.Text = "Ref:"
        '
        'lblOrderName
        '
        Me.lblOrderName.AutoSize = True
        Me.lblOrderName.Location = New System.Drawing.Point(11, 72)
        Me.lblOrderName.Name = "lblOrderName"
        Me.lblOrderName.Size = New System.Drawing.Size(38, 13)
        Me.lblOrderName.TabIndex = 14
        Me.lblOrderName.Text = "Name:"
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(687, 847)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(289, 24)
        Me.cmdSubmit.TabIndex = 36
        Me.cmdSubmit.Text = "Submit transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(1376, 539)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Filter Status"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Location = New System.Drawing.Point(11, 847)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(115, 26)
        Me.cmdRefresh.TabIndex = 33
        Me.cmdRefresh.Text = "Refresh Movements"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'TabMovement
        '
        Me.TabMovement.AllowDrop = True
        Me.TabMovement.Controls.Add(Me.OrderTab)
        Me.TabMovement.Controls.Add(Me.BeneficiaryTab)
        Me.TabMovement.Controls.Add(Me.FileTab)
        Me.TabMovement.Controls.Add(Me.HistoryTab)
        Me.TabMovement.Controls.Add(Me.PreviewTab)
        Me.TabMovement.Controls.Add(Me.RequestTab)
        Me.TabMovement.Location = New System.Drawing.Point(15, 157)
        Me.TabMovement.Name = "TabMovement"
        Me.TabMovement.SelectedIndex = 0
        Me.TabMovement.Size = New System.Drawing.Size(1567, 373)
        Me.TabMovement.TabIndex = 6
        '
        'OrderTab
        '
        Me.OrderTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.OrderTab.Controls.Add(Me.OrderGroupBox)
        Me.OrderTab.Location = New System.Drawing.Point(4, 22)
        Me.OrderTab.Name = "OrderTab"
        Me.OrderTab.Padding = New System.Windows.Forms.Padding(3)
        Me.OrderTab.Size = New System.Drawing.Size(1559, 347)
        Me.OrderTab.TabIndex = 0
        Me.OrderTab.Text = "Order"
        '
        'BeneficiaryTab
        '
        Me.BeneficiaryTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.BeneficiaryTab.Controls.Add(Me.BeneficiaryGroupBankGroupBox)
        Me.BeneficiaryTab.Controls.Add(Me.BeneficiaryGroupBox)
        Me.BeneficiaryTab.Location = New System.Drawing.Point(4, 22)
        Me.BeneficiaryTab.Name = "BeneficiaryTab"
        Me.BeneficiaryTab.Padding = New System.Windows.Forms.Padding(3)
        Me.BeneficiaryTab.Size = New System.Drawing.Size(1559, 347)
        Me.BeneficiaryTab.TabIndex = 1
        Me.BeneficiaryTab.Text = "Beneficiary"
        '
        'BeneficiaryGroupBankGroupBox
        '
        Me.BeneficiaryGroupBankGroupBox.Controls.Add(Me.BeneficiarySubBankGroupBox)
        Me.BeneficiaryGroupBankGroupBox.Controls.Add(Me.BeneficiaryBankGroupBox)
        Me.BeneficiaryGroupBankGroupBox.Location = New System.Drawing.Point(551, 7)
        Me.BeneficiaryGroupBankGroupBox.Name = "BeneficiaryGroupBankGroupBox"
        Me.BeneficiaryGroupBankGroupBox.Size = New System.Drawing.Size(531, 332)
        Me.BeneficiaryGroupBankGroupBox.TabIndex = 11
        Me.BeneficiaryGroupBankGroupBox.TabStop = False
        Me.BeneficiaryGroupBankGroupBox.Text = "Bank Details"
        Me.BeneficiaryGroupBankGroupBox.Visible = False
        '
        'BeneficiarySubBankGroupBox
        '
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.cboBenSubBankCountry)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubbankINN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankBIK)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankBIK)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankINN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.ChkBenSubBank)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankOther1)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.PicBenSubBankIBAN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.PicBenSubBankSwift)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.CboBenSubBankName)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankSwift)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankIBAN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankIBAN)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankSwift)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.lblBenSubBankOther1)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankZip)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label13)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankAddress2)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label14)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.txtBenSubBankAddress1)
        Me.BeneficiarySubBankGroupBox.Controls.Add(Me.Label15)
        Me.BeneficiarySubBankGroupBox.Location = New System.Drawing.Point(6, 19)
        Me.BeneficiarySubBankGroupBox.Name = "BeneficiarySubBankGroupBox"
        Me.BeneficiarySubBankGroupBox.Size = New System.Drawing.Size(519, 150)
        Me.BeneficiarySubBankGroupBox.TabIndex = 67
        Me.BeneficiarySubBankGroupBox.TabStop = False
        Me.BeneficiarySubBankGroupBox.Text = "Intermediary Institution (56A)"
        '
        'cboBenSubBankCountry
        '
        Me.cboBenSubBankCountry.BackColor = System.Drawing.Color.White
        Me.cboBenSubBankCountry.FormattingEnabled = True
        Me.cboBenSubBankCountry.Location = New System.Drawing.Point(124, 94)
        Me.cboBenSubBankCountry.Name = "cboBenSubBankCountry"
        Me.cboBenSubBankCountry.Size = New System.Drawing.Size(144, 21)
        Me.cboBenSubBankCountry.TabIndex = 149
        Me.cboBenSubBankCountry.Tag = "ControlAddressIntermediary"
        '
        'txtBenSubbankINN
        '
        Me.txtBenSubbankINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubbankINN.Enabled = False
        Me.txtBenSubbankINN.Location = New System.Drawing.Point(310, 120)
        Me.txtBenSubbankINN.Name = "txtBenSubbankINN"
        Me.txtBenSubbankINN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenSubbankINN.TabIndex = 71
        Me.txtBenSubbankINN.Tag = "ControlAddressIntermediary"
        '
        'txtBenSubBankBIK
        '
        Me.txtBenSubBankBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankBIK.Enabled = False
        Me.txtBenSubBankBIK.Location = New System.Drawing.Point(62, 120)
        Me.txtBenSubBankBIK.Name = "txtBenSubBankBIK"
        Me.txtBenSubBankBIK.Size = New System.Drawing.Size(206, 20)
        Me.txtBenSubBankBIK.TabIndex = 69
        Me.txtBenSubBankBIK.Tag = "ControlAddressIntermediary"
        Me.txtBenSubBankBIK.Visible = False
        '
        'lblBenSubBankBIK
        '
        Me.lblBenSubBankBIK.AutoSize = True
        Me.lblBenSubBankBIK.Location = New System.Drawing.Point(7, 123)
        Me.lblBenSubBankBIK.Name = "lblBenSubBankBIK"
        Me.lblBenSubBankBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblBenSubBankBIK.TabIndex = 70
        Me.lblBenSubBankBIK.Text = "BIK.ACC:"
        Me.lblBenSubBankBIK.Visible = False
        '
        'lblBenSubBankINN
        '
        Me.lblBenSubBankINN.AutoSize = True
        Me.lblBenSubBankINN.Location = New System.Drawing.Point(276, 123)
        Me.lblBenSubBankINN.Name = "lblBenSubBankINN"
        Me.lblBenSubBankINN.Size = New System.Drawing.Size(29, 13)
        Me.lblBenSubBankINN.TabIndex = 68
        Me.lblBenSubBankINN.Text = "INN:"
        Me.lblBenSubBankINN.Visible = False
        '
        'ChkBenSubBank
        '
        Me.ChkBenSubBank.AutoSize = True
        Me.ChkBenSubBank.Location = New System.Drawing.Point(21, 19)
        Me.ChkBenSubBank.Name = "ChkBenSubBank"
        Me.ChkBenSubBank.Size = New System.Drawing.Size(166, 17)
        Me.ChkBenSubBank.TabIndex = 66
        Me.ChkBenSubBank.Text = "Tick to add intermediary bank"
        Me.ChkBenSubBank.UseVisualStyleBackColor = True
        '
        'txtBenSubBankOther1
        '
        Me.txtBenSubBankOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankOther1.Enabled = False
        Me.txtBenSubBankOther1.Location = New System.Drawing.Point(311, 94)
        Me.txtBenSubBankOther1.Name = "txtBenSubBankOther1"
        Me.txtBenSubBankOther1.Size = New System.Drawing.Size(173, 20)
        Me.txtBenSubBankOther1.TabIndex = 31
        Me.txtBenSubBankOther1.Tag = "ControlAddressIntermediary"
        '
        'PicBenSubBankIBAN
        '
        Me.PicBenSubBankIBAN.Location = New System.Drawing.Point(486, 72)
        Me.PicBenSubBankIBAN.Name = "PicBenSubBankIBAN"
        Me.PicBenSubBankIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicBenSubBankIBAN.TabIndex = 65
        Me.PicBenSubBankIBAN.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenSubBankIBAN, "X verifies that this is not a valid IBAN. You can still import if you add an acco" &
        "unt number")
        '
        'PicBenSubBankSwift
        '
        Me.PicBenSubBankSwift.Location = New System.Drawing.Point(486, 49)
        Me.PicBenSubBankSwift.Name = "PicBenSubBankSwift"
        Me.PicBenSubBankSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicBenSubBankSwift.TabIndex = 64
        Me.PicBenSubBankSwift.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenSubBankSwift, "X verifies that this swift has not been found in IMS. You can still import with a" &
        " valid BIC code not found in IMS")
        '
        'CboBenSubBankName
        '
        Me.CboBenSubBankName.Enabled = False
        Me.CboBenSubBankName.FormattingEnabled = True
        Me.CboBenSubBankName.Location = New System.Drawing.Point(193, 17)
        Me.CboBenSubBankName.Name = "CboBenSubBankName"
        Me.CboBenSubBankName.Size = New System.Drawing.Size(291, 21)
        Me.CboBenSubBankName.TabIndex = 25
        Me.CboBenSubBankName.Tag = "ControlAddressIntermediary"
        '
        'lblBenSubBankSwift
        '
        Me.lblBenSubBankSwift.AutoSize = True
        Me.lblBenSubBankSwift.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenSubBankSwift.Location = New System.Drawing.Point(281, 53)
        Me.lblBenSubBankSwift.Name = "lblBenSubBankSwift"
        Me.lblBenSubBankSwift.Size = New System.Drawing.Size(24, 9)
        Me.lblBenSubBankSwift.TabIndex = 58
        Me.lblBenSubBankSwift.Text = "Swift:"
        '
        'txtBenSubBankIBAN
        '
        Me.txtBenSubBankIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankIBAN.Enabled = False
        Me.txtBenSubBankIBAN.Location = New System.Drawing.Point(311, 70)
        Me.txtBenSubBankIBAN.Name = "txtBenSubBankIBAN"
        Me.txtBenSubBankIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenSubBankIBAN.TabIndex = 30
        Me.txtBenSubBankIBAN.Tag = "ControlAddressIntermediary"
        '
        'lblBenSubBankIBAN
        '
        Me.lblBenSubBankIBAN.AutoSize = True
        Me.lblBenSubBankIBAN.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenSubBankIBAN.Location = New System.Drawing.Point(280, 77)
        Me.lblBenSubBankIBAN.Name = "lblBenSubBankIBAN"
        Me.lblBenSubBankIBAN.Size = New System.Drawing.Size(25, 9)
        Me.lblBenSubBankIBAN.TabIndex = 59
        Me.lblBenSubBankIBAN.Text = "IBAN:"
        '
        'txtBenSubBankSwift
        '
        Me.txtBenSubBankSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankSwift.Enabled = False
        Me.txtBenSubBankSwift.Location = New System.Drawing.Point(311, 46)
        Me.txtBenSubBankSwift.Name = "txtBenSubBankSwift"
        Me.txtBenSubBankSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtBenSubBankSwift.TabIndex = 29
        Me.txtBenSubBankSwift.Tag = "ControlAddressIntermediary"
        '
        'lblBenSubBankOther1
        '
        Me.lblBenSubBankOther1.AutoSize = True
        Me.lblBenSubBankOther1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenSubBankOther1.Location = New System.Drawing.Point(279, 101)
        Me.lblBenSubBankOther1.Name = "lblBenSubBankOther1"
        Me.lblBenSubBankOther1.Size = New System.Drawing.Size(26, 9)
        Me.lblBenSubBankOther1.TabIndex = 60
        Me.lblBenSubBankOther1.Text = "Other:"
        '
        'txtBenSubBankZip
        '
        Me.txtBenSubBankZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankZip.Enabled = False
        Me.txtBenSubBankZip.Location = New System.Drawing.Point(38, 94)
        Me.txtBenSubBankZip.Name = "txtBenSubBankZip"
        Me.txtBenSubBankZip.Size = New System.Drawing.Size(80, 20)
        Me.txtBenSubBankZip.TabIndex = 28
        Me.txtBenSubBankZip.Tag = "ControlAddressIntermediary"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 49)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 13)
        Me.Label13.TabIndex = 61
        Me.Label13.Text = "Add:"
        '
        'txtBenSubBankAddress2
        '
        Me.txtBenSubBankAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankAddress2.Enabled = False
        Me.txtBenSubBankAddress2.Location = New System.Drawing.Point(38, 70)
        Me.txtBenSubBankAddress2.Name = "txtBenSubBankAddress2"
        Me.txtBenSubBankAddress2.Size = New System.Drawing.Size(230, 20)
        Me.txtBenSubBankAddress2.TabIndex = 27
        Me.txtBenSubBankAddress2.Tag = "ControlAddressIntermediary"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 73)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(27, 13)
        Me.Label14.TabIndex = 62
        Me.Label14.Text = "City:"
        '
        'txtBenSubBankAddress1
        '
        Me.txtBenSubBankAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenSubBankAddress1.Enabled = False
        Me.txtBenSubBankAddress1.Location = New System.Drawing.Point(38, 46)
        Me.txtBenSubBankAddress1.Name = "txtBenSubBankAddress1"
        Me.txtBenSubBankAddress1.Size = New System.Drawing.Size(230, 20)
        Me.txtBenSubBankAddress1.TabIndex = 26
        Me.txtBenSubBankAddress1.Tag = "ControlAddressIntermediary"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(7, 97)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(25, 13)
        Me.Label15.TabIndex = 63
        Me.Label15.Text = "Zip:"
        '
        'BeneficiaryBankGroupBox
        '
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.cboBenBankCountry)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankINN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankBIK)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankBIK)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankINN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankOther1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.PicBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.PicBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.CboBenBankName)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankIBAN)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankSwift)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankOther1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankZip)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankAddress2)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankAddress2)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.txtBenBankAddress1)
        Me.BeneficiaryBankGroupBox.Controls.Add(Me.lblBenBankZip)
        Me.BeneficiaryBankGroupBox.Location = New System.Drawing.Point(6, 179)
        Me.BeneficiaryBankGroupBox.Name = "BeneficiaryBankGroupBox"
        Me.BeneficiaryBankGroupBox.Size = New System.Drawing.Size(519, 147)
        Me.BeneficiaryBankGroupBox.TabIndex = 66
        Me.BeneficiaryBankGroupBox.TabStop = False
        Me.BeneficiaryBankGroupBox.Text = "Account With Institution (57A/57D)"
        '
        'cboBenBankCountry
        '
        Me.cboBenBankCountry.BackColor = System.Drawing.Color.White
        Me.cboBenBankCountry.FormattingEnabled = True
        Me.cboBenBankCountry.Location = New System.Drawing.Point(122, 93)
        Me.cboBenBankCountry.Name = "cboBenBankCountry"
        Me.cboBenBankCountry.Size = New System.Drawing.Size(146, 21)
        Me.cboBenBankCountry.TabIndex = 150
        Me.cboBenBankCountry.Tag = "ControlAddressBenBank"
        '
        'txtBenBankINN
        '
        Me.txtBenBankINN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankINN.Location = New System.Drawing.Point(311, 118)
        Me.txtBenBankINN.Name = "txtBenBankINN"
        Me.txtBenBankINN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenBankINN.TabIndex = 75
        Me.txtBenBankINN.Tag = "ControlAddressIntermediary"
        '
        'txtBenBankBIK
        '
        Me.txtBenBankBIK.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankBIK.Location = New System.Drawing.Point(62, 118)
        Me.txtBenBankBIK.Name = "txtBenBankBIK"
        Me.txtBenBankBIK.Size = New System.Drawing.Size(206, 20)
        Me.txtBenBankBIK.TabIndex = 73
        Me.txtBenBankBIK.Tag = "ControlAddressIntermediary"
        Me.txtBenBankBIK.Visible = False
        '
        'lblBenBankBIK
        '
        Me.lblBenBankBIK.AutoSize = True
        Me.lblBenBankBIK.Location = New System.Drawing.Point(7, 121)
        Me.lblBenBankBIK.Name = "lblBenBankBIK"
        Me.lblBenBankBIK.Size = New System.Drawing.Size(51, 13)
        Me.lblBenBankBIK.TabIndex = 74
        Me.lblBenBankBIK.Text = "BIK.ACC:"
        Me.lblBenBankBIK.Visible = False
        '
        'lblBenBankINN
        '
        Me.lblBenBankINN.AutoSize = True
        Me.lblBenBankINN.Location = New System.Drawing.Point(276, 123)
        Me.lblBenBankINN.Name = "lblBenBankINN"
        Me.lblBenBankINN.Size = New System.Drawing.Size(29, 13)
        Me.lblBenBankINN.TabIndex = 72
        Me.lblBenBankINN.Text = "INN:"
        Me.lblBenBankINN.Visible = False
        '
        'txtBenBankOther1
        '
        Me.txtBenBankOther1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankOther1.Location = New System.Drawing.Point(311, 94)
        Me.txtBenBankOther1.Name = "txtBenBankOther1"
        Me.txtBenBankOther1.Size = New System.Drawing.Size(172, 20)
        Me.txtBenBankOther1.TabIndex = 31
        Me.txtBenBankOther1.Tag = "ControlAddressBenBank"
        '
        'PicBenBankIBAN
        '
        Me.PicBenBankIBAN.Location = New System.Drawing.Point(486, 72)
        Me.PicBenBankIBAN.Name = "PicBenBankIBAN"
        Me.PicBenBankIBAN.Size = New System.Drawing.Size(23, 17)
        Me.PicBenBankIBAN.TabIndex = 65
        Me.PicBenBankIBAN.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenBankIBAN, "X verifies that this is not a valid IBAN. You can still import if you add an acco" &
        "unt number")
        '
        'lblBenBankName
        '
        Me.lblBenBankName.AutoSize = True
        Me.lblBenBankName.Location = New System.Drawing.Point(67, 19)
        Me.lblBenBankName.Name = "lblBenBankName"
        Me.lblBenBankName.Size = New System.Drawing.Size(109, 13)
        Me.lblBenBankName.TabIndex = 34
        Me.lblBenBankName.Text = "Account Bank Name:"
        '
        'PicBenBankSwift
        '
        Me.PicBenBankSwift.Location = New System.Drawing.Point(486, 49)
        Me.PicBenBankSwift.Name = "PicBenBankSwift"
        Me.PicBenBankSwift.Size = New System.Drawing.Size(23, 17)
        Me.PicBenBankSwift.TabIndex = 64
        Me.PicBenBankSwift.TabStop = False
        Me.MovementToolTip.SetToolTip(Me.PicBenBankSwift, "X verifies that this swift has not been found in IMS. You can still import with a" &
        " valid BIC code not found in IMS")
        '
        'CboBenBankName
        '
        Me.CboBenBankName.FormattingEnabled = True
        Me.CboBenBankName.Location = New System.Drawing.Point(193, 16)
        Me.CboBenBankName.Name = "CboBenBankName"
        Me.CboBenBankName.Size = New System.Drawing.Size(291, 21)
        Me.CboBenBankName.TabIndex = 25
        Me.CboBenBankName.Tag = "ControlAddressBenBank"
        '
        'lblBenBankSwift
        '
        Me.lblBenBankSwift.AutoSize = True
        Me.lblBenBankSwift.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenBankSwift.Location = New System.Drawing.Point(281, 53)
        Me.lblBenBankSwift.Name = "lblBenBankSwift"
        Me.lblBenBankSwift.Size = New System.Drawing.Size(24, 9)
        Me.lblBenBankSwift.TabIndex = 58
        Me.lblBenBankSwift.Text = "Swift:"
        '
        'txtBenBankIBAN
        '
        Me.txtBenBankIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankIBAN.Location = New System.Drawing.Point(311, 70)
        Me.txtBenBankIBAN.Name = "txtBenBankIBAN"
        Me.txtBenBankIBAN.Size = New System.Drawing.Size(173, 20)
        Me.txtBenBankIBAN.TabIndex = 30
        Me.txtBenBankIBAN.Tag = "ControlAddressBenBank"
        '
        'lblBenBankIBAN
        '
        Me.lblBenBankIBAN.AutoSize = True
        Me.lblBenBankIBAN.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenBankIBAN.Location = New System.Drawing.Point(280, 76)
        Me.lblBenBankIBAN.Name = "lblBenBankIBAN"
        Me.lblBenBankIBAN.Size = New System.Drawing.Size(25, 9)
        Me.lblBenBankIBAN.TabIndex = 59
        Me.lblBenBankIBAN.Text = "IBAN:"
        '
        'txtBenBankSwift
        '
        Me.txtBenBankSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankSwift.Location = New System.Drawing.Point(311, 46)
        Me.txtBenBankSwift.Name = "txtBenBankSwift"
        Me.txtBenBankSwift.Size = New System.Drawing.Size(173, 20)
        Me.txtBenBankSwift.TabIndex = 29
        Me.txtBenBankSwift.Tag = "ControlAddressBenBank"
        '
        'lblBenBankOther1
        '
        Me.lblBenBankOther1.AutoSize = True
        Me.lblBenBankOther1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBenBankOther1.Location = New System.Drawing.Point(279, 101)
        Me.lblBenBankOther1.Name = "lblBenBankOther1"
        Me.lblBenBankOther1.Size = New System.Drawing.Size(26, 9)
        Me.lblBenBankOther1.TabIndex = 60
        Me.lblBenBankOther1.Text = "Other:"
        '
        'txtBenBankZip
        '
        Me.txtBenBankZip.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankZip.Location = New System.Drawing.Point(38, 93)
        Me.txtBenBankZip.Name = "txtBenBankZip"
        Me.txtBenBankZip.Size = New System.Drawing.Size(78, 20)
        Me.txtBenBankZip.TabIndex = 28
        Me.txtBenBankZip.Tag = "ControlAddressBenBank"
        '
        'lblBenBankAddress1
        '
        Me.lblBenBankAddress1.AutoSize = True
        Me.lblBenBankAddress1.Location = New System.Drawing.Point(7, 49)
        Me.lblBenBankAddress1.Name = "lblBenBankAddress1"
        Me.lblBenBankAddress1.Size = New System.Drawing.Size(29, 13)
        Me.lblBenBankAddress1.TabIndex = 61
        Me.lblBenBankAddress1.Text = "Add:"
        '
        'txtBenBankAddress2
        '
        Me.txtBenBankAddress2.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankAddress2.Location = New System.Drawing.Point(38, 69)
        Me.txtBenBankAddress2.Name = "txtBenBankAddress2"
        Me.txtBenBankAddress2.Size = New System.Drawing.Size(230, 20)
        Me.txtBenBankAddress2.TabIndex = 27
        Me.txtBenBankAddress2.Tag = "ControlAddressBenBank"
        '
        'lblBenBankAddress2
        '
        Me.lblBenBankAddress2.AutoSize = True
        Me.lblBenBankAddress2.Location = New System.Drawing.Point(9, 73)
        Me.lblBenBankAddress2.Name = "lblBenBankAddress2"
        Me.lblBenBankAddress2.Size = New System.Drawing.Size(27, 13)
        Me.lblBenBankAddress2.TabIndex = 62
        Me.lblBenBankAddress2.Text = "City:"
        '
        'txtBenBankAddress1
        '
        Me.txtBenBankAddress1.BackColor = System.Drawing.SystemColors.Window
        Me.txtBenBankAddress1.Location = New System.Drawing.Point(38, 46)
        Me.txtBenBankAddress1.Name = "txtBenBankAddress1"
        Me.txtBenBankAddress1.Size = New System.Drawing.Size(230, 20)
        Me.txtBenBankAddress1.TabIndex = 26
        Me.txtBenBankAddress1.Tag = "ControlAddressBenBank"
        '
        'lblBenBankZip
        '
        Me.lblBenBankZip.AutoSize = True
        Me.lblBenBankZip.Location = New System.Drawing.Point(7, 96)
        Me.lblBenBankZip.Name = "lblBenBankZip"
        Me.lblBenBankZip.Size = New System.Drawing.Size(25, 13)
        Me.lblBenBankZip.TabIndex = 63
        Me.lblBenBankZip.Text = "Zip:"
        '
        'FileTab
        '
        Me.FileTab.AllowDrop = True
        Me.FileTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.FileTab.Controls.Add(Me.GrpFiles)
        Me.FileTab.Location = New System.Drawing.Point(4, 22)
        Me.FileTab.Name = "FileTab"
        Me.FileTab.Padding = New System.Windows.Forms.Padding(3)
        Me.FileTab.Size = New System.Drawing.Size(1559, 347)
        Me.FileTab.TabIndex = 2
        Me.FileTab.Text = "Files"
        '
        'GrpFiles
        '
        Me.GrpFiles.Controls.Add(Me.TreeViewFiles)
        Me.GrpFiles.Controls.Add(Me.LVFiles)
        Me.GrpFiles.Location = New System.Drawing.Point(18, 7)
        Me.GrpFiles.Name = "GrpFiles"
        Me.GrpFiles.Size = New System.Drawing.Size(1063, 332)
        Me.GrpFiles.TabIndex = 8
        Me.GrpFiles.TabStop = False
        Me.GrpFiles.Text = "Files associated with this payment are stored here (drag && drop to add, right cl" &
    "ick for file menu):"
        '
        'TreeViewFiles
        '
        Me.TreeViewFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.TreeViewFiles.ImageIndex = 0
        Me.TreeViewFiles.ImageList = Me.ImageListFiles
        Me.TreeViewFiles.Location = New System.Drawing.Point(15, 19)
        Me.TreeViewFiles.Name = "TreeViewFiles"
        Me.TreeViewFiles.SelectedImageIndex = 0
        Me.TreeViewFiles.Size = New System.Drawing.Size(387, 303)
        Me.TreeViewFiles.TabIndex = 7
        '
        'ImageListFiles
        '
        Me.ImageListFiles.ImageStream = CType(resources.GetObject("ImageListFiles.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListFiles.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListFiles.Images.SetKeyName(0, "computer.jpg")
        Me.ImageListFiles.Images.SetKeyName(1, "download3.jpg")
        '
        'LVFiles
        '
        Me.LVFiles.AllowDrop = True
        Me.LVFiles.BackColor = System.Drawing.SystemColors.Window
        Me.LVFiles.BackgroundImage = CType(resources.GetObject("LVFiles.BackgroundImage"), System.Drawing.Image)
        Me.LVFiles.FullRowSelect = True
        Me.LVFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LVFiles.Location = New System.Drawing.Point(408, 19)
        Me.LVFiles.MultiSelect = False
        Me.LVFiles.Name = "LVFiles"
        Me.LVFiles.Size = New System.Drawing.Size(649, 303)
        Me.LVFiles.TabIndex = 6
        Me.LVFiles.UseCompatibleStateImageBehavior = False
        '
        'HistoryTab
        '
        Me.HistoryTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.HistoryTab.Controls.Add(Me.GroupBox1)
        Me.HistoryTab.Location = New System.Drawing.Point(4, 22)
        Me.HistoryTab.Name = "HistoryTab"
        Me.HistoryTab.Padding = New System.Windows.Forms.Padding(3)
        Me.HistoryTab.Size = New System.Drawing.Size(1559, 347)
        Me.HistoryTab.TabIndex = 3
        Me.HistoryTab.Text = "History"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvHistory)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1535, 332)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "IMS Transaction history for this portfolio and currency"
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToOrderColumns = True
        Me.dgvHistory.BackgroundColor = System.Drawing.Color.LightSteelBlue
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistory.GridColor = System.Drawing.Color.LightSteelBlue
        Me.dgvHistory.Location = New System.Drawing.Point(15, 19)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.Size = New System.Drawing.Size(1514, 307)
        Me.dgvHistory.TabIndex = 36
        '
        'PreviewTab
        '
        Me.PreviewTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PreviewTab.Controls.Add(Me.GroupSWIFT)
        Me.PreviewTab.Location = New System.Drawing.Point(4, 22)
        Me.PreviewTab.Name = "PreviewTab"
        Me.PreviewTab.Size = New System.Drawing.Size(1559, 347)
        Me.PreviewTab.TabIndex = 4
        Me.PreviewTab.Text = "Preview"
        '
        'GroupSWIFT
        '
        Me.GroupSWIFT.Controls.Add(Me.txtSWIFT)
        Me.GroupSWIFT.Location = New System.Drawing.Point(18, 7)
        Me.GroupSWIFT.Name = "GroupSWIFT"
        Me.GroupSWIFT.Size = New System.Drawing.Size(1538, 332)
        Me.GroupSWIFT.TabIndex = 34
        Me.GroupSWIFT.TabStop = False
        Me.GroupSWIFT.Text = "SWIFT Export File contents"
        '
        'txtSWIFT
        '
        Me.txtSWIFT.AcceptsReturn = True
        Me.txtSWIFT.AcceptsTab = True
        Me.txtSWIFT.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.txtSWIFT.Location = New System.Drawing.Point(6, 19)
        Me.txtSWIFT.Multiline = True
        Me.txtSWIFT.Name = "txtSWIFT"
        Me.txtSWIFT.ReadOnly = True
        Me.txtSWIFT.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSWIFT.Size = New System.Drawing.Size(1526, 307)
        Me.txtSWIFT.TabIndex = 33
        '
        'RequestTab
        '
        Me.RequestTab.BackColor = System.Drawing.Color.LightSteelBlue
        Me.RequestTab.Controls.Add(Me.GroupBox2)
        Me.RequestTab.Location = New System.Drawing.Point(4, 22)
        Me.RequestTab.Name = "RequestTab"
        Me.RequestTab.Size = New System.Drawing.Size(1559, 347)
        Me.RequestTab.TabIndex = 5
        Me.RequestTab.Text = "Request"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.GrpSendMessage)
        Me.GroupBox2.Controls.Add(Me.txtEmailRequest)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1538, 332)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Payment request/email contents"
        '
        'GrpSendMessage
        '
        Me.GrpSendMessage.Controls.Add(Me.txtRelatedRef)
        Me.GrpSendMessage.Controls.Add(Me.Label2)
        Me.GrpSendMessage.Controls.Add(Me.Label1)
        Me.GrpSendMessage.Controls.Add(Me.cboSendTo)
        Me.GrpSendMessage.Location = New System.Drawing.Point(7, 20)
        Me.GrpSendMessage.Name = "GrpSendMessage"
        Me.GrpSendMessage.Size = New System.Drawing.Size(1525, 54)
        Me.GrpSendMessage.TabIndex = 34
        Me.GrpSendMessage.TabStop = False
        Me.GrpSendMessage.Text = "Message Details"
        '
        'txtRelatedRef
        '
        Me.txtRelatedRef.Location = New System.Drawing.Point(394, 20)
        Me.txtRelatedRef.Name = "txtRelatedRef"
        Me.txtRelatedRef.Size = New System.Drawing.Size(173, 20)
        Me.txtRelatedRef.TabIndex = 41
        Me.txtRelatedRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(288, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 13)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "Related Reference:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Send To:"
        '
        'cboSendTo
        '
        Me.cboSendTo.FormattingEnabled = True
        Me.cboSendTo.Location = New System.Drawing.Point(67, 19)
        Me.cboSendTo.Name = "cboSendTo"
        Me.cboSendTo.Size = New System.Drawing.Size(198, 21)
        Me.cboSendTo.TabIndex = 2
        '
        'txtEmailRequest
        '
        Me.txtEmailRequest.AcceptsReturn = True
        Me.txtEmailRequest.AcceptsTab = True
        Me.txtEmailRequest.BackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(247, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.txtEmailRequest.Location = New System.Drawing.Point(6, 80)
        Me.txtEmailRequest.Multiline = True
        Me.txtEmailRequest.Name = "txtEmailRequest"
        Me.txtEmailRequest.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmailRequest.Size = New System.Drawing.Size(1526, 246)
        Me.txtEmailRequest.TabIndex = 33
        '
        'TimerBroker
        '
        Me.TimerBroker.Interval = 1000
        '
        'txtID
        '
        Me.txtID.AcceptsReturn = True
        Me.txtID.AcceptsTab = True
        Me.txtID.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtID.Enabled = False
        Me.txtID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtID.Location = New System.Drawing.Point(1494, 10)
        Me.txtID.Multiline = True
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(88, 28)
        Me.txtID.TabIndex = 50
        '
        'ImageListTickCross
        '
        Me.ImageListTickCross.ImageStream = CType(resources.GetObject("ImageListTickCross.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageListTickCross.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageListTickCross.Images.SetKeyName(0, "button_cancel16.png")
        Me.ImageListTickCross.Images.SetKeyName(1, "tick (1)16.png")
        Me.ImageListTickCross.Images.SetKeyName(2, "images (1).jpg")
        '
        'ContextMenuStripFiles
        '
        Me.ContextMenuStripFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenFileToolStripMenuItem, Me.RemoveFileToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStripFiles.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles.Size = New System.Drawing.Size(139, 70)
        '
        'OpenFileToolStripMenuItem
        '
        Me.OpenFileToolStripMenuItem.Name = "OpenFileToolStripMenuItem"
        Me.OpenFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.OpenFileToolStripMenuItem.Text = "&Open File"
        '
        'RemoveFileToolStripMenuItem
        '
        Me.RemoveFileToolStripMenuItem.Name = "RemoveFileToolStripMenuItem"
        Me.RemoveFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RemoveFileToolStripMenuItem.Text = "&Remove File"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Interval = 120000
        '
        'CboFilterStatus
        '
        Me.CboFilterStatus.FormattingEnabled = True
        Me.CboFilterStatus.Location = New System.Drawing.Point(1444, 536)
        Me.CboFilterStatus.Name = "CboFilterStatus"
        Me.CboFilterStatus.Size = New System.Drawing.Size(138, 21)
        Me.CboFilterStatus.TabIndex = 51
        '
        'ChkCashRefresh
        '
        Me.ChkCashRefresh.AutoSize = True
        Me.ChkCashRefresh.Checked = True
        Me.ChkCashRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkCashRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkCashRefresh.Location = New System.Drawing.Point(184, 854)
        Me.ChkCashRefresh.Name = "ChkCashRefresh"
        Me.ChkCashRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkCashRefresh.TabIndex = 52
        Me.ChkCashRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkCashRefresh.UseVisualStyleBackColor = True
        '
        'cboPayCheck
        '
        Me.cboPayCheck.FormattingEnabled = True
        Me.cboPayCheck.Location = New System.Drawing.Point(1232, 536)
        Me.cboPayCheck.Name = "cboPayCheck"
        Me.cboPayCheck.Size = New System.Drawing.Size(138, 21)
        Me.cboPayCheck.TabIndex = 66
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(1140, 539)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(86, 13)
        Me.Label23.TabIndex = 65
        Me.Label23.Text = "Payments Period"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(137, 846)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(41, 27)
        Me.CmdExportToExcel.TabIndex = 67
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'cboFilterType
        '
        Me.cboFilterType.FormattingEnabled = True
        Me.cboFilterType.Location = New System.Drawing.Point(934, 535)
        Me.cboFilterType.Name = "cboFilterType"
        Me.cboFilterType.Size = New System.Drawing.Size(200, 21)
        Me.cboFilterType.TabIndex = 69
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(842, 538)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 68
        Me.Label3.Text = "Movement Type"
        '
        'cboFilterLocation
        '
        Me.cboFilterLocation.FormattingEnabled = True
        Me.cboFilterLocation.Location = New System.Drawing.Point(723, 535)
        Me.cboFilterLocation.Name = "cboFilterLocation"
        Me.cboFilterLocation.Size = New System.Drawing.Size(113, 21)
        Me.cboFilterLocation.TabIndex = 71
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(669, 539)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "Location"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(489, 539)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 72
        Me.Label5.Text = "ID"
        '
        'txtFilterID
        '
        Me.txtFilterID.Location = New System.Drawing.Point(514, 536)
        Me.txtFilterID.Name = "txtFilterID"
        Me.txtFilterID.Size = New System.Drawing.Size(149, 20)
        Me.txtFilterID.TabIndex = 73
        '
        'ChkShowHidden
        '
        Me.ChkShowHidden.AutoSize = True
        Me.ChkShowHidden.ForeColor = System.Drawing.Color.Maroon
        Me.ChkShowHidden.Location = New System.Drawing.Point(1467, 850)
        Me.ChkShowHidden.Name = "ChkShowHidden"
        Me.ChkShowHidden.Size = New System.Drawing.Size(115, 17)
        Me.ChkShowHidden.TabIndex = 74
        Me.ChkShowHidden.Text = "Show hidden items"
        Me.ChkShowHidden.UseVisualStyleBackColor = True
        '
        'CmdSubmitBatch
        '
        Me.CmdSubmitBatch.Location = New System.Drawing.Point(1259, 847)
        Me.CmdSubmitBatch.Name = "CmdSubmitBatch"
        Me.CmdSubmitBatch.Size = New System.Drawing.Size(202, 24)
        Me.CmdSubmitBatch.TabIndex = 75
        Me.CmdSubmitBatch.Text = "Submit manual batch transactions"
        Me.CmdSubmitBatch.UseVisualStyleBackColor = True
        '
        'FrmPayment
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1594, 873)
        Me.Controls.Add(Me.CmdSubmitBatch)
        Me.Controls.Add(Me.ChkShowHidden)
        Me.Controls.Add(Me.txtFilterID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboFilterLocation)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboFilterType)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CmdExportToExcel)
        Me.Controls.Add(Me.cboPayCheck)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.ChkCashRefresh)
        Me.Controls.Add(Me.CboFilterStatus)
        Me.Controls.Add(Me.txtID)
        Me.Controls.Add(Me.TabMovement)
        Me.Controls.Add(Me.cmdRefresh)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.GrpTransType)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.Controls.Add(Me.dgvCash)
        Me.Controls.Add(Me.cmdAddCash)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "FrmPayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cash Movements Form"
        CType(Me.dgvCash, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpTransType.ResumeLayout(False)
        Me.GrpTransType.PerformLayout()
        CType(Me.PicPayType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryGroupBox.ResumeLayout(False)
        Me.TabBeneficiary.ResumeLayout(False)
        Me.MainTab.ResumeLayout(False)
        Me.MainTab.PerformLayout()
        Me.BankDetailsTab.ResumeLayout(False)
        Me.BankDetailsTab.PerformLayout()
        CType(Me.PicBenSwift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBenIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        Me.OrderGroupBox.ResumeLayout(False)
        Me.OrderGroupBox.PerformLayout()
        CType(Me.PicOrderIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicOrderSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabMovement.ResumeLayout(False)
        Me.OrderTab.ResumeLayout(False)
        Me.BeneficiaryTab.ResumeLayout(False)
        Me.BeneficiaryGroupBankGroupBox.ResumeLayout(False)
        Me.BeneficiarySubBankGroupBox.ResumeLayout(False)
        Me.BeneficiarySubBankGroupBox.PerformLayout()
        CType(Me.PicBenSubBankIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBenSubBankSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BeneficiaryBankGroupBox.ResumeLayout(False)
        Me.BeneficiaryBankGroupBox.PerformLayout()
        CType(Me.PicBenBankIBAN, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicBenBankSwift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FileTab.ResumeLayout(False)
        Me.GrpFiles.ResumeLayout(False)
        Me.HistoryTab.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PreviewTab.ResumeLayout(False)
        Me.GroupSWIFT.ResumeLayout(False)
        Me.GroupSWIFT.PerformLayout()
        Me.RequestTab.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GrpSendMessage.ResumeLayout(False)
        Me.GrpSendMessage.PerformLayout()
        Me.ContextMenuStripFiles.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdAddCash As System.Windows.Forms.Button
    Friend WithEvents dgvCash As System.Windows.Forms.DataGridView
    Friend WithEvents lblpaymentTitle As System.Windows.Forms.Label
    Friend WithEvents GrpTransType As System.Windows.Forms.GroupBox
    Friend WithEvents dtSettleDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblPayType As System.Windows.Forms.Label
    Friend WithEvents CboPayType As System.Windows.Forms.ComboBox
    Friend WithEvents BeneficiaryGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents lblSettleDate As System.Windows.Forms.Label
    Friend WithEvents OrderGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents txtOrderRef As System.Windows.Forms.TextBox
    Friend WithEvents lblorderref As System.Windows.Forms.Label
    Friend WithEvents lblOrderName As System.Windows.Forms.Label
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents lblTransDate As System.Windows.Forms.Label
    Friend WithEvents dtTransDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents CboOrderName As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSubmit As System.Windows.Forms.Button
    Friend WithEvents lblOrderZip As System.Windows.Forms.Label
    Friend WithEvents lblOrderAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblOrderAddress1 As System.Windows.Forms.Label
    Friend WithEvents lblOrderOther1 As System.Windows.Forms.Label
    Friend WithEvents lblOrderIBAN As System.Windows.Forms.Label
    Friend WithEvents lblOrderSwift As System.Windows.Forms.Label
    Friend WithEvents txtComments As System.Windows.Forms.TextBox
    Friend WithEvents lblcomments As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtOrderOther1 As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderIBAN As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderSwift As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderZip As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents lblportfolio As System.Windows.Forms.Label
    Friend WithEvents CboPortfolio As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderAccount As System.Windows.Forms.Label
    Friend WithEvents CboCCY As System.Windows.Forms.ComboBox
    Friend WithEvents lblCCY As System.Windows.Forms.Label
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents CboOrderAccount As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrderBalance As System.Windows.Forms.Label
    Friend WithEvents txtOrderBalance As System.Windows.Forms.TextBox
    Friend WithEvents TabMovement As System.Windows.Forms.TabControl
    Friend WithEvents OrderTab As System.Windows.Forms.TabPage
    Friend WithEvents BeneficiaryTab As System.Windows.Forms.TabPage
    Friend WithEvents BeneficiaryGroupBankGroupBox As System.Windows.Forms.GroupBox
    Friend WithEvents txtBenBankOther1 As System.Windows.Forms.TextBox
    Friend WithEvents txtBenBankIBAN As System.Windows.Forms.TextBox
    Friend WithEvents txtBenBankSwift As System.Windows.Forms.TextBox
    Friend WithEvents txtBenBankZip As System.Windows.Forms.TextBox
    Friend WithEvents txtBenBankAddress2 As System.Windows.Forms.TextBox
    Friend WithEvents txtBenBankAddress1 As System.Windows.Forms.TextBox
    Friend WithEvents lblBenBankZip As System.Windows.Forms.Label
    Friend WithEvents lblBenBankAddress2 As System.Windows.Forms.Label
    Friend WithEvents lblBenBankAddress1 As System.Windows.Forms.Label
    Friend WithEvents lblBenBankOther1 As System.Windows.Forms.Label
    Friend WithEvents lblBenBankIBAN As System.Windows.Forms.Label
    Friend WithEvents lblBenBankSwift As System.Windows.Forms.Label
    Friend WithEvents CboBenBankName As System.Windows.Forms.ComboBox
    Friend WithEvents lblBenBankName As System.Windows.Forms.Label
    Friend WithEvents MovementToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lblportfoliofee As System.Windows.Forms.Label
    Friend WithEvents TimerBroker As System.Windows.Forms.Timer
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents ImageListTickCross As System.Windows.Forms.ImageList
    Friend WithEvents PicOrderSwift As System.Windows.Forms.PictureBox
    Friend WithEvents PicOrderIBAN As System.Windows.Forms.PictureBox
    Friend WithEvents PicBenBankIBAN As System.Windows.Forms.PictureBox
    Friend WithEvents PicBenBankSwift As System.Windows.Forms.PictureBox
    Friend WithEvents PicPayType As PictureBox
    Friend WithEvents FileTab As TabPage
    Friend WithEvents LVFiles As ListView
    Friend WithEvents TreeViewFiles As TreeView
    Friend WithEvents ImageListFiles As ImageList
    Friend WithEvents GrpFiles As GroupBox
    Friend WithEvents ContextMenuStripFiles As ContextMenuStrip
    Friend WithEvents OpenFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TimerRefresh As Timer
    Friend WithEvents lblordermsg1 As LinkLabel
    Friend WithEvents lblordermsg7 As LinkLabel
    Friend WithEvents lblordermsg6 As LinkLabel
    Friend WithEvents lblordermsg5 As LinkLabel
    Friend WithEvents lblordermsg4 As LinkLabel
    Friend WithEvents lblordermsg3 As LinkLabel
    Friend WithEvents lblordermsg2 As LinkLabel
    Friend WithEvents BeneficiaryBankGroupBox As GroupBox
    Friend WithEvents CboFilterStatus As ComboBox
    Friend WithEvents HistoryTab As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents dgvHistory As DataGridView
    Friend WithEvents ChkCashRefresh As CheckBox
    Friend WithEvents lblOrderBalancePostInc As Label
    Friend WithEvents txtOrderBalanceInc As TextBox
    Friend WithEvents txtOrderBalancePostInc As TextBox
    Friend WithEvents lblOrderBalanceInc As Label
    Friend WithEvents lblordermsg8 As LinkLabel
    Friend WithEvents cboBenCCY As ComboBox
    Friend WithEvents lblBenCCY As Label
    Friend WithEvents lblExchangeRate As Label
    Friend WithEvents lblCCYAmt As Label
    Friend WithEvents PreviewTab As TabPage
    Friend WithEvents GroupSWIFT As GroupBox
    Friend WithEvents txtSWIFT As TextBox
    Friend WithEvents ChkAutoComments As CheckBox
    Friend WithEvents txtBenBankBIK As TextBox
    Friend WithEvents lblBenBankBIK As Label
    Friend WithEvents lblBenBankINN As Label
    Friend WithEvents txtBenBankINN As TextBox
    Friend WithEvents lblOrderVOCode As Label
    Friend WithEvents cboOrderVOCode As ComboBox
    Friend WithEvents cboPayCheck As ComboBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtIMSRef As TextBox
    Friend WithEvents lblIMSRef As Label
    Friend WithEvents RequestTab As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtEmailRequest As TextBox
    Friend WithEvents TabBeneficiary As TabControl
    Friend WithEvents MainTab As TabPage
    Friend WithEvents cboBenType As ComboBox
    Friend WithEvents lblbenType As Label
    Friend WithEvents cboBenCountry As ComboBox
    Friend WithEvents lblBenCountry As Label
    Friend WithEvents lblBenCounty As Label
    Friend WithEvents txtbencounty As TextBox
    Friend WithEvents txtBenZip As TextBox
    Friend WithEvents txtBenAddress1 As TextBox
    Friend WithEvents txtBenAddress2 As TextBox
    Friend WithEvents txtbensurname As TextBox
    Friend WithEvents txtbenmiddlename As TextBox
    Friend WithEvents txtbenfirstname As TextBox
    Friend WithEvents txtBenRef As TextBox
    Friend WithEvents txtBenBalancePostInc As TextBox
    Friend WithEvents txtBenBalanceInc As TextBox
    Friend WithEvents txtBenBalance As TextBox
    Friend WithEvents lblBenAddress1 As Label
    Friend WithEvents lblBenAddress2 As Label
    Friend WithEvents lblBenZip As Label
    Friend WithEvents cbobenrelationship As ComboBox
    Friend WithEvents lblBenRelationship As Label
    Friend WithEvents lblBenSurname As Label
    Friend WithEvents lblBenMiddlename As Label
    Friend WithEvents lblBenFirstname As Label
    Friend WithEvents lblBenBalancePostInc As Label
    Friend WithEvents lblBenBalanceInc As Label
    Friend WithEvents lblBenName As Label
    Friend WithEvents lblBenRef As Label
    Friend WithEvents CboBenName As ComboBox
    Friend WithEvents CboBenAccount As ComboBox
    Friend WithEvents lblBenAccount As Label
    Friend WithEvents lblBenBalance As Label
    Friend WithEvents BankDetailsTab As TabPage
    Friend WithEvents lblbenmsg2 As LinkLabel
    Friend WithEvents lblbenmsg1 As LinkLabel
    Friend WithEvents txtBenBIK As TextBox
    Friend WithEvents txtBenINN As TextBox
    Friend WithEvents txtBenSwift As TextBox
    Friend WithEvents txtBenIBAN As TextBox
    Friend WithEvents txtBenOther1 As TextBox
    Friend WithEvents lblbenBIK As Label
    Friend WithEvents lblBenSwift As Label
    Friend WithEvents lblBenIBAN As Label
    Friend WithEvents lblbenINN As Label
    Friend WithEvents ChkBenSendRef As CheckBox
    Friend WithEvents ChkBenUrgent As CheckBox
    Friend WithEvents lblBenOther1 As Label
    Friend WithEvents PicBenSwift As PictureBox
    Friend WithEvents PicBenIBAN As PictureBox
    Friend WithEvents cboOrderCountry As ComboBox
    Friend WithEvents lblOrderCountry As Label
    Friend WithEvents BeneficiarySubBankGroupBox As GroupBox
    Friend WithEvents cboBenSubBankCountry As ComboBox
    Friend WithEvents txtBenSubbankINN As TextBox
    Friend WithEvents txtBenSubBankBIK As TextBox
    Friend WithEvents lblBenSubBankBIK As Label
    Friend WithEvents lblBenSubBankINN As Label
    Friend WithEvents ChkBenSubBank As CheckBox
    Friend WithEvents txtBenSubBankOther1 As TextBox
    Friend WithEvents PicBenSubBankIBAN As PictureBox
    Friend WithEvents PicBenSubBankSwift As PictureBox
    Friend WithEvents CboBenSubBankName As ComboBox
    Friend WithEvents lblBenSubBankSwift As Label
    Friend WithEvents txtBenSubBankIBAN As TextBox
    Friend WithEvents lblBenSubBankIBAN As Label
    Friend WithEvents txtBenSubBankSwift As TextBox
    Friend WithEvents lblBenSubBankOther1 As Label
    Friend WithEvents txtBenSubBankZip As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents txtBenSubBankAddress2 As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents txtBenSubBankAddress1 As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents cboBenBankCountry As ComboBox
    Friend WithEvents ChkBenTemplate As CheckBox
    Friend WithEvents ChkBenTemplateEdit As CheckBox
    Friend WithEvents lblbenmsg3 As LinkLabel
    Friend WithEvents GrpSendMessage As GroupBox
    Friend WithEvents txtRelatedRef As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboSendTo As ComboBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents cboFilterType As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboFilterLocation As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtFilterID As TextBox
    Friend WithEvents ChkShowHidden As CheckBox
    Friend WithEvents lblExchRate As Label
    Friend WithEvents txtExchRate As TextBox
    Friend WithEvents CmdSubmitBatch As Button
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
