﻿Public Class FrmBalances
    Dim BalConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet

    Private Sub BalLoadGrid(Optional ByVal varFilterWhereClause As String = "")
        Dim varSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If cboBreakdown.Text = "Portfolios" Then
                varSQL = "exec DolfinPaymentBalancesPortfolio " & FilterWhereClause()
            ElseIf cboBreakdown.Text = "Brokers" Then
                varSQL = "exec DolfinPaymentBalancesBroker " & FilterWhereClause()
            ElseIf cboBreakdown.Text = "Money Markets" Then
                varSQL = "exec DolfinPaymentBalancesMoneyMarkets " & FilterWhereClause()
            ElseIf cboBreakdown.Text = "Finance Overview by Broker" Then
                varSQL = "exec DolfinPaymentBalancesBroker " & FilterWhereClause(True)
            End If

            dgvViewBal.DataSource = Nothing
            BalConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, BalConn)
            ds = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(ds, "vwDolfinPaymentBalances")
            dgvViewBal.AutoGenerateColumns = True
            dgvViewBal.DataSource = ds.Tables("vwDolfinPaymentBalances")

            If cboBreakdown.Text = "Portfolios" Then
                dgvViewBal.Columns("Portfolio").Width = 300
                dgvViewBal.Columns("PortfolioType").Width = 100
                dgvViewBal.Columns("Account").Width = 250
            End If

            dgvViewBal.Columns("CCY").Width = 60
            dgvViewBal.Columns("Broker").Width = 150
            dgvViewBal.Columns("BrokerClass").Width = 150
            dgvViewBal.Columns("Balance").Width = 100
            dgvViewBal.Columns("Balance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvViewBal.Columns("Balance").DefaultCellStyle.Format = "N2"
            dgvViewBal.Columns("Balance").ValueType = GetType(SqlTypes.SqlMoney)

            dgvViewBal.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvViewBal.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvViewBal.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing balances grid")
        End Try
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvViewBal, 0)
        Cursor = Cursors.Default
    End Sub

    Private Function FilterWhereClause(Optional ByVal AllBranches As Boolean = False) As String
        Dim varWhereClauseFilter As String = ""

        varWhereClauseFilter = "'" & dtToDate.Value.ToString("yyyy-MM-dd") & "',"
        If Not AllBranches Then
            varWhereClauseFilter = varWhereClauseFilter & ClsIMS.UserLocationCode & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If cboBalClient.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & cboBalClient.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If cboBalPortfolio.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & cboBalPortfolio.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If cboBalCCY.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & cboBalCCY.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If cboBalAccount.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & cboBalAccount.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If CboBalBroker.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & CboBalBroker.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        If cboBalClass.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & cboBalClass.SelectedValue & ","
        Else
            varWhereClauseFilter = varWhereClauseFilter & "null,"
        End If

        FilterWhereClause = varWhereClauseFilter & IIf(ChkShowZero.Checked, "1", "0")
    End Function

    Private Sub FrmBalances_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If BalConn.State = ConnectionState.Open Then
            BalConn.Close()
        End If
    End Sub

    Private Sub FrmBalances_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvViewBal, True)
        ClsIMS.PopulateComboboxWithData(cboBalClient, "Select Cust_id,Name from vwDolfinPaymentClientPortfolio where branchCode = " & ClsIMS.UserLocationCode & " group by Cust_id,Name order by Name")
        cboBalClient.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBalClient.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboBalPortfolio, "Select pf_code,Portfolio from vwDolfinPaymentSelectAccountAll where branchCode = " & ClsIMS.UserLocationCode & " group by pf_code,Portfolio order by Portfolio")
        cboBalPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBalPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboBalCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccountAll where branchCode = " & ClsIMS.UserLocationCode & " group by CR_Code,CR_Name1 order by CR_Name1")
        cboBalCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBalCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboBalAccount, "Select t_code,t_name1 from vwDolfinPaymentSelectAccountAll where branchCode = " & ClsIMS.UserLocationCode & " group by t_code,t_name1 order by t_name1")
        cboBalAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBalAccount.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(CboBalBroker, "Select B_Code,B_Name1 from vwDolfinPaymentSelectAccountAll where branchCode = " & ClsIMS.UserLocationCode & " group by B_Code,B_Name1 order by B_Name1")
        CboBalBroker.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboBalBroker.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboBalClass, "Select B_Class,BrokerClass from vwDolfinPaymentSelectAccountAll where branchCode = " & ClsIMS.UserLocationCode & " group by B_Class,BrokerClass order by BrokerClass")
        cboBalClass.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboBalClass.AutoCompleteSource = AutoCompleteSource.ListItems

        cboBreakdown.Items.Add("Portfolios")
        cboBreakdown.Items.Add("Brokers")
        cboBreakdown.Items.Add("Money Markets")
        cboBreakdown.Text = "Portfolios"
        If ClsIMS.UserLocationCode = 42 Then
            cboBreakdown.Items.Add("Finance Overview by Broker")
        End If
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        ReRunGrid()
    End Sub

    Private Sub cboBreakdown_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBreakdown.SelectedIndexChanged
        ReRunGrid()
    End Sub

    Private Sub ReRunGrid()
        cboBalClient.Text = ""
        cboBalPortfolio.Text = ""
        cboBalCCY.Text = ""
        CboBalBroker.Text = ""
        cboBalClass.Text = ""
        cboBalAccount.Text = ""
        BalLoadGrid()
    End Sub

    Private Sub dgvViewBal_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvViewBal.CellFormatting
        If Not IsDBNull(dgvViewBal.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
            If IsNumeric(dgvViewBal.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                If CDbl(dgvViewBal.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) < 0 Then
                    dgvViewBal.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.ForeColor = Color.DarkRed
                Else
                    dgvViewBal.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.ForeColor = Color.Black
                End If
            End If
        End If
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        BalLoadGrid()
    End Sub
End Class