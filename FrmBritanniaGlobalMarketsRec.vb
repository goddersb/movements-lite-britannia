﻿Public Class FrmBritanniaGlobalMarketsRec
    Dim RecConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dsRec As New DataSet
    Dim dgvView As DataView

    Public Sub RunRec()
        Try
            Dim varSQL As String = "exec DolfinPaymentBGMRec  '" & dtRunDate.Value.ToString("yyyy-MM-dd") & "'"

            Cursor = Cursors.WaitCursor
            dgvRec.DataSource = Nothing
            RecConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RecConn)
            dsRec = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(dsRec, "TblRec")

            dgvRec.AutoGenerateColumns = True
            dgvRec.DataSource = dsRec.Tables("TblRec")

            dgvView = New DataView(dsRec.Tables("TblRec"))

            FormatGrid()
            dgvRec.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvRec.Refresh()

            Cursor = Cursors.Default

        Catch ex As Exception
            dgvRec.DataSource = Nothing
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub FormatGrid()
        dgvRec.Columns("RunDate").Width = 50
        dgvRec.Columns("MatchStatus").Width = 100
        dgvRec.Columns("DataSource").Width = 150
        dgvRec.Columns("Portfolio").Width = 80
        dgvRec.Columns("BGM_SubAccount").Width = 80
        dgvRec.Columns("BGM_Contract").Width = 50
        dgvRec.Columns("BGM_RollDeliveryDate").Width = 50
        dgvRec.Columns("BGM_CCY").Width = 80
        dgvRec.Columns("IMSCCY").Width = 50
        dgvRec.Columns("BGM_LotsSigned").Width = 50
        dgvRec.Columns("IMSQuantity").Width = 80
        dgvRec.Columns("BGM_AvgClosePrice").DefaultCellStyle.Format = "##,0.00000"
        dgvRec.Columns("IMSAvgPrice").DefaultCellStyle.Format = "##,0.00000"
        dgvRec.Columns("BGM_MTMValue").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("IMSMTMValue").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("MTMDifference").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("BGM_BusGroup").Width = 80
        dgvRec.Columns("BGM_Account").Width = 50
        dgvRec.Columns("BGM_Matched").Width = 50
        dgvRec.Columns("BGM_LinkedIMS").Width = 80
        dgvRec.Columns("BGM_MatchedQty").Width = 50
        dgvRec.Columns("BGM_MatchedMTM").Width = 50
    End Sub

    Private Sub cmdRunRec_Click(sender As Object, e As EventArgs) Handles cmdRunRec.Click
        RunRec()
    End Sub

    Private Sub FrmBritanniaGlobalMarketsRec_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvRec, True)
        dtRunDate.Value = ClsIMS.GetSQLFunction("select dbo.dolfinpaymentaddbusinessdays(getdate(),-1)")
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportEmailsToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvRec, 0)
        Cursor = Cursors.Default
    End Sub


End Class