﻿Imports System.ComponentModel

Public Class FrmEmail
    Dim CanClose As Boolean = True

    Private Sub CmdSend_Click(sender As Object, e As EventArgs) Handles CmdSend.Click
        Dim LstRecipients As New List(Of String)

        For varemailitem As Integer = 0 To LstEmailRecipients.Items.Count - 1
            LstRecipients.Add(LstEmailRecipients.Items(varemailitem))
        Next

        If LstRecipients.Count > 0 Then
            If ClsPayAuth.SelectedPaymentID = -1 Then
                Dim TmpFilePath As String = ClsPayAuth.TempPaymentFilePath
                ClsPay = ClsPayAuth
                ClsIMS.UpdatePaymentTable(0, ClsPayAuth.SelectedPaymentID)
                'ClsPay.PaymentFilePath = TmpFilePath
                'ClsPayAuth.ConfirmDirectory()
                ClsPayAuth.CopyPaymentFiles(IIf(TmpFilePath = "", ClsIMS.GetFilePath("TempPaymentFiles") & ClsPayAuth.Portfolio & "\", TmpFilePath), ClsPayAuth.PaymentFilePath)
                If ClsPayAuth.SelectedPaymentID > 0 Then
                    If ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketBuy Or ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketSell Then
                        ClsEmail.SendApproveMovementEmailMM(ChkAddFiles.Checked, LstRecipients, ChkCCMe.Checked)
                        MsgBox("The recipients have been emailed and the money markets movements have been booked awaiting authorisation", MsgBoxStyle.Information, "Email Sent")
                    Else
                        ClsEmail.SendApproveMovementEmailCompliance(ChkAddFiles.Checked, LstRecipients, ClsPayAuth.PaymentRequestedEmail, ChkCCMe.Checked)
                        MsgBox("The recipients have been emailed and the payment has been booked awaiting payments team authorisation", MsgBoxStyle.Information, "Email Sent")
                    End If
                Else
                    MsgBox("There has been an issue creating this payment." & vbNewLine & "Please try to create this payment again and if you get continuous problems, please contact systems support for assistance", vbCritical, "")
                End If
                ClsPay = New ClsPayment
                ClsPayAuth = New ClsPayment
            ElseIf ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingCompliance) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Then
                If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)) And ClsPayAuth.PaymentIDOther <> 0 Then
                    If ClsPayAuth.BatchPaymentTable Is Nothing Then
                        PopulateBatchTableIfEmpty(ClsPayAuth.PaymentIDOther)
                    End If
                    If ClsEmail.SendApproveMovementEmailBatchCompliance(ChkAddFiles.Checked, LstRecipients, ClsPayAuth.AboveThreshold, ChkCCMe.Checked) Then
                        MsgBox("The recipients have been emailed For the batch payments And awaiting payments team authorisation", MsgBoxStyle.Information, "Email Sent")
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Email batch request re-sent from SendApproveMovementEmailCompliance to payments team from " & ClsIMS.FullUserName, ClsPayAuth.SelectedPaymentID)
                        ClsPayAuth = New ClsPayment
                    Else
                        MsgBox("There has been an issue sending the batch payments team payments email. Please contact systems support For assistance", MsgBoxStyle.Information, "Email Not Sent")
                    End If
                Else
                    ClsEmail.SendApproveMovementEmailCompliance(ChkAddFiles.Checked, LstRecipients, ClsPayAuth.PaymentRequestedEmail, ChkCCMe.Checked)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Email request re-sent from SendApproveMovementEmailCompliance to payments team from " & ClsIMS.FullUserName, ClsPayAuth.SelectedPaymentID)
                    ClsPayAuth = New ClsPayment
                    MsgBox("The recipients have been emailed And the payment has been booked awaiting payments team authorisation", MsgBoxStyle.Information, "Email Sent")
                End If
            Else
                If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)) And ClsPayAuth.PaymentIDOther <> 0 Then
                    If ClsPayAuth.BatchPaymentTable Is Nothing Then
                        PopulateBatchTableIfEmpty(ClsPayAuth.PaymentIDOther)
                    End If
                    If ClsEmail.SendApproveMovementEmailBatch(ChkAddFiles.Checked, LstRecipients, ClsPayAuth.AboveThreshold, ChkCCMe.Checked) Then
                        MsgBox("The recipients have been emailed For the batch payments And awaiting authorisation", MsgBoxStyle.Information, "Email Sent")
                    End If
                ElseIf ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketBuy Or ClsPayAuth.OtherID = cPayDescInOtherMoneyMarketSell Then
                    ClsEmail.SendApproveMovementEmailMM(ChkAddFiles.Checked, LstRecipients, ChkCCMe.Checked)
                    MsgBox("The recipients have been emailed And the money markets movements have been booked awaiting authorisation", MsgBoxStyle.Information, "Email Sent")
                Else
                    If ClsEmail.SendApproveMovementEmail(ChkAddFiles.Checked, LstRecipients, ClsPayAuth.AboveThreshold, ChkCCMe.Checked) Then
                        MsgBox("The recipients have been emailed", MsgBoxStyle.Information, "Email Sent")
                    End If
                End If
            End If
            CanClose = True
            Me.Close()
        Else
            MsgBox("Please add recipients To send email", vbExclamation)
        End If
    End Sub

    Public Sub PopulateBatchTableIfEmpty(ByVal BatchID As Double)
        Dim TblBP As New DataTable
        PopulateBatchTable(TblBP)
        ClsIMS.GetReaderItemIntoDataTable(BatchID, TblBP)
        ClsPayAuth.BatchPaymentTable = TblBP
    End Sub

    Private Sub FrmEmail_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim LstGetRecipients As List(Of String)

        LstSelectRecipients.Items.Clear()
        LstEmailRecipients.Items.Clear()
        If ClsPayAuth.AboveThreshold Then
            If ClsPayAuth.SelectedPaymentID = -1 Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusAboveThreshold) Then
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_AuthoriseAboveThreshold = 1")
                LabelTextAndPoint(lblTitle, "Send Email Recipients to authorise Payment Over Threshold", 50, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email as payment is above the threshold", 24, 52)
            Else
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Authorise3rdPartyPayments = 1")
                LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email", 24, 52)
            End If
        ElseIf ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
            If ClsPayAuth.SelectedPaymentID = -1 Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingCompliance) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Then
                If ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Then
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('paymentsteam')")
                    'LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliancepayments','paymentsteam')")
                Else
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliancepaymentsmalta')")
                End If
                LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive payments team authorisation email", 24, 52)
            Else
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Authorise3rdPartyPayments = 1")
                LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email", 24, 52)
            End If
        ElseIf ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            If ClsPayAuth.Status = "Pending Account Mgt" Then
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username = 'accountmanagement' or U_AuthoriseAccountMgt = 1")
            ElseIf ClsPayAuth.Status = "Pending Account Mgt (Head)" Then
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username = 'accountmanagement' or U_AuthoriseAccountMgtLevel2 = 1")
            Else
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Authorise3rdPartyPayments = 1")
            End If
            LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
            LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email for Volopa payment", 24, 52)
        ElseIf ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Then
            If ClsPayAuth.SelectedPaymentID = -1 Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingCompliance) Or ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Then
                If ClsIMS.UserLocationCode = 47 Then
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliancepaymentsmalta')")
                Else
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('paymentsteam')")
                    'LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliancepayments','paymentsteam')")
                End If
                LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive payments team authorisation email", 24, 52)
            Else
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Authorise3rdPartyPayments = 1")
                LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
                LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email", 24, 52)
            End If
        Else
                LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_AuthorisePayments = 1")
            LabelTextAndPoint(lblTitle, "Send Email Recipients", 244, 9)
            LabelTextAndPoint(lblMsg, "Type email below or select user from listbox to receive authorisation email", 24, 52)
        End If
        ChkAddFiles.Enabled = True
        ChkCCMe.Enabled = True
        ClsPayAuth.SelectedEmailAttachments = True

        If Not LstGetRecipients Is Nothing Then
            For varEmailItem As Integer = 0 To LstGetRecipients.Count - 1
                LstSelectRecipients.Items.Add(LstGetRecipients(varEmailItem))
            Next
            ChkAddFiles.Checked = ClsPayAuth.SelectedEmailAttachments
        End If
    End Sub

    Private Sub CmdAdd_Click(sender As Object, e As EventArgs) Handles CmdAdd.Click
        If txtSelectRecipients.Text <> "" Then
            LstEmailRecipients.Items.Add(txtSelectRecipients.Text)
            txtSelectRecipients.Text = ""
        ElseIf LstSelectRecipients.SelectedItems.Count > 0 Then
            LstEmailRecipients.Items.Add(LstSelectRecipients.SelectedItem)
            LstSelectRecipients.Items.Remove(LstSelectRecipients.SelectedItem)
        End If
    End Sub

    Private Sub CmdRemove_Click(sender As Object, e As EventArgs) Handles CmdRemove.Click
        If LstEmailRecipients.SelectedItems.Count > 0 Then
            LstSelectRecipients.Items.Add(LstEmailRecipients.SelectedItem)
            LstEmailRecipients.Items.Remove(LstEmailRecipients.SelectedItem)
        End If
    End Sub

    Private Sub FrmEmail_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Not CanClose Then
            e.Cancel = True
        End If
    End Sub

End Class