﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmClientStatementGenerator
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientStatementGenerator))
        Me.dgvBalanceChecker = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboPortfolio = New System.Windows.Forms.ComboBox()
        Me.btnRunBalanceChecker = New System.Windows.Forms.Button()
        Me.dtToPicker = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtFromPicker = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvBalanceCheckResults = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.CRM = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.CmdSendEmail = New System.Windows.Forms.Button()
        Me.CmdGenerateReportsUnSent = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboFilterLog = New System.Windows.Forms.ComboBox()
        Me.CmdReportsLog = New System.Windows.Forms.Button()
        Me.CmdSendReports = New System.Windows.Forms.Button()
        Me.DtQtrFrom = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.CmdGenerateReports = New System.Windows.Forms.Button()
        Me.DtQtrTo = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ChkRefresh = New System.Windows.Forms.CheckBox()
        Me.CmdExportStatmentGridToExcel = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Operations = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.CmdRefreshFix = New System.Windows.Forms.Button()
        Me.dtToDatePickerRefresh = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CmdExportEmailsToExcel = New System.Windows.Forms.Button()
        Me.CmdResetGrid = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboStatus = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboPortfolioName = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdFilter = New System.Windows.Forms.Button()
        Me.dtToDatePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtFromDatePicker = New System.Windows.Forms.DateTimePicker()
        CType(Me.dgvBalanceChecker, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBalanceCheckResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.CRM.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Operations.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvBalanceChecker
        '
        Me.dgvBalanceChecker.AllowUserToAddRows = False
        Me.dgvBalanceChecker.AllowUserToDeleteRows = False
        Me.dgvBalanceChecker.AllowUserToOrderColumns = True
        Me.dgvBalanceChecker.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBalanceChecker.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBalanceChecker.ColumnHeadersHeight = 29
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBalanceChecker.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvBalanceChecker.Location = New System.Drawing.Point(5, 162)
        Me.dgvBalanceChecker.MultiSelect = False
        Me.dgvBalanceChecker.Name = "dgvBalanceChecker"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBalanceChecker.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvBalanceChecker.RowHeadersWidth = 51
        Me.dgvBalanceChecker.Size = New System.Drawing.Size(1677, 647)
        Me.dgvBalanceChecker.TabIndex = 71
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(21, 28)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Portfolio"
        '
        'cboPortfolio
        '
        Me.cboPortfolio.FormattingEnabled = True
        Me.cboPortfolio.Location = New System.Drawing.Point(70, 25)
        Me.cboPortfolio.Name = "cboPortfolio"
        Me.cboPortfolio.Size = New System.Drawing.Size(342, 21)
        Me.cboPortfolio.TabIndex = 31
        '
        'btnRunBalanceChecker
        '
        Me.btnRunBalanceChecker.Location = New System.Drawing.Point(431, 25)
        Me.btnRunBalanceChecker.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRunBalanceChecker.Name = "btnRunBalanceChecker"
        Me.btnRunBalanceChecker.Size = New System.Drawing.Size(164, 33)
        Me.btnRunBalanceChecker.TabIndex = 4
        Me.btnRunBalanceChecker.Text = "Email Report"
        Me.btnRunBalanceChecker.UseVisualStyleBackColor = True
        '
        'dtToPicker
        '
        Me.dtToPicker.Location = New System.Drawing.Point(280, 57)
        Me.dtToPicker.Margin = New System.Windows.Forms.Padding(2)
        Me.dtToPicker.Name = "dtToPicker"
        Me.dtToPicker.Size = New System.Drawing.Size(132, 20)
        Me.dtToPicker.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(227, 57)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "To Date:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 57)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "From Date:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtFromPicker
        '
        Me.dtFromPicker.Location = New System.Drawing.Point(70, 57)
        Me.dtFromPicker.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFromPicker.Name = "dtFromPicker"
        Me.dtFromPicker.Size = New System.Drawing.Size(135, 20)
        Me.dtFromPicker.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Maroon
        Me.Label1.Location = New System.Drawing.Point(-397, -169)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(399, 13)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "To balance check a portfolio, select portfolio and from/to dates below and click " &
    "run"
        '
        'dgvBalanceCheckResults
        '
        Me.dgvBalanceCheckResults.AllowUserToOrderColumns = True
        Me.dgvBalanceCheckResults.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBalanceCheckResults.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvBalanceCheckResults.ColumnHeadersHeight = 29
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvBalanceCheckResults.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvBalanceCheckResults.Location = New System.Drawing.Point(13, 154)
        Me.dgvBalanceCheckResults.MultiSelect = False
        Me.dgvBalanceCheckResults.Name = "dgvBalanceCheckResults"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBalanceCheckResults.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvBalanceCheckResults.RowHeadersWidth = 51
        Me.dgvBalanceCheckResults.Size = New System.Drawing.Size(1664, 655)
        Me.dgvBalanceCheckResults.TabIndex = 73
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.CRM)
        Me.TabControl1.Controls.Add(Me.Operations)
        Me.TabControl1.Location = New System.Drawing.Point(11, 11)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1695, 840)
        Me.TabControl1.TabIndex = 74
        '
        'CRM
        '
        Me.CRM.BackColor = System.Drawing.Color.FloralWhite
        Me.CRM.Controls.Add(Me.GroupBox3)
        Me.CRM.Controls.Add(Me.GroupBox1)
        Me.CRM.Controls.Add(Me.Label9)
        Me.CRM.Controls.Add(Me.dgvBalanceCheckResults)
        Me.CRM.Controls.Add(Me.Label1)
        Me.CRM.Location = New System.Drawing.Point(4, 22)
        Me.CRM.Margin = New System.Windows.Forms.Padding(2)
        Me.CRM.Name = "CRM"
        Me.CRM.Padding = New System.Windows.Forms.Padding(2)
        Me.CRM.Size = New System.Drawing.Size(1687, 814)
        Me.CRM.TabIndex = 0
        Me.CRM.Text = "CRM"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.CmdSendEmail)
        Me.GroupBox3.Controls.Add(Me.CmdGenerateReportsUnSent)
        Me.GroupBox3.Controls.Add(Me.GroupBox4)
        Me.GroupBox3.Controls.Add(Me.CmdReportsLog)
        Me.GroupBox3.Controls.Add(Me.CmdSendReports)
        Me.GroupBox3.Controls.Add(Me.DtQtrFrom)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.CmdGenerateReports)
        Me.GroupBox3.Controls.Add(Me.DtQtrTo)
        Me.GroupBox3.Location = New System.Drawing.Point(994, 11)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(688, 137)
        Me.GroupBox3.TabIndex = 119
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Quarterly Reports Statement Criteria"
        '
        'CmdSendEmail
        '
        Me.CmdSendEmail.Location = New System.Drawing.Point(458, 56)
        Me.CmdSendEmail.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdSendEmail.Name = "CmdSendEmail"
        Me.CmdSendEmail.Size = New System.Drawing.Size(225, 33)
        Me.CmdSendEmail.TabIndex = 9
        Me.CmdSendEmail.Text = "Send Email to Clients (Ticked items in log)"
        Me.CmdSendEmail.UseVisualStyleBackColor = True
        '
        'CmdGenerateReportsUnSent
        '
        Me.CmdGenerateReportsUnSent.Location = New System.Drawing.Point(458, 18)
        Me.CmdGenerateReportsUnSent.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdGenerateReportsUnSent.Name = "CmdGenerateReportsUnSent"
        Me.CmdGenerateReportsUnSent.Size = New System.Drawing.Size(225, 33)
        Me.CmdGenerateReportsUnSent.TabIndex = 8
        Me.CmdGenerateReportsUnSent.Text = "Generate Client Quarterly Reports (Un-Sent)"
        Me.CmdGenerateReportsUnSent.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cboFilterLog)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 75)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(200, 56)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Filter Log"
        '
        'cboFilterLog
        '
        Me.cboFilterLog.FormattingEnabled = True
        Me.cboFilterLog.Location = New System.Drawing.Point(13, 23)
        Me.cboFilterLog.Name = "cboFilterLog"
        Me.cboFilterLog.Size = New System.Drawing.Size(181, 21)
        Me.cboFilterLog.TabIndex = 0
        '
        'CmdReportsLog
        '
        Me.CmdReportsLog.Location = New System.Drawing.Point(224, 99)
        Me.CmdReportsLog.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdReportsLog.Name = "CmdReportsLog"
        Me.CmdReportsLog.Size = New System.Drawing.Size(459, 33)
        Me.CmdReportsLog.TabIndex = 6
        Me.CmdReportsLog.Text = "View Generated Reports Log"
        Me.CmdReportsLog.UseVisualStyleBackColor = True
        '
        'CmdSendReports
        '
        Me.CmdSendReports.Location = New System.Drawing.Point(224, 56)
        Me.CmdSendReports.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdSendReports.Name = "CmdSendReports"
        Me.CmdSendReports.Size = New System.Drawing.Size(230, 33)
        Me.CmdSendReports.TabIndex = 5
        Me.CmdSendReports.Text = "Send Quarterly Reports to MyDolfin (Un-Sent)"
        Me.CmdSendReports.UseVisualStyleBackColor = True
        '
        'DtQtrFrom
        '
        Me.DtQtrFrom.Enabled = False
        Me.DtQtrFrom.Location = New System.Drawing.Point(74, 24)
        Me.DtQtrFrom.Margin = New System.Windows.Forms.Padding(2)
        Me.DtQtrFrom.Name = "DtQtrFrom"
        Me.DtQtrFrom.Size = New System.Drawing.Size(135, 20)
        Me.DtQtrFrom.TabIndex = 0
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(11, 24)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "From Date:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(19, 50)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(49, 13)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "To Date:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CmdGenerateReports
        '
        Me.CmdGenerateReports.Location = New System.Drawing.Point(224, 18)
        Me.CmdGenerateReports.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdGenerateReports.Name = "CmdGenerateReports"
        Me.CmdGenerateReports.Size = New System.Drawing.Size(230, 33)
        Me.CmdGenerateReports.TabIndex = 4
        Me.CmdGenerateReports.Text = "Generate Client Quarterly Reports (ALL)"
        Me.CmdGenerateReports.UseVisualStyleBackColor = True
        '
        'DtQtrTo
        '
        Me.DtQtrTo.Enabled = False
        Me.DtQtrTo.Location = New System.Drawing.Point(74, 50)
        Me.DtQtrTo.Margin = New System.Windows.Forms.Padding(2)
        Me.DtQtrTo.Name = "DtQtrTo"
        Me.DtQtrTo.Size = New System.Drawing.Size(135, 20)
        Me.DtQtrTo.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ChkRefresh)
        Me.GroupBox1.Controls.Add(Me.CmdExportStatmentGridToExcel)
        Me.GroupBox1.Controls.Add(Me.cboPortfolio)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtFromPicker)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.btnRunBalanceChecker)
        Me.GroupBox1.Controls.Add(Me.dtToPicker)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(851, 99)
        Me.GroupBox1.TabIndex = 118
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Statement Criteria"
        '
        'ChkRefresh
        '
        Me.ChkRefresh.AutoSize = True
        Me.ChkRefresh.Location = New System.Drawing.Point(431, 73)
        Me.ChkRefresh.Name = "ChkRefresh"
        Me.ChkRefresh.Size = New System.Drawing.Size(416, 17)
        Me.ChkRefresh.TabIndex = 122
        Me.ChkRefresh.Text = "Refresh/Fix every day between report dates selected (Could take time to complete)" &
    ""
        Me.ChkRefresh.UseVisualStyleBackColor = True
        '
        'CmdExportStatmentGridToExcel
        '
        Me.CmdExportStatmentGridToExcel.Image = CType(resources.GetObject("CmdExportStatmentGridToExcel.Image"), System.Drawing.Image)
        Me.CmdExportStatmentGridToExcel.Location = New System.Drawing.Point(610, 25)
        Me.CmdExportStatmentGridToExcel.Name = "CmdExportStatmentGridToExcel"
        Me.CmdExportStatmentGridToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportStatmentGridToExcel.TabIndex = 121
        Me.CmdExportStatmentGridToExcel.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label9.Location = New System.Drawing.Point(9, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(260, 24)
        Me.Label9.TabIndex = 117
        Me.Label9.Text = "Client Statement Generator"
        '
        'Operations
        '
        Me.Operations.BackColor = System.Drawing.Color.FloralWhite
        Me.Operations.Controls.Add(Me.GroupBox5)
        Me.Operations.Controls.Add(Me.Label10)
        Me.Operations.Controls.Add(Me.GroupBox2)
        Me.Operations.Controls.Add(Me.dgvBalanceChecker)
        Me.Operations.Location = New System.Drawing.Point(4, 22)
        Me.Operations.Margin = New System.Windows.Forms.Padding(2)
        Me.Operations.Name = "Operations"
        Me.Operations.Padding = New System.Windows.Forms.Padding(2)
        Me.Operations.Size = New System.Drawing.Size(1687, 814)
        Me.Operations.TabIndex = 1
        Me.Operations.Text = "Operations"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label15)
        Me.GroupBox5.Controls.Add(Me.CmdRefreshFix)
        Me.GroupBox5.Controls.Add(Me.dtToDatePickerRefresh)
        Me.GroupBox5.Location = New System.Drawing.Point(1297, 43)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(372, 113)
        Me.GroupBox5.TabIndex = 120
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Refresh Criteria"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(8, 26)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(49, 13)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "To Date:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CmdRefreshFix
        '
        Me.CmdRefreshFix.Location = New System.Drawing.Point(213, 18)
        Me.CmdRefreshFix.Margin = New System.Windows.Forms.Padding(2)
        Me.CmdRefreshFix.Name = "CmdRefreshFix"
        Me.CmdRefreshFix.Size = New System.Drawing.Size(152, 33)
        Me.CmdRefreshFix.TabIndex = 4
        Me.CmdRefreshFix.Text = "Refresh/Fix Date Selected"
        Me.CmdRefreshFix.UseVisualStyleBackColor = True
        '
        'dtToDatePickerRefresh
        '
        Me.dtToDatePickerRefresh.Location = New System.Drawing.Point(63, 26)
        Me.dtToDatePickerRefresh.Margin = New System.Windows.Forms.Padding(2)
        Me.dtToDatePickerRefresh.Name = "dtToDatePickerRefresh"
        Me.dtToDatePickerRefresh.Size = New System.Drawing.Size(135, 20)
        Me.dtToDatePickerRefresh.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label10.Location = New System.Drawing.Point(5, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(245, 24)
        Me.Label10.TabIndex = 118
        Me.Label10.Text = "Operations Statement Fix"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CmdExportEmailsToExcel)
        Me.GroupBox2.Controls.Add(Me.CmdResetGrid)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.cboStatus)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.cboPortfolioName)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cmdFilter)
        Me.GroupBox2.Controls.Add(Me.dtToDatePicker)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.dtFromDatePicker)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 43)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(830, 113)
        Me.GroupBox2.TabIndex = 72
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fix Criteria"
        '
        'CmdExportEmailsToExcel
        '
        Me.CmdExportEmailsToExcel.Image = CType(resources.GetObject("CmdExportEmailsToExcel.Image"), System.Drawing.Image)
        Me.CmdExportEmailsToExcel.Location = New System.Drawing.Point(780, 36)
        Me.CmdExportEmailsToExcel.Name = "CmdExportEmailsToExcel"
        Me.CmdExportEmailsToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportEmailsToExcel.TabIndex = 120
        Me.CmdExportEmailsToExcel.UseVisualStyleBackColor = True
        '
        'CmdResetGrid
        '
        Me.CmdResetGrid.Location = New System.Drawing.Point(652, 79)
        Me.CmdResetGrid.Name = "CmdResetGrid"
        Me.CmdResetGrid.Size = New System.Drawing.Size(112, 21)
        Me.CmdResetGrid.TabIndex = 119
        Me.CmdResetGrid.Text = "Reset Filter"
        Me.CmdResetGrid.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(14, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label11.Size = New System.Drawing.Size(37, 13)
        Me.Label11.TabIndex = 87
        Me.Label11.Text = "Status"
        '
        'cboStatus
        '
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(64, 79)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(136, 21)
        Me.cboStatus.TabIndex = 86
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 85
        Me.Label8.Text = "Portfolio"
        '
        'cboPortfolioName
        '
        Me.cboPortfolioName.FormattingEnabled = True
        Me.cboPortfolioName.Location = New System.Drawing.Point(64, 52)
        Me.cboPortfolioName.Name = "cboPortfolioName"
        Me.cboPortfolioName.Size = New System.Drawing.Size(355, 21)
        Me.cboPortfolioName.TabIndex = 84
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(445, 75)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(49, 13)
        Me.Label6.TabIndex = 83
        Me.Label6.Text = "To Date:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdFilter
        '
        Me.cmdFilter.Location = New System.Drawing.Point(652, 37)
        Me.cmdFilter.Name = "cmdFilter"
        Me.cmdFilter.Size = New System.Drawing.Size(112, 36)
        Me.cmdFilter.TabIndex = 81
        Me.cmdFilter.Text = "Filter Data Grid"
        Me.cmdFilter.UseVisualStyleBackColor = True
        '
        'dtToDatePicker
        '
        Me.dtToDatePicker.Location = New System.Drawing.Point(498, 75)
        Me.dtToDatePicker.Margin = New System.Windows.Forms.Padding(2)
        Me.dtToDatePicker.Name = "dtToDatePicker"
        Me.dtToDatePicker.Size = New System.Drawing.Size(135, 20)
        Me.dtToDatePicker.TabIndex = 82
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.Maroon
        Me.Label5.Location = New System.Drawing.Point(9, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(319, 13)
        Me.Label5.TabIndex = 80
        Me.Label5.Text = "Set From/To date below to view client statements that need fixing:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(435, 49)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 13)
        Me.Label7.TabIndex = 79
        Me.Label7.Text = "From Date:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtFromDatePicker
        '
        Me.dtFromDatePicker.Location = New System.Drawing.Point(497, 49)
        Me.dtFromDatePicker.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFromDatePicker.Name = "dtFromDatePicker"
        Me.dtFromDatePicker.Size = New System.Drawing.Size(135, 20)
        Me.dtFromDatePicker.TabIndex = 78
        '
        'FrmClientStatementGenerator
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1717, 862)
        Me.Controls.Add(Me.TabControl1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "FrmClientStatementGenerator"
        Me.Text = "Client Statement Generator"
        CType(Me.dgvBalanceChecker, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBalanceCheckResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.CRM.ResumeLayout(False)
        Me.CRM.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Operations.ResumeLayout(False)
        Me.Operations.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents dgvBalanceChecker As DataGridView
    Friend WithEvents btnRunBalanceChecker As Button
    Friend WithEvents dtToPicker As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dtFromPicker As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents dgvBalanceCheckResults As DataGridView
    Friend WithEvents Label4 As Label
    Friend WithEvents cboPortfolio As ComboBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents CRM As TabPage
    Friend WithEvents Operations As TabPage
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cboPortfolioName As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cmdFilter As Button
    Friend WithEvents dtToDatePicker As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents dtFromDatePicker As DateTimePicker
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents CmdSendReports As Button
    Friend WithEvents DtQtrFrom As DateTimePicker
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents CmdGenerateReports As Button
    Friend WithEvents DtQtrTo As DateTimePicker
    Friend WithEvents CmdReportsLog As Button
    Friend WithEvents CmdResetGrid As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents cboStatus As ComboBox
    Friend WithEvents CmdExportEmailsToExcel As Button
    Friend WithEvents CmdExportStatmentGridToExcel As Button
    Friend WithEvents ChkRefresh As CheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents cboFilterLog As ComboBox
    Friend WithEvents CmdGenerateReportsUnSent As Button
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents Label15 As Label
    Friend WithEvents CmdRefreshFix As Button
    Friend WithEvents dtToDatePickerRefresh As DateTimePicker
    Friend WithEvents CmdSendEmail As Button
End Class
