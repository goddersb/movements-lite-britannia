﻿Imports System.Data.SqlClient
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.IO
Imports System.Reflection

Public Class ClsIMSData
    Private varFLOWVersion As String = "Brtiannia.2.7.3"
    'Private varIMSConnection As String = "Integrated Security=True;Initial Catalog=DolfinPayment;Server=IMSDB01.ims-britannia.local;MultipleActiveResultSets=true"
    Private varIMSConnection As String = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=DolfinPayment;Data Source=imsdb01.ims-britannia.local;MultipleActiveResultSets=true"
    Private varIMSDb As String = ""
    Private varIFTDb As String = ""
    Private conn As SqlConnection

    'LIVE
    Private varMappDriveUser As String = "IMS-BRITANNIA\da-dharper"
    Private varMappDrivePwd As String = "8sHji9!is"

    Private varUserNameDomain As String = Environment.UserDomainName '"BRITANNIA\"
    Private varUserName As String = Environment.UserDomainName & "\" & Environment.UserName

    'TEST
    'Private varUserName As String = varUserNameDomain & Replace(Environment.UserName, "da-", "")
    'Private varUserName As String = varUserNameDomain & "DHarper"

    Private varUserMachine As String = Environment.MachineName
    Private varUserCanLogin As Boolean = False
    Private varUserLocationCode As Integer = 42
    Private varUserLocation As String = "London"
    Private varUserOptRefresh As Boolean = False
    Private varUserOptInActivity As Integer = 1
    Private varUserOptGridSort As Integer
    Private varFilePaths As New Dictionary(Of String, String)
    Private varFileExportNames As New Dictionary(Of String, String)
    Private varDolfinAddress As New Dictionary(Of String, String)
    Private varPaymentTypes As New Dictionary(Of Integer, String)
    Private varPaymentAllTypes As New Dictionary(Of Integer, String)
    Private varPaymentTypesShortCodes As New Dictionary(Of Integer, String)
    Private varPaymentOtherTypes As New Dictionary(Of Integer, String)
    Private varLocationTypes As New Dictionary(Of Integer, String)
    Private varStatusTypes As New Dictionary(Of Integer, String)
    Private varPayCheckTypes As New Dictionary(Of Integer, String)
    Private varMessageTypes As New Dictionary(Of Integer, String)
    Private varMessageTypesRD As New Dictionary(Of Integer, String)
    Private varMessageTypesAuto As New Dictionary(Of Integer, String)
    Private varPaymentTypeManagementFees As New Dictionary(Of Integer, String)
    Private varAuditLogStatusTypes As New Dictionary(Of Integer, String)
    Private varAuditLogGroupNames As New Dictionary(Of Integer, String)

    Property FLOWVersion() As String
        Get
            Return varFLOWVersion
        End Get
        Set(value As String)
            varFLOWVersion = value
        End Set
    End Property

    Property FullUserName() As String
        Get
            Return varUserName
        End Get
        Set(value As String)
            varUserName = value
        End Set
    End Property

    Property UserLocationCode() As Integer
        Get
            Return varUserLocationCode
        End Get
        Set(value As Integer)
            varUserLocationCode = value
        End Set
    End Property

    Property UserLocation() As String
        Get
            Return varUserLocation
        End Get
        Set(value As String)
            varUserLocation = value
        End Set
    End Property

    Property MappDriveUser() As String
        Get
            Return varMappDriveUser
        End Get
        Set(value As String)
            varMappDriveUser = value
        End Set
    End Property

    Property MappDrivePwd() As String
        Get
            Return varMappDrivePwd
        End Get
        Set(value As String)
            varMappDrivePwd = value
        End Set
    End Property

    Property UserCanLogin() As Boolean
        Get
            Return varUserCanLogin
        End Get
        Set(value As Boolean)
            varUserCanLogin = value
        End Set
    End Property

    Property UserOptRefresh() As Boolean
        Get
            Return varUserOptRefresh
        End Get
        Set(value As Boolean)
            varUserOptRefresh = value
        End Set
    End Property

    Property UserOptInActivity() As Integer
        Get
            Return varUserOptInActivity
        End Get
        Set(value As Integer)
            varUserOptInActivity = value
        End Set
    End Property

    Property UserOptGridSort() As Integer
        Get
            Return varUserOptGridSort
        End Get
        Set(value As Integer)
            varUserOptGridSort = value
        End Set
    End Property

    Property GetFilePath(ByVal FilePathName As String) As String
        Get
            Return varFilePaths(FilePathName)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetFileExportName(ByVal varFileExportKey As String) As String
        Get
            Return varFileExportNames(varFileExportKey)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetDolfinAddress(ByVal varID As String) As String
        Get
            Return varDolfinAddress(varID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetPaymentType(ByVal varPaytypeID As Integer) As String
        Get
            Return varPaymentTypes(varPaytypeID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetPaymentOtherType(ByVal varPayOthertypeID As Integer) As String
        Get
            GetPaymentOtherType = ""
            If varPayOthertypeID <> 0 Then
                Return varPaymentOtherTypes(varPayOthertypeID)
            End If
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetPaymentTypeShortCode(ByVal varPaytypeSCID As Integer) As String
        Get
            Return varPaymentTypesShortCodes(varPaytypeSCID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetStatusType(ByVal varStatusTypeID As Integer) As String
        Get
            Return varStatusTypes(varStatusTypeID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetMessageType(ByVal varMsgID As Integer) As String
        Get
            If varMsgID <> 0 Then
                Return varMessageTypes(varMsgID)
            Else
                GetMessageType = ""
            End If
        End Get
        Set(value As String)

        End Set
    End Property

    Property GetAuditLogStatusTypes(ByVal varStatusTypeID As Integer) As String
        Get
            Return varAuditLogStatusTypes(varStatusTypeID)
        End Get
        Set(value As String)

        End Set
    End Property

    Property IMSDB() As String
        Get
            Return varIMSDb
        End Get
        Set(value As String)
            varIMSDb = value
        End Set
    End Property

    Property IMSIFTDB() As String
        Get
            Return varIFTDb
        End Get
        Set(value As String)
            varIFTDb = value
        End Set
    End Property

    Public Sub GetIMSDB()
        varIMSDb = GetSQLItem("SELECT COALESCE(PARSENAME(base_object_name,3),DB_NAME(DB_ID())) AS dbName FROM sys.synonyms where name = 'portfolios'")
        varIFTDb = GetSQLItem("SELECT COALESCE(PARSENAME(base_object_name,3),DB_NAME(DB_ID())) AS dbName FROM sys.synonyms where name = 'IFT_FileLog'")
    End Sub

    Public Function GetNewOpenConnection() As SqlConnection
        Dim newconn = New SqlConnection
        Try
            newconn.ConnectionString = varIMSConnection

            If newconn.State <> ConnectionState.Open Then
                newconn.Open()
            End If
            'GetNewOpenConnection = newconn
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        Finally
            GetNewOpenConnection = newconn
        End Try
    End Function

    Public Function GetCurrentConnection() As SqlConnection
        GetCurrentConnection = Nothing
        If conn.State = ConnectionState.Open Then
            GetCurrentConnection = conn
        End If
    End Function

    Public Sub ExecuteString(ByVal execonn As SqlConnection, ByVal varSQL As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = execonn
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            Cmd.ExecuteNonQuery()
            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in ExecuteString")
            Cmd = Nothing
        End Try
    End Sub

    Public Function GetSQLData(ByVal varSQL As String) As SqlDataReader
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = GetNewOpenConnection()
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            GetSQLData = Cmd.ExecuteReader()
            Return GetSQLData
            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in GetSQLData")
            GetSQLData = Nothing
        End Try
    End Function

    Public Sub ExecuteStringWithThrow(ByVal execonn As SqlConnection, ByVal varSQL As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = execonn
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            Cmd.ExecuteNonQuery()
            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in ExecuteString")
            Cmd = Nothing
            Throw ex
        End Try
    End Sub

    Public Function GetSQLDataWithThrow(ByVal varSQL As String) As SqlDataReader
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = GetNewOpenConnection()
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            GetSQLDataWithThrow = Cmd.ExecuteReader()
            Return GetSQLDataWithThrow
            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in GetSQLData")
            GetSQLDataWithThrow = Nothing
            Throw ex
        End Try
    End Function

    Public Function GetSQLDataNonQuery(ByVal varSQL As String) As Integer
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = GetNewOpenConnection()
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            GetSQLDataNonQuery = Cmd.ExecuteNonQuery
            Return GetSQLDataNonQuery
            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in GetSQLDataNonQuery")
            GetSQLDataNonQuery = Nothing
        End Try
    End Function

    Public Function GetSQLFunction(ByVal varSQL As String) As String
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = GetNewOpenConnection()
            Cmd.CommandText = varSQL
            Cmd.CommandType = CommandType.Text
            GetSQLFunction = Cmd.ExecuteScalar.ToString

            Cmd = Nothing
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in GetSQLFunction")
            GetSQLFunction = Nothing
        End Try
    End Function

    Public Function GetSQLItem(ByVal varSQL As String) As String
        Dim reader As SqlDataReader

        Try
            GetSQLItem = ""
            reader = GetSQLData(varSQL)
            If reader.HasRows Then
                While (reader.Read())
                    GetSQLItem = reader(0).ToString()
                End While
            End If
            reader.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConnectivity, Err.Description & ", sql:" & varSQL & " - error in GetSQLItem")
            GetSQLItem = Nothing
        End Try
    End Function

    Public Function GetTransactionData(SqlString As String) As DataSet
        Dim SICConn As SqlClient.SqlConnection = GetNewOpenConnection()
        Dim SQLDA As SqlDataAdapter = New SqlDataAdapter(SqlString, SICConn)
        Dim SQLDS As DataSet = New DataSet

        Try
            SQLDA.Fill(SQLDS)
            SICConn.Close()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return SQLDS
    End Function

    Public Function GenerateRef(ByVal varPaymentType As String, ByVal varSelectedPaymentID As Double, ByVal OtherID As Integer) As String
        GenerateRef = ""
        GenerateRef = GetSQLFunction("Select dbo.DolfinPaymentGenerateRef('" & varPaymentType & "'," & varSelectedPaymentID & "," & OtherID & ")")
    End Function

    Public Function GetRef(ByVal varSelectedPaymentID As Double) As String
        GetRef = ""
        GetRef = GetSQLFunction("Select dbo.DolfinPaymentGetRef(" & varSelectedPaymentID & ")")
    End Function

    Public Function IsISINRestricted(ByVal varISIN As String) As Integer
        IsISINRestricted = 0
        Dim tmpIsISINRestricted As String = GetSQLFunction("Select dolfinorders.dbo.IsBlockedInstrument('" & varISIN & "'," & UserLocationCode & ",'" & Replace(varUserName, varUserNameDomain, "") & "')")
        If IsNumeric(tmpIsISINRestricted) Or LCase(tmpIsISINRestricted) = "true" Then
            IsISINRestricted = 1
        End If
    End Function


    Public Function HasFunds(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double, ByVal varAmount As Double) As Boolean
        HasFunds = True
        Dim tmpHasFunds As String = GetSQLFunction("Select dbo.DolfinPaymentHasFunds(" & varPortfolioCode & "," & varCCYCode & "," & varAmount & ")")
        If tmpHasFunds = "" Or tmpHasFunds = "False" Then
            HasFunds = False
        End If
    End Function

    Public Function VolopaInLimits(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double, ByVal varAmount As Double) As Boolean
        VolopaInLimits = True
        Dim tmpVolopaInLimits As String = GetSQLFunction("Select dbo.DolfinPaymentVolopaInLimits(" & varPortfolioCode & "," & varCCYCode & "," & varAmount & ")")
        If tmpVolopaInLimits = "" Or tmpVolopaInLimits = "False" Then
            VolopaInLimits = False
        End If
    End Function

    Public Function HasFundsInPaymentAccount(ByVal varID As Double) As String
        HasFundsInPaymentAccount = ""
        HasFundsInPaymentAccount = GetSQLFunction("Select dbo.DolfinPaymentHasFundsInPaymentAccount(" & varID & ")")
    End Function

    Public Function HasMMHoldings(ByVal varPortfolioCode As Double, ByVal varInstrumentCode As Double, ByVal varCCYCode As Double, ByVal varAmount As Double) As Boolean
        HasMMHoldings = True
        Dim tmpHasMMHoldings As String = GetSQLFunction("Select dbo.DolfinPaymentHasMMHolding(" & varPortfolioCode & "," & varInstrumentCode & "," & varCCYCode & "," & varAmount & ")")
        If tmpHasMMHoldings = "" Or tmpHasMMHoldings = "False" Then
            HasMMHoldings = False
        End If
    End Function

    Public Function GetCCYRate(ByVal varDate As Date, ByVal varCCY As String, ByVal varCCYBase As String) As String
        GetCCYRate = ""
        GetCCYRate = GetSQLFunction("Select dbo.ufGetParity('" & varDate.ToString("yyyy-MM-dd") & "',(select cr_code from currencies where cr_name1 = '" & varCCY & "'),(select cr_code from currencies where cr_name1 = '" & varCCYBase & "'))")
    End Function

    Public Sub DeleteTemplate(ByVal varTemplateName As String)
        ExecuteString(conn, "Update vwDolfinPaymentAddress set Pa_TemplateName = '' where Pa_TemplateName = '" & varTemplateName & "'")
    End Sub

    Public Function GetDataToList(ByVal varSQL As String) As Dictionary(Of String, String)
        Dim reader As SqlDataReader = GetSQLData(varSQL)
        Dim Lst As New Dictionary(Of String, String)

        GetDataToList = Nothing
        Try
            If reader.HasRows Then
                GetDataToList = PopulateAllFieldsReaderToList(reader)
            End If
            reader.Close()
        Catch ex As Exception

        End Try
    End Function

    Public Function GetMultipleDataToList(ByVal varSQL As String) As List(Of String)
        Dim reader As SqlDataReader = GetSQLData(varSQL)
        Dim Lst As New List(Of String)

        If reader.HasRows Then
            GetMultipleDataToList = PopulateMultipleReaderToList(reader)
        Else
            GetMultipleDataToList = Nothing
        End If
        reader.Close()
    End Function

    Public Function AvailableSubmissions() As Boolean
        Dim reader As SqlDataReader = GetSQLData("select P_Status from vwDolfinPaymentGridView where p_status = '" & GetStatusType(cStatusReady) & "'  and cast(P_TransDate as date) <= cast(getdate() as date)")

        AvailableSubmissions = False
        If reader.HasRows Then
            AvailableSubmissions = True
        End If
        reader.Close()
    End Function

    Public Function AvailableSubmissionsBatch() As Boolean
        Dim reader As SqlDataReader = GetSQLData("select P_Status from vwDolfinPaymentGridView where p_status = '" & GetStatusType(cStatusBatchReady) & "'  and cast(P_TransDate as date) <= cast(getdate() as date)")

        AvailableSubmissionsBatch = False
        If reader.HasRows Then
            AvailableSubmissionsBatch = True
        End If
        reader.Close()
    End Function

    Public Function IsTemplateNameUnique(ByVal varTemplateName As String) As Boolean
        Dim reader As SqlDataReader = GetSQLData("select Pa_TemplateName from vwDolfinPaymentAddress where lower(Pa_TemplateName) = '" & LCase(varTemplateName) & "'")

        IsTemplateNameUnique = False
        If Not reader.HasRows Then
            IsTemplateNameUnique = True
        End If
        reader.Close()
    End Function

    Public Function GetTemplateFormData(ByVal varPortfolioCode As Integer, ByVal varCCYcode As Integer, ByVal varTemplateName As String) As Dictionary(Of String, String)
        GetTemplateFormData = Nothing
        Dim reader As SqlDataReader = GetSQLData("select Pa_ID,Pa_Portfolio,Pa_CCY,Pa_BeneficiaryTypeID,Pa_Name,Pa_Firstname,Pa_Middlename,Pa_Surname,Pa_Address1,Pa_Address2,Pa_County,Pa_Zip,Pa_CountryID,Pa_Country,Pa_Swift,Pa_IBAN,Pa_Other,Pa_BIK,Pa_INN,Pa_RelationshipID,Pa_TemplateSelected from vwDolfinPaymentTemplates where lower(Pa_TemplateName) = '" & LCase(varTemplateName) & "' and Pa_PortfolioCode = " & varPortfolioCode & " and Pa_CCYCode = " & varCCYcode)
        Dim Lst As New Dictionary(Of String, String)
        Dim varKeyString As String = ""

        Try
            If Not reader Is Nothing Then
                If reader.HasRows Then
                    While (reader.Read())
                        For vField = 0 To reader.FieldCount - 1
                            varKeyString = IIf(reader("Pa_TemplateSelected") = 2, "Ben " & reader.GetName(vField).ToString, IIf(reader("Pa_TemplateSelected") = 3, "SubBank " & reader.GetName(vField).ToString, IIf(reader("Pa_TemplateSelected") = 4, "Bank " & reader.GetName(vField).ToString, reader.GetName(vField).ToString)))
                            If Not Lst.ContainsKey(varKeyString) Then
                                Lst.Add(varKeyString, reader(vField).ToString)
                            End If
                        Next
                    End While
                    GetTemplateFormData = Lst
                Else
                    GetTemplateFormData = Nothing
                End If
            End If
        Catch ex As Exception
            GetTemplateFormData = Nothing
        Finally
            If Not reader Is Nothing Then
                reader.Close()
            End If
        End Try
    End Function

    Public Function GetMyEmailAddress() As String
        GetMyEmailAddress = GetSQLItem("Select U_EmailAddress from vwDolfinPaymentUsers where U_Username = '" & varUserName & "'")
    End Function

    Public Sub PopulateComboboxWithData(ByRef cbo As System.Windows.Forms.ComboBox, ByVal varSQL As String, Optional ByVal CboReset As Boolean = True)
        Dim dt = New DataTable
        Dim Reader As SqlDataReader = GetSQLData(varSQL)

        If Reader.HasRows Then
            dt.Load(Reader)

            cbo.DataSource = dt
            cbo.DisplayMember = dt.Columns(dt.Columns.Item(1).ColumnName).ToString
            cbo.ValueMember = dt.Columns(dt.Columns.Item(0).ColumnName).ToString

            If CboReset Then
                cbo.SelectedIndex = -1
            End If
            cbo.Text = ""
        Else
            cbo.DataSource = Nothing
        End If
        Reader.Close()
    End Sub

    Public Function GetPortfolioCCYBalance(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As Double
        GetPortfolioCCYBalance = 0
        Dim CCYBalanceUnblockedIncPending As String = GetSQLFunction("Select dbo.DolfinPaymentGetCCYBalance(" & varPortfolioCode & "," & varCCYCode & ")")
        If IsNumeric(CCYBalanceUnblockedIncPending) Then
            GetPortfolioCCYBalance = CDbl(CCYBalanceUnblockedIncPending)
        End If
    End Function

    Public Function GetPortfolioFeeAccountCode(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As Double
        GetPortfolioFeeAccountCode = 0
        Dim tmpAccountCode As String = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeAccountCode(" & varPortfolioCode & "," & varCCYCode & ")")
        If IsNumeric(tmpAccountCode) Then
            GetPortfolioFeeAccountCode = CDbl(tmpAccountCode)
        End If
    End Function

    Public Function GetPortfolioFeeRMSAccountCode(ByVal varCCYCode As Double) As Double
        Dim tmpAccountCode As String = ""
        GetPortfolioFeeRMSAccountCode = 0
        If UserLocationCode = 42 Then
            tmpAccountCode = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeRMSAccountCode(" & varCCYCode & ")")
        ElseIf UserLocationCode = 47 Then
            tmpAccountCode = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeRMSAccountCodeMalta(" & varCCYCode & ")")
        End If

        If IsNumeric(tmpAccountCode) Then
            GetPortfolioFeeRMSAccountCode = CDbl(tmpAccountCode)
        End If
    End Function

    Public Function GetPortfolioFeeAccountCodeFX(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As Double
        GetPortfolioFeeAccountCodeFX = 0
        Dim tmpAccountCode As String = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeAccountCodeFX(" & varPortfolioCode & "," & varCCYCode & ")")
        If IsNumeric(tmpAccountCode) Then
            GetPortfolioFeeAccountCodeFX = CDbl(tmpAccountCode)
        End If
    End Function

    Public Function GetPortfolioFeeAccountCodeFXCCYCode(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As Double
        GetPortfolioFeeAccountCodeFXCCYCode = 0
        Dim tmpCCYCode As String = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeAccountCodeFXCCYCode(" & varPortfolioCode & "," & varCCYCode & ")")
        If IsNumeric(tmpCCYCode) Then
            GetPortfolioFeeAccountCodeFXCCYCode = CDbl(tmpCCYCode)
        End If
    End Function

    Public Function GetPortfolioFeeAccountCodeFXCCY(ByVal varPortfolioCode As Double, ByVal varCCYCode As Double) As String
        GetPortfolioFeeAccountCodeFXCCY = ""
        GetPortfolioFeeAccountCodeFXCCY = GetSQLFunction("Select dbo.DolfinPaymentGetPortfolioFeeAccountCodeFXCCY(" & varPortfolioCode & "," & varCCYCode & ")")
    End Function

    Public Sub GetTransferFeeAccount(ByRef varFoundOrderAccountCCY As String, ByRef varFoundOrderAccount As String, ByRef varFoundOrderAccountBalance As Double, ByRef varFoundOrderAccountFee As Double, ByRef varFoundOrderAccountConvertedFee As Double, ByRef varFoundExchangeRate As Double)
        Dim Reader As SqlDataReader = GetSQLData("exec DolfinPaymentGetTransferFeeAccount " & ClsPay.PortfolioCode & "," & ClsPay.CCYCode & "," & varFoundOrderAccount & "," & ClsPay.Amount)

        If Reader.HasRows Then
            Reader.Read()
            varFoundOrderAccountCCY = IIf(IsDBNull(Reader("CR_Name1").ToString()), 0, Reader("CR_Name1").ToString())
            varFoundOrderAccount = IIf(IsDBNull(Reader("T_Name1").ToString()), 0, Reader("T_Name1").ToString())
            If IsNumeric(varFoundOrderAccountBalance) Then
                varFoundOrderAccountBalance = CDbl(IIf(IsDBNull(Reader("Balance")), 0, Reader("Balance")))
            Else
                varFoundOrderAccountBalance = 0
            End If
            If IsNumeric(varFoundOrderAccountFee) Then
                varFoundOrderAccountFee = CDbl(IIf(IsDBNull(Reader("Fee")), 0, Reader("Fee")))
            Else
                varFoundOrderAccountFee = 0
            End If
            If IsNumeric(varFoundOrderAccountConvertedFee) Then
                varFoundOrderAccountConvertedFee = CDbl(IIf(IsDBNull(Reader("ConvertedFee")), 0, Reader("ConvertedFee")))
            Else
                varFoundOrderAccountConvertedFee = 0
            End If
            If IsNumeric(varFoundExchangeRate) Then
                varFoundExchangeRate = CDbl(IIf(IsDBNull(Reader("ExchangeRate")), 0, Reader("ExchangeRate")))
            Else
                varFoundExchangeRate = 0
            End If
        End If
    End Sub

    Public Sub GetRMSTransferFeeAccount(ByVal varRMSAccountCode As Integer, ByRef varFoundBenAccountCCY As String, ByRef varFoundBenAccount As String, ByRef varFoundBenAccountBalance As Double)
        Dim Reader As SqlDataReader = GetSQLData("select CR_Name1,T_Name1,Balance from vwDolfinPaymentSelectAccount where pf_Code in (" & varRMSAccountCode & ") and cr_Code = " & ClsPay.CCYCode & " and t_code = " & ClsPay.OrderAccountCode)

        If Reader.HasRows Then
            Reader.Read()
            varFoundBenAccountCCY = Reader("CR_Name1").ToString()
            varFoundBenAccount = Reader("T_Name1").ToString()
            If IsNumeric(Reader("Balance")) Then
                varFoundBenAccountBalance = CDbl(Reader("Balance"))
            Else
                varFoundBenAccountBalance = 0
            End If
        End If
    End Sub

    Private Sub GetUserOptions()
        Dim Reader As SqlDataReader = GetSQLData("select BranchCode, branchLocation, U_OptAutoRefresh,U_OptInactivity,U_OptGridSort from vwDolfinPaymentUsers left join vwDolfinPaymentBranches on U_Location = branchlocation where u_username = '" & varUserName & "' group by BranchCode, branchLocation, U_OptAutoRefresh,U_OptInactivity,U_OptGridSort")
        Dim cmd As New SqlCommand

        If Reader.HasRows Then
            Reader.Read()
            varUserLocationCode = IIf(Not IsNumeric(Reader("BranchCode")), 42, Reader("BranchCode"))
            varUserLocation = IIf(Reader("branchLocation") = "", "London", Reader("branchLocation"))
            varUserOptRefresh = IIf(Not IsDBNull(Reader("U_OptAutoRefresh")), Reader("U_OptAutoRefresh"), True)
            varUserOptInActivity = IIf(IsNumeric(Reader("U_OptInactivity")), Reader("U_OptInactivity"), 60)
            varUserOptGridSort = IIf(IsNumeric(Reader("U_OptGridSort")), Reader("U_OptGridSort"), 0)
        End If
        Reader.Close()
    End Sub

    Private Sub PopulateFilePaths()
        Dim varFilePathName As String = ""
        Dim varFilePath As String = ""
        Dim ReaderFP As SqlDataReader = GetSQLData("select Path_Name, Path_FilePath from vwDolfinPaymentFilePath")

        If ReaderFP.HasRows Then
            While ReaderFP.Read()
                varFilePathName = IIf(IsDBNull(ReaderFP("Path_Name")), "", ReaderFP("Path_Name"))
                varFilePath = IIf(IsDBNull(ReaderFP("Path_FilePath")), "", ReaderFP("Path_FilePath"))
                varFilePaths.Add(varFilePathName, varFilePath)
            End While
        End If
        ReaderFP.Close()
    End Sub

    Private Sub PopulateFileExportNames()
        Dim varFileExportKey As String = ""
        Dim varFileExportName As String = ""
        Dim ReaderFE As SqlDataReader = GetSQLData("select FE_Key, FE_Name from vwDolfinPaymentFileExport")

        If ReaderFE.HasRows Then
            While ReaderFE.Read()
                varFileExportKey = IIf(IsDBNull(ReaderFE("FE_Key")), "", ReaderFE("FE_Key"))
                varFileExportName = IIf(IsDBNull(ReaderFE("FE_Name")), "", ReaderFE("FE_Name"))
                varFileExportNames.Add(varFileExportKey, varFileExportName)
            End While
        End If
        ReaderFE.Close()
    End Sub

    Private Sub PopulateDolfinAddress()
        Dim ReaderDA As SqlDataReader = GetSQLData("select SwiftCode,Name,Address,City,PostCode from vwDolfinPaymentDolfinAddress")

        If ReaderDA.HasRows Then
            ReaderDA.Read()
            varDolfinAddress.Add("SwiftCode", IIf(IsDBNull(ReaderDA("SwiftCode")), "", ReaderDA("SwiftCode")))
            varDolfinAddress.Add("Name", IIf(IsDBNull(ReaderDA("Name")), "", ReaderDA("Name")))
            varDolfinAddress.Add("Address", IIf(IsDBNull(ReaderDA("Address")), "", ReaderDA("Address")))
            varDolfinAddress.Add("City", IIf(IsDBNull(ReaderDA("City")), "", ReaderDA("City")))
            varDolfinAddress.Add("PostCode", IIf(IsDBNull(ReaderDA("PostCode")), "", ReaderDA("PostCode")))
        End If
        ReaderDA.Close()
    End Sub

    Private Sub PopulateLocation()
        Dim varBranchID As Integer
        Dim varBranch As String
        Dim ReaderLC As SqlDataReader = GetSQLData("SELECT BranchCode,BranchLocation from vwDolfinPaymentBranches group by BranchCode,BranchLocation union SELECT 1,'All'")

        If ReaderLC.HasRows Then
            While ReaderLC.Read()
                varBranchID = IIf(IsNumeric(ReaderLC("BranchCode")), ReaderLC("BranchCode"), 0)
                varBranch = IIf(IsDBNull(ReaderLC("BranchLocation")), "", ReaderLC("BranchLocation"))
                varLocationTypes.Add(varBranchID, varBranch)
            End While
        End If
        ReaderLC.Close()
    End Sub

    Public Sub PopulatecboPaymentTypes(ByVal cbo As ComboBox)
        PopulateComboboxWithDictionary(cbo, varPaymentTypes)
    End Sub

    Public Sub PopulatecbostatusTypes(ByVal cbo As ComboBox)
        PopulateComboboxWithDictionary(cbo, varStatusTypes)
    End Sub

    Public Sub PopulatecboLocationTypes(ByVal cbo As ComboBox)
        PopulateComboboxWithDictionary(cbo, varLocationTypes)
    End Sub

    Public Sub PopulatecboPaymentAllTypes(ByVal cbo As ComboBox)
        PopulateComboboxWithDictionary(cbo, varPaymentAllTypes)
    End Sub

    Public Sub PopulatecboPayCheck(ByVal cbo As ComboBox)
        PopulateComboboxWithDictionary(cbo, varPayCheckTypes)
    End Sub

    Public Function PaymentSendsSwift(ByVal PT As String) As Boolean
        PaymentSendsSwift = False
        If PT = GetPaymentType(cPayDescIn) Or PT = GetPaymentType(cPayDescExClientIn) Or
            PT = GetPaymentType(cPayDescExOut) Or PT = GetPaymentType(cPayDescExClientOut) Or PT = GetPaymentType(cPayDescExVolopaOut) Or
            PT = GetPaymentType(cPayDescInMaltaLondon) Or PT = GetPaymentType(cPayDescInLondonMalta) Or
            PT = GetPaymentType(cPayDescInOther) Then
            PaymentSendsSwift = True
        End If
    End Function

    Private Sub PopulatePaymentTypes()
        Dim varPaymentTypeID As Integer = 0
        Dim varPaymentType As String = ""
        Dim ReaderPT As SqlDataReader = GetSQLData("Select PT_ID, PT_PaymentType from vwDolfinPaymentType where PT_Movement = 1 Order by PT_PaymentType")

        If ReaderPT.HasRows Then
            While ReaderPT.Read()
                varPaymentTypeID = IIf(IsDBNull(ReaderPT("PT_ID")), 0, ReaderPT("PT_ID"))
                varPaymentType = IIf(IsDBNull(ReaderPT("PT_PaymentType")), "", ReaderPT("PT_PaymentType"))
                varPaymentTypes.Add(varPaymentTypeID, varPaymentType)
            End While
        End If
        ReaderPT.Close()
    End Sub

    Private Sub PopulatePaymentAllTypes()
        Dim varPaymentTypeID As Integer = 0
        Dim varPaymentType As String = ""
        Dim ReaderPAT As SqlDataReader = GetSQLData("Select PT_ID, PT_PaymentType from vwDolfinPaymentType where PT_Movement = 1 union Select OTH_ID+100 as PT_ID, OTH_Type as PT_PaymentType from vwDolfinPaymentOther Order by PT_PaymentType")

        If ReaderPAT.HasRows Then
            While ReaderPAT.Read()
                varPaymentTypeID = IIf(IsDBNull(ReaderPAT("PT_ID")), 0, ReaderPAT("PT_ID"))
                varPaymentType = IIf(IsDBNull(ReaderPAT("PT_PaymentType")), "", ReaderPAT("PT_PaymentType"))
                varPaymentAllTypes.Add(varPaymentTypeID, varPaymentType)
            End While
        End If
        ReaderPAT.Close()
    End Sub

    Private Sub PopulatePaymentTypesShortCode()
        Dim varPaymentTypeID As Integer = 0
        Dim varPaymentTypeShortCode As String = ""
        Dim ReaderPT As SqlDataReader = GetSQLData("Select PT_ID, PT_ShortCode from vwDolfinPaymentType")

        If ReaderPT.HasRows Then
            While ReaderPT.Read()
                varPaymentTypeID = IIf(IsDBNull(ReaderPT("PT_ID")), 0, ReaderPT("PT_ID"))
                varPaymentTypeShortCode = IIf(IsDBNull(ReaderPT("PT_ShortCode")), "", ReaderPT("PT_ShortCode"))
                varPaymentTypesShortCodes.Add(varPaymentTypeID, varPaymentTypeShortCode)
            End While
        End If
        ReaderPT.Close()
    End Sub

    Private Sub PopulatePaymentOtherTypes()
        Dim varPaymentOtherTypeID As Integer = 0
        Dim varPaymentOtherType As String = ""
        Dim ReaderOT As SqlDataReader = GetSQLData("Select OTH_ID, OTH_Type from DolfinPaymentOther")

        If ReaderOT.HasRows Then
            While ReaderOT.Read()
                varPaymentOtherTypeID = IIf(IsDBNull(ReaderOT("OTH_ID")), 0, ReaderOT("OTH_ID"))
                varPaymentOtherType = IIf(IsDBNull(ReaderOT("OTH_Type")), "", ReaderOT("OTH_Type"))
                varPaymentOtherTypes.Add(varPaymentOtherTypeID, varPaymentOtherType)
            End While
        End If
        ReaderOT.Close()
    End Sub

    Private Sub PopulateStatusTypes()
        Dim varStatusTypeID As Integer = 0
        Dim varStatusType As String = ""
        Dim ReaderST As SqlDataReader = GetSQLData("Select S_ID, S_Status from vwDolfinPaymentStatus")

        If ReaderST.HasRows Then
            While ReaderST.Read()
                varStatusTypeID = IIf(IsDBNull(ReaderST("S_ID")), 0, ReaderST("S_ID"))
                varStatusType = IIf(IsDBNull(ReaderST("S_Status")), "", ReaderST("S_Status"))
                varStatusTypes.Add(varStatusTypeID, varStatusType)
            End While
        End If
        ReaderST.Close()
    End Sub

    Private Sub PopulatePayCheckTypes()
        varPayCheckTypes.Add(0, "Today")
        varPayCheckTypes.Add(1, "Last Business Day")
        varPayCheckTypes.Add(2, "2 Business Days Ago")
        varPayCheckTypes.Add(3, "3 Business Days Ago")
        varPayCheckTypes.Add(4, "4 Business Days Ago")
        varPayCheckTypes.Add(5, "5 Business Days Ago")
    End Sub

    Private Sub PopulateMessageTypes()
        Dim varMessageID As Integer = 0
        Dim varMessageType As String = ""
        Dim ReaderMT As SqlDataReader = GetSQLData("Select M_ID, M_MessageType, M_RD, M_Auto from vwDolfinPaymentMessageType")

        If ReaderMT.HasRows Then
            While ReaderMT.Read()
                varMessageID = IIf(IsDBNull(ReaderMT("M_ID")), 0, ReaderMT("M_ID"))
                varMessageType = IIf(IsDBNull(ReaderMT("M_MessageType")), "", ReaderMT("M_MessageType"))
                varMessageTypes.Add(varMessageID, varMessageType)
                If IIf(IsDBNull(ReaderMT("M_RD")), 0, ReaderMT("M_RD")) Then
                    varMessageTypesRD.Add(varMessageID, varMessageType)
                End If
                If IIf(IsDBNull(ReaderMT("M_Auto")), 0, ReaderMT("M_Auto")) Then
                    varMessageTypesAuto.Add(varMessageID, varMessageType)
                End If
            End While
        End If
        ReaderMT.Close()
    End Sub

    Private Sub PopulateAuditLogStatusTypes()
        Dim varStatusTypeID As Integer = 0
        Dim varStatusType As String = ""
        Dim ReaderALS As SqlDataReader = GetSQLData("Select ALS_ID, ALS_Status from vwDolfinPaymentAuditLogStatus")

        If ReaderALS.HasRows Then
            While ReaderALS.Read()
                varStatusTypeID = IIf(IsDBNull(ReaderALS("ALS_ID")), 0, ReaderALS("ALS_ID"))
                varStatusType = IIf(IsDBNull(ReaderALS("ALS_Status")), "", ReaderALS("ALS_Status"))
                varAuditLogStatusTypes.Add(varStatusTypeID, varStatusType)
            End While
        End If
        ReaderALS.Close()
    End Sub

    Private Sub PopulateAuditLogGroupNames()
        Dim varGroupNameID As Integer = 0
        Dim varGroupName As String = ""
        Dim ReaderALG As SqlDataReader = GetSQLData("Select ALG_ID, ALG_GroupName from vwDolfinPaymentAuditLogGroupName")

        If ReaderALG.HasRows Then
            While ReaderALG.Read()
                varGroupNameID = IIf(IsDBNull(ReaderALG("ALG_ID")), 0, ReaderALG("ALG_ID"))
                varGroupName = IIf(IsDBNull(ReaderALG("ALG_GroupName")), "", ReaderALG("ALG_GroupName"))
                varAuditLogGroupNames.Add(varGroupNameID, varGroupName)
            End While
        End If
        ReaderALG.Close()
    End Sub

    Public Function PopulateReaderToList(ByVal Reader As SqlDataReader) As List(Of String)
        Dim Lst As New List(Of String)

        If Reader.HasRows Then
            While (Reader.Read())
                If Not Lst.Contains(Reader(0).ToString) Then
                    Lst.Add(Reader(0).ToString)
                End If
            End While
        End If
        PopulateReaderToList = Lst
    End Function

    Public Function PopulateAllFieldsReaderToList(ByVal Reader As SqlDataReader) As Dictionary(Of String, String)
        Dim Lst As New Dictionary(Of String, String)
        Dim vField As Integer

        If Reader.HasRows Then
            While (Reader.Read())
                For vField = 0 To Reader.FieldCount - 1
                    If Not Lst.ContainsKey(Reader.GetName(vField).ToString) Then
                        Lst.Add(Reader.GetName(vField).ToString, Reader(vField).ToString)
                    End If
                Next
            End While
        End If
        PopulateAllFieldsReaderToList = Lst
    End Function

    Public Function PopulateMultipleReaderToList(ByVal Reader As SqlDataReader) As List(Of String)
        Dim Lst As New List(Of String)
        Dim vField As Integer

        If Reader.HasRows Then
            While (Reader.Read())
                For vField = 0 To Reader.FieldCount - 1
                    Lst.Add(Reader(vField).ToString)
                Next
            End While
        End If
        PopulateMultipleReaderToList = Lst
    End Function

    Public Function PopulateAllIDReaderToList(ByVal Reader As SqlDataReader) As Dictionary(Of String, String)
        Dim Lst As New Dictionary(Of String, String)

        If Reader.HasRows Then
            While (Reader.Read())
                If Not Lst.ContainsKey(Reader(0).ToString) Then
                    Lst.Add(Reader(0).ToString, Reader(1).ToString)
                End If
            End While
        End If
        PopulateAllIDReaderToList = Lst
    End Function

    Public Sub PopulateDataTreeView(ByVal tv As TreeView, tvData As SqlClient.SqlDataReader, colOptions As List(Of String))
        Dim IntCNode As Integer, colIndex As Integer

        tv.Nodes.Clear()
        IntCNode = 0
        If tvData.HasRows Then
            While tvData.Read
                tv.Nodes.Add(tvData.Item("Name"))
                tv.Nodes(IntCNode).Nodes.Add(tvData.Item("Portfolio"))
                For colIndex = 0 To colOptions.Count - 1
                    tv.Nodes(IntCNode).Nodes(0).Nodes.Add(colOptions(colIndex))
                Next
                IntCNode += 1
            End While
        End If
    End Sub

    Public Function GetExportedFileNames() As List(Of String)
        Dim ReaderSF As SqlDataReader = GetSQLData("Select p_swiftfile from vwdolfinPayment where P_StatusID = 5 and datediff(minute,P_SwiftSenttime,getdate()) < 2")
        GetExportedFileNames = PopulateReaderToList(ReaderSF)
    End Function

    Public Sub PopulateComboboxWithList(ByVal cbo As System.Windows.Forms.ComboBox, Lst As List(Of String))
        cbo.Items.Clear()
        For Each L In Lst
            cbo.Items.Add(L.ToString)
        Next
    End Sub

    Public Sub PopulateComboboxWithDictionaryold(ByVal cbo As System.Windows.Forms.ComboBox, Lst As Dictionary(Of Integer, String))
        cbo.Items.Clear()

        cbo.DisplayMember = "Value"
        cbo.ValueMember = "Key"
        cbo.DataSource = New BindingSource(Lst, Nothing)
    End Sub

    Public Sub PopulateComboboxWithDictionary(ByVal cbo As System.Windows.Forms.ComboBox, Lst As Dictionary(Of Integer, String))
        cbo.Items.Clear()

        cbo.DisplayMember = "Text"
        cbo.ValueMember = "Value"

        Dim tb As New DataTable
        tb.Columns.Add("Text", GetType(String))
        tb.Columns.Add("Value", GetType(Integer))
        tb.Rows.Add("", -1)
        For Each L In Lst
            tb.Rows.Add(L.Value.ToString, L.Key)
        Next
        cbo.DataSource = tb
    End Sub

    Public Function GetBalance(ByVal PortfolioCode As Integer, ByVal AccountCode As Integer) As Double
        Dim GetBalanceStr As String

        GetBalance = 0
        GetBalanceStr = GetSQLFunction("Select dbo.DolfinPaymentGetBalances(1, " & AccountCode & ", " & PortfolioCode & ", getdate())")

        If IsNumeric(GetBalanceStr) Then
            GetBalance = CDbl(GetBalanceStr)
        End If
    End Function

    Public Function GetPendingAmount(CurrentID As Integer, PortfolioCode As Integer, CCYCode As Integer, AccountCode As Integer) As Double
        Dim GetPendingStr As String

        GetPendingAmount = 0
        GetPendingStr = GetSQLFunction("Select dbo.DolfinPaymentGetPendingAmount(" & CurrentID & "," & PortfolioCode & "," & CCYCode & "," & AccountCode & ")")
        If IsNumeric(GetPendingStr) Then
            GetPendingAmount = CDbl(GetPendingStr)
        End If
    End Function

    Public Function GetFormData(ByVal varportfolioCode As Integer, ByVal varCCYCode As Integer, ByVal varAccountCode As Integer, Optional ByVal varNotAccountCode As Integer = -1) As Dictionary(Of String, String)
        GetFormData = Nothing
        If ClsPay.SelectedOrder = 1 Or ClsPay.SelectedOrder = 2 Then
            If varNotAccountCode <> -1 Then
                GetFormData = GetDataToList("Select B_Name1, B_Code, B_Address, B_Area, B_Zip, B_SwiftAddress, t_Code, T_IBAN, T_Account,PF_Shortcut1,T_Shortcut1,B_Shortcut1,IBankSwiftAddress,isnull(SC_SwiftTransferMessTypeID,0) as SC_SwiftTransferMessTypeID,isnull(SC_Swift3rdPartyMessTypeID,0) as SC_Swift3rdPartyMessTypeID,isnull(SC_SwiftOtherMessTypeID,0) as SC_SwiftOtherMessTypeID,SC_CountryID,SC_Country from vwDolfinPaymentSelectAccount where pf_code = " & varportfolioCode & " And cr_Code = " & varCCYCode & " And t_Code = " & varAccountCode & " And t_Code <> " & varNotAccountCode & " order by B_Name1")
            Else
                GetFormData = GetDataToList("Select B_Name1,B_Code,B_Address,B_Area,B_Zip,B_SwiftAddress,t_Code, T_IBAN, T_Account,PF_Shortcut1,T_Shortcut1,B_Shortcut1,IBankSwiftAddress,isnull(SC_SwiftTransferMessTypeID,0) as SC_SwiftTransferMessTypeID,isnull(SC_Swift3rdPartyMessTypeID,0) as SC_Swift3rdPartyMessTypeID,isnull(SC_SwiftOtherMessTypeID,0) as SC_SwiftOtherMessTypeID,SC_CountryID,SC_Country from vwDolfinPaymentSelectAccount where pf_code = " & varportfolioCode & " And cr_code = " & varCCYCode & " And t_Code = " & varAccountCode & " order by B_Name1")
            End If
        End If
    End Function

    Public Function GetBankFormData(ByVal varSwiftCode As String) As Dictionary(Of String, String)
        GetBankFormData = Nothing
        If ClsPay.SelectedOrder = 3 Or ClsPay.SelectedOrder = 4 Then
            GetBankFormData = GetDataToList("Select SC_BankName As B_Name1, 0 As B_Code, SC_Address1 As B_Address, SC_City As B_Area, SC_PostCode As B_Zip, SC_SwiftCode As B_SwiftAddress, SC_BIK, SC_INN, '' as B_Shortcut1,SC_CountryID,SC_Country from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varSwiftCode & "' order by SC_BankName")
        End If
    End Function

    Public Function GetFormDataFromSwift(ByVal varSwiftcode As String, Optional ByVal varBankSwiftCode As String = "", Optional ByVal varCCYCode As Integer = 0) As Dictionary(Of String, String)
        If (ClsPay.PaymentType = GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = GetPaymentType(cPayDescInOther)) And ClsPay.SelectedOrder = 3 And varBankSwiftCode <> "" Then
            GetFormDataFromSwift = GetDataToList("Select IBankSwiftCode as SC_SwiftCode, IBankName as SC_BankName, IBankCity as SC_City, IBankAddress1 as SC_Address1, IBankCountryID as SC_CountryID, IBankCountry as SC_Country, BankPostCode as SC_PostCode, IBankIBAN, IBankBIK, IBankINN from vwDolfinPaymentSwiftCodesIntermediaryBankLinks where IBankSwiftCode = '" & varSwiftcode & "' and BankSwiftCode = '" & varBankSwiftCode & "' and BankCCYCode = " & varCCYCode)
        Else
            GetFormDataFromSwift = GetDataToList("Select SC_SwiftCode, SC_BankName, SC_City, SC_Address1,SC_CountryID,SC_Country, SC_PostCode, SC_BIK, SC_INN, SC_FailMessage from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & varSwiftcode & "'")
        End If
    End Function

    Public Function GetFormDataTransfer(ByVal varCCYCode As Integer, ByVal varAccountCode As Integer) As Dictionary(Of String, String)
        GetFormDataTransfer = GetDataToList("Select top 1 B_SwiftAddress, T_IBAN, T_Account, SC_SwiftTransferMessTypeID,isnull(SC_Swift3rdPartyMessTypeID,0) as SC_Swift3rdPartyMessTypeID,isnull(SC_SwiftOtherMessTypeID,0) as SC_SwiftOtherMessTypeID,SC_CountryID,SC_Country from vwDolfinPaymentSelectAccount where cr_code = " & varCCYCode & " And t_Code = " & varAccountCode)
    End Function

    Public Sub PopulateFormCombo(ByRef cbo As ComboBox, Optional ByVal Arg1 As String = "", Optional ByVal Arg2 As String = "", Optional ByVal Arg3 As String = "", Optional ByVal ForceUserLocationCode As Integer = 0)
        Dim varUserLocationCode As Integer = IIf(ForceUserLocationCode <> 0, ForceUserLocationCode, UserLocationCode)

        If LCase(cbo.Name) = "cboportfolio" Or LCase(cbo.Name) = "cbotf" Or LCase(cbo.Name) = "cbotmportfolio" Then
            If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                PopulateComboboxWithData(cbo, "select PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) group by PF_Code,Portfolio order by Portfolio")
            ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescIn) Then
                PopulateComboboxWithData(cbo, "Select PF_Code,Portfolio From vwDolfinPaymentSelectAccount sa1 Where branchCode = " & varUserLocationCode & " and b_class In (1,25,37,39) And exists(select pf_code from vwDolfinPaymentSelectAccount sa2 where b_class in (1,25,37,39) And sa1.pf_code = sa2.pf_code group by pf_code having count(pf_code) > 1) group by PF_Code,Portfolio order by Portfolio")
            ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                PopulateComboboxWithData(cbo, "select cust_id, name from vwDolfinPaymentSelectClientAccount ca where branchCode = " & varUserLocationCode & " and cust_id in (select cust_id from vwDolfinPaymentSelectClientAccount ca2 where ca.cust_id = ca2.cust_id group by cust_id, name, cr_name1 having count(pf_code)>1) group by cust_id, name order by name")
            ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                PopulateComboboxWithData(cbo, "Select Oth_ID, OTH_Type from vwDolfinPaymentOther order by OTH_Type")
            Else
                PopulateComboboxWithData(cbo, "select PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) group by PF_Code,Portfolio order by Portfolio")
            End If
        ElseIf LCase(cbo.Name) = "cboorderaccount" Or LCase(cbo.Name) = "cbobenaccount" Then
            If Arg1 <> "" And Arg2 <> "" And Arg3 <> "" Then
                If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Then
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) and PF_Code = " & Arg1 & " and cr_code = " & Arg2 & " and t_code <> " & Arg3 & " order by t_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = GetPaymentType(cPayDescInPortLoan) Then
                    PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and t_code = " & Arg2 & " and pf_code <> " & Arg3 & " order by Portfolio")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                    If IsFirmPortfolio(Arg3) Then
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " order by Portfolio")
                    ElseIf ClsPay.OtherID = cPayDescInOtherManagementFee Or ClsPay.OtherID = cPayDescInOtherAccountClosingManagementFee Then
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and pf_code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(varUserLocationCode = 47, "All", "") & ") and cr_code = " & Arg1 & " order by Portfolio")
                    Else
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and pf_code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(varUserLocationCode = 47, "Malta", "") & ") and cr_code = " & Arg1 & " order by Portfolio")
                    End If
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                    PopulateComboboxWithData(cbo, "Select pf_code, portfolio from vwDolfinPaymentSelectClientAccount ca where branchCode = " & varUserLocationCode & " and cr_code = '" & Arg1 & "' and cust_id = " & Arg2 & " and pf_code <> " & Arg3 & " group by pf_code, portfolio")
                Else
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and PF_Code = " & Arg1 & " and cr_code = " & Arg2 & " and t_code <> " & Arg3 & " order by t_Name1")
                End If
            ElseIf Arg1 <> "" And Arg2 <> "" Then
                If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                    If LCase(cbo.Name) = "cbobenaccount" Then
                        PopulateComboboxWithData(cbo, "select 0,'' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & Arg1 & " and Pa_CCYCode = " & Arg2 & " and isnull(Pa_TemplateName,'')<>'' group by Pa_TemplateName order by Pa_TemplateName")
                    Else
                        PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) and PF_Code = " & Arg1 & " and cr_code = " & Arg2 & " order by t_Name1")
                    End If
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = GetPaymentType(cPayDescInPortLoan) Then
                    PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and t_code = " & Arg2 & " order by Portfolio")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                    If IsFirmPortfolio(Arg1) Or IsFirmPortfolio(Arg2) Or ClsPay.OtherID = cPayDescInOtherNoticetoReceive Then
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and (b_class in (1,25,37,39) and pf_code not in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(varUserLocationCode = 47, "Malta", "") & ")) and cr_code = " & Arg1 & " order by Portfolio")
                    ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And (ClsPay.OtherID = cPayDescInOtherIntTra Or ClsPay.OtherID = cPayDescInOtherFXBuy Or ClsPay.OtherID = cPayDescInOtherFXSell) Then
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and PF_Code = " & Arg1 & " and cr_code = " & Arg2)
                    ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.OtherID = cPayDescInOtherStatementRequest Then
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and PF_Code = 365 and cr_code = " & Arg1)
                    Else
                        PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentFirmPortfolios" & IIf(varUserLocationCode = 47, "Malta", "") & " order by Portfolio")
                    End If
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                    PopulateComboboxWithData(cbo, "Select pf_code, portfolio from vwDolfinPaymentSelectClientAccount ca where branchCode = " & varUserLocationCode & " and cust_id = " & Arg1 & " and cr_code = " & Arg2 & " group by pf_code, portfolio")
                Else
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and PF_Code = " & Arg1 & " and cr_code = " & Arg2 & " order by t_Name1")
                End If
            Else
                If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                    PopulateComboboxWithData(cbo, "select B_Code,B_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) group by B_Code,B_Name1 order by B_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = GetPaymentType(cPayDescInPortLoan) Or ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                    PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and (b_class in (1,25,37,39) or pf_code in (select distinct PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(varUserLocationCode = 47, "Malta", "") & ")) and cr_code = " & Arg1 & " order by Portfolio")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInMaltaLondon) Then
                    PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & IIf(LCase(cbo.Name) = "cboorderaccount", 47, 42) & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " order by Portfolio")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInLondonMalta) Then
                    PopulateComboboxWithData(cbo, "select distinct PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & IIf(LCase(cbo.Name) = "cboorderaccount", 42, 47) & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " order by Portfolio")
                Else
                    PopulateComboboxWithData(cbo, "select B_Code,B_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) group by B_Code,B_Name1 order by B_Name1")
                End If
            End If
        ElseIf LCase(cbo.Name) = "cboordername" Or LCase(cbo.Name) = "cbobenname" Then
            If Arg1 <> "" And Arg2 <> "" And Arg3 <> "" Then
                If ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                    If IsFirmPortfolio(Arg2) Then
                        PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and PF_Code = " & Arg2 & " and t_Code <> " & Arg3 & " order by t_Name1")
                    Else
                        PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and PF_Code = " & Arg2 & " order by t_Name1")
                    End If
                Else
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and PF_Code = " & Arg2 & " and t_Code = " & Arg3 & " order by t_Name1")
                End If
            ElseIf Arg1 <> "" And Arg2 <> "" Then
                If ClsPay.PaymentType = GetPaymentType(cPayDescInOther) Then
                    If ClsPay.OtherID = cPayDescInOtherManagementFee Or ClsPay.OtherID = cPayDescInOtherAccountClosingManagementFee Then
                        PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and cr_code = " & Arg1 & " and pf_code = " & Arg2 & " order by t_Name1")
                    Else
                        PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and pf_code = " & Arg2 & " order by t_Name1")
                    End If
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = GetPaymentType(cPayDescInLondonMalta) Then
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and cr_code = " & Arg1 & " and PF_Code = " & Arg2 & " order by t_Name1")
                Else
                    PopulateComboboxWithData(cbo, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) and cr_code = " & Arg1 & " and PF_Code = " & Arg2 & " order by t_Name1")
                End If
            ElseIf Arg1 <> "" Then
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.OtherID = cPayDescInOtherIntTra Then
                    PopulateComboboxWithData(cbo, "select t_Code,T_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and cr_code = " & Arg1 & " group by t_Code, T_Name1 order by T_Name1")
                Else
                    PopulateComboboxWithData(cbo, "select t_Code,T_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and cr_code = " & Arg1 & " group by t_Code, T_Name1 order by T_Name1")
                End If

            End If
        ElseIf LCase(cbo.Name) = "cboccy" Or LCase(cbo.Name) = "cbobenccy" Or LCase(cbo.Name) = "cbotmccy" Or LCase(cbo.Name) = "cbotaccy" Or LCase(cbo.Name) = "cbotatdccy" Then
            If Arg1 <> "" Then
                If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                    PopulateComboboxWithData(cbo, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) and PF_Code = " & Arg1 & " group by CR_Code,CR_Name1 order by CR_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescIn) Then
                    PopulateComboboxWithData(cbo, "Select CR_Code,CR_Name1 From vwDolfinPaymentSelectAccount sa1 Where branchCode = " & varUserLocationCode & " and b_class In (1,25,37,39) and PF_Code = " & Arg1 & " And exists(select pf_code from vwDolfinPaymentSelectAccount sa2 where b_class in (1,25,37,39) And sa1.pf_code = sa2.pf_code group by pf_code having count(pf_code) > 1) group by CR_Code,CR_Name1 order by CR_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                    PopulateComboboxWithData(cbo, "Select cr_code, cr_name1 from vwDolfinPaymentSelectClientAccount ca where branchCode = " & varUserLocationCode & " and cr_code In (Select cr_code from vwDolfinPaymentSelectClientAccount ca2 where ca2.cust_id = " & Arg1 & " group by cr_code, name having count(pf_code)>1) group by cr_code, cr_name1 order by CR_Name1")
                Else
                    PopulateComboboxWithData(cbo, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and PF_Code = " & Arg1 & " group by CR_Code,CR_Name1 order by CR_Name1")
                End If
            Else
                If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                    PopulateComboboxWithData(cbo, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,37,39) group by CR_Code,CR_Name1 order by CR_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                    PopulateComboboxWithData(cbo, "Select CR_Code,CR_Name1 From vwDolfinPaymentSelectAccount sa1 Where branchCode = " & varUserLocationCode & " and b_class In (1,25,37,39) And exists(select pf_code from vwDolfinPaymentSelectAccount sa2 where b_class in (1,25,37,39) And sa1.pf_code = sa2.pf_code group by pf_code having count(pf_code) > 1) group by CR_Code,CR_Name1 order by CR_Name1")
                ElseIf ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
                    PopulateComboboxWithData(cbo, "Select cr_code, cr_name1 from vwDolfinPaymentSelectClientAccount ca where branchCode = " & varUserLocationCode & " and cr_code In (Select cr_code from vwDolfinPaymentSelectClientAccount ca2 where ca.cust_id = ca2.cust_id group by cr_code, name having count(pf_code)>1) group by cr_code, cr_name1 order by CR_Name1")
                Else
                    PopulateComboboxWithData(cbo, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " group by CR_Code,CR_Name1 order by CR_Name1")
                End If
            End If
        ElseIf LCase(cbo.Name) = "cbocutoffbroker" Or LCase(cbo.Name) = "cbotainsta" Or LCase(cbo.Name) = "cbotainstb" Then
            PopulateComboboxWithData(cbo, "select B_Code,B_Name1 from vwDolfinPaymentSelectAccount where branchCode = " & varUserLocationCode & " and b_class in (1,25,37,39) group by B_Code,B_Name1 order by B_Name1")
        ElseIf LCase(cbo.Name) = "cbotatdinsta" Then
            'PopulateComboboxWithData(cbo, "select BrokerCodeA,BrokerA from vwDolfinPaymentSelectAccountTreasuryTermDeposits where branchCodeA = " & varUserLocationCode & " group by BrokerCodeA,BrokerA order by BrokerA")
            PopulateComboboxWithData(cbo, "select BrokerCodeA,BrokerA from dbo.vwTermDepositBrokers group by BrokerCodeA,BrokerA order by BrokerA")
        ElseIf LCase(cbo.Name) = "cbobensubbankname" Or LCase(cbo.Name) = "cbobenbankname" Then
            PopulateComboboxWithData(cbo, "select '' as SC_SwiftCode,'' as SC_BankName union select SC_SwiftCode,SC_BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName order by SC_BankName")
        ElseIf LCase(cbo.Name) = "cbouser" Then
            PopulateComboboxWithData(cbo, "select 1,U_Username from vwDolfinPaymentUsers order by U_Username")
        ElseIf LCase(cbo.Name) = "cboordervocode" Then
            PopulateComboboxWithData(cbo, "select VO_ID,VO_Code from vwDolfinPaymentVOCodes order by VO_Code")
        ElseIf LCase(cbo.Name) = "cboordercountry" Or LCase(cbo.Name) = "cbobencountry" Or LCase(cbo.Name) = "cbobensubbankcountry" Or LCase(cbo.Name) = "cbobenbankcountry" Then
            PopulateComboboxWithData(cbo, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ElseIf LCase(cbo.Name) = "cbobentype" Then
            PopulateComboboxWithData(cbo, "Select BeneficiaryTypeID, BeneficiaryType from vwDolfinPaymentBeneficiaryType order by BeneficiaryType")
        ElseIf LCase(cbo.Name) = "cbobenrelationship" Then
            PopulateComboboxWithData(cbo, "SELECT pty_id, pty_description from DolfinPaymentRARangePayType group by pty_id, pty_description")
        End If
    End Sub

    Public Function IsFirmPortfolio(ByVal varPortfolioCode As Integer) As Boolean
        Dim varFirmPortfolio As String = ""
        IsFirmPortfolio = False

        varFirmPortfolio = GetSQLItem("select PF_Code from vwDolfinPaymentFirmPortfolios" & IIf(UserLocationCode = 47, "Malta", "") & " where PF_Code = " & varPortfolioCode)
        If varFirmPortfolio <> "" Then
            IsFirmPortfolio = True
        End If
    End Function

    Public Function PopulateFormValue(ByRef cbo As System.Windows.Forms.ComboBox, Optional ByVal Arg1 As String = "", Optional ByVal Arg2 As String = "") As String
        PopulateFormValue = ""
        If LCase(cbo.Name) = "cboccy" Then
            PopulateFormValue = GetSQLItem("select CR_Name1 from vwDolfinPaymentSelectAccount where PF_Code = " & Arg1 & " and t_code = " & Arg2)
        End If
    End Function

    Public Function LiveDB() As Boolean
        LiveDB = False
        If conn.Database.ToString = "DolfinPayment" Then
            LiveDB = True
        End If
    End Function

    Public Function IsAboveThreshold(ByVal vAmount As Double, ByVal vCCYCode As Double) As Boolean
        IsAboveThreshold = 0
        Dim varIsAbovethreshold As String = 0
        varIsAbovethreshold = GetSQLFunction("Select dbo.IsAboveThreshold(" & vAmount & "," & vCCYCode & ")")

        If CBool(varIsAbovethreshold) Then
            IsAboveThreshold = 1
        End If
    End Function

    Public Function CreateTransactionsSwift(ByRef ExportFileCount As Dictionary(Of String, Integer)) As Boolean
        Dim ExportFilesList As New Dictionary(Of String, String)

        'ClsPay.CreateTransactionsXL()
        UpdatePreMovement()

        Dim TransConn As New SqlConnection
        TransConn.ConnectionString = varIMSConnection
        TransConn.Open()
        Dim transaction As SqlTransaction = TransConn.BeginTransaction


        Dim RunAllSwifts As String = ClsIMS.GetSQLItem("Select * from vwDolfinPaymentAllowSwifts")
        If RunAllSwifts = "True" Or RunAllSwifts = "1" Then
            Dim reader As SqlClient.SqlDataReader = GetSQLData("select * from vwDolfinPaymentReadySwift")
            Try
                Dim vReviewCount As Integer = 0
                If reader.HasRows Then
                    While reader.Read
                        GetReaderItemIntoPaymentClass(reader)

                        If ClsPay.PaymentType <> GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> GetPaymentType(cPayDescInPortLoan) And ClsPay.PaymentType <> GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> GetPaymentType(cPayDescExIn) And ClsPay.PaymentType <> GetPaymentType(cPayDescExClientIn) Then
                            ClsPay.BeneficiaryPortfolioShortcut = ClsPay.OrderPortfolioShortcut
                        End If
                    End While
                End If

                If LiveDB() Then
                    RunSQLJob(cSwiftJobName)
                Else
                    RunSQLJob(cSwiftJobName & " UAT")
                End If

                'If Not ClsPay.CreateExportFilesSwift(ExportFilesList) Then
                'Throw New System.Exception("There has been a problem creating the export files")
                'End If

                ExportFileCount.Add(GetStatusType(cStatusPendingReview), vReviewCount)
                ClsPay.CopyExportFiles(ExportFilesList, ExportFileCount)

                transaction.Commit()
                CreateTransactionsSwift = True
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwift: Created transactions files", 0)

            Catch ex As Exception
                CreateTransactionsSwift = False
                Try
                    transaction.Rollback()
                    AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwift - Rollback transactions")
                Catch ex2 As Exception
                    AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwift closing trans cannot rollback")
                End Try
            Finally
                reader.Close()
                TransConn.Close()
            End Try
        Else
            CreateTransactionsSwift = False
            MsgBox("The block swift option is currently activated. No Swifts will be sent until the block has been removed from the system settings.", vbCritical, "No Swifts will be sent")
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwift: RunAllSwifts in Systems settings is false. No Swifts will be sent", 0)
        End If
    End Function

    Public Function CreateTransactionsIMS() As Boolean
        Dim ExportFileCount As New Dictionary(Of String, Integer)
        Dim ExportFilesListIMS As New Dictionary(Of String, String)

        Dim TransConn As New SqlConnection
        TransConn.ConnectionString = varIMSConnection
        TransConn.Open()
        Dim transaction As SqlTransaction = TransConn.BeginTransaction

        Dim reader As SqlClient.SqlDataReader = GetSQLData("select * from vwDolfinPaymentReadyIMSSendNow")
        Try
            Dim vReviewCount As Integer = 0
            If reader.HasRows Then
                ClsPayAuth = New ClsPayment
                While reader.Read
                    GetReaderItemIntoAuthPaymentClass(IIf(IsDBNull(reader.Item("P_ID")), 0, reader.Item("P_ID")))
                    If LockSendIMSFile(0, 1, ClsPayAuth.SelectedPaymentID) Then
                        If ClsPayAuth.PaymentType <> GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> GetPaymentType(cPayDescInPortLoan) And ClsPayAuth.PaymentType <> GetPaymentType(cPayDescInOther) And ClsPayAuth.PaymentType <> GetPaymentType(cPayDescExIn) And ClsPayAuth.PaymentType <> GetPaymentType(cPayDescExClientIn) Then
                            ClsPayAuth.BeneficiaryPortfolioShortcut = ClsPayAuth.OrderPortfolioShortcut
                        End If

                        If Not ClsPayAuth.CreateExportFilesIMS(ExportFilesListIMS) Then
                            Throw New System.Exception("There has been a problem creating the export files")
                        End If
                    End If
                End While
            End If
            ExportFileCount.Add(GetStatusType(cStatusPendingReview), vReviewCount)
            ClsPayAuth.CopyExportFiles(ExportFilesListIMS, ExportFileCount)
            transaction.Commit()
            CreateTransactionsIMS = True
            AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CreateTransactionsIMS: Created transactions files", 0)
        Catch ex As Exception
            CreateTransactionsIMS = False
            Try
                transaction.Rollback()
                LockSendIMSFile(0, 0, ClsPayAuth.SelectedPaymentID)
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsIMS - Rollback transactions")
            Catch ex2 As Exception
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsIMS closing trans cannot rollback")
            End Try
        Finally
            reader.Close()
            TransConn.Close()
        End Try
    End Function

    Public Function CreateTransactionsIMSRD() As Boolean
        Dim ExportFileCount As New Dictionary(Of String, Integer)
        Dim ExportFilesListIMS As New Dictionary(Of String, String)

        Dim TransConn As New SqlConnection
        TransConn.ConnectionString = varIMSConnection
        TransConn.Open()
        Dim transaction As SqlTransaction = TransConn.BeginTransaction

        Dim reader As SqlClient.SqlDataReader = GetSQLData("select * from vwDolfinPaymentReceiveDeliverGridView where PRD_StatusID in (14,15) and PRD_IMSAcknowledged = 0 and isnull(PRD_IMSSentTime,'') = ''")
        Try
            Dim vReviewCount As Integer = 0
            If reader.HasRows Then
                While reader.Read
                    GetReaderItemIntoRDClass(IIf(IsDBNull(reader.Item("PRD_ID")), 0, reader.Item("PRD_ID")))
                    If LockSendIMSFile(1, 1, ClsRD.SelectedTransactionID) Then
                        If Not ClsRD.CreateExportFilesIMSRD(ExportFilesListIMS) Then
                            Throw New System.Exception("There has been a problem creating the export files")
                        End If
                    End If
                End While
            End If
            ExportFileCount.Add(GetStatusType(cStatusPendingReview), vReviewCount)
            ClsRD.CopyExportFilesRD(ExportFilesListIMS, ExportFileCount)
            transaction.Commit()
            CreateTransactionsIMSRD = True
            AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "CreateTransactionsIMSRD: Created transactions files", 0)
        Catch ex As Exception
            CreateTransactionsIMSRD = False
            Try
                transaction.Rollback()
                LockSendIMSFile(1, 0, ClsPayAuth.SelectedPaymentID)
                AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in CreateTransactionsIMSRD - Rollback transactions")
            Catch ex2 As Exception
                AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in CreateTransactionsIMSRD closing trans cannot rollback")
            End Try
        Finally
            reader.Close()
            TransConn.Close()
        End Try
    End Function

    Public Function CreateTransactionsSwiftBatch() As Boolean
        CreateTransactionsSwiftBatch = False
        UpdatePreMovement()

        Dim RunAllSwifts As String = ClsIMS.GetSQLItem("Select * from vwDolfinPaymentAllowSwifts")
        If RunAllSwifts = "True" Or RunAllSwifts = "1" Then
            Try
                If LiveDB() Then
                    RunSQLJob(cSwiftJobNameBatch)
                Else
                    RunSQLJob(cSwiftJobNameBatch & " UAT")
                End If
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwiftBatch: Created transactions files", 0)
                CreateTransactionsSwiftBatch = True
            Catch ex As Exception
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CreateTransactionsSwiftBatch")
            End Try
        Else
            MsgBox("The block swift option is currently activated. No Swifts will be sent until the block has been removed from the system settings.", vbCritical, "No Swifts will be sent")
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, "CreateTransactionsSwiftBatch: RunAllSwifts in Systems settings is false. No Swifts will be sent", 0)
        End If
    End Function

    Public Function UpdatePreMovement() As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPreUpdateMovementsRow"

            Cmd.ExecuteNonQuery()
            UpdatePreMovement = True
        Catch ex As Exception
            UpdatePreMovement = False
        End Try
    End Function

    Public Function UpdateStatusRow(ByVal varID As Double, ByVal varStatus As String) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPostUpdateMovementsRowStatus"
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("varStatus", SqlDbType.NVarChar).Value = varStatus

            Cmd.ExecuteNonQuery()
            UpdateStatusRow = True
        Catch ex As Exception
            UpdateStatusRow = False
        End Try
    End Function

    Public Function UpdateTransactionRow(ByVal varID As Double, ByVal varName As String, ByVal varFileName As String) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPostUpdateMovementsRow"

            If varName = GetFileExportName("SwiftName") Then
                Cmd.Parameters.Add("Opt", SqlDbType.Int).Value = 1
            Else
                Cmd.Parameters.Add("Opt", SqlDbType.Int).Value = 2
            End If
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("varFileName", SqlDbType.NVarChar).Value = varFileName
            Cmd.ExecuteNonQuery()
            UpdateTransactionRow = True
        Catch ex As Exception
            UpdateTransactionRow = False
        End Try
    End Function

    Public Function AddJobToReportQueue(ByVal varPFCode As Double, ByVal varFromDate As Date, ByVal varToDate As Date, ByVal varEmailTo As String, ByVal varRunNow As Boolean) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddJobToReportQueue"
            Cmd.CommandTimeout = 0

            Cmd.Parameters.Add("ReportParamValue", SqlDbType.Int).Value = varPFCode
            Cmd.Parameters.Add("FromDate", SqlDbType.Date).Value = varFromDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("ToDate", SqlDbType.Date).Value = varToDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("EmailTo", SqlDbType.NVarChar).Value = varEmailTo
            Cmd.Parameters.Add("RunNow", SqlDbType.NVarChar).Value = IIf(varRunNow, 1, 0)
            Cmd.ExecuteNonQuery()
            AddJobToReportQueue = True
        Catch ex As Exception
            AddJobToReportQueue = False
            AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - error in AddJobToReportQueue")
        End Try
    End Function


    Public Function AddQuarterlyJobToReportQueue(ByVal RunType As Integer, ByVal varFromDate As Date, ByVal varToDate As Date, ByVal varEmailTo As String, ByVal varRunNow As Boolean) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddQuarterlyJobToReportQueue"
            Cmd.CommandTimeout = 0

            Cmd.Parameters.Add("RunType", SqlDbType.Int).Value = RunType
            Cmd.Parameters.Add("FromDate", SqlDbType.Date).Value = varFromDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("ToDate", SqlDbType.Date).Value = varToDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("EmailTo", SqlDbType.NVarChar).Value = varEmailTo
            Cmd.Parameters.Add("RunNow", SqlDbType.NVarChar).Value = IIf(varRunNow, 1, 0)
            Cmd.ExecuteNonQuery()
            AddQuarterlyJobToReportQueue = True
        Catch ex As Exception
            AddQuarterlyJobToReportQueue = False
            AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - error in AddQuarterlyJobToReportQueue")
        End Try
    End Function

    Public Function UpdateSendToEmailTickbox(ByVal varID As Double, ByVal varTick As Boolean) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateRptScheduleLogSendToEmail"
            Cmd.CommandTimeout = 0

            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Tick", SqlDbType.Bit).Value = IIf(varTick, 1, 0)
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = FullUserName
            Cmd.ExecuteNonQuery()
            UpdateSendToEmailTickbox = True
        Catch ex As Exception
            UpdateSendToEmailTickbox = False
            AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - error in UpdateSendToEmailTickbox")
        End Try
    End Function

    Public Function UpdateTransactionRowRD(ByVal varID As Double, ByVal varName As String, ByVal varFileName As String) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPostUpdateMovementsRowRD"

            Cmd.Parameters.Add("Opt", SqlDbType.Int).Value = 2
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("varFileName", SqlDbType.NVarChar).Value = varFileName
            Cmd.ExecuteNonQuery()
            UpdateTransactionRowRD = True
        Catch ex As Exception
            UpdateTransactionRowRD = False
        End Try
    End Function

    Public Function GetIMSFile(ByRef varID As Double, ByRef IMSFile As String) As Boolean
        Dim Cmd As New SqlClient.SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentCreateIMSFile"
            Cmd.Parameters.Add("@varSelectedPaymentID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@varIMSFile", SqlDbType.NVarChar).Value = IMSFile

            Dim IMSreader As SqlClient.SqlDataReader = Cmd.ExecuteReader()
            If IMSreader.HasRows Then
                IMSreader.Read()
                IMSFile = IMSreader(0).ToString
            End If
            IMSreader.Close()
            GetIMSFile = True
        Catch ex As Exception
            GetIMSFile = False
        End Try
    End Function

    Public Function LockSendIMSFile(ByVal varLockPayType As Integer, ByVal varLock As Boolean, ByVal varID As Double) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            If varLockPayType = 0 Then
                Cmd.CommandText = "DolfinPaymentLockSendIMSFile"
            Else
                Cmd.CommandText = "DolfinPaymentLockSendIMSFileRD"
            End If


            If varLock Then
                Cmd.Parameters.Add("Lock", SqlDbType.Int).Value = 1
            Else
                Cmd.Parameters.Add("Lock", SqlDbType.Int).Value = 0
            End If
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
            Cmd.ExecuteNonQuery()
            LockSendIMSFile = True
        Catch ex As Exception
            LockSendIMSFile = False
        End Try
    End Function

    Public Sub UpdatePendingMovements()
        Dim UpdatePendingConn As New SqlConnection
        Dim reader As SqlDataReader = Nothing
        Dim PendingReview As Boolean = False
        Dim varReaderID As Double = 0
        Dim varReaderSettleDate As Date = Now
        'Dim varReaderStatusID As Integer
        'Dim varReaderSentApproveEmail As Boolean
        'Dim varReaderSentApproveEmailCompliance As Boolean
        Dim varReaderSendSwift As Boolean = False
        'Dim varReaderSendIMS As Boolean
        'Dim varReaderAboveThreshold As Boolean
        'Dim varReaderModifiedTime As Date
        Dim varReaderPaymentType As String = ""
        Dim varReaderPaymentTypeID As Double = 0
        Dim varReaderAmount As Double = 0
        Dim varReaderOrderAccountCode As Integer = 0
        Dim varReaderPortfolioCode As Integer = 0
        Dim varReaderOrderBrokerCode As Integer = 0
        Dim varReaderCCYCode As Integer = 0
        Dim varReaderBenAccountNumber As String = ""
        Dim varReaderBenSwift As String = ""
        Dim varReaderBenIBAN As String = ""
        Dim varReaderBenBankSwift As String = ""
        Dim varReaderBenBankIBAN As String = ""
        Dim varReaderOtherID As Integer = 0
        Dim varReaderDefaultSendSwift As Boolean = False
        Dim varReaderOtherDefaultSendSwift As Boolean = False
        Dim varReaderOtherDefaultCheckSwift As Boolean = False
        Dim varPendingReason As String = ""

        Try
            UpdatePendingConn.ConnectionString = varIMSConnection
            UpdatePendingConn.Open()

            'reader = GetSQLData("select P_ID,P_SettleDate,P_StatusId,P_SentApproveEmail,P_SentApproveEmailCompliance,P_SendSwift,P_SendIMS,P_AboveThreshold,P_ModifiedTime,P_Other_ID from vwDolfinPayment where P_StatusId in (" & cStatusReady & "," & cStatusPendingAuth & "," & cStatusPendingCompliance & "," & cStatusComplianceRejected & ")")
            'If reader.HasRows Then
            'While reader.Read
            'varReaderID = IIf(IsNumeric(reader("P_ID")), CInt(reader("P_ID")), 0)
            'varReaderSettleDate = IIf(IsDate(reader("P_SettleDate")), CDate(reader("P_SettleDate")), Now)
            'varReaderStatusID = IIf(IsNumeric(reader("P_StatusId")), CInt(reader("P_StatusId")), 0)
            'varReaderSentApproveEmail = IIf(IsDBNull(reader("P_SentApproveEmail")), 0, reader("P_SentApproveEmail"))
            'varReaderSentApproveEmailCompliance = IIf(Not IsNumeric("P_SentApproveEmailCompliance"), 0, reader("P_SentApproveEmailCompliance"))
            'varReaderSendSwift = IIf(IsDBNull(reader("P_SendSwift")), 0, reader("P_SendSwift"))
            'varReaderSendIMS = IIf(IsDBNull(reader("P_SendIMS")), 0, reader("P_SendIMS"))
            'varReaderAboveThreshold = IIf(IsDBNull(reader("P_AboveThreshold")), 0, reader("P_AboveThreshold"))
            'varReaderModifiedTime = IIf(IsDate(reader("P_ModifiedTime")), reader("P_ModifiedTime"), "2000-01-01 10:00")
            'varReaderOtherID = IIf(IsDBNull(reader("P_Other_ID")), 0, reader("P_Other_ID"))
            'If varReaderSettleDate.Date < Now.Date And varReaderStatusID <> cStatusComplianceRejected And varReaderOtherID <> cPayDescInOtherBankCharges And varReaderOtherID <> cPayDescInOtherBankChargesCovChg _
            '           And varReaderOtherID <> cPayDescInOtherBankInterestCreditOrdOnly And varReaderOtherID <> cPayDescInOtherBankInterestDebitOrdOnly _
            '          And varReaderOtherID <> cPayDescInOtherBankInterestCreditBenOnly And varReaderOtherID <> cPayDescInOtherBankInterestDebitBenOnly Then
            'RollSettleDate(varReaderID)
            'End If
            '
            'If varReaderStatusID = cStatusPendingAuth And (varReaderSentApproveEmail Or varReaderAboveThreshold) Then
            'ClsPay.CheckApproveMovementEmail(varReaderID, varReaderSendSwift, varReaderSendIMS, varReaderAboveThreshold, varReaderModifiedTime, False)
            'ElseIf (varReaderStatusID = cStatusPendingCompliance And varReaderSentApproveEmailCompliance) Or (varReaderStatusID = cStatusComplianceRejected And varReaderSettleDate.Date = Now.Date) Then
            'ClsPay.CheckApproveMovementEmail(varReaderID, varReaderSendSwift, varReaderSendIMS, varReaderAboveThreshold, varReaderModifiedTime, True)
            '        End If
            'End While
            'End If
            'reader.Close()


            reader = GetSQLData("select P_ID,P_statusid,P_PaymentTypeid,P_PaymentType,P_SettleDate,P_Amount,P_OrderAccountCode,P_PortfolioCode,
                                P_OrderBrokerCode,P_CCYCode,P_BenAccountNumber,P_BenName,P_BenAddress1,P_BenAddress2,P_BenZip,
                                P_BenSwift,P_BenIBAN,P_BenBankSwift,P_BenBankIBAN,P_SendSwift,P_Other_ID,PT_DefaultSendSwift,OTH_DefaultSendSwift,OTH_DefaultCheckSwift,P_SendSwift
                                from vwDolfinPaymentGridView where p_statusid in (" & cStatusReady & "," & cStatusPendingAuth & ")")

            If reader.HasRows Then
                While reader.Read
                    PendingReview = False
                    varReaderID = 0
                    If Not IsDBNull(reader("P_ID")) Then
                        varReaderID = IIf(IsNumeric(reader("P_ID")), CInt(reader("P_ID")), 0)
                    End If

                    varReaderPaymentType = ""
                    If Not IsDBNull(reader("P_PaymentType")) Then
                        varReaderPaymentType = reader("P_PaymentType").ToString
                    End If

                    varReaderPaymentTypeID = IIf(Not IsDBNull(reader("P_PaymentTypeid")), reader("P_PaymentTypeid").ToString, "")

                    varReaderSettleDate = Now
                    If Not IsDBNull(reader("P_SettleDate")) Then
                        varReaderSettleDate = IIf(IsDate(reader("P_SettleDate")), CDate(reader("P_SettleDate")), Now)
                    End If

                    varReaderAmount = 0
                    If Not IsDBNull(reader("P_Amount")) Then
                        varReaderAmount = IIf(IsNumeric(reader("P_Amount")), CDbl(reader("P_Amount")), 0)
                    End If

                    varReaderOrderAccountCode = 0
                    If Not IsDBNull(reader("P_OrderAccountCode")) Then
                        varReaderOrderAccountCode = IIf(IsNumeric(reader("P_OrderAccountCode")), CInt(reader("P_OrderAccountCode")), 0)
                    End If

                    varReaderPortfolioCode = 0
                    If Not IsDBNull(reader("P_PortfolioCode")) Then
                        varReaderPortfolioCode = IIf(IsNumeric(reader("P_PortfolioCode")), CInt(reader("P_PortfolioCode")), 0)
                    End If

                    varReaderOrderBrokerCode = 0
                    If Not IsDBNull(reader("P_OrderBrokerCode")) Then
                        varReaderOrderBrokerCode = IIf(IsNumeric(reader("P_OrderBrokerCode")), CInt(reader("P_OrderBrokerCode")), 0)
                    End If

                    varReaderCCYCode = 0
                    If Not IsDBNull(reader("P_CCYCode")) Then
                        varReaderCCYCode = IIf(IsNumeric(reader("P_CCYCode")), CInt(reader("P_CCYCode")), 0)
                    End If

                    varReaderBenAccountNumber = ""
                    If Not IsDBNull(reader("P_BenAccountNumber")) Then
                        varReaderBenAccountNumber = reader("P_BenAccountNumber").ToString
                    End If

                    varReaderBenSwift = ""
                    If Not IsDBNull(reader("P_BenSwift")) Then
                        varReaderBenSwift = reader("P_BenSwift").ToString
                    End If

                    varReaderBenIBAN = ""
                    If Not IsDBNull(reader("P_BenIBAN")) Then
                        varReaderBenIBAN = reader("P_BenIBAN").ToString
                    End If

                    varReaderOtherID = 0
                    If Not IsDBNull(reader("P_Other_ID")) Then
                        varReaderOtherID = IIf(IsNumeric(reader("P_Other_ID")), CInt(reader("P_Other_ID")), 0)
                    End If

                    varReaderSendSwift = IIf(IsDBNull(reader("P_SendSwift")), 0, reader("P_SendSwift"))
                    varReaderBenBankSwift = IIf(Not IsDBNull(reader("P_BenBankSwift")), reader("P_BenBankSwift").ToString, "")
                    varReaderBenBankIBAN = IIf(Not IsDBNull(reader("P_BenBankIBAN")), reader("P_BenBankIBAN").ToString, "")
                    varReaderDefaultSendSwift = IIf(Not IsDBNull(reader("PT_DefaultSendSwift")), reader("PT_DefaultSendSwift"), False)
                    varReaderOtherDefaultSendSwift = IIf(Not IsDBNull(reader("OTH_DefaultSendSwift")), reader("OTH_DefaultSendSwift"), False)
                    varReaderOtherDefaultCheckSwift = IIf(Not IsDBNull(reader("OTH_DefaultCheckSwift")), reader("OTH_DefaultCheckSwift"), False)

                    If varReaderPaymentType <> GetPaymentType(cPayDescExIn) And varReaderOtherID <> cPayDescInOtherBankCharges Then
                        Dim varBalance As Double = GetBalance(varReaderPortfolioCode, varReaderOrderAccountCode)
                        Dim varPendingAmount As Double = GetPendingAmount(varReaderID, varReaderPortfolioCode, varReaderCCYCode, varReaderOrderAccountCode)

                        If varReaderAmount > Strings.Format(varPendingAmount + varBalance, "#,##0.00") Then
                            varPendingReason = "Not enough in balance inc pending amounts (" & Strings.Format(varPendingAmount + varBalance, "#,##0.00") & ")"
                            'PendingReview = True
                        End If
                    End If

                    If varReaderPaymentType = GetPaymentType(cPayDescExOut) Or varReaderPaymentType = GetPaymentType(cPayDescExClientOut) Or varReaderPaymentType = GetPaymentType(cPayDescExVolopaOut) Or varReaderPaymentType = GetPaymentType(cPayDescIn) Or varReaderPaymentType = GetPaymentType(cPayDescExClientIn) Or varReaderPaymentType = GetPaymentType(cPayDescInOther) Then
                        If varReaderSettleDate = Now.Date Then
                            Dim varReaderCutOff As String = GetSQLItem("select co_cutofftime + ',' + co_cutofftimezone from vwDolfinPaymentCutOffTimes where co_brokercode = " & varReaderOrderBrokerCode & " and CO_CCYCode = " & varReaderCCYCode)

                            If varReaderCutOff <> "" Then
                                Dim varReaderCutOffTime As String = Left(varReaderCutOff, InStr(1, varReaderCutOff, ",", vbTextCompare) - 1)
                                Dim varReaderCutOffTimeZone As String = Right(varReaderCutOff, Len(varReaderCutOff) - InStr(1, varReaderCutOff, ",", vbTextCompare))

                                If CDate(varReaderSettleDate.Date & " " & varReaderCutOffTime) < ConvertTimeZone(varReaderCutOffTimeZone).DateTime Then
                                    varPendingReason = "Cut Off time gone - " & ConvertTimeZone(varReaderCutOffTimeZone).DateTime
                                    'PendingReview = True
                                End If
                            End If
                        End If
                    End If

                    If varReaderAmount = 0 Then
                        varPendingReason = "Cannot pay 0 amount"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderPaymentTypeID <> cPayDescExOut And varReaderPaymentTypeID <> cPayDescExClientOut And varReaderPaymentTypeID <> cPayDescExVolopaOut And varReaderBenAccountNumber = "" Then
                        varPendingReason = "Beneficiary account number is empty"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderBenSwift = "" And varReaderBenBankSwift = "" And varReaderSendSwift Then
                        varPendingReason = "Beneficiary swift is empty and trying to send a swift message"
                        PendingReview = True
                    End If

                    If ((varReaderDefaultSendSwift And Not varReaderOtherDefaultSendSwift) Or (varReaderDefaultSendSwift And varReaderOtherDefaultSendSwift And varReaderOtherDefaultCheckSwift)) And varReaderBenIBAN = "" And varReaderBenBankIBAN = "" And varReaderSendSwift Then
                        varPendingReason = "Beneficiary IBAN is empty and trying to send a swift message"
                        PendingReview = True
                    End If

                    If PendingReview Then
                        UpdateStatusRow(varReaderID, GetStatusType(cStatusPendingReview))
                        AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, "Pending transaction in UpdatePendingMovements because " & varPendingReason, varReaderID)
                    End If

                End While
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in UpdatePendingMovements", varReaderID)
        Finally
            If Not reader Is Nothing Then
                reader.Close()
            End If

            If Not UpdatePendingConn Is Nothing Then
                UpdatePendingConn.Close()
            End If
        End Try
    End Sub

    Public Function RollSettleDate(ByVal varID As Double) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentRollSettleDate"
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID

            Cmd.ExecuteNonQuery()
            RollSettleDate = True
        Catch ex As Exception
            RollSettleDate = False
        End Try
    End Function

    Public Sub UpdatePendingMovementsRD()
        ExecuteString(conn, "update dolfinpaymentreceivedeliver set prd_StatusID=3 where prd_StatusID in (1) And 
                            (isnull(prd_portfolioCode,'')='' or isnull(prd_ccyCode,'')='' or isnull(prd_instCode,'')=''
                            Or isnull(prd_BrokerCode,'')='' Or isnull(prd_Amount,'')='')")
    End Sub

    Public Sub RefreshMovementGrid(ByVal dgv As DataGridView, ByVal varWhereClauseFilter As String)
        If varWhereClauseFilter <> "" Then
            RefreshPaymentGrid(dgv, varWhereClauseFilter)
        Else
            RefreshPaymentGrid(dgv,)
        End If
        ClsPay.FormatPaymentGrid(dgv)
        FormatGridPaymentBalanceColour(dgv, "OrderBalance")
    End Sub

    Public Sub CheckStatusGrid(ByRef dgv As DataGridView, ByRef CheckSwiftAck As Boolean, ByRef CheckSwiftPaid As Boolean, ByRef CheckPending As Boolean)
        Try
            For i As Integer = 0 To dgv.RowCount - 1
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSent) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusAwaitingSwift) Then
                    CheckSwiftAck = True
                ElseIf dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSwiftAck) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusIMSAck) Then
                    CheckSwiftPaid = True
                ElseIf dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusReady) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusPendingAuth) Then
                    CheckPending = True
                End If
            Next
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CheckStatusGrid")
        End Try
    End Sub

    Public Sub CheckStatusGridView(ByRef dgv As DataGridView, ByRef SendIMS As Boolean, ByRef CheckSwiftAck As Boolean, ByRef CheckSwiftPaid As Boolean, ByRef CheckPending As Boolean)
        Try
            For i As Integer = 0 To dgv.RowCount - 1
                If dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusSwiftAck) And dgv.Rows(i).Cells("P_SwiftAcknowledged").Value And Not dgv.Rows(i).Cells("P_IMSAcknowledged").Value _
                        And dgv.Rows(i).Cells("P_SendIMS").Value And Not IsDate(dgv.Rows(i).Cells("P_IMSSentTime").Value) Then
                    SendIMS = True
                End If
                If dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusSent) Or dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusAwaitingSwift) Then
                    CheckSwiftAck = True
                ElseIf dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusSwiftAck) Then
                    CheckSwiftPaid = True
                ElseIf dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusReady) Or dgv.Rows(i).Cells("P_Status").Value = GetStatusType(cStatusPendingAuth) Then
                    CheckPending = True
                End If
            Next
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CheckStatusGrid")
        End Try
    End Sub

    Public Sub CheckStatusGridRD(ByRef dgv As DataGridView, ByRef SendIMS As Boolean, ByRef CheckSwiftAck As Boolean, ByRef CheckSwiftPaid As Boolean)
        Try
            For i As Integer = 0 To dgv.RowCount - 1
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusCustodianConfirmAcknowledged) And Not dgv.Rows(i).Cells("IMSAck").Value And Not IsDate(dgv.Rows(i).Cells("IMSSentTime").Value) Then
                    SendIMS = True
                End If
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSent) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusAwaitingSwift) Then
                    CheckSwiftAck = True
                End If
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSwiftAck) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusCustodianNotMatched) Or
                    dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusCustodianPending) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusCustodianRejected) Or
                    dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSwiftAck) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSwiftErr) Or
                    dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusCustodianCancelled) And DateDiff("d", IIf(IsDBNull(dgv.Rows(i).Cells("SentTime").Value), "2015-01-01", dgv.Rows(i).Cells("SentTime").Value), Now.Date) <= 3 Then
                    CheckSwiftPaid = True
                End If
            Next
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - error in CheckStatusGridRD")
        End Try
    End Sub

    Public Sub CheckStatusGridSwift(ByRef dgv As DataGridView, ByRef CheckSwiftAck As Boolean, ByRef CheckSwiftPaid As Boolean)
        Try
            For i As Integer = 0 To dgv.RowCount - 1
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSent) Or dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusAwaitingSwift) Then
                    CheckSwiftAck = True
                End If
                If dgv.Rows(i).Cells("Status").Value = GetStatusType(cStatusSwiftAck) And DateDiff("d", IIf(IsDBNull(dgv.Rows(i).Cells("SwiftSentTime").Value), "2015-01-01", dgv.Rows(i).Cells("SwiftSentTime").Value), Now.Date) <= 3 Then
                    CheckSwiftPaid = True
                End If
            Next
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameViewAllSwifts, Err.Description & " - error in CheckStatusGridSwift")
        End Try
    End Sub

    Public Function CreateTreasuryTransactions(ByRef dgv As DataGridView, ByRef ExportFileCount As Dictionary(Of String, Integer), ByRef TransDate As Date) As Boolean
        Dim varFileDate As Date
        Dim ExportFilesList As New Dictionary(Of String, String)
        '
        Dim varFileDateStr As String = GetSQLFunction("select getdate()")
        If IsDate(varFileDateStr) Then
            varFileDate = CDate(varFileDateStr)
        End If

        Try
            ClsPayTreasury.SendSwift = False
            ClsPayTreasury.SendIMS = True

            Dim FullFilePath As String = ClsPayTreasury.GetFullFilePath(GetFileExportName("IMSName"), ExportFilesList, varFileDate)
            Dim objWriter As IO.StreamWriter = IO.File.CreateText(FullFilePath)
            Dim varCount As Integer

            ClsPayTreasury.CreateTransactionsHeadersIFT(objWriter, varFileDate)
            For i As Integer = 0 To dgv.Rows.Count - 2
                If IsNumeric(dgv.Rows(i).Cells(2).Value) Then
                    GetReaderTreasuryItemIntoPaymentClass(dgv, i, FullFilePath, TransDate)
                    For j = 1 To 2
                        If Not ClsPayTreasury.CreateTreasuryExportFiles(FullFilePath, objWriter, IIf(j = 1, True, False)) Then
                            Throw New System.Exception("There has been a problem creating the treasury export files")
                        End If
                        objWriter.WriteLine()
                        varCount = varCount + 1
                    Next
                End If
            Next
            ClsPayTreasury.CreateTransactionsTrailerIFT(objWriter, varCount)
            objWriter.Close()
            CreateTreasuryTransactions = True
            ClsPayTreasury.CopyExportFiles(ExportFilesList, ExportFileCount)
            AddAudit(cAuditStatusSuccess, cAuditGroupNameOmnibusSwitch, "CreateTreasuryTransactions: Created movement files", 0)
        Catch ex As Exception
            CreateTreasuryTransactions = False
            AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - error in CreateTreasuryTransactions")
        End Try
    End Function

    Public Sub GetReaderTreasuryItemIntoPaymentClass(ByVal dgv As DataGridView, ByVal vRow As Integer, ByVal FullFilePath As String, ByRef TransDate As Date)
        Try
            ClsPayTreasury.PaymentType = "Internal Omnibus Switch"
            ClsPayTreasury.CCYCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("CCYCode").Value), "", dgv.Rows(vRow).Cells("CCYCode").Value)
            ClsPayTreasury.CCY = IIf(IsDBNull(dgv.Rows(vRow).Cells("CCY").Value), "", dgv.Rows(vRow).Cells("CCY").Value)
            ClsPayTreasury.BeneficiaryCCY = IIf(IsDBNull(dgv.Rows(vRow).Cells("CCY").Value), "", dgv.Rows(vRow).Cells("CCY").Value)
            ClsPayTreasury.TransDate = TransDate
            ClsPayTreasury.SettleDate = TransDate
            ClsPayTreasury.PortfolioCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioCodeA").Value), 0, dgv.Rows(vRow).Cells("PortfolioCodeA").Value)
            ClsPayTreasury.Portfolio = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioA").Value), "", dgv.Rows(vRow).Cells("PortfolioA").Value)
            ClsPayTreasury.Amount = IIf(IsDBNull(dgv.Rows(vRow).Cells(2).Value) Or dgv.Rows(vRow).Cells(2).Value = "", 0, Math.Abs(CDbl(dgv.Rows(vRow).Cells(2).Value)))
            ClsPayTreasury.IMSPayInCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("PT_IMSPayInCode").Value), 0, dgv.Rows(vRow).Cells("PT_IMSPayInCode").Value)
            ClsPayTreasury.IMSPayOutCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("PT_IMSPayOutCode").Value), 0, dgv.Rows(vRow).Cells("PT_IMSPayOutCode").Value)
            ClsPayTreasury.PaymentFilePath = FullFilePath

            If dgv.Rows(vRow).Cells(2).Value > 0 Then
                ClsPayTreasury.OrderAccountCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountCodeB").Value), 0, dgv.Rows(vRow).Cells("AccountCodeB").Value)
                ClsPayTreasury.OrderAccount = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountB").Value), "", dgv.Rows(vRow).Cells("AccountB").Value)
                ClsPayTreasury.OrderPortfolioShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioShortcutB").Value), "", dgv.Rows(vRow).Cells("PortfolioShortcutB").Value)
                ClsPayTreasury.OrderBrokerShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerShortcutB").Value), "", dgv.Rows(vRow).Cells("BrokerShortcutB").Value)
                ClsPayTreasury.OrderAccountShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountShortcutB").Value), "", dgv.Rows(vRow).Cells("AccountShortcutB").Value)
                ClsPayTreasury.BeneficiaryAccountCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountCodeA").Value), 0, dgv.Rows(vRow).Cells("AccountCodeA").Value)
                ClsPayTreasury.BeneficiaryAccount = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountA").Value), "", dgv.Rows(vRow).Cells("AccountA").Value)
                ClsPayTreasury.BeneficiaryPortfolioShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioShortcutA").Value), "", dgv.Rows(vRow).Cells("PortfolioShortcutA").Value)
                ClsPayTreasury.BeneficiaryBrokerShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerShortcutA").Value), "", dgv.Rows(vRow).Cells("BrokerShortcutA").Value)
                ClsPayTreasury.BeneficiaryAccountShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountShortcutA").Value), "", dgv.Rows(vRow).Cells("AccountShortcutA").Value)
                ClsPayTreasury.Comments = "Internal Omnibus Switch from " & IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerB").Value), "", dgv.Rows(vRow).Cells("BrokerB").Value) & " to " & IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerA").Value), "", dgv.Rows(vRow).Cells("BrokerA").Value)
            Else
                ClsPayTreasury.OrderAccountCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountCodeA").Value), 0, dgv.Rows(vRow).Cells("AccountCodeA").Value)
                ClsPayTreasury.OrderAccount = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountA").Value), "", dgv.Rows(vRow).Cells("AccountA").Value)
                ClsPayTreasury.OrderPortfolioShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioShortcutA").Value), "", dgv.Rows(vRow).Cells("PortfolioShortcutA").Value)
                ClsPayTreasury.OrderBrokerShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerShortcutA").Value), "", dgv.Rows(vRow).Cells("BrokerShortcutA").Value)
                ClsPayTreasury.OrderAccountShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountShortcutA").Value), "", dgv.Rows(vRow).Cells("AccountShortcutA").Value)
                ClsPayTreasury.BeneficiaryAccountCode = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountCodeB").Value), 0, dgv.Rows(vRow).Cells("AccountCodeB").Value)
                ClsPayTreasury.BeneficiaryAccount = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountB").Value), "", dgv.Rows(vRow).Cells("AccountB").Value)
                ClsPayTreasury.BeneficiaryPortfolioShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("PortfolioShortcutB").Value), "", dgv.Rows(vRow).Cells("PortfolioShortcutB").Value)
                ClsPayTreasury.BeneficiaryBrokerShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerShortcutB").Value), "", dgv.Rows(vRow).Cells("BrokerShortcutB").Value)
                ClsPayTreasury.BeneficiaryAccountShortcut = IIf(IsDBNull(dgv.Rows(vRow).Cells("AccountShortcutB").Value), "", dgv.Rows(vRow).Cells("AccountShortcutB").Value)
                ClsPayTreasury.Comments = "Internal Omnibus Switch from " & IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerA").Value), "", dgv.Rows(vRow).Cells("BrokerA").Value) & " to " & IIf(IsDBNull(dgv.Rows(vRow).Cells("BrokerB").Value), "", dgv.Rows(vRow).Cells("BrokerB").Value)
            End If
            ClsPayTreasury.OrderRef = Right("O" & Now.ToString("ddMMyyHHmmssfff") & vRow.ToString, 20)
            ClsPayTreasury.BeneficiaryRef = Right("I" & Now.ToString("ddMMyyHHmmssfff") & vRow.ToString, 20)
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - error in GetReaderTreasuryItemIntoPaymentClass")
        End Try
    End Sub

    Public Sub GetReaderItemIntoPaymentClass(ByRef reader As SqlDataReader)
        Try
            ClsPay.SelectedPaymentID = IIf(IsDBNull(reader.Item("P_ID")), 0, reader.Item("P_ID"))
            ClsPay.PaymentType = IIf(IsDBNull(reader.Item("P_PaymentType")), "", reader.Item("P_PaymentType"))
            ClsPay.OrderMessageTypeID = IIf(IsDBNull(reader.Item("P_MessageTypeId")), "", reader.Item("P_MessageTypeId"))
            ClsPay.MessageType = IIf(IsDBNull(reader.Item("P_MessageType")), "", reader.Item("P_MessageType"))
            ClsPay.PortfolioCode = IIf(IsDBNull(reader.Item("P_PortfolioCode")), 0, reader.Item("P_PortfolioCode"))
            ClsPay.Portfolio = IIf(IsDBNull(reader.Item("P_Portfolio")), "", reader.Item("P_Portfolio"))
            ClsPay.TransDate = IIf(IsDBNull(reader.Item("P_TransDate")), Now, reader.Item("P_transDate"))
            ClsPay.SettleDate = IIf(IsDBNull(reader.Item("P_SettleDate")), Now, reader.Item("P_SettleDate"))
            ClsPay.Amount = IIf(IsDBNull(reader.Item("P_Amount")), 0, reader.Item("P_Amount"))
            ClsPay.CCYCode = IIf(IsDBNull(reader.Item("P_CCYCode")), 0, reader.Item("P_CCYCode"))
            ClsPay.CCY = IIf(IsDBNull(reader.Item("P_CCY")), "", reader.Item("P_CCY"))
            ClsPay.OrderBrokerCode = IIf(IsDBNull(reader.Item("P_OrderBrokerCode")), 0, reader.Item("P_OrderBrokerCode"))
            ClsPay.OrderAccountCode = IIf(IsDBNull(reader.Item("P_OrderAccountCode")), 0, reader.Item("P_OrderAccountCode"))
            ClsPay.OrderAccount = IIf(IsDBNull(reader.Item("P_OrderAccount")), "", reader.Item("P_OrderAccount"))
            ClsPay.OrderName = IIf(IsDBNull(reader.Item("P_OrderName")), "", reader.Item("P_OrderName"))
            ClsPay.OrderRef = IIf(IsDBNull(reader.Item("P_OrderRef")), "", reader.Item("P_OrderRef"))
            ClsPay.OrderAddress1 = IIf(IsDBNull(reader.Item("P_OrderAddress1")), "", reader.Item("P_OrderAddress1"))
            ClsPay.OrderAddress2 = IIf(IsDBNull(reader.Item("P_OrderAddress2")), "", reader.Item("P_OrderAddress2"))
            ClsPay.OrderZip = IIf(IsDBNull(reader.Item("P_OrderZip")), "", reader.Item("P_OrderZip"))
            ClsPay.OrderCountryID = IIf(IsDBNull(reader.Item("P_OrderCountryID")), 0, reader.Item("P_OrderCountryID"))
            ClsPay.OrderCountry = IIf(IsDBNull(reader.Item("P_OrderCountry")), "", reader.Item("P_OrderCountry"))
            ClsPay.OrderSwift = IIf(IsDBNull(reader.Item("P_OrderSwift")), "", reader.Item("P_OrderSwift"))
            ClsPay.OrderAccountNumber = IIf(IsDBNull(reader.Item("P_OrderAccountNumber")), "", reader.Item("P_OrderAccountNumber"))
            ClsPay.OrderIBAN = IIf(IsDBNull(reader.Item("P_OrderIBAN")), "", reader.Item("P_OrderIBAN"))
            ClsPay.OrderOther = IIf(IsDBNull(reader.Item("P_OrderOther")), "", reader.Item("P_OrderOther"))
            ClsPay.BeneficiaryBrokerCode = IIf(IsDBNull(reader.Item("P_BenBrokerCode")), 0, reader.Item("P_BenBrokerCode"))
            ClsPay.BeneficiaryAccountCode = IIf(IsDBNull(reader.Item("P_BenAccountCode")), 0, reader.Item("P_BenAccountCode"))
            ClsPay.BeneficiaryAccount = IIf(IsDBNull(reader.Item("P_BenAccount")), "", reader.Item("P_BenAccount"))
            ClsPay.BeneficiaryAddressID = IIf(IsDBNull(reader.Item("P_BenAddressID")), 0, reader.Item("P_BenAddressID"))
            ClsPay.BeneficiaryName = IIf(IsDBNull(reader.Item("P_BenName")), "", reader.Item("P_BenName"))
            ClsPay.BeneficiaryRef = IIf(IsDBNull(reader.Item("P_BenRef")), "", reader.Item("P_BenRef"))
            ClsPay.BeneficiaryAddress1 = IIf(IsDBNull(reader.Item("P_BenAddress1")), "", reader.Item("P_BenAddress1"))
            ClsPay.BeneficiaryAddress2 = IIf(IsDBNull(reader.Item("P_BenAddress2")), "", reader.Item("P_BenAddress2"))
            ClsPay.BeneficiaryCounty = IIf(IsDBNull(reader.Item("P_BenCounty")), "", reader.Item("P_BenCounty"))
            ClsPay.BeneficiaryZip = IIf(IsDBNull(reader.Item("P_BenZip")), "", reader.Item("P_BenZip"))
            ClsPay.BeneficiaryCountryID = IIf(IsDBNull(reader.Item("P_BenCountryID")), 0, reader.Item("P_BenCountryID"))
            ClsPay.BeneficiaryCountry = IIf(IsDBNull(reader.Item("P_BenCountry")), "", reader.Item("P_BenCountry"))
            ClsPay.BeneficiarySwift = IIf(IsDBNull(reader.Item("P_BenSwift")), "", reader.Item("P_BenSwift"))
            ClsPay.BeneficiaryAccountNumber = IIf(IsDBNull(reader.Item("P_BenAccountNumber")), "", reader.Item("P_BenAccountNumber"))
            ClsPay.BeneficiaryIBAN = IIf(IsDBNull(reader.Item("P_BenIBAN")), "", reader.Item("P_BenIBAN"))
            ClsPay.BeneficiaryOther = IIf(IsDBNull(reader.Item("P_BenOther")), "", reader.Item("P_BenOther"))
            ClsPay.BeneficiaryCCYCode = IIf(IsDBNull(reader.Item("P_BenCCYCode")), 0, reader.Item("P_BenCCYCode"))
            ClsPay.BeneficiaryCCY = IIf(IsDBNull(reader.Item("P_BenCCY")), "", reader.Item("P_BenCCY"))
            ClsPay.BeneficiaryRelationshipID = IIf(IsDBNull(reader.Item("P_BenRelationshipID")), 0, reader.Item("P_BenRelationshipID"))
            ClsPay.BeneficiaryTypeID = IIf(IsDBNull(reader.Item("P_BenTypeID")), 0, reader.Item("P_BenTypeID"))
            ClsPay.ExchangeRate = IIf(IsDBNull(reader.Item("P_ExchangeRate")), 1, reader.Item("P_ExchangeRate"))
            ClsPay.BeneficiarySubBankAddressID = IIf(IsDBNull(reader.Item("P_BenSubBankAddressID")), 0, reader.Item("P_BenSubBankAddressID"))
            ClsPay.BeneficiarySubBankName = IIf(IsDBNull(reader.Item("P_BenSubBankName")), "", reader.Item("P_BenSubBankName"))
            ClsPay.BeneficiarySubBankAddress1 = IIf(IsDBNull(reader.Item("P_BenSubBankAddress1")), "", reader.Item("P_BenSubBankAddress1"))
            ClsPay.BeneficiarySubBankAddress2 = IIf(IsDBNull(reader.Item("P_BenSubBankAddress2")), "", reader.Item("P_BenSubBankAddress2"))
            ClsPay.BeneficiarySubBankZip = IIf(IsDBNull(reader.Item("P_BenSubBankZip")), "", reader.Item("P_BenSubBankZip"))
            ClsPay.BeneficiarySubBankCountryID = IIf(IsDBNull(reader.Item("P_BenSubBankCountryID")), 0, reader.Item("P_BenSubBankCountryID"))
            ClsPay.BeneficiarySubBankCountry = IIf(IsDBNull(reader.Item("P_BenSubBankCountry")), "", reader.Item("P_BenSubBankCountry"))
            ClsPay.BeneficiarySubBankSwift = IIf(IsDBNull(reader.Item("P_BenSubBankSwift")), "", reader.Item("P_BenSubBankSwift"))
            ClsPay.BeneficiarySubBankIBAN = IIf(IsDBNull(reader.Item("P_BenSubBankIBAN")), "", reader.Item("P_BenSubBankIBAN"))
            ClsPay.BeneficiarySubBankOther = IIf(IsDBNull(reader.Item("P_BenSubBankOther")), "", reader.Item("P_BenSubBankOther"))
            ClsPay.BeneficiaryBankAddressID = IIf(IsDBNull(reader.Item("P_BenBankAddressID")), 0, reader.Item("P_BenBankAddressID"))
            ClsPay.BeneficiaryBankName = IIf(IsDBNull(reader.Item("P_BenBankName")), "", reader.Item("P_BenBankName"))
            ClsPay.BeneficiaryBankAddress1 = IIf(IsDBNull(reader.Item("P_BenBankAddress1")), "", reader.Item("P_BenBankAddress1"))
            ClsPay.BeneficiaryBankAddress2 = IIf(IsDBNull(reader.Item("P_BenBankAddress2")), "", reader.Item("P_BenBankAddress2"))
            ClsPay.BeneficiaryBankZip = IIf(IsDBNull(reader.Item("P_BenBankZip")), "", reader.Item("P_BenBankZip"))
            ClsPay.BeneficiaryBankCountryID = IIf(IsDBNull(reader.Item("P_BenBankCountryID")), 0, reader.Item("P_BenBankCountryID"))
            ClsPay.BeneficiaryBankCountry = IIf(IsDBNull(reader.Item("P_BenBankCountry")), "", reader.Item("P_BenBankCountry"))
            ClsPay.BeneficiaryBankSwift = IIf(IsDBNull(reader.Item("P_BenBankSwift")), "", reader.Item("P_BenBankSwift"))
            ClsPay.BeneficiaryBankIBAN = IIf(IsDBNull(reader.Item("P_BenBankIBAN")), "", reader.Item("P_BenBankIBAN"))
            ClsPay.BeneficiaryBankOther = IIf(IsDBNull(reader.Item("P_BenBankOther")), "", reader.Item("P_BenBankOther"))
            ClsPay.Comments = IIf(IsDBNull(reader.Item("P_Comments")), "", reader.Item("P_Comments"))
            ClsPay.SendSwift = IIf(IsDBNull(reader.Item("P_SendSwift")), True, reader.Item("P_SendSwift"))
            ClsPay.SendIMS = IIf(IsDBNull(reader.Item("P_SendIMS")), True, reader.Item("P_SendIMS"))
            ClsPay.SwiftUrgent = IIf(IsDBNull(reader.Item("P_SwiftUrgent")), False, reader.Item("P_SwiftUrgent"))
            ClsPay.SwiftSendRef = IIf(IsDBNull(reader.Item("P_SwiftSendRef")), False, reader.Item("P_SwiftSendRef"))
            ClsPay.OtherID = IIf(IsDBNull(reader.Item("P_Other_ID")), 0, reader.Item("P_Other_ID"))
            ClsPay.PortfolioFeeID = IIf(IsDBNull(reader.Item("P_PortfolioFeeID")), 0, reader.Item("P_PortfolioFeeID"))
            ClsPay.PortfolioIsFee = IIf(IsDBNull(reader.Item("P_PortfolioIsFee")), False, reader.Item("P_PortfolioIsFee"))
            ClsPay.PortfolioIsCommission = IIf(IsDBNull(reader.Item("P_PortfolioIsCommission")), False, reader.Item("P_PortfolioIsCommission"))
            ClsPay.OrderPortfolioShortcut = IIf(IsDBNull(reader.Item("P_OrderPortfolioShortcut")), "", reader.Item("P_OrderPortfolioShortcut"))
            ClsPay.OrderBrokerShortcut = IIf(IsDBNull(reader.Item("P_OrderBrokerShortcut")), "", reader.Item("P_OrderBrokerShortcut"))
            ClsPay.OrderAccountShortcut = IIf(IsDBNull(reader.Item("P_OrderAccountShortcut")), "", reader.Item("P_OrderAccountShortcut"))
            ClsPay.BeneficiaryPortfolioShortcut = IIf(IsDBNull(reader.Item("P_BenPortfolioShortcut")), "", reader.Item("P_BenPortfolioShortcut"))
            ClsPay.BeneficiaryBrokerShortcut = IIf(IsDBNull(reader.Item("P_BenBrokerShortcut")), "", reader.Item("P_BenBrokerShortcut"))
            ClsPay.BeneficiaryAccountShortcut = IIf(IsDBNull(reader.Item("P_BenAccountShortcut")), "", reader.Item("P_BenAccountShortcut"))
            ClsPay.IMSPayInCode = IIf(IsDBNull(reader.Item("PT_IMSPayINCode")), "", reader.Item("PT_IMSPayINCode"))
            ClsPay.IMSPayOutCode = IIf(IsDBNull(reader.Item("PT_IMSPayOutCode")), "", reader.Item("PT_IMSPayOutCode"))
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in GetReaderItemIntoPaymentClass")
        End Try
    End Sub

    Public Sub GetReaderItemIntoAuthPaymentClass(ByRef varID As String)
        Dim Reader As SqlDataReader = GetSQLData("select * from vwDolfinPaymentGridViewIncRemoved where p_ID = " & varID)
        Try
            If Reader.HasRows Then
                Reader.Read()
                ClsPayAuth.SelectedPaymentID = IIf(IsDBNull(Reader.Item("P_ID")), 0, Reader.Item("P_ID"))
                ClsPayAuth.Status = IIf(IsDBNull(Reader.Item("P_Status")), 0, Reader.Item("P_Status"))
                ClsPayAuth.PaymentType = IIf(IsDBNull(Reader.Item("P_PaymentType")), "", Reader.Item("P_PaymentType"))
                ClsPayAuth.OrderMessageTypeID = IIf(IsDBNull(Reader.Item("P_MessageTypeId")), "", Reader.Item("P_MessageTypeId"))
                ClsPayAuth.MessageType = IIf(IsDBNull(Reader.Item("P_MessageType")), "", Reader.Item("P_MessageType"))
                ClsPayAuth.PortfolioCode = IIf(IsDBNull(Reader.Item("P_PortfolioCode")), 0, Reader.Item("P_PortfolioCode"))
                ClsPayAuth.Portfolio = IIf(IsDBNull(Reader.Item("P_Portfolio")), "", Reader.Item("P_Portfolio"))
                ClsPayAuth.TransDate = IIf(IsDBNull(Reader.Item("P_TransDate")), Now, Reader.Item("P_transDate"))
                ClsPayAuth.SettleDate = IIf(IsDBNull(Reader.Item("P_SettleDate")), Now, Reader.Item("P_SettleDate"))
                ClsPayAuth.Amount = IIf(IsDBNull(Reader.Item("P_Amount")), 0, Reader.Item("P_Amount"))
                ClsPayAuth.CCYCode = IIf(IsDBNull(Reader.Item("P_CCYCode")), 0, Reader.Item("P_CCYCode"))
                ClsPayAuth.CCY = IIf(IsDBNull(Reader.Item("P_CCY")), "", Reader.Item("P_CCY"))
                ClsPayAuth.CustomerId = IIf(IsDBNull(Reader.Item("P_CustomerId")), 0, Reader.Item("P_CustomerId"))
                ClsPayAuth.CustomerName = IIf(IsDBNull(Reader.Item("P_CustomerName")), 0, Reader.Item("P_CustomerName"))
                ClsPayAuth.OrderAccountCode = IIf(IsDBNull(Reader.Item("P_OrderAccountCode")), 0, Reader.Item("P_OrderAccountCode"))
                ClsPayAuth.OrderAccount = IIf(IsDBNull(Reader.Item("P_OrderAccount")), "", Reader.Item("P_OrderAccount"))
                ClsPayAuth.OrderName = IIf(IsDBNull(Reader.Item("P_OrderName")), "", Reader.Item("P_OrderName"))
                ClsPayAuth.OrderRef = IIf(IsDBNull(Reader.Item("P_OrderRef")), "", Reader.Item("P_OrderRef"))
                ClsPayAuth.OrderAddress1 = IIf(IsDBNull(Reader.Item("P_OrderAddress1")), "", Reader.Item("P_OrderAddress1"))
                ClsPayAuth.OrderAddress2 = IIf(IsDBNull(Reader.Item("P_OrderAddress2")), "", Reader.Item("P_OrderAddress2"))
                ClsPayAuth.OrderZip = IIf(IsDBNull(Reader.Item("P_OrderZip")), "", Reader.Item("P_OrderZip"))
                ClsPayAuth.OrderCountryID = IIf(IsDBNull(Reader.Item("P_OrderCountryID")), 0, Reader.Item("P_OrderCountryID"))
                ClsPayAuth.OrderCountry = IIf(IsDBNull(Reader.Item("P_OrderCountry")), "", Reader.Item("P_OrderCountry"))
                ClsPayAuth.OrderSwift = IIf(IsDBNull(Reader.Item("P_OrderSwift")), "", Reader.Item("P_OrderSwift"))
                ClsPayAuth.OrderAccountNumber = IIf(IsDBNull(Reader.Item("P_OrderAccountNumber")), "", Reader.Item("P_OrderAccountNumber"))
                ClsPayAuth.OrderIBAN = IIf(IsDBNull(Reader.Item("P_OrderIBAN")), "", Reader.Item("P_OrderIBAN"))
                ClsPayAuth.OrderOther = IIf(IsDBNull(Reader.Item("P_OrderOther")), "", Reader.Item("P_OrderOther"))
                ClsPayAuth.OrderBrokerCode = IIf(IsDBNull(Reader.Item("P_OrderBrokerCode")), 0, Reader.Item("P_OrderBrokerCode"))
                ClsPayAuth.OrderVOCodeID = IIf(IsDBNull(Reader.Item("P_OrderVO_ID")), 0, Reader.Item("P_OrderVO_ID"))
                ClsPayAuth.BeneficiaryPortfolioCode = IIf(IsDBNull(Reader.Item("P_BenPortfolioCode")), 0, Reader.Item("P_BenPortfolioCode"))
                ClsPayAuth.BeneficiaryPortfolio = IIf(IsDBNull(Reader.Item("P_BenPortfolio")), 0, Reader.Item("P_BenPortfolio"))
                ClsPayAuth.BeneficiaryAccountCode = IIf(IsDBNull(Reader.Item("P_BenAccountCode")), 0, Reader.Item("P_BenAccountCode"))
                ClsPayAuth.BeneficiaryAccount = IIf(IsDBNull(Reader.Item("P_BenAccount")), "", Reader.Item("P_BenAccount"))
                ClsPayAuth.BeneficiaryAddressID = IIf(IsDBNull(Reader.Item("P_BenAddressID")), 0, Reader.Item("P_BenAddressID"))
                ClsPayAuth.BeneficiaryName = IIf(IsDBNull(Reader.Item("P_BenName")), "", Reader.Item("P_BenName"))
                ClsPayAuth.BeneficiaryFirstName = IIf(IsDBNull(Reader.Item("P_BenFirstName")), "", Reader.Item("P_BenFirstName"))
                ClsPayAuth.BeneficiaryMiddleName = IIf(IsDBNull(Reader.Item("P_BenMiddleName")), "", Reader.Item("P_BenMiddleName"))
                ClsPayAuth.BeneficiarySurname = IIf(IsDBNull(Reader.Item("P_BenSurname")), "", Reader.Item("P_BenSurname"))
                ClsPayAuth.BeneficiaryRef = IIf(IsDBNull(Reader.Item("P_BenRef")), "", Reader.Item("P_BenRef"))
                ClsPayAuth.BeneficiaryAddress1 = IIf(IsDBNull(Reader.Item("P_BenAddress1")), "", Reader.Item("P_BenAddress1"))
                ClsPayAuth.BeneficiaryAddress2 = IIf(IsDBNull(Reader.Item("P_BenAddress2")), "", Reader.Item("P_BenAddress2"))
                ClsPayAuth.BeneficiaryCounty = IIf(IsDBNull(Reader.Item("P_BenCounty")), "", Reader.Item("P_BenCounty"))
                ClsPayAuth.BeneficiaryZip = IIf(IsDBNull(Reader.Item("P_BenZip")), "", Reader.Item("P_BenZip"))
                ClsPayAuth.BeneficiaryCountryID = IIf(IsDBNull(Reader.Item("P_BenCountryID")), 0, Reader.Item("P_BenCountryID"))
                ClsPayAuth.BeneficiaryCountry = IIf(IsDBNull(Reader.Item("P_BenCountry")), "", Reader.Item("P_BenCountry"))
                ClsPayAuth.BeneficiarySwift = IIf(IsDBNull(Reader.Item("P_BenSwift")), "", Reader.Item("P_BenSwift"))
                ClsPayAuth.BeneficiaryAccountNumber = IIf(IsDBNull(Reader.Item("P_BenAccountNumber")), "", Reader.Item("P_BenAccountNumber"))
                ClsPayAuth.BeneficiaryIBAN = IIf(IsDBNull(Reader.Item("P_BenIBAN")), "", Reader.Item("P_BenIBAN"))
                ClsPayAuth.BeneficiaryOther = IIf(IsDBNull(Reader.Item("P_BenOther")), "", Reader.Item("P_BenOther"))
                ClsPayAuth.BeneficiaryBIK = IIf(IsDBNull(Reader.Item("P_BenBIK")), "", Reader.Item("P_BenBIK"))
                ClsPayAuth.BeneficiaryINN = IIf(IsDBNull(Reader.Item("P_BenINN")), "", Reader.Item("P_BenINN"))
                ClsPayAuth.BeneficiaryCCYCode = IIf(IsDBNull(Reader.Item("P_BenCCYCode")), 0, Reader.Item("P_BenCCYCode"))
                ClsPayAuth.BeneficiaryCCY = IIf(IsDBNull(Reader.Item("P_BenCCY")), "", Reader.Item("P_BenCCY"))
                ClsPayAuth.BeneficiaryBrokerCode = IIf(IsDBNull(Reader.Item("P_BenBrokerCode")), 0, Reader.Item("P_BenBrokerCode"))
                ClsPayAuth.BeneficiaryRelationshipID = IIf(IsDBNull(Reader.Item("P_BenRelationshipID")), 0, Reader.Item("P_BenRelationshipID"))
                ClsPayAuth.BeneficiaryRelationship = IIf(IsDBNull(Reader.Item("P_BenRelationship")), "", Reader.Item("P_BenRelationship"))
                ClsPayAuth.BeneficiaryTypeID = IIf(IsDBNull(Reader.Item("P_BenTypeID")), 0, Reader.Item("P_BenTypeID"))
                ClsPayAuth.BeneficiaryType = IIf(IsDBNull(Reader.Item("P_BenType")), "", Reader.Item("P_BenType"))
                ClsPayAuth.BeneficiaryTemplateName = IIf(IsDBNull(Reader.Item("P_BenTemplateName")), 0, Reader.Item("P_BenTemplateName"))
                ClsPayAuth.ExchangeRate = IIf(IsDBNull(Reader.Item("P_ExchangeRate")), 1, Reader.Item("P_ExchangeRate"))
                ClsPayAuth.BeneficiarySubBankAddressID = IIf(IsDBNull(Reader.Item("P_BenSubBankAddressID")), 0, Reader.Item("P_BenSubBankAddressID"))
                ClsPayAuth.BeneficiarySubBankName = IIf(IsDBNull(Reader.Item("P_BenSubBankName")), "", Reader.Item("P_BenSubBankName"))
                ClsPayAuth.BeneficiarySubBankAddress1 = IIf(IsDBNull(Reader.Item("P_BenSubBankAddress1")), "", Reader.Item("P_BenSubBankAddress1"))
                ClsPayAuth.BeneficiarySubBankAddress2 = IIf(IsDBNull(Reader.Item("P_BenSubBankAddress2")), "", Reader.Item("P_BenSubBankAddress2"))
                ClsPayAuth.BeneficiarySubBankZip = IIf(IsDBNull(Reader.Item("P_BenSubBankZip")), "", Reader.Item("P_BenSubBankZip"))
                ClsPayAuth.BeneficiarySubBankCountryID = IIf(IsDBNull(Reader.Item("P_BenSubBankCountryID")), 0, Reader.Item("P_BenSubBankCountryID"))
                ClsPayAuth.BeneficiarySubBankCountry = IIf(IsDBNull(Reader.Item("P_BenSubBankCountry")), "", Reader.Item("P_BenSubBankCountry"))
                ClsPayAuth.BeneficiarySubBankSwift = IIf(IsDBNull(Reader.Item("P_BenSubBankSwift")), "", Reader.Item("P_BenSubBankSwift"))
                ClsPayAuth.BeneficiarySubBankIBAN = IIf(IsDBNull(Reader.Item("P_BenSubBankIBAN")), "", Reader.Item("P_BenSubBankIBAN"))
                ClsPayAuth.BeneficiarySubBankOther = IIf(IsDBNull(Reader.Item("P_BenSubBankOther")), "", Reader.Item("P_BenSubBankOther"))
                ClsPayAuth.BeneficiarySubBankBIK = IIf(IsDBNull(Reader.Item("P_BenSubBankBIK")), "", Reader.Item("P_BenSubBankBIK"))
                ClsPayAuth.BeneficiarySubBankINN = IIf(IsDBNull(Reader.Item("P_BenSubBankINN")), "", Reader.Item("P_BenSubBankINN"))
                ClsPayAuth.BeneficiaryBankAddressID = IIf(IsDBNull(Reader.Item("P_BenBankAddressID")), 0, Reader.Item("P_BenBankAddressID"))
                ClsPayAuth.BeneficiaryBankName = IIf(IsDBNull(Reader.Item("P_BenBankName")), "", Reader.Item("P_BenBankName"))
                ClsPayAuth.BeneficiaryBankAddress1 = IIf(IsDBNull(Reader.Item("P_BenBankAddress1")), "", Reader.Item("P_BenBankAddress1"))
                ClsPayAuth.BeneficiaryBankAddress2 = IIf(IsDBNull(Reader.Item("P_BenBankAddress2")), "", Reader.Item("P_BenBankAddress2"))
                ClsPayAuth.BeneficiaryBankZip = IIf(IsDBNull(Reader.Item("P_BenBankZip")), "", Reader.Item("P_BenBankZip"))
                ClsPayAuth.BeneficiaryBankCountryID = IIf(IsDBNull(Reader.Item("P_BenBankCountryID")), 0, Reader.Item("P_BenBankCountryID"))
                ClsPayAuth.BeneficiaryBankCountry = IIf(IsDBNull(Reader.Item("P_BenBankCountry")), "", Reader.Item("P_BenBankCountry"))
                ClsPayAuth.BeneficiaryBankSwift = IIf(IsDBNull(Reader.Item("P_BenBankSwift")), "", Reader.Item("P_BenBankSwift"))
                ClsPayAuth.BeneficiaryBankIBAN = IIf(IsDBNull(Reader.Item("P_BenBankIBAN")), "", Reader.Item("P_BenBankIBAN"))
                ClsPayAuth.BeneficiaryBankOther = IIf(IsDBNull(Reader.Item("P_BenBankOther")), "", Reader.Item("P_BenBankOther"))
                ClsPayAuth.BeneficiaryBankBIK = IIf(IsDBNull(Reader.Item("P_BenBankBik")), "", Reader.Item("P_BenBankBik"))
                ClsPayAuth.BeneficiaryBankINN = IIf(IsDBNull(Reader.Item("P_BenBankInn")), "", Reader.Item("P_BenBankInn"))
                ClsPayAuth.Comments = IIf(IsDBNull(Reader.Item("P_Comments")), "", Reader.Item("P_Comments"))
                ClsPayAuth.SendSwift = IIf(IsDBNull(Reader.Item("P_SendSwift")), True, Reader.Item("P_SendSwift"))
                ClsPayAuth.SendIMS = IIf(IsDBNull(Reader.Item("P_SendIMS")), True, Reader.Item("P_SendIMS"))
                ClsPayAuth.SwiftUrgent = IIf(IsDBNull(Reader.Item("P_SwiftUrgent")), False, Reader.Item("P_SwiftUrgent"))
                ClsPayAuth.SwiftSendRef = IIf(IsDBNull(Reader.Item("P_SwiftSendRef")), False, Reader.Item("P_SwiftSendRef"))
                ClsPayAuth.OtherID = IIf(IsDBNull(Reader.Item("P_Other_ID")), 0, Reader.Item("P_Other_ID"))
                ClsPayAuth.PortfolioFeeID = IIf(IsDBNull(Reader.Item("P_PortfolioFeeID")), 0, Reader.Item("P_PortfolioFeeID"))
                ClsPayAuth.PortfolioIsFee = IIf(IsDBNull(Reader.Item("P_PortfolioIsFee")), False, Reader.Item("P_PortfolioIsFee"))
                ClsPayAuth.PortfolioIsCommission = IIf(IsDBNull(Reader.Item("P_PortfolioIsCommission")), False, Reader.Item("P_PortfolioIsCommission"))
                ClsPayAuth.PaymentFilePath = IIf(IsDBNull(Reader.Item("P_PaymentFilePath")), "", Reader.Item("P_PaymentFilePath"))
                ClsPayAuth.PaymentFilePathRemote = IIf(IsDBNull(Reader.Item("P_PaymentFilePathRemote")), "", Reader.Item("P_PaymentFilePathRemote"))
                ClsPayAuth.SwiftReturned = IIf(IsDBNull(Reader.Item("P_SwiftReturned")), False, Reader.Item("P_SwiftReturned"))
                ClsPayAuth.SwiftAcknowledged = IIf(IsDBNull(Reader.Item("P_SwiftAcknowledged")), False, Reader.Item("P_SwiftAcknowledged"))
                ClsPayAuth.SwiftStatus = IIf(IsDBNull(Reader.Item("P_SwiftStatus")), "", Reader.Item("P_SwiftStatus"))
                ClsPayAuth.IMSReturned = IIf(IsDBNull(Reader.Item("P_IMSReturned")), False, Reader.Item("P_IMSReturned"))
                ClsPayAuth.IMSAcknowledged = IIf(IsDBNull(Reader.Item("P_IMSAcknowledged")), False, Reader.Item("P_IMSAcknowledged"))
                ClsPayAuth.IMSStatus = IIf(IsDBNull(Reader.Item("P_IMSStatus")), "", Reader.Item("P_IMSStatus"))
                ClsPayAuth.IMSPayInCode = IIf(IsDBNull(Reader.Item("PT_IMSPayINCode")), "", Reader.Item("PT_IMSPayINCode"))
                ClsPayAuth.IMSPayOutCode = IIf(IsDBNull(Reader.Item("PT_IMSPayOutCode")), "", Reader.Item("PT_IMSPayOutCode"))
                ClsPay.IMSPayInCode = IIf(IsDBNull(Reader.Item("PT_IMSPayINCode")), "", Reader.Item("PT_IMSPayINCode"))
                ClsPay.IMSPayOutCode = IIf(IsDBNull(Reader.Item("PT_IMSPayOutCode")), "", Reader.Item("PT_IMSPayOutCode"))
                ClsPayAuth.OrderPortfolioShortcut = IIf(IsDBNull(Reader.Item("P_OrderPortfolioShortcut")), "", Reader.Item("P_OrderPortfolioShortcut"))
                ClsPayAuth.OrderBrokerShortcut = IIf(IsDBNull(Reader.Item("P_OrderBrokerShortcut")), "", Reader.Item("P_OrderBrokerShortcut"))
                ClsPayAuth.OrderAccountShortcut = IIf(IsDBNull(Reader.Item("P_OrderAccountShortcut")), "", Reader.Item("P_OrderAccountShortcut"))
                ClsPayAuth.BeneficiaryPortfolioShortcut = IIf(IsDBNull(Reader.Item("P_BenPortfolioShortcut")), "", Reader.Item("P_BenPortfolioShortcut"))
                ClsPayAuth.BeneficiaryBrokerShortcut = IIf(IsDBNull(Reader.Item("P_BenBrokerShortcut")), "", Reader.Item("P_BenBrokerShortcut"))
                ClsPayAuth.BeneficiaryAccountShortcut = IIf(IsDBNull(Reader.Item("P_BenAccountShortcut")), "", Reader.Item("P_BenAccountShortcut"))
                ClsPayAuth.UserNameEntered = IIf(IsDBNull(Reader.Item("P_Username")), "", Reader.Item("P_Username"))
                ClsPayAuth.AboveThreshold = IIf(IsDBNull(Reader.Item("P_AboveThreshold")), False, Reader.Item("P_AboveThreshold"))
                ClsPayAuth.IMSRef = IIf(IsDBNull(Reader.Item("P_IMSRef")), "", Reader.Item("P_IMSRef"))
                ClsPayAuth.PaymentRequested = IIf(IsDBNull(Reader.Item("PaymentRequested")), "", Reader.Item("PaymentRequested"))
                ClsPayAuth.PaymentRequestedEmail = IIf(IsDBNull(Reader.Item("PR_Email")), "", Reader.Item("PR_Email"))
                ClsPayAuth.PaymentRequestedType = IIf(IsDBNull(Reader.Item("PR_Type")), 0, Reader.Item("PR_Type"))
                ClsPayAuth.PaymentRequestedRate = IIf(IsDBNull(Reader.Item("PR_Rate")), 0, Reader.Item("PR_Rate"))
                ClsPayAuth.PaymentRequestedBeneficiaryName = IIf(IsDBNull(Reader.Item("PR_BeneficiaryName")), "", Reader.Item("PR_BeneficiaryName"))
                ClsPayAuth.PaymentRequestedCheckedSantions = IIf(IsDBNull(Reader.Item("PR_CheckedSantions")), False, Reader.Item("PR_CheckedSantions"))
                ClsPayAuth.PaymentRequestedCheckedPEP = IIf(IsDBNull(Reader.Item("PR_CheckedPEP")), False, Reader.Item("PR_CheckedPEP"))
                ClsPayAuth.PaymentRequestedCheckedCDD = IIf(IsDBNull(Reader.Item("PR_CheckedCDD")), False, Reader.Item("PR_CheckedCDD"))
                ClsPayAuth.PaymentRequestedIssuesDisclosed = IIf(IsDBNull(Reader.Item("PR_IssuesDisclosed")), "", Reader.Item("PR_IssuesDisclosed"))
                ClsPayAuth.PaymentRequestedCheckedBankName = IIf(IsDBNull(Reader.Item("PR_CheckedBankName")), "", Reader.Item("PR_CheckedBankName"))
                ClsPayAuth.PaymentRequestedMMBuySell = IIf(IsDBNull(Reader.Item("PR_MM_BS")), 0, Reader.Item("PR_MM_BS"))
                ClsPayAuth.PaymentRequestedMMInstrumentCode = IIf(IsDBNull(Reader.Item("PR_MM_InstrCode")), 0, Reader.Item("PR_MM_InstrCode"))
                ClsPayAuth.PaymentRequestedMMDurationType = IIf(IsDBNull(Reader.Item("PR_MM_DurationType")), 0, Reader.Item("PR_MM_DurationType"))
                ClsPayAuth.PaymentRequestedMMStartDate = IIf(IsDBNull(Reader.Item("PR_MM_StartDate")), Now, Reader.Item("PR_MM_StartDate"))
                ClsPayAuth.PaymentRequestedMMEndDate = IIf(IsDBNull(Reader.Item("PR_MM_EndDate")), Now, Reader.Item("PR_MM_EndDate"))
                ClsPayAuth.PaymentRequestedMMFirmMove = IIf(IsDBNull(Reader.Item("PR_MM_Firmmove")), 0, Reader.Item("PR_MM_Firmmove"))
                ClsPayAuth.PaymentIDOther = IIf(IsDBNull(Reader.Item("P_PaymentIDOther")), 0, Reader.Item("P_PaymentIDOther"))
                ClsPayAuth.CanSendEmail = IIf(IsDBNull(Reader.Item("CanSendEmail")), 0, Reader.Item("CanSendEmail"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in GetReaderItemIntoAuthPaymentClass")
        Finally
            Reader.Close()
        End Try
    End Sub

    Public Sub GetReaderItemIntoRDClass(ByRef varID As String)
        Dim ReaderRD As SqlDataReader = GetSQLData("select * from vwDolfinPaymentReceiveDeliverGridView where PRD_ID = " & varID)
        Try
            If ReaderRD.HasRows Then
                ReaderRD.Read()
                ClsRD.SelectedTransactionID = IIf(IsDBNull(ReaderRD.Item("PRD_ID")), 0, ReaderRD.Item("PRD_ID"))
                ClsRD.StatusID = IIf(IsDBNull(ReaderRD.Item("PRD_StatusID")), "", ReaderRD.Item("PRD_StatusID"))
                ClsRD.OrderRef = IIf(IsDBNull(ReaderRD.Item("PRD_OrderRef")), "", ReaderRD.Item("PRD_OrderRef"))
                ClsRD.RecType = IIf(IsDBNull(ReaderRD.Item("PRD_RecType")), 0, ReaderRD.Item("PRD_RecType"))
                ClsRD.MessageTypeID = IIf(IsDBNull(ReaderRD.Item("PRD_MessageTypeID")), 0, ReaderRD.Item("PRD_MessageTypeID"))
                ClsRD.PortfolioCode = IIf(IsDBNull(ReaderRD.Item("PRD_PortfolioCode")), 0, ReaderRD.Item("PRD_PortfolioCode"))
                ClsRD.PortfolioName = IIf(IsDBNull(ReaderRD.Item("Portfolio")), "", ReaderRD.Item("Portfolio"))
                ClsRD.PortfolioShortcut = IIf(IsDBNull(ReaderRD.Item("PortfolioShortcut")), "", ReaderRD.Item("PortfolioShortcut"))
                ClsRD.CCYCode = IIf(IsDBNull(ReaderRD.Item("PRD_CCYCode")), 0, ReaderRD.Item("PRD_CCYCode"))
                ClsRD.CCYName = IIf(IsDBNull(ReaderRD.Item("CCYName")), "", ReaderRD.Item("CCYName"))
                ClsRD.BrokerCode = IIf(IsDBNull(ReaderRD.Item("PRD_BrokerCode")), 0, ReaderRD.Item("PRD_BrokerCode"))
                ClsRD.BrokerName = IIf(IsDBNull(ReaderRD.Item("BrokerName")), "", ReaderRD.Item("BrokerName"))
                ClsRD.BrokerShortcut = IIf(IsDBNull(ReaderRD.Item("BrokerShortcut")), "", ReaderRD.Item("BrokerShortcut"))
                ClsRD.CashAccountCode = IIf(IsDBNull(ReaderRD.Item("PRD_CashAccountCode")), 0, ReaderRD.Item("PRD_CashAccountCode"))
                ClsRD.CashAccountBrokerName = IIf(IsDBNull(ReaderRD.Item("CashAccountBrokerName")), "", ReaderRD.Item("CashAccountBrokerName"))
                ClsRD.CashAccountBrokerShortcut = IIf(IsDBNull(ReaderRD.Item("CashAccountBrokerShortcut")), "", ReaderRD.Item("CashAccountBrokerShortcut"))
                ClsRD.CashAccountName = IIf(IsDBNull(ReaderRD.Item("CashAccountName")), "", ReaderRD.Item("CashAccountName"))
                ClsRD.CashAccountShortcut = IIf(IsDBNull(ReaderRD.Item("CashAccountShortcut")), "", ReaderRD.Item("CashAccountShortcut"))
                ClsRD.TransDate = IIf(IsDBNull(ReaderRD.Item("PRD_TransDate")), Now, ReaderRD.Item("PRD_TransDate"))
                ClsRD.SettleDate = IIf(IsDBNull(ReaderRD.Item("PRD_SettleDate")), Now, ReaderRD.Item("PRD_SettleDate"))
                ClsRD.Amount = IIf(IsDBNull(ReaderRD.Item("PRD_Amount")), 0, ReaderRD.Item("PRD_Amount"))
                ClsRD.ClearerCode = IIf(IsDBNull(ReaderRD.Item("PRD_ClearerCode")), 0, ReaderRD.Item("PRD_ClearerCode"))
                ClsRD.ClearerName = IIf(IsDBNull(ReaderRD.Item("ClearerName")), "", ReaderRD.Item("ClearerName"))
                ClsRD.InstrCode = IIf(IsDBNull(ReaderRD.Item("PRD_InstCode")), 0, ReaderRD.Item("PRD_InstCode"))
                ClsRD.InstrName = IIf(IsDBNull(ReaderRD.Item("InstName")), "", ReaderRD.Item("InstName"))
                ClsRD.InstrISIN = IIf(IsDBNull(ReaderRD.Item("InstISIN")), "", ReaderRD.Item("InstISIN"))
                ClsRD.InstrShortcut = IIf(IsDBNull(ReaderRD.Item("InstShortcut")), "", ReaderRD.Item("InstShortcut"))
                ClsRD.Agent = IIf(IsDBNull(ReaderRD.Item("PRD_Agent")), 0, ReaderRD.Item("PRD_Agent"))
                ClsRD.AgentName = IIf(IsDBNull(ReaderRD.Item("PRD_Agent")), "", ReaderRD.Item("PRD_Agent"))
                ClsRD.AgentAccountNo = IIf(IsDBNull(ReaderRD.Item("PRD_AgentAccountNo")), 0, ReaderRD.Item("PRD_AgentAccountNo"))
                ClsRD.PlaceofSettlement = IIf(IsDBNull(ReaderRD.Item("PRD_PlaceofSettlement")), 0, ReaderRD.Item("PRD_PlaceofSettlement"))
                ClsRD.SendIMS = IIf(IsDBNull(ReaderRD.Item("PRD_SendIMS")), True, ReaderRD.Item("PRD_SendIMS"))
                ClsRD.SendMiniOMS = IIf(IsDBNull(ReaderRD.Item("PRD_SendMiniOMS")), False, ReaderRD.Item("PRD_SendMiniOMS"))
                ClsRD.Email = IIf(IsDBNull(ReaderRD.Item("PRD_Email")), "", ReaderRD.Item("PRD_Email"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in GetReaderItemIntoRDClass")
        Finally
            ReaderRD.Close()
        End Try
    End Sub

    Public Sub GetReaderItemIntoFSFeeClass(ByRef varID As String)
        Dim ReaderFS As SqlDataReader = GetSQLData("select * from vwDolfinPaymentFeeSchedulesLinks where FS_ID = " & varID)
        Try
            If ReaderFS.HasRows Then
                ReaderFS.Read()
                ClsFS.ID = IIf(IsDBNull(ReaderFS.Item("FS_ID")), 0, ReaderFS.Item("FS_ID"))
                ClsFS.PortfolioCode = IIf(IsDBNull(ReaderFS.Item("PF_Code")), 0, ReaderFS.Item("PF_Code"))
                ClsFS.PortfolioName = IIf(IsDBNull(ReaderFS.Item("Portfolio")), "", ReaderFS.Item("Portfolio"))
                ClsFS.PortfolioShortcut = IIf(IsDBNull(ReaderFS.Item("PF_Shortcut1")), "", ReaderFS.Item("PF_Shortcut1"))
                ClsFS.CCYCode = IIf(IsDBNull(ReaderFS.Item("CR_Code")), 0, ReaderFS.Item("CR_Code"))
                ClsFS.CCYName = IIf(IsDBNull(ReaderFS.Item("CR_Name1")), "", ReaderFS.Item("CR_Name1"))
                ClsFS.StartDate = IIf(IsDBNull(ReaderFS.Item("FS_StartDate")), Now, ReaderFS.Item("FS_StartDate"))
                ClsFS.EndDate = IIf(IsDBNull(ReaderFS.Item("FS_EndDate")), Now, ReaderFS.Item("FS_EndDate"))
                ClsFS.DestinationID = IIf(IsDBNull(ReaderFS.Item("FS_DestinationID")), 0, ReaderFS.Item("FS_DestinationID"))
                ClsFS.Destination = IIf(IsDBNull(ReaderFS.Item("FS_Destination")), "", ReaderFS.Item("FS_Destination"))
                ClsFS.PayFreqID = IIf(IsDBNull(ReaderFS.Item("FS_PayFreqID")), 0, ReaderFS.Item("FS_PayFreqID"))
                ClsFS.PayFreq = IIf(IsDBNull(ReaderFS.Item("FS_PayFreq")), "", ReaderFS.Item("FS_PayFreq"))
                ClsFS.MGTID = IIf(IsDBNull(ReaderFS.Item("MGTID")), 0, ReaderFS.Item("MGTID"))
                ClsFS.CFID = IIf(IsDBNull(ReaderFS.Item("CFID")), 0, ReaderFS.Item("CFID"))
                ClsFS.TFID = IIf(IsDBNull(ReaderFS.Item("TFID")), 0, ReaderFS.Item("TFID"))
                ClsFS.TSID = IIf(IsDBNull(ReaderFS.Item("TSID")), 0, ReaderFS.Item("TSID"))
                ClsFS.FilePath = IIf(IsDBNull(ReaderFS.Item("FS_FilePath")), 0, ReaderFS.Item("FS_FilePath"))
                ClsFS.ClientName = LTrim(IIf(IsDBNull(ReaderFS.Item("ClientName")), "", ReaderFS.Item("ClientName")))
                ClsFS.RM1 = IIf(IsDBNull(ReaderFS.Item("RM1")), "", ReaderFS.Item("RM1"))
                ClsFS.RM2 = IIf(IsDBNull(ReaderFS.Item("RM2")), "", ReaderFS.Item("RM2"))
                ClsFS.IsTemplate = IIf(IsDBNull(ReaderFS.Item("FS_Template")), False, ReaderFS.Item("FS_Template"))
                ClsFS.TemplateName = IIf(IsDBNull(ReaderFS.Item("FS_TemplateName")), "", ReaderFS.Item("FS_TemplateName"))
                ClsFS.TemplateLinkedID = IIf(IsDBNull(ReaderFS.Item("TemplateLinkedID")), 0, ReaderFS.Item("TemplateLinkedID"))
                ClsFS.TemplateLinkedName = IIf(IsDBNull(ReaderFS.Item("TemplateLinkedName")), "", ReaderFS.Item("TemplateLinkedName"))
                ClsFS.IsEAM = IIf(IsDBNull(ReaderFS.Item("FS_IsEAM")), False, ReaderFS.Item("FS_IsEAM"))
                ClsFS.PortfolioCodeRedirect = IIf(IsDBNull(ReaderFS.Item("PortfolioRedirectCode")), 0, ReaderFS.Item("PortfolioRedirectCode"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoFSClass")
        Finally
            If ReaderFS.HasRows Then
                ReaderFS.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoMgtFeeClass(ByRef varID As String)
        Dim ReaderRD As SqlDataReader = GetSQLData("select top 1 * from vwDolfinPaymentManagementFeeLinks where MGT_ID = " & varID)
        Try
            If ReaderRD.HasRows Then
                ReaderRD.Read()
                ClsMgtFees.ID = IIf(IsDBNull(ReaderRD.Item("MGT_ID")), 0, ReaderRD.Item("MGT_ID"))
                ClsMgtFees.ChargeableCCYCode = IIf(IsDBNull(ReaderRD.Item("MGT_ChargeableCCYCode")), 0, ReaderRD.Item("MGT_ChargeableCCYCode"))
                ClsMgtFees.TypeID = IIf(IsDBNull(ReaderRD.Item("MGT_TypeID")), 0, ReaderRD.Item("MGT_TypeID"))
                ClsMgtFees.CCYEqvCode = IIf(IsDBNull(ReaderRD.Item("MGT_CCYEqvCode")), 0, ReaderRD.Item("MGT_CCYEqvCode"))
                ClsMgtFees.MinCharge = IIf(IsDBNull(ReaderRD.Item("MGT_MinCharge")), 0, ReaderRD.Item("MGT_MinCharge"))
                ClsMgtFees.MaxCharge = IIf(IsDBNull(ReaderRD.Item("MGT_MaxCharge")), cEndAmt, ReaderRD.Item("MGT_MaxCharge"))
                ClsMgtFees.MinMaintCharge = IIf(IsDBNull(ReaderRD.Item("MGT_MinMaintCharge")), cEndAmt, ReaderRD.Item("MGT_MinMaintCharge"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoMgtFeeClass")
        Finally
            If ReaderRD.HasRows Then
                ReaderRD.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoCFClass(ByRef varID As String)
        Dim ReaderCF As SqlDataReader = GetSQLData("select top 1 * from vwDolfinPaymentCustodyFeeLinks where CF_ID = " & varID)
        Try
            If ReaderCF.HasRows Then
                ReaderCF.Read()
                ClsCF.ID = IIf(IsDBNull(ReaderCF.Item("CF_ID")), 0, ReaderCF.Item("CF_ID"))
                ClsCF.ChargeableCCYCode = IIf(IsDBNull(ReaderCF.Item("CF_ChargeableCCYCode")), 0, ReaderCF.Item("CF_ChargeableCCYCode"))
                ClsCF.ChargeableCCYName = IIf(IsDBNull(ReaderCF.Item("CF_ChargeableCCY")), "", ReaderCF.Item("CF_ChargeableCCY"))
                ClsCF.TypeID = IIf(IsDBNull(ReaderCF.Item("CF_TypeID")), 0, ReaderCF.Item("CF_TypeID"))
                ClsCF.CCYEqvCode = IIf(IsDBNull(ReaderCF.Item("CF_CCYEqvCode")), 0, ReaderCF.Item("CF_CCYEqvCode"))
                ClsCF.MinCharge = IIf(IsDBNull(ReaderCF.Item("CF_MinCharge")), 0, ReaderCF.Item("CF_MinCharge"))
                ClsCF.MaxCharge = IIf(IsDBNull(ReaderCF.Item("CF_MaxCharge")), cEndAmt, ReaderCF.Item("CF_MaxCharge"))
                ClsCF.MinMaintCharge = IIf(IsDBNull(ReaderCF.Item("CF_MinMaintCharge")), cEndAmt, ReaderCF.Item("CF_MinMaintCharge"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoCFClass")
        Finally
            If ReaderCF.HasRows Then
                ReaderCF.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoTFClass(ByRef varID As String)
        Dim ReaderTF As SqlDataReader = GetSQLData("select top 1 * from vwDolfinPaymentTransactionFeeLinks where TF_ID = " & varID)
        Try
            If ReaderTF.HasRows Then
                ReaderTF.Read()
                ClsTF.ID = IIf(IsDBNull(ReaderTF.Item("TF_ID")), 0, ReaderTF.Item("TF_ID"))
                ClsTF.TypeID = IIf(IsDBNull(ReaderTF.Item("TF_TypeID")), 0, ReaderTF.Item("TF_TypeID"))
                ClsTF.CCYEqvCode = IIf(IsDBNull(ReaderTF.Item("TF_CCYEqvCode")), 0, ReaderTF.Item("TF_CCYEqvCode"))
                ClsTF.TicketFee = IIf(IsDBNull(ReaderTF.Item("TF_TicketFee")), 0, ReaderTF.Item("TF_TicketFee"))
                ClsTF.TicketFeeCCY = IIf(IsDBNull(ReaderTF.Item("TF_TicketFeeCCYCode")), 0, ReaderTF.Item("TF_TicketFeeCCYCode"))
                ClsTF.TransFee = IIf(IsDBNull(ReaderTF.Item("TF_TransFee")), 0, ReaderTF.Item("TF_TransFee"))
                ClsTF.TransFeeCCY = IIf(IsDBNull(ReaderTF.Item("TF_TransFeeCCYCode")), 0, ReaderTF.Item("TF_TransFeeCCYCode"))
                ClsTF.FOPFee = IIf(IsDBNull(ReaderTF.Item("TF_FOPFee")), 0, ReaderTF.Item("TF_FOPFee"))
                ClsTF.FOPFeeCCY = IIf(IsDBNull(ReaderTF.Item("TF_FOPFeeCCYCode")), 0, ReaderTF.Item("TF_FOPFeeCCYCode"))
                ClsTF.CoverBrokerage = IIf(IsDBNull(ReaderTF.Item("TF_CoverBrokerage")), 0, ReaderTF.Item("TF_CoverBrokerage"))
                ClsTF.MinCharge = IIf(IsDBNull(ReaderTF.Item("TF_MinCharge")), 0, ReaderTF.Item("TF_MinCharge"))
                ClsTF.MaxCharge = IIf(IsDBNull(ReaderTF.Item("TF_MaxCharge")), cEndAmt, ReaderTF.Item("TF_MaxCharge"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoTFClass")
        Finally
            If ReaderTF.HasRows Then
                ReaderTF.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoPRClass(ByRef varID As String)
        Dim ReaderPR As SqlDataReader = GetSQLData("select * from vwDolfinPaymentGridViewLitePaymentRequestAll where ID = " & varID)
        Try
            If ReaderPR.HasRows Then
                ReaderPR.Read()
                ClsPR.ID = IIf(IsDBNull(ReaderPR.Item("ID")), 0, ReaderPR.Item("ID"))
                ClsPR.Status = IIf(IsDBNull(ReaderPR.Item("Status")), 0, ReaderPR.Item("Status"))
                ClsPR.PortfolioCode = IIf(IsDBNull(ReaderPR.Item("P_PortfolioCode")), 0, ReaderPR.Item("P_PortfolioCode"))
                ClsPR.PortfolioName = IIf(IsDBNull(ReaderPR.Item("Portfolio")), "", ReaderPR.Item("Portfolio"))
                ClsPR.CCYCode = IIf(IsDBNull(ReaderPR.Item("P_CCYCode")), 0, ReaderPR.Item("P_CCYCode"))
                ClsPR.CCYName = IIf(IsDBNull(ReaderPR.Item("CCY")), "", ReaderPR.Item("CCY"))
                ClsPR.OrderRef = IIf(IsDBNull(ReaderPR.Item("OrderRef")), "", ReaderPR.Item("OrderRef"))
                ClsPR.Amount = IIf(IsDBNull(ReaderPR.Item("Amount")), 0, ReaderPR.Item("Amount"))
                ClsPR.PayRequestEmail = IIf(IsDBNull(ReaderPR.Item("PayRequestEmail")), "", ReaderPR.Item("PayRequestEmail"))
                ClsPR.FilePath = IIf(IsDBNull(ReaderPR.Item("FilePath")), "", ReaderPR.Item("FilePath"))
                ClsPR.BeneficiaryName = IIf(IsDBNull(ReaderPR.Item("BeneficiaryName")), "", ReaderPR.Item("BeneficiaryName"))
                ClsPR.TemplateName = IIf(IsDBNull(ReaderPR.Item("TemplateName")), "", ReaderPR.Item("TemplateName"))
                ClsPR.PayRequestType = IIf(IsDBNull(ReaderPR.Item("PayRequestType")), 0, ReaderPR.Item("PayRequestType"))
                ClsPR.PayRequestRate = IIf(IsDBNull(ReaderPR.Item("PayRequestRate")), 0, ReaderPR.Item("PayRequestRate"))
                ClsPR.BeneficiaryPortfolioCode = IIf(IsDBNull(ReaderPR.Item("P_BenPortfolioCode")), 0, ReaderPR.Item("P_BenPortfolioCode"))
                ClsPR.BeneficiaryPortfolioName = IIf(IsDBNull(ReaderPR.Item("BenPortfolio")), "", ReaderPR.Item("BenPortfolio"))
                ClsPR.CheckedSanctions = IIf(IsDBNull(ReaderPR.Item("PayCheckedSantions")), False, ReaderPR.Item("PayCheckedSantions"))
                ClsPR.CheckedPEP = IIf(IsDBNull(ReaderPR.Item("PayCheckedPEP")), False, ReaderPR.Item("PayCheckedPEP"))
                ClsPR.CheckedCDD = IIf(IsDBNull(ReaderPR.Item("PayCheckedCDD")), False, ReaderPR.Item("PayCheckedCDD"))
                ClsPR.IssuesDisclosed = IIf(IsDBNull(ReaderPR.Item("PayIssuesDisclosed")), "", ReaderPR.Item("PayIssuesDisclosed"))
                ClsPR.CheckedBankName = IIf(IsDBNull(ReaderPR.Item("PayCheckedBankName")), "", ReaderPR.Item("PayCheckedBankName"))
                ClsPR.CheckedBankCountry = IIf(IsDBNull(ReaderPR.Item("PayCheckedBankCountry")), "", ReaderPR.Item("PayCheckedBankCountry"))
                ClsPR.CheckedBankCountryID = IIf(IsDBNull(ReaderPR.Item("PayCheckedBankCountryID")), 0, ReaderPR.Item("PayCheckedBankCountryID"))
                ClsPR.CheckedSwiftSortCode = IIf(IsDBNull(ReaderPR.Item("PayCheckedSwiftSortCode")), "", ReaderPR.Item("PayCheckedSwiftSortCode"))
                ClsPR.CheckedAccountNo = IIf(IsDBNull(ReaderPR.Item("PayCheckedAccountNo")), "", ReaderPR.Item("PayCheckedAccountNo"))
                ClsPR.BeneficiaryTypeCode = IIf(IsDBNull(ReaderPR.Item("P_BenTypeID")), 0, ReaderPR.Item("P_BenTypeID"))
                ClsPR.BeneficiaryType = IIf(IsDBNull(ReaderPR.Item("P_BenType")), "", ReaderPR.Item("P_BenType"))
                ClsPR.BusinessName = IIf(IsDBNull(ReaderPR.Item("P_BenName")), "", ReaderPR.Item("P_BenName"))
                ClsPR.Firstname = IIf(IsDBNull(ReaderPR.Item("P_BenFirstname")), "", ReaderPR.Item("P_BenFirstname"))
                ClsPR.Middlename = IIf(IsDBNull(ReaderPR.Item("P_BenMiddlename")), "", ReaderPR.Item("P_BenMiddlename"))
                ClsPR.Surname = IIf(IsDBNull(ReaderPR.Item("P_BenSurname")), "", ReaderPR.Item("P_BenSurname"))
                ClsPR.Address = IIf(IsDBNull(ReaderPR.Item("P_BenAddress1")), "", ReaderPR.Item("P_BenAddress1"))
                ClsPR.City = IIf(IsDBNull(ReaderPR.Item("P_BenAddress2")), "", ReaderPR.Item("P_BenAddress2"))
                ClsPR.County = IIf(IsDBNull(ReaderPR.Item("P_BenCounty")), "", ReaderPR.Item("P_BenCounty"))
                ClsPR.Postcode = IIf(IsDBNull(ReaderPR.Item("P_BenZip")), "", ReaderPR.Item("P_BenZip"))
                ClsPR.CountryCode = IIf(IsDBNull(ReaderPR.Item("P_BenCountryID")), 0, ReaderPR.Item("P_BenCountryID"))
                ClsPR.Country = IIf(IsDBNull(ReaderPR.Item("P_BenCountry")), "", ReaderPR.Item("P_BenCountry"))
                ClsPR.RelationshipCode = IIf(IsDBNull(ReaderPR.Item("P_BenRelationshipID")), 0, ReaderPR.Item("P_BenRelationshipID"))
                ClsPR.Relationship = IIf(IsDBNull(ReaderPR.Item("P_BenRelationship")), "", ReaderPR.Item("P_BenRelationship"))
                ClsPR.Reference = IIf(IsDBNull(ReaderPR.Item("P_OrderOther")), "", ReaderPR.Item("P_OrderOther"))
                ClsPR.BeneficiarySubBankName = IIf(IsDBNull(ReaderPR.Item("P_BenSubBankName")), "", ReaderPR.Item("P_BenSubBankName"))
                ClsPR.BeneficiarySubBankSwift = IIf(IsDBNull(ReaderPR.Item("P_BenSubBankSwift")), "", ReaderPR.Item("P_BenSubBankSwift"))
                ClsPR.BeneficiarySubBankIBAN = IIf(IsDBNull(ReaderPR.Item("P_BenSubBankIBAN")), "", ReaderPR.Item("P_BenSubBankIBAN"))
                ClsPR.BeneficiaryBankName = IIf(IsDBNull(ReaderPR.Item("P_BenBankName")), "", ReaderPR.Item("P_BenBankName"))
                ClsPR.BeneficiaryBankSwift = IIf(IsDBNull(ReaderPR.Item("P_BenBankSwift")), "", ReaderPR.Item("P_BenBankSwift"))
                ClsPR.BeneficiaryBankIBAN = IIf(IsDBNull(ReaderPR.Item("P_BenBankIBAN")), "", ReaderPR.Item("P_BenBankIBAN"))
                ClsPR.ManualPay = IIf(IsDBNull(ReaderPR.Item("ManualPay")), False, ReaderPR.Item("ManualPay"))
                ClsPR.MMBuySell = IIf(IsDBNull(ReaderPR.Item("PR_MM_BS")), 0, ReaderPR.Item("PR_MM_BS"))
                ClsPR.MMInstrumentCode = IIf(IsDBNull(ReaderPR.Item("PR_MM_InstrCode")), 0, ReaderPR.Item("PR_MM_InstrCode"))
                ClsPR.MMDurationType = IIf(IsDBNull(ReaderPR.Item("PR_MM_DurationType")), 0, ReaderPR.Item("PR_MM_DurationType"))
                ClsPR.MMStartDate = IIf(IsDBNull(ReaderPR.Item("PR_MM_StartDate")), Now, ReaderPR.Item("PR_MM_StartDate"))
                ClsPR.MMEndDate = IIf(IsDBNull(ReaderPR.Item("PR_MM_EndDate")), Now, ReaderPR.Item("PR_MM_EndDate"))
                ClsPR.MMFirmMove = IIf(IsDBNull(ReaderPR.Item("PR_MM_FirmMove")), 0, ReaderPR.Item("PR_MM_FirmMove"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoPRClass")
        Finally
            If ReaderPR.HasRows Then
                ReaderPR.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoCAClass(ByRef varID As String)
        Dim ReaderCA As SqlDataReader = GetSQLData("select * from vwDolfinPaymentCorporateActionsLinks where [CA ID] = " & varID)
        Try
            If ReaderCA.HasRows Then
                ReaderCA.Read()
                ClsCA.ID = IIf(IsDBNull(ReaderCA.Item("CA ID")), 0, ReaderCA.Item("CA ID"))
                ClsCA.InstrumentName = IIf(IsDBNull(ReaderCA.Item("Name")), 0, ReaderCA.Item("Name"))
                ClsCA.ISIN = IIf(IsDBNull(ReaderCA.Item("ISIN")), 0, ReaderCA.Item("ISIN"))
                ClsCA.CAEvent = IIf(IsDBNull(ReaderCA.Item("CA Event")), "", ReaderCA.Item("CA Event"))
                ClsCA.MV = IIf(IsDBNull(ReaderCA.Item("M/V")), 0, ReaderCA.Item("M/V"))
                ClsCA.Custodian = IIf(IsDBNull(ReaderCA.Item("M/V")), 0, ReaderCA.Item("M/V"))
                ClsCA.CustodianDeadline = IIf(IsDBNull(ReaderCA.Item("DeadlineDate")), "2001-01-01", ReaderCA.Item("DeadlineDate"))
                ClsCA.DolfinDeadline = IIf(IsDBNull(ReaderCA.Item("DolfinDeadlineDate")), "2001-01-01", ReaderCA.Item("DolfinDeadlineDate"))
                ClsCA.ExDate = IIf(IsDBNull(ReaderCA.Item("ExDate")), "2001-01-01", ReaderCA.Item("ExDate"))
                ClsCA.RecDate = IIf(IsDBNull(ReaderCA.Item("RecDate")), "2001-01-01", ReaderCA.Item("RecDate"))
                ClsCA.SettledPosition = IIf(IsDBNull(ReaderCA.Item("Settled Position")), 0, ReaderCA.Item("Settled Position"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoPRClass")
        Finally
            If ReaderCA.HasRows Then
                ReaderCA.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoCRMClass(ByRef varID As String)
        Dim ReaderCRM As SqlDataReader = GetSQLData("select * from vwDolfinPaymentConfirmReceiptsAll where ID = " & varID)
        Try
            If ReaderCRM.HasRows Then
                ReaderCRM.Read()
                ClsCRM.ID = IIf(IsDBNull(ReaderCRM.Item("ID")), 0, ReaderCRM.Item("ID"))
                ClsCRM.Status = IIf(IsDBNull(ReaderCRM.Item("Status")), 0, ReaderCRM.Item("Status"))
                ClsCRM.PortfolioCode = IIf(IsDBNull(ReaderCRM.Item("PF_Code")), 0, ReaderCRM.Item("PF_Code"))
                ClsCRM.PortfolioName = IIf(IsDBNull(ReaderCRM.Item("Portfolio")), 0, ReaderCRM.Item("Portfolio"))
                ClsCRM.Type = IIf(IsDBNull(ReaderCRM.Item("IncomingFundsType")), "", ReaderCRM.Item("IncomingFundsType"))
                ClsCRM.CCYCode = IIf(IsDBNull(ReaderCRM.Item("CCYCode")), 0, ReaderCRM.Item("CCYCode"))
                ClsCRM.CCYName = IIf(IsDBNull(ReaderCRM.Item("AmountCCY")), 0, ReaderCRM.Item("AmountCCY"))
                ClsCRM.TradeDate = IIf(IsDBNull(ReaderCRM.Item("TradeDate")), Now, ReaderCRM.Item("TradeDate"))
                ClsCRM.SettleDate = IIf(IsDBNull(ReaderCRM.Item("SettleDate")), Now, ReaderCRM.Item("SettleDate"))
                ClsCRM.Amount = IIf(IsDBNull(ReaderCRM.Item("Amount")), 0, ReaderCRM.Item("Amount"))
                ClsCRM.SenderName = IIf(IsDBNull(ReaderCRM.Item("Sender")), "", ReaderCRM.Item("Sender"))
                ClsCRM.SwiftCode = IIf(IsDBNull(ReaderCRM.Item("SwiftCode")), "", ReaderCRM.Item("SwiftCode"))
                ClsCRM.BankName = IIf(IsDBNull(ReaderCRM.Item("SenderBankInstitution")), "", ReaderCRM.Item("SenderBankInstitution"))
                ClsCRM.FilePath = IIf(IsDBNull(ReaderCRM.Item("Filename")), "", ReaderCRM.Item("Filename"))
                ClsCRM.Notes = IIf(IsDBNull(ReaderCRM.Item("Notes")), "", ReaderCRM.Item("Notes"))
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - error in GetReaderItemIntoCRMClass")
        Finally
            If ReaderCRM.HasRows Then
                ReaderCRM.Close()
            End If
        End Try
    End Sub

    Public Sub GetReaderItemIntoDataTable(ByRef BatchID As Double, ByRef TblBP As DataTable)
        Dim bprow As DataRow
        Dim ReaderDT As SqlDataReader = GetSQLData("select * from vwDolfinPaymentGridView where P_PaymentIDOther = " & BatchID)
        Try
            If ReaderDT.HasRows Then
                While ReaderDT.Read()
                    bprow = TblBP.NewRow()
                    bprow("P_ID") = IIf(IsDBNull(ReaderDT.Item("P_PaymentIDOther")), 0, ReaderDT.Item("P_PaymentIDOther"))
                    bprow("P_PaymentType") = IIf(IsDBNull(ReaderDT.Item("P_PaymentType")), "", ReaderDT.Item("P_PaymentType"))
                    bprow("P_PortfolioCode") = IIf(IsDBNull(ReaderDT.Item("P_PortfolioCode")), 0, ReaderDT.Item("P_PortfolioCode"))
                    bprow("P_Portfolio") = IIf(IsDBNull(ReaderDT.Item("P_Portfolio")), "", ReaderDT.Item("P_Portfolio"))
                    bprow("P_TransDate") = IIf(IsDBNull(ReaderDT.Item("P_TransDate")), Now, ReaderDT.Item("P_TransDate"))
                    bprow("P_SettleDate") = IIf(IsDBNull(ReaderDT.Item("P_SettleDate")), Now, ReaderDT.Item("P_SettleDate"))
                    bprow("P_Amount") = IIf(IsDBNull(ReaderDT.Item("P_Amount")), 0, ReaderDT.Item("P_Amount"))
                    bprow("P_CCYCode") = IIf(IsDBNull(ReaderDT.Item("P_CCYCode")), 0, ReaderDT.Item("P_CCYCode"))
                    bprow("P_CCY") = IIf(IsDBNull(ReaderDT.Item("P_CCY")), "", ReaderDT.Item("P_CCY"))
                    bprow("P_OrderBrokerCode") = IIf(IsDBNull(ReaderDT.Item("P_OrderBrokerCode")), 0, ReaderDT.Item("P_OrderBrokerCode"))
                    bprow("P_OrderAccountCode") = IIf(IsDBNull(ReaderDT.Item("P_OrderAccountCode")), 0, ReaderDT.Item("P_OrderAccountCode"))
                    bprow("P_OrderAccount") = IIf(IsDBNull(ReaderDT.Item("P_OrderAccount")), "", ReaderDT.Item("P_OrderAccount"))
                    bprow("P_OrderName") = IIf(IsDBNull(ReaderDT.Item("P_OrderName")), "", ReaderDT.Item("P_OrderName"))
                    bprow("P_OrderSwift") = IIf(IsDBNull(ReaderDT.Item("P_OrderSwift")), "", ReaderDT.Item("P_OrderSwift"))
                    bprow("P_OrderIBAN") = IIf(IsDBNull(ReaderDT.Item("P_OrderIBAN")), "", ReaderDT.Item("P_OrderIBAN"))
                    bprow("P_OrderVO_ID") = IIf(IsDBNull(ReaderDT.Item("P_OrderVO_ID")), 0, ReaderDT.Item("P_OrderVO_ID"))
                    bprow("P_OrderOther") = IIf(IsDBNull(ReaderDT.Item("P_OrderOther")), "", ReaderDT.Item("P_OrderOther"))
                    bprow("P_BenTypeID") = IIf(IsDBNull(ReaderDT.Item("P_BenTypeID")), 0, ReaderDT.Item("P_BenTypeID"))
                    bprow("P_BenType") = IIf(IsDBNull(ReaderDT.Item("P_BenType")), "", ReaderDT.Item("P_BenType"))
                    bprow("P_BenName") = IIf(IsDBNull(ReaderDT.Item("P_BenName")), "", ReaderDT.Item("P_BenName"))
                    bprow("P_BenFirstname") = IIf(IsDBNull(ReaderDT.Item("P_BenFirstname")), "", ReaderDT.Item("P_BenFirstname"))
                    bprow("P_BenMiddlename") = IIf(IsDBNull(ReaderDT.Item("P_BenMiddlename")), "", ReaderDT.Item("P_BenMiddlename"))
                    bprow("P_BenSurname") = IIf(IsDBNull(ReaderDT.Item("P_BenSurname")), "", ReaderDT.Item("P_BenSurname"))
                    bprow("P_BenCountryID") = IIf(IsDBNull(ReaderDT.Item("P_BenCountryID")), 0, ReaderDT.Item("P_BenCountryID"))
                    bprow("P_BenCountry") = IIf(IsDBNull(ReaderDT.Item("P_BenCountry")), "", ReaderDT.Item("P_BenCountry"))
                    bprow("P_BenSwift") = IIf(IsDBNull(ReaderDT.Item("P_BenSwift")), "", ReaderDT.Item("P_BenSwift"))
                    bprow("P_BenIBAN") = IIf(IsDBNull(ReaderDT.Item("P_BenIBAN")), "", ReaderDT.Item("P_BenIBAN"))
                    bprow("P_BenBIK") = IIf(IsDBNull(ReaderDT.Item("P_BenBIK")), "", ReaderDT.Item("P_BenBIK"))
                    bprow("P_BenINN") = IIf(IsDBNull(ReaderDT.Item("P_BenINN")), "", ReaderDT.Item("P_BenINN"))
                    bprow("P_BenRelationshipID") = IIf(IsDBNull(ReaderDT.Item("P_BenRelationshipID")), 0, ReaderDT.Item("P_BenRelationshipID"))
                    bprow("P_BenRelationship") = IIf(IsDBNull(ReaderDT.Item("P_BenRelationship")), "", ReaderDT.Item("P_BenRelationship"))
                    bprow("P_BenSubBankName") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankName")), "", ReaderDT.Item("P_BenSubBankName"))
                    bprow("P_BenSubBankCountryID") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankCountryID")), 0, ReaderDT.Item("P_BenSubBankCountryID"))
                    bprow("P_BenSubBankCountry") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankCountry")), "", ReaderDT.Item("P_BenSubBankCountry"))
                    bprow("P_BenSubBankSwift") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankSwift")), "", ReaderDT.Item("P_BenSubBankSwift"))
                    bprow("P_BenSubBankIBAN") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankIBAN")), "", ReaderDT.Item("P_BenSubBankIBAN"))
                    bprow("P_BenSubBankBIK") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankBIK")), "", ReaderDT.Item("P_BenSubBankBIK"))
                    bprow("P_BenSubBankINN") = IIf(IsDBNull(ReaderDT.Item("P_BenSubBankINN")), "", ReaderDT.Item("P_BenSubBankINN"))
                    bprow("P_RequestTypeID") = IIf(IsDBNull(ReaderDT.Item("PR_Type")), 0, ReaderDT.Item("PR_Type"))
                    bprow("P_RequestType") = IIf(IsDBNull(ReaderDT.Item("PR_RequestType")), 0, ReaderDT.Item("PR_RequestType"))
                    bprow("P_RequestRate") = 0
                    bprow("P_RequestFee") = 0
                    bprow("P_RequestFeeCCY") = ""
                    bprow("P_RequestKYCFolder") = ""
                    bprow("P_RequestCheckedBankName") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedBankName")), "", ReaderDT.Item("PR_CheckedBankName"))
                    bprow("P_RequestCheckedBankCountryID") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedBankCountryID")), 0, ReaderDT.Item("PR_CheckedBankCountryID"))
                    bprow("P_RequestCheckedBankCountry") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedBankCountry")), "", ReaderDT.Item("PR_CheckedBankCountry"))
                    bprow("P_RequestCheckedSanctions") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedSantions")), False, ReaderDT.Item("PR_CheckedSantions"))
                    bprow("P_RequestCheckedPEP") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedPEP")), False, ReaderDT.Item("PR_CheckedPEP"))
                    bprow("P_RequestCheckedCDD") = IIf(IsDBNull(ReaderDT.Item("PR_CheckedCDD")), False, ReaderDT.Item("PR_CheckedCDD"))
                    bprow("P_RequestIssuesDisclosed") = IIf(IsDBNull(ReaderDT.Item("PR_IssuesDisclosed")), "", ReaderDT.Item("PR_IssuesDisclosed"))
                    bprow("P_AddFee") = 0
                    bprow("P_Comments") = IIf(IsDBNull(ReaderDT.Item("P_Comments")), "", ReaderDT.Item("P_Comments"))
                    bprow("P_Username") = IIf(IsDBNull(ReaderDT.Item("P_Username")), "", ReaderDT.Item("P_Username"))
                    TblBP.Rows.Add(bprow)
                End While
            End If
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameBatchProcessing, Err.Description & " - error in GetReaderItemIntoDataTable")
        Finally
            If ReaderDT.HasRows Then
                ReaderDT.Close()
            End If
        End Try
    End Sub

    Public Sub UpdatePaymentTable(ByVal varAddNew As Integer, Optional ByRef varID As Double = 0, Optional ByRef varCommsID As Double = 0, Optional ByRef varFeeID As Double = 0)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdate"

            If varAddNew = 0 Then
                Cmd.Parameters.Add("NewPay", SqlDbType.Int).Value = 0
                Cmd.Parameters.Add("P_ID", SqlDbType.Int).Value = 1
                Cmd.Parameters.Add("P_PaymentFilePath", SqlDbType.NVarChar).Value = GetFilePath("Default")
                Cmd.Parameters.Add("P_AuthorisedUsername", SqlDbType.NVarChar).Value = DBNull.Value
            ElseIf varAddNew = 1 Then
                Cmd.Parameters.Add("NewPay", SqlDbType.Int).Value = 1
                Cmd.Parameters.Add("P_ID", SqlDbType.Int).Value = 1
                Cmd.Parameters.Add("P_PaymentFilePath", SqlDbType.NVarChar).Value = GetFilePath("Default")
                Cmd.Parameters.Add("P_AuthorisedUsername", SqlDbType.NVarChar).Value = DBNull.Value
            Else
                Cmd.Parameters.Add("NewPay", SqlDbType.Int).Value = 2
                Cmd.Parameters.Add("P_ID", SqlDbType.Int).Value = ClsPay.SelectedPaymentID
                Cmd.Parameters.Add("P_PaymentFilePath", SqlDbType.NVarChar).Value = ClsPay.PaymentFilePath
                Cmd.Parameters.Add("P_AuthorisedUsername", SqlDbType.NVarChar).Value = varUserName
            End If
            Cmd.Parameters.Add("P_PaymentType", SqlDbType.NVarChar).Value = ClsPay.PaymentType
            Cmd.Parameters.Add("P_MessageTypeID", SqlDbType.NVarChar).Value = ClsPay.OrderMessageTypeID
            Cmd.Parameters.Add("P_PortfolioCode", SqlDbType.Int).Value = ClsPay.PortfolioCode
            Cmd.Parameters.Add("P_Portfolio", SqlDbType.NVarChar).Value = ClsPay.Portfolio
            Cmd.Parameters.Add("P_TransDate", SqlDbType.SmallDateTime).Value = ClsPay.TransDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("P_SettleDate", SqlDbType.SmallDateTime).Value = ClsPay.SettleDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("P_Amount", SqlDbType.Money).Value = ClsPay.Amount
            Cmd.Parameters.Add("P_CCYCode", SqlDbType.Int).Value = ClsPay.CCYCode
            Cmd.Parameters.Add("P_CCY", SqlDbType.NVarChar).Value = ClsPay.CCY
            Cmd.Parameters.Add("P_OrderRef", SqlDbType.NVarChar).Value = ClsPay.OrderRef
            Cmd.Parameters.Add("P_OrderBrokerCode", SqlDbType.Int).Value = ClsPay.OrderBrokerCode
            Cmd.Parameters.Add("P_OrderAccountCode", SqlDbType.Int).Value = ClsPay.OrderAccountCode
            Cmd.Parameters.Add("P_OrderAccount", SqlDbType.NVarChar).Value = ClsPay.OrderAccount
            Cmd.Parameters.Add("P_OrderName", SqlDbType.NVarChar).Value = ClsPay.OrderName
            Cmd.Parameters.Add("P_OrderAddress1", SqlDbType.NVarChar).Value = ClsPay.OrderAddress1
            Cmd.Parameters.Add("P_OrderAddress2", SqlDbType.NVarChar).Value = ClsPay.OrderAddress2
            Cmd.Parameters.Add("P_OrderZip", SqlDbType.NVarChar).Value = ClsPay.OrderZip
            Cmd.Parameters.Add("P_OrderCountryID", SqlDbType.Int).Value = ClsPay.OrderCountryID
            Cmd.Parameters.Add("P_OrderSwift", SqlDbType.NVarChar).Value = ClsPay.OrderSwift
            Cmd.Parameters.Add("P_OrderIBAN", SqlDbType.NVarChar).Value = ClsPay.OrderIBAN
            Cmd.Parameters.Add("P_OrderOther", SqlDbType.NVarChar).Value = ClsPay.OrderOther
            Cmd.Parameters.Add("P_OrderVO_ID", SqlDbType.Int).Value = ClsPay.OrderVOCodeID
            Cmd.Parameters.Add("P_BenRef", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryRef
            Cmd.Parameters.Add("P_BenBrokerCode", SqlDbType.Int).Value = ClsPay.BeneficiaryBrokerCode
            Cmd.Parameters.Add("P_BenAccountCode", SqlDbType.Int).Value = ClsPay.BeneficiaryAccountCode
            Cmd.Parameters.Add("P_BenAccount", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryAccount
            Cmd.Parameters.Add("P_BenAddressID", SqlDbType.Int).Value = ClsPay.BeneficiaryAddressID
            Cmd.Parameters.Add("P_BenTypeID", SqlDbType.Int).Value = ClsPay.BeneficiaryTypeID
            Cmd.Parameters.Add("P_BenName", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryName
            Cmd.Parameters.Add("P_BenFirstname", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryFirstName
            Cmd.Parameters.Add("P_BenMiddlename", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryMiddleName
            Cmd.Parameters.Add("P_BenSurname", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySurname
            Cmd.Parameters.Add("P_BenAddress1", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryAddress1
            Cmd.Parameters.Add("P_BenAddress2", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryAddress2
            Cmd.Parameters.Add("P_BenCounty", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryCounty
            Cmd.Parameters.Add("P_BenZip", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryZip
            Cmd.Parameters.Add("P_BenCountryID", SqlDbType.Int).Value = ClsPay.BeneficiaryCountryID
            Cmd.Parameters.Add("P_BenSwift", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySwift
            Cmd.Parameters.Add("P_BenIBAN", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryIBAN
            Cmd.Parameters.Add("P_BenOther", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryOther
            Cmd.Parameters.Add("P_BenBIK", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBIK
            Cmd.Parameters.Add("P_BenINN", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryINN
            Cmd.Parameters.Add("P_BenRelationshipID", SqlDbType.Int).Value = ClsPay.BeneficiaryRelationshipID
            Cmd.Parameters.Add("P_BenPortfolioCode", SqlDbType.Int).Value = ClsPay.BeneficiaryPortfolioCode
            Cmd.Parameters.Add("P_BenPortfolio", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryPortfolio
            Cmd.Parameters.Add("P_BenCCYCode", SqlDbType.Int).Value = ClsPay.BeneficiaryCCYCode
            Cmd.Parameters.Add("P_BenCCY", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryCCY
            Cmd.Parameters.Add("P_ExchangeRate", SqlDbType.Float).Value = ClsPay.ExchangeRate
            Cmd.Parameters.Add("P_BenTemplateName", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryTemplateName
            Cmd.Parameters.Add("P_BenSubBankAddressID", SqlDbType.Int).Value = ClsPay.BeneficiarySubBankAddressID
            Cmd.Parameters.Add("P_BenSubBankName", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankName
            Cmd.Parameters.Add("P_BenSubBankAddress1", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankAddress1
            Cmd.Parameters.Add("P_BenSubBankAddress2", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankAddress2
            Cmd.Parameters.Add("P_BenSubBankZip", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankZip
            Cmd.Parameters.Add("P_BenSubBankCountryID", SqlDbType.Int).Value = ClsPay.BeneficiarySubBankCountryID
            Cmd.Parameters.Add("P_BenSubBankSwift", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankSwift
            Cmd.Parameters.Add("P_BenSubBankIBAN", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankIBAN
            Cmd.Parameters.Add("P_BenSubBankOther", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankOther
            Cmd.Parameters.Add("P_BenSubBankBIK", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankBIK
            Cmd.Parameters.Add("P_BenSubBankINN", SqlDbType.NVarChar).Value = ClsPay.BeneficiarySubBankINN
            Cmd.Parameters.Add("P_BenBankAddressID", SqlDbType.Int).Value = ClsPay.BeneficiaryBankAddressID
            Cmd.Parameters.Add("P_BenBankName", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankName
            Cmd.Parameters.Add("P_BenBankAddress1", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankAddress1
            Cmd.Parameters.Add("P_BenBankAddress2", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankAddress2
            Cmd.Parameters.Add("P_BenBankZip", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankZip
            Cmd.Parameters.Add("P_BenBankCountryID", SqlDbType.Int).Value = ClsPay.BeneficiaryBankCountryID
            Cmd.Parameters.Add("P_BenBankSwift", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankSwift
            Cmd.Parameters.Add("P_BenBankIBAN", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankIBAN
            Cmd.Parameters.Add("P_BenBankOther", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankOther
            Cmd.Parameters.Add("P_BenBankBIK", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankBIK
            Cmd.Parameters.Add("P_BenBankINN", SqlDbType.NVarChar).Value = ClsPay.BeneficiaryBankINN
            Cmd.Parameters.Add("P_Comments", SqlDbType.NVarChar).Value = ClsPay.Comments
            Cmd.Parameters.Add("P_SendSwift", SqlDbType.Bit).Value = ClsPay.SendSwift
            Cmd.Parameters.Add("P_SendIMS", SqlDbType.Bit).Value = ClsPay.SendIMS
            Cmd.Parameters.Add("P_SendSwift2", SqlDbType.Bit).Value = ClsPay.SendSwift2
            Cmd.Parameters.Add("P_SendIMS2", SqlDbType.Bit).Value = ClsPay.SendIMS2
            Cmd.Parameters.Add("P_SwiftUrgent", SqlDbType.Bit).Value = ClsPay.SwiftUrgent
            Cmd.Parameters.Add("P_SwiftSendRef", SqlDbType.Bit).Value = ClsPay.SwiftSendRef
            Cmd.Parameters.Add("P_Other_ID", SqlDbType.Int).Value = ClsPay.OtherID
            Cmd.Parameters.Add("P_PortfolioFee", SqlDbType.Money).Value = ClsPay.PortfolioFee
            Cmd.Parameters.Add("P_PortfolioFeeCCY", SqlDbType.NVarChar).Value = ClsPay.PortfolioFeeCCY
            Cmd.Parameters.Add("P_PortfolioFeeInt", SqlDbType.Money).Value = ClsPay.PortfolioFeeInt
            Cmd.Parameters.Add("P_PortfolioFeeCCYInt", SqlDbType.NVarChar).Value = ClsPay.PortfolioFeeCCYInt
            Cmd.Parameters.Add("P_PortfolioFeeAccountCode", SqlDbType.Int).Value = ClsPay.PortfolioFeeAccountCode
            Cmd.Parameters.Add("P_PortfolioFeeRMSAccountCode", SqlDbType.Int).Value = ClsPay.PortfolioFeeRMSAccountCode
            Cmd.Parameters.Add("P_PortfolioIsFee", SqlDbType.Bit).Value = ClsPay.PortfolioIsFee
            Cmd.Parameters.Add("P_PortfolioIsCommission", SqlDbType.Bit).Value = ClsPay.PortfolioIsCommission
            Cmd.Parameters.Add("P_PortfolioFeeID", SqlDbType.Int).Value = ClsPay.PortfolioFeeID
            Cmd.Parameters.Add("P_Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.Parameters.Add("P_SourceSystem", SqlDbType.NVarChar).Value = GetFileExportName("SourceSystem")
            Cmd.Parameters.Add("P_AboveThreshold", SqlDbType.Bit).Value = ClsPay.AboveThreshold
            Cmd.Parameters.Add("P_IMSRef", SqlDbType.NVarChar).Value = ClsPay.IMSRef
            Cmd.Parameters.Add("P_SentApproveEmailCompliance", SqlDbType.Int).Value = ClsPay.SentApproveEmailCompliance
            Cmd.Parameters.Add("PR_PaymentRequested", SqlDbType.Bit).Value = ClsPay.PaymentRequested
            Cmd.Parameters.Add("PR_PaymentRequestedType", SqlDbType.Int).Value = ClsPay.PaymentRequestedType
            Cmd.Parameters.Add("PR_PaymentRequestedRate", SqlDbType.Float).Value = ClsPay.PaymentRequestedRate
            Cmd.Parameters.Add("PR_PaymentRequestedFrom", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedFrom
            Cmd.Parameters.Add("PR_PaymentRequestedEmail", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedEmail
            Cmd.Parameters.Add("PR_PaymentRequestedBeneficiaryName", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedBeneficiaryName
            Cmd.Parameters.Add("PR_CheckedSantions", SqlDbType.Bit).Value = ClsPay.PaymentRequestedCheckedSantions
            Cmd.Parameters.Add("PR_CheckedPEP", SqlDbType.Bit).Value = ClsPay.PaymentRequestedCheckedPEP
            Cmd.Parameters.Add("PR_CheckedCDD", SqlDbType.Bit).Value = ClsPay.PaymentRequestedCheckedCDD
            Cmd.Parameters.Add("PR_IssuesDisclosed", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedIssuesDisclosed
            Cmd.Parameters.Add("PR_CheckedBankName", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedCheckedBankName
            Cmd.Parameters.Add("PR_CheckedBankCountryID", SqlDbType.Int).Value = ClsPay.PaymentRequestedCheckedBankCountryID
            Cmd.Parameters.Add("PR_CheckedBankCountry", SqlDbType.NVarChar).Value = ClsPay.PaymentRequestedCheckedBankCountry
            Cmd.Parameters.Add("PR_MM_BS", SqlDbType.Int).Value = ClsPay.PaymentRequestedMMBuySell
            Cmd.Parameters.Add("PR_MM_InstrCode", SqlDbType.Int).Value = ClsPay.PaymentRequestedMMInstrumentCode
            Cmd.Parameters.Add("PR_MM_DurationType", SqlDbType.Int).Value = ClsPay.PaymentRequestedMMDurationType
            Cmd.Parameters.Add("PR_MM_StartDate", SqlDbType.SmallDateTime).Value = ClsPay.PaymentRequestedMMStartDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("PR_MM_EndDate", SqlDbType.SmallDateTime).Value = ClsPay.PaymentRequestedMMEndDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("PR_MM_FirmMove", SqlDbType.Bit).Value = ClsPay.PaymentRequestedMMFirmMove
            Cmd.Parameters.Add("NewID", SqlDbType.Int)
            Cmd.Parameters("NewID").Direction = ParameterDirection.Output
            Cmd.Parameters.Add("NewCommsID", SqlDbType.Int)
            Cmd.Parameters("NewCommsID").Direction = ParameterDirection.Output
            Cmd.Parameters.Add("NewFeeID", SqlDbType.Int)
            Cmd.Parameters("NewFeeID").Direction = ParameterDirection.Output

            Cmd.ExecuteNonQuery()
            If varAddNew = 0 Or varAddNew = 1 Then
                If IsDBNull(Cmd.Parameters("NewID").Value) Then
                    varID = GetIDFromDB("DolfinPayment")
                Else
                    varID = Cmd.Parameters("NewID").Value
                End If
            Else
                varID = ClsPay.SelectedPaymentID
            End If

            If Not IsDBNull(Cmd.Parameters("NewCommsID").Value) Then
                varCommsID = Cmd.Parameters("NewCommsID").Value
            End If

            If Not IsDBNull(Cmd.Parameters("NewFeeID").Value) Then
                varFeeID = Cmd.Parameters("NewFeeID").Value
            End If

            If varAddNew = 0 Or varAddNew = 1 Then
                AboveThresholdEmail(varID)
            Else
                AboveThresholdEmail(ClsPay.SelectedPaymentID)
            End If
        Catch ex As Exception
            If varAddNew = 0 Or varAddNew = 1 Then
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on adding spDolfinPaymentupdate ID: " & vID & ", portfolio: " & ClsPay.Portfolio & " - UpdatePaymentTable", IIf(IsNumeric(vID), vID, varID))
            Else
                AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on editing spDolfinPaymentupdate ID:" & ClsPay.SelectedPaymentID & ", portfolio: " & ClsPay.Portfolio & " - UpdatePaymentTable", ClsPay.SelectedPaymentID)
            End If
        Finally
            Cmd = Nothing
            If varAddNew = 0 Or varAddNew = 1 Then
                If varID < 0 Then
                    AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - NO PAYMENT CREATED FOR Portfolio: " & ClsPay.Portfolio & " - Error on adding spDolfinPaymentupdate ID:" & vID & ", portfolio: " & ClsPay.Portfolio & " - UpdatePaymentTable", IIf(IsNumeric(vID), vID, varID))
                Else
                    AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "Added transaction ID:" & vID & ", portfolio: " & ClsPay.Portfolio & " - UpdatePaymentTable", IIf(IsNumeric(vID), vID, varID))
                End If
            Else
                AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "Edited transaction ID:" & ClsPay.SelectedPaymentID & ", portfolio: " & ClsPay.Portfolio & " - UpdatePaymentTable", ClsPay.SelectedPaymentID)
            End If
        End Try
    End Sub

    Public Sub UpdateManualFunds(ByRef varNewFundsID As Double, ByRef varFilePath As String)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateFunds"

            Cmd.Parameters.Add("varTradeDate", SqlDbType.SmallDateTime).Value = ClsCRM.TradeDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("varSettleDate", SqlDbType.SmallDateTime).Value = ClsCRM.SettleDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("varSender", SqlDbType.NVarChar).Value = ClsCRM.SenderName
            Cmd.Parameters.Add("varAmount", SqlDbType.Money).Value = ClsCRM.Amount
            Cmd.Parameters.Add("varCCYCode", SqlDbType.Int).Value = ClsCRM.CCYCode
            Cmd.Parameters.Add("varSwiftCode", SqlDbType.NVarChar).Value = ClsCRM.SwiftCode
            Cmd.Parameters.Add("varFilePath", SqlDbType.NVarChar).Value = ClsCRM.FilePath
            Cmd.Parameters.Add("varNotes", SqlDbType.NVarChar).Value = ClsCRM.Notes
            Cmd.Parameters.Add("varUsername", SqlDbType.NVarChar).Value = FullUserName
            Cmd.Parameters.Add("NewFundsID", SqlDbType.Int)
            Cmd.Parameters.Add("NewFilePath", SqlDbType.NVarChar)
            Cmd.Parameters("NewFundsID").Direction = ParameterDirection.Output
            Cmd.Parameters("NewFilePath").Direction = ParameterDirection.Output
            Cmd.Parameters("NewFilePath").Size = 2000
            Cmd.ExecuteNonQuery()
            varNewFundsID = Cmd.Parameters("NewFundsID").Value
            varFilePath = Cmd.Parameters("NewFilePath").Value
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on adding DolfinPaymentUpdateFunds", varNewFundsID)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub RetrictionListAdd(ByVal varTicker As String)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentRestrictionListAdd"

            Cmd.Parameters.Add("varTicker", SqlDbType.NVarChar).Value = varTicker
            Cmd.Parameters.Add("varUsername", SqlDbType.NVarChar).Value = FullUserName
            Cmd.ExecuteNonQuery()

        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error in RetrictionListAdd", 0)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub RetrictionListRemove(ByVal varTicker As String)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentRestrictionListRemove"

            Cmd.Parameters.Add("varTicker", SqlDbType.NVarChar).Value = varTicker
            Cmd.Parameters.Add("varUsername", SqlDbType.NVarChar).Value = FullUserName
            Cmd.ExecuteNonQuery()

        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error in RetrictionListRemove", 0)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdateFundsPortfolio(ByRef varID As Double, ByRef varPortfolioCode As Double)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateFundsPortfolio"

            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("varPortfolioCode", SqlDbType.Int).Value = varPortfolioCode
            Cmd.Parameters.Add("varUsername", SqlDbType.NVarChar).Value = FullUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on adding UpdateFundsPortfolio", varID)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub AboveThresholdEmail(ByVal varSelectedPaymentID As Double)
        GetReaderItemIntoAuthPaymentClass(varSelectedPaymentID)
        If ClsPay.AboveThreshold Then
            ClsPayAuth.SelectedEmailAttachments = False
            FrmEmail.ShowDialog()
        End If
    End Sub

    Public Function UpdatePaymentTableRD(ByVal varAddNew As Boolean) As Double
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        UpdatePaymentTableRD = 0
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateReceiveDeliver"
            If varAddNew Then
                Cmd.Parameters.Add("NewRD", SqlDbType.Int).Value = 1
                Cmd.Parameters.Add("PRD_ID", SqlDbType.Int).Value = 1
            Else
                Cmd.Parameters.Add("NewRD", SqlDbType.Int).Value = 2
                Cmd.Parameters.Add("PRD_ID", SqlDbType.Int).Value = ClsRD.SelectedTransactionID
            End If

            Cmd.Parameters.Add("PRD_RecType", SqlDbType.Int).Value = ClsRD.RecType
            Cmd.Parameters.Add("PRD_IMS_TrCode", SqlDbType.Int).Value = ClsRD.IMSTrCode
            Cmd.Parameters.Add("PRD_PortfolioCode", SqlDbType.Int).Value = ClsRD.PortfolioCode
            Cmd.Parameters.Add("PRD_CCYCode", SqlDbType.Int).Value = ClsRD.CCYCode
            Cmd.Parameters.Add("PRD_InstCode", SqlDbType.Int).Value = ClsRD.InstrCode
            Cmd.Parameters.Add("PRD_BrokerCode", SqlDbType.Int).Value = ClsRD.BrokerCode
            Cmd.Parameters.Add("PRD_CashAccountCode", SqlDbType.Int).Value = ClsRD.CashAccountCode
            Cmd.Parameters.Add("PRD_ClearerCode", SqlDbType.Int).Value = ClsRD.ClearerCode
            Cmd.Parameters.Add("PRD_TransDate", SqlDbType.SmallDateTime).Value = ClsRD.TransDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("PRD_SettleDate", SqlDbType.SmallDateTime).Value = ClsRD.SettleDate.ToString("yyyy-MM-dd")
            Cmd.Parameters.Add("PRD_Amount", SqlDbType.Money).Value = ClsRD.Amount
            Cmd.Parameters.Add("PRD_Agent", SqlDbType.NVarChar).Value = IIf(IsNothing(ClsRD.Agent), DBNull.Value, ClsRD.Agent)
            Cmd.Parameters.Add("PRD_AgentAccountNo", SqlDbType.NVarChar).Value = IIf(IsNothing(ClsRD.AgentAccountNo), DBNull.Value, ClsRD.AgentAccountNo)
            Cmd.Parameters.Add("PRD_PlaceofSettlement", SqlDbType.NVarChar).Value = IIf(IsNothing(ClsRD.PlaceofSettlement), DBNull.Value, ClsRD.PlaceofSettlement)
            Cmd.Parameters.Add("PRD_Instructions", SqlDbType.NVarChar).Value = IIf(IsNothing(ClsRD.Instructions), DBNull.Value, ClsRD.Instructions)
            Cmd.Parameters.Add("PRD_SendIMS", SqlDbType.Bit).Value = ClsRD.SendIMS
            Cmd.Parameters.Add("PRD_SendMiniOMS", SqlDbType.Bit).Value = ClsRD.SendMiniOMS
            Cmd.Parameters.Add("PRD_Email", SqlDbType.NVarChar).Value = ClsRD.Email
            'Cmd.Parameters.Add("PRD_FilePath", SqlDbType.NVarChar).Value = ClsRD.FilePath
            Cmd.Parameters.Add("PRD_Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
            vID = GetIDFromDB("DolfinPaymentReceiveDeliver")
            UpdatePaymentTableRD = vID
        Catch ex As Exception
            If varAddNew Then
                AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error on adding UpdatePaymentTableRD ID: " & vID & " - UpdatePaymentTable", IIf(IsNumeric(vID), vID, 0))
            Else
                AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error on adding UpdatePaymentTableRD ID:" & vID & " - UpdatePaymentTable", IIf(IsNumeric(vID), vID, 0))
            End If
        Finally
            Cmd = Nothing
            If varAddNew Then
                AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "Added transaction ID:" & vID & " - UpdatePaymentTableRD", IIf(IsNumeric(vID), vID, 0))
            Else
                AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "Edited transaction ID:" & vID & " - UpdatePaymentTableRD", IIf(IsNumeric(vID), vID, 0))
            End If
        End Try
    End Function

    Public Sub UpdateTransferFee(ByVal varPortfolioCode As Double, ByVal varTFCCY As String, ByVal varTFFee As Double, ByVal varTFIntCCY As String, ByVal varTFIntFee As Double)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateTransferFee"
            Cmd.Parameters.Add("@TF_PortfolioCode", SqlDbType.Int).Value = varPortfolioCode
            Cmd.Parameters.Add("@TF_CCY", SqlDbType.NVarChar).Value = varTFCCY
            Cmd.Parameters.Add("@TF_Fee", SqlDbType.Int).Value = varTFFee
            Cmd.Parameters.Add("@TF_Int_CCY", SqlDbType.NVarChar).Value = varTFIntCCY
            Cmd.Parameters.Add("@TF_Int_Fee", SqlDbType.Int).Value = varTFIntFee
            Cmd.Parameters.Add("Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on adding Transfer Fee", IIf(IsNumeric(vID), vID, 0))
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "Updated Transfer Fee", IIf(IsNumeric(vID), vID, 0))
        End Try
    End Sub

    Public Function UpdateFeeSchedule(ByVal varActionFeeSchedule As Integer, ByRef TblFeeSchedules As DataTable, ByRef TblTransferFees As DataTable,
            ByRef varActionMGTFees As Integer, ByRef TblMgtFee As DataTable, ByRef TblMgtFeeRanges As DataTable, ByRef TblMgtFeeExceptions As DataTable,
        ByRef varActionCF As Integer, ByRef TblCF As DataTable, ByRef TblCFRanges As DataTable, ByRef TblCFExceptions As DataTable, ByRef TblCFSK As DataTable,
        ByRef varActionTF As Integer, ByRef TblTF As DataTable, ByRef TblTFRanges As DataTable, ByRef TblTFSK As DataTable, ByRef TblTFSKFixed As DataTable) As Boolean
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        UpdateFeeSchedule = True
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateFeeSchedule"
            Cmd.Parameters.Add("@NewFS", SqlDbType.Int).Value = varActionFeeSchedule
            Cmd.Parameters.Add("@TblFeeSchedules", SqlDbType.Structured).Value = TblFeeSchedules
            Cmd.Parameters.Add("@TblTransferFees", SqlDbType.Structured).Value = TblTransferFees
            Cmd.Parameters.Add("@NewMgtFee", SqlDbType.Int).Value = varActionMGTFees
            Cmd.Parameters.Add("@TblMgtFee", SqlDbType.Structured).Value = TblMgtFee
            Cmd.Parameters.Add("@TblMgtFeeRanges", SqlDbType.Structured).Value = TblMgtFeeRanges
            Cmd.Parameters.Add("@TblMgtFeeExceptions", SqlDbType.Structured).Value = TblMgtFeeExceptions
            Cmd.Parameters.Add("@NewCF", SqlDbType.Int).Value = varActionCF
            Cmd.Parameters.Add("@TblCF", SqlDbType.Structured).Value = TblCF
            Cmd.Parameters.Add("@TblCFRanges", SqlDbType.Structured).Value = TblCFRanges
            Cmd.Parameters.Add("@TblCFExceptions", SqlDbType.Structured).Value = TblCFExceptions
            Cmd.Parameters.Add("@TblCFSK", SqlDbType.Structured).Value = TblCFSK
            Cmd.Parameters.Add("@NewTF", SqlDbType.Int).Value = varActionTF
            Cmd.Parameters.Add("@TblTF", SqlDbType.Structured).Value = TblTF
            Cmd.Parameters.Add("@TblTFRanges", SqlDbType.Structured).Value = TblTFRanges
            Cmd.Parameters.Add("@TblTFSK", SqlDbType.Structured).Value = TblTFSK
            Cmd.Parameters.Add("@TblTFSKFixed", SqlDbType.Structured).Value = TblTFSKFixed
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
            vID = GetIDFromDB("DolfinPaymentFeeSchedules")
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "Updated UpdateFeeSchedule", IIf(IsNumeric(vID), vID, 0))
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on adding UpdateFeeSchedule", IIf(IsNumeric(vID), vID, 0))
            UpdateFeeSchedule = False
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Sub UpdateRiskAssessmentPersonTable(ByVal TblPerson As DataTable, ByVal TblAddresses As DataTable, ByVal TblAlias As DataTable,
                                               ByVal TblArticles As DataTable, ByVal TblSanctions As DataTable, ByVal TblNotes As DataTable,
                                               ByVal TblLinkedBusinesses As DataTable, ByVal TblLinkedPersons As DataTable, ByVal TblPoliticalPositions As DataTable)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddRAPerson"
            Cmd.Parameters.Add("@TblPerson", SqlDbType.Structured).Value = TblPerson
            Cmd.Parameters.Add("@TblAddress", SqlDbType.Structured).Value = TblAddresses
            Cmd.Parameters.Add("@TblAlias", SqlDbType.Structured).Value = TblAlias
            Cmd.Parameters.Add("@TblArticle", SqlDbType.Structured).Value = TblArticles
            Cmd.Parameters.Add("@TblSanctions", SqlDbType.Structured).Value = TblSanctions
            Cmd.Parameters.Add("@TblNotes", SqlDbType.Structured).Value = TblNotes
            Cmd.Parameters.Add("@TblLinkedBusiness", SqlDbType.Structured).Value = TblLinkedBusinesses
            Cmd.Parameters.Add("@TblLinkedPerson", SqlDbType.Structured).Value = TblLinkedPersons
            Cmd.Parameters.Add("@TblPoliticalPosition", SqlDbType.Structured).Value = TblPoliticalPositions
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error on UpdateRiskAssessmentPersonTable")
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdateRiskAssessmentBusinessTable(ByVal TblBusiness As DataTable, ByVal TblAddresses As DataTable, ByVal TblAlias As DataTable,
                                               ByVal TblArticles As DataTable, ByVal TblSanctions As DataTable, ByVal TblNotes As DataTable,
                                               ByVal TblLinkedBusinesses As DataTable, ByVal TblLinkedPersons As DataTable)
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddRABusiness"
            Cmd.Parameters.Add("@TblBusiness", SqlDbType.Structured).Value = TblBusiness
            Cmd.Parameters.Add("@TblAddress", SqlDbType.Structured).Value = TblAddresses
            Cmd.Parameters.Add("@TblAlias", SqlDbType.Structured).Value = TblAlias
            Cmd.Parameters.Add("@TblArticle", SqlDbType.Structured).Value = TblArticles
            Cmd.Parameters.Add("@TblSanctions", SqlDbType.Structured).Value = TblSanctions
            Cmd.Parameters.Add("@TblNotes", SqlDbType.Structured).Value = TblNotes
            Cmd.Parameters.Add("@TblLinkedBusiness", SqlDbType.Structured).Value = TblLinkedBusinesses
            Cmd.Parameters.Add("@TblLinkedPerson", SqlDbType.Structured).Value = TblLinkedPersons
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameRiskAssessment, Err.Description & " - Error on UpdateRiskAssessmentPersonTable")
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Function AuthoriseFeesPaid(ByVal TblFeesPaid As DataTable, ByVal varOpt As Boolean) As Boolean
        Dim Cmd As New SqlCommand
        Try
            AuthoriseFeesPaid = False
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAuthoriseFeesPaid"
            Cmd.Parameters.Add("@TblFeesPaid", SqlDbType.Structured).Value = TblFeesPaid
            Cmd.Parameters.Add("@Opt", SqlDbType.Bit).Value = varOpt
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on AuthoriseFeesPaid")
        Finally
            Cmd = Nothing
            AuthoriseFeesPaid = True
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "AuthoriseFeesPaid")
        End Try
    End Function

    Public Function UpdateFeesPaid(ByVal TblFeesPaid As DataTable) As Boolean
        Dim Cmd As New SqlCommand
        Try
            UpdateFeesPaid = False
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateFeesPaid"
            Cmd.Parameters.Add("@TblFeesPaid", SqlDbType.Structured).Value = TblFeesPaid
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on UpdateFeesPaid")
        Finally
            Cmd = Nothing
            UpdateFeesPaid = True
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "UpdateFeesPaid")
        End Try
    End Function

    Public Function AddBatchPayments(ByRef varOpt As Integer, ByRef TblBP As DataTable, ByRef BatchID As Double) As Boolean
        Dim Cmd As New SqlCommand
        Dim vID As String = ""

        AddBatchPayments = True
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddBatchPayments"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@TblBP", SqlDbType.Structured).Value = TblBP
            Cmd.Parameters.Add("@BatchID", SqlDbType.Int)
            Cmd.Parameters("@BatchID").Direction = ParameterDirection.Output
            Cmd.ExecuteNonQuery()

            If Not IsDBNull(Cmd.Parameters("@BatchID").Value) Then
                BatchID = Cmd.Parameters("@BatchID").Value
            End If

            AddAudit(cAuditStatusSuccess, cAuditGroupNameBatchProcessing, "Added Batch Payments", IIf(IsNumeric(vID), vID, 0))
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameBatchProcessing, Err.Description & " - Error on AddBatchPayments", IIf(IsNumeric(vID), vID, 0))
            AddBatchPayments = False
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Sub UpdateSendApproveMovementEmail(ByVal varID As Double)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentSentApproveEmail"
            Cmd.Parameters.Add("@varID", SqlDbType.Int).Value = varID
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on UpdateSendApproveMovementEmail for ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "UpdateSendApproveMovementEmail for ID:" & varID, varID)
        End Try
    End Sub

    Public Sub RefreshPaymentGrid(ByVal dgv As DataGridView, Optional varWhereClauseFilter As String = "")
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = ""

        Try
            If varWhereClauseFilter <> "" Then
                varSQL = "Select top 1000 * from vwDolfinPaymentGridViewLiteAll " & varWhereClauseFilter & " and Branchcode in (" & UserLocationCode & ")"
                'varSQL = "Select top 1000 * from vwDolfinPaymentGridViewLiteAll " & varWhereClauseFilter & " and Branchcode in (" & IIf(BranchLocationCode = 1, "42,47", IIf(BranchLocationCode <> 0, BranchLocationCode, UserLocationCode)) & ")"
            Else
                varSQL = "Select top 1000 * from vwDolfinPaymentGridViewLite where Branchcode in (" & UserLocationCode & ")"
                'varSQL = "Select top 1000 * from vwDolfinPaymentGridViewLite where Branchcode in (" & IIf(BranchLocationCode = 1, "42,47", IIf(BranchLocationCode <> 0, BranchLocationCode, UserLocationCode)) & ")"
            End If

            If UserOptGridSort = 0 Then
                varSQL = varSQL & " order by orderval, SettleDate, AuthorisedTime"
            Else
                varSQL = varSQL & " order by id desc, SettleDate, AuthorisedTime"
            End If

            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in RefreshPaymentGrid: whereclause: " & varWhereClauseFilter)
        End Try
    End Sub

    Public Sub RefreshViewGrid(ByVal dgv As DataGridView, Optional varWhereClauseFilter As String = "")
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = ""

        Try
            varSQLSelect = "Select * from vwDolfinPaymentGridViewAll "

            If varWhereClauseFilter <> "" Then
                varSQL = varSQLSelect & varWhereClauseFilter
            Else
                varSQL = varSQLSelect & "where cast(P_TransDate as date) = cast(getdate() as date) "
            End If

            If UserOptGridSort = 0 Then
                varSQL = varSQL & "order by orderval, p_SettleDate, p_AuthorisedTime"
            Else
                varSQL = varSQL & "order by p_id desc, p_SettleDate, p_AuthorisedTime"
            End If

            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameViewMovements, Err.Description & " - error in RefreshPaymentGrid: whereclause: " & varWhereClauseFilter)
        End Try
    End Sub

    Public Sub RefreshHistoryGrid(ByVal dgv As DataGridView, ByVal AddAccount As Boolean)
        Dim dgvHistData As SqlClient.SqlDataReader
        Dim varSQL As String = ""

        Try
            varSQL = "Select Portfolio, Broker, Account, TradeDate, SettleDate, Amount, CCY, Comments, Reference, DocNo, LastUpdated from vwDolfinPaymentTransactionHistory where tr_pfcode = " & ClsPay.PortfolioCode & " and TR_Currency = " & ClsPay.CCYCode

            If AddAccount Then
                If ClsPay.OrderAccountCode > 0 Then
                    varSQL = varSQL & " and tr_tcode = " & ClsPay.OrderAccountCode
                End If
            End If

            varSQL = varSQL & " order by SettleDate desc, LastUpdated desc"

            dgvHistData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvHistData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvHistData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error In RefreshHistoryGrid")
        End Try
    End Sub

    Public Sub RefreshRDGrid(ByVal dgv As DataGridView, Optional varWhereClauseFilter As String = "", Optional varCRMView As Boolean = False)
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = ""

        Try
            If varCRMView Then
                varSQL = "Select top 1000 * from vwDolfinPaymentReceiveDeliverGridViewLitePaymentRequest "
            Else
                varSQL = "Select top 1000 * from vwDolfinPaymentReceiveDeliverGridViewLite "
            End If

            If varWhereClauseFilter <> "" Then
                varSQL = varSQL & varWhereClauseFilter
            End If

            varSQL = varSQL & " order by ID desc"

            'If UserOptGridSort = 0 Then
            ' varSQL = varSQL & " order by orderval, SettleDate, AuthorisedTime"
            'Else
            'varSQL = varSQL & " order by id desc, SettleDate, AuthorisedTime"
            'End If

            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in RefreshRDGrid: whereclause: " & varWhereClauseFilter)
        End Try
    End Sub


    Public Sub RefreshGridSwift(ByVal dgv As DataGridView, Optional varWhereClauseFilter As String = "")
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = ""

        Try
            varSQL = "Select top 1000 * from vwDolfinPaymentSwiftGridViewLite order by ID desc"
            If varWhereClauseFilter <> "" Then
                varSQL = varSQL & varWhereClauseFilter
            End If

            'If UserOptGridSort = 0 Then
            ' varSQL = varSQL & " order by orderval, SettleDate, AuthorisedTime"
            'Else
            'varSQL = varSQL & " order by id desc, SettleDate, AuthorisedTime"
            'End If

            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameViewAllSwifts, Err.Description & " - error in RefreshGridSwift: whereclause: " & varWhereClauseFilter)
        End Try
    End Sub

    Public Sub RefreshGridAuditTrail(ByVal Opt As Integer, ByVal dgv As DataGridView, Optional varWhereClauseFilter As String = "")
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = ""

        Try
            If Opt = 0 Then
                varSQL = "Select * from vwDolfinPaymentAuditLog "
                If varWhereClauseFilter <> "" Then
                    varSQL = varSQL & varWhereClauseFilter
                End If

                varSQL = varSQL & "order by A_AuditDateTime desc"
            ElseIf Opt = 1 Then
                varSQL = "exec imsplus.[dbo].[_Dolfin_CalcManagementFeeByDateBreakdown] "
                If varWhereClauseFilter <> "" Then
                    varSQL = varSQL & varWhereClauseFilter
                End If
            ElseIf Opt = 2 Then
                varSQL = "exec imsplus.[dbo].[_Dolfin_CalcCustodyFeeByDateBreakdown] "
                If varWhereClauseFilter <> "" Then
                    varSQL = varSQL & varWhereClauseFilter
                End If
            ElseIf Opt = 3 Then
                varSQL = "Select top 1000 * from vwDolfinPaymentFeeHistoryPayments "
                If varWhereClauseFilter <> "" Then
                    varSQL = varSQL & varWhereClauseFilter
                End If
            ElseIf Opt = 4 Then
                varSQL = "Select top 1000 * from vwDolfinPaymentAuditLog Where (A_AuditTransactionID = " & varWhereClauseFilter & " And A_AuditGroupName = 'ConfirmReceipts') 
                union Select top 1000 * from vwDolfinPaymentAuditLog Where A_AuditTransactionID in (select crm_SuspensepaymentID from [DolfinPaymentConfirmReceiptManualEntry] where crm_id = " & varWhereClauseFilter & ")
				union Select top 1000 * from vwDolfinPaymentAuditLog Where A_AuditTransactionID in (select crm_PortfolioPaymentID from [DolfinPaymentConfirmReceiptManualEntry] where crm_id = " & varWhereClauseFilter & ")
                 order by A_AuditDateTime desc"
            End If


            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameViewAllSwifts, Err.Description & " - error in RefreshGridAuditTrail: whereclause: " & varWhereClauseFilter)
        End Try
    End Sub

    Public Sub RefreshDGrid(ByVal dgv As DataGridView, varPortfolio As Double, varFromDate As Date, varToDate As Date)
        Dim dgvData As SqlClient.SqlDataReader
        Dim varSQL As String = "", varSQLSelect As String = "", varColSQL As String = ""

        Try
            varSQL = "Select top 1000 * from dbo.DolfinPaymentDeliver('" & varFromDate.ToString("yyyy-MM-dd") & "','" & varToDate.ToString("yyyy-MM-dd") & "'," & varPortfolio & ")"

            dgvData = Me.GetSQLData(varSQL)

            Dim dt = New DataTable
            dt.Load(dgvData)
            dgv.AutoGenerateColumns = True
            dgv.DataSource = dt

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            dgvData.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in RefreshDGrid")
        End Try
    End Sub

    Public Function SendEmail(ByVal varRecipients As String, ByVal varBody As String, ByVal varSubject As String) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentSendMail"

            Cmd.Parameters.Add("varRecipients", SqlDbType.NVarChar).Value = varRecipients
            Cmd.Parameters.Add("varBody", SqlDbType.NVarChar).Value = varBody
            Cmd.Parameters.Add("varSubject", SqlDbType.NVarChar).Value = varSubject
            Cmd.Parameters.Add("FileAttachementPath", SqlDbType.NVarChar).Value = ""

            Cmd.ExecuteNonQuery()
            AddAudit(cAuditStatusSuccess, 4, "Emails sent To: " & varRecipients & " subject: " & varSubject)
            SendEmail = True
        Catch ex As Exception
            AddAudit(cAuditStatusError, 4, Err.Description & " - error in sending email")
            SendEmail = False
        End Try
    End Function

    Public Sub EnterClientStatus(ClientID As Integer, ClientStatus As String)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = ClsIMS.GetCurrentConnection
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo.DolfinPaymentInsertClientStatus"
            Cmd.Parameters.Add("@CustID", SqlDbType.Int).Value = ClientID
            Cmd.Parameters.Add("@CustStatus", SqlDbType.Char).Value = ClientStatus
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameComplainceWatchList, Err.Description & " - error in EnterClientStatus")
        End Try
    End Sub

    Public Function CheckDrives(ByVal varFilePath As String) As Boolean
        CheckDrives = True
        If Not System.IO.Directory.Exists(varFilePath) Then
            CheckDrives = False
        Else
            Dim varDriveLetter As String = System.IO.Path.GetPathRoot(varFilePath)
            Dim allDrives() As System.IO.DriveInfo = System.IO.DriveInfo.GetDrives()

            Dim DriveReady As Boolean = False
            Dim d As System.IO.DriveInfo
            For Each d In allDrives
                If varDriveLetter = d.Name Then
                    If d.IsReady = True Then
                        DriveReady = True
                    End If
                End If
            Next

            If Not DriveReady Then
                CheckDrives = False
            End If
        End If
    End Function

    Public Function UpdateTemplate(LstUpdate As Dictionary(Of String, String)) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateTemplate"

            Cmd.Parameters.Add("NewPay", SqlDbType.Int).Value = CInt(LstUpdate.Item("NewPay"))
            Cmd.Parameters.Add("P_TemplateName", SqlDbType.NVarChar).Value = LstUpdate.Item("TemplateName")
            Cmd.Parameters.Add("P_PortfolioCode", SqlDbType.Int).Value = CInt(LstUpdate.Item("Ben Pa_PortfolioCode"))
            Cmd.Parameters.Add("P_CCYCode", SqlDbType.Int).Value = CInt(LstUpdate.Item("Ben Pa_CCYCode"))
            Cmd.Parameters.Add("P_BenAddressID", SqlDbType.Int).Value = CInt(LstUpdate.Item("Ben Pa_ID"))
            Cmd.Parameters.Add("P_BenTypeID", SqlDbType.Int).Value = LstUpdate.Item("Ben Pa_TypeID")
            Cmd.Parameters.Add("P_BenName", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Name")
            Cmd.Parameters.Add("P_BenFirstname", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Firstname")
            Cmd.Parameters.Add("P_BenMiddlename", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Middlename")
            Cmd.Parameters.Add("P_BenSurname", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Surname")
            Cmd.Parameters.Add("P_BenAddress1", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Address1")
            Cmd.Parameters.Add("P_BenAddress2", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Address2")
            Cmd.Parameters.Add("P_BenCounty", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_County")
            Cmd.Parameters.Add("P_BenZip", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Zip")
            Cmd.Parameters.Add("P_BenCountryID", SqlDbType.Int).Value = LstUpdate.Item("Ben Pa_CountryID")
            Cmd.Parameters.Add("P_BenSwift", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Swift")
            Cmd.Parameters.Add("P_BenIBAN", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_IBAN")
            Cmd.Parameters.Add("P_BenOther", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_Other")
            Cmd.Parameters.Add("P_BenBIK", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_BIK")
            Cmd.Parameters.Add("P_BenINN", SqlDbType.NVarChar).Value = LstUpdate.Item("Ben Pa_INN")
            Cmd.Parameters.Add("P_BenRelationshipID", SqlDbType.Int).Value = LstUpdate.Item("Ben Pa_RelationshipID")
            Cmd.Parameters.Add("P_BenSubBankAddressID", SqlDbType.Int).Value = CInt(LstUpdate.Item("SubBank Pa_ID"))
            Cmd.Parameters.Add("P_BenSubBankName", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Name")
            Cmd.Parameters.Add("P_BenSubBankAddress1", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Address1")
            Cmd.Parameters.Add("P_BenSubBankAddress2", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Address2")
            Cmd.Parameters.Add("P_BenSubBankZip", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Zip")
            Cmd.Parameters.Add("P_BenSubBankCountryID", SqlDbType.Int).Value = LstUpdate.Item("SubBank Pa_CountryID")
            Cmd.Parameters.Add("P_BenSubBankSwift", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Swift")
            Cmd.Parameters.Add("P_BenSubBankIBAN", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_IBAN")
            Cmd.Parameters.Add("P_BenSubBankOther", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_Other")
            Cmd.Parameters.Add("P_BenSubBankBIK", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_BIK")
            Cmd.Parameters.Add("P_BenSubBankINN", SqlDbType.NVarChar).Value = LstUpdate.Item("SubBank Pa_INN")
            Cmd.Parameters.Add("P_BenBankAddressID", SqlDbType.Int).Value = CInt(LstUpdate.Item("Bank Pa_ID"))
            Cmd.Parameters.Add("P_BenBankName", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Name")
            Cmd.Parameters.Add("P_BenBankAddress1", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Address1")
            Cmd.Parameters.Add("P_BenBankAddress2", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Address2")
            Cmd.Parameters.Add("P_BenBankZip", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Zip")
            Cmd.Parameters.Add("P_BenBankCountryID", SqlDbType.Int).Value = LstUpdate.Item("Bank Pa_CountryID")
            Cmd.Parameters.Add("P_BenBankSwift", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Swift")
            Cmd.Parameters.Add("P_BenBankIBAN", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_IBAN")
            Cmd.Parameters.Add("P_BenBankOther", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_Other")
            Cmd.Parameters.Add("P_BenBankBIK", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_BIK")
            Cmd.Parameters.Add("P_BenBankINN", SqlDbType.NVarChar).Value = LstUpdate.Item("Bank Pa_INN")

            Cmd.ExecuteNonQuery()
            AddAudit(cAuditStatusSuccess, cAuditGroupNameTemplates, "Template Updated: " & LstUpdate.Item("TemplateName"))
            UpdateTemplate = True
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameTemplates, Err.Description & " - error in UpdateTemplate: " & LstUpdate.Item("TemplateName"))
            UpdateTemplate = False
        End Try
    End Function

    Public Function SendPaymentSlackMessage(ByVal varID As Double) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateSlackPayment"
            Cmd.Parameters.Add("varID", SqlDbType.Int).Value = varID

            Cmd.ExecuteNonQuery()
            SendPaymentSlackMessage = True
        Catch ex As Exception
            SendPaymentSlackMessage = False
        End Try
    End Function

    Public Function ValidAccountFound(ByVal varPortfolioCode As Integer, ByVal varCCY As String) As Boolean
        ValidAccountFound = False
        Dim ValidAccount = GetSQLItem("select T_Code from vwDolfinPaymentSelectAccount where PF_Code = " & varPortfolioCode & " and CR_Name1 = '" & varCCY & "' and (B_shortcut1 like '%LLOYDS%' or B_Shortcut1 = 'LLOYDS')")
        If ValidAccount <> "" Then
            ValidAccountFound = True
        End If
    End Function

    Public Function ConfirmReceipt(ByVal varID As Double, ByVal varBeneficiaryName As String, ByVal varPortfolioCode As Integer, ByVal varTradeDate As Date,
                                   ByVal varSettleDate As Date, ByVal varAmount As Double, ByVal varCCY As String) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentCreateConfirmReceipt"
            Cmd.Parameters.Add("P_SRID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("P_BeneficiaryName", SqlDbType.NVarChar).Value = varBeneficiaryName
            Cmd.Parameters.Add("P_PortfolioCode", SqlDbType.Int).Value = varPortfolioCode
            Cmd.Parameters.Add("P_TradeDate", SqlDbType.SmallDateTime).Value = varTradeDate
            Cmd.Parameters.Add("P_SettleDate", SqlDbType.SmallDateTime).Value = varSettleDate
            Cmd.Parameters.Add("P_Amount", SqlDbType.Money).Value = varAmount
            Cmd.Parameters.Add("P_CCY", SqlDbType.NVarChar).Value = varCCY
            Cmd.Parameters.Add("P_Username", SqlDbType.NVarChar).Value = varUserName

            Cmd.ExecuteNonQuery()
            ConfirmReceipt = True
        Catch ex As Exception
            ConfirmReceipt = False
        End Try
    End Function

    Public Function SaveGeneratedPassword(ByVal VarNew As Boolean, ByVal varCustID As Double, ByVal varPassword As String, ByVal varQ1 As String, ByVal varA1 As String, ByVal varQ2 As String, ByVal varA2 As String, ByVal varQ3 As String, ByVal varA3 As String,
                                          ByVal varVolopaToken As Double, ByVal varVolopaTier As Double, ByVal varStartDate As Date, ByVal varChkAnnualFee As Boolean, ByVal varAnnualFee As Double) As Boolean
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdatePasswords"
            Cmd.Parameters.Add("@varNew", SqlDbType.Int).Value = VarNew
            Cmd.Parameters.Add("@Cust_id", SqlDbType.Int).Value = varCustID
            Cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = varPassword
            Cmd.Parameters.Add("@Q1", SqlDbType.NVarChar).Value = varQ1
            Cmd.Parameters.Add("@A1", SqlDbType.NVarChar).Value = varA1
            Cmd.Parameters.Add("@Q2", SqlDbType.NVarChar).Value = varQ2
            Cmd.Parameters.Add("@A2", SqlDbType.NVarChar).Value = varA2
            Cmd.Parameters.Add("@Q3", SqlDbType.NVarChar).Value = varQ3
            Cmd.Parameters.Add("@A3", SqlDbType.NVarChar).Value = varA3
            Cmd.Parameters.Add("@VolopaToken", SqlDbType.BigInt).Value = varVolopaToken
            Cmd.Parameters.Add("@VolopaTier", SqlDbType.Int).Value = varVolopaTier
            Cmd.Parameters.Add("@VolopaStartDate", SqlDbType.SmallDateTime).Value = varStartDate
            Cmd.Parameters.Add("@VolopaAnnualFee", SqlDbType.Money).Value = IIf(varChkAnnualFee, varAnnualFee, DBNull.Value)
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = ClsIMS.FullUserName
            Cmd.ExecuteNonQuery()
            SaveGeneratedPassword = True
        Catch ex As Exception
            SaveGeneratedPassword = False
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in SaveGeneratedPassword")
        End Try
    End Function

    Public Sub BankIMSRecManualMatch(ByVal MatchOption As Integer, ByVal TblList As DataTable)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentBankIMSRecManualMatch"
            Cmd.Parameters.Add("@MatchOpt", SqlDbType.Int).Value = MatchOption
            Cmd.Parameters.Add("@UDTList", SqlDbType.Structured).Value = TblList
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = ClsIMS.FullUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in BankIMSRecManualMatch")
        End Try
    End Sub

    Public Function getPaymentFilePath(ByVal varSelectPaymentID As Double) As String
        getPaymentFilePath = GetSQLItem("Select P_PaymentFilePath from vwdolfinpayment where P_ID = " & varSelectPaymentID)
    End Function

    Public Function IsIMSRefValid(ByVal IMSRef As String) As Boolean
        IsIMSRefValid = True

        If Len(IMSRef) <= 10 Then
            Dim varIMSRef As String = GetSQLItem("Select tr_docNo from transactions where tr_docNo = '" & IMSRef & "' group by tr_docNo")
            If varIMSRef = "" Then
                IsIMSRefValid = False
            End If
        End If
    End Function

    Public Function GetCutOff(ByVal varOrderBrokerCode As Double, varCCYCode As Double) As String
        ClsPay.CutOffTime = ""
        ClsPay.CutOffTimeZone = ""
        ClsPay.CutOffTimeGMT = ""
        GetCutOff = ""

        Dim varGetCutOffTime As String = GetSQLItem("select co_cutofftime + ',' + co_cutofftimezone from vwDolfinPaymentCutOffTimes where co_brokercode = " & varOrderBrokerCode & " and CO_CCYCode = " & varCCYCode)

        If varGetCutOffTime <> "" Then
            If IsDate(Left(varGetCutOffTime, InStr(1, varGetCutOffTime, ",", vbTextCompare) - 1)) Then
                ClsPay.CutOffTime = Left(varGetCutOffTime, InStr(1, varGetCutOffTime, ",", vbTextCompare) - 1)
            End If
            ClsPay.CutOffTimeZone = Right(varGetCutOffTime, Len(varGetCutOffTime) - InStr(1, varGetCutOffTime, ",", vbTextCompare))
            ClsPay.CutOffTimeGMT = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(CDate(Now.Date & " " & ClsPay.CutOffTime), ClsPay.CutOffTimeZone, "GMT Standard Time").ToString("HH:mm")
            GetCutOff = ClsPay.CutOffTimeGMT
        End If
    End Function

    Public Function GetIDFromDB(ByVal varTable As String) As Double
        GetIDFromDB = 0
        Dim GetIDStr As String = GetSQLFunction("select IDENT_CURRENT('" & varTable & "')")

        If IsNumeric(GetIDStr) Then
            ClsPay.SelectedPaymentID = CDbl(GetIDStr)
            GetIDFromDB = CDbl(GetIDStr)
        End If
    End Function

    Public Function GetLatestRefFromDB() As String
        GetLatestRefFromDB = ""
        Dim GetLatestRefFromDBStr As String = GetSQLFunction("select dbo.[DolfinPaymentGetLatestRef]()")

        If Not GetLatestRefFromDBStr Is Nothing Then
            GetLatestRefFromDB = GetLatestRefFromDBStr
        End If
    End Function

    Public Sub GetExchangeRate(ByVal CCYPair As String)
        ClsPay.ExchangeRate = 1
        Dim varExchRate As String = GetSQLItem("select top 1 ER_Rate from DolfinPaymentExchangeRates where ER_Pair in ('" & CCYPair & "') And ER_Date = (select max(ER_Date) from DolfinPaymentExchangeRates)")
        If IsNumeric(varExchRate) Then
            ClsPay.ExchangeRate = varExchRate
        End If
    End Sub

    Public Function GetTAStatus(ByVal varFileName As String, ByRef varErrors As Integer, ByRef varErrorDesc As String) As Boolean
        Dim varSQL As String = ""
        GetTAStatus = False
        varFileName = Replace(varFileName, IO.Path.GetExtension(varFileName), "")
        varSQL = "Select top 1 * from vwDolfinPaymentIFTErrorStatus where Errors = 1 and charindex('" & varFileName & "',FileName)<>0"
        If Not GetIFTStatus(varSQL, varErrors, varErrorDesc) Then
            varErrors = 0 : varErrorDesc = ""
            varSQL = "Select top 1 * from vwDolfinPaymentIFTImportStatus where Errors = 1 and charindex('" & varFileName & "',FileName)<>0"
            If Not GetIFTStatus(varSQL, varErrors, varErrorDesc) Then
                varErrors = 0 : varErrorDesc = ""
                varSQL = "Select top 1 * from vwDolfinPaymentIFTImportStatus where charindex('" & varFileName & "',FileName)<>0"
                If GetIFTStatus(varSQL, varErrors, varErrorDesc) Then
                    GetTAStatus = True
                    MDIParent.UpdateStatusBar("Status")
                Else
                    MDIParent.UpdateStatusBar("Awaiting IFT import status")
                End If
            End If
        End If
    End Function

    Private Function GetIFTStatus(ByVal varSQL As String, ByRef varErrors As Integer, ByRef varErrorDesc As String) As Boolean
        On Error Resume Next
        GetIFTStatus = False
        Dim reader As SqlClient.SqlDataReader
        reader = GetSQLData(varSQL)
        If reader.HasRows Then
            reader.Read()
            varErrors = IIf(IsNumeric(reader("Errors")), CInt(reader("Errors")), 0)
            varErrorDesc = reader("ErrDesc").ToString
            GetIFTStatus = True
        End If
        reader.Close()
    End Function

    Public Function CanUser(ByVal varUsername As String, ByVal varPermissionName As String) As Boolean
        Dim reader As SqlClient.SqlDataReader = Nothing

        CanUser = False
        Try
            reader = GetSQLData("select U_UserName from vwDolfinPaymentUsers where lower(U_Username) = '" & LCase(varUsername) & "' and " & varPermissionName & " = 1")

            If reader.HasRows Then
                CanUser = True
            End If
        Catch ex As Exception
        Finally
            If Not reader Is Nothing Then
                reader.Close()
            End If
        End Try
    End Function

    Public Sub ResetUserLogin(ByVal varUsername As String, ByVal varPassword As String)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUserAdmin"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = 1
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUsername
            Cmd.Parameters.Add("@Password", SqlDbType.NVarChar).Value = varPassword
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Error on ResetUserLogin for " & varUsername)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameUsers, "ResetUserLogin for " & varUsername)
        End Try
    End Sub

    Public Function CheckCanUserLogin(ByVal varUsername As String, ByVal varPassword As String) As Integer
        Dim Reader As SqlDataReader = GetSQLData("exec DolfinPaymentUserAdmin 2,'" & varUsername & "','" & varPassword & "'")
        CheckCanUserLogin = 0
        varUserCanLogin = False
        If Reader.HasRows Then
            Reader.Read()
            If Reader("response") = "nodbuser" Then
                CheckCanUserLogin = 1
            ElseIf Reader("response") = "nouser" Then
                CheckCanUserLogin = 2
            ElseIf Reader("response") = "change" Then
                CheckCanUserLogin = 3
            ElseIf Reader("response") = "wrong" Then
                CheckCanUserLogin = 4
            Else
                CheckCanUserLogin = 5
                varUserCanLogin = True
            End If
        End If
    End Function

    Public Function PaymentHasFee(ByVal varSelectedPaymentID As Integer, Optional ByRef FeeID As Double = 0) As Boolean
        PaymentHasFee = False
        Dim varFeeID = GetSQLItem("Select P_PortfolioFeeID From vwDolfinPaymentGridView p1 Where p_id = " & varSelectedPaymentID & " And (isnull(p1.P_PortfolioFeeID, 0) <> 0 and exists(Select P_PortfolioFeeID from vwDolfinPaymentGridView p2 where p2.p_id = p1.P_PortfolioFeeID And P_Status<>'" & GetStatusType(cStatusCancelled) & "'))")
        If varFeeID <> "" Then
            PaymentHasFee = True
            FeeID = varFeeID
        End If
    End Function

    Public Sub UpdatePaymentSwiftDetails(ByRef varID As String, ByRef varFileType As String, ByRef varStatus As Integer, ByRef varErrorCode As String, ByRef varFileName As String)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateSwift"
            Cmd.Parameters.Add("@ID", SqlDbType.NVarChar).Value = varID
            Cmd.Parameters.Add("@varFileType", SqlDbType.NVarChar).Value = varFileType
            Cmd.Parameters.Add("@varStatus", SqlDbType.Int).Value = varStatus
            Cmd.Parameters.Add("@varErrorCode", SqlDbType.NVarChar).Value = Strings.Left(varErrorCode, 200)
            Cmd.Parameters.Add("@varFileName", SqlDbType.NVarChar).Value = varFileName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on UpdatePaymentSwiftDetails:" & ",Movement ID: " & varID & ", Varstatus: " & varStatus & ", Errorcode: " & varErrorCode)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdatePaymentSwiftDetailsRD(ByRef varID As String, ByRef varFileType As String, ByRef varStatus As Integer, ByRef varErrorCode As String, ByRef varFileName As String, ByRef varStatusCode As String, ByRef varReasonCode As String)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateSwiftRD"
            Cmd.Parameters.Add("@ID", SqlDbType.NVarChar).Value = varID
            Cmd.Parameters.Add("@varFileType", SqlDbType.NVarChar).Value = varFileType
            Cmd.Parameters.Add("@varStatus", SqlDbType.Int).Value = varStatus
            Cmd.Parameters.Add("@varErrorCode", SqlDbType.NVarChar).Value = Strings.Left(varErrorCode, 200)
            Cmd.Parameters.Add("@varFileName", SqlDbType.NVarChar).Value = varFileName
            Cmd.Parameters.Add("@varStatusCode", SqlDbType.NVarChar).Value = varStatusCode
            Cmd.Parameters.Add("@varReasonCode", SqlDbType.NVarChar).Value = varReasonCode
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on UpdatePaymentSwiftDetailsRD:" & ",Movement ID: " & varID & ", Varstatus: " & varStatus & ", Errorcode: " & varErrorCode)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdatePaymentSwiftDetailsAuto(ByRef varID As String, ByRef varFileType As String, ByRef varStatus As Integer, ByRef varErrorCode As String, ByRef varFileName As String)
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateSwiftAuto"
            Cmd.Parameters.Add("@ID", SqlDbType.NVarChar).Value = varID
            Cmd.Parameters.Add("@varFileType", SqlDbType.NVarChar).Value = varFileType
            Cmd.Parameters.Add("@varStatus", SqlDbType.Int).Value = varStatus
            Cmd.Parameters.Add("@varErrorCode", SqlDbType.NVarChar).Value = Strings.Left(varErrorCode, 200)
            Cmd.Parameters.Add("@varFileName", SqlDbType.NVarChar).Value = varFileName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on UpdatePaymentSwiftDetailsRD:" & ",Movement ID: " & varID & ", Varstatus: " & varStatus & ", Errorcode: " & varErrorCode)
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdateMovementStatus()
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateStatus"
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on UpdateMovementStatus")
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub UpdateMovementStatusRD()
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateStatusRD"
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error on UpdateMovementStatusRD")
        Finally
            Cmd = Nothing
        End Try
    End Sub

    Public Sub DeleteSwiftFile()
        Dim varFilename As String = GetSQLItem("Select P_SwiftFile from vwDolfinPayment where P_ID = " & ClsPay.SelectedPaymentID)
        If varFilename <> "" Then
            If IO.File.Exists(GetFilePath("SWIFTExport") & varFilename) Then
                IO.File.Delete(GetFilePath("SWIFTExport") & varFilename)
            End If
        End If
    End Sub

    Public Sub ActionMovement(ByVal varOpt As Integer, ByVal varID As Double, ByVal varSendSwift As Boolean, ByVal varSendIMS As Boolean, ByVal varAuthorisedUserName As String, ByVal varDesc As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAction"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Swift", SqlDbType.Bit).Value = varSendSwift
            Cmd.Parameters.Add("@IMS", SqlDbType.Bit).Value = varSendIMS
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Error on ActionMovement:" & varOpt & ",Movement ID:" & varID & " " & varDesc, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "ActionMovement:" & varOpt & ",Movement ID:" & varID & ",sendswift:" & CStr(varSendSwift) & ",sendims:" & CStr(varSendIMS), varID)
        End Try
    End Sub

    Public Sub ActionMovementRD(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentActionRD"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - Error on ActionMovementRD:" & varOpt & ",Movement ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "ActionMovementRD:" & varOpt & ",Movement ID:" & varID, varID)
        End Try
    End Sub

    Public Sub ActionMovementFS(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentActionFS"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on ActionMovementFS:" & varOpt & ",Movement ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "ActionMovementFS:" & varOpt & ",Movement ID:" & varID, varID)
        End Try
    End Sub

    Public Sub ActionMovementPR(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String, ByVal varDesc As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentActionPR"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on ActionMovementPR:" & varOpt & ",Movement ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "ActionMovementPR:" & varOpt & ",Movement ID:" & varID & " " & varDesc, varID)
        End Try
    End Sub

    Public Sub ActionMovementCA(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentActionCA"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on ActionMovementCA:" & varOpt & ",Movement ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameFees, "ActionMovementCA:" & varOpt & ",Movement ID:" & varID, varID)
        End Try
    End Sub

    Public Sub ActionMovementCR(ByVal varOpt As Integer, ByVal varID As Double, ByVal varAuthorisedUserName As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentActionCR"
            Cmd.Parameters.Add("@Opt", SqlDbType.Int).Value = varOpt
            Cmd.Parameters.Add("@ID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varAuthorisedUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameConfirmReceipts, Err.Description & " - Error on ActionMovementCR:" & varOpt & ",Movement ID:" & varID, varID)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameConfirmReceipts, "ActionMovementCR:" & varOpt & ",Movement ID:" & varID, varID)
        End Try
    End Sub

    Public Sub AddFeeHistoryPayments(ByVal varFeeID As Integer, ByVal varPaymentID As Integer, ByVal varAmount As Double, ByVal varSource As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUpdateFeeHistoryPayments"
            Cmd.Parameters.Add("@Fee_ID", SqlDbType.Int).Value = varFeeID
            Cmd.Parameters.Add("@FeePay_PaymentID", SqlDbType.Int).Value = varPaymentID
            Cmd.Parameters.Add("@FeePay_Amount", SqlDbType.Money).Value = varAmount
            Cmd.Parameters.Add("@FeePay_Source", SqlDbType.NVarChar).Value = varSource
            Cmd.Parameters.Add("@FeePay_UpdateBy", SqlDbType.NVarChar).Value = varUserName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Error on AddFeeHistoryPayments - varFeeID:" & varFeeID, varFeeID)
        End Try
    End Sub

    Public Sub AddAudit(ByVal varAuditStatus As Integer, ByVal varAuditGroupName As Integer, ByVal varAuditdesc As String, Optional ByVal varAuditTransactionID As Double = -1)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentAddAuditToLog"
            Cmd.Parameters.Add("@varStatus", SqlDbType.Int).Value = varAuditStatus
            Cmd.Parameters.Add("@varGroupName", SqlDbType.Int).Value = varAuditGroupName
            Cmd.Parameters.Add("@Id", SqlDbType.Int).Value = varAuditTransactionID
            Cmd.Parameters.Add("@varDesc", SqlDbType.NVarChar).Value = varAuditdesc
            Cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.Parameters.Add("@UserMachine", SqlDbType.NVarChar).Value = varUserMachine
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
        End Try
    End Sub

    Public Function FeeIncurred() As String
        FeeIncurred = ""
        ClsPay.PortfolioFee = 0
        ClsPay.PortfolioFeeCCY = ""
        ClsPay.PortfolioFeeInt = 0
        ClsPay.PortfolioFeeCCYInt = ""
        ClsPay.PortfolioFeeDue = False

        If ClsPay.PaymentType = GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Or ClsPay.PaymentType = GetPaymentType(cPayDescExClientIn) Then
            If ClsPay.CCY = "GBP" Then
                Dim varFee As String = GetSQLItem("select convert(nvarchar(20),TF_Fee) + ',' + TF_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & ClsPay.PortfolioCode)
                If varFee <> "" Then
                    FeeIncurred = Replace(varFee, ",", " ")
                    ClsPay.PortfolioFeeDue = True
                    If IsNumeric(Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1)) Then
                        ClsPay.PortfolioFee = CInt(Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1))
                    End If
                    ClsPay.PortfolioFeeCCY = Right(varFee, Len(varFee) - InStr(1, varFee, ",", vbTextCompare))
                End If
            Else
                Dim varIntFee As String = GetSQLItem("select convert(nvarchar(20),TF_Int_Fee) + ',' + TF_Int_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & ClsPay.PortfolioCode)
                If varIntFee <> "" Then
                    ClsPay.PortfolioFeeDue = True
                    If IsNumeric(Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1)) Then
                        ClsPay.PortfolioFeeInt = CInt(Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1))
                    End If
                    ClsPay.PortfolioFeeCCYInt = Right(varIntFee, Len(varIntFee) - InStr(1, varIntFee, ",", vbTextCompare))
                End If
            End If
        End If
    End Function

    Public Function UpdateSwiftStatus(ByVal CheckSwiftAck As Boolean, ByVal CheckSwiftPaid As Boolean, Optional ByVal SwiftCheckDays As Integer = 0) As Boolean
        UpdateSwiftStatus = True
        If CheckSwiftAck Then
            ReadSwift(0, GetFilePath("SWIFTImport"), SwiftCheckDays)
            ReadSwift(1, GetFilePath("SWIFTError"), SwiftCheckDays)
        End If
        If CheckSwiftPaid Then
            ReadSwift(0, GetFilePath("SWIFTProcessed"), SwiftCheckDays)
        End If
    End Function

    Public Function getSearchInterval() As Integer
        getSearchInterval = IIf(Now.DayOfWeek = 3, 5, 3)
    End Function

    Public Function getSearchString(ByVal DaysSearch As Integer) As String
        Dim SearchString As String = "*_"

        If DaysSearch = 4 Then
            DaysSearch = 5
        End If

        If Now.Year = DateAdd(DateInterval.Day, -DaysSearch, Now).Year Then
            SearchString = SearchString & Now.ToString("yyyy")
        Else
            SearchString = SearchString & DateAdd(DateInterval.Day, -DaysSearch, Now).ToString("yyyy")
        End If

        If Now.Month = DateAdd(DateInterval.Day, -DaysSearch, Now).Month Then
            SearchString = SearchString & Now.ToString("MM")
        End If

        If DaysSearch = 0 Then
            SearchString = SearchString & Now.ToString("dd")
        End If

        getSearchString = SearchString & "*.fin"
    End Function

    Public Sub ProcessSwiftPaymentStatus(ByVal varType As Integer, ByVal SwiftFinFile As String, ByVal varID As String, ByVal varStatus As Integer, ByVal varFileType As String,
                                         ByVal varErrorCode As String, ByVal varStatusCode As String, ByVal varReasonCode As String, ByRef UpdatedID As Boolean)
        If varType = 0 And varID <> "" And varStatus <> 0 And Not varMessageTypesRD.ContainsValue("MT" & varFileType) Then
            If varStatus = 2 Then
                varErrorCode = varErrorCode & ", filename=" & Dir(SwiftFinFile)
            End If
            UpdatePaymentSwiftDetails(varID, varFileType, varStatus, varErrorCode, Dir(SwiftFinFile))
            UpdatedID = True
        ElseIf varType = 1 And varID <> "" And Not varMessageTypesRD.ContainsValue("MT" & varFileType) Then
            varErrorCode = GetSwiftError(Dir(SwiftFinFile))
            UpdatePaymentSwiftDetails(varID, varFileType, 3, varErrorCode, Dir(SwiftFinFile))
            UpdatedID = True
        End If
    End Sub

    Public Sub ReadSwift(ByVal varType As Integer, ByVal varFilePath As String, Optional ByVal SwiftCheckDays As Integer = 0)
        Dim varStatus As Integer = 0
        Dim vLine As String = "", varErrorCode As String = "", varID As String = "", varStatusCode As String = "", varReasonCode As String = ""

        Try
            For Each SwiftFinFile As String In System.IO.Directory.GetFiles(varFilePath, getSearchString(SwiftCheckDays))
                Dim SwiftFinFileDate As Date = IO.File.GetLastWriteTime(SwiftFinFile)
                If DateDiff("d", SwiftFinFileDate.Date, Now.Date) <= SwiftCheckDays Then
                    'If SwiftFinFile.Contains("DH.fin") Then
                    varID = ""
                    varStatus = 0
                    varErrorCode = ""
                    varStatusCode = ""
                    varReasonCode = ""
                    Dim objSwiftReader As IO.StreamReader = GetSwiftFile(SwiftFinFile)
                    If Not objSwiftReader Is Nothing Then
                        If objSwiftReader.Peek() >= 0 Then
                            vLine = objSwiftReader.ReadLine()
                            Dim vHeaderLine As String = vLine
                            Dim varFileType As String = GetFileTypeFromSwift(vLine)
                            If varMessageTypes.ContainsValue("MT" & varFileType) Then
                                Dim UpdatedID As Boolean = False

                                Do While objSwiftReader.Peek() >= 0
                                    If InStr(1, vLine, "-}", vbTextCompare) <> 0 Then
                                        Call ProcessSwiftPaymentStatus(varType, SwiftFinFile, varID, varStatus, varFileType, varErrorCode, varStatusCode, varReasonCode, UpdatedID)
                                        varID = ""
                                        varStatus = 0
                                        varErrorCode = ""
                                        varStatusCode = ""
                                        varReasonCode = ""
                                        UpdatedID = False
                                        vHeaderLine = vLine
                                        varFileType = GetFileTypeFromSwift(vLine)
                                    End If

                                    If Not UpdatedID Then
                                        If varID = "" Then
                                            varID = GetIDFromSwift(varID, vLine)
                                        End If

                                        If varID <> "" Then
                                            GetStatusFromSwift(varStatus, varErrorCode, vHeaderLine)
                                        End If

                                        If varMessageTypesRD.ContainsValue("MT" & varFileType) Then
                                            GetStatusReasonCodeFromSwift(varStatusCode, varReasonCode, vLine)
                                        End If

                                        Call ProcessSwiftPaymentStatus(varType, SwiftFinFile, varID, varStatus, varFileType, varErrorCode, varStatusCode, varReasonCode, UpdatedID)

                                    End If
                                    vLine = objSwiftReader.ReadLine()
                                Loop

                                If InStr(1, vLine, "-}", vbTextCompare) <> 0 Then
                                    Call ProcessSwiftPaymentStatus(varType, SwiftFinFile, varID, varStatus, varFileType, varErrorCode, varStatusCode, varReasonCode, UpdatedID)
                                End If
                            End If
                        End If
                    End If
                    objSwiftReader = Nothing
                    'End If
                End If

                'System.IO.File.Move(SwiftFinFile, GetFilePath("SWIFTProcessed") & Dir(SwiftFinFile))
            Next
        Catch ex As Exception
            'Ignore all other application threads trying to run this process. Only one process of the file required
        End Try
    End Sub


    Private Function GetSwiftError(ByVal SwiftFinFile As String) As String
        GetSwiftError = ""
        Dim vLine As String = "", varErrorCode As String = "", varID As String = ""

        For Each SwiftFinErrorFile As String In System.IO.Directory.GetFiles(GetFilePath("SWIFTError"), Left(SwiftFinFile, InStr(SwiftFinFile, ".", vbTextCompare) - 1) & "*.fin.err")
            Dim SwiftErrorSearch = Left(Dir(SwiftFinErrorFile), Len(Dir(SwiftFinErrorFile)) - 15)

            If InStr(1, SwiftFinFile, SwiftErrorSearch, vbTextCompare) <> 0 Then
                Dim objSwiftErrorReader As IO.StreamReader = GetSwiftFile(SwiftFinErrorFile)
                If Not objSwiftErrorReader Is Nothing Then
                    If objSwiftErrorReader.Peek() >= 0 Then
                        GetSwiftError = "Swift Error: " & objSwiftErrorReader.ReadLine()
                        Exit For
                    End If
                End If
                objSwiftErrorReader.Close()
            End If
        Next
    End Function

    Private Function ValidID(ByVal VarID As String) As Boolean
        ValidID = False

        If System.Text.RegularExpressions.Regex.IsMatch(Left(UCase(VarID), 6), "^[A-Za-z]+$") Then
            If IsNumeric(Right(VarID, 7)) Then
                ValidID = True
            End If
        End If
    End Function

    Private Function GetSwiftFile(ByVal SwiftFinFile As String) As IO.StreamReader
        GetSwiftFile = Nothing
        Try
            Dim swiftstream As New IO.FileStream(SwiftFinFile, IO.FileMode.Open, IO.FileAccess.Read)
            GetSwiftFile = New IO.StreamReader(swiftstream)
        Catch ex As Exception
            GetSwiftFile = Nothing
        End Try
    End Function

    Public Function GetIDFromSwift(ByRef varID As String, ByRef vLine As String) As String
        Dim varIDStart As String
        GetIDFromSwift = ""
        varIDStart = InStr(1, vLine, cSwiftFileId, vbTextCompare)
        If varIDStart <> 0 Then
            varID = GetMessageBlock(varIDStart, vLine)
            varID = Replace(Replace(varID, cSwiftFileId, ""), "}", "")
            If ValidID(varID) Then
                GetIDFromSwift = varID
            End If
        Else
            varIDStart = Left(vLine, 4)
            If varIDStart = cSwiftFileId2 Then
                varID = Replace(Replace(vLine, cSwiftFileId2, ""), "}", "")
                If ValidID(varID) Then
                    GetIDFromSwift = varID
                End If
            Else
                varIDStart = InStr(1, vLine, cSwiftFileId3, vbTextCompare)
                If varIDStart <> 0 Then
                    varID = Right(Strings.Left(vLine, InStr(1, vLine, cSwiftFileId3, vbTextCompare) - 1), 13)
                    If ValidID(varID) Then
                        GetIDFromSwift = varID
                    End If
                Else
                    varIDStart = InStr(1, vLine, cSwiftFileId4, vbTextCompare)
                    If varIDStart <> 0 Then
                        varID = Replace(Replace(vLine, cSwiftFileId4, ""), "}", "")
                        If ValidID(varID) Then
                            GetIDFromSwift = varID
                        End If
                    End If
                End If
            End If
        End If
    End Function

    Public Sub GetStatusFromSwift(ByRef varStatus As Integer, ByRef varErrorCode As String, ByRef vLine As String)
        If InStr(1, vLine, cSwiftFileAck, vbTextCompare) <> 0 Then
            varStatus = 1
        ElseIf InStr(1, vLine, cSwiftFileNoAck, vbTextCompare) <> 0 Then
            varStatus = 2
            Dim varErrorStart = InStr(1, vLine, cSwiftFileError, vbTextCompare)
            If varErrorStart <> 0 Then
                varErrorCode = "Swift Error: " & GetMessageBlock(varErrorStart, vLine)
            End If
        End If
    End Sub

    Public Sub GetStatusReasonCodeFromSwift(ByRef varStatusCode As String, ByRef varReasonCode As String, ByRef vLine As String)
        If InStr(1, vLine, cSwiftStatusCode, vbTextCompare) <> 0 Then
            varStatusCode = Replace(vLine, cSwiftStatusCode, "")
        End If
        If InStr(1, vLine, cSwiftReasonCode, vbTextCompare) <> 0 Then
            varReasonCode = Replace(vLine, cSwiftReasonCode, "")
        End If
    End Sub

    Public Function GetFileTypeFromSwiftold(ByRef vLine As String) As String
        GetFileTypeFromSwiftold = ""
        Dim varPos = InStr(1, vLine, cSwiftFileType, vbTextCompare)
        If varPos <> 0 Then
            GetFileTypeFromSwiftold = Right(Replace(UCase(Mid(vLine, varPos, 7)), cSwiftFileType, ""), 3)
        End If
    End Function

    Private Function GetFileTypeFromSwift(ByRef vLine As String) As String
        Dim varPos As Integer
        GetFileTypeFromSwift = ""
        varPos = InStr(1, vLine, "{2:I", vbTextCompare)
        If varPos <> 0 Then
            GetFileTypeFromSwift = Right(Replace(UCase(Mid(vLine, varPos, 7)), "{2:I", ""), 3)
        Else
            varPos = InStr(1, vLine, "{2:O", vbTextCompare)
            If varPos <> 0 Then
                GetFileTypeFromSwift = Right(Replace(UCase(Mid(vLine, varPos, 7)), "{2:O", ""), 3)
            End If
        End If
    End Function


    Private Function GetMessageBlock(ByVal varStart As Integer, ByVal vLine As String) As String
        GetMessageBlock = ""
        If Mid(vLine, varStart, 1) = "{" Then
            For i As Integer = varStart To Len(vLine)
                If Mid(vLine, i, 1) = "}" Then
                    GetMessageBlock = GetMessageBlock & "}"
                    Exit For
                ElseIf Mid(vLine, i, 1) <> "}" Then
                    GetMessageBlock = GetMessageBlock & Mid(vLine, i, 1)
                End If
            Next
        End If
    End Function

    Private Sub UserAdmin(ByVal varOption As Integer, ByVal varPassword As String)
        Dim Cmd As New SqlCommand

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentUserAdmin"

            Cmd.Parameters.Add("Opt", SqlDbType.Int).Value = varOption
            Cmd.Parameters.Add("Username", SqlDbType.NVarChar).Value = varUserName
            Cmd.Parameters.Add("Password", SqlDbType.NVarChar).Value = varPassword
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, "UserAdmin", Err.Description & " - error in UserAdmin")
        End Try
    End Sub

    Public Function RunSQLJob(ByVal varJobName As String) As Boolean
        Dim Cmd As New SqlCommand
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentRunJob"
            Cmd.Parameters.Add("@varJobName", SqlDbType.NVarChar).Value = varJobName
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on RunSQLJob for " & varJobName)
        Finally
            Cmd = Nothing
            AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "RunSQLJob for " & varJobName)
        End Try
        RunSQLJob = True
    End Function

    ''' <summary>
    ''' Fetch the List of File Paths.
    ''' </summary>
    ''' <returns></returns>
    Public Function FetchFilePaths() As List(Of FilePaths)

        Dim Cmd As New SqlClient.SqlCommand
        Dim listFilePaths As List(Of FilePaths) = New List(Of FilePaths)()
        Dim _reader As SqlClient.SqlDataReader

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentGetFilePathsApi"
            _reader = Cmd.ExecuteReader
            If _reader.HasRows Then
                listFilePaths = DataReaderMapToList(Of FilePaths)(_reader)
            End If

            Return listFilePaths
        Catch ex As SqlClient.SqlException
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, ex.Message & " - Error On FetchFilePaths")
        End Try

    End Function

    ''' <summary>
    ''' Convert SQl data reader to a List.
    ''' </summary>
    ''' <typeparam name="T"></typeparam>
    ''' <param name="filePaths"></param>
    ''' <returns></returns>
    Private Function DataReaderMapToList(Of T)(ByVal filePaths As IDataReader) As List(Of T)

        Dim list As List(Of T) = New List(Of T)()
        Dim obj As T = Nothing

        Try
            While filePaths.Read()
                obj = Activator.CreateInstance(Of T)()

                For Each prop As PropertyInfo In obj.[GetType]().GetProperties()
                    If Not Object.Equals(filePaths(prop.Name), DBNull.Value) Then
                        prop.SetValue(obj, filePaths(prop.Name), Nothing)
                    End If
                Next

                list.Add(obj)
            End While

            Return list
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, ex.Message & " - Error On DataReaderMapToList")
            Return Nothing
        End Try

    End Function

    Public Function GetSwiftFileData(ByRef _swiftPayload As SwiftPayloadData, ByVal _uri As String) As String

        Dim _payload As String = String.Empty
        Dim _serializer As New JavaScriptSerializer()

        GetSwiftFileData = ""
        Try
            If ClsPay.MessageType <> "" Then
                Select Case _swiftPayload.P_PaymentTypeID
                    Case 7
                        Select Case _swiftPayload.P_Other_ID
                            Case 9 'MT210
                                _uri = $"{_uri}{SwiftTypeEnum.MT210}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt210 As New MT210 With
                                   {
                                     .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                     .BenSwiftCode = _swiftPayload.P_BenSwift,
                                     .OrderRef = _swiftPayload.P_OrderRef,
                                     .RelatedOrderRef = _swiftPayload.P_BenRef,
                                     .Amount = _swiftPayload.P_Amount,
                                     .Ccy = _swiftPayload.P_BenCCY,
                                     .SettleDate = _swiftPayload.P_SettleDate,
                                     .OrderSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                     .BenSubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                     .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                     .Email = _swiftPayload.E_Email
                                   }

                                _payload = _serializer.Serialize(_mt210)

                            Case 22, 28, 41 'All n99 Swift Files
                                Select Case _swiftPayload.P_Other_ID
                                    Case 22
                                        _uri = $"{_uri}{SwiftTypeEnum.MT999}?clientId={_swiftPayload.P_ClientId}"
                                    Case 28
                                        _uri = $"{_uri}{SwiftTypeEnum.MT199}?clientId={_swiftPayload.P_ClientId}"
                                    Case 41
                                        _uri = $"{_uri}{SwiftTypeEnum.MT599}?clientId={_swiftPayload.P_ClientId}"
                                End Select

                                Dim _mt99 As New MT99 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwiftCode = _swiftPayload.P_OrderOther,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .OrderRelatedRef = _swiftPayload.P_IMSRef,
                                    .Message = _swiftPayload.P_Comments,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt99)

                            Case 24 'MT920
                                _uri = $"{_uri}{SwiftTypeEnum.MT920}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt920 As New MT920 With
                                    {
                                     .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                     .OrderSwift = _swiftPayload.P_BenSwift,
                                     .OrderRef = _swiftPayload.P_OrderRef,
                                     .OrderCcy = _swiftPayload.P_BenCCY,
                                     .OrderAccountNumber = _swiftPayload.P_BenAccountNumber,
                                     .OrderAmount = _swiftPayload.P_OrderAmount,
                                     .OrderType = "942",
                                     .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                     .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt920)

                            Case 29 'MT192
                                _uri = $"{_uri}{SwiftTypeEnum.MT192}?clientId={_swiftPayload.P_ClientId}"

                                If String.IsNullOrEmpty(FetchOtherSettleDate(_swiftPayload.P_OrderOther)) Then
                                    Dim _mt192 As New MT192 With
                                   {
                                   .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                   .OrderSwift = _swiftPayload.P_OrderOther,
                                   .OrderRef = _swiftPayload.P_OrderRef,
                                   .OrderRelatedRef = _swiftPayload.P_IMSRef,
                                   .Message = _swiftPayload.P_Comments,
                                   .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                   .Email = _swiftPayload.E_Email
                                   }
                                    _payload = _serializer.Serialize(_mt192)

                                Else
                                    Dim _mt192 As New MT192 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwift = _swiftPayload.P_OrderOther,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .OrderRelatedRef = _swiftPayload.P_IMSRef,
                                    .OrderRelatedDate = FetchOtherSettleDate(_swiftPayload.P_OrderOther),
                                    .Message = _swiftPayload.P_Comments,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .Email = _swiftPayload.E_Email
                                    }
                                    _payload = _serializer.Serialize(_mt192)

                                End If
                            Case Else

                                Select Case _swiftPayload.P_OrderOtherMessTypeID
                                    Case 75 'MT101
                                        _uri = $"{_uri}{SwiftTypeEnum.MT101}?clientId={_swiftPayload.P_ClientId}"

                                        Dim _mt101 As New MT101 With
                                        {
                                        .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                        .OrderSwift = _swiftPayload.P_OrderSwift,
                                        .OrderRef = _swiftPayload.P_OrderRef,
                                        .SettleDate = _swiftPayload.P_SettleDate,
                                        .Amount = _swiftPayload.P_Amount,
                                        .Ccy = _swiftPayload.P_CCY,
                                        .OrderIban = _swiftPayload.P_OrderIBAN,
                                        .OrderOther = _swiftPayload.P_OrderOther,
                                        .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                        .OrderName = _swiftPayload.O_OrderName,
                                        .OrderAddress = _swiftPayload.O_Address,
                                        .OrderCity = _swiftPayload.O_City,
                                        .OrderPostCode = _swiftPayload.O_PostCode,
                                        .BeneficiaryName = _swiftPayload.P_BenName,
                                        .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                        .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                        .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                        .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                        .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                        .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                        .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                        .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                        .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                        .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                        .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                        .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                        .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                        .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                        .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                        .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                        .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                        .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                        .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                        .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                        .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                        .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                        .Email = _swiftPayload.E_Email
                                        }
                                        _payload = _serializer.Serialize(_mt101)

                                    Case 0, 78 'MT103

                                        _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                        Dim _mt103 As New MT103 With
                                        {
                                        .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                        .OrderSwift = _swiftPayload.P_OrderSwift,
                                        .OrderRef = _swiftPayload.P_OrderRef,
                                        .SettleDate = _swiftPayload.P_SettleDate,
                                        .Amount = _swiftPayload.P_Amount,
                                        .Ccy = _swiftPayload.P_CCY,
                                        .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                        .OrderOther = _swiftPayload.P_OrderOther,
                                        .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                        .OrderName = _swiftPayload.O_OrderName,
                                        .OrderAddress = _swiftPayload.O_Address,
                                        .OrderCity = _swiftPayload.O_City,
                                        .OrderPostCode = _swiftPayload.O_PostCode,
                                        .BeneficiaryName = _swiftPayload.P_BenName,
                                        .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                        .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                        .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                        .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                        .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                        .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                        .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                        .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                        .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                        .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                        .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                        .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                        .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                        .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                        .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                        .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                        .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                        .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                        .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                        .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                        .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                        .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                        .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                        .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                        .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                        .Email = _swiftPayload.E_Email
                                        }
                                        _payload = _serializer.Serialize(_mt103)

                                    Case 95 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift  'MT200

                                        _uri = $"{_uri}{SwiftTypeEnum.MT200}?clientId={_swiftPayload.P_ClientId}"

                                        Dim _mt200 As New MT200 With
                                            {
                                            .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                            .OrderSwiftCode = _swiftPayload.P_OrderSwift,
                                            .OrderRef = _swiftPayload.P_OrderRef,
                                            .SettleDate = _swiftPayload.P_SettleDate,
                                            .Amount = _swiftPayload.P_Amount,
                                            .Ccy = _swiftPayload.P_CCY,
                                            .OrderIBAN = _swiftPayload.P_OrderIBAN,
                                            .BenIBAN = _swiftPayload.P_BenIBAN,
                                            .OrderOther = _swiftPayload.P_OrderOther,
                                            .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                            .Email = _swiftPayload.E_Email
                                            }
                                        _payload = _serializer.Serialize(_mt200)

                                    Case 97 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift 'MT202

                                        _uri = $"{_uri}{SwiftTypeEnum.MT202}?clientId={_swiftPayload.P_ClientId}"

                                        Dim _mt202 As New MT202 With
                                            {
                                            .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                            .OrderSwift = _swiftPayload.P_OrderSwift,
                                            .OrderRef = _swiftPayload.P_OrderRef,
                                            .SettleDate = _swiftPayload.P_SettleDate,
                                            .Amount = _swiftPayload.P_Amount,
                                            .Ccy = _swiftPayload.P_CCY,
                                            .OrderAccountNumber = _swiftPayload.P_OrderAccountNumber,
                                            .OrderIban = _swiftPayload.P_OrderIBAN,
                                            .OrderOther = _swiftPayload.P_OrderOther,
                                            .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                            .OrderName = _swiftPayload.O_OrderName,
                                            .OrderAddress = _swiftPayload.O_Address,
                                            .OrderCity = _swiftPayload.O_City,
                                            .OrderPostCode = _swiftPayload.O_PostCode,
                                            .BeneficiaryRef = _swiftPayload.P_BenRef,
                                            .BeneficiaryName = _swiftPayload.P_BenName,
                                            .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                            .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                            .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                            .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                            .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                            .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                            .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                            .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                            .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                            .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                            .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                            .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                            .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                            .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                            .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                            .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                            .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                            .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                            .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                            .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                            .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                            .BeneficiaryBankBIK = _swiftPayload.P_BenBankBIK,
                                            .BeneficiaryBankINN = _swiftPayload.P_BenBankINN,
                                            .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                            .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                            .Email = _swiftPayload.E_Email
                                            }
                                        _payload = _serializer.Serialize(_mt202)

                                    Case Else 'MT103

                                        _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                        Dim _mt103 As New MT103 With
                                        {
                                        .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                        .OrderSwift = _swiftPayload.P_OrderSwift,
                                        .OrderRef = _swiftPayload.P_OrderRef,
                                        .SettleDate = _swiftPayload.P_SettleDate,
                                        .Amount = _swiftPayload.P_Amount,
                                        .Ccy = _swiftPayload.P_CCY,
                                        .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                        .OrderOther = _swiftPayload.P_OrderOther,
                                        .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                        .OrderName = _swiftPayload.O_OrderName,
                                        .OrderAddress = _swiftPayload.O_Address,
                                        .OrderCity = _swiftPayload.O_City,
                                        .OrderPostCode = _swiftPayload.O_PostCode,
                                        .BeneficiaryName = _swiftPayload.P_BenName,
                                        .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                        .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                        .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                        .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                        .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                        .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                        .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                        .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                        .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                        .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                        .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                        .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                        .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                        .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                        .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                        .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                        .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                        .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                        .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                        .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                        .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                        .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                        .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                        .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                        .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                        .Email = _swiftPayload.E_Email
                                        }
                                        _payload = _serializer.Serialize(_mt103)

                                End Select
                        End Select
                    Case 2, 3
                        Select Case _swiftPayload.P_OrderOtherMessTypeID
                            Case 75 'MT101

                                _uri = $"{_uri}{SwiftTypeEnum.MT101}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt101 As New MT101 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIban = _swiftPayload.P_OrderIBAN,
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt101)

                            Case 0, 78 'MT103
                                _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt103 As New MT103 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt103)

                            Case 95 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift 'MT200

                                _uri = $"{_uri}{SwiftTypeEnum.MT200}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt200 As New MT200 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwiftCode = _swiftPayload.P_OrderSwift,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .SettleDate = _swiftPayload.P_SettleDate,
                                    .Amount = _swiftPayload.P_Amount,
                                    .Ccy = _swiftPayload.P_CCY,
                                    .OrderIBAN = _swiftPayload.P_OrderIBAN,
                                    .BenIBAN = _swiftPayload.P_BenIBAN,
                                    .OrderOther = _swiftPayload.P_OrderOther,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt200)

                            Case 97 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift

                                _uri = $"{_uri}{SwiftTypeEnum.MT202}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt202 As New MT202 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwift = _swiftPayload.P_OrderSwift,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .SettleDate = _swiftPayload.P_SettleDate,
                                    .Amount = _swiftPayload.P_Amount,
                                    .Ccy = _swiftPayload.P_CCY,
                                    .OrderAccountNumber = _swiftPayload.P_OrderAccountNumber,
                                    .OrderIban = _swiftPayload.P_OrderIBAN,
                                    .OrderOther = _swiftPayload.P_OrderOther,
                                    .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                    .OrderName = _swiftPayload.O_OrderName,
                                    .OrderAddress = _swiftPayload.O_Address,
                                    .OrderCity = _swiftPayload.O_City,
                                    .OrderPostCode = _swiftPayload.O_PostCode,
                                    .BeneficiaryRef = _swiftPayload.P_BenRef,
                                    .BeneficiaryName = _swiftPayload.P_BenName,
                                    .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                    .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                    .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                    .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                    .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                    .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                    .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                    .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                    .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                    .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                    .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                    .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                    .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                    .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                    .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                    .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                    .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                    .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                    .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                    .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                    .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                    .BeneficiaryBankBIK = _swiftPayload.P_BenBankBIK,
                                    .BeneficiaryBankINN = _swiftPayload.P_BenBankINN,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                    .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt202)

                            Case Else 'MT103

                                _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt103 As New MT103 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt103)
                        End Select
                    Case Else
                        Select Case _swiftPayload.P_Order3rdPartyMessTypeID
                            Case 75 'MT101

                                _uri = $"{_uri}{SwiftTypeEnum.MT101}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt101 As New MT101 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIban = _swiftPayload.P_OrderIBAN,
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt101)
                            Case 0, 78 'MT103

                                _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt103 As New MT103 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt103)
                            Case 95 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift 'MT200

                                _uri = $"{_uri}{SwiftTypeEnum.MT200}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt200 As New MT200 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwiftCode = _swiftPayload.P_OrderSwift,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .SettleDate = _swiftPayload.P_SettleDate,
                                    .Amount = _swiftPayload.P_Amount,
                                    .Ccy = _swiftPayload.P_CCY,
                                    .OrderIBAN = _swiftPayload.P_OrderIBAN,
                                    .BenIBAN = _swiftPayload.P_BenIBAN,
                                    .OrderOther = _swiftPayload.P_OrderOther,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt200)

                            Case 97 And _swiftPayload.P_OrderSwift = _swiftPayload.P_BenSwift 'MT202

                                _uri = $"{_uri}{SwiftTypeEnum.MT202}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt202 As New MT202 With
                                    {
                                    .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                    .OrderSwift = _swiftPayload.P_OrderSwift,
                                    .OrderRef = _swiftPayload.P_OrderRef,
                                    .SettleDate = _swiftPayload.P_SettleDate,
                                    .Amount = _swiftPayload.P_Amount,
                                    .Ccy = _swiftPayload.P_CCY,
                                    .OrderAccountNumber = _swiftPayload.P_OrderAccountNumber,
                                    .OrderIban = _swiftPayload.P_OrderIBAN,
                                    .OrderOther = _swiftPayload.P_OrderOther,
                                    .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                    .OrderName = _swiftPayload.O_OrderName,
                                    .OrderAddress = _swiftPayload.O_Address,
                                    .OrderCity = _swiftPayload.O_City,
                                    .OrderPostCode = _swiftPayload.O_PostCode,
                                    .BeneficiaryRef = _swiftPayload.P_BenRef,
                                    .BeneficiaryName = _swiftPayload.P_BenName,
                                    .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                    .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                    .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                    .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                    .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                    .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                    .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                    .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                    .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                    .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                    .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                    .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                    .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                    .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                    .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                    .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                    .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                    .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                    .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                    .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                    .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                    .BeneficiaryBankBIK = _swiftPayload.P_BenBankBIK,
                                    .BeneficiaryBankINN = _swiftPayload.P_BenBankINN,
                                    .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                    .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                    .Email = _swiftPayload.E_Email
                                    }
                                _payload = _serializer.Serialize(_mt202)

                            Case Else 'MT103

                                _uri = $"{_uri}{SwiftTypeEnum.MT103}?clientId={_swiftPayload.P_ClientId}"

                                Dim _mt103 As New MT103 With
                                {
                                .ClientSwiftCode = _swiftPayload.P_ClientSwiftCode,
                                .OrderSwift = _swiftPayload.P_OrderSwift,
                                .OrderRef = _swiftPayload.P_OrderRef,
                                .SettleDate = _swiftPayload.P_SettleDate,
                                .Amount = _swiftPayload.P_Amount,
                                .Ccy = _swiftPayload.P_CCY,
                                .OrderIbanAccountNumber = IIf(String.IsNullOrEmpty(_swiftPayload.P_OrderIBAN), _swiftPayload.P_OrderAccountNumber, _swiftPayload.P_OrderIBAN),
                                .OrderOther = _swiftPayload.P_OrderOther,
                                .OrderVOCode = _swiftPayload.P_OrderVOCode,
                                .OrderName = _swiftPayload.O_OrderName,
                                .OrderAddress = _swiftPayload.O_Address,
                                .OrderCity = _swiftPayload.O_City,
                                .OrderPostCode = _swiftPayload.O_PostCode,
                                .BeneficiaryName = _swiftPayload.P_BenName,
                                .BeneficiaryAddress1 = _swiftPayload.P_BenAddress1,
                                .BeneficiaryAddress2 = _swiftPayload.P_BenAddress2,
                                .BeneficiaryPostCode = _swiftPayload.P_BenZip,
                                .BeneficiarySwift = _swiftPayload.P_BenSwift,
                                .BeneficiaryIBAN = _swiftPayload.P_BenIBAN,
                                .BeneficiaryCcy = _swiftPayload.P_BenCCY,
                                .BeneficiarySubBankName = _swiftPayload.P_BenSubBankName,
                                .BeneficiarySubBankAddress1 = _swiftPayload.P_BenSubBankAddress1,
                                .BeneficiarySubBankAddress2 = _swiftPayload.P_BenSubBankAddress2,
                                .BeneficiarySubBankPostCode = _swiftPayload.P_BenSubBankZip,
                                .BeneficiarySubBankSwift = _swiftPayload.P_BenSubBankSwift,
                                .BeneficiarySubBankIBAN = _swiftPayload.P_BenSubBankIBAN,
                                .BeneficiarySubBankOther = _swiftPayload.P_BenSubBankOther,
                                .BeneficiarySubBankBik = _swiftPayload.P_BenSubBankBIK,
                                .BeneficiarySubBankINN = _swiftPayload.P_BenSubBankINN,
                                .BeneficiaryBankName = _swiftPayload.P_BenBankName,
                                .BeneficiaryBankAddress1 = _swiftPayload.P_BenBankAddress1,
                                .BeneficiaryBankAddress2 = _swiftPayload.P_BenBankAddress2,
                                .BeneficiaryBankPostCode = _swiftPayload.P_BenBankZip,
                                .BeneficiaryBankSwift = _swiftPayload.P_BenBankSwift,
                                .BeneficiaryBankIBAN = _swiftPayload.P_BenBankIBAN,
                                .BeneficiaryAccountNumber = _swiftPayload.P_BenAccountNumber,
                                .SwiftUrgent = _swiftPayload.P_SwiftUrgent,
                                .SwiftSendRef = _swiftPayload.P_SwiftSendRef,
                                .ExchangeRate = _swiftPayload.P_ExchangeRate,
                                .Email = _swiftPayload.E_Email
                                }
                                _payload = _serializer.Serialize(_mt103)

                        End Select
                End Select

                If Not String.IsNullOrEmpty(_payload) Then
                    Dim _request As HttpWebRequest = CType(WebRequest.Create(_uri), HttpWebRequest)
                    _request.Method = "Post"
                    Dim _byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(_payload)
                    _request.Headers("ApiKey") = _swiftPayload.A_ApiKey
                    _request.ContentType = "application/Json"
                    _request.ContentLength = _byteArray.Length
                    Dim _dataStream As Stream = _request.GetRequestStream()
                    _dataStream.Write(_byteArray, 0, _byteArray.Length)
                    _dataStream.Close()
                    Dim _response As HttpWebResponse = CType(_request.GetResponseWithoutException(), HttpWebResponse)
                    _dataStream = _response.GetResponseStream()
                    Dim _reader As New StreamReader(_dataStream)
                    Dim _webResponse As String = _reader.ReadToEnd()

                    AddAudit(CInt(IIf(_response.StatusCode.ToString.Contains("OK"), 1, 3)), 3, $"Client ID: {_swiftPayload.P_ClientId}, Swift Api {Mid(_uri, 37, 5)} response code " & _response.StatusCode & $", response description {_response.StatusDescription} - {IIf(_response.StatusCode.ToString.Contains("OK"), "Successfully generated ", "Error fetching")} the Swift details ({_webResponse})", CDbl(_swiftPayload.P_PaymentId))

                    Return _webResponse
                Else
                    AddAudit(3, 3, $"Client ID: {_swiftPayload.P_ClientId}, no payload generated for the payment Id {_swiftPayload.P_PaymentId}", CDbl(_swiftPayload.P_PaymentId))

                    Return String.Empty
                End If
            End If
        Catch ex As Exception
            Return String.Empty
        End Try

    End Function

    Public Function FetchClientDetails() As ClientDetails

        Dim Cmd As New SqlClient.SqlCommand
        Dim _reader As SqlClient.SqlDataReader
        Dim _clientDetails As New ClientDetails

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "_Dolfin_spFetchCustomerDetails"
            _reader = Cmd.ExecuteReader
            While _reader.Read
                _clientDetails = New ClientDetails With
                    {
                    .ClientSwiftCode = _reader("swiftcode").ToString,
                    .ClientName = _reader("name").ToString,
                    .ClientAddress = _reader("Address").ToString,
                    .ClientCity = _reader("city").ToString,
                    .ClientPostCode = _reader("postcode").ToString
                    }
            End While

            Return _clientDetails
        Catch ex As SqlClient.SqlException
            Return Nothing
        End Try

    End Function

    Public Function FetchPaymentTypes(ByVal paymentId As Integer, ByVal swiftCode As String) As Dictionary(Of String, String)

        Dim Cmd As New SqlClient.SqlCommand
        Dim _reader As SqlClient.SqlDataReader
        Dim _types As New Dictionary(Of String, String)

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.Text
            Cmd.CommandText = $"SELECT * FROM _Dolfin_FnFetchSwiftTypeId ({paymentId}, '{swiftCode}')"
            _reader = Cmd.ExecuteReader
            If _reader.HasRows Then
                _reader.Read()
                _types.Add("P_OrderTransferMessTypeID", _reader("P_OrderTransferMessTypeID").ToString)
                _types.Add("P_Order3rdPartyMessTypeID", _reader("P_Order3rdPartyMessTypeID").ToString)
                _types.Add("P_OrderOtherMessTypeID", _reader("P_OrderOtherMessTypeID").ToString)
                Return _types
            Else
                Return Nothing
            End If

        Catch ex As SqlClient.SqlException
            Return Nothing
        End Try

    End Function

    Private Function FetchOtherSettleDate(ByVal _orderOther As String) As String

        Dim Cmd As New SqlClient.SqlCommand
        Dim _result As String

        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "_Dolfin_spFetchOtherSettleDate"
            Cmd.Parameters.Add("@OrderOther", SqlDbType.VarChar, 150).Value = _orderOther
            _result = CStr(Cmd.ExecuteScalar())
            If String.IsNullOrEmpty(_result) Then
                Return String.Empty
            Else
                Return CDate(_result).ToString("yyyy-MM-ddT00:00:00")
            End If
        Catch ex As SqlClient.SqlException
            Return String.Empty
        End Try

    End Function

    Public Function GetSwiftFileByID(ByVal varID As Double) As String
        Dim Cmd As New SqlCommand
        GetSwiftFileByID = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPreCreateSwiftFile"
            Cmd.Parameters.Add("@varSelectedPaymentID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@varSwiftFile", SqlDbType.NVarChar).Value = ""
            Dim reader As SqlDataReader = Cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                GetSwiftFileByID = reader(0).ToString
            End If
            reader.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Error on GetSwiftFileByID")
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Function GetSwiftFileByIDBatch(ByVal varID As Double) As String
        Dim Cmd As New SqlCommand
        GetSwiftFileByIDBatch = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentPreCreateSwiftFileBatch"
            Cmd.Parameters.Add("@varSelectedPaymentID", SqlDbType.Int).Value = varID
            Cmd.Parameters.Add("@varSwiftFile", SqlDbType.NVarChar).Value = ""
            Dim reader As SqlDataReader = Cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                GetSwiftFileByIDBatch = reader(0).ToString
            End If
            reader.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Error on GetSwiftFileByIDBatch")
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Function GetSwiftFileData540(Lst As Dictionary(Of String, String)) As String

        Dim Cmd As New SqlCommand
        GetSwiftFileData540 = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentCreateSwiftFileMT540"
            Cmd.Parameters.Add("@New", SqlDbType.Bit).Value = 1
            Cmd.Parameters.Add("@OrderRef", SqlDbType.NVarChar).Value = Lst("OrderRef").ToString
            Cmd.Parameters.Add("@InstrCode", SqlDbType.Int).Value = CInt(Lst("InstrCode"))
            Cmd.Parameters.Add("@TransDate", SqlDbType.SmallDateTime).Value = CDate(Lst("SettleDate").ToString)
            Cmd.Parameters.Add("@SettleDate", SqlDbType.SmallDateTime).Value = CDate(Lst("SettleDate").ToString)
            Cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = CDbl(Lst("Amount"))
            Cmd.Parameters.Add("@BrokerCode", SqlDbType.Int).Value = CInt(Lst("BrokerCode"))
            Cmd.Parameters.Add("@ClearerCode", SqlDbType.Int).Value = CInt(Lst("ClearerCode"))
            Cmd.Parameters.Add("@DelivAgent", SqlDbType.NVarChar).Value = Lst("Agent")
            Cmd.Parameters.Add("@DelivAgentAccountNo", SqlDbType.NVarChar).Value = Lst("AgentAccountNo")
            Cmd.Parameters.Add("@PlaceofSettlement", SqlDbType.NVarChar).Value = Lst("PlaceofSettlement")
            Cmd.Parameters.Add("@Instructions", SqlDbType.NVarChar).Value = Lst("Instructions")
            Cmd.Parameters.Add("@varSwiftFile", SqlDbType.NVarChar).Value = ""
            Dim reader As SqlDataReader = Cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                GetSwiftFileData540 = reader(0).ToString
            End If
            reader.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Error on GetSwiftFileData")
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Function GetSwiftFileData542(Lst As Dictionary(Of String, String)) As String

        Dim Cmd As New SqlCommand
        GetSwiftFileData542 = ""
        Try
            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "DolfinPaymentCreateSwiftFileMT542"
            Cmd.Parameters.Add("@New", SqlDbType.Bit).Value = 1
            Cmd.Parameters.Add("@OrderRef", SqlDbType.NVarChar).Value = Lst("OrderRef").ToString
            Cmd.Parameters.Add("@InstrCode", SqlDbType.Int).Value = CInt(Lst("InstrCode"))
            Cmd.Parameters.Add("@TransDate", SqlDbType.SmallDateTime).Value = CDate(Lst("SettleDate").ToString)
            Cmd.Parameters.Add("@SettleDate", SqlDbType.SmallDateTime).Value = CDate(Lst("SettleDate").ToString)
            Cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = CDbl(Lst("Amount"))
            Cmd.Parameters.Add("@BrokerCode", SqlDbType.Int).Value = CInt(Lst("BrokerCode"))
            Cmd.Parameters.Add("@ClearerCode", SqlDbType.Int).Value = CInt(Lst("ClearerCode"))
            Cmd.Parameters.Add("@ReceivAgent", SqlDbType.NVarChar).Value = Lst("Agent")
            Cmd.Parameters.Add("@ReceivAgentAccountNo", SqlDbType.NVarChar).Value = Lst("AgentAccountNo")
            Cmd.Parameters.Add("@PlaceofSettlement", SqlDbType.NVarChar).Value = Lst("PlaceofSettlement")
            Cmd.Parameters.Add("@Instructions", SqlDbType.NVarChar).Value = Lst("Instructions")
            Cmd.Parameters.Add("@varSwiftFile", SqlDbType.NVarChar).Value = ""
            Dim reader As SqlDataReader = Cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                GetSwiftFileData542 = reader(0).ToString
            End If
            reader.Close()
        Catch ex As Exception
            AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Error on GetSwiftFileData")
        Finally
            Cmd = Nothing
        End Try
    End Function

    Public Function PopulateComments(ByVal varComPayType As String, ByVal varComPort As String, ByVal varComOrderAcc As String,
                                     ByVal varComBenAcc As String, ByVal varComAmt As String, ByVal varComCCY As String,
                                     ByVal varPayReqID As Integer, ByVal varBenBankName As String) As String
        PopulateComments = ""
        varComOrderAcc = Replace(varComOrderAcc, "Own Portfolio (Discretionary)", "Britannia Global Markets Ltd")
        varComBenAcc = Replace(varComBenAcc, "Own Portfolio (Discretionary)", "Britannia Global Markets Ltd")
        If varComBenAcc <> "" And IsNumeric(varComAmt) And varComCCY <> "" Then
            Dim getHeader As String = GetSQLItem("Select PT_AutoCommentHeader from vwDolfinPaymentType where PT_PaymentType = '" & varComPayType & "'")

            If varComPort <> "" Then
                getHeader = IIf(getHeader = "", "", getHeader & " ") & varComPort
            ElseIf getHeader = "" Then
                getHeader = varComPayType
            End If

            If varComPayType = GetPaymentType(cPayDescExIn) Then
                PopulateComments = getHeader & " to " & varComBenAcc & " for " & varComAmt & " " & varComCCY
            ElseIf varComPayType = GetPaymentType(cPayDescExOut) Or varComPayType = GetPaymentType(cPayDescExClientOut) Or varComPayType = GetPaymentType(cPayDescExVolopaOut) Then
                If varPayReqID = 4 Then
                    PopulateComments = "Own funds transfer to " & varComBenAcc & IIf(varBenBankName <> "", " at " & varBenBankName, "")
                Else
                    PopulateComments = getHeader & " " & varComBenAcc & IIf(varBenBankName <> "", " account in " & varBenBankName, "")
                End If
            ElseIf varComOrderAcc <> "" Then
                PopulateComments = getHeader & " from " & varComOrderAcc & " to " & varComBenAcc & " for " & varComAmt & " " & varComCCY
            ElseIf varComPayType = GetPaymentType(cPayDescInPortLoan) Then
                PopulateComments = "Payment to " & varComBenAcc & " for Loan Agreement No " & Now.Date.ToString("yyyymmdd")
            End If
        End If
    End Function

    Public Sub OpenSwiftFile(ByVal varSwiftFile As String)
        If IO.File.Exists(GetFilePath("SWIFTImport") & varSwiftFile) = True Then
            Process.Start(GetFilePath("SWIFTImport") & varSwiftFile)
        End If
    End Sub

    Public Sub CloseConnection()
        conn.Close()
    End Sub

    Public Sub New()
        'FOR TESTING
        If Environment.MachineName = "IMSAPPPROD01" Then
            varUserNameDomain = "IMS-BRITANNIA\"
            varUserName = varUserNameDomain & "da-dHarper"
        End If

        conn = GetNewOpenConnection()
        GetUserOptions()
        PopulatePaymentTypes()
        PopulatePaymentAllTypes()
        PopulatePaymentTypesShortCode()
        PopulatePaymentOtherTypes()
        PopulateDolfinAddress()
        PopulateLocation()
        PopulateStatusTypes()
        PopulatePayCheckTypes()
        PopulateFilePaths()
        PopulateFileExportNames()
        PopulateMessageTypes()
        PopulateAuditLogStatusTypes()
        PopulateAuditLogGroupNames()
        GetIMSDB()
    End Sub
End Class
