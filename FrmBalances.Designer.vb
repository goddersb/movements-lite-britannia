﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBalances
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBalances))
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtToDate = New System.Windows.Forms.DateTimePicker()
        Me.cboBalAccount = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboBalClient = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboBalBroker = New System.Windows.Forms.ComboBox()
        Me.ChkShowZero = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboBalClass = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboBalCCY = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboBalPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dgvViewBal = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboBreakdown = New System.Windows.Forms.ComboBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvViewBal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.Label7)
        Me.ViewGroupBox.Controls.Add(Me.Label6)
        Me.ViewGroupBox.Controls.Add(Me.dtToDate)
        Me.ViewGroupBox.Controls.Add(Me.cboBalAccount)
        Me.ViewGroupBox.Controls.Add(Me.Label5)
        Me.ViewGroupBox.Controls.Add(Me.cboBalClient)
        Me.ViewGroupBox.Controls.Add(Me.Label3)
        Me.ViewGroupBox.Controls.Add(Me.CboBalBroker)
        Me.ViewGroupBox.Controls.Add(Me.ChkShowZero)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Controls.Add(Me.cboBalClass)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboBalCCY)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboBalPortfolio)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 45)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(890, 103)
        Me.ViewGroupBox.TabIndex = 31
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Movements Criteria"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 65
        Me.Label7.Text = "Account"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(357, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 13)
        Me.Label6.TabIndex = 63
        Me.Label6.Text = "To Date"
        '
        'dtToDate
        '
        Me.dtToDate.Location = New System.Drawing.Point(408, 75)
        Me.dtToDate.Name = "dtToDate"
        Me.dtToDate.Size = New System.Drawing.Size(144, 20)
        Me.dtToDate.TabIndex = 62
        '
        'cboBalAccount
        '
        Me.cboBalAccount.FormattingEnabled = True
        Me.cboBalAccount.Location = New System.Drawing.Point(59, 74)
        Me.cboBalAccount.Name = "cboBalAccount"
        Me.cboBalAccount.Size = New System.Drawing.Size(292, 21)
        Me.cboBalAccount.TabIndex = 64
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "Client"
        '
        'cboBalClient
        '
        Me.cboBalClient.FormattingEnabled = True
        Me.cboBalClient.Location = New System.Drawing.Point(59, 19)
        Me.cboBalClient.Name = "cboBalClient"
        Me.cboBalClient.Size = New System.Drawing.Size(292, 21)
        Me.cboBalClient.TabIndex = 60
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(488, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Broker"
        '
        'CboBalBroker
        '
        Me.CboBalBroker.FormattingEnabled = True
        Me.CboBalBroker.Location = New System.Drawing.Point(526, 21)
        Me.CboBalBroker.Name = "CboBalBroker"
        Me.CboBalBroker.Size = New System.Drawing.Size(182, 21)
        Me.CboBalBroker.TabIndex = 58
        '
        'ChkShowZero
        '
        Me.ChkShowZero.AutoSize = True
        Me.ChkShowZero.Location = New System.Drawing.Point(576, 76)
        Me.ChkShowZero.Name = "ChkShowZero"
        Me.ChkShowZero.Size = New System.Drawing.Size(132, 17)
        Me.ChkShowZero.TabIndex = 57
        Me.ChkShowZero.Text = "Show Empty Balances"
        Me.ChkShowZero.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(370, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Class"
        '
        'cboBalClass
        '
        Me.cboBalClass.FormattingEnabled = True
        Me.cboBalClass.Location = New System.Drawing.Point(408, 48)
        Me.cboBalClass.Name = "cboBalClass"
        Me.cboBalClass.Size = New System.Drawing.Size(300, 21)
        Me.cboBalClass.TabIndex = 55
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(842, 21)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 54
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(374, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "CCY"
        '
        'cboBalCCY
        '
        Me.cboBalCCY.FormattingEnabled = True
        Me.cboBalCCY.Location = New System.Drawing.Point(408, 21)
        Me.cboBalCCY.Name = "cboBalCCY"
        Me.cboBalCCY.Size = New System.Drawing.Size(69, 21)
        Me.cboBalCCY.TabIndex = 42
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(724, 66)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 28)
        Me.CmdRefreshView.TabIndex = 29
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(724, 19)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 41)
        Me.CmdFilter.TabIndex = 31
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Portfolio"
        '
        'cboBalPortfolio
        '
        Me.cboBalPortfolio.FormattingEnabled = True
        Me.cboBalPortfolio.Location = New System.Drawing.Point(59, 46)
        Me.cboBalPortfolio.Name = "cboBalPortfolio"
        Me.cboBalPortfolio.Size = New System.Drawing.Size(292, 21)
        Me.cboBalPortfolio.TabIndex = 29
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(95, 24)
        Me.Label13.TabIndex = 40
        Me.Label13.Text = "Balances"
        '
        'dgvViewBal
        '
        Me.dgvViewBal.AllowUserToAddRows = False
        Me.dgvViewBal.AllowUserToDeleteRows = False
        Me.dgvViewBal.AllowUserToOrderColumns = True
        Me.dgvViewBal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvViewBal.Location = New System.Drawing.Point(12, 159)
        Me.dgvViewBal.Name = "dgvViewBal"
        Me.dgvViewBal.ReadOnly = True
        Me.dgvViewBal.Size = New System.Drawing.Size(1275, 660)
        Me.dgvViewBal.TabIndex = 41
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(1051, 135)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 43
        Me.Label4.Text = "Breakdown:"
        '
        'cboBreakdown
        '
        Me.cboBreakdown.FormattingEnabled = True
        Me.cboBreakdown.Location = New System.Drawing.Point(1121, 132)
        Me.cboBreakdown.Name = "cboBreakdown"
        Me.cboBreakdown.Size = New System.Drawing.Size(166, 21)
        Me.cboBreakdown.TabIndex = 42
        '
        'FrmBalances
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1299, 831)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboBreakdown)
        Me.Controls.Add(Me.dgvViewBal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmBalances"
        Me.Text = "Balances"
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvViewBal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents cboBalCCY As ComboBox
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CmdFilter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboBalPortfolio As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents dgvViewBal As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents cboBalClass As ComboBox
    Friend WithEvents ChkShowZero As CheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents CboBalBroker As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cboBreakdown As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboBalClient As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents dtToDate As DateTimePicker
    Friend WithEvents Label7 As Label
    Friend WithEvents cboBalAccount As ComboBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
