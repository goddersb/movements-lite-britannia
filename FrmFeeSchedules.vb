﻿Public Class FrmFeeSchedules
    Dim FSConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet

    Private Sub LoadGridFeeSchedule(Optional ByVal varFilter As String = "")
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "SELECT FS_ID as ID,S_Status as Status,Portfolio,CR_Name1 as CCY, ClientName, PF_Shortcut1 as ShortCode, RM1, RM2, TemplateLinkedName as LinkedTemplate, FS_Destination as Destination, " &
                        "cast(Case when MGTID<>0 then 1 else 0 end as bit) as MGT,cast(Case when CFID<>0 then 1 else 0 end as bit) as Custody," &
                        "cast(Case when TFID<>0 then 1 else 0 end as bit) as Transact,cast(Case when TSID<>0 then 1 else 0 end as bit) as Transfer, " &
                        "FS_Template as Template, cast(Activity as bit) as Activity, cast(InvVisa as bit) as InvVisa, cast(FS_IsEAM as bit) as IsEAM, FS_FilePath as FilePath, PF_Code " &
                        "FROM vwDolfinPaymentFeeSchedulesLinks "
            If varFilter <> "" Then
                varSQL = varSQL & varFilter & " and (branchCode is null or branchCode = " & ClsIMS.UserLocationCode & ")"
            Else
                varSQL = varSQL & "where (branchcode is null or branchCode = " & ClsIMS.UserLocationCode & ")"
            End If

            varSQL = varSQL & " Order by FS_Template desc, Portfolio"

            dgvFS.DataSource = Nothing
            FSConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FSConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentFeeSchedulesLinks")
            dgvFS.AutoGenerateColumns = True
            dgvFS.DataSource = ds.Tables("vwDolfinPaymentFeeSchedulesLinks")

            dgvFS.Columns("ID").Width = 60
            dgvFS.Columns("Status").Width = 125
            dgvFS.Columns("Portfolio").Width = 250
            dgvFS.Columns("CCY").Width = 60
            dgvFS.Columns("ClientName").Width = 200
            dgvFS.Columns("ShortCode").Width = 100
            dgvFS.Columns("RM1").Width = 100
            dgvFS.Columns("RM2").Width = 100
            dgvFS.Columns("Destination").Width = 100
            dgvFS.Columns("LinkedTemplate").Width = 150
            dgvFS.Columns("MGT").Width = 60
            dgvFS.Columns("Custody").Width = 60
            dgvFS.Columns("Transact").Width = 60
            dgvFS.Columns("Transfer").Width = 60
            dgvFS.Columns("Template").Width = 60
            dgvFS.Columns("Activity").Width = 60
            dgvFS.Columns("InvVisa").Width = 60
            dgvFS.Columns("FilePath").Width = 500
            dgvFS.Columns("PF_Code").Visible = False
            dgvFS.Columns("Status").Visible = False

            dgvFS.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvFS.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvFS.DataSource = Nothing

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with viewing fee schedule grid")
        End Try
    End Sub

    Private Sub AssignCbo()
        cboFSPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFSPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        cboFSFeeStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFSFeeStatus.AutoCompleteSource = AutoCompleteSource.ListItems
        cboFSTemplates.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFSTemplates.AutoCompleteSource = AutoCompleteSource.ListItems
        cboFSRM1.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFSRM1.AutoCompleteSource = AutoCompleteSource.ListItems
        cboFSRM2.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFSRM2.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub FrmFeeSchedules_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call PopulateCbo(cboFSPortfolio, "Select -1 as pf_Code, '' as Portfolio union Select 0 as pf_Code, '<New Template>' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentFeeSchedulesLinks where branchCode = " & ClsIMS.UserLocationCode & " and isnull(fs_template,0) = 0 order by Portfolio")
        Call PopulateCbo(cboFSFeeStatus, "Select 0,ISNULL(S_Status,'') as S_Status from vwDolfinPaymentFeeSchedulesLinks group by ISNULL(S_Status,'') order by ISNULL(S_Status,'')")
        Call PopulateCbo(cboFSTemplates, "Select TemplateLinkedID,TemplateLinkedName from vwDolfinPaymentFeeSchedulesLinks group by TemplateLinkedID,TemplateLinkedName order by TemplateLinkedName")
        Call PopulateCbo(cboFSRM1, "Select 0,ISNULL(RM1,'') as RM1 from vwDolfinPaymentFeeSchedulesLinks where branchCode = " & ClsIMS.UserLocationCode & "  group by ISNULL(RM1,'') order by ISNULL(RM1,'')")
        Call PopulateCbo(cboFSRM2, "Select 0,ISNULL(RM2,'') as RM2 from vwDolfinPaymentFeeSchedulesLinks where branchCode = " & ClsIMS.UserLocationCode & " group by ISNULL(RM2,'') order by ISNULL(RM2,'')")
        Call PopulateCbo(cboFSDestination, "Select 0,ISNULL(FS_Destination,'') from vwDolfinPaymentFeeSchedulesLinks where branchCode = " & ClsIMS.UserLocationCode & " group by ISNULL(FS_Destination,'') order by ISNULL(FS_Destination,'')")
        ModRMS.DoubleBuffered(dgvFS, True)
        AssignCbo()
        LoadGridFeeSchedule()
    End Sub

    Private Sub dgvFS_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvFS.RowHeaderMouseDoubleClick
        Dim FrmFeeAuth As New FrmFeeAuthorise

        ClsFS = New ClsFeeSchedule
        ClsMgtFees = New ClsMgtFee
        ClsCF = New ClsCustodyFee
        ClsTF = New ClsTransactionFee
        If Not IsDBNull(dgvFS.Rows(dgvFS.SelectedRows.Item(0).Index).Cells("id").Value) Then
            If IsNumeric(dgvFS.Rows(dgvFS.SelectedRows.Item(0).Index).Cells("id").Value) Then
                ClsIMS.GetReaderItemIntoFSFeeClass(dgvFS.Rows(dgvFS.SelectedRows.Item(0).Index).Cells("id").Value)
                If ClsFS.MGTID <> 0 Then
                    ClsIMS.GetReaderItemIntoMgtFeeClass(ClsFS.MGTID)
                End If

                If ClsFS.CFID <> 0 Then
                    ClsIMS.GetReaderItemIntoCFClass(ClsFS.CFID)
                End If

                If ClsFS.TFID <> 0 Then
                    ClsIMS.GetReaderItemIntoTFClass(ClsFS.TFID)
                End If

                FrmFeeAuth.ShowDialog()
                If ClsFS.SelectedAuthoriseOption = 3 Then 'edit
                    Dim FrmFees As New FrmFees
                    FrmFees.ShowDialog()
                End If
                LoadGridFeeSchedule()
            Else
                NewFeeSchedule()
            End If
        Else
            ClsFS.PortfolioCode = dgvFS.Rows(dgvFS.SelectedRows.Item(0).Index).Cells("pf_Code").Value
            NewFeeSchedule()
        End If
    End Sub


    Private Sub FrmFeeSchedules_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If FSConn.State = ConnectionState.Open Then
            FSConn.Close()
        End If
    End Sub

    Private Sub dgvFS_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvFS.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvFS, e.RowIndex)
        End If
    End Sub

    Private Sub CmdNewFS_Click(sender As Object, e As EventArgs) Handles CmdNewFS.Click
        If IsNumeric(cboFSPortfolio.SelectedValue) And IsNumeric(dgvFS.Rows(0).Cells(0).Value) Then
            If cboFSPortfolio.SelectedValue > 0 Then
                MsgBox("You can only create a new fee schedule when a fee schedule is not available for this portfolio." & vbNewLine & "Please delete the current schedule for this portfolio or double click on the row divider to edit this schedule", vbInformation, "Cannot create new schedule")
            Else
                If IsNumeric(cboFSPortfolio.SelectedValue) And cboFSPortfolio.SelectedValue > 0 Then
                    ClsFS.PortfolioCode = cboFSPortfolio.SelectedValue
                Else
                    ClsFS = New ClsFeeSchedule
                    NewFeeSchedule()
                End If
            End If
        Else
            If IsNumeric(cboFSPortfolio.SelectedValue) And cboFSPortfolio.SelectedValue > 0 Then
                ClsFS.PortfolioCode = cboFSPortfolio.SelectedValue
            Else
                ClsFS = New ClsFeeSchedule
            End If
            NewFeeSchedule()
        End If
    End Sub

    Private Sub NewFeeSchedule()
        Dim FrmFees As New FrmFees

        If Not IsNumeric(ClsFS.PortfolioCode) Or ClsFS.PortfolioCode = 0 Then
            ClsFS = New ClsFeeSchedule
        End If
        ClsMgtFees = New ClsMgtFee
        ClsCF = New ClsCustodyFee
        ClsTF = New ClsTransactionFee
        FrmFees.ShowDialog()
        LoadGridFeeSchedule()
    End Sub


    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        LoadGridFeeSchedule()
        ClearFilters()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvFS, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varWhereClauseFilter As String = "where"

        If cboFSFeeStatus.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and S_Status in ('" & cboFSFeeStatus.Text & "')"
        End If

        If cboFSTemplates.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(TemplateLinkedID,0) in (" & cboFSTemplates.SelectedValue & ")"
        End If

        'If cboPSFeeStatus.Text = "" And cboPSTemplates.Text = "" And Not ChkMGT.Checked And Not ChkCustody.Checked And Not ChkTransactions.Checked _
        '   And Not ChkTransfers.Checked And Not ChkTemplates.Checked And cboPSRM1.Text = "" And cboPSRM2.Text = "" Then
        'varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(MGTID, 0) = 0 and ISNULL(CFID, 0) = 0 and ISNULL(TFID, 0) = 0 and ISNULL(TSID, 0) = 0 and ISNULL(FS_Template,0) = " & IIf(ChkTemplates.Checked, 1, 0) & " and ISNULL(Activity,0) = " & IIf(ChkActivity.Checked, 1, 0) & " and ISNULL(InvVisa,0) = " & IIf(ChkInvVisa.Checked, 1, 0) & " and ISNULL(TemplateLinkedID, 0) = 0 "

        If ChkNoTemplate.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(TemplateLinkedID, 0) = 0 "
        End If

        If ChkMGT.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(MGTID, 0) <> 0"
        End If

        If ChkCustody.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(CFID, 0) <> 0"
        End If

        If ChkTransactions.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(TFID, 0) <> 0"
        End If

        If ChkTransfers.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(TSID, 0) <> 0"
        End If

        If ChkTemplates.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(FS_Template,0) = " & IIf(ChkTemplates.Checked, 1, 0)
        End If

        If ChkActivity.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(Activity,0) = " & IIf(ChkActivity.Checked, 1, 0)
        End If

        If ChkInvVisa.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(InvVisa,0) = " & IIf(ChkInvVisa.Checked, 1, 0)
        End If

        If cboFSRM1.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(RM1,'') In ('" & cboFSRM1.Text & "')"
        End If

        If cboFSRM2.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(RM2,'') in ('" & cboFSRM2.Text & "')"
        End If

        If cboFSDestination.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(FS_Destination,'') in ('" & cboFSDestination.Text & "')"
        End If

        If ChkIsEAM.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISNULL(FS_IsEAM,0) = " & IIf(ChkIsEAM.Checked, 1, 0)
        End If


        If varWhereClauseFilter <> "where" Then
            LoadGridFeeSchedule(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            LoadGridFeeSchedule()
        End If
    End Sub

    Public Sub ClearFilters()
        cboFSPortfolio.Text = ""
        cboFSFeeStatus.Text = ""
        cboFSTemplates.Text = ""
        ChkMGT.Checked = False
        ChkCustody.Checked = False
        ChkTransactions.Checked = False
        ChkTransfers.Checked = False
        ChkTemplates.Checked = False
        ChkActivity.Checked = True
        ChkInvVisa.Checked = False
        cboFSRM1.Text = ""
        cboFSRM2.Text = ""
        cboFSDestination.Text = ""
        ChkNoTemplate.Checked = False
        ChkIsEAM.Checked = False
    End Sub

    Private Sub cmdResetFilter_Click(sender As Object, e As EventArgs) Handles cmdResetFilter.Click
        LoadGridFeeSchedule()
        ClearFilters()
    End Sub

    Private Sub cboPSPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFSPortfolio.SelectedIndexChanged
        dgvFS.Select()
    End Sub

    Private Sub cboPSPortfolio_Leave(sender As Object, e As EventArgs) Handles cboFSPortfolio.Leave
        If IsNumeric(cboFSPortfolio.SelectedValue) Then
            If cboFSPortfolio.SelectedValue > 0 Then
                LoadGridFeeSchedule("where pf_code = " & cboFSPortfolio.SelectedValue)
            Else
                LoadGridFeeSchedule()
            End If
            CmdNewFS.Enabled = True
        Else
            ClsFS.PortfolioCode = 0
        End If
    End Sub
End Class