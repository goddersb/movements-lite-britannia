﻿Imports System.Math
Imports Microsoft.VisualBasic.FileIO
Imports System.Net.Mail
Imports System.IO
Imports Microsoft.Office

Public Class ClsPayment
    Private varPayOptions As New Dictionary(Of Integer, String)
    Private varStatus As String = ""
    Private varAmount As Double = 0
    Private varSelectedType As String = ""
    Private varSelectedName As String = ""
    Private varPortfolio As String = ""
    Private varPortfolioCode As Integer = 0
    Private varCustomerId As Integer = 0
    Private varCustomerName As String = ""
    Private varOrderPortfolioShortcut As String = ""
    Private varOtherID As Integer = 0
    Private varPortfolioIsCommission As Boolean = False
    Private varPortfolioIsFee As Boolean = False
    Private varPortfolioFee As Double = 0
    Private varPortfolioFeeCCY As String = ""
    Private varPortfolioFeeInt As Double = 0
    Private varPortfolioFeeCCYInt As String = ""
    Private varPortfolioFeeDue As Boolean = False
    Private varPortfolioFeeID As Integer = 0
    Private varPortfolioFeeAccountCode As Integer = 0
    Private varPortfolioFeeRMSAccountCode As Integer = 0
    Private varCCY As String = ""
    Private varCCYCode As Integer = 0
    Private varSelectedBeneficiaryBankSwiftCode As String = ""
    Private varSelectedBeneficiarySubBankSwiftCode As String = ""
    Private varSelectedPaymentID As Double = 0
    Private varSelectedAccount As String = ""
    Private varSelectedOrder As Integer = 0
    Private varSelectedPaymentOption As Integer = 0
    Private varSelectedAuthoriseOption As Integer = 0
    Private varSelectedTransferFeeOption As Integer = 0
    Private varSelectedTemplateName As String = ""
    Private varSelectedEmailAttachments As Boolean = False
    Private varPaymentType As String = ""
    Private varMessageType As String = ""
    Private varOrderFormData As New Dictionary(Of String, String)
    Private varOrderBrokerCode As Integer = 0
    Private varOrderBrokerShortcut As String = ""
    Private varOrderAccount As String = ""
    Private varOrderAccountCode As Integer = 0
    Private varOrderAccountShortcut As String = ""
    Private varOrderBalance As Double = 0
    Private varOrderName As String = ""
    Private varOrderRef As String = ""
    Private varOrderAddress1 As String = ""
    Private varOrderAddress2 As String = ""
    Private varOrderZip As String = ""
    Private varOrderCountryID As Integer = 0
    Private varOrderCountry As String = ""
    Private varOrderSwift As String = ""
    Private varOrderAccountNumber As String = ""
    Private varOrderIBAN As String = ""
    Private varOrderOther As String = ""
    Private varOrderVOCodeID As Integer = 0
    Private varOrderMessageTypeID As Integer = 0
    Private varBenFormData As New Dictionary(Of String, String)
    Private varBenAddressID As Integer = 0
    Private varBenBrokerCode As Integer = 0
    Private varBenBrokerShortcut As String = ""
    Private varBenAccount As String = ""
    Private varBenAccountCode As Integer = 0
    Private varBenAccountShortcut As String = ""
    Private varBenAccountFiltered As Boolean = False
    Private varBenBalance As Double = 0
    Private varBenTypeID As Integer = 0
    Private varBenType As String = ""
    Private varBenName As String = ""
    Private varBenFirstname As String = ""
    Private varBenMiddlename As String = ""
    Private varBenSurname As String = ""
    Private varBenRef As String = ""
    Private varBenAddress1 As String = ""
    Private varBenAddress2 As String = ""
    Private varBenCounty As String = ""
    Private varBenZip As String = ""
    Private varBenCountryID As Integer = 0
    Private varBenCountry As String = ""
    Private varBenSwift As String = ""
    Private varBenAccountNumber As String = ""
    Private varBenIBAN As String = ""
    Private varBenOther As String = ""
    Private varBenBIK As String = ""
    Private varBenINN As String = ""
    Private varBenRelationshipID As Integer = 0
    Private varBenRelationship As String = ""
    Private varBenPortfolioCode As Integer = 0
    Private varBenPortfolio As String = ""
    Private varBenPortfolioShortcut As String = ""
    Private varBenCCY As String = ""
    Private varBenCCYCode As Integer = 0
    Private varBenSubBankFormData As New Dictionary(Of String, String)
    Private varBenSubBankAddressID As Integer = 0
    Private varBenSubBankName As String = ""
    Private varBenSubBankAddress1 As String = ""
    Private varBenSubBankAddress2 As String = ""
    Private varBenSubBankZip As String = ""
    Private varBenSubBankCountryID As Integer = 0
    Private varBenSubBankCountry As String = ""
    Private varBenSubBankSwift As String = ""
    Private varBenSubBankIBAN As String = ""
    Private varBenSubBankOther As String = ""
    Private varBenSubBankBIK As String = ""
    Private varBenSubBankINN As String = ""
    Private varBenBankFormData As New Dictionary(Of String, String)
    Private varBenBankAddressID As Integer = 0
    Private varBenBankName As String = ""
    Private varBenBankAddress1 As String = ""
    Private varBenBankAddress2 As String = ""
    Private varBenBankZip As String = ""
    Private varBenBankCountryID As Integer = 0
    Private varBenBankCountry As String = ""
    Private varBenBankSwift As String = ""
    Private varBenBankIBAN As String = ""
    Private varBenBankOther As String = ""
    Private varBenBankBIK As String = ""
    Private varBenBankINN As String = ""
    Private varBenTemplateName As String = ""
    Private varTransDate As Date = Now
    Private varSettleDate As Date = Now
    Private varComments As String = ""
    Private varSendSwift As Boolean = True
    Private varSendIMS As Boolean = True
    Private varSendSwift2 As Boolean = True
    Private varSendIMS2 As Boolean = True
    Private varCutOfftime As String = ""
    Private varCutOfftimeZone As String = ""
    Private varCutOfftimeGMT As String = ""
    Private varPaymentFilePath As String = ""
    Private varTempPaymentFilePath As String = ""
    Private varPaymentFilePathRemote As String = ""
    Private varSwiftUrgent As Boolean = False
    Private varSwiftSendRef As Boolean = False
    Private varSwiftReturned As Boolean = False
    Private varSwiftAcknowledged As Boolean = False
    Private varSwiftStatus As String = ""
    Private varIMSReturned As Boolean = False
    Private varIMSAcknowledged As Boolean = False
    Private varIMSStatus As String = ""
    Private varIMSPayInCode As String = ""
    Private varIMSPayOutCode As String = ""
    Private varExchangeRate As Double = 1
    Private varUserNameEntered As String = ""
    Private varAbovethreshold As Boolean = False
    Private varIMSRef As String = ""
    Private varSentApproveEmailCompliance As Integer = 0
    Private varPaymentRequested As Boolean = False
    Private varPaymentRequestedType As Integer = 0
    Private varPaymentRequestedRate As Double = 0
    Private varPaymentRequestedEmail As String = ""
    Private varPaymentRequestedFrom As String = ""
    Private varPaymentRequestedBeneficiaryName As String = ""
    Private varPaymentRequestedCheckedSantions As Boolean = False
    Private varPaymentRequestedCheckedPEP As Boolean = False
    Private varPaymentRequestedCheckedCDD As Boolean = False
    Private varPaymentRequestedIssuesDisclosed As String = ""
    Private varPaymentRequestedCheckedBankName As String = ""
    Private varPaymentRequestedCheckedBankCountryID As Integer = 0
    Private varPaymentRequestedCheckedBankCountry As String = ""
    Private varPaymentRequestedMMBuySell As Integer = 0
    Private varPaymentRequestedMMInstrumentCode As Integer = 0
    Private varPaymentRequestedMMInstrument As String = ""
    Private varPaymentRequestedMMDurationType As Integer = 0
    Private varPaymentRequestedMMStartDate As Date = Now
    Private varPaymentRequestedMMEndDate As Date = Now
    Private varPaymentRequestedMMFirmMove As Boolean = False
    Private varCheckedSwiftSortCode As String = ""
    Private varCheckedAccountNo As String = ""
    Private varPaymentRequestedKYCFolder As String = "\\EgnyteDrive\DolfinGroup\Shared\Dolfin\Business Development\Sales\Clients\Private\"
    Private varPaymentIDOther As Double = 0
    Private varBPT As DataTable = Nothing
    Private varCanSendEmail As Boolean

    Property OrderAccountCode() As Integer
        Get
            Return varOrderAccountCode
        End Get
        Set(value As Integer)
            varOrderAccountCode = value
            varOrderAccountNumber = ""

            If IsNumeric(varOrderAccountCode) Then
                If varPortfolioCode <> 0 And varCCYCode <> 0 Then
                    varOrderFormData = ClsIMS.GetFormData(varPortfolioCode, varCCYCode, varOrderAccountCode)
                End If
            End If

            If Not varOrderFormData Is Nothing Then
                If varOrderFormData.ContainsKey("B_Code") Then
                    If IsNumeric(varOrderFormData("B_Code")) Then
                        varOrderBrokerCode = Trim(varOrderFormData("B_Code"))
                        varOrderAccountNumber = Trim(varOrderFormData("T_Account"))
                    End If
                End If
            End If

            If varPortfolioCode <> 0 And varCCYCode <> 0 And varOrderAccountCode <> 0 Then
                varOrderBalance = ClsIMS.GetBalance(varPortfolioCode, varOrderAccountCode)
            Else
                varOrderFormData = Nothing
            End If
        End Set
    End Property

    Property BeneficiaryAccountCode() As Integer
        Get
            Return varBenAccountCode
        End Get
        Set(value As Integer)
            varBenAccountCode = value
            varBenAccountNumber = ""
            If IsNumeric(varBenAccountCode) Then
                If varBenPortfolioCode <> 0 And varBenCCYCode <> 0 Then
                    If IsNumeric(varBenAccountCode) Then
                        If varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                            varBenFormData = ClsIMS.GetFormData(varBenPortfolioCode, varBenCCYCode, varBenAccountCode, varOrderAccountCode)
                        Else
                            varBenFormData = ClsIMS.GetFormData(varBenPortfolioCode, varBenCCYCode, varBenAccountCode)
                        End If
                    Else
                        varBenFormData = ClsIMS.GetFormData(varBenPortfolioCode, varBenCCYCode, varBenAccountCode)
                    End If
                End If

                If Not varBenFormData Is Nothing Then
                    If varBenFormData.ContainsKey("B_Code") Then
                        If IsNumeric(varBenFormData("B_Code")) Then
                            varBenBrokerCode = Trim(varBenFormData("B_Code"))
                            varBenAccountNumber = Trim(varBenFormData("T_Account"))
                        End If
                    End If
                End If

                If varBenPortfolioCode <> 0 And varBenCCYCode <> 0 And varBenAccountCode <> 0 Then
                    varBenBalance = ClsIMS.GetBalance(varBenPortfolioCode, varBenAccountCode)
                Else
                    varBenFormData = Nothing
                End If
            End If
        End Set
    End Property

    Property SelectedBeneficiarySubBankSwiftCode() As String
        Get
            Return varSelectedBeneficiarySubBankSwiftCode
        End Get
        Set(value As String)
            varSelectedBeneficiarySubBankSwiftCode = value
            If varSelectedBeneficiarySubBankSwiftCode <> "" Then
                varBenSubBankFormData = ClsIMS.GetBankFormData(varSelectedBeneficiarySubBankSwiftCode)
            Else
                varBenSubBankFormData = Nothing
            End If
        End Set
    End Property

    Property SelectedBeneficiaryBankSwiftCode() As String
        Get
            Return varSelectedBeneficiaryBankSwiftCode
        End Get
        Set(value As String)
            varSelectedBeneficiaryBankSwiftCode = value
            If varSelectedBeneficiaryBankSwiftCode <> "" Then
                varBenBankFormData = ClsIMS.GetBankFormData(varSelectedBeneficiaryBankSwiftCode)
            Else
                varBenBankFormData = Nothing
            End If
        End Set
    End Property

    Property SelectedPaymentID() As Double
        Get
            Return varSelectedPaymentID
        End Get
        Set(value As Double)
            varSelectedPaymentID = value
        End Set
    End Property

    Property SelectedOrder() As Integer
        Get
            Return varSelectedOrder
        End Get
        Set(value As Integer)
            varSelectedOrder = value
        End Set
    End Property

    Property SelectedPaymentOption() As Integer
        Get
            Return varSelectedPaymentOption
        End Get
        Set(value As Integer)
            varSelectedPaymentOption = value
        End Set
    End Property

    Property SelectedAuthoriseOption() As Integer
        Get
            Return varSelectedAuthoriseOption
        End Get
        Set(value As Integer)
            varSelectedAuthoriseOption = value
        End Set
    End Property

    Property SelectedTransferFeeOption() As Integer
        Get
            Return varSelectedTransferFeeOption
        End Get
        Set(value As Integer)
            varSelectedTransferFeeOption = value
        End Set
    End Property

    Property SelectedTemplateName() As String
        Get
            Return varSelectedTemplateName
        End Get
        Set(value As String)
            varSelectedTemplateName = value
        End Set
    End Property

    Property SelectedEmailAttachments() As Boolean
        Get
            Return varSelectedEmailAttachments
        End Get
        Set(value As Boolean)
            varSelectedEmailAttachments = value
        End Set
    End Property

    Property Status() As String
        Get
            Return varStatus
        End Get
        Set(value As String)
            varStatus = value
        End Set
    End Property

    Property PortfolioCode() As Integer
        Get
            Return varPortfolioCode
        End Get
        Set(value As Integer)
            varPortfolioCode = value
        End Set
    End Property

    Property Portfolio() As String
        Get
            Return varPortfolio
        End Get
        Set(value As String)
            varPortfolio = value
        End Set
    End Property

    Property CustomerId() As Integer
        Get
            Return varCustomerId
        End Get
        Set(value As Integer)
            varCustomerId = value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return varCustomerName
        End Get
        Set(value As String)
            varCustomerName = value
        End Set
    End Property

    Property OtherID() As Integer
        Get
            Return varOtherID
        End Get
        Set(value As Integer)
            varOtherID = value
        End Set
    End Property

    Property PortfolioIsCommission() As Boolean
        Get
            Return varPortfolioIsCommission
        End Get
        Set(value As Boolean)
            varPortfolioIsCommission = value
        End Set
    End Property

    Property PortfolioIsFee() As Boolean
        Get
            Return varPortfolioIsFee
        End Get
        Set(value As Boolean)
            varPortfolioIsFee = value
        End Set
    End Property

    Property PortfolioFee() As Double
        Get
            Return varPortfolioFee
        End Get
        Set(value As Double)
            varPortfolioFee = value
        End Set
    End Property

    Property PortfolioFeeCCY() As String
        Get
            Return varPortfolioFeeCCY
        End Get
        Set(value As String)
            varPortfolioFeeCCY = value
        End Set
    End Property

    Property PortfolioFeeInt() As Double
        Get
            Return varPortfolioFeeInt
        End Get
        Set(value As Double)
            varPortfolioFeeInt = value
        End Set
    End Property

    Property PortfolioFeeCCYInt() As String
        Get
            Return varPortfolioFeeCCYInt
        End Get
        Set(value As String)
            varPortfolioFeeCCYInt = value
        End Set
    End Property

    Property PortfolioFeeDue() As Boolean
        Get
            Return varPortfolioFeeDue
        End Get
        Set(value As Boolean)
            varPortfolioFeeDue = value
        End Set
    End Property

    Property PortfolioFeeID() As Integer
        Get
            Return varPortfolioFeeID
        End Get
        Set(value As Integer)
            varPortfolioFeeID = value
        End Set
    End Property

    Property PortfolioFeeAccountCode() As Integer
        Get
            Return varPortfolioFeeAccountCode
        End Get
        Set(value As Integer)
            varPortfolioFeeAccountCode = value
        End Set
    End Property

    Property PortfolioFeeRMSAccountCode() As Integer
        Get
            Return varPortfolioFeeRMSAccountCode
        End Get
        Set(value As Integer)
            varPortfolioFeeRMSAccountCode = value
        End Set
    End Property

    Property CCYCode() As Integer
        Get
            Return varCCYCode
        End Get
        Set(value As Integer)
            varCCYCode = value
        End Set
    End Property

    Property CCY() As String
        Get
            Return varCCY
        End Get
        Set(value As String)
            varCCY = value
        End Set
    End Property

    Property OrderPortfolioShortcut() As String
        Get
            Return varOrderPortfolioShortcut
        End Get
        Set(value As String)
            varOrderPortfolioShortcut = value
        End Set
    End Property

    Property OrderBrokerCode() As Integer
        Get
            Return varOrderBrokerCode
        End Get
        Set(value As Integer)
            varOrderBrokerCode = value
        End Set
    End Property

    Property OrderBrokerShortcut() As String
        Get
            Return varOrderBrokerShortcut
        End Get
        Set(value As String)
            varOrderBrokerShortcut = value
        End Set
    End Property

    Property OrderAccount() As String
        Get
            Return varOrderAccount
        End Get
        Set(value As String)
            varOrderAccount = value
        End Set
    End Property

    Property OrderAccountShortcut() As String
        Get
            Return varOrderAccountShortcut
        End Get
        Set(value As String)
            varOrderAccountShortcut = value
        End Set
    End Property


    Property OrderBalance() As Double
        Get
            Return varOrderBalance
        End Get
        Set(value As Double)
            varOrderBalance = value
        End Set
    End Property

    Property OrderName() As String
        Get
            Return varOrderName
        End Get
        Set(value As String)
            varOrderName = value
        End Set
    End Property

    Property OrderRef() As String
        Get
            Return varOrderRef
        End Get
        Set(value As String)
            varOrderRef = value
        End Set
    End Property

    Property OrderAddress1() As String
        Get
            Return varOrderAddress1
        End Get
        Set(value As String)
            varOrderAddress1 = value
        End Set
    End Property

    Property OrderAddress2() As String
        Get
            Return varOrderAddress2
        End Get
        Set(value As String)
            varOrderAddress2 = value
        End Set
    End Property

    Property OrderZip() As String
        Get
            Return varOrderZip
        End Get
        Set(value As String)
            varOrderZip = value
        End Set
    End Property

    Property OrderCountryID() As Integer
        Get
            Return varOrderCountryID
        End Get
        Set(value As Integer)
            varOrderCountryID = value
        End Set
    End Property

    Property OrderCountry() As String
        Get
            Return varOrderCountry
        End Get
        Set(value As String)
            varOrderCountry = value
        End Set
    End Property

    Property OrderSwift() As String
        Get
            Return varOrderSwift
        End Get
        Set(value As String)
            varOrderSwift = value
        End Set
    End Property

    Property OrderAccountNumber() As String
        Get
            Return varOrderAccountNumber
        End Get
        Set(value As String)
            varOrderAccountNumber = value
        End Set
    End Property

    Property OrderIBAN() As String
        Get
            Return varOrderIBAN
        End Get
        Set(value As String)
            varOrderIBAN = value
        End Set
    End Property

    Property OrderOther() As String
        Get
            Return varOrderOther
        End Get
        Set(value As String)
            varOrderOther = value
        End Set
    End Property

    Property OrderVOCodeID() As Integer
        Get
            Return varOrderVOCodeID
        End Get
        Set(value As Integer)
            varOrderVOCodeID = value
        End Set
    End Property

    Property OrderMessageTypeID() As Integer
        Get
            Return varOrderMessageTypeID
        End Get
        Set(value As Integer)
            varOrderMessageTypeID = value
        End Set
    End Property

    Property OrderFormData As Dictionary(Of String, String)
        Get
            Return varOrderFormData
        End Get
        Set(value As Dictionary(Of String, String))
        End Set
    End Property

    Property BeneficiaryBrokerCode() As Integer
        Get
            Return varBenBrokerCode
        End Get
        Set(value As Integer)
            varBenBrokerCode = value
        End Set
    End Property

    Property BeneficiaryBrokerShortcut() As String
        Get
            Return varBenBrokerShortcut
        End Get
        Set(value As String)
            varBenBrokerShortcut = value
        End Set
    End Property

    Property BeneficiaryAccount() As String
        Get
            Return varBenAccount
        End Get
        Set(value As String)
            varBenAccount = value
        End Set
    End Property

    Property BeneficiaryAccountShortcut() As String
        Get
            Return varBenAccountShortcut
        End Get
        Set(value As String)
            varBenAccountShortcut = value
        End Set
    End Property

    Property BeneficiaryAddressID() As Integer
        Get
            Return varBenAddressID
        End Get
        Set(value As Integer)
            varBenAddressID = value
        End Set
    End Property

    Property BeneficaryAccountFiltered() As Boolean
        Get
            Return varBenAccountFiltered
        End Get
        Set(value As Boolean)
            varBenAccountFiltered = value
        End Set
    End Property

    Property BeneficiaryBalance() As Double
        Get
            Return varBenBalance
        End Get
        Set(value As Double)
            varBenBalance = value
        End Set
    End Property

    Property BeneficiaryTypeID() As Integer
        Get
            Return varBenTypeID
        End Get
        Set(value As Integer)
            varBenTypeID = value
        End Set
    End Property

    Property BeneficiaryType() As String
        Get
            Return varBenType
        End Get
        Set(value As String)
            varBenType = value
        End Set
    End Property

    Property BeneficiaryName() As String
        Get
            Return varBenName
        End Get
        Set(value As String)
            varBenName = value
        End Set
    End Property

    Property BeneficiaryFirstName() As String
        Get
            Return varBenFirstname
        End Get
        Set(value As String)
            varBenFirstname = value
        End Set
    End Property

    Property BeneficiaryMiddleName() As String
        Get
            Return varBenMiddlename
        End Get
        Set(value As String)
            varBenMiddlename = value
        End Set
    End Property

    Property BeneficiarySurname() As String
        Get
            Return varBenSurname
        End Get
        Set(value As String)
            varBenSurname = value
        End Set
    End Property

    Property BeneficiaryRef() As String
        Get
            Return varBenRef
        End Get
        Set(value As String)
            varBenRef = value
        End Set
    End Property

    Property BeneficiaryAddress1() As String
        Get
            Return varBenAddress1
        End Get
        Set(value As String)
            varBenAddress1 = value
        End Set
    End Property

    Property BeneficiaryAddress2() As String
        Get
            Return varBenAddress2
        End Get
        Set(value As String)
            varBenAddress2 = value
        End Set
    End Property

    Property BeneficiaryCounty() As String
        Get
            Return varBenCounty
        End Get
        Set(value As String)
            varBenCounty = value
        End Set
    End Property

    Property BeneficiaryZip() As String
        Get
            Return varBenZip
        End Get
        Set(value As String)
            varBenZip = value
        End Set
    End Property

    Property BeneficiaryCountryID() As Integer
        Get
            Return varBenCountryID
        End Get
        Set(value As Integer)
            varBenCountryID = value
        End Set
    End Property

    Property BeneficiaryCountry() As String
        Get
            Return varBenCountry
        End Get
        Set(value As String)
            varBenCountry = value
        End Set
    End Property

    Property BeneficiarySwift() As String
        Get
            Return varBenSwift
        End Get
        Set(value As String)
            varBenSwift = value
        End Set
    End Property

    Property BeneficiaryAccountNumber() As String
        Get
            Return varBenAccountNumber
        End Get
        Set(value As String)
            varBenAccountNumber = value
        End Set
    End Property

    Property BeneficiaryIBAN() As String
        Get
            Return varBenIBAN
        End Get
        Set(value As String)
            varBenIBAN = value
        End Set
    End Property

    Property BeneficiaryOther() As String
        Get
            Return varBenOther
        End Get
        Set(value As String)
            varBenOther = value
        End Set
    End Property

    Property BeneficiaryBIK() As String
        Get
            Return varBenBIK
        End Get
        Set(value As String)
            varBenBIK = value
        End Set
    End Property

    Property BeneficiaryINN() As String
        Get
            Return varBenINN
        End Get
        Set(value As String)
            varBenINN = value
        End Set
    End Property

    Property BeneficiaryRelationshipID() As Integer
        Get
            Return varBenRelationshipID
        End Get
        Set(value As Integer)
            varBenRelationshipID = value
        End Set
    End Property

    Property BeneficiaryRelationship() As String
        Get
            Return varBenRelationship
        End Get
        Set(value As String)
            varBenRelationship = value
        End Set
    End Property

    Property BeneficiaryPortfolioCode() As Integer
        Get
            Return varBenPortfolioCode
        End Get
        Set(value As Integer)
            varBenPortfolioCode = value
        End Set
    End Property

    Property BeneficiaryPortfolio() As String
        Get
            Return varBenPortfolio
        End Get
        Set(value As String)
            varBenPortfolio = value
        End Set
    End Property

    Property BeneficiaryPortfolioShortcut() As String
        Get
            Return varBenPortfolioShortcut
        End Get
        Set(value As String)
            varBenPortfolioShortcut = value
        End Set
    End Property

    Property BeneficiaryTemplateName() As String
        Get
            Return varBenTemplateName
        End Get
        Set(value As String)
            varBenTemplateName = value
        End Set
    End Property

    Property BeneficiaryCCYCode() As Integer
        Get
            Return varBenCCYCode
        End Get
        Set(value As Integer)
            varBenCCYCode = value
        End Set
    End Property

    Property BeneficiaryCCY() As String
        Get
            Return varBenCCY
        End Get
        Set(value As String)
            varBenCCY = value
        End Set
    End Property

    Property BeneficiaryFormData As Dictionary(Of String, String)
        Get
            Return varBenFormData
        End Get
        Set(value As Dictionary(Of String, String))
        End Set
    End Property

    Property BeneficiarySubBankAddressID() As Integer
        Get
            Return varBenSubBankAddressID
        End Get
        Set(value As Integer)
            varBenSubBankAddressID = value
        End Set
    End Property

    Property BeneficiarySubBankName() As String
        Get
            Return varBenSubBankName
        End Get
        Set(value As String)
            varBenSubBankName = value
        End Set
    End Property

    Property BeneficiarySubBankAddress1() As String
        Get
            Return varBenSubBankAddress1
        End Get
        Set(value As String)
            varBenSubBankAddress1 = value
        End Set
    End Property

    Property BeneficiarySubBankAddress2() As String
        Get
            Return varBenSubBankAddress2
        End Get
        Set(value As String)
            varBenSubBankAddress2 = value
        End Set
    End Property

    Property BeneficiarySubBankZip() As String
        Get
            Return varBenSubBankZip
        End Get
        Set(value As String)
            varBenSubBankZip = value
        End Set
    End Property

    Property BeneficiarySubBankCountryID() As Integer
        Get
            Return varBenSubBankCountryID
        End Get
        Set(value As Integer)
            varBenSubBankCountryID = value
        End Set
    End Property

    Property BeneficiarySubBankCountry() As String
        Get
            Return varBenSubBankCountry
        End Get
        Set(value As String)
            varBenSubBankCountry = value
        End Set
    End Property


    Property BeneficiarySubBankSwift() As String
        Get
            Return varBenSubBankSwift
        End Get
        Set(value As String)
            varBenSubBankSwift = value
        End Set
    End Property

    Property BeneficiarySubBankIBAN() As String
        Get
            Return varBenSubBankIBAN
        End Get
        Set(value As String)
            varBenSubBankIBAN = value
        End Set
    End Property

    Property BeneficiarySubBankOther() As String
        Get
            Return varBenSubBankOther
        End Get
        Set(value As String)
            varBenSubBankOther = value
        End Set
    End Property

    Property BeneficiarySubBankBIK() As String
        Get
            Return varBenSubBankBIK
        End Get
        Set(value As String)
            varBenSubBankBIK = value
        End Set
    End Property

    Property BeneficiarySubBankINN() As String
        Get
            Return varBenSubBankINN
        End Get
        Set(value As String)
            varBenSubBankINN = value
        End Set
    End Property

    Property BeneficiarySubBankFormData As Dictionary(Of String, String)
        Get
            Return varBenSubBankFormData
        End Get
        Set(value As Dictionary(Of String, String))
        End Set
    End Property

    Property BeneficiaryBankAddressID() As Integer
        Get
            Return varBenBankAddressID
        End Get
        Set(value As Integer)
            varBenBankAddressID = value
        End Set
    End Property

    Property BeneficiaryBankName() As String
        Get
            Return varBenBankName
        End Get
        Set(value As String)
            varBenBankName = value
        End Set
    End Property

    Property BeneficiaryBankAddress1() As String
        Get
            Return varBenBankAddress1
        End Get
        Set(value As String)
            varBenBankAddress1 = value
        End Set
    End Property

    Property BeneficiaryBankAddress2() As String
        Get
            Return varBenBankAddress2
        End Get
        Set(value As String)
            varBenBankAddress2 = value
        End Set
    End Property

    Property BeneficiaryBankZip() As String
        Get
            Return varBenBankZip
        End Get
        Set(value As String)
            varBenBankZip = value
        End Set
    End Property

    Property BeneficiaryBankCountryID() As Integer
        Get
            Return varBenBankCountryID
        End Get
        Set(value As Integer)
            varBenBankCountryID = value
        End Set
    End Property

    Property BeneficiaryBankCountry() As String
        Get
            Return varBenBankCountry
        End Get
        Set(value As String)
            varBenBankCountry = value
        End Set
    End Property


    Property BeneficiaryBankSwift() As String
        Get
            Return varBenBankSwift
        End Get
        Set(value As String)
            varBenBankSwift = value
        End Set
    End Property

    Property BeneficiaryBankIBAN() As String
        Get
            Return varBenBankIBAN
        End Get
        Set(value As String)
            varBenBankIBAN = value
        End Set
    End Property

    Property BeneficiaryBankOther() As String
        Get
            Return varBenBankOther
        End Get
        Set(value As String)
            varBenBankOther = value
        End Set
    End Property

    Property BeneficiaryBankBIK() As String
        Get
            Return varBenBankBIK
        End Get
        Set(value As String)
            varBenBankBIK = value
        End Set
    End Property

    Property BeneficiaryBankINN() As String
        Get
            Return varBenBankINN
        End Get
        Set(value As String)
            varBenBankINN = value
        End Set
    End Property

    Property BeneficiaryBankFormData As Dictionary(Of String, String)
        Get
            Return varBenBankFormData
        End Get
        Set(value As Dictionary(Of String, String))
        End Set
    End Property

    Property TransDate() As Date
        Get
            Return varTransDate
        End Get
        Set(value As Date)
            varTransDate = value
        End Set
    End Property

    Property SettleDate() As Date
        Get
            Return varSettleDate
        End Get
        Set(value As Date)
            varSettleDate = value
        End Set
    End Property

    Property PaymentType() As String
        Get
            Return varPaymentType
        End Get
        Set(value As String)
            varPaymentType = value
        End Set
    End Property

    Property Comments() As String
        Get
            Return varComments
        End Get
        Set(value As String)
            varComments = value
        End Set
    End Property

    Property MessageType() As String
        Get
            Return varMessageType
        End Get
        Set(value As String)
            varMessageType = value
        End Set
    End Property

    Property Amount() As Double
        Get
            Return varAmount
        End Get
        Set(value As Double)
            varAmount = value
        End Set
    End Property

    Property PayOptions As Dictionary(Of Integer, String)
        Get
            Return varPayOptions
        End Get
        Set(value As Dictionary(Of Integer, String))

        End Set
    End Property

    Property IsPayOption(ByVal varOption As String) As Boolean
        Get
            If varPayOptions.ContainsValue(varOption) Then
                IsPayOption = True
            Else
                IsPayOption = False
            End If
        End Get
        Set(value As Boolean)

        End Set
    End Property

    Property SendSwift() As Boolean
        Get
            Return varSendSwift
        End Get
        Set(value As Boolean)
            varSendSwift = value
        End Set
    End Property

    Property SendIMS() As Boolean
        Get
            Return varSendIMS
        End Get
        Set(value As Boolean)
            varSendIMS = value
        End Set
    End Property

    Property SendSwift2() As Boolean
        Get
            Return varSendSwift2
        End Get
        Set(value As Boolean)
            varSendSwift2 = value
        End Set
    End Property

    Property SendIMS2() As Boolean
        Get
            Return varSendIMS2
        End Get
        Set(value As Boolean)
            varSendIMS2 = value
        End Set
    End Property

    Property CutOffTime As String
        Get
            Return varCutOfftime
        End Get
        Set(value As String)
            varCutOfftime = value
        End Set
    End Property

    Property CutOffTimeZone As String
        Get
            Return varCutOfftimeZone
        End Get
        Set(value As String)
            varCutOfftimeZone = value
        End Set
    End Property

    Property CutOffTimeGMT As String
        Get
            Return varCutOfftimeGMT
        End Get
        Set(value As String)
            varCutOfftimeGMT = value
        End Set
    End Property

    Property PaymentFilePath As String
        Get
            Return varPaymentFilePath
        End Get
        Set(value As String)
            varPaymentFilePath = value
        End Set
    End Property

    Property TempPaymentFilePath As String
        Get
            Return varTempPaymentFilePath
        End Get
        Set(value As String)
            varTempPaymentFilePath = value
        End Set
    End Property

    Property PaymentFilePathRemote As String
        Get
            Return varPaymentFilePathRemote
        End Get
        Set(value As String)
            varPaymentFilePathRemote = value
        End Set
    End Property

    Property SwiftUrgent As Boolean
        Get
            Return varSwiftUrgent
        End Get
        Set(value As Boolean)
            varSwiftUrgent = value
        End Set
    End Property

    Property SwiftSendRef As Boolean
        Get
            Return varSwiftSendRef
        End Get
        Set(value As Boolean)
            varSwiftSendRef = value
        End Set
    End Property

    Property SwiftReturned As Boolean
        Get
            Return varSwiftReturned
        End Get
        Set(value As Boolean)
            varSwiftReturned = value
        End Set
    End Property

    Property SwiftAcknowledged As Boolean
        Get
            Return varSwiftAcknowledged
        End Get
        Set(value As Boolean)
            varSwiftAcknowledged = value
        End Set
    End Property

    Property SwiftStatus As String
        Get
            Return varSwiftStatus
        End Get
        Set(value As String)
            varSwiftStatus = value
        End Set
    End Property

    Property IMSReturned As Boolean
        Get
            Return varIMSReturned
        End Get
        Set(value As Boolean)
            varIMSReturned = value
        End Set
    End Property

    Property IMSAcknowledged As Boolean
        Get
            Return varIMSAcknowledged
        End Get
        Set(value As Boolean)
            varIMSAcknowledged = value
        End Set
    End Property

    Property IMSStatus As String
        Get
            Return varIMSStatus
        End Get
        Set(value As String)
            varIMSStatus = value
        End Set
    End Property

    Property IMSPayInCode As String
        Get
            Return varIMSPayInCode
        End Get
        Set(value As String)
            varIMSPayInCode = value
        End Set
    End Property

    Property IMSPayOutCode As String
        Get
            Return varIMSPayOutCode
        End Get
        Set(value As String)
            varIMSPayOutCode = value
        End Set
    End Property

    Property ExchangeRate() As Double
        Get
            Return varExchangeRate
        End Get
        Set(value As Double)
            varExchangeRate = value
        End Set
    End Property

    Property UserNameEntered As String
        Get
            Return varUserNameEntered
        End Get
        Set(value As String)
            varUserNameEntered = value
        End Set
    End Property

    Property AboveThreshold As Boolean
        Get
            Return varAbovethreshold
        End Get
        Set(value As Boolean)
            varAbovethreshold = value
        End Set
    End Property

    Property IMSRef As String
        Get
            Return varIMSRef
        End Get
        Set(value As String)
            varIMSRef = value
        End Set
    End Property

    Property SentApproveEmailCompliance As Integer
        Get
            Return varSentApproveEmailCompliance
        End Get
        Set(value As Integer)
            varSentApproveEmailCompliance = value
        End Set
    End Property

    Property PaymentRequested As Boolean
        Get
            Return varPaymentRequested
        End Get
        Set(value As Boolean)
            varPaymentRequested = value
        End Set
    End Property

    Property PaymentRequestedType As Integer
        Get
            Return varPaymentRequestedType
        End Get
        Set(value As Integer)
            varPaymentRequestedType = value
        End Set
    End Property

    Property PaymentRequestedRate As Double
        Get
            Return varPaymentRequestedRate
        End Get
        Set(value As Double)
            varPaymentRequestedRate = value
        End Set
    End Property

    Property PaymentRequestedFrom As String
        Get
            Return varPaymentRequestedFrom
        End Get
        Set(value As String)
            varPaymentRequestedFrom = value
        End Set
    End Property

    Property PaymentRequestedEmail As String
        Get
            Return varPaymentRequestedEmail
        End Get
        Set(value As String)
            varPaymentRequestedEmail = value
        End Set
    End Property

    Property PaymentRequestedBeneficiaryName As String
        Get
            Return varPaymentRequestedBeneficiaryName
        End Get
        Set(value As String)
            varPaymentRequestedBeneficiaryName = value
        End Set
    End Property

    Property PaymentRequestedCheckedSantions() As Boolean
        Get
            Return varPaymentRequestedCheckedSantions
        End Get
        Set(value As Boolean)
            varPaymentRequestedCheckedSantions = value
        End Set
    End Property

    Property PaymentRequestedCheckedPEP() As Boolean
        Get
            Return varPaymentRequestedCheckedPEP
        End Get
        Set(value As Boolean)
            varPaymentRequestedCheckedPEP = value
        End Set
    End Property

    Property PaymentRequestedCheckedCDD() As Boolean
        Get
            Return varPaymentRequestedCheckedCDD
        End Get
        Set(value As Boolean)
            varPaymentRequestedCheckedCDD = value
        End Set
    End Property

    Property PaymentRequestedIssuesDisclosed() As String
        Get
            Return varPaymentRequestedIssuesDisclosed
        End Get
        Set(value As String)
            varPaymentRequestedIssuesDisclosed = value
        End Set
    End Property

    Property PaymentRequestedCheckedBankName() As String
        Get
            Return varPaymentRequestedCheckedBankName
        End Get
        Set(value As String)
            varPaymentRequestedCheckedBankName = value
        End Set
    End Property

    Property PaymentRequestedCheckedBankCountryID() As Integer
        Get
            Return varPaymentRequestedCheckedBankCountryID
        End Get
        Set(value As Integer)
            varPaymentRequestedCheckedBankCountryID = value
        End Set
    End Property

    Property PaymentRequestedCheckedBankCountry() As String
        Get
            Return varPaymentRequestedCheckedBankCountry
        End Get
        Set(value As String)
            varPaymentRequestedCheckedBankCountry = value
        End Set
    End Property

    Property PaymentRequestedKYCFolder() As String
        Get
            Return varPaymentRequestedKYCFolder
        End Get
        Set(value As String)
            varPaymentRequestedKYCFolder = value
        End Set
    End Property

    Property PaymentIDOther() As Double
        Get
            Return varPaymentIDOther
        End Get
        Set(value As Double)
            varPaymentIDOther = value
        End Set
    End Property

    Property BatchPaymentTable() As DataTable
        Get
            Return varBPT
        End Get
        Set(value As DataTable)
            varBPT = value
        End Set
    End Property

    Property CanSendEmail() As Boolean
        Get
            Return varCanSendEmail
        End Get
        Set(value As Boolean)
            varCanSendEmail = value
        End Set
    End Property

    Property PaymentRequestedMMBuySell() As Integer
        Get
            Return varPaymentRequestedMMBuySell
        End Get
        Set(value As Integer)
            varPaymentRequestedMMBuySell = value
        End Set
    End Property

    Property PaymentRequestedMMInstrumentCode() As Integer
        Get
            Return varPaymentRequestedMMInstrumentCode
        End Get
        Set(value As Integer)
            varPaymentRequestedMMInstrumentCode = value
        End Set
    End Property

    Property PaymentRequestedMMInstrument() As String
        Get
            Return varPaymentRequestedMMInstrument
        End Get
        Set(value As String)
            varPaymentRequestedMMInstrument = value
        End Set
    End Property

    Property PaymentRequestedMMDurationType() As Integer
        Get
            Return varPaymentRequestedMMDurationType
        End Get
        Set(value As Integer)
            varPaymentRequestedMMDurationType = value
        End Set
    End Property

    Property PaymentRequestedMMStartDate() As Date
        Get
            Return varPaymentRequestedMMStartDate
        End Get
        Set(value As Date)
            varPaymentRequestedMMStartDate = value
        End Set
    End Property

    Property PaymentRequestedMMEndDate() As Date
        Get
            Return varPaymentRequestedMMEndDate
        End Get
        Set(value As Date)
            varPaymentRequestedMMEndDate = value
        End Set
    End Property

    Property PaymentRequestedMMFirmMove() As Boolean
        Get
            Return varPaymentRequestedMMFirmMove
        End Get
        Set(value As Boolean)
            varPaymentRequestedMMFirmMove = value
        End Set
    End Property

    Public Function PopulatePayOptions(Optional ByVal varPaymentType As String = "") As Integer
        PopulatePayOptions = 0
        If varPaymentType = "" Then
            varPayOptions.Add(0, ClsIMS.GetPaymentType(cPayDescInPort))
            varPayOptions.Add(1, ClsIMS.GetPaymentType(cPayDescIn))
            varPayOptions.Add(2, ClsIMS.GetPaymentType(cPayDescInOther))
            varPayOptions.Add(3, ClsIMS.GetPaymentType(cPayDescExClientIn))
            varPayOptions.Add(4, ClsIMS.GetPaymentType(cPayDescExClientOut))
            varPayOptions.Add(5, ClsIMS.GetPaymentType(cPayDescExOut))
            varPayOptions.Add(6, ClsIMS.GetPaymentType(cPayDescExIn))
            varPayOptions.Add(7, ClsIMS.GetPaymentType(cPayDescInPortLoan))
            varPayOptions.Add(8, ClsIMS.GetPaymentType(cPayDescInMaltaLondon))
            varPayOptions.Add(9, ClsIMS.GetPaymentType(cPayDescInLondonMalta))
            varPayOptions.Add(10, ClsIMS.GetPaymentType(cPayDescExVolopaOut))
        Else
            If varPaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Then
                PopulatePayOptions = 0
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescIn) Then
                PopulatePayOptions = 1
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                PopulatePayOptions = 2
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                PopulatePayOptions = 3
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Then
                PopulatePayOptions = 4
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Then
                PopulatePayOptions = 5
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                PopulatePayOptions = 6
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                PopulatePayOptions = 7
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Then
                PopulatePayOptions = 8
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                PopulatePayOptions = 9
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                PopulatePayOptions = 10
            End If
        End If
    End Function

    Public Sub CreateTransactionsHeaders(ByVal objWriter As IO.StreamWriter)
        objWriter.Write("Date,")
        objWriter.Write("Portfolio Shortcut,")
        objWriter.Write("Security Shortcut,")
        objWriter.Write("Title Currency,")
        objWriter.Write("Quantity,")
        objWriter.Write("Accrual Amount(TI),")
        objWriter.Write("Price (TI),")
        objWriter.Write("Value (TI),")
        objWriter.Write("FX Rate,")
        objWriter.Write("Value (PF),")
        objWriter.Write("Fx Rate(TiCCY),")
        objWriter.Write("Value(TiCCY),")
        objWriter.Write("Comments,")
        objWriter.Write("TDShortcut,")
        objWriter.Write("Counterparty Shortcut,")
        objWriter.Write("Account Shortcut,")
        objWriter.Write("Settlement Date,")
        objWriter.Write("Broker Com (TI),")
        objWriter.Write("Mngmnt Com (TI),")
        objWriter.Write("Expenses (TI),")
        objWriter.Write("Executing Party,")
        objWriter.Write("Title Settlement Broker,")
        objWriter.Write("Cash Settlement Broker,")
        objWriter.Write("TaxAct,")
        objWriter.Write("DocNo,")
        objWriter.Write("SettleRate,")
        objWriter.WriteLine()
    End Sub

    Public Sub CreateTransactionsHeadersIFT(ByVal objWriter As IO.StreamWriter, ByVal varFileDate As Date)
        objWriter.Write("00;") 'rec type - always 00
        objWriter.Write("TRANDC;") ' file type
        objWriter.Write(varFileDate.Date.ToString("yyyy-MM-dd") & ";") ' ref date for data in file
        objWriter.Write(varFileDate.Date.ToString("yyyy-MM-dd") & ";") ' extraction date for data in file
        objWriter.Write(varFileDate.ToString("HH:mm:ss.fff") & ";") ' extraction date for data in file
        objWriter.Write("F;") 'Full or partial data
        objWriter.Write(ClsIMS.GetFileExportName("SourceSystem") & ";") 'source system
        objWriter.Write("001;") 'always 1.0
        objWriter.Write("DM") 'always FU
        objWriter.WriteLine()
    End Sub

    Public Sub CreateTransactionsTrailerIFT(ByVal objWriter As IO.StreamWriter, ByVal varCount As Integer)
        objWriter.Write("99;") 'rec type - always 00
        objWriter.Write(varCount) ' file type
    End Sub

    Public Sub CreateTransactionsHeadersXL(ByVal xlWorkSheet As Interop.Excel.Worksheet)
        xlWorkSheet.Cells(1, 1) = "Date"
        xlWorkSheet.Cells(1, 2) = "Portfolio Shortcut"
        xlWorkSheet.Cells(1, 3) = "Security Shortcut"
        xlWorkSheet.Cells(1, 4) = "Title Currency"
        xlWorkSheet.Cells(1, 5) = "Quantity"
        xlWorkSheet.Cells(1, 6) = "Accrual Amount(TI)"
        xlWorkSheet.Cells(1, 7) = "Price (TI)"
        xlWorkSheet.Cells(1, 8) = "Value (TI)"
        xlWorkSheet.Cells(1, 9) = "FX Rate"
        xlWorkSheet.Cells(1, 10) = "Value (PF)"
        xlWorkSheet.Cells(1, 11) = "Fx Rate(TiCCY)"
        xlWorkSheet.Cells(1, 12) = "Value(TiCCY)"
        xlWorkSheet.Cells(1, 13) = "Comments"
        xlWorkSheet.Cells(1, 14) = "TDShortcut"
        xlWorkSheet.Cells(1, 15) = "Counterparty Shortcut"
        xlWorkSheet.Cells(1, 16) = "Account Shortcut"
        xlWorkSheet.Cells(1, 17) = "Settlement Date"
        xlWorkSheet.Cells(1, 18) = "Broker Com (TI)"
        xlWorkSheet.Cells(1, 19) = "Mngmnt Com (TI)"
        xlWorkSheet.Cells(1, 20) = "Expenses (TI)"
        xlWorkSheet.Cells(1, 21) = "Executing Party"
        xlWorkSheet.Cells(1, 22) = "Title Settlement Broker"
        xlWorkSheet.Cells(1, 23) = "Cash Settlement Broker"
        xlWorkSheet.Cells(1, 24) = "TaxAct"
        xlWorkSheet.Cells(1, 25) = "DocNo"
        xlWorkSheet.Cells(1, 26) = "SettleRate"
    End Sub


    Public Function CreateTransactionsXL() As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim j As Integer

        Try

            Dim FullFilePathXL As String = ClsIMS.GetFilePath("IMSExportXL") & "\IMSXL" & Now.ToString("_yyMMdd_HHmmssfff") & ".xlsx"

            xlApp = New Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")
            CreateTransactionsHeadersXL(xlWorkSheet)

            Dim reader As SqlClient.SqlDataReader = ClsIMS.GetSQLData("select * from vwDolfinPaymentGridView where p_status = '" & ClsIMS.GetStatusType(cStatusReady) & "'")

            j = 2
            If reader.HasRows Then
                While reader.Read
                    ClsIMS.GetReaderItemIntoPaymentClass(reader)
                    If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                        xlWorkSheet.Cells(j, 1) = varTransDate.ToString("yyyy-MM-dd")
                        xlWorkSheet.Cells(j, 2) = varOrderPortfolioShortcut
                        xlWorkSheet.Cells(j, 3) = varOrderAccountShortcut
                        xlWorkSheet.Cells(j, 4) = varCCY
                        xlWorkSheet.Cells(j, 5) = Abs(varAmount)
                        xlWorkSheet.Cells(j, 6) = ""
                        xlWorkSheet.Cells(j, 7) = ""
                        xlWorkSheet.Cells(j, 8) = Abs(varAmount)
                        xlWorkSheet.Cells(j, 9) = "1"
                        xlWorkSheet.Cells(j, 10) = "0.00"
                        xlWorkSheet.Cells(j, 11) = "1"
                        xlWorkSheet.Cells(j, 12) = "0.00"
                        xlWorkSheet.Cells(j, 13) = varComments
                        If varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                            xlWorkSheet.Cells(j, 14) = "INPA"
                        ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                            xlWorkSheet.Cells(j, 14) = "OGP"
                        Else
                            xlWorkSheet.Cells(j, 14) = "CTTO"
                        End If
                        xlWorkSheet.Cells(j, 15) = varOrderBrokerShortcut
                        xlWorkSheet.Cells(j, 16) = varOrderAccountShortcut
                        xlWorkSheet.Cells(j, 17) = varSettleDate.ToString("yyyy-MM-dd")
                        xlWorkSheet.Cells(j, 18) = ""
                        xlWorkSheet.Cells(j, 19) = ""
                        xlWorkSheet.Cells(j, 20) = ""
                        xlWorkSheet.Cells(j, 21) = varOrderBrokerShortcut
                        xlWorkSheet.Cells(j, 22) = varOrderBrokerShortcut
                        xlWorkSheet.Cells(j, 23) = varOrderBrokerShortcut
                        xlWorkSheet.Cells(j, 24) = ""
                        xlWorkSheet.Cells(j, 25) = OrderRef & "_" & Now.ToString("yyyyMMddHHmmssfff")
                        xlWorkSheet.Cells(j, 26) = ""
                        j = j + 1
                    End If

                    If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                        xlWorkSheet.Cells(j, 1) = varTransDate.ToString("yyyy-MM-dd")
                        If varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExIn) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                            xlWorkSheet.Cells(j, 2) = varOrderPortfolioShortcut
                        Else
                            xlWorkSheet.Cells(j, 2) = varBenPortfolioShortcut
                        End If
                        xlWorkSheet.Cells(j, 3) = varBenAccountShortcut
                        xlWorkSheet.Cells(j, 4) = varBenCCY
                        xlWorkSheet.Cells(j, 5) = Abs(varAmount)
                        xlWorkSheet.Cells(j, 6) = ""
                        xlWorkSheet.Cells(j, 7) = ""
                        xlWorkSheet.Cells(j, 8) = Abs(varAmount)
                        xlWorkSheet.Cells(j, 9) = "1"
                        xlWorkSheet.Cells(j, 10) = "0.00"
                        xlWorkSheet.Cells(j, 11) = "1"
                        xlWorkSheet.Cells(j, 12) = "0.00"
                        xlWorkSheet.Cells(j, 13) = varComments
                        If varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                            xlWorkSheet.Cells(j, 14) = "INPA"
                        ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                            xlWorkSheet.Cells(j, 14) = "OGP"
                        Else
                            xlWorkSheet.Cells(j, 14) = "CCTI"
                        End If
                        xlWorkSheet.Cells(j, 15) = varBenBrokerShortcut
                        xlWorkSheet.Cells(j, 16) = varBenAccountShortcut
                        xlWorkSheet.Cells(j, 17) = varSettleDate.ToString("yyyy-MM-dd")
                        xlWorkSheet.Cells(j, 18) = ""
                        xlWorkSheet.Cells(j, 19) = ""
                        xlWorkSheet.Cells(j, 20) = ""
                        xlWorkSheet.Cells(j, 21) = varBenBrokerShortcut
                        xlWorkSheet.Cells(j, 22) = varBenBrokerShortcut
                        xlWorkSheet.Cells(j, 23) = varBenBrokerShortcut
                        xlWorkSheet.Cells(j, 24) = ""
                        xlWorkSheet.Cells(j, 25) = BeneficiaryRef & "_" & Now.ToString("yyyyMMddHHmmssfff")
                        xlWorkSheet.Cells(j, 26) = ""
                        j = j + 1
                    End If
                End While
            End If
            xlWorkSheet.SaveAs(FullFilePathXL)
            xlWorkBook.Close()
            xlApp.Quit()
            CreateTransactionsXL = True
        Catch ex As Exception
            CreateTransactionsXL = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - error in CreateTransactionsXL")
        End Try
    End Function

    Public Function CreateTreasuryTransactionsXL(ByRef dgv As DataGridView, ByRef TransDate As Date) As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim j As Integer, k As Integer

        Try
            ClsPayTreasury.SendSwift = False
            ClsPayTreasury.SendIMS = True

            Dim FullFilePathXL As String = ClsIMS.GetFilePath("IMSExportXL") & "\IMSXLTreasury" & Now.ToString("_yyMMdd_HHmmssfff") & ".xlsx"

            xlApp = New Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            j = 2
            CreateTransactionsHeadersXL(xlWorkSheet)
            For i As Integer = 0 To dgv.Rows.Count - 2
                For k = 1 To 2
                    If IsNumeric(dgv.Rows(i).Cells(2).Value) Then
                        ClsIMS.GetReaderTreasuryItemIntoPaymentClass(dgv, i, FullFilePathXL, TransDate)
                        If k = 1 Then
                            xlWorkSheet.Cells(j, 1) = varTransDate.ToString("yyyy-MM-dd")
                            xlWorkSheet.Cells(j, 2) = varOrderPortfolioShortcut
                            xlWorkSheet.Cells(j, 3) = varOrderAccountShortcut
                            xlWorkSheet.Cells(j, 4) = varCCY
                            xlWorkSheet.Cells(j, 5) = Abs(varAmount)
                            xlWorkSheet.Cells(j, 6) = ""
                            xlWorkSheet.Cells(j, 7) = ""
                            xlWorkSheet.Cells(j, 8) = Abs(varAmount)
                            xlWorkSheet.Cells(j, 9) = "1"
                            xlWorkSheet.Cells(j, 10) = "0.00"
                            xlWorkSheet.Cells(j, 11) = "1"
                            xlWorkSheet.Cells(j, 12) = "0.00"
                            xlWorkSheet.Cells(j, 13) = varComments
                            If varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                                xlWorkSheet.Cells(j, 14) = "INPA"
                            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                                xlWorkSheet.Cells(j, 14) = "OGP"
                            Else
                                xlWorkSheet.Cells(j, 14) = "CTTO"
                            End If
                            xlWorkSheet.Cells(j, 15) = varOrderBrokerShortcut
                            xlWorkSheet.Cells(j, 16) = varOrderAccountShortcut
                            xlWorkSheet.Cells(j, 17) = varSettleDate.ToString("yyyy-MM-dd")
                            xlWorkSheet.Cells(j, 18) = ""
                            xlWorkSheet.Cells(j, 19) = ""
                            xlWorkSheet.Cells(j, 20) = ""
                            xlWorkSheet.Cells(j, 21) = varOrderBrokerShortcut
                            xlWorkSheet.Cells(j, 22) = varOrderBrokerShortcut
                            xlWorkSheet.Cells(j, 23) = varOrderBrokerShortcut
                            xlWorkSheet.Cells(j, 24) = ""
                            xlWorkSheet.Cells(j, 25) = OrderRef & "_" & Now.ToString("yyyyMMddHHmmssfff")
                            xlWorkSheet.Cells(j, 26) = ""
                        Else
                            xlWorkSheet.Cells(j, 1) = varTransDate.ToString("yyyy-MM-dd")
                            xlWorkSheet.Cells(j, 2) = varBenPortfolioShortcut
                            xlWorkSheet.Cells(j, 3) = varBenAccountShortcut
                            xlWorkSheet.Cells(j, 4) = varCCY
                            xlWorkSheet.Cells(j, 5) = Abs(varAmount)
                            xlWorkSheet.Cells(j, 6) = ""
                            xlWorkSheet.Cells(j, 7) = ""
                            xlWorkSheet.Cells(j, 8) = Abs(varAmount)
                            xlWorkSheet.Cells(j, 9) = "1"
                            xlWorkSheet.Cells(j, 10) = "0.00"
                            xlWorkSheet.Cells(j, 11) = "1"
                            xlWorkSheet.Cells(j, 12) = "0.00"
                            xlWorkSheet.Cells(j, 13) = varComments
                            If varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                                xlWorkSheet.Cells(j, 14) = "INPA"
                            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                                xlWorkSheet.Cells(j, 14) = "OGP"
                            Else
                                xlWorkSheet.Cells(j, 14) = "CCTI"
                            End If
                            xlWorkSheet.Cells(j, 15) = varBenBrokerShortcut
                            xlWorkSheet.Cells(j, 16) = varBenAccountShortcut
                            xlWorkSheet.Cells(j, 17) = varSettleDate.ToString("yyyy-MM-dd")
                            xlWorkSheet.Cells(j, 18) = ""
                            xlWorkSheet.Cells(j, 19) = ""
                            xlWorkSheet.Cells(j, 20) = ""
                            xlWorkSheet.Cells(j, 21) = varBenBrokerShortcut
                            xlWorkSheet.Cells(j, 22) = varBenBrokerShortcut
                            xlWorkSheet.Cells(j, 23) = varBenBrokerShortcut
                            xlWorkSheet.Cells(j, 24) = ""
                            xlWorkSheet.Cells(j, 25) = BeneficiaryRef & "_" & Now.ToString("yyyyMMddHHmmssfff")
                            xlWorkSheet.Cells(j, 26) = ""
                        End If
                        j = j + 1
                    End If
                Next
            Next

            xlWorkSheet.SaveAs(FullFilePathXL)
            xlWorkBook.Close()
            xlApp.Quit()
            CreateTreasuryTransactionsXL = True
        Catch ex As Exception
            CreateTreasuryTransactionsXL = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - error in CreateTreasuryTransactionsXL")
        End Try
    End Function

    Public Function CreateExportFilesSwift(ByRef ExportFilesList As Dictionary(Of String, String)) As Boolean
        Dim varFileDate As Date = Now

        CreateExportFilesSwift = True
        If ClsPay.SendSwift Then
            Dim FullFilePath As String = ClsPay.GetFullFilePath(ClsIMS.GetFileExportName("SwiftName"), ExportFilesList, varFileDate)
            'Dim objWriter As IO.StreamWriter = IO.File.CreateText(FullFilePath)

            'If Not ClsIMS.LiveDB Then
            'If Not CreateTransactionLine(FullFilePath, objWriter, ClsIMS.GetFileExportName("SwiftName"), ExportFilesList, True) Then
            'CreateExportFilesSwift = False
            'End If
            '   objWriter.Close()
            'End If
        End If
    End Function

    Public Function CreateExportFilesIMS(ByRef ExportFilesList As Dictionary(Of String, String)) As Boolean
        Dim varFileDate As Date = Now

        CreateExportFilesIMS = True
        If ClsPayAuth.varSendIMS Then
            Dim IMSFile As String = ""
            ClsIMS.GetIMSFile(ClsPayAuth.SelectedPaymentID, IMSFile)
            If IMSFile <> "" Then
                Dim FullFilePath As String = ClsPayAuth.GetFullFilePath(ClsIMS.GetFileExportName("IMSName"), ExportFilesList, varFileDate)
                Dim objWriter As IO.StreamWriter = IO.File.CreateText(FullFilePath)
                Dim varCount As Integer = 0

                'CreateTransactionsHeadersIFT(objWriter, varFileDate)

                'If ClsPayAuth.IMSPayOutCode <> "" Then
                ' If Not CreateTransactionLine(False, FullFilePath, objWriter, ClsIMS.GetFileExportName("IMSName"), ExportFilesList, True) Then
                'CreateExportFilesIMS = False
                'End If
                '   varCount = varCount + 1
                'End If

                'If ClsPayAuth.IMSPayOutCode <> "" And ClsPayAuth.IMSPayInCode <> "" Then
                'objWriter.WriteLine()
                'End If

                'If ClsPayAuth.IMSPayInCode <> "" Then
                'If Not CreateTransactionLine(False, FullFilePath, objWriter, ClsIMS.GetFileExportName("IMSName"), ExportFilesList, False) Then
                'CreateExportFilesIMS = False
                'End If
                '   varCount = varCount + 1
                'End If
                'objWriter.WriteLine()
                'CreateTransactionsTrailerIFT(objWriter, varCount)

                objWriter.Write(IMSFile)

                objWriter.Close()
                ClsIMS.UpdateTransactionRow(ClsPayAuth.SelectedPaymentID, ClsIMS.GetFileExportName("IMSName"), Dir(FullFilePath))
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "ID: " & ClsPayAuth.SelectedPaymentID & ", File: " & FullFilePath, ClsPay.SelectedPaymentID)
            Else
                CreateExportFilesIMS = False
            End If
        End If
    End Function

    Public Function CreateTreasuryExportFiles(ByRef FullFilePath As String, ByRef objWriter As IO.StreamWriter, ByRef SelectedOrder As Boolean) As Boolean
        CreateTreasuryExportFiles = True
        If Not CreateTransactionLine(True, FullFilePath, objWriter, ClsIMS.GetFileExportName("IMSName"), SelectedOrder) Then
            CreateTreasuryExportFiles = False
        End If
    End Function

    Public Function CopyExportFiles(ByRef ExportFilesList As Dictionary(Of String, String), Optional ByRef ExportFileCount As Dictionary(Of String, Integer) = Nothing) As Boolean
        Dim ExportFileName As String = ""
        Dim varSwiftCount As Integer, varIMSCount As Integer
        Try
            If Not IO.Directory.Exists(ClsIMS.GetFilePath("SWIFTExport")) Then
                IO.Directory.CreateDirectory(ClsIMS.GetFilePath("SWIFTExport"))
            End If
            If Not IO.Directory.Exists(ClsIMS.GetFilePath("IMSExport")) Then
                IO.Directory.CreateDirectory(ClsIMS.GetFilePath("IMSExport"))
            End If

            For exportFile As Integer = 0 To ExportFilesList.Count - 1
                ExportFileName = ExportFilesList.Item(ExportFilesList.Keys(exportFile)).ToString
                If InStr(1, ExportFilesList.Keys(exportFile).ToString, ClsIMS.GetFileExportName("SwiftName"), vbTextCompare) <> 0 Then
                    IO.File.Copy(ClsIMS.GetFilePath("SWIFTExport") & ExportFileName, ClsIMS.GetFilePath("Buffer") & ExportFileName)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "File saved to:" & ClsIMS.GetFilePath("Buffer") & Dir(ExportFileName), 0)
                    varSwiftCount = varSwiftCount + 1
                ElseIf InStr(1, ExportFilesList.Keys(exportFile).ToString, ClsIMS.GetFileExportName("IMSName"), vbTextCompare) <> 0 Then
                    IO.File.Copy(ExportFileName, ClsIMS.GetFilePath("IMSExport") & Dir(ExportFileName))
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "Copied file:  " & ExportFileName & " to " & ClsIMS.GetFilePath("IMSExport") & Dir(ExportFileName), 0)
                    varIMSCount = varIMSCount + 1
                End If
            Next

            If Not ExportFileCount Is Nothing Then
                ExportFileCount.Add(ClsIMS.GetFileExportName("SwiftName"), varSwiftCount)
                ExportFileCount.Add(ClsIMS.GetFileExportName("IMSName"), varIMSCount)
            End If
            CopyExportFiles = True
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyExportFiles: Copied all files", 0)
        Catch ex As Exception
            CopyExportFiles = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - CopyExportFiles: could not copy transaction files - exportfilesList(exportfile): " & ExportFileName)
        End Try
    End Function

    Public Function GetFullFilePath(ByVal varFileSelect As String, ByRef ExportFilesList As Dictionary(Of String, String), ByVal varFileDate As Date) As String
        Dim FullFilePath As String = ""
        Dim varFileName As String = ""

        GetFullFilePath = ""
        If varFileSelect = ClsIMS.GetFileExportName("SwiftName") Then
            'varFileName = ClsIMS.GetFileExportName("SwiftName") & varMessageType & "_" & varFileDate.ToString("yyyyMMddHHmmssfff") & ".fin"
            Dim Lst As List(Of String) = ClsIMS.GetExportedFileNames

            For i As Integer = 0 To Lst.Count - 1
                ExportFilesList.Add((varFileSelect & (ExportFilesList.Count + 1).ToString).ToString, Lst.Item(i).ToString)
            Next
        ElseIf varFileSelect = ClsIMS.GetFileExportName("IMSName") Then
            varFileName = ClsIMS.GetFileExportName("IMSName") & varFileDate.ToString("_yyyyMMdd_HHmmssfff") & ".txt"
            FullFilePath = ClsIMS.GetFilePath("Buffer") & varFileName

            If FullFilePath <> "" Then
                ExportFilesList.Add((varFileSelect & (ExportFilesList.Count + 1).ToString).ToString, FullFilePath)

                If Not IO.Directory.Exists(ClsIMS.GetFilePath("Buffer")) Then
                    IO.Directory.CreateDirectory(ClsIMS.GetFilePath("Buffer"))
                End If
                If IO.File.Exists(FullFilePath) Then
                    IO.File.Delete(FullFilePath)
                End If

                GetFullFilePath = FullFilePath
            End If
        End If
    End Function

    Private Function CreateTransactionLine(ByRef Treasury As Boolean, ByRef FullFilePath As String, ByRef objWriter As IO.StreamWriter, ByRef varFileSelect As String, ByRef SelectedOrder As Boolean) As Boolean
        Try
            Dim stLine As String = ""

            If FullFilePath <> "" Then
                If varFileSelect = ClsIMS.GetFileExportName("SwiftName") Then
                    If varPaymentType <> ClsIMS.GetPaymentType(cPayDescExIn) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                        If varMessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID) Then
                            CreateSwiftMT103String(objWriter)
                        ElseIf varMessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID) Then
                            CreateSwiftMT202String(objWriter)
                        End If
                        ClsIMS.UpdateTransactionRow(ClsPay.SelectedPaymentID, ClsIMS.GetFileExportName("SwiftName"), Dir(FullFilePath))
                    End If
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "ID: " & ClsPay.SelectedPaymentID & ", File: " & FullFilePath, ClsPay.SelectedPaymentID)
                ElseIf varFileSelect = ClsIMS.GetFileExportName("IMSName") Then
                    CreateIMSStringIFT(objWriter, SelectedOrder)
                    If Not Treasury And ClsPayAuth.SelectedPaymentID <> 0 Then
                        ClsIMS.UpdateTransactionRow(ClsPayAuth.SelectedPaymentID, ClsIMS.GetFileExportName("IMSName"), Dir(FullFilePath))
                    End If
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "ID: " & ClsPayAuth.SelectedPaymentID & ", File: " & FullFilePath, ClsPay.SelectedPaymentID)
                End If
                CreateTransactionLine = True
            Else
                CreateTransactionLine = False
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Createfile: Fullfilepath is blank. Attempting: " & FullFilePath)
            End If
        Catch ex As Exception
            CreateTransactionLine = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - Createfile: Error in Createfile " & FullFilePath)
        End Try
    End Function

    Private Sub CreateSwiftMT103String(ByRef objWriter As IO.StreamWriter)
        Dim varAcc As String = ""

        Try
            objWriter.Write("{1:F01" & Strings.Left(ClsIMS.GetDolfinAddress("SwiftCode") & "A", 12) & Strings.StrDup(12 - Len(ClsIMS.GetDolfinAddress("SwiftCode") & "A"), "X") & Strings.StrDup(10, "0") & "}")
            If varBenBankName <> "" Then
                objWriter.Write("{2:I103" & Strings.Left(varBenBankSwift, 8) & Strings.StrDup(12 - Len(varBenBankSwift), "X") & IIf(varSwiftUrgent, "U2", "N") & "}")
            Else
                objWriter.Write("{2:I103" & Strings.Left(varOrderSwift, 8) & Strings.StrDup(12 - Len(varOrderSwift), "X") & IIf(varSwiftUrgent, "U2", "N") & "}")
            End If
            objWriter.Write("{3:{108:" & Strings.Left(varOrderRef, 16) & "}}")
            objWriter.Write("{4:" & vbNewLine)  'senders reference
            CreateSwiftStringItem(objWriter, ":20:", varOrderRef)
            objWriter.Write(":23B:CRED" & vbNewLine) 'bank operation code

            Dim varAmountStr As String = Strings.Replace(Strings.Replace(varAmount.ToString(), ",", ""), ".", ",")
            If InStr(1, varAmountStr, ",", vbTextCompare) = 0 Then
                varAmountStr = varAmountStr & ","
            End If

            objWriter.Write(":32A:" & varSettleDate.ToString("yyMMdd") & varCCY & varAmountStr & vbNewLine) ' 'valuedate/currency/interbank settled amount

            If varCCYCode = varBenCCYCode Then
                objWriter.Write(":33B:" & varCCY & varAmountStr & vbNewLine) ' 'currency/instructed amount
            Else
                objWriter.Write(":33B:" & varBenCCY & varAmountStr & vbNewLine) ' 'currency/instructed amount
                objWriter.Write(":36:" & varExchangeRate & vbNewLine) ' 'exchange rate - only shown if currency amount different from senders ccy
            End If

            CreateIBANandAddress(objWriter, ":50K:", varOrderIBAN, "", ClsIMS.GetDolfinAddress("Name"), ClsIMS.GetDolfinAddress("Address"), ClsIMS.GetDolfinAddress("City") & "," & ClsIMS.GetDolfinAddress("PostCode")) 'RMS account & address) 'Ordering Customer (account, name, and address format)
            CreateSwiftStringItem(objWriter, ":52A:", varOrderSwift) 'Ordering Institution (BIC format)
            'CreateIBANandAddress(objWriter, ":52D:", "", varOrderName, varOrderAddress1, varOrderAddress2, varOrderZip) ':52D: 'Ordering Institution (name, and address format)

            'objWriter.Write(":53A:" & vbNewLine) 'Sender's Correspondent (BIC format)

            varAcc = ValidateAccountNumber(varOrderAccountNumber)
            If varAcc <> "" Then
                'CreateSwiftStringItem(objWriter, ":53B:", "/D/" & varAcc) 'Sender's Correspondent (account)
                CreateSwiftStringItem(objWriter, ":53B:", "/" & varAcc) 'Sender's Correspondent (account)
            Else
                varAcc = ""
                varAcc = ValidateAccountNumber(Right(varOrderIBAN, 10))
                If varAcc <> "" Then
                    'CreateSwiftStringItem(objWriter, ":53B:", "/D/" & varAcc) 'Sender's Correspondent (account)
                    CreateSwiftStringItem(objWriter, ":53B:", "/" & varAcc) 'Sender's Correspondent (account)
                ElseIf LTrim(varOrderIBAN) <> "" Then
                    CreateSwiftStringItem(objWriter, ":53B:", "/" & LTrim(varOrderIBAN)) 'Sender's Correspondent (account)
                End If
            End If

            If varBenSubBankName <> "" Then
                CreateSwiftStringItem(objWriter, ":56A:", varBenSubBankSwift) 'Correspondant With Institution (BIC format)
                'CreateIBANandAddress(objWriter, "", varBenSubBankIBAN, varBenSubBankName, varBenSubBankAddress1, varBenSubBankAddress2, varBenSubBankZip) 'Correspondant Customer (account, name, and address format)
            End If

            If varPaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                If varBenSubBankName <> "" And varBenSubBankIBAN <> "" Then
                    'varAcc = ValidateAccountNumber(varBenSubBankIBAN)
                    'If varAcc <> "" Then
                    ' CreateSwiftStringItem(objWriter, ":57A:", "/D/" & varAcc) 'Sender's Correspondent (account)
                    'Else
                    '   varAcc = ""
                    '  varAcc = ValidateAccountNumber(Right(varBenSubBankIBAN, 10))
                    ' If varAcc <> "" Then
                    'CreateSwiftStringItem(objWriter, ":57A:", "/D/" & varAcc) 'Sender's Correspondent (account)
                    'ElseIf LTrim(varBenSubBankIBAN) <> "" Then
                    '   CreateSwiftStringItem(objWriter, ":57A:", "/" & LTrim(varBenSubBankIBAN)) 'Sender's Correspondent (account)
                    'End If
                    'End If
                    CreateSwiftStringItem(objWriter, ":57A:/", varBenSubBankIBAN)
                    CreateSwiftStringItem(objWriter, "", varBenSwift) 'Account With Institution (BIC format)
                Else
                    CreateSwiftStringItem(objWriter, ":57A:", varBenSwift) 'Account With Institution (BIC format)
                End If

                CreateIBANandAddress(objWriter, ":59:", varBenIBAN, "", ClsIMS.GetDolfinAddress("Name"), ClsIMS.GetDolfinAddress("Address"), ClsIMS.GetDolfinAddress("City") & "," & ClsIMS.GetDolfinAddress("PostCode")) 'Beneficiary Customer (account, name, and address format)
            Else
                If varBenBankName <> "" Then
                    If varBenBankSwift <> "" Then
                        CreateSwiftStringItem(objWriter, ":57A:", varBenBankSwift) 'Account With Institution (BIC format)
                    ElseIf varBenSwift <> "" Then
                        CreateSwiftStringItem(objWriter, ":57A:", varBenSwift) 'Account With Institution (BIC format)
                    End If
                    CreateIBANandAddress(objWriter, "", "", varBenBankName, varBenBankAddress1, varBenBankAddress2, varBenBankZip) 'Account with institution (account, name, and address format)
                    CreateIBANandAddress(objWriter, ":59:", IIf(varBenIBAN <> "", varBenIBAN, varBenBankIBAN), varBenName, varBenAddress1, varBenAddress2, varBenZip) 'Beneficiary Customer (account, name, and address format)
                Else
                    CreateSwiftStringItem(objWriter, ":57A:", varBenSwift) 'Account With Institution (BIC format)
                    CreateIBANandAddress(objWriter, ":59:", varBenIBAN, varBenName, varBenAddress1, varBenAddress2, varBenZip) 'Beneficiary Customer (account, name, and address format)
                End If
            End If

            If varSwiftSendRef Then
                CreateSwiftStringItem(objWriter, ":70:", varOrderRef) 'if we want to send receiver our payment reference
            End If

            CreateSwiftStringItem(objWriter, ":71A:", "OUR")

            If varOrderOther <> "" Then
                Dim varOtherStr As String = Left(varOrderOther, 35)

                If Len(varOrderOther) > 35 Then
                    varOtherStr = varOtherStr & vbNewLine & Mid(varOrderOther, 36, 35)
                End If
                If Len(varOrderOther) > 71 Then
                    varOtherStr = varOtherStr & vbNewLine & Mid(varOrderOther, 72, 35)
                End If
                If Len(varOrderOther) > 107 Then
                    varOtherStr = varOtherStr & vbNewLine & Mid(varOrderOther, 108, 35)
                End If
                If Len(varOrderOther) > 143 Then
                    varOtherStr = varOtherStr & vbNewLine & Mid(varOrderOther, 144, 35)
                End If

                CreateSwiftStringItem(objWriter, ":72:", varOtherStr)
            End If

            'objWriter.Write(":71A:" & vbNewLine) 'Details of Charges
            'If transfer fee then add ClsPay.PortfolioFee
            'objWriter.Write(":71F:" & ClsPay.PortfolioFee & vbNewLine) 'Sender's Charges
            'objWriter.Write(":71G:" & vbNewLine) 'Receiver's Charges

            objWriter.Write("-}")
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in CreateSwiftMT103String")
        End Try
    End Sub

    Private Function ValidateAccountNumber(ByVal varAcc As String) As String
        Dim TempValidateAccountNumber As String = ""

        ValidateAccountNumber = ""
        TempValidateAccountNumber = Right(LTrim(Replace(Replace(Replace(Replace(Replace(varAcc, " ", ""), "-", ""), ".", ""), "/", ""), "\", "")), 10)
        If IsNumeric(TempValidateAccountNumber) And Len(TempValidateAccountNumber) = 10 Then
            ValidateAccountNumber = TempValidateAccountNumber
        End If
    End Function

    Private Sub CreateSwiftStringItem(ByRef objWriter As IO.StreamWriter, ByRef varIDString As String, ByRef varItem As String)
        If LTrim(varItem) <> "" Then
            objWriter.Write(varIDString & Strings.UCase(Replace(Replace(varItem, "-", ""), " ", "")) & vbNewLine)
        End If
    End Sub

    Private Sub CreateIBANandAddress(ByRef objWriter As IO.StreamWriter, ByRef varIDString As String, ByRef varIBAN As String, ByRef varName As String, ByRef varA1 As String, ByRef varA2 As String, ByRef varAZip As String)
        If varIBAN <> "" Or varA1 <> "" Then
            objWriter.Write(varIDString)
            If LTrim(varIBAN) <> "" Then
                objWriter.Write("/" & Strings.UCase(Replace(Replace(varIBAN, "-", ""), " ", "")) & vbNewLine)
            End If
            If LTrim(varName) <> "" Then
                objWriter.Write(Strings.UCase(Strings.Left(varName, 35)) & vbNewLine)
            End If
            If LTrim(varA1) <> "" Then
                objWriter.Write(Strings.UCase(Strings.Left(varA1, 35)) & vbNewLine)
            End If
            If LTrim(varA2) <> "" Then
                objWriter.Write(Strings.UCase(Strings.Left(varA2, 35)) & vbNewLine)
            End If
            If LTrim(varAZip) <> "" Then
                objWriter.Write(Strings.UCase(Strings.Left(varAZip, 35)) & vbNewLine)
            End If
        End If
    End Sub

    Private Sub CreateSwiftMT202String(ByRef objWriter As IO.StreamWriter)
        Try
            objWriter.Write("{1:F01" & Strings.Left(varOrderSwift, 12) & Strings.StrDup(12 - Len(varOrderSwift), "X") & Strings.StrDup(10, "0") & "}")
            objWriter.Write("{2:I202" & Strings.Left(varBenSwift, 12) & Strings.StrDup(12 - Len(varOrderSwift), "X") & IIf(varSwiftUrgent, "U2", "N") & "}")
            objWriter.Write("{3:{108:" & varOrderRef & "}}")
            objWriter.Write("{4:" & vbNewLine)  'senders reference
            CreateSwiftStringItem(objWriter, ":20:", varOrderRef) 'Transaction Reference Number
            CreateSwiftStringItem(objWriter, ":21:", varBenRef) 'Related Reference
            objWriter.Write(":32A:" & varSettleDate.ToString("yyMMdd") & varCCY & Strings.Replace(Strings.Replace(varAmount, ",", ""), ".", ",") & vbNewLine) 'valuedate/currency/interbank settled amount
            CreateSwiftStringItem(objWriter, ":57A:", varOrderSwift) 'Account With Institution (BIC format)
            CreateIBANandAddress(objWriter, ":57D:", varOrderIBAN, varOrderName, varOrderAddress1, varOrderAddress2, varOrderZip) 'Order Customer (account, name, and address format)
            CreateSwiftStringItem(objWriter, ":58A:", varBenSwift) 'Beneficiary Institution (BIC format)
            CreateIBANandAddress(objWriter, ":58D:", varBenIBAN, varBenName, varBenAddress1, varBenAddress2, varBenZip) 'Beneficiary Institution (name, and address)
            If varOrderOther <> "" Then
                CreateSwiftStringItem(objWriter, ":72:", Left(varOrderOther, 35) & vbNewLine & Mid(varOrderOther, 36, 35) & vbNewLine & Mid(varOrderOther, 72, 35) & vbNewLine & Mid(varOrderOther, 108, 35) & vbNewLine & Mid(varOrderOther, 144, 35))
            End If
            objWriter.Write("-}")
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in CreateSwiftMT202String")
        End Try
    End Sub

    Private Sub CreateIMSString(ByRef objWriter As IO.StreamWriter, ByRef SelectedOrder As Boolean)
        Dim stLine As String = ""

        Try
            objWriter.Write(varTransDate.ToString("yyyy-MM-dd") & ",") ' date

            If SelectedOrder Then
                objWriter.Write(varOrderPortfolioShortcut & ",") 'Portfolio Shortcut
                objWriter.Write(varOrderAccountShortcut & ",") 'Security Shortcut
            Else
                objWriter.Write(varBenPortfolioShortcut & ",") 'Portfolio Shortcut
                objWriter.Write(varBenAccountShortcut & ",") 'Security Shortcut
            End If

            objWriter.Write(varCCY & ",") 'Title Currency
            objWriter.Write(Abs(varAmount) & ",") ' Quantity
            objWriter.Write(",") ' Accrual Amount(TI)
            objWriter.Write(",") ' Price (TI)
            objWriter.Write(Abs(varAmount) & ",") ' Value (TI)
            objWriter.Write("1,") ' FX Rate
            objWriter.Write("0.00,") ' Value (PF)
            objWriter.Write("1,") ' Fx Rate(TiCCY)
            objWriter.Write("0.00,") ' Value(TiCCY)

            Dim append = If(varComments.Contains(","), String.Format("""{0}""", varComments), varComments)
            stLine = String.Format("{0}{1},", stLine, append)
            objWriter.Write(stLine) 'comments

            If varPaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                objWriter.Write("INPA,") 'INPA
            ElseIf varPaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                objWriter.Write("OGP,") 'OGP
            Else
                If SelectedOrder Then
                    objWriter.Write("CTTO,") 'Cash Internal Custodian Out
                Else
                    objWriter.Write("CCTI,") 'Cash Internal Custodian In 
                End If
            End If

            If SelectedOrder Then
                objWriter.Write(varOrderBrokerShortcut & ",") 'Counterparty Shortcut
                objWriter.Write(varOrderAccountShortcut & ",") 'Account Shortcut
            Else
                objWriter.Write(varBenBrokerShortcut & ",") 'Counterparty Shortcut
                objWriter.Write(varBenAccountShortcut & ",") 'Account Shortcut
            End If

            objWriter.Write(varSettleDate.ToString("yyyy-MM-dd") & ",") ' Settlement Date

            objWriter.Write(",") 'Broker Com (TI)
            objWriter.Write(",") 'Mngmnt Com (TI)
            objWriter.Write(",") 'Expenses (TI)

            If SelectedOrder Then
                objWriter.Write(varOrderBrokerShortcut & ",") 'Executing Party
                objWriter.Write(varOrderBrokerShortcut & ",") 'Title Settlement Broker
                objWriter.Write(varOrderBrokerShortcut & ",") 'Cash Settlement Broker
            Else
                objWriter.Write(varBenBrokerShortcut & ",") 'Executing Party
                objWriter.Write(varBenBrokerShortcut & ",") 'Title Settlement Broker
                objWriter.Write(varBenBrokerShortcut & ",") 'Cash Settlement Broker
            End If

            objWriter.Write(",") ' TaxAct
            If SelectedOrder Then
                objWriter.Write(OrderRef & Now.ToString("_yyyyMMdd_HHmmssfff") & ",") 'DocNo
            Else
                objWriter.Write(BeneficiaryRef & Now.ToString("_yyyyMMdd_HHmmssfff") & ",") 'DocNo
            End If
            objWriter.Write(",") 'SettleRate
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in CreateIMSString")
        End Try
    End Sub

    Private Sub CreateIMSStringIFT(ByRef objWriter As IO.StreamWriter, ByRef SelectedOrder As Boolean)
        Dim stLine As String = ""

        Try
            objWriter.Write("01;") ' record layout code

            If SelectedOrder Then
                objWriter.Write(OrderRef & ";") 'TranId
            Else
                objWriter.Write(BeneficiaryRef & ";") 'TranId
            End If

            objWriter.Write("6;") ' ProdCode
            objWriter.Write("IF;") ' ActCode
            objWriter.Write("CS;") ' EventStatus

            If SelectedOrder Then
                objWriter.Write(varIMSPayOutCode & ";") 'Event
            Else
                objWriter.Write(varIMSPayInCode & ";") 'Event
            End If

            objWriter.Write(varSettleDate.ToString("yyyy-MM-dd") & ";") ' TranDate (date portfolio is updated)

            If SelectedOrder Then
                objWriter.Write(varOrderPortfolioShortcut & ";") 'Unique portfolio code (shortcut)
                objWriter.Write(varOrderAccountShortcut & ";") 'Unique account code (shortcut)
                objWriter.Write(varCCY & ";") 'Currency code (eg. EUR, USD, GBP)
                objWriter.Write(varOrderBrokerShortcut & ";") 'Unique bank code (shortcut)
            Else
                objWriter.Write(varBenPortfolioShortcut & ";") 'Unique portfolio code (shortcut)
                objWriter.Write(varBenAccountShortcut & ";") 'Unique account code (shortcut)
                objWriter.Write(varBenCCY & ";") 'Currency code (eg. EUR, USD, GBP)
                objWriter.Write(varBenBrokerShortcut & ";") 'Unique bank code (shortcut)
            End If

            objWriter.Write(Abs(varAmount) & ";") ' Transaction amount in account currency

            If SelectedOrder Then
                objWriter.Write(";") 'INFO only
                objWriter.Write(OrderRef & ";") 'INFO Only. Can be the transaction Ref.No of the source system as displayed to end-users
                objWriter.Write(";") 'INFO only: It be used to group related transactions. The i/f won't process this info (except from persisting it).
            Else
                objWriter.Write(";") 'INFO only
                objWriter.Write(BeneficiaryRef & ";") 'INFO Only. Can be the transaction Ref.No of the source system as displayed to end-users
                objWriter.Write(";") 'INFO only: It be used to group related transactions. The i/f won't process this info (except from persisting it).
            End If

            objWriter.Write(Replace(Replace(Replace(Replace(Left(varComments, 255), ",", ""), """", ""), "'", ""), ";", "")) 'Free narrative field

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - error in CreateIMSStringIFT")
        End Try
    End Sub

    Public Function SubmitTransactions(ByRef dgv As DataGridView, ByVal PayCheck As Integer) As Boolean
        Dim Msg As String = ""
        Dim ExportFileCount As New Dictionary(Of String, Integer)

        SubmitTransactions = False

        If ClsIMS.AvailableSubmissions Then
            Dim varResponse As Integer = MsgBox("All authorised transactions (Ready as status) will be submitted." & vbNewLine & "Click yes to confirm sending authorised transactions?", vbInformation + vbYesNo, "Send transactions?")
            If varResponse = vbYes Then
                ClsIMS.CreateTransactionsIMS()
                If ClsIMS.CreateTransactionsSwift(ExportFileCount) Then
                    Msg = "Files Exported:" & vbNewLine & "Buffer directory: " & ClsIMS.GetFilePath("Buffer") & vbNewLine & vbNewLine
                    Msg = Msg & "Swift Files exported to " & vbNewLine & ClsIMS.GetFilePath("SWIFTExport") & vbNewLine & vbNewLine
                    Msg = Msg & "IMS Files will be sent once swift has been confirmed." & vbNewLine & vbNewLine & "If all transactions were not imported, Please check pending items for any issues"
                    MsgBox(Msg, vbInformation, "Exported")
                    ClsIMS.RefreshMovementGrid(dgv, "")
                    SubmitTransactions = True
                Else
                    MsgBox("There was a problem creating the transactions. Please contact systems support for assistance", vbCritical, "Could not send transactions")
                    ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Problem with creating transactions")
                End If
                ClsPay.SelectedPaymentID = 0
            End If
        Else
            MsgBox("There are no transactions ready to be submitted", MsgBoxStyle.Exclamation, "No transactions")
        End If
    End Function

    Public Sub SubmitTreasuryTransactions(ByRef dgv As DataGridView, ByRef TransDate As Date)
        Dim Msg As String = ""
        Dim ExportFileCount As New Dictionary(Of String, Integer)

        Dim varResponse As Integer = MsgBox("All internal movements will be submitted." & vbNewLine & "Click yes to confirm sending movements", vbInformation + vbYesNo, "Make movements")
        If varResponse = vbYes Then
            If ClsIMS.CreateTreasuryTransactions(dgv, ExportFileCount, TransDate) Then
                Msg = "Files Exported:" & vbNewLine & "Buffer directory: " & ClsIMS.GetFilePath("Buffer") & vbNewLine & vbNewLine
                If ExportFileCount.ContainsKey(ClsIMS.GetFileExportName("IMSName")) Then
                    Msg = Msg & ExportFileCount.Item(ClsIMS.GetFileExportName("IMSName")) & " IMS Files copied to: " & ClsIMS.GetFilePath("IMSExport") & vbNewLine
                Else
                    Msg = Msg & "0 IMS Files copied to: " & ClsIMS.GetFilePath("IMSExport") & vbNewLine
                End If
                MsgBox(Msg, vbInformation, "Exported")
            Else
                MsgBox("There was a problem creating the transactions. Please contact systems support for assistance", vbCritical, "Could not process movements")
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - Problem with creating omnibus switch movements")
            End If
        End If
    End Sub

    Public Function SubmitTransactionsBatch(ByRef dgv As DataGridView) As Boolean
        Dim Msg As String = ""
        Dim ExportFileCount As New Dictionary(Of String, Integer)

        SubmitTransactionsBatch = False

        If ClsIMS.AvailableSubmissionsBatch Then
            Dim varResponse As Integer = MsgBox("All authorised transactions (Batch Ready as status) will be submitted." & vbNewLine & "Click yes to confirm sending authorised transactions?", vbInformation + vbYesNo, "Send transactions?")
            If varResponse = vbYes Then
                If ClsIMS.CreateTransactionsSwiftBatch() Then
                    Msg = "Batch Ready files have been sent"
                    MsgBox(Msg, vbInformation, "Exported")
                    ClsIMS.RefreshMovementGrid(dgv, "")
                    SubmitTransactionsBatch = True
                Else
                    MsgBox("There was a problem sending the transactions. Please contact systems support for assistance", vbCritical, "Could not send transactions")
                    ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Problem with creating transactions (SubmitTransactionsBatch)")
                End If
                ClsPay.SelectedPaymentID = 0
            End If
        Else
            MsgBox("There are no batch transactions ready to be submitted", MsgBoxStyle.Exclamation, "No batch transactions")
        End If
    End Function

    Public Sub GetGridValuesintoClassPayment(ByRef Cls As ClsPayment, ByRef dgv As DataGridView)
        Try
            Cls.SelectedPaymentID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_ID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_ID").Index).Value)
            Cls.Status = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Status").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Status").Index).Value)
            Cls.PaymentType = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PaymentType").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_PaymentType").Index).Value)
            Cls.MessageType = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_MessageType").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_MessageType").Index).Value)
            Cls.PortfolioCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_PortfolioCode").Index).Value)
            Cls.Portfolio = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Portfolio").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Portfolio").Index).Value)
            Cls.TransDate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_TransDate").Index).Value), Now, dgv.SelectedCells(dgv.Columns("P_TransDate").Index).Value)
            Cls.SettleDate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SettleDate").Index).Value), Now, dgv.SelectedCells(dgv.Columns("P_SettleDate").Index).Value)
            Cls.Amount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Amount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Amount").Index).Value)
            Cls.CCYCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_CCYCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_CCYCode").Index).Value)
            Cls.CCY = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_CCY").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_CCY").Index).Value)
            Cls.OrderBrokerCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderBrokerCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_OrderBrokerCode").Index).Value)
            Cls.OrderAccountCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccountCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_OrderAccountCode").Index).Value)
            Cls.OrderAccount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAccount").Index).Value)
            Cls.OrderName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderName").Index).Value)
            Cls.OrderRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderRef").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderRef").Index).Value)
            Cls.OrderAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAddress1").Index).Value)
            Cls.OrderAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAddress2").Index).Value)
            Cls.OrderZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderZip").Index).Value)
            Cls.OrderSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderSwift").Index).Value)
            Cls.OrderAccountNumber = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccountNumber").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAccountNumber").Index).Value)
            Cls.OrderIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderIBAN").Index).Value)
            Cls.OrderOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderOther").Index).Value)
            Cls.BeneficiaryBrokerCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBrokerCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenBrokerCode").Index).Value)
            Cls.BeneficiaryAccountCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccountCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenAccountCode").Index).Value)
            Cls.BeneficiaryAccount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAccount").Index).Value)
            Cls.BeneficiaryAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenAddressID").Index).Value)
            Cls.BeneficiaryName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenName").Index).Value)
            Cls.BeneficiaryRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenRef").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenRef").Index).Value)
            Cls.BeneficiaryAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAddress1").Index).Value)
            Cls.BeneficiaryAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAddress2").Index).Value)
            Cls.BeneficiaryZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenZip").Index).Value)
            Cls.BeneficiarySwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSwift").Index).Value)
            Cls.BeneficiaryAccountNumber = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccountNumber").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAccountNumber").Index).Value)
            Cls.BeneficiaryIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenIBAN").Index).Value)
            Cls.BeneficiaryOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenOther").Index).Value)
            Cls.BeneficiaryPortfolioCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenPortfolioCode").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenPortfolioCode").Index).Value)
            Cls.BeneficiaryPortfolio = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenPortfolio").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenPortfolio").Index).Value)
            Cls.BeneficiaryCCYCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenCCYCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenCCYCode").Index).Value)
            Cls.BeneficiaryCCY = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenCCY").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenCCY").Index).Value)
            Cls.ExchangeRate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_ExchangeRate").Index).Value), 1, dgv.SelectedCells(dgv.Columns("P_ExchangeRate").Index).Value)
            Cls.BeneficiarySubBankAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenSubBankAddressID").Index).Value)
            Cls.BeneficiarySubBankName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankName").Index).Value)
            Cls.BeneficiarySubBankAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress1").Index).Value)
            Cls.BeneficiarySubBankAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress2").Index).Value)
            Cls.BeneficiarySubBankZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankZip").Index).Value)
            Cls.BeneficiarySubBankSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankSwift").Index).Value)
            Cls.BeneficiarySubBankIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankIBAN").Index).Value)
            Cls.BeneficiarySubBankOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankOther").Index).Value)
            Cls.BeneficiaryBankAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenBankAddressID").Index).Value)
            Cls.BeneficiaryBankName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankName").Index).Value)
            Cls.BeneficiaryBankAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankAddress1").Index).Value)
            Cls.BeneficiaryBankAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankAddress2").Index).Value)
            Cls.BeneficiaryBankZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankZip").Index).Value)
            Cls.BeneficiaryBankSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankSwift").Index).Value)
            Cls.BeneficiaryBankIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankIBAN").Index).Value)
            Cls.BeneficiaryBankOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankOther").Index).Value)
            Cls.Comments = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Comments").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Comments").Index).Value)
            Cls.SendSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SendSwift").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SendSwift").Index).Value)
            Cls.SendIMS = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SendIMS").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SendIMS").Index).Value)
            Cls.SwiftUrgent = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftUrgent").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftUrgent").Index).Value)
            Cls.SwiftSendRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftSendref").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftSendref").Index).Value)
            Cls.PortfolioFeeID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioFeeID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_PortfolioFeeID").Index).Value)
            Cls.PortfolioIsFee = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioIsFee").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_PortfolioIsFee").Index).Value)
            Cls.PortfolioIsCommission = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioIsCommission").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_PortfolioIsCommission").Index).Value)
            Cls.PaymentFilePath = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PaymentFilePath").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_PaymentFilePath").Index).Value)
            Cls.SwiftReturned = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftReturned").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftReturned").Index).Value)
            Cls.SwiftAcknowledged = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftAcknowledged").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftAcknowledged").Index).Value)
            Cls.SwiftStatus = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftStatus").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_SwiftStatus").Index).Value)
            Cls.IMSReturned = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSReturned").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_IMSReturned").Index).Value)
            Cls.IMSAcknowledged = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSAcknowledged").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_IMSAcknowledged").Index).Value)
            Cls.IMSStatus = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSStatus").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_IMSStatus").Index).Value)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Problem with getgridvaluesintoclass")
        End Try
    End Sub

    Public Sub GetGridValuesintoClass(ByRef dgv As DataGridView)
        Try
            varSelectedPaymentID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_ID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_ID").Index).Value)
            varStatus = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Status").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Status").Index).Value)
            varPaymentType = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PaymentType").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_PaymentType").Index).Value)
            varMessageType = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_MessageType").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_MessageType").Index).Value)
            varPortfolioCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_PortfolioCode").Index).Value)
            varPortfolio = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Portfolio").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Portfolio").Index).Value)
            varTransDate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_TransDate").Index).Value), Now, dgv.SelectedCells(dgv.Columns("P_TransDate").Index).Value)
            varSettleDate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SettleDate").Index).Value), Now, dgv.SelectedCells(dgv.Columns("P_SettleDate").Index).Value)
            varAmount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Amount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Amount").Index).Value)
            varCCYCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_CCYCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_CCYCode").Index).Value)
            varCCY = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_CCY").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_CCY").Index).Value)
            varOrderBrokerCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderBrokerCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_OrderBrokerCode").Index).Value)
            varOrderAccountCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccountCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_OrderAccountCode").Index).Value)
            varOrderAccount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAccount").Index).Value)
            varOrderName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderName").Index).Value)
            varOrderRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderRef").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderRef").Index).Value)
            varOrderAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAddress1").Index).Value)
            varOrderAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAddress2").Index).Value)
            varOrderZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderZip").Index).Value)
            varOrderSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderSwift").Index).Value)
            varOrderAccountNumber = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderAccountNumber").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderAccountNumber").Index).Value)
            varOrderIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderIBAN").Index).Value)
            varOrderOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_OrderOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_OrderOther").Index).Value)
            varBenBrokerCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBrokerCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenBrokerCode").Index).Value)
            varBenAccountCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccountCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenAccountCode").Index).Value)
            varBenAccount = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccount").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAccount").Index).Value)
            varBenAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenAddressID").Index).Value)
            varBenName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenName").Index).Value)
            varBenRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenRef").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenRef").Index).Value)
            varBenAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAddress1").Index).Value)
            varBenAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAddress2").Index).Value)
            varBenZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenZip").Index).Value)
            varBenSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSwift").Index).Value)
            varBenAccountNumber = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenAccountNumber").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenAccountNumber").Index).Value)
            varBenIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenIBAN").Index).Value)
            varBenOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenOther").Index).Value)
            varBenPortfolioCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenPortfolioCode").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenPortfolioCode").Index).Value)
            varBenPortfolio = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenPortfolio").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenPortfolio").Index).Value)
            varCCYCode = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenCCYCode").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenCCYCode").Index).Value)
            varBenCCY = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenCCY").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenCCY").Index).Value)
            varExchangeRate = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_ExchangeRate").Index).Value), 1, dgv.SelectedCells(dgv.Columns("P_ExchangeRate").Index).Value)
            varBenSubBankAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenSubBankAddressID").Index).Value)
            varBenSubBankName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankName").Index).Value)
            varBenSubBankAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress1").Index).Value)
            varBenSubBankAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankAddress2").Index).Value)
            varBenSubBankZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankZip").Index).Value)
            varBenSubBankSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankSwift").Index).Value)
            varBenSubBankIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankIBAN").Index).Value)
            varBenSubBankOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenSubBankOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenSubBankOther").Index).Value)
            varBenBankAddressID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddressID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_BenBankAddressID").Index).Value)
            varBenBankName = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankName").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankName").Index).Value)
            varBenBankAddress1 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddress1").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankAddress1").Index).Value)
            varBenBankAddress2 = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankAddress2").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankAddress2").Index).Value)
            varBenBankZip = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankZip").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankZip").Index).Value)
            varBenBankSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankSwift").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankSwift").Index).Value)
            varBenBankIBAN = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankIBAN").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankIBAN").Index).Value)
            varBenBankOther = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_BenBankOther").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_BenBankOther").Index).Value)
            varComments = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_Comments").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_Comments").Index).Value)
            varSendSwift = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SendSwift").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SendSwift").Index).Value)
            varSendIMS = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SendIMS").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SendIMS").Index).Value)
            varSwiftUrgent = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftUrgent").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftUrgent").Index).Value)
            varSwiftSendRef = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftSendref").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftSendref").Index).Value)
            varPortfolioFeeID = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioFeeID").Index).Value), 0, dgv.SelectedCells(dgv.Columns("P_PortfolioFeeID").Index).Value)
            varPortfolioIsFee = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioIsFee").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_PortfolioIsFee").Index).Value)
            varPortfolioIsCommission = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PortfolioIsCommission").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_PortfolioIsCommission").Index).Value)
            varPaymentFilePath = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PaymentFilePath").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_PaymentFilePath").Index).Value)
            varPaymentFilePathRemote = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_PaymentFilePathRemote").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_PaymentFilePathRemote").Index).Value)
            varSwiftReturned = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftReturned").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftReturned").Index).Value)
            varSwiftAcknowledged = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftAcknowledged").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_SwiftAcknowledged").Index).Value)
            varSwiftStatus = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_SwiftStatus").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_SwiftStatus").Index).Value)
            varIMSReturned = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSReturned").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_IMSReturned").Index).Value)
            varIMSAcknowledged = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSAcknowledged").Index).Value), False, dgv.SelectedCells(dgv.Columns("P_IMSAcknowledged").Index).Value)
            varIMSStatus = IIf(IsDBNull(dgv.SelectedCells(dgv.Columns("P_IMSStatus").Index).Value), "", dgv.SelectedCells(dgv.Columns("P_IMSStatus").Index).Value)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - Problem with getgridvaluesintoclass")
        End Try
    End Sub

    Public Sub EmailAuthorisedUsers()
        Try
            ClsIMS.SendEmail("david.harper@rmscapmarkets.com;david.harper@rmscapmarkets.com;", "test body3", "test subject3")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub OpenOutlookEmail(ByVal varAttachments As Boolean)
        Dim CreateBody As String = ""

        Try
            If varSelectedPaymentID = 0 Then
                ClsIMS.GetIDFromDB("DolfinPayment")
            End If
            CreateBody = "ID: " & varSelectedPaymentID & vbNewLine
            CreateBody = CreateBody & "Movement Type: " & varPaymentType & vbNewLine
            CreateBody = CreateBody & "Portfolio: " & varPortfolio & vbNewLine
            If (varPaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                     varPaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                     varPaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And varBenPortfolio <> "" Then
                CreateBody = CreateBody & "Portfolio Beneficiary: " & varBenPortfolio & vbNewLine
            End If
            CreateBody = CreateBody & "CCY: " & varCCY & vbNewLine
            CreateBody = CreateBody & "Amount: " & varAmount & vbNewLine
            CreateBody = CreateBody & "Transaction Date: " & varTransDate & vbNewLine
            CreateBody = CreateBody & "Settlement Date: " & varSettleDate & vbNewLine
            CreateBody = CreateBody & "Reference: " & varOrderRef & vbNewLine
            If varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                varPaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                     varOrderAccount <> "" Then
                CreateBody = CreateBody & "Order Account: " & varOrderAccount & vbNewLine
            ElseIf (varPaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                     varPaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or varPaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                     varPaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And varOrderName <> "" Then
                CreateBody = CreateBody & "Order Account: " & varOrderName & vbNewLine
            End If
            If (varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    varPaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                        varPaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And varOrderName <> "" Then
                CreateBody = CreateBody & "Order Name: " & varOrderName & vbNewLine
            End If
            If varOrderSwift <> "" Then
                CreateBody = CreateBody & "Order Swift: " & varOrderSwift & vbNewLine
            End If
            If varOrderIBAN <> "" Then
                CreateBody = CreateBody & "Order IBAN: " & varOrderIBAN & vbNewLine
            End If
            If varBenRef <> "" Then
                CreateBody = CreateBody & "Beneficiary Ref: " & varBenRef & vbNewLine
            End If
            If (varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    varPaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                        varPaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And varBenAccount <> "" Then
                CreateBody = CreateBody & "Beneficiary Account: " & varBenAccount & vbNewLine
            ElseIf (varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    varPaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                        varPaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And varBenName <> "" Then
                CreateBody = CreateBody & "Beneficiary Account: " & varBenName & vbNewLine
            End If
            If (varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    varPaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And varPaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                        varPaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And varBenName <> "" Then
                CreateBody = CreateBody & "Beneficiary Name: " & varBenName & vbNewLine
            End If
            If varBenSwift <> "" Then
                CreateBody = CreateBody & "Beneficiary Swift: " & varBenSwift & vbNewLine
            End If
            If varBenIBAN <> "" Then
                CreateBody = CreateBody & "Beneficiary IBAN: " & varBenIBAN & vbNewLine
            End If
            If varBenBankName <> "" Then
                CreateBody = CreateBody & "ID: " & varBenBankName & vbNewLine
            End If
            If varComments <> "" Then
                CreateBody = CreateBody & "Comments: " & varComments & vbNewLine
            End If

            Dim Outl As Interop.Outlook.Application = CreateObject("Outlook.Application")
            Dim omsg As Interop.Outlook.MailItem

            If Outl IsNot Nothing Then
                omsg = Outl.CreateItem(0)
                omsg.Subject = "Order Ref: " & varOrderRef & ",ID: " & varSelectedPaymentID
                omsg.Body = CreateBody
                If varAttachments Then
                    If Directory.Exists(varPaymentFilePathRemote) Then
                        If varPaymentFilePathRemote <> "" Then
                            Dim varFiles() As String = Directory.GetFiles(varPaymentFilePathRemote)
                            If varFiles.Count > 0 Then
                                For i As Integer = 0 To varFiles.Count - 1
                                    omsg.Attachments.Add(varFiles(i))
                                Next
                            End If
                        End If
                    Else
                        MsgBox("The file path for this payment: " & varPaymentFilePathRemote & " could not be found", MsgBoxStyle.Information, "Could not find files")
                    End If
                    omsg.Display(True)
                Else
                    omsg.Display(True)
                End If
            End If
            Outl = Nothing
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovements, Err.Description & " - email issues - body: " & CreateBody)
        End Try
    End Sub

    Public Sub ConfirmDirectory()
        Dim CorrectDirectory As String = ""
        Try
            If HasFiles(ClsPay.PaymentFilePath) Then
                If ClsPay.SelectedPaymentID = 0 Then

                    Dim vID As String = ClsIMS.GetIDFromDB("DolfinPayment")

                    If ClsPay.PortfolioFeeDue Then
                        Dim vTransferID As String = ClsIMS.GetSQLItem("Select P_PortfolioFeeID from vwDolfinPayment where P_ID = " & vID)
                        If IsNumeric(vTransferID) Then
                            If vTransferID <> 0 Then
                                CorrectDirectory = FileFormat(ClsIMS.GetFilePath("Default"), vTransferID)
                                If Not Directory.Exists(CorrectDirectory) Then
                                    Directory.Move(ClsPay.PaymentFilePath, CorrectDirectory)
                                End If
                            End If
                            CorrectDirectory = FileFormat(ClsIMS.GetFilePath("Default"), vID)
                            If Not Directory.Exists(CorrectDirectory) Then
                                Directory.Move(ClsPay.PaymentFilePath, CorrectDirectory)
                                ClsPay.PaymentFilePath = CorrectDirectory
                            End If
                        End If
                    Else
                        CorrectDirectory = FileFormat(ClsIMS.GetFilePath("Default"), vID)
                        If Not Directory.Exists(CorrectDirectory) Then
                            Directory.Move(ClsPay.PaymentFilePath, CorrectDirectory)
                            ClsPay.PaymentFilePath = CorrectDirectory
                        End If
                    End If
                Else
                    If ClsPay.PortfolioFeeDue Then
                        Dim vTransferID As String = ClsIMS.GetSQLItem("Select P_PortfolioFeeID from vwDolfinPayment where P_ID = " & ClsPay.SelectedPaymentID)
                        If IsNumeric(vTransferID) Then
                            CorrectDirectory = FileFormat(ClsIMS.GetFilePath("Default"), vTransferID)
                            ConfirmDirectoryCreated(CorrectDirectory)
                        End If
                    End If
                    CorrectDirectory = FileFormat(ClsIMS.GetFilePath("Default"), ClsPay.SelectedPaymentID)
                    ConfirmDirectoryCreated(CorrectDirectory)
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ConfirmDirectory: " & CorrectDirectory)
        End Try
    End Sub

    Public Sub CopyPaymentFiles(ByVal FromDir As String, ByVal ToDir As String)
        Dim MappDriveLetter As String = "J"
        Dim varDirDate As Date
        Dim TempDirName As String = ""
        Dim TempDir As String = ""
        Dim NewDirName As String = ""

        Try
            MapDrive(MappDriveLetter, "\\IMSAPPPROD01.ims-britannia.local\c$", ClsIMS.MappDriveUser, ClsIMS.MappDrivePwd)

            If Right(FromDir, 1) = "\" Then
                FromDir = Left(FromDir, Len(FromDir) - 1)
            End If
            If Right(ToDir, 1) = "\" Then
                ToDir = Replace(Left(ToDir, Len(ToDir) - 1), "C:", MappDriveLetter & ":")
            End If

            TempDirName = Right(FromDir, Len(FromDir) - Microsoft.VisualBasic.Strings.InStrRev(FromDir, "\"))
            TempDir = Replace(ClsIMS.GetFilePath("TempPaymentFilesLocal"), "C:", MappDriveLetter & ":")
            NewDirName = Right(ToDir, Len(ToDir) - Microsoft.VisualBasic.Strings.InStrRev(ToDir, "\"))

            If IO.Directory.Exists(TempDir & TempDirName) Then
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyPaymentFiles - rename attempt from: " & TempDir & TempDirName & " to: " & NewDirName)
                My.Computer.FileSystem.RenameDirectory(TempDir & TempDirName, NewDirName)
            Else
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyPaymentFiles - directory does not exists so no rename of : " & TempDir & TempDirName & ", to: " & NewDirName)
            End If

            If IO.Directory.Exists(TempDir & NewDirName) Then
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyPaymentFiles - copy attempt from: " & TempDir & NewDirName & ", to: " & ToDir)
                My.Computer.FileSystem.CopyDirectory(TempDir & NewDirName, ToDir)
                ClsPayAuth.TempPaymentFilePath = TempDir & NewDirName
            Else
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyPaymentFiles - directory does not exist so no copy from: " & TempDir & NewDirName & ", to: " & ToDir)
            End If


            'HouseKeeping on temp directory
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovementsFiles, "CopyPaymentFiles - Housekeeping attempt on: " & TempDir & " < " & DateAdd(DateInterval.Day, -3, Now.Date))
            For Each Dir As String In Directory.GetDirectories(TempDir)
                varDirDate = Directory.GetLastWriteTime(Dir)
                If InStr(1, Dir, ".", vbTextCompare) = 0 And varDirDate.Date < DateAdd(DateInterval.Day, -3, Now.Date) Then
                    Dim Dirfiles As String() = Directory.GetFiles(Dir)
                    For Each DirFile As String In Dirfiles
                        File.Delete(DirFile)
                    Next
                    Directory.Delete(Dir, True)
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - CopyPaymentFiles error from: " & FromDir & ", to: " & ToDir)
        End Try
    End Sub

    Private Function FileFormat(ByVal varFilePath As String, ByVal varID As Double) As String
        FileFormat = varFilePath & ClsPay.TransDate.Date.ToString("yyyy-MM-dd") & " - " & Replace(ClsPay.Portfolio, ":", "") & " - " & ClsPay.CCY & " - " & varID & "\"
    End Function

    Private Sub ConfirmDirectoryCreated(ByVal CorrectDirectory As String)
        If Not Directory.Exists(CorrectDirectory) Then
            Directory.Move(ClsPay.PaymentFilePath, CorrectDirectory)
            ClsPay.PaymentFilePath = CorrectDirectory
        End If
        If Strings.InStr(1, ClsPay.PaymentFilePath, ClsPay.SelectedPaymentID, vbTextCompare) = 0 Then
            If Not Directory.Exists(CorrectDirectory) Then
                Directory.Move(ClsPay.PaymentFilePath, CorrectDirectory)
                ClsPay.PaymentFilePath = CorrectDirectory
            End If
        End If
    End Sub

    Public Sub GetTempDirectory(ByVal FilePath As String)
        Dim result As String = ""
        Try
            result = Path.GetRandomFileName()

            If Not Directory.Exists(varPaymentFilePath) Then
                Directory.CreateDirectory(FilePath & result)
                varPaymentFilePath = FilePath & result & "\"
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - GetTempDirectory: " & result)
        End Try
    End Sub

    Public Sub DeleteFileDirectory(ByVal varDirectory As String)
        Try
            If varDirectory <> "" Then
                If InStr(1, varDirectory, ".", vbTextCompare) <> 0 Then
                    If Directory.Exists(varDirectory) Then
                        Directory.Delete(varDirectory, True)
                    End If
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - DeleteTempDirectory: " & varDirectory)
        End Try
    End Sub

    Public Function HasFiles(ByVal varDirectory As String) As Boolean
        HasFiles = False
        Dim files As String() = Directory.GetFiles(varDirectory)
        If files.Count > 0 Then
            HasFiles = True
        End If
    End Function

    Public Sub FormatPaymentGrid(ByRef dgv As DataGridView)
        dgv.Columns("ID").Width = 60
        dgv.Columns("Status").Width = 120
        dgv.Columns("EmailSent").Width = 60
        dgv.Columns("PaymentType").Width = 180
        dgv.Columns("Branch").Width = 50
        dgv.Columns("Portfolio").Width = 200
        dgv.Columns("Amount").Width = 80
        dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv.Columns("TransDate").Width = 80
        dgv.Columns("SettleDate").Width = 80
        dgv.Columns("CCY").Width = 40
        dgv.Columns("BenPortfolio").Width = 180
        dgv.Columns("OrderRef").Width = 120
        dgv.Columns("BenRef").Width = 120
        dgv.Columns("SendSwift").Width = 60
        dgv.Columns("SendIMS").Width = 60
        dgv.Columns("SwiftReturned").Width = 60
        dgv.Columns("SwiftAck").Width = 60
        dgv.Columns("IMSReturned").Width = 60
        dgv.Columns("IMSAck").Width = 60
        dgv.Columns("PaidAck").Width = 60
        dgv.Columns("CreatedTime").Width = 100
        dgv.Columns("ModifiedTime").Width = 100
        dgv.Columns("SwiftSentTime").Width = 100
        dgv.Columns("IMSSentTime").Width = 100
        dgv.Columns("PortfolioCode").Visible = False
        dgv.Columns("CCYCode").Visible = False
        dgv.Columns("SwiftReceivedTime").Visible = False
        dgv.Columns("IMSReceivedTime").Visible = False
        dgv.Columns("AuthorisedTime").Visible = False
        dgv.Columns("Orderval").Visible = False

        For i As Integer = 0 To dgv.RowCount - 1
            dgv(dgv.Columns("IMSFile").Index, i) = New DataGridViewLinkCell
            dgv(dgv.Columns("PaidReceivedInPayFile").Index, i) = New DataGridViewLinkCell
            dgv(dgv.Columns("PaidReceivedOutPayFile").Index, i) = New DataGridViewLinkCell
        Next
    End Sub

    Public Sub FormatViewGrid(ByRef dgv As DataGridView)
        Try
            dgv.Columns("P_ID").Width = 60
            dgv.Columns("P_Status").Width = 120
            dgv.Columns("P_PaymentType").Width = 180
            dgv.Columns("P_Portfolio").Width = 200
            dgv.Columns("P_Amount").Width = 80
            dgv.Columns("P_Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("P_TransDate").Width = 80
            dgv.Columns("P_SettleDate").Width = 80
            dgv.Columns("P_CCY").Width = 40
            dgv.Columns("P_BenPortfolio").Width = 180
            dgv.Columns("P_OrderRef").Width = 120
            dgv.Columns("P_BenRef").Width = 120
            dgv.Columns("P_SendSwift").Width = 60
            dgv.Columns("P_SendIMS").Width = 60
            dgv.Columns("P_SwiftUrgent").Width = 60
            dgv.Columns("P_SwiftSendRef").Width = 60
            dgv.Columns("P_SwiftReturned").Width = 60
            dgv.Columns("P_SwiftAcknowledged").Width = 60
            dgv.Columns("P_SwiftStatus").Width = 200
            dgv.Columns("P_IMSReturned").Width = 60
            dgv.Columns("P_IMSAcknowledged").Width = 60
            dgv.Columns("P_IMSStatus").Width = 200

            For i As Integer = 0 To dgv.RowCount - 1
                dgv(dgv.Columns("P_SwiftFile").Index, i) = New DataGridViewLinkCell
                dgv(dgv.Columns("P_IMSFile").Index, i) = New DataGridViewLinkCell
                dgv(dgv.Columns("P_PaidReceivedInPayFile").Index, i) = New DataGridViewLinkCell
                dgv(dgv.Columns("P_PaidReceivedOutPayFile").Index, i) = New DataGridViewLinkCell
            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Sub FormatHistoryGrid(ByRef dgv As DataGridView)
        If Not dgv.DataSource Is Nothing Then
            dgv.Columns("Portfolio").Width = 100
            dgv.Columns("Broker").Width = 150
            dgv.Columns("Account").Width = 200
            dgv.Columns("TradeDate").Width = 80
            dgv.Columns("settleDate").Width = 80
            dgv.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgv.Columns("Amount").DefaultCellStyle.Format = "N2"
            dgv.Columns("Amount").ValueType = GetType(SqlTypes.SqlMoney)
            dgv.Columns("CCY").Width = 60
            dgv.Columns("Comments").Width = 300
            dgv.Columns("Reference").Width = 120
        End If
    End Sub

    Public Sub DirectoryHousekeeping()
        Dim varDirDate As Date

        Try
            For Each Dir As String In Directory.GetDirectories(ClsIMS.GetFilePath("TempPaymentFiles"))
                varDirDate = Directory.GetLastWriteTime(Dir)
                If InStr(1, Dir, ".", vbTextCompare) = 0 And varDirDate.Date < Now.Date Then
                    Dim Dirfiles As String() = Directory.GetFiles(Dir)
                    For Each DirFile As String In Dirfiles
                        File.Delete(DirFile)
                    Next
                    Directory.Delete(Dir, True)
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameCashMovementsFiles, Err.Description & " - Error on DirectoryHousekeeping")
        End Try
    End Sub

    Public Function ExportGridToExcel(ByVal dgv As DataGridView, Optional ByVal varMAXcolumns As Integer = 0, Optional varFullFilePath As String = "") As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim i As Integer, j As Integer
        Dim varFilePath As String = "", varFileName As String = ""

        Try
            If varFullFilePath <> "" Then
                varFilePath = Path.GetDirectoryName(varFullFilePath) & "\"
                varFileName = Path.GetFileName(varFullFilePath)
            Else
                varFilePath = "c:\Temp\"
                varFileName = "Movements.xlsx"
            End If

            xlApp = New Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            If varMAXcolumns = 0 Then
                varMAXcolumns = dgv.ColumnCount
            End If

            For k As Integer = 1 To varMAXcolumns
                xlWorkSheet.Cells(1, k) = dgv.Columns(k - 1).HeaderText
            Next

            For i = 0 To dgv.RowCount - 1
                For j = 0 To varMAXcolumns - 1
                    If Not dgv(j, i).Value Is Nothing Then
                        xlWorkSheet.Cells(i + 2, j + 1) = dgv(j, i).Value.ToString()
                    End If
                Next
            Next

            xlWorkSheet.Cells.WrapText = False

            If Not Directory.Exists(varFilePath) Then
                Directory.CreateDirectory(varFilePath)
            End If

            If File.Exists(varFilePath & varFileName) Then
                File.Delete(varFilePath & varFileName)
            End If

            xlWorkSheet.SaveAs(varFilePath & varFileName)
            xlWorkBook.Close()
            xlApp.Quit()

            ShellExecute(varFilePath & varFileName)
            MsgBox("Your file has been saved to " & varFilePath & varFileName & " and is open in Excel", vbInformation, "File Saved")
            ExportGridToExcel = True
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExportGridToExcel filepath = " & varFilePath & varFileName)
            MsgBox("Unable to save file to " & varFilePath & varFileName, vbCritical, "File NOT Saved")
            ExportGridToExcel = False
        Finally
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)
        End Try
    End Function

    Public Function ExportGridToCSV(ByVal dgv As DataGridView) As Boolean
        Dim i As Integer, j As Integer
        Dim varFilePath As String = "", varFileName As String = ""
        Dim objWriter As IO.StreamWriter = Nothing

        Try
            varFilePath = "c:\Temp\"
            varFileName = "Movements.csv"
            objWriter = IO.File.CreateText(varFilePath & varFileName)

            For k As Integer = 1 To dgv.Columns.Count
                objWriter.Write(dgv.Columns(k - 1).HeaderText & ",")
            Next

            For i = 0 To dgv.RowCount - 1
                For j = 0 To dgv.ColumnCount - 1
                    objWriter.Write(dgv(j, i).Value.ToString() & ",")
                Next
                objWriter.WriteLine()
            Next

            If Not Directory.Exists(varFilePath) Then
                Directory.CreateDirectory(varFilePath)
            End If

            If File.Exists(varFilePath & varFileName) Then
                File.Delete(varFilePath & varFileName)
            End If

            MsgBox("Your CSV file has been saved to " & varFilePath & varFileName, vbInformation, "File Saved")
            ExportGridToCSV = True
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExportGridToExcel filepath = " & varFilePath & varFileName)
            MsgBox("Unable to save CSV file to " & varFilePath & varFileName, vbCritical, "File NOT Saved")
            ExportGridToCSV = False
        Finally
            objWriter.Close()
            objWriter = Nothing
        End Try
    End Function

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub



    Public Sub ClearClass()
        On Error Resume Next
        varStatus = ""
        varAmount = 0
        varSelectedType = ""
        varSelectedName = ""
        varPortfolio = ""
        varPortfolioCode = 0
        varPortfolioIsCommission = False
        varPortfolioIsFee = False
        varPortfolioFee = 0
        varPortfolioFeeCCY = ""
        varPortfolioFeeInt = 0
        varPortfolioFeeCCYInt = ""
        PortfolioFeeDue = False
        varPortfolioFeeID = 0
        varPortfolioFeeAccountCode = 0
        varPortfolioFeeRMSAccountCode = 0
        varCCY = ""
        varCCYCode = 0
        varSelectedBeneficiaryBankSwiftCode = ""
        varSelectedPaymentID = 0
        varSelectedAccount = ""
        varSelectedOrder = 0
        varSelectedPaymentOption = 0
        varSelectedAuthoriseOption = 0
        varSelectedTransferFeeOption = 0
        varSelectedEmailAttachments = False
        varPaymentType = ""
        varMessageType = ""
        varOrderFormData = Nothing
        varOrderBrokerCode = 0
        varOrderAccount = ""
        varOrderAccountCode = 0
        varOrderBalance = 0
        varOrderName = ""
        varOrderRef = ""
        varOrderAddress1 = ""
        varOrderAddress2 = ""
        varOrderZip = ""
        varOrderSwift = ""
        varOrderAccountNumber = ""
        varOrderIBAN = ""
        varOrderOther = ""
        varOrderVOCodeID = 0
        varOtherID = 0
        varOrderMessageTypeID = 0
        varBenFormData = Nothing
        varBenBrokerCode = 0
        varBenAccount = ""
        varBenAccountCode = 0
        varBenAccountFiltered = False
        varBenBalance = 0
        varBenName = ""
        varBenRef = ""
        varBenAddress1 = ""
        varBenAddress2 = ""
        varBenZip = ""
        varBenSwift = ""
        varBenAccountNumber = ""
        varBenIBAN = ""
        varBenOther = ""
        varBenBIK = ""
        varBenINN = ""
        varBenPortfolioCode = 0
        varBenPortfolio = ""
        varBenCCY = ""
        varBenCCYCode = 0
        varBenAddressID = 0
        varExchangeRate = 1
        varBenSubBankFormData = Nothing
        varBenSubBankAddressID = 0
        varBenSubBankName = ""
        varBenSubBankAddress1 = ""
        varBenSubBankAddress2 = ""
        varBenSubBankZip = ""
        varBenSubBankSwift = ""
        varBenSubBankIBAN = ""
        varBenSubBankOther = ""
        varBenSubBankBIK = ""
        varBenSubBankINN = ""
        varBenBankFormData = Nothing
        varBenBankAddressID = 0
        varBenBankName = ""
        varBenBankAddress1 = ""
        varBenBankAddress2 = ""
        varBenBankZip = ""
        varBenBankSwift = ""
        varBenBankIBAN = ""
        varBenBankOther = ""
        varBenBankBIK = ""
        varBenBankINN = ""
        varTransDate = Now
        varSettleDate = Now
        varComments = ""
        varSendSwift = True
        varSendIMS = True
        varSendSwift = True
        varSendIMS = True
        varCutOfftime = ""
        varCutOfftimeZone = ""
        varCutOfftimeGMT = ""
        varPaymentFilePath = ""
        varPaymentFilePathRemote = ""
        varSwiftUrgent = False
        varSwiftSendRef = False
        varSwiftReturned = False
        varSwiftAcknowledged = False
        varSwiftStatus = ""
        varIMSReturned = False
        varIMSAcknowledged = False
        varIMSStatus = ""
        varBenTemplateName = ""
        varAbovethreshold = False
        varIMSRef = ""
    End Sub

    Public Sub New()
        PopulatePayOptions()
    End Sub
End Class
