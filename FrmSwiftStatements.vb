﻿Public Class FrmSwiftStatements
    Dim AccConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet

    Private Sub LoadAccountsGrid()
        Dim varSQL As String
        Dim Boldstyle As New DataGridViewCellStyle()

        Try
            Cursor = Cursors.WaitCursor

            varSQL = "exec DolfinPaymentSwiftAccountStatement '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "','" & dtToDate.Value.ToString("yyyy-MM-dd") & "'," & cboType.SelectedValue & ", '" & cboCCY.Text & "', '" & cboAccounts.SelectedValue & "'"
            dgvAccounts.DataSource = Nothing
            AccConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, AccConn)
            ds = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(ds, "vwDolfinPaymentSwiftAccountStatement")
            dgvAccounts.AutoGenerateColumns = True
            dgvAccounts.DataSource = ds.Tables("vwDolfinPaymentSwiftAccountStatement")

            dgvAccounts.Columns("RowID").Visible = False
            dgvAccounts.Columns("ID").Width = 60
            dgvAccounts.Columns("Credit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAccounts.Columns("Credit").DefaultCellStyle.Format = "N2"
            dgvAccounts.Columns("Credit").ValueType = GetType(SqlTypes.SqlMoney)
            dgvAccounts.Columns("Debit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAccounts.Columns("Debit").DefaultCellStyle.Format = "N2"
            dgvAccounts.Columns("Debit").ValueType = GetType(SqlTypes.SqlMoney)
            dgvAccounts.Columns("RunningBalance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvAccounts.Columns("RunningBalance").DefaultCellStyle.Format = "N2"
            dgvAccounts.Columns("RunningBalance").ValueType = GetType(SqlTypes.SqlMoney)
            dgvAccounts.Columns("BankInfo1").Width = 200
            dgvAccounts.Columns("BankInfo2").Width = 450
            dgvAccounts.Columns("Portfolio").Width = 250

            dgvAccounts.Rows(0).DefaultCellStyle.BackColor = Color.LightGray
            dgvAccounts.Rows(dgvAccounts.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray
            dgvAccounts.Rows(dgvAccounts.Rows.Count - 1).DefaultCellStyle.Font = New Font(dgvAccounts.Font, FontStyle.Bold)

            dgvAccounts.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvAccounts.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvAccounts.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing account statements grid")
        End Try
    End Sub

    Private Sub FrmSwiftStatements_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvAccounts, True)
        dtFromDate.Value = DateAdd(DateInterval.Day, -7, Now)
        dtToDate.Value = Now
        ClsIMS.PopulateComboboxWithData(cboCCY, "select cr_code,cr_name1 from vwdolfinpaymentcurrencies group by cr_code,cr_name1 order by cr_name1")
        cboAccounts.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboAccounts.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub cboCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCCY.SelectedIndexChanged
        If IsNumeric(cboCCY.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboAccounts, "Select * from (Select dbo.[DolfinPaymentGetAccountItem](0, sra_accountno, sra_ccy, sra_swiftcode) As AccountNo, sc_bankname + ' (' + (dbo.[DolfinPaymentGetAccountItem](0,sra_accountno,sra_ccy,sra_swiftcode)) + ')' as BankName from dolfinpaymentswiftreaddetail inner join dolfinpaymentswiftcodes on sra_swiftcode = sc_swiftcode where sra_ccy = '" & cboCCY.Text & "' group by sra_accountno,sra_ccy,sc_bankname,sra_swiftcode) acc where BankName is not null order by BankName")
            ClsIMS.PopulateComboboxWithData(cboType, "select '942', 'Intra-day statement (MT942)' union select '940','End of day statement (MT940)' union select '950','End of day statement (MT950)'")
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvAccounts, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        LoadAccountsGrid()
    End Sub

    Private Sub cboType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboType.SelectedIndexChanged
        If IsNumeric(cboType.SelectedValue) Then
            If cboType.SelectedValue = 942 Then
                dtFromDate.Value = Now
                dtToDate.Value = Now
            End If
        End If
    End Sub
End Class