﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCorpActionAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CARb2 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtCAAuthMV = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCAAuthEvent = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCAAuthName = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtCAAuthISIN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.txtCAAuthID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CARb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CARb2
        '
        Me.CARb2.AutoSize = True
        Me.CARb2.Checked = True
        Me.CARb2.Location = New System.Drawing.Point(97, 234)
        Me.CARb2.Name = "CARb2"
        Me.CARb2.Size = New System.Drawing.Size(79, 17)
        Me.CARb2.TabIndex = 71
        Me.CARb2.TabStop = True
        Me.CARb2.Text = "View Event"
        Me.CARb2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtCAAuthMV)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCAAuthEvent)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtCAAuthName)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtCAAuthISIN)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Location = New System.Drawing.Point(27, 57)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(371, 143)
        Me.GroupBox1.TabIndex = 70
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'txtCAAuthMV
        '
        Me.txtCAAuthMV.Enabled = False
        Me.txtCAAuthMV.Location = New System.Drawing.Point(70, 106)
        Me.txtCAAuthMV.Name = "txtCAAuthMV"
        Me.txtCAAuthMV.Size = New System.Drawing.Size(125, 20)
        Me.txtCAAuthMV.TabIndex = 90
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 89
        Me.Label3.Text = "M/V:"
        '
        'txtCAAuthEvent
        '
        Me.txtCAAuthEvent.Enabled = False
        Me.txtCAAuthEvent.Location = New System.Drawing.Point(70, 80)
        Me.txtCAAuthEvent.Name = "txtCAAuthEvent"
        Me.txtCAAuthEvent.Size = New System.Drawing.Size(125, 20)
        Me.txtCAAuthEvent.TabIndex = 88
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "CA Event:"
        '
        'txtCAAuthName
        '
        Me.txtCAAuthName.Enabled = False
        Me.txtCAAuthName.Location = New System.Drawing.Point(70, 28)
        Me.txtCAAuthName.Name = "txtCAAuthName"
        Me.txtCAAuthName.Size = New System.Drawing.Size(291, 20)
        Me.txtCAAuthName.TabIndex = 86
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(26, 31)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(38, 13)
        Me.lblauthportfolio.TabIndex = 85
        Me.lblauthportfolio.Text = "Name:"
        '
        'txtCAAuthISIN
        '
        Me.txtCAAuthISIN.Enabled = False
        Me.txtCAAuthISIN.Location = New System.Drawing.Point(70, 54)
        Me.txtCAAuthISIN.Name = "txtCAAuthISIN"
        Me.txtCAAuthISIN.Size = New System.Drawing.Size(148, 20)
        Me.txtCAAuthISIN.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "ISIN:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(179, 54)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'txtCAAuthID
        '
        Me.txtCAAuthID.AcceptsReturn = True
        Me.txtCAAuthID.AcceptsTab = True
        Me.txtCAAuthID.BackColor = System.Drawing.Color.FloralWhite
        Me.txtCAAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCAAuthID.Enabled = False
        Me.txtCAAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCAAuthID.Location = New System.Drawing.Point(12, 12)
        Me.txtCAAuthID.Multiline = True
        Me.txtCAAuthID.Name = "txtCAAuthID"
        Me.txtCAAuthID.Size = New System.Drawing.Size(79, 29)
        Me.txtCAAuthID.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(93, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(245, 24)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "Corporate Action Options"
        '
        'CARb1
        '
        Me.CARb1.AutoSize = True
        Me.CARb1.Location = New System.Drawing.Point(97, 211)
        Me.CARb1.Name = "CARb1"
        Me.CARb1.Size = New System.Drawing.Size(89, 17)
        Me.CARb1.TabIndex = 68
        Me.CARb1.Text = "Achive Event"
        Me.CARb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(25, 272)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(373, 23)
        Me.cmdContinue.TabIndex = 67
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'FrmCorpActionAuthorise
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(426, 317)
        Me.Controls.Add(Me.CARb2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CARb1)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.txtCAAuthID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCorpActionAuthorise"
        Me.Text = "FrmCorpActionAuthorise"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CARb2 As RadioButton
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtCAAuthMV As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCAAuthEvent As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCAAuthName As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtCAAuthISIN As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents txtCAAuthID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CARb1 As RadioButton
    Friend WithEvents cmdContinue As Button
End Class
