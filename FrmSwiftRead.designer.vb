﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSwiftRead
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSwiftRead))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.CboAmtCCY = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CboAmt = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.CboISIN = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.CboFileName = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboLinkedRef = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboRelatedref = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboSwiftRef = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboAccountNo = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CboToDate = New System.Windows.Forms.ComboBox()
        Me.cboBroker = New System.Windows.Forms.ComboBox()
        Me.lblBroker = New System.Windows.Forms.Label()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.CboFromDate = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblSwiftCode = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboUserRef = New System.Windows.Forms.ComboBox()
        Me.cboSwiftCode = New System.Windows.Forms.ComboBox()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.cboType = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvSwiftRead = New System.Windows.Forms.DataGridView()
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TimerCheckSwift = New System.Windows.Forms.Timer(Me.components)
        Me.lbllastread = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cboDetailSelect = New System.Windows.Forms.ComboBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvSwiftRead, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.CboAmtCCY)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.CboAmt)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.CboISIN)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.CboFileName)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.cboLinkedRef)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cboRelatedref)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cboSwiftRef)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboAccountNo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.CboToDate)
        Me.GroupBox1.Controls.Add(Me.cboBroker)
        Me.GroupBox1.Controls.Add(Me.lblBroker)
        Me.GroupBox1.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.CboFromDate)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblSwiftCode)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.CboUserRef)
        Me.GroupBox1.Controls.Add(Me.cboSwiftCode)
        Me.GroupBox1.Controls.Add(Me.CmdFilter)
        Me.GroupBox1.Controls.Add(Me.cboType)
        Me.GroupBox1.Controls.Add(Me.CmdRefreshView)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 47)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(950, 158)
        Me.GroupBox1.TabIndex = 41
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "SWIFT Criteria"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(499, 101)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 13)
        Me.Label13.TabIndex = 76
        Me.Label13.Text = "Amount CCY:"
        '
        'CboAmtCCY
        '
        Me.CboAmtCCY.FormattingEnabled = True
        Me.CboAmtCCY.Location = New System.Drawing.Point(574, 98)
        Me.CboAmtCCY.Name = "CboAmtCCY"
        Me.CboAmtCCY.Size = New System.Drawing.Size(210, 21)
        Me.CboAmtCCY.TabIndex = 75
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(522, 73)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(46, 13)
        Me.Label12.TabIndex = 74
        Me.Label12.Text = "Amount:"
        '
        'CboAmt
        '
        Me.CboAmt.FormattingEnabled = True
        Me.CboAmt.Location = New System.Drawing.Point(574, 70)
        Me.CboAmt.Name = "CboAmt"
        Me.CboAmt.Size = New System.Drawing.Size(210, 21)
        Me.CboAmt.TabIndex = 73
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(537, 46)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 72
        Me.Label11.Text = "ISIN:"
        '
        'CboISIN
        '
        Me.CboISIN.FormattingEnabled = True
        Me.CboISIN.Location = New System.Drawing.Point(574, 43)
        Me.CboISIN.Name = "CboISIN"
        Me.CboISIN.Size = New System.Drawing.Size(210, 21)
        Me.CboISIN.TabIndex = 71
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(514, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(54, 13)
        Me.Label10.TabIndex = 70
        Me.Label10.Text = "FileName:"
        '
        'CboFileName
        '
        Me.CboFileName.FormattingEnabled = True
        Me.CboFileName.Location = New System.Drawing.Point(574, 16)
        Me.CboFileName.Name = "CboFileName"
        Me.CboFileName.Size = New System.Drawing.Size(210, 21)
        Me.CboFileName.TabIndex = 69
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(256, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 13)
        Me.Label9.TabIndex = 68
        Me.Label9.Text = "Linked Ref:"
        '
        'cboLinkedRef
        '
        Me.cboLinkedRef.FormattingEnabled = True
        Me.cboLinkedRef.Location = New System.Drawing.Point(324, 127)
        Me.cboLinkedRef.Name = "cboLinkedRef"
        Me.cboLinkedRef.Size = New System.Drawing.Size(169, 21)
        Me.cboLinkedRef.TabIndex = 67
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(3, 127)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(67, 13)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Related Ref:"
        '
        'cboRelatedref
        '
        Me.cboRelatedref.FormattingEnabled = True
        Me.cboRelatedref.Location = New System.Drawing.Point(77, 124)
        Me.cboRelatedref.Name = "cboRelatedref"
        Me.cboRelatedref.Size = New System.Drawing.Size(169, 21)
        Me.cboRelatedref.TabIndex = 65
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(265, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 64
        Me.Label4.Text = "Swift Ref:"
        '
        'cboSwiftRef
        '
        Me.cboSwiftRef.FormattingEnabled = True
        Me.cboSwiftRef.Location = New System.Drawing.Point(324, 100)
        Me.cboSwiftRef.Name = "cboSwiftRef"
        Me.cboSwiftRef.Size = New System.Drawing.Size(169, 21)
        Me.cboSwiftRef.TabIndex = 63
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(255, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 62
        Me.Label1.Text = "Account No:"
        '
        'cboAccountNo
        '
        Me.cboAccountNo.FormattingEnabled = True
        Me.cboAccountNo.Location = New System.Drawing.Point(324, 73)
        Me.cboAccountNo.Name = "cboAccountNo"
        Me.cboAccountNo.Size = New System.Drawing.Size(169, 21)
        Me.cboAccountNo.TabIndex = 61
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(269, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 60
        Me.Label8.Text = "To Date:"
        '
        'CboToDate
        '
        Me.CboToDate.FormattingEnabled = True
        Me.CboToDate.Location = New System.Drawing.Point(324, 16)
        Me.CboToDate.Name = "CboToDate"
        Me.CboToDate.Size = New System.Drawing.Size(169, 21)
        Me.CboToDate.TabIndex = 59
        '
        'cboBroker
        '
        Me.cboBroker.FormattingEnabled = True
        Me.cboBroker.Location = New System.Drawing.Point(324, 43)
        Me.cboBroker.Name = "cboBroker"
        Me.cboBroker.Size = New System.Drawing.Size(169, 21)
        Me.cboBroker.TabIndex = 57
        '
        'lblBroker
        '
        Me.lblBroker.AutoSize = True
        Me.lblBroker.Location = New System.Drawing.Point(277, 46)
        Me.lblBroker.Name = "lblBroker"
        Me.lblBroker.Size = New System.Drawing.Size(41, 13)
        Me.lblBroker.TabIndex = 58
        Me.lblBroker.Text = "Broker:"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(832, 89)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 56
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 47
        Me.Label6.Text = "From Date:"
        '
        'CboFromDate
        '
        Me.CboFromDate.FormattingEnabled = True
        Me.CboFromDate.Location = New System.Drawing.Point(77, 16)
        Me.CboFromDate.Name = "CboFromDate"
        Me.CboFromDate.Size = New System.Drawing.Size(169, 21)
        Me.CboFromDate.TabIndex = 46
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(52, 13)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "User Ref:"
        '
        'lblSwiftCode
        '
        Me.lblSwiftCode.AutoSize = True
        Me.lblSwiftCode.Location = New System.Drawing.Point(8, 73)
        Me.lblSwiftCode.Name = "lblSwiftCode"
        Me.lblSwiftCode.Size = New System.Drawing.Size(61, 13)
        Me.lblSwiftCode.TabIndex = 44
        Me.lblSwiftCode.Text = "Swift Code:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 43
        Me.Label3.Text = "Swift Type:"
        '
        'CboUserRef
        '
        Me.CboUserRef.FormattingEnabled = True
        Me.CboUserRef.Location = New System.Drawing.Point(77, 97)
        Me.CboUserRef.Name = "CboUserRef"
        Me.CboUserRef.Size = New System.Drawing.Size(169, 21)
        Me.CboUserRef.TabIndex = 42
        '
        'cboSwiftCode
        '
        Me.cboSwiftCode.FormattingEnabled = True
        Me.cboSwiftCode.Location = New System.Drawing.Point(77, 70)
        Me.cboSwiftCode.Name = "cboSwiftCode"
        Me.cboSwiftCode.Size = New System.Drawing.Size(169, 21)
        Me.cboSwiftCode.TabIndex = 41
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(832, 19)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 38)
        Me.CmdFilter.TabIndex = 40
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'cboType
        '
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(77, 43)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(169, 21)
        Me.cboType.TabIndex = 36
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(832, 60)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 39
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(19, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 24)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "SWIFT Read"
        '
        'dgvSwiftRead
        '
        Me.dgvSwiftRead.AllowUserToAddRows = False
        Me.dgvSwiftRead.AllowUserToDeleteRows = False
        Me.dgvSwiftRead.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSwiftRead.Location = New System.Drawing.Point(12, 211)
        Me.dgvSwiftRead.Name = "dgvSwiftRead"
        Me.dgvSwiftRead.ReadOnly = True
        Me.dgvSwiftRead.Size = New System.Drawing.Size(1644, 654)
        Me.dgvSwiftRead.TabIndex = 43
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Enabled = True
        Me.TimerRefresh.Interval = 120000
        '
        'TimerCheckSwift
        '
        Me.TimerCheckSwift.Enabled = True
        Me.TimerCheckSwift.Interval = 3600000
        '
        'lbllastread
        '
        Me.lbllastread.AutoSize = True
        Me.lbllastread.ForeColor = System.Drawing.Color.Maroon
        Me.lbllastread.Location = New System.Drawing.Point(708, 31)
        Me.lbllastread.Name = "lbllastread"
        Me.lbllastread.Size = New System.Drawing.Size(154, 13)
        Me.lbllastread.TabIndex = 44
        Me.lbllastread.Text = "Last time Swifts read from files: "
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(1327, 186)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 13)
        Me.Label14.TabIndex = 72
        Me.Label14.Text = "Swift Read Type:"
        '
        'cboDetailSelect
        '
        Me.cboDetailSelect.FormattingEnabled = True
        Me.cboDetailSelect.Location = New System.Drawing.Point(1422, 184)
        Me.cboDetailSelect.Name = "cboDetailSelect"
        Me.cboDetailSelect.Size = New System.Drawing.Size(210, 21)
        Me.cboDetailSelect.TabIndex = 71
        '
        'FrmSwiftRead
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1659, 874)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.cboDetailSelect)
        Me.Controls.Add(Me.lbllastread)
        Me.Controls.Add(Me.dgvSwiftRead)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmSwiftRead"
        Me.Text = "Swift Reader"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvSwiftRead, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cboLinkedRef As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboRelatedref As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cboSwiftRef As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboAccountNo As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents CboToDate As ComboBox
    Friend WithEvents cboBroker As ComboBox
    Friend WithEvents lblBroker As Label
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents CboFromDate As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents lblSwiftCode As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents CboUserRef As ComboBox
    Friend WithEvents cboSwiftCode As ComboBox
    Friend WithEvents CmdFilter As Button
    Friend WithEvents cboType As ComboBox
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents dgvSwiftRead As DataGridView
    Friend WithEvents TimerRefresh As Timer
    Friend WithEvents TimerCheckSwift As Timer
    Friend WithEvents Label11 As Label
    Friend WithEvents CboISIN As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents CboFileName As ComboBox
    Friend WithEvents Label13 As Label
    Friend WithEvents CboAmtCCY As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents CboAmt As ComboBox
    Friend WithEvents lbllastread As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents cboDetailSelect As ComboBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
