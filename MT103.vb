﻿Public Class MT103

    Public Property ClientSwiftCode As String
    Public Property SettleDate As String
    Public Property Amount As Double?
    Public Property Ccy As String
    Public Property OrderRef As String
    Public Property OrderName As String
    Public Property OrderAddress As String
    Public Property OrderCity As String
    Public Property OrderPostCode As String
    Public Property OrderSwift As String
    'Public Property OrderAccountNumber As String
    Public Property OrderIbanAccountNumber As String
    Public Property OrderOther As String
    Public Property OrderVOCode As String
    Public Property BeneficiaryName As String
    Public Property BeneficiaryAddress1 As String
    Public Property BeneficiaryAddress2 As String
    Public Property BeneficiaryPostCode As String
    Public Property BeneficiarySwift As String
    Public Property BeneficiaryAccountNumber As String
    Public Property BeneficiaryIBAN As String
    Public Property BeneficiaryCcy As String
    Public Property BeneficiarySubBankName As String
    Public Property BeneficiarySubBankAddress1 As String
    Public Property BeneficiarySubBankAddress2 As String
    Public Property BeneficiarySubBankPostCode As String
    Public Property BeneficiarySubBankSwift As String
    Public Property BeneficiarySubBankIBAN As String
    Public Property BeneficiarySubBankOther As String
    Public Property BeneficiarySubBankBik As String
    Public Property BeneficiarySubBankINN As String
    Public Property BeneficiaryBankName As String
    Public Property BeneficiaryBankAddress1 As String
    Public Property BeneficiaryBankAddress2 As String
    Public Property BeneficiaryBankPostCode As String
    Public Property BeneficiaryBankSwift As String
    Public Property BeneficiaryBankIBAN As String
    Public Property SwiftUrgent As Boolean
    Public Property SwiftSendRef As Boolean
    Public Property ExchangeRate As Double?
    Public Property Email As String
    Public Property Ftp As Boolean?

End Class