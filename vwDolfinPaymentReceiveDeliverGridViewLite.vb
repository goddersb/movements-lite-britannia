'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class vwDolfinPaymentReceiveDeliverGridViewLite
    Public Property ID As Integer
    Public Property Status As String
    Public Property SwiftAck As Nullable(Of Boolean)
    Public Property ConfAck As Nullable(Of Boolean)
    Public Property StatusAck As Nullable(Of Boolean)
    Public Property ReceiveDeliver As String
    Public Property OrderRef As String
    Public Property Portfolio As String
    Public Property InstName As String
    Public Property Amount As Nullable(Of Decimal)
    Public Property CCYName As String
    Public Property TransDate As Nullable(Of Date)
    Public Property SettleDate As Nullable(Of Date)
    Public Property BrokerName As String
    Public Property ClearerName As String
    Public Property Agent As String
    Public Property AgentAccountNo As String
    Public Property PlaceofSettlement As String
    Public Property Instructions As String
    Public Property IMSCode As Nullable(Of Integer)
    Public Property SwiftRet As Nullable(Of Boolean)
    Public Property SwiftStatus As String
    Public Property Username As String
    Public Property Created As Nullable(Of Date)
    Public Property Modified As Nullable(Of Date)
    Public Property SentTime As Nullable(Of Date)
    Public Property ReceivedTime As Nullable(Of Date)
    Public Property ConfReceivedTime As Nullable(Of Date)
    Public Property StatusReceivedTime As Nullable(Of Date)
    Public Property SwiftFile As String
    Public Property ConfirmSwiftFile As String
    Public Property StatusSwiftFile As String

End Class
