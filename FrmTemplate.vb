﻿Public Class FrmTemplate
    Dim varBenAddressID As Integer = 0
    Dim varBenSubBankAddressID As Integer = 0
    Dim varBenBankAddressID As Integer = 0

    Private Sub ClearBen()
        'varBenAddressID = 0
        cboTemBenType.Text = ""
        txttembenfirstname.Text = ""
        txtTembenmiddlename.Text = ""
        txttembensurname.Text = ""
        txtTemBenAddress1.Text = ""
        txtTemBenAddress2.Text = ""
        txttembencounty.Text = ""
        txtTemBenZip.Text = ""
        cboTemBenCountry.Text = ""
        txtTemBenSwift.Text = ""
        txtTemBenIBAN.Text = ""
        txtTemBenOther1.Text = ""
        txtTemBenBIK.Text = ""
        txtTemBenINN.Text = ""
        cbotembenrelationship.Text = ""
        PicTemBenSwift.Image = Nothing
        PicTemBenIBAN.Image = Nothing
    End Sub

    Private Sub ClearSubBank()
        'varBenSubBankAddressID = 0
        txtTemBenSubBankAddress1.Text = ""
        txtTemBenSubBankAddress2.Text = ""
        txtTemBenSubBankZip.Text = ""
        cboTemBenSubBankCountry.Text = ""
        txtTemBenSubBankSwift.Text = ""
        txtTemBenSubBankIBAN.Text = ""
        txtTemBenSubBankOther1.Text = ""
        txtTemBenSubBankBIK.Text = ""
        txtTemBenSubBankINN.Text = ""
        ChkTemBenSubBank.Checked = False
        CboTemBenSubBankName.Enabled = False
        txtTemBenSubBankAddress1.Enabled = False
        txtTemBenSubBankAddress2.Enabled = False
        txtTemBenSubBankZip.Enabled = False
        cboTemBenSubBankCountry.Enabled = False
        txtTemBenSubBankSwift.Enabled = False
        txtTemBenSubBankIBAN.Enabled = False
        txtTemBenSubBankOther1.Enabled = False
        txtTemBenSubBankBIK.Enabled = False
        txtTemBenSubBankINN.Enabled = False
        PicTemBenSubBankSwift.Image = Nothing
        PicTemBenSubBankIBAN.Image = Nothing
    End Sub

    Private Sub ClearBank()
        'varBenBankAddressID = 0
        txtTemBenBankAddress1.Text = ""
        txtTemBenBankAddress2.Text = ""
        txtTemBenBankZip.Text = ""
        cboTemBenBankCountry.Text = ""
        txtTemBenBankSwift.Text = ""
        txtTemBenBankIBAN.Text = ""
        txtTemBenBankOther1.Text = ""
        txtTemBenBankBIK.Text = ""
        txtTemBenBankINN.Text = ""
        PicTemBenBankSwift.Image = Nothing
        PicTemBenBankIBAN.Image = Nothing
    End Sub

    Private Sub EnableSubBank()
        CboTemBenSubBankName.Enabled = True
        txtTemBenSubBankAddress1.Enabled = True
        txtTemBenSubBankAddress2.Enabled = True
        txtTemBenSubBankZip.Enabled = True
        cboTemBenSubBankCountry.Enabled = True
        txtTemBenSubBankSwift.Enabled = True
        txtTemBenSubBankIBAN.Enabled = True
        txtTemBenSubBankOther1.Enabled = True
        txtTemBenSubBankBIK.Enabled = True
        txtTemBenSubBankINN.Enabled = True
    End Sub

    Private Sub ClearTemplateForm()
        txtTemTemplateName.Text = ""
        cboTemPortfolio.Text = ""
        cboTemCCY.Text = ""
        txtTemBenName.Text = ""
        CboTemBenSubBankName.Text = ""
        CboTemBenBankName.Text = ""
        ClearBen()
        ClearSubBank()
        ClearBank()
    End Sub

    Private Sub ClearTemplateSearches()
        cboTemFindName.DataSource = Nothing
        cboTemFindCCY.DataSource = Nothing
        cboTemFindCCY.Text = ""
        cboTemRetrieveCCY.DataSource = Nothing
        cboTemRetrieveCCY.Text = ""
        CboTemRetrieveBen.DataSource = Nothing
        CboTemRetrieveBen.Text = ""
        cboTemCCY.DataSource = Nothing
        cboTemCCY.Text = ""
        ClearTemplateForm()
    End Sub


    Private Sub PopulateTemplateFormDetails(ByVal varPortfolioCode As Integer, ByVal varCCYCode As Integer, ByVal varBeneficiaryTemplateName As String)
        Dim Lst As New Dictionary(Of String, String)
        ClearTemplateForm()

        If varBeneficiaryTemplateName <> "" Then
            Lst = ClsIMS.GetTemplateFormData(varPortfolioCode, varCCYCode, varBeneficiaryTemplateName)
            If Not Lst Is Nothing Then
                If Lst.ContainsKey("Ben Pa_Portfolio") Then
                    ClsIMS.PopulateComboboxWithData(cboTemPortfolio, "select PF_Code,Portfolio from vwDolfinPaymentSelectAccount group by PF_Code,Portfolio order by Portfolio")
                    cboTemPortfolio.Text = Lst.Item("Ben Pa_Portfolio").ToString
                End If

                If Lst.ContainsKey("Ben Pa_CCY") Then
                    If Lst.ContainsKey("Ben Pa_Portfolio") Then
                        ClsIMS.PopulateComboboxWithData(cboTemCCY, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where portfolio = '" & Lst.Item("Ben Pa_Portfolio").ToString & "' group by CR_Code,CR_Name1 order by CR_Name1")
                    End If
                    cboTemCCY.Text = Lst.Item("Ben Pa_CCY").ToString
                End If

                If Lst.ContainsKey("Ben Pa_Name") Or Lst.ContainsKey("Ben Pa_Surname") Then
                    txtTemTemplateName.Text = cboTemFindName.Text
                    If Lst.ContainsKey("Ben Pa_ID") Then
                        varBenAddressID = Lst.Item("Ben Pa_ID")
                    End If
                    txtTemBenName.Text = Lst.Item("Ben Pa_Name").ToString
                    If Lst.ContainsKey("Ben Pa_Firstname") Then
                        txttembenfirstname.Text = Lst.Item("Ben Pa_Firstname").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Middlename") Then
                        txtTembenmiddlename.Text = Lst.Item("Ben Pa_Middlename").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Surname") Then
                        txttembensurname.Text = Lst.Item("Ben Pa_Surname").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Address1") Then
                        txtTemBenAddress1.Text = Lst.Item("Ben Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Address2") Then
                        txtTemBenAddress2.Text = Lst.Item("Ben Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_County") Then
                        txttembencounty.Text = Lst.Item("Ben Pa_County").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Zip") Then
                        txtTemBenZip.Text = Lst.Item("Ben Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_CountryID") Then
                        cboTemBenCountry.SelectedValue = IIf(Lst.Item("Ben Pa_CountryID") = "", 0, Lst.Item("Ben Pa_CountryID"))
                    End If
                    If Lst.ContainsKey("Ben Pa_Country") Then
                        cboTemBenCountry.Text = Lst.Item("Ben Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Swift") Then
                        txtTemBenSwift.Text = Lst.Item("Ben Pa_Swift").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_IBAN") Then
                        txtTemBenIBAN.Text = Lst.Item("Ben Pa_IBAN").ToString()
                    End If
                    If Lst.ContainsKey("Ben Pa_Other") Then
                        txtTemBenOther1.Text = Lst.Item("Ben Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_BIK") Then
                        txtTemBenBIK.Text = Lst.Item("Ben Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_INN") Then
                        txtTemBenINN.Text = Lst.Item("Ben Pa_INN").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_BeneficiaryTypeID") Then
                        cboTemBenType.SelectedValue = IIf(Lst.Item("Ben Pa_BeneficiaryTypeID") = "", 0, Lst.Item("Ben Pa_BeneficiaryTypeID"))
                    End If
                    If Lst.ContainsKey("Ben Pa_RelationshipID") Then
                        cbotembenrelationship.SelectedValue = IIf(Lst.Item("Ben Pa_RelationshipID") = "", 5, Lst.Item("Ben Pa_RelationshipID"))
                    End If
                End If

                If Lst.ContainsKey("SubBank Pa_Name") Then
                    varBenSubBankAddressID = Lst.Item("SubBank Pa_ID")
                    CboTemBenSubBankName.Text = Lst.Item("SubBank Pa_Name").ToString
                    ChkTemBenSubBank.Checked = True
                    EnableSubBank()

                    If Lst.ContainsKey("SubBank Pa_Address1") Then
                        txtTemBenSubBankAddress1.Text = Lst.Item("SubBank Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Address2") Then
                        txtTemBenSubBankAddress2.Text = Lst.Item("SubBank Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Zip") Then
                        txtTemBenSubBankZip.Text = Lst.Item("SubBank Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_CountryID") Then
                        cboTemBenSubBankCountry.SelectedValue = IIf(Lst.Item("SubBank Pa_CountryID") = "", 0, Lst.Item("SubBank Pa_CountryID"))
                    End If
                    If Lst.ContainsKey("SubBank Pa_Country") Then
                        cboTemBenSubBankCountry.Text = Lst.Item("SubBank Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Swift") Then
                        txtTemBenSubBankSwift.Text = Lst.Item("SubBank Pa_Swift").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_IBAN") Then
                        txtTemBenSubBankIBAN.Text = Lst.Item("SubBank Pa_IBAN").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Other") Then
                        txtTemBenSubBankOther1.Text = Lst.Item("SubBank Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_BIK") Then
                        txtTemBenSubBankBIK.Text = Lst.Item("SubBank Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_INN") Then
                        txtTemBenSubBankINN.Text = Lst.Item("SubBank Pa_INN").ToString
                    End If
                End If

                If Lst.ContainsKey("Bank Pa_Name") Then
                    varBenBankAddressID = Lst.Item("Bank Pa_ID")
                    CboTemBenBankName.Text = Lst.Item("Bank Pa_Name").ToString

                    If Lst.ContainsKey("Bank Pa_Address1") Then
                        txtTemBenBankAddress1.Text = Lst.Item("Bank Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Address2") Then
                        txtTemBenBankAddress2.Text = Lst.Item("Bank Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Zip") Then
                        txtTemBenBankZip.Text = Lst.Item("Bank Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_CountryID") Then
                        cboTemBenBankCountry.SelectedValue = IIf(Lst.Item("Bank Pa_CountryID") = "", 0, Lst.Item("Bank Pa_CountryID"))
                    End If
                    If Lst.ContainsKey("Bank Pa_Country") Then
                        cboTemBenBankCountry.Text = Lst.Item("Bank Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Swift") Then
                        txtTemBenBankSwift.Text = Lst.Item("Bank Pa_Swift").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_IBAN") Then
                        txtTemBenBankIBAN.Text = Lst.Item("Bank Pa_IBAN").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Other") Then
                        txtTemBenBankOther1.Text = Lst.Item("Bank Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_BIK") Then
                        txtTemBenBankBIK.Text = Lst.Item("Bank Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_INN") Then
                        txtTemBenBankINN.Text = Lst.Item("Bank Pa_INN").ToString
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub PopulateTemplateFormRetrievalDetails(ByVal varID As Integer)
        Dim Lst As New Dictionary(Of String, String)
        ClearTemplateForm()

        Dim varSQL As String
        varSQL = "select Pa_ID,Pa_BeneficiaryTypeID,Pa_Name,Pa_Firstname,Pa_Middlename,Pa_Surname,Pa_Address1,Pa_Address2,Pa_County,Pa_CountryID,Pa_Country,Pa_Zip,Pa_Swift,Pa_IBAN,Pa_Other,Pa_BIK,Pa_INN,Pa_TemplateSelected from vwDolfinPayment p inner join vwDolfinPaymentaddress pa on p.P_BenAddressID = pa.Pa_ID where p_id = " & varID
        Lst = ClsIMS.GetDataToList(varSQL)

        If Not Lst Is Nothing Then
            If Lst.ContainsKey("Pa_Name") Then
                varBenAddressID = Lst.Item("Pa_ID")
                txtTemBenName.Text = Lst.Item("Pa_Name").ToString

                If Lst.ContainsKey("Pa_BeneficiaryTypeID") Then
                    cboTemBenType.SelectedValue = IIf(Lst.Item("Pa_BeneficiaryTypeID") = "", 0, Lst.Item("Pa_BeneficiaryTypeID"))
                End If
                If Lst.ContainsKey("Pa_Firstname") Then
                    txttembenfirstname.Text = Lst.Item("Pa_Firstname").ToString
                End If
                If Lst.ContainsKey("Pa_Middlename") Then
                    txtTembenmiddlename.Text = Lst.Item("Pa_Middlename").ToString
                End If
                If Lst.ContainsKey("Pa_Surname") Then
                    txttembensurname.Text = Lst.Item("Pa_Surname").ToString
                End If
                If Lst.ContainsKey("Pa_Address1") Then
                    txtTemBenAddress1.Text = Lst.Item("Pa_Address1").ToString
                End If
                If Lst.ContainsKey("Pa_Address2") Then
                    txtTemBenAddress2.Text = Lst.Item("Pa_Address2").ToString
                End If
                If Lst.ContainsKey("Pa_County") Then
                    txttembencounty.Text = Lst.Item("Pa_County").ToString
                End If
                If Lst.ContainsKey("Pa_Zip") Then
                    txtTemBenZip.Text = Lst.Item("Pa_Zip").ToString
                End If
                If Lst.ContainsKey("Pa_CountryID") Then
                    cboTemBenCountry.SelectedValue = IIf(Lst.Item("Pa_CountryID") = "", 0, Lst.Item("Pa_CountryID"))
                End If
                If Lst.ContainsKey("Pa_Country") Then
                    cboTemBenCountry.Text = Lst.Item("Pa_Country").ToString
                End If
                If Lst.ContainsKey("Pa_Swift") Then
                    txtTemBenSwift.Text = Lst.Item("Pa_Swift").ToString
                End If
                If Lst.ContainsKey("Pa_IBAN") Then
                    txtTemBenIBAN.Text = Lst.Item("Pa_IBAN").ToString()
                End If
                If Lst.ContainsKey("Pa_Other") Then
                    txtTemBenOther1.Text = Lst.Item("Pa_Other").ToString
                End If
                If Lst.ContainsKey("Pa_BIK") Then
                    txtTemBenBIK.Text = Lst.Item("Pa_BIK").ToString
                End If
                If Lst.ContainsKey("Pa_INN") Then
                    txtTemBenINN.Text = Lst.Item("Pa_INN").ToString
                End If
                If Lst.ContainsKey("Pa_RelationshipID") Then
                    cbotembenrelationship.SelectedValue = IIf(Lst.Item("Pa_RelationshipID") = "", 5, Lst.Item("Pa_RelationshipID"))
                End If
            End If
        End If

        varSQL = "Select Pa_ID,Pa_Name,Pa_Address1,Pa_Address2,Pa_Zip,Pa_CountryID,Pa_Country,Pa_Swift,Pa_IBAN,Pa_Other,Pa_BIK,Pa_INN,Pa_TemplateSelected from vwDolfinPayment p inner join vwDolfinPaymentaddress pa On p.P_BenSubBankAddressID = pa.Pa_ID where p_id = " & varID
        Lst = ClsIMS.GetDataToList(varSQL)

        If Not Lst Is Nothing Then
            If Lst.ContainsKey("Pa_Name") Then
                varBenSubBankAddressID = Lst.Item("Pa_ID")
                CboTemBenSubBankName.Text = Lst.Item("Pa_Name").ToString
                ChkTemBenSubBank.Checked = True
                EnableSubBank()

                If Lst.ContainsKey("Pa_Address1") Then
                    txtTemBenSubBankAddress1.Text = Lst.Item("Pa_Address1").ToString
                End If
                If Lst.ContainsKey("Pa_Address2") Then
                    txtTemBenSubBankAddress2.Text = Lst.Item("Pa_Address2").ToString
                End If
                If Lst.ContainsKey("Pa_Zip") Then
                    txtTemBenSubBankZip.Text = Lst.Item("Pa_Zip").ToString
                End If
                If Lst.ContainsKey("Pa_CountryID") Then
                    cboTemBenSubBankCountry.SelectedValue = IIf(Lst.Item("Pa_CountryID") = "", 0, Lst.Item("Pa_CountryID"))
                End If
                If Lst.ContainsKey("Pa_Country") Then
                    cboTemBenSubBankCountry.Text = Lst.Item("Pa_Country").ToString
                End If
                If Lst.ContainsKey("Pa_Swift") Then
                    txtTemBenSubBankSwift.Text = Lst.Item("Pa_Swift").ToString
                End If
                If Lst.ContainsKey("Pa_IBAN") Then
                    txtTemBenSubBankIBAN.Text = Lst.Item("Pa_IBAN").ToString
                End If
                If Lst.ContainsKey("Pa_Other") Then
                    txtTemBenSubBankOther1.Text = Lst.Item("Pa_Other").ToString
                End If
                If Lst.ContainsKey("Pa_BIK") Then
                    txtTemBenSubBankBIK.Text = Lst.Item("Pa_BIK").ToString
                End If
                If Lst.ContainsKey("Pa_INN") Then
                    txtTemBenSubBankINN.Text = Lst.Item("Pa_INN").ToString
                End If
            End If
        End If

        varSQL = "select Pa_ID,Pa_Name,Pa_Address1,Pa_Address2,Pa_Zip,Pa_CountryID,Pa_Country,Pa_Swift,Pa_IBAN,Pa_Other,Pa_BIK,Pa_INN,Pa_TemplateSelected from vwDolfinPayment p inner join vwDolfinPaymentaddress pa on p.P_BenBankAddressID = pa.Pa_ID where p_id = " & varID
        Lst = ClsIMS.GetDataToList(varSQL)
        If Not Lst Is Nothing Then
            If Lst.ContainsKey("Pa_Name") Then
                varBenBankAddressID = Lst.Item("Pa_ID")
                CboTemBenBankName.Text = Lst.Item("Pa_Name").ToString

                If Lst.ContainsKey("Pa_Address1") Then
                    txtTemBenBankAddress1.Text = Lst.Item("Pa_Address1").ToString
                End If
                If Lst.ContainsKey("Pa_Address2") Then
                    txtTemBenBankAddress2.Text = Lst.Item("Pa_Address2").ToString
                End If
                If Lst.ContainsKey("Pa_Zip") Then
                    txtTemBenBankZip.Text = Lst.Item("Pa_Zip").ToString
                End If
                If Lst.ContainsKey("Pa_CountryID") Then
                    cboTemBenBankCountry.SelectedValue = IIf(Lst.Item("Pa_CountryID") = "", 0, Lst.Item("Pa_CountryID"))
                End If
                If Lst.ContainsKey("Pa_Country") Then
                    cboTemBenBankCountry.Text = Lst.Item("Pa_Country").ToString
                End If
                If Lst.ContainsKey("Pa_Swift") Then
                    txtTemBenBankSwift.Text = Lst.Item("Pa_Swift").ToString
                End If
                If Lst.ContainsKey("Pa_IBAN") Then
                    txtTemBenBankIBAN.Text = Lst.Item("Pa_IBAN").ToString
                End If
                If Lst.ContainsKey("Pa_Other") Then
                    txtTemBenBankOther1.Text = Lst.Item("Pa_Other").ToString
                End If
                If Lst.ContainsKey("Pa_BIK") Then
                    txtTemBenBankBIK.Text = Lst.Item("Pa_BIK").ToString
                End If
                If Lst.ContainsKey("Pa_INN") Then
                    txtTemBenBankINN.Text = Lst.Item("Pa_INN").ToString
                End If
            End If
        End If
    End Sub

    Private Sub PopulateExtraFormDetails(ByVal SelectedOrder As Integer)
        Dim Lst As New Dictionary(Of String, String)

        If SelectedOrder = 2 Then
            If IsNumeric(txtTemBenName.Text) Then
                ClearBen()
                Lst = ClsIMS.GetDataToList("Select B_Name1, B_Code, B_Address, B_Area, B_Zip, B_SwiftAddress,SC_CountryID,SC_Country from vwDolfinPaymentBrokers where b_Code = " & txtTemBenName.Text & " order by B_Name1")
                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        txtTemBenName.Text = Lst.Item("B_Name1").ToString
                        If Lst.ContainsKey("B_Address") Then
                            txtTemBenAddress1.Text = Lst.Item("B_Address").ToString
                        End If
                        If Lst.ContainsKey("B_Area") Then
                            txtTemBenAddress2.Text = Lst.Item("B_Area").ToString
                        End If
                        If Lst.ContainsKey("B_Zip") Then
                            txtTemBenZip.Text = Lst.Item("B_Zip").ToString
                        End If
                        If Lst.ContainsKey("SC_CountryID") Then
                            cboTemBenCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                        If Lst.ContainsKey("SC_Country") Then
                            cboTemBenCountry.Text = Lst.Item("SC_Country").ToString
                        End If
                        If Lst.ContainsKey("B_SwiftAddress") Then
                            txtTemBenSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        End If
                        If Lst.ContainsKey("T_IBAN") Then
                            If Lst.Item("T_IBAN").ToString <> "" Then
                                txtTemBenIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                            End If
                        End If
                        If Lst.ContainsKey("B_Other1") Then
                            txtTemBenOther1.Text = Lst.Item("B_Other1").ToString
                        End If
                    End If
                End If
            End If
        ElseIf SelectedOrder = 3 Then
            If IsNumeric(CboTemBenSubBankName.SelectedValue) Then
                ClearSubBank()
                Lst = ClsIMS.GetDataToList("Select B_Name1, B_Code, B_Address, B_Area, B_Zip, B_SwiftAddress,SC_CountryID,SC_Country from vwDolfinPaymentBrokers where b_Code = " & CboTemBenSubBankName.SelectedValue & " order by B_Name1")

                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        CboTemBenSubBankName.Text = Lst.Item("B_Name1").ToString
                        ChkTemBenSubBank.Checked = True
                        EnableSubBank()

                        If Lst.ContainsKey("B_Address") Then
                            txtTemBenSubBankAddress1.Text = Lst.Item("B_Address").ToString
                        End If
                        If Lst.ContainsKey("B_Area") Then
                            txtTemBenSubBankAddress2.Text = Lst.Item("B_Area").ToString
                        End If
                        If Lst.ContainsKey("B_Zip") Then
                            txtTemBenSubBankZip.Text = Lst.Item("B_Zip").ToString
                        End If
                        If Lst.ContainsKey("SC_CountryID") Then
                            cboTemBenSubBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                        If Lst.ContainsKey("SC_Country") Then
                            cboTemBenSubBankCountry.Text = Lst.Item("SC_Country").ToString
                        End If
                        If Lst.ContainsKey("B_SwiftAddress") Then
                            txtTemBenSubBankSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        End If
                        If Lst.ContainsKey("T_IBAN") Then
                            txtTemBenSubBankIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        End If
                        If Lst.ContainsKey("B_Other1") Then
                            txtTemBenSubBankOther1.Text = Lst.Item("B_Other1").ToString
                        End If
                    End If
                End If
            End If
        ElseIf SelectedOrder = 4 Then
            If IsNumeric(CboTemBenBankName.SelectedValue) Then
                ClearBank()
                Lst = ClsIMS.GetDataToList("Select B_Name1, B_Code, B_Address, B_Area, B_Zip, B_SwiftAddress,SC_CountryID,SC_Country from vwDolfinPaymentBrokers where b_Code = " & CboTemBenBankName.SelectedValue & " order by B_Name1")

                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        CboTemBenBankName.Text = Lst.Item("B_Name1").ToString

                        If Lst.ContainsKey("B_Address") Then
                            txtTemBenBankAddress1.Text = Lst.Item("B_Address").ToString
                        End If
                        If Lst.ContainsKey("B_Area") Then
                            txtTemBenBankAddress2.Text = Lst.Item("B_Area").ToString
                        End If
                        If Lst.ContainsKey("B_Zip") Then
                            txtTemBenBankZip.Text = Lst.Item("B_Zip").ToString
                        End If
                        If Lst.ContainsKey("SC_CountryID") Then
                            cboTemBenBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                        If Lst.ContainsKey("SC_Country") Then
                            cboTemBenBankCountry.Text = Lst.Item("SC_Country").ToString
                        End If
                        If Lst.ContainsKey("B_SwiftAddress") Then
                            txtTemBenBankSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        End If
                        If Lst.ContainsKey("T_IBAN") Then
                            txtTemBenBankIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        End If
                        If Lst.ContainsKey("B_Other1") Then
                            txtTemBenBankOther1.Text = Lst.Item("B_Other1").ToString
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function PopulateExtraFormDetailsFromSwiftCode(ByVal SelectedOrder As Integer, ByVal varSwiftcode As String, Optional ByVal varBankSwiftCode As String = "", Optional ByVal varCCYCode As Integer = 0) As Boolean
        Dim Lst As New Dictionary(Of String, String)

        PopulateExtraFormDetailsFromSwiftCode = False
        If SelectedOrder = 2 Then
            Lst = ClsIMS.GetFormDataFromSwift(varSwiftcode)

            If Not Lst Is Nothing Then
                PopulateExtraFormDetailsFromSwiftCode = True
                txtTemBenSwift.Text = Lst.Item("SC_SwiftCode").ToString
                If Lst.ContainsKey("SC_Address1") Then
                    txtTemBenAddress1.Text = Lst.Item("SC_Address1").ToString
                End If
                If Lst.ContainsKey("SC_City") Then
                    txtTemBenAddress2.Text = Lst.Item("SC_City").ToString
                End If
                If Lst.ContainsKey("SC_CountryID") Then
                    cboTemBenCountry.SelectedValue = Lst.Item("SC_CountryID")
                End If
                If Lst.ContainsKey("SC_Country") Then
                    cboTemBenCountry.Text = Lst.Item("SC_Country").ToString
                End If
                If Lst.ContainsKey("SC_PostCode") Then
                    txtTemBenZip.Text = Lst.Item("SC_PostCode").ToString
                End If
                If Lst.ContainsKey("SC_BIK") Then
                    txtTemBenBIK.Text = Lst.Item("SC_BIK").ToString
                End If
                If Lst.ContainsKey("SC_INN") Then
                    txtTemBenINN.Text = Lst.Item("SC_INN").ToString
                End If
            End If
        ElseIf SelectedOrder = 3 Then
            Lst = ClsIMS.GetFormDataFromSwift(varSwiftcode, varBankSwiftCode, varCCYCode)

            If Not Lst Is Nothing Then
                PopulateExtraFormDetailsFromSwiftCode = True
                txtTemBenSubBankSwift.Text = Lst.Item("SC_SwiftCode").ToString
                If Lst.ContainsKey("SC_BankName") Then
                    CboTemBenSubBankName.Text = Lst.Item("SC_BankName").ToString
                End If
                If Lst.ContainsKey("SC_Address1") Then
                    txtTemBenSubBankAddress1.Text = Lst.Item("SC_Address1").ToString
                End If
                If Lst.ContainsKey("SC_City") Then
                    txtTemBenSubBankAddress2.Text = Lst.Item("SC_City").ToString
                End If
                If Lst.ContainsKey("SC_PostCode") Then
                    txtTemBenSubBankZip.Text = Lst.Item("SC_PostCode").ToString
                End If
                If Lst.ContainsKey("SC_CountryID") Then
                    cboTemBenSubBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                End If
                If Lst.ContainsKey("SC_Country") Then
                    cboTemBenSubBankCountry.Text = Lst.Item("SC_Country").ToString
                End If
                If Lst.ContainsKey("IBankIBAN") Then
                    txtTemBenSubBankIBAN.Text = Lst.Item("IBankIBAN").ToString
                End If
                If Lst.ContainsKey("IBankBIK") Then
                    txtTemBenSubBankBIK.Text = Lst.Item("IBankBIK").ToString
                End If
                If Lst.ContainsKey("IBankINN") Then
                    txtTemBenSubBankINN.Text = Lst.Item("IBankINN").ToString
                End If
            End If
        ElseIf SelectedOrder = 4 Then
            Lst = ClsIMS.GetFormDataFromSwift(varSwiftcode)

            If Not Lst Is Nothing Then
                PopulateExtraFormDetailsFromSwiftCode = True
                txtTemBenBankSwift.Text = Lst.Item("SC_SwiftCode").ToString
                If Lst.ContainsKey("SC_BankName") Then
                    CboTemBenBankName.Text = Lst.Item("SC_BankName").ToString
                End If
                If Lst.ContainsKey("SC_Address1") Then
                    txtTemBenBankAddress1.Text = Lst.Item("SC_Address1").ToString
                End If
                If Lst.ContainsKey("SC_City") Then
                    txtTemBenBankAddress2.Text = Lst.Item("SC_City").ToString
                End If
                If Lst.ContainsKey("SC_PostCode") Then
                    txtTemBenBankZip.Text = Lst.Item("SC_PostCode").ToString
                End If
                If Lst.ContainsKey("SC_CountryID") Then
                    cboTemBenBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                End If
                If Lst.ContainsKey("SC_Country") Then
                    cboTemBenBankCountry.Text = Lst.Item("SC_Country").ToString
                End If
                If Lst.ContainsKey("SC_BIK") Then
                    txtTemBenBankBIK.Text = Lst.Item("SC_BIK").ToString
                End If
                If Lst.ContainsKey("SC_INN") Then
                    txtTemBenBankINN.Text = Lst.Item("SC_INN").ToString
                End If
            End If
        End If
    End Function

    Public Sub AssignBoxes()
        cboTemFindPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemFindPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        cboTemFindCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemFindCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        cboTemFindName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemFindName.AutoCompleteSource = AutoCompleteSource.ListItems
        CboTemRetrievePortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboTemRetrievePortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        cboTemRetrieveCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemRetrieveCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        CboTemRetrieveBen.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboTemRetrieveBen.AutoCompleteSource = AutoCompleteSource.ListItems
        cboTemPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        cboTemCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTemCCY.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub FrmTemplate_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PopulateCombos()
    End Sub

    Public Sub PopulateCombos()
        ClsIMS.PopulateComboboxWithData(CboTemBenSubBankName, "select '' as SC_SwiftCode,'' as SC_BankName union select SC_SwiftCode,SC_BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName order by SC_BankName", False)
        ClsIMS.PopulateComboboxWithData(CboTemBenBankName, "select '' as SC_SwiftCode,'' as SC_BankName union select SC_SwiftCode,SC_BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName order by SC_BankName", False)
        ClsIMS.PopulateComboboxWithData(cboTemFindPortfolio, "select Pa_portfolioCode,Pa_Portfolio from vwDolfinPaymentTemplates where branchCode = " & ClsIMS.UserLocationCode & " group by Pa_portfolioCode, Pa_Portfolio order by Pa_Portfolio")
        ClsIMS.PopulateComboboxWithData(CboTemRetrievePortfolio, "select p_portfolioCode,p_Portfolio from vwDolfinPaymentGridview where isnull(p_Portfolio,'')<>'' and isnull(P_BenAddressID,0) <> 0 and p_paymenttype in ('External Client Payment','External Payment') group by p_portfolioCode,p_Portfolio order by p_Portfolio")
        ClsIMS.PopulateComboboxWithData(cboTemPortfolio, "select PF_Code,Portfolio from vwDolfinPaymentSelectAccount where branchCode = " & ClsIMS.UserLocationCode & " group by PF_Code,Portfolio order by Portfolio")
        ClsIMS.PopulateComboboxWithData(cboTemBenCountry, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ClsIMS.PopulateComboboxWithData(cboTemBenSubBankCountry, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ClsIMS.PopulateComboboxWithData(cboTemBenBankCountry, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name")
        ClsIMS.PopulateComboboxWithData(cboTemBenType, "Select BeneficiaryTypeID, BeneficiaryType from vwDolfinPaymentBeneficiaryType order by BeneficiaryType")
        ClsIMS.PopulateComboboxWithData(cbotembenrelationship, "SELECT pty_id, pty_description from DolfinPaymentRARangePayType group by pty_id, pty_description")
        AssignBoxes()
    End Sub

    Private Sub cboTemName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemFindName.SelectedIndexChanged
        If IsNumeric(cboTemFindPortfolio.SelectedValue) Then
            If cboTemFindPortfolio.Text <> "" And cboTemFindCCY.Text <> "" Then
                PopulateTemplateFormDetails(cboTemFindPortfolio.SelectedValue, cboTemFindCCY.SelectedValue, cboTemFindName.Text)
            End If
        End If
    End Sub

    Private Sub ChkTemBenSubBank_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTemBenSubBank.CheckedChanged
        If Not ChkTemBenSubBank.Checked Then
            CboTemBenSubBankName.Text = ""
            ClearSubBank()
        Else
            EnableSubBank()
        End If
    End Sub

    Private Function CanSaveTemplate() As Boolean
        CanSaveTemplate = True
        If txtTemTemplateName.Text = "" Then
            MsgBox("Please enter a template name to save this template", vbInformation)
            CanSaveTemplate = False
        ElseIf cbotembenrelationship.Text = "" Then
            MsgBox("Please enter a relationship type to save this template", vbInformation)
            CanSaveTemplate = False
        ElseIf cbotembentype.Text = "" Then
            MsgBox("Please enter a beneficiary type to save this template", vbInformation)
            CanSaveTemplate = False
        ElseIf cboTemBenCountry.Text = "" Then
            MsgBox("Please enter a beneficiary country to save this template", vbInformation)
            CanSaveTemplate = False
        ElseIf (cboTemPortfolio.Text = "" Or cboTemCCY.Text = "") Then
            MsgBox("Please complete external client information i.e. enter portfolio and CCY", vbExclamation, "Complete External client info")
            CanSaveTemplate = False
        ElseIf txtTemTemplateName.Text <> cboTemFindName.Text Then
            If Not ClsIMS.IsTemplateNameUnique(txtTemTemplateName.Text) Then
                MsgBox("This template name is not unique. Please enter another name or cancel", vbExclamation, "Unique name required")
                CanSaveTemplate = False
            End If
        End If
    End Function

    Private Sub cmdTemAddCash_Click(sender As Object, e As EventArgs) Handles cmdTemAddCash.Click
        Dim LstUpdate As New Dictionary(Of String, String)

        If CanSaveTemplate() Then
            LstUpdate.Add("NewPay", IIf(txtTemTemplateName.Text <> cboTemFindName.Text, 1, 2))
            LstUpdate.Add("TemplateName", txtTemTemplateName.Text)
            LstUpdate.Add("Ben Pa_PortfolioCode", cboTemPortfolio.SelectedValue)
            LstUpdate.Add("Ben Pa_CCYCode", cboTemCCY.SelectedValue)
            LstUpdate.Add("Ben Pa_ID", varBenAddressID)
            LstUpdate.Add("Ben Pa_TypeID", IIf(IsNumeric(cboTemBenType.SelectedValue), cboTemBenType.SelectedValue, 0))
            LstUpdate.Add("Ben Pa_Name", txtTemBenName.Text)
            LstUpdate.Add("Ben Pa_Firstname", txttembenfirstname.Text)
            LstUpdate.Add("Ben Pa_Middlename", txtTembenmiddlename.Text)
            LstUpdate.Add("Ben Pa_Surname", txttembensurname.Text)
            LstUpdate.Add("Ben Pa_Address1", txtTemBenAddress1.Text)
            LstUpdate.Add("Ben Pa_Address2", txtTemBenAddress2.Text)
            LstUpdate.Add("Ben Pa_County", txttembencounty.Text)
            LstUpdate.Add("Ben Pa_Zip", txtTemBenZip.Text)
            LstUpdate.Add("Ben Pa_CountryID", IIf(IsNumeric(cboTemBenCountry.SelectedValue), cboTemBenCountry.SelectedValue, 0))
            LstUpdate.Add("Ben Pa_Swift", txtTemBenSwift.Text)
            LstUpdate.Add("Ben Pa_IBAN", txtTemBenIBAN.Text)
            LstUpdate.Add("Ben Pa_Other", txtTemBenOther1.Text)
            LstUpdate.Add("Ben Pa_BIK", txtTemBenBIK.Text)
            LstUpdate.Add("Ben Pa_INN", txtTemBenINN.Text)
            LstUpdate.Add("Ben Pa_RelationshipID", IIf(IsNumeric(cbotembenrelationship.SelectedValue), cbotembenrelationship.SelectedValue, 5))
            LstUpdate.Add("SubBank Pa_ID", varBenSubBankAddressID)
            LstUpdate.Add("SubBank Pa_Name", CboTemBenSubBankName.Text)
            LstUpdate.Add("SubBank Pa_Address1", txtTemBenSubBankAddress1.Text)
            LstUpdate.Add("SubBank Pa_Address2", txtTemBenSubBankAddress2.Text)
            LstUpdate.Add("SubBank Pa_Zip", txtTemBenSubBankZip.Text)
            LstUpdate.Add("SubBank Pa_CountryID", IIf(IsNumeric(cboTemBenSubBankCountry.SelectedValue), cboTemBenSubBankCountry.SelectedValue, 0))
            LstUpdate.Add("SubBank Pa_Swift", txtTemBenSubBankSwift.Text)
            LstUpdate.Add("SubBank Pa_IBAN", txtTemBenSubBankIBAN.Text)
            LstUpdate.Add("SubBank Pa_Other", txtTemBenSubBankOther1.Text)
            LstUpdate.Add("SubBank Pa_BIK", txtTemBenSubBankBIK.Text)
            LstUpdate.Add("SubBank Pa_INN", txtTemBenSubBankINN.Text)
            LstUpdate.Add("Bank Pa_ID", varBenBankAddressID)
            LstUpdate.Add("Bank Pa_Name", CboTemBenBankName.Text)
            LstUpdate.Add("Bank Pa_Address1", txtTemBenBankAddress1.Text)
            LstUpdate.Add("Bank Pa_Address2", txtTemBenBankAddress2.Text)
            LstUpdate.Add("Bank Pa_Zip", txtTemBenBankZip.Text)
            LstUpdate.Add("Bank Pa_CountryID", IIf(IsNumeric(cboTemBenBankCountry.SelectedValue), cboTemBenBankCountry.SelectedValue, 0))
            LstUpdate.Add("Bank Pa_Swift", txtTemBenBankSwift.Text)
            LstUpdate.Add("Bank Pa_IBAN", txtTemBenBankIBAN.Text)
            LstUpdate.Add("Bank Pa_Other", txtTemBenBankOther1.Text)
            LstUpdate.Add("Bank Pa_BIK", txtTemBenBankBIK.Text)
            LstUpdate.Add("Bank Pa_INN", txtTemBenBankINN.Text)

            If ClsIMS.UpdateTemplate(LstUpdate) Then
                MsgBox("Template updated", vbInformation)
                ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where isnull(Pa_TemplateName,'')<>'' group by Pa_TemplateName order by Pa_TemplateName")
                txtTemTemplateName.Text = ""
                cboTemFindPortfolio.Text = ""
                CboTemRetrievePortfolio.Text = ""
                cboTemPortfolio.Text = ""
                ClearTemplateSearches()
                PopulateCombos()
            Else
                MsgBox("There was a problem updating the template. Please contact systems support for assistance", vbCritical)
            End If
        End If
    End Sub

    Private Sub CboTemBenSubBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboTemBenSubBankName.SelectedIndexChanged
        If CboTemBenSubBankName.SelectedValue.ToString <> "System.Data.DataRowView" Then
            If CboTemBenSubBankName.SelectedValue.ToString <> "" Then
                PopulateExtraFormDetailsFromSwiftCode(3, CboTemBenSubBankName.SelectedValue, CboTemBenBankName.SelectedValue, cboTemCCY.SelectedValue)
            Else
                ClearSubBank()
            End If
        End If
    End Sub

    Private Sub CboTemBenBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboTemBenBankName.SelectedIndexChanged
        If CboTemBenBankName.SelectedValue.ToString <> "System.Data.DataRowView" Then
            If CboTemBenBankName.SelectedValue.ToString <> "" Then
                PopulateExtraFormDetailsFromSwiftCode(4, CboTemBenBankName.SelectedValue)
            Else
                ClearBank()
            End If
        End If
    End Sub

    Private Sub CboTemBenName_SelectedIndexChanged(sender As Object, e As EventArgs)
        If txtTemBenName.Text.ToString <> "System.Data.DataRowView" Then
            If txtTemBenName.Text.ToString <> "" Then
                PopulateExtraFormDetailsFromSwiftCode(2, txtTemBenName.Text)
            Else
                ClearBen()
            End If
        End If
    End Sub

    Private Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        If cboTemFindName.Text <> "" Then
            Dim varResponse As Integer = MsgBox("Are you sure you want to delete the template called " & cboTemFindName.Text & "?", vbCritical + vbYesNo, "Are you sure?")
            If varResponse = vbYes Then
                ClsIMS.DeleteTemplate(cboTemFindName.Text)
                MsgBox("Template Deleted", vbInformation)
                cboTemFindName.Text = ""
            End If
        End If
    End Sub


    Private Sub CboTemRetrieveBen_Leave(sender As Object, e As EventArgs) Handles CboTemRetrieveBen.Leave
        If IsNumeric(CboTemRetrieveBen.SelectedValue) Then
            If CboTemRetrievePortfolio.Text <> "" And CboTemRetrieveBen.Text <> "" Then
                PopulateTemplateFormRetrievalDetails(CboTemRetrieveBen.SelectedValue)
            End If
        End If
    End Sub

    Private Sub cboTemFindPortfolio_Leave(sender As Object, e As EventArgs) Handles cboTemFindPortfolio.Leave
        If IsNumeric(cboTemFindPortfolio.SelectedValue) Then
            cboTemPortfolio.Text = ""
            CboTemRetrievePortfolio.Text = ""
            ClearTemplateSearches()
            ClsIMS.PopulateComboboxWithData(cboTemFindCCY, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboTemFindPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
        End If
    End Sub

    Private Sub cboTemFindCCY_Leave(sender As Object, e As EventArgs) Handles cboTemFindCCY.Leave
        If IsNumeric(cboTemFindPortfolio.SelectedValue) And IsNumeric(cboTemFindCCY.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboTemFindName, "select 0,'' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & cboTemFindPortfolio.SelectedValue & " and Pa_CCYCode = " & cboTemFindCCY.SelectedValue & " group by Pa_TemplateName order by Pa_TemplateName")
        End If
    End Sub

    Private Sub CboTemRetrievePortfolio_Leave(sender As Object, e As EventArgs) Handles CboTemRetrievePortfolio.Leave
        If IsNumeric(CboTemRetrievePortfolio.SelectedValue) Then
            cboTemFindPortfolio.Text = ""
            cboTemPortfolio.Text = ""
            ClearTemplateSearches()
            ClsIMS.PopulateComboboxWithData(cboTemRetrieveCCY, "Select p_ccyCode,p_ccy from vwDolfinPaymentGridview where isnull(P_BenAddressID,0) <> 0 and P_portfoliocode = " & CboTemRetrievePortfolio.SelectedValue & " And p_paymenttype = 'External Payment' group by p_ccyCode,p_ccy order by p_ccy")
        End If
    End Sub

    Private Sub cboTemRetrieveCCY_Leave(sender As Object, e As EventArgs) Handles cboTemRetrieveCCY.Leave
        If IsNumeric(CboTemRetrievePortfolio.SelectedValue) And IsNumeric(cboTemRetrieveCCY.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(CboTemRetrieveBen, "select p_id,p_benName + ' (id:' + convert(nvarchar(10),P_ID) + ' td:' + convert(nvarchar(10),P_TransDate,103) + ')' from vwDolfinPaymentGridView p where isnull(P_BenAddressID,0) <> 0 and p_paymenttype = 'External Payment' and p.p_portfolioCode=" & CboTemRetrievePortfolio.SelectedValue & " and p.p_CCYCode=" & cboTemRetrieveCCY.SelectedValue & " group by p_id,p_benName + ' (id:' + convert(nvarchar(10),P_ID) + ' td:' + convert(nvarchar(10),P_TransDate,103) + ')'  order by p_benName + ' (id:' + convert(nvarchar(10),P_ID) + ' td:' + convert(nvarchar(10),P_TransDate,103) + ')' ")
        End If
    End Sub

    Private Sub cboTemPortfolio_Leave(sender As Object, e As EventArgs) Handles cboTemPortfolio.Leave
        If IsNumeric(cboTemPortfolio.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(cboTemCCY, "select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & cboTemPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
        End If
    End Sub

    Private Sub txtTemBenSubBankSwift_Leave(sender As Object, e As EventArgs) Handles txtTemBenSubBankSwift.Leave
        PopulateExtraFormDetailsFromSwiftCode(3, txtTemBenSubBankSwift.Text)
    End Sub

    Private Sub txtTemBenBankSwift_Leave(sender As Object, e As EventArgs) Handles txtTemBenBankSwift.Leave
        PopulateExtraFormDetailsFromSwiftCode(4, txtTemBenBankSwift.Text)
    End Sub

    Private Sub cboTemBenType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemBenType.SelectedIndexChanged
        If IsNumeric(cboTemBenType.SelectedValue) Then
            If cboTemBenType.SelectedValue = 0 Then
                txtTemBenName.Enabled = False
                txtTemBenName.Text = ""
                txttembenfirstname.Enabled = True
                txtTembenmiddlename.Enabled = True
                txttembensurname.Enabled = True
            Else
                txtTemBenName.Enabled = True
                txttembenfirstname.Text = ""
                txttembenfirstname.Enabled = False
                txtTembenmiddlename.Text = ""
                txtTembenmiddlename.Enabled = False
                txttembensurname.Text = ""
                txttembensurname.Enabled = False
                cbotembenrelationship.SelectedValue = 5
            End If
        End If
    End Sub

    Private Sub cboTemFindPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemFindPortfolio.SelectedIndexChanged

    End Sub
End Class