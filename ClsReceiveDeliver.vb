﻿Imports Microsoft.Office
Imports System.Math
Imports System.Web.Script
Imports System.Net

Public Class ClsReceiveDeliver
    Private varTransactionID As Double
    Private varSelectedAuthoriseOption As Integer
    Private varStatusID As Double
    Private varOrderRef As String
    Private varPortfolioCode As Double
    Private varPortfolioName As String
    Private varPortfolioShortcut As String
    Private varCCYCode As Double
    Private varCCYName As String
    Private varRecType As Double
    Private varMessageTypeID As Double
    Private varIMSTrCode As Double
    Private varInstrCode As Double
    Private varInstrName As String
    Private varInstrISIN As String
    Private varInstrShortcut As String
    Private varBrokerCode As Double
    Private varBrokerName As String
    Private varBrokerShortcut As String
    Private varCashAccountCode As Double
    Private varCashAccountBrokerName As String
    Private varCashAccountBrokerShortcut As String
    Private varCashAccountName As String
    Private varCashAccountShortcut As String
    Private varClearerCode As Double
    Private varClearerName As String
    Private varTransDate As Date
    Private varSettleDate As Date
    Private varAmount As Double
    Private varAgent As String
    Private varAgentName As String
    Private varAgentAccountNo As String
    Private varPlaceofSettlement As String
    Private varInstructions As String
    Private varSendIMS As Boolean
    Private varSendMiniOMS As Boolean
    Private varEmail As String = ""
    Private varFilePath As String = ""

    Property SelectedTransactionID() As Double
        Get
            Return varTransactionID
        End Get
        Set(value As Double)
            varTransactionID = value
        End Set
    End Property

    Property SelectedAuthoriseOption As Integer
        Get
            Return varSelectedAuthoriseOption
        End Get
        Set(value As Integer)
            varSelectedAuthoriseOption = value
        End Set
    End Property

    Property StatusID() As Double
        Get
            Return varStatusID
        End Get
        Set(value As Double)
            varStatusID = value
        End Set
    End Property

    Property OrderRef() As String
        Get
            Return varOrderRef
        End Get
        Set(value As String)
            varOrderRef = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property PortfolioShortcut() As String
        Get
            Return varPortfolioShortcut
        End Get
        Set(value As String)
            varPortfolioShortcut = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property

    Property RecType() As Double
        Get
            Return varRecType
        End Get
        Set(value As Double)
            varRecType = value
        End Set
    End Property

    Property MessageTypeID() As Double
        Get
            Return varMessageTypeID
        End Get
        Set(value As Double)
            varMessageTypeID = value
        End Set
    End Property

    Property IMSTrCode() As Double
        Get
            Return varIMSTrCode
        End Get
        Set(value As Double)
            varIMSTrCode = value
        End Set
    End Property

    Property InstrCode() As Double
        Get
            Return varInstrCode
        End Get
        Set(value As Double)
            varInstrCode = value
        End Set
    End Property

    Property InstrISIN() As String
        Get
            Return varInstrISIN
        End Get
        Set(value As String)
            varInstrISIN = value
        End Set
    End Property

    Property InstrShortcut() As String
        Get
            Return varInstrShortcut
        End Get
        Set(value As String)
            varInstrShortcut = value
        End Set
    End Property

    Property InstrName() As String
        Get
            Return varInstrName
        End Get
        Set(value As String)
            varInstrName = value
        End Set
    End Property

    Property BrokerCode() As Double
        Get
            Return varBrokerCode
        End Get
        Set(value As Double)
            varBrokerCode = value
        End Set
    End Property

    Property BrokerName() As String
        Get
            Return varBrokerName
        End Get
        Set(value As String)
            varBrokerName = value
        End Set
    End Property

    Property BrokerShortcut() As String
        Get
            Return varBrokerShortcut
        End Get
        Set(value As String)
            varBrokerShortcut = value
        End Set
    End Property

    Property CashAccountCode() As Double
        Get
            Return varCashAccountCode
        End Get
        Set(value As Double)
            varCashAccountCode = value
        End Set
    End Property

    Property CashAccountBrokerName() As String
        Get
            Return varCashAccountBrokerName
        End Get
        Set(value As String)
            varCashAccountBrokerName = value
        End Set
    End Property

    Property CashAccountBrokerShortcut() As String
        Get
            Return varCashAccountBrokerShortcut
        End Get
        Set(value As String)
            varCashAccountBrokerShortcut = value
        End Set
    End Property

    Property CashAccountName() As String
        Get
            Return varCashAccountName
        End Get
        Set(value As String)
            varCashAccountName = value
        End Set
    End Property

    Property CashAccountShortcut() As String
        Get
            Return varCashAccountShortcut
        End Get
        Set(value As String)
            varCashAccountShortcut = value
        End Set
    End Property


    Property ClearerCode() As Double
        Get
            Return varClearerCode
        End Get
        Set(value As Double)
            varClearerCode = value
        End Set
    End Property

    Property ClearerName() As String
        Get
            Return varClearerName
        End Get
        Set(value As String)
            varClearerName = value
        End Set
    End Property

    Property TransDate() As Date
        Get
            Return varTransDate
        End Get
        Set(value As Date)
            varTransDate = value
        End Set
    End Property

    Property SettleDate() As Date
        Get
            Return varSettleDate
        End Get
        Set(value As Date)
            varSettleDate = value
        End Set
    End Property

    Property Amount() As Double
        Get
            Return varAmount
        End Get
        Set(value As Double)
            varAmount = value
        End Set
    End Property

    Property Agent() As String
        Get
            Return varAgent
        End Get
        Set(value As String)
            varAgent = value
        End Set
    End Property

    Property AgentName() As String
        Get
            Return varAgentName
        End Get
        Set(value As String)
            varAgentName = value
        End Set
    End Property

    Property AgentAccountNo() As String
        Get
            Return varAgentAccountNo
        End Get
        Set(value As String)
            varAgentAccountNo = value
        End Set
    End Property

    Property PlaceofSettlement() As String
        Get
            Return varPlaceofSettlement
        End Get
        Set(value As String)
            varPlaceofSettlement = value
        End Set
    End Property

    Property Instructions() As String
        Get
            Return varInstructions
        End Get
        Set(value As String)
            varInstructions = value
        End Set
    End Property

    Property SendIMS() As Boolean
        Get
            Return varSendIMS
        End Get
        Set(value As Boolean)
            varSendIMS = value
        End Set
    End Property

    Property SendMiniOMS() As Boolean
        Get
            Return varSendMiniOMS
        End Get
        Set(value As Boolean)
            varSendMiniOMS = value
        End Set
    End Property

    Property Email() As String
        Get
            Return varEmail
        End Get
        Set(value As String)
            varEmail = value
        End Set
    End Property

    Property FilePath() As String
        Get
            Return varFilePath
        End Get
        Set(value As String)
            varFilePath = value
        End Set
    End Property


    Public Sub ClearClass()
        varTransactionID = 0
        varSelectedAuthoriseOption = 0
        varStatusID = 0
        OrderRef = ""
        varPortfolioCode = 0
        varPortfolioName = ""
        varPortfolioShortcut = ""
        varCCYCode = 0
        varCCYName = ""
        varRecType = 0
        varMessageTypeID = 0
        varIMSTrCode = 0
        varInstrCode = 0
        varInstrName = ""
        varInstrISIN = ""
        varInstrShortcut = ""
        varBrokerCode = 0
        varBrokerName = ""
        varBrokerShortcut = ""
        varCashAccountCode = 0
        varCashAccountBrokerName = ""
        varCashAccountBrokerShortcut = ""
        varCashAccountName = ""
        varCashAccountShortcut = ""
        varClearerCode = 0
        varClearerName = ""
        varTransDate = Now
        varSettleDate = Now
        varAmount = 0
        varAgent = ""
        varAgentName = ""
        varAgentAccountNo = ""
        varPlaceofSettlement = ""
        varInstructions = ""
        varEmail = ""
        varSendIMS = True
        varSendMiniOMS = False
    End Sub

    Public Sub CreateTransactionsHeadersXLRD(ByVal xlWorkSheet As Interop.Excel.Worksheet)
        xlWorkSheet.Cells(1, 1) = "Date"
        xlWorkSheet.Cells(1, 2) = "Portfolio Shortcut"
        xlWorkSheet.Cells(1, 3) = "Security Shortcut"
        xlWorkSheet.Cells(1, 4) = "Title Currency"
        xlWorkSheet.Cells(1, 5) = "Quantity"
        xlWorkSheet.Cells(1, 6) = "Accrual Amount(TI)"
        xlWorkSheet.Cells(1, 7) = "Price (TI)"
        xlWorkSheet.Cells(1, 8) = "Value (TI)"
        xlWorkSheet.Cells(1, 9) = "FX Rate"
        xlWorkSheet.Cells(1, 10) = "Value (PF)"
        xlWorkSheet.Cells(1, 11) = "Fx Rate(TiCCY)"
        xlWorkSheet.Cells(1, 12) = "Value(TiCCY)"
        xlWorkSheet.Cells(1, 13) = "Comments"
        xlWorkSheet.Cells(1, 14) = "TDShortcut"
        xlWorkSheet.Cells(1, 15) = "Counterparty Shortcut"
        xlWorkSheet.Cells(1, 16) = "Account Shortcut"
        xlWorkSheet.Cells(1, 17) = "Settlement Date"
        xlWorkSheet.Cells(1, 18) = "Broker Com (TI)"
        xlWorkSheet.Cells(1, 19) = "Mngmnt Com (TI)"
        xlWorkSheet.Cells(1, 20) = "Expenses (TI)"
        xlWorkSheet.Cells(1, 21) = "Executing Party"
        xlWorkSheet.Cells(1, 22) = "Title Settlement Broker"
        xlWorkSheet.Cells(1, 23) = "Cash Settlement Broker"
        xlWorkSheet.Cells(1, 24) = "TaxAct"
        xlWorkSheet.Cells(1, 25) = "DocNo"
        xlWorkSheet.Cells(1, 26) = "SettleRate"
    End Sub


    Public Function CreateTransactionsXLRD(ByVal FullFilePathXL As String) As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim j As Integer

        Try
            xlApp = New Interop.Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")
            CreateTransactionsHeadersXLRD(xlWorkSheet)

            Dim reader As SqlClient.SqlDataReader = ClsIMS.GetSQLData("select * from vwDolfinPaymentReceiveDeliverGridView where prd_statusid = 1")

            j = 2
            If reader.HasRows Then
                While reader.Read
                    ClsIMS.GetReaderItemIntoRDClass(reader(0).ToString)
                    xlWorkSheet.Cells(j, 1) = varTransDate.ToString("yyyy-MM-dd")
                    xlWorkSheet.Cells(j, 2) = varPortfolioShortcut
                    xlWorkSheet.Cells(j, 3) = varInstrShortcut
                    xlWorkSheet.Cells(j, 4) = varCCYName
                    xlWorkSheet.Cells(j, 5) = Abs(varAmount)
                    xlWorkSheet.Cells(j, 6) = ""
                    xlWorkSheet.Cells(j, 7) = ""
                    xlWorkSheet.Cells(j, 8) = Abs(varAmount)
                    xlWorkSheet.Cells(j, 9) = "1"
                    xlWorkSheet.Cells(j, 10) = "0.00"
                    xlWorkSheet.Cells(j, 11) = "1"
                    xlWorkSheet.Cells(j, 12) = "0.00"
                    If varRecType = 1 Then
                        xlWorkSheet.Cells(j, 13) = "Receive Free"
                        xlWorkSheet.Cells(j, 14) = "RFR"
                    ElseIf varRecType = 2 Then
                        xlWorkSheet.Cells(j, 13) = "Deliver Free"
                        xlWorkSheet.Cells(j, 14) = "DFR"
                    End If
                    xlWorkSheet.Cells(j, 15) = varBrokerShortcut
                    xlWorkSheet.Cells(j, 16) = ""
                    xlWorkSheet.Cells(j, 17) = varSettleDate.ToString("yyyy-MM-dd")
                    xlWorkSheet.Cells(j, 18) = ""
                    xlWorkSheet.Cells(j, 19) = ""
                    xlWorkSheet.Cells(j, 20) = ""
                    xlWorkSheet.Cells(j, 21) = varBrokerShortcut
                    xlWorkSheet.Cells(j, 22) = varBrokerShortcut
                    xlWorkSheet.Cells(j, 23) = varBrokerShortcut
                    xlWorkSheet.Cells(j, 24) = ""
                    xlWorkSheet.Cells(j, 25) = OrderRef & "_" & Now.ToString("yyyyMMddHHmmssfff")
                    xlWorkSheet.Cells(j, 26) = ""
                    j = j + 1
                End While
            End If
            xlWorkSheet.SaveAs(FullFilePathXL)
            xlWorkBook.Close()
            xlApp.Quit()
            CreateTransactionsXLRD = True
        Catch ex As Exception
            CreateTransactionsXLRD = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in CreateTransactionsXLRD")
        End Try
    End Function

    Public Function CreateExportFilesIMSRD(ByRef ExportFilesList As Dictionary(Of String, String)) As Boolean
        Dim varFileDate As Date = Now

        CreateExportFilesIMSRD = True
        Dim FullFilePath As String = ClsPayAuth.GetFullFilePath(ClsIMS.GetFileExportName("IMSName"), ExportFilesList, varFileDate)
        Dim objWriter As IO.StreamWriter = IO.File.CreateText(FullFilePath)
        Dim varCount As Integer = 0

        CreateTransactionsHeadersIFT(objWriter, varFileDate)
        CreateIMSStringIFTRD(objWriter)
        CreateTransactionsTrailerIFT(objWriter, 1)

        If ClsRD.SelectedTransactionID <> 0 Then
            ClsIMS.UpdateTransactionRowRD(ClsRD.SelectedTransactionID, ClsIMS.GetFileExportName("IMSName"), Dir(FullFilePath))
        End If

        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "ID: " & ClsRD.SelectedTransactionID & ", File: " & FullFilePath, ClsRD.SelectedTransactionID)
        varCount = varCount + 1

        objWriter.WriteLine()
        objWriter.Close()
    End Function

    Public Sub CreateTransactionsHeadersIFT(ByVal objWriter As IO.StreamWriter, ByVal varFileDate As Date)
        objWriter.Write("00;") 'rec type - always 00
        objWriter.Write("TRANDC;") ' file type
        objWriter.Write(varFileDate.Date.ToString("yyyy-MM-dd") & ";") ' ref date for data in file
        objWriter.Write(varFileDate.Date.ToString("yyyy-MM-dd") & ";") ' extraction date for data in file
        objWriter.Write(varFileDate.ToString("HH:mm:ss.fff") & ";") ' extraction date for data in file
        objWriter.Write("F;") 'Full or partial data
        objWriter.Write(ClsIMS.GetFileExportName("SourceSystem") & ";") 'source system
        objWriter.Write("001;") 'always 1.0
        objWriter.Write("DM") 'always FU
        objWriter.WriteLine()
    End Sub

    Public Sub CreateTransactionsTrailerIFT(ByVal objWriter As IO.StreamWriter, ByVal varCount As Integer)
        objWriter.Write("99;") 'rec type - always 00
        objWriter.Write(varCount) ' file type
    End Sub

    Private Sub CreateIMSStringIFTRD(ByRef objWriter As IO.StreamWriter)
        Dim stLine As String = ""

        Try
            objWriter.Write("20;") ' record layout code
            objWriter.Write(OrderRef & ";")
            objWriter.Write("1;") ' ProdCode
            objWriter.Write("RD;") ' ActCode
            objWriter.Write("SEC;") ' EventStatus
            objWriter.Write("IO;") ' EventStatus

            If varRecType = 1 Then
                objWriter.Write("RFR;")
            ElseIf varRecType = 2 Then
                objWriter.Write("DFR;")
            End If

            objWriter.Write(varSettleDate.ToString("yyyy-MM-dd") & ";") ' TranDate (date portfolio is updated)
            objWriter.Write(varSettleDate.ToString("yyyy-MM-dd") & ";") ' TranDate (date portfolio is updated)
            objWriter.Write(varPortfolioShortcut & ";") 'Unique portfolio code (shortcut)
            objWriter.Write(varInstrShortcut & ";") 'Unique account code (shortcut)
            objWriter.Write(varCCYName & ";") 'Currency code (eg. EUR, USD, GBP)
            objWriter.Write(varCashAccountShortcut & ";") 'Unique bank code (shortcut)
            objWriter.Write(";;")
            objWriter.Write(varBrokerShortcut & ";") 'Unique bank code (shortcut)
            objWriter.Write(varCashAccountBrokerShortcut & ";") 'Unique bank code (shortcut)
            objWriter.Write(varCashAccountBrokerShortcut & ";") 'Unique bank code (shortcut)

            objWriter.Write(Abs(varAmount) & ";") ' Transaction amount in account currency
            objWriter.Write("0.0;0.0;0.0;0.0;0.0;0.0;") 'INFO only
            objWriter.Write(OrderRef & ";") 'INFO Only. Can be the transaction Ref.No of the source system as displayed to end-users
            objWriter.Write(";;") 'INFO only: It be used to group related transactions. The i/f won't process this info (except from persisting it).
            If varRecType = 1 Then
                objWriter.Write(Replace(Replace(Replace(Replace(Left("Receive Free", 255), ",", ""), """", ""), "'", ""), ";", "")) 'Free narrative field
            ElseIf varRecType = 2 Then
                objWriter.Write(Replace(Replace(Replace(Replace(Left("Deliver Free", 255), ",", ""), """", ""), "'", ""), ";", "")) 'Free narrative field
            End If
            objWriter.WriteLine()

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - error in CreateIMSStringIFT RD")
        End Try
    End Sub

    Public Function CopyExportFilesRD(ByRef ExportFilesList As Dictionary(Of String, String), ByRef ExportFileCount As Dictionary(Of String, Integer)) As Boolean
        Dim ExportFileName As String = ""
        Dim varIMSCount As Integer
        Try
            If Not IO.Directory.Exists(ClsIMS.GetFilePath("IMSExport")) Then
                IO.Directory.CreateDirectory(ClsIMS.GetFilePath("IMSExport"))
            End If

            For exportFile As Integer = 0 To ExportFilesList.Count - 1
                ExportFileName = ExportFilesList.Item(ExportFilesList.Keys(exportFile)).ToString
                If InStr(1, ExportFilesList.Keys(exportFile).ToString, ClsIMS.GetFileExportName("IMSName"), vbTextCompare) <> 0 Then
                    IO.File.Copy(ExportFileName, ClsIMS.GetFilePath("IMSExport") & Dir(ExportFileName))
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "Copied file:  " & ExportFileName & " to " & ClsIMS.GetFilePath("IMSExport") & Dir(ExportFileName), 0)
                    varIMSCount = varIMSCount + 1
                End If
            Next
            ExportFileCount.Add(ClsIMS.GetFileExportName("IMSName"), varIMSCount)
            CopyExportFilesRD = True
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "CopyExportFiles: Copied all files", 0)
        Catch ex As Exception
            CopyExportFilesRD = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - CopyExportFiles: could not copy transaction files - exportfilesList(exportfile): " & ExportFileName)
        End Try
    End Function

    Public Function GetBloombergPrices(ByVal Ticker As String, FromDate As Date, ToDate As Date, Optional ByVal ReturnError As Boolean = False) As Object
        Dim url As String = "http://10.1.15.96:35111/?columns=PX_OPEN,PX_LAST" & "&tickers=" & Replace(Ticker, " ", "%20") & "&dates=" & Format(FromDate, "yyyMMdd") & "-" & Format(ToDate, "yyyMMdd") ' ,PX_HIGH,PX_LOW,PX_VOLUME
        Dim varwebclient As New WebClient()
        Dim JSONString As String = ""
        Dim jss As New Serialization.JavaScriptSerializer
        Dim JSON As Object
        Try
            JSONString = varwebclient.DownloadString(url)
            Dim BBGResult As Dictionary(Of String, Object) = jss.Deserialize(Of Dictionary(Of String, Object))(JSONString)
            ' This will return the collection of prices
            JSON = BBGResult(Ticker)
            Return JSON
        Catch ex As Exception
            MsgBox("Error retrieving price details for: {0}. {1}", Ticker, ex.Message)
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - GetBloombergPrices")
            Return Nothing
        End Try
    End Function

    Public Sub ImportBbgPrices(Ticker As String, FromDate As Date, ToDate As Date, conn As SqlClient.SqlConnection)
        Dim PriceData As Object
        Dim PriceLastData As Object
        Dim PriceOpenData As Object
        Dim Cmd As New SqlClient.SqlCommand

        Dim colPriceDate As List(Of String) = ClsIMS.GetMultipleDataToList("SELECT DateValue = FORMAT(Date, 'yyyy-MM-dd') FROM dbo.uftblWorkingDates('" & FromDate.ToString("yyyy-MM-dd") & "', '" & ToDate.ToString("yyyy-MM-dd") & "', 165)")

        Try
            PriceData = GetBloombergPrices(Ticker, FromDate, ToDate)
            PriceLastData = PriceData("PX_LAST") ' This navigates to the PX_LAST set
            PriceOpenData = PriceData("PX_OPEN")

            Cmd.Connection = conn
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "dbo._Dolfin_InsertPrice"

            For Each item In colPriceDate
                Try
                    If PriceLastData(item.ToString) & PriceOpenData(item.ToString) <> "" Then
                        With Cmd
                            .Parameters.Clear()
                            Cmd.Parameters.Add("@secSymbol", SqlDbType.VarChar).Value = Ticker
                            Cmd.Parameters.Add("@prDate", SqlDbType.DateTime).Value = item.ToString ' Date Value
                            Cmd.Parameters.Add("@prClose", SqlDbType.Float).Value = CDec(PriceLastData(item.ToString))
                            Cmd.Parameters.Add("@prOpen", SqlDbType.Float).Value = CDec(PriceOpenData(item.ToString))
                            Cmd.ExecuteNonQuery()
                        End With
                    End If
                Catch ex As Exception
                    ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - ImportBbgPrices (adding price to table)")
                End Try
            Next
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameReceiveDeliver, "ImportBbgPrices (added prices to IMS price table) for " & Ticker & " from: " & FromDate & " to: " & ToDate)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameReceiveDeliver, Err.Description & " - ImportBbgPrices (general function)")
        End Try
    End Sub

    Public Sub GetRDGridIntoClass(ByVal dgv As DataGridView, ByVal varRow As Integer)
        varRecType = dgv.Rows(varRow).Cells(0).Value
        varTransDate = dgv.Rows(varRow).Cells(1).Value
        varSettleDate = dgv.Rows(varRow).Cells(2).Value
        varInstrISIN = dgv.Rows(varRow).Cells(3).Value
        varAmount = dgv.Rows(varRow).Cells(4).Value
        varCCYName = dgv.Rows(varRow).Cells(5).Value
        varCCYCode = ClsIMS.GetSQLItem("Select cr_code from vwdolfinpaymentcurrencies where cr_name1 = '" & varCCYName & "'")
        Dim varInstrCodeStr As String = ClsIMS.GetSQLItem("Select t_code from vwdolfinpaymenttitles where t_isin = '" & varInstrISIN & "' and t_currency = " & varCCYCode)
        If IsNumeric(varInstrCodeStr) Then
            varInstrCode = CDbl(varInstrCodeStr)
        End If
        varSendIMS = True
    End Sub

    Public Sub SendReceiveDeliverEmailBodyTitle(ByRef varBody As String)
        varBody = varBody & "<tr><td width=250 valign=top style='width:5.0cm;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Portfolio<o:p></o:p></span></b></p></td>" &
            "<td width=100 valign=top style='width:60pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>ID<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Type<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Trans Date<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Settle Date<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>ISIN<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Amount<o:p></o:p></span></b></p></td>" &
            "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>CCY<o:p></o:p></span></b></p></td>" &
            "<td width=250 valign=top style='width:2.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Notes<o:p></o:p></span></b></p></td>"
    End Sub

    Public Sub SendReceiveDeliverEmailBody(ByRef FirstSend As Boolean, ByRef varBody As String, ByVal dgv As DataGridView, ByVal varRow As Integer, varPortfolioName As String, varID As Double, ByVal varNotes As String)
        If FirstSend Then
            varBody = varBody & "<tr><td width=250 valign=top style='width:5.0cm;border:solid windowtext 1.0pt;border-top:none;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal align=center style='text-align:center;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varPortfolioName & "<o:p></o:p></span></p></td>" &
        "<td width=100 valign=top style='width:60pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varID & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(0).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(1).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(2).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(3).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(4).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(5).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=200 valign=top style='width:2.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varNotes & "<o:p></o:p></span></p></td>" &
        "<o:p></o:p></span></p></td>"
        Else
            varBody = varBody & "<tr><td width=250 valign=top style='width:5.0cm;border:solid windowtext 1.0pt;border-top:none;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal align=center style='text-align:center;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varPortfolioName & "<o:p></o:p></span></p></td>" &
        "<td width=100 valign=top style='width:60pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varID & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(2).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(9).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(10).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(5).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(7).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=120 valign=top style='width:70pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & dgv.Rows(varRow).Cells(8).FormattedValue & "<o:p></o:p></span></p></td>" &
        "<td width=200 valign=top style='width:2.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10pt;font-family:""Arial"",""sans-serif""'>" & varNotes & "<o:p></o:p></span></p></td>" &
        "<o:p></o:p></span></p></td>"
        End If
    End Sub

    Public Sub CreateAndSendFOPEmail(ByRef FirstSend As Boolean, ByRef dgv As DataGridView, ByRef varRow As Integer, ByRef varPortfolioName As String, ByRef varID As Double, ByRef varEmail As String, ByRef varCompliance As Boolean, ByRef varNotes As String)
        Dim varBody As String = ""
        If varInstrCode = 0 Then
            varNotes = "New Instrument: This instrument for this CCY has not been found in IMS and will need to be setup"
        End If
        ClsRD.SendReceiveDeliverEmailBodyTitle(varBody)
        Dim varLinkedID As String = ClsIMS.GetSQLItem("select PRD_LinkedID from vwdolfinpaymentreceivedeliver where PRD_ID = " & varID)

        If varLinkedID <> "" Then
            Dim LstRows As New List(Of List(Of String))
            LstRows.Add(New List(Of String))
            LstRows.Add(New List(Of String))
            For varGridFindRow As Integer = 0 To dgv.RowCount - 1
                If Not IsDBNull(dgv.Rows(varGridFindRow).Cells("PRD_LinkedID").Value) Then
                    If dgv.Rows(varGridFindRow).Cells("PRD_LinkedID").Value = varLinkedID Then
                        LstRows(0).Add(dgv.Rows(varGridFindRow).Cells("PRD_LinkedID").RowIndex)
                        LstRows(1).Add(dgv.Rows(varGridFindRow).Cells("ID").Value)
                    End If
                End If
            Next
            If Not LstRows Is Nothing Then
                Dim varMaxRows As Integer = LstRows(0).Count - 1
                For j As Integer = 0 To varMaxRows
                    ClsRD.SendReceiveDeliverEmailBody(FirstSend, varBody, dgv, LstRows(0).Item(j), varPortfolioName, LstRows(1).Item(j), varNotes)
                Next
            End If
            varID = varLinkedID
        Else
            ClsRD.SendReceiveDeliverEmailBody(FirstSend, varBody, dgv, varRow, varPortfolioName, varID, varNotes)
        End If

        ClsEmail.SendReceiveDeliverEmail(varBody, varEmail, varCompliance, varID)
    End Sub


    Public Sub ConfirmDirectoryFOP()
        Dim CorrectDirectoryFOP As String = ""
        Try
            'CorrectDirectoryFOP = FileFormat(ClsIMS.GetFilePath("FOP"), varTransactionID)
            If Not IO.Directory.Exists(CorrectDirectoryFOP) Then
                IO.Directory.CreateDirectory(CorrectDirectoryFOP)
                ClsRD.FilePath = CorrectDirectoryFOP
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ConfirmDirectoryFOP: " & CorrectDirectoryFOP)
        End Try
    End Sub

End Class

