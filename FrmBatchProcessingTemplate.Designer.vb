﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBatchProcessingTemplate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboTemFindName = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPortfolio = New System.Windows.Forms.TextBox()
        Me.txtCCY = New System.Windows.Forms.TextBox()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtCCY)
        Me.GroupBox2.Controls.Add(Me.txtPortfolio)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.cboTemFindName)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(811, 88)
        Me.GroupBox2.TabIndex = 37
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Template Criteria Selection"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(687, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "CCY:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(45, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Portfolio:"
        '
        'cboTemFindName
        '
        Me.cboTemFindName.BackColor = System.Drawing.Color.LightYellow
        Me.cboTemFindName.FormattingEnabled = True
        Me.cboTemFindName.Location = New System.Drawing.Point(99, 50)
        Me.cboTemFindName.Name = "cboTemFindName"
        Me.cboTemFindName.Size = New System.Drawing.Size(706, 21)
        Me.cboTemFindName.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Template Name:"
        '
        'txtPortfolio
        '
        Me.txtPortfolio.Enabled = False
        Me.txtPortfolio.Location = New System.Drawing.Point(99, 23)
        Me.txtPortfolio.Name = "txtPortfolio"
        Me.txtPortfolio.Size = New System.Drawing.Size(582, 20)
        Me.txtPortfolio.TabIndex = 53
        '
        'txtCCY
        '
        Me.txtCCY.Enabled = False
        Me.txtCCY.Location = New System.Drawing.Point(724, 23)
        Me.txtCCY.Name = "txtCCY"
        Me.txtCCY.Size = New System.Drawing.Size(81, 20)
        Me.txtCCY.TabIndex = 54
        '
        'FrmBatchProcessingTemplate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(835, 113)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmBatchProcessingTemplate"
        Me.Text = "Batch Processing Template Select"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents cboTemFindName As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCCY As TextBox
    Friend WithEvents txtPortfolio As TextBox
End Class
