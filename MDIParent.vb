﻿Imports System.IO

Public Class MDIParent
    Implements IMessageFilter

    Private Declare Auto Function SendMessage Lib "user32" (ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr
    Public ss As StatusStrip

    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, ByVal keyData As System.Windows.Forms.Keys) As Boolean
        Try
            SendMessage(Me.ActiveMdiChild.Handle, msg.Msg, msg.WParam, msg.LParam)
            'ProcessCmdKey = True
            'Return MyBase.ProcessCmdKey(msg, keyData)
            'This allows Ctrl-C, V etc
#Disable Warning BC42353 ' Function doesn't return a value on all code paths
        Catch ex As Exception
        End Try
    End Function
#Enable Warning BC42353 ' Function doesn't return a value on all code path

    Public Sub New()
        InitializeComponent()
        Application.AddMessageFilter(Me)
        TimerInActivity.Enabled = True

    End Sub

    Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
        '' Retrigger timer on keyboard and mouse messages - 512 is auto refresh - ignore this
        PreFilterMessage = PreFilterMessage
        If (m.Msg >= &H100 And m.Msg <= &H109) Or (m.Msg >= &H200 And m.Msg <= &H20E) And m.Msg <> 512 Then
            TimerInActivity.Stop()
            TimerInActivity.Start()
        End If
    End Function

    Private Sub Users()
        NewUsers = New FrmUsers
        NewUsers.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserLogin) Then
            NewUsers.Show()
        Else
            MsgBox("You are not authorised to view the users screen", vbCritical, "Cannot view users screen")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameUsers, "Refused entry to add users form")
        End If
    End Sub

    Private Sub Audit()
        NewAudit = New FrmAuditlog
        NewAudit.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAudit) Then
            NewAudit.Show()
        Else
            MsgBox("You are not authorised to view the audit log", vbCritical, "Cannot view audit log")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameAudit, "Refused entry to audit log form")
        End If
    End Sub

    Private Sub Treasury()
        NewTA = New FrmTreasuryAllocation
        NewTA.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserOmnibus) Then
            NewTA.Show()
        Else
            MsgBox("You are not authorised to use the internal omnibus switch facility", vbCritical, "Cannot select switch facility")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameOmnibusSwitch, "Refused entry to internal omnibus switch form")
        End If
    End Sub

    Private Sub TreasuryTermDeposits()
        NewTATD = New FrmTreasuryAllocationTermDeposits
        NewTATD.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserOmnibus) Then
            NewTATD.Show()
        Else
            MsgBox("You are not authorised to use the internal omnibus switch (term deposits) facility", vbCritical, "Cannot select switch (term deposits) facility")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameOmnibusSwitch, "Refused entry to internal omnibus switch  (term deposits) form")
        End If
    End Sub


    Private Sub Movement()
        NewPaymentForm = New FrmPayment
        NewPaymentForm.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserPayments) Then
            NewPaymentForm.Show()
        Else
            MsgBox("You are not authorised to view the cash movements form", vbCritical, "Cannot view movements form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovements, "Refused entry to cash movements form")
        End If
    End Sub

    Private Sub ReceiveDeliver()
        RDForm = New FrmReceiveDeliver
        RDForm.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserRD) Then
            RDForm.Show()
        Else
            MsgBox("You are not authorised to view the Receive/Deliver form", vbCritical, "Cannot view Receive/Deliver form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameReceiveDeliver, "Refused entry to Receive/Deliver form")
        End If
    End Sub

    Private Sub AutoSwifts()
        ASForm = New FrmAllSwifts
        ASForm.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAutoSwift) Then
            ASForm.Show()
        Else
            MsgBox("You are not authorised to view the Auto Swifts form", vbCritical, "Cannot view Auto Swifts form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameViewAllSwifts, "Refused entry to Auto Swifts form")
        End If
    End Sub

    Private Sub ReadSwiftForm()
        NewSwiftRead = New FrmSwiftRead
        NewSwiftRead.MdiParent = Me
        NewSwiftRead.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSwiftRead) Then
            NewSwiftRead.Show()
        Else
            MsgBox("You are not authorised to view the Read Swifts form", vbCritical, "Cannot view Read Swifts form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameReadSwifts, "Refused entry to Read Swifts form")
        End If
    End Sub

    Private Sub ConfirmReceipt()
        NewConfirmReceipt = New FrmConfirmReceipt
        NewConfirmReceipt.MdiParent = Me
        m_ChildFormNumber += 1
        NewConfirmReceipt.Dock = DockStyle.Fill
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserConfirmReceipts) Then
            NewConfirmReceipt.Show()
        Else
            MsgBox("You are not authorised to view the receipts form", vbCritical, "Cannot view receipts form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameConfirmReceipts, "Refused entry to receipts form")
        End If
    End Sub

    Private Sub Fees()
        NewFeeSchedule = New FrmFeeSchedules
        NewFeeSchedule.MdiParent = Me
        NewFeeSchedule.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserFees) Then
            NewFeeSchedule.Show()
        Else
            MsgBox("You are not authorised to view the Fee schedules form", vbCritical, "Cannot view Fee schedules form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to Fee schedules form")
        End If
    End Sub

    Private Sub FeesPaid()
        NewFeesPaid = New FrmFeesPaid
        NewFeesPaid.MdiParent = Me
        NewFeesPaid.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserFeesPaid) Then
            NewFeesPaid.Show()
        Else
            MsgBox("You are not authorised to view the Fee Paid form", vbCritical, "Cannot view Fee Paid form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to Fee Paid form")
        End If
    End Sub

    Private Sub TreasuryManagement()
        NewTresMgt = New FrmTreasuryManagement
        NewTresMgt.MdiParent = Me
        NewTresMgt.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserTreasuryMgt) Then
            NewTresMgt.Show()
        Else
            MsgBox("You are not authorised to view the treasury management form", vbCritical, "Cannot view treasury management form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to treasury management form")
        End If
    End Sub

    Private Sub SwiftAccountStatement()
        NewAccStatement = New FrmSwiftStatements
        NewAccStatement.MdiParent = Me
        NewAccStatement.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSwiftAccStatement) Then
            NewAccStatement.Show()
        Else
            MsgBox("You are not authorised to view the swift account statement form", vbCritical, "Cannot view swift account statement form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to swift account statement form")
        End If
    End Sub

    Private Sub BatchProcessing()
        NewBP = New FrmBatchProcessing
        NewBP.MdiParent = Me
        NewBP.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserBatchProcessing) Then
            NewBP.Show()
        Else
            MsgBox("You are not authorised to view the batch processing form", vbCritical, "Cannot view batch processing form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameBatchProcessing, "Refused entry to batch processing form")
        End If
    End Sub

    Private Sub ComplianceWatchList()
        NewCWL = New FrmClientDeactivation
        NewCWL.MdiParent = Me
        'NewCWL.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserComplianceWatchList) Then
            NewCWL.Show()
        Else
            MsgBox("You are not authorised to view the compliance watchlist form", vbCritical, "Cannot view compliance watchlist form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameComplainceWatchList, "Refused entry to compliance watchlist form")
        End If
    End Sub

    Private Sub CASSAccounts()
        NewCASS = New FrmCASS
        NewCASS.MdiParent = Me
        m_ChildFormNumber += 1
        NewCASS.Dock = DockStyle.Fill
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserCASS) Then
            NewCASS.Show()
        Else
            MsgBox("You are not authorised to view the CASS accounts form", vbCritical, "Cannot view compliance watchlist form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCASSAccounts, "Refused entry to CASS accounts form")
        End If
    End Sub

    Private Sub Rec()
        NewRec = New FrmRec
        NewRec.MdiParent = Me
        NewRec.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserRec) Then
            NewRec.Show()
        Else
            MsgBox("You are not authorised to view the reconciliation tool", vbCritical, "Cannot view reconciliation form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to reconciliation form")
        End If
    End Sub

    Private Sub CorpAction()
        NewCA = New FrmCorpAction
        NewCA.MdiParent = Me
        NewCA.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserCA) Then
            NewCA.Show()
        Else
            MsgBox("You are not authorised to view the Corporate Action form", vbCritical, "Cannot view Corporate Action form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCorpAction, "Refused entry to Corporate Action form")
        End If
    End Sub

    Private Sub RiskAssessment()
        NewRA = New FrmRASettings
        NewRA.MdiParent = Me
        NewRA.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserRA) Then
            NewRA.Show()
        Else
            MsgBox("You are not authorised to view the Risk Assessment form", vbCritical, "Cannot view Risk Assessment form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCorpAction, "Refused entry to Risk Assessment form")
        End If
    End Sub

    Private Sub CashTransactionComments()
        NewC = New FrmCashTranComment
        NewC.MdiParent = Me
        NewC.Dock = DockStyle.Fill
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserC) Then
            NewC.Show()
        Else
            MsgBox("You are not authorised to view the Cash Transaction Comments form", vbCritical, "Cannot view Cash Transaction Comments form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCorpAction, "Refused entry to Cash Transaction Comments form")
        End If
    End Sub

    Private Sub PasswordGenerator()
        NewPG = New FrmPasswordGenerator
        NewPG.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserPG) Then
            NewPG.Show()
        Else
            MsgBox("You are not authorised to view the Password Generator form", vbCritical, "Cannot view Password Generator form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCorpAction, "Refused entry to Password Generator form")
        End If
    End Sub

    Private Sub GPPMappingCodes()
        NewGPP = New FrmGppMappingCodes
        NewGPP.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserGPP) Then
            NewGPP.Show()
        Else
            MsgBox("You are not authorised to view the GPP Mapping codes form", vbCritical, "Cannot view GPP Mapping codes form")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameGPP, "Refused entry to GPP Mapping codes form")
        End If
    End Sub


    Private Sub ViewMovements()
        NewViewPay = New FrmViewPayments
        NewViewPay.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserPayments) Then
            NewViewPay.Show()
        Else
            MsgBox("You are not authorised to view the movements screen", vbCritical, "Cannot view movements screen")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameViewMovements, "Refused entry to view movements screen")
        End If
    End Sub

    Private Sub CutOff()
        NewCutoff = New FrmCutOff
        NewCutoff.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserCutoff) Then
            NewCutoff.Show()
        Else
            MsgBox("You are not authorised to view the cut off times screen", vbCritical, "Cannot view cut off times screen")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCutOff, "Refused entry to cut off times form")
        End If
    End Sub

    Private Sub TransferFees()
        NewTF = New FrmTransferFees
        NewTF.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserTransferFees) Then
            NewTF.Show()
        Else
            MsgBox("You are not authorised to edit the transfer fees", vbCritical, "Cannot edit transfer fees")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameTransferFees, "Refused entry to trasnfer fees form")
        End If
    End Sub

    Private Sub Templates()
        NewTemplate = New FrmTemplate
        NewTemplate.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserTemplates) Then
            NewTemplate.Show()
        Else
            MsgBox("You are not authorised to edit the templates", vbCritical, "Cannot edit templates")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameTemplates, "Refused entry to templates form")
        End If
    End Sub

    Private Sub Balances()
        NewBal = New FrmBalances
        NewBal.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserBalances) Then
            NewBal.Show()
        Else
            MsgBox("You are not authorised to view balances", vbCritical, "Cannot view balances")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameBalances, "Refused entry to view balances")
        End If
    End Sub

    Private Sub Swiftcodes()
        NewSC = New FrmSwiftCodes
        NewSC.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSwift) Then
            NewSC.Show()
        Else
            MsgBox("You are not authorised to view Swift Codes", vbCritical, "Cannot view Swift Codes")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameSwiftCodes, "Refused entry to view Swift Codes")
        End If
    End Sub

    Private Sub ClientEmailAddresses()
        NewCEA = New FrmClientEmailAddresses
        NewCEA.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientEmailAddresses) Then
            NewCEA.Show()
        Else
            MsgBox("You are not authorised to view Client Email Addresses", vbCritical, "Cannot view Client Email Addresses")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientEmailAddresses, "Refused entry to view Client Email Addresses")
        End If
    End Sub

    Private Sub BritanniaGlobalMarketsRec()
        NewBGMR = New FrmBritanniaGlobalMarketsRec
        NewBGMR.MdiParent = Me
        m_ChildFormNumber += 1
        NewBGMR.Dock = DockStyle.Fill
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserBGMRec) Then
            NewBGMR.Show()
        Else
            MsgBox("You are not authorised to view BGM Reconcilliation", vbCritical, "Cannot view BGM Reconcilliation")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameBGMRec, "Refused entry to view BGM Reconcilliation")
        End If
    End Sub

    Private Sub ClientStatementReportGenerator()
        NewCSG = New FrmClientStatementGenerator
        NewCSG.MdiParent = Me
        m_ChildFormNumber += 1
        'If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientStatementGenerator) Then
        NewCSG.Show()
        'Else
        '    MsgBox("You are not authorised to view Client Staements", vbCritical, "Cannot view Client Statements")
        '    ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientStatementGenerator, "Refused entry to view Client Statements")
        'End If
    End Sub

    Private Sub Intermediary()
        NewSIC = New FrmSwiftIntermediaryCodes
        NewSIC.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSwiftIntermediary) Then
            NewSIC.Show()
        Else
            MsgBox("You are not authorised to view Swift Intermediary Codes", vbCritical, "Cannot view Swift Intermediary Codes")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameSwiftCodes, "Refused entry to view Swift Intermediary Codes")
        End If
    End Sub

    Private Sub SystemSettings()
        NewSS = New FrmSystemSettings
        NewSS.MdiParent = Me
        m_ChildFormNumber += 1
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSettings) Then
            NewSS.Show()
        Else
            MsgBox("You are not authorised to view System Settings", vbCritical, "Cannot view System Settings")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameSwiftCodes, "Refused entry to view System Settings")
        End If
    End Sub


    Private Sub Options()
        NewOpt = New FrmOptions
        NewOpt.MdiParent = Me
        m_ChildFormNumber += 1
        NewOpt.Show()
    End Sub

    Public Sub UpdateStatusBar(ByVal varStatusUpdate As String)
        ToolStripStatusLabel.Text = varStatusUpdate
    End Sub

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CutToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CopyToolStripMenuItem.Click
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
        'My.Computer.Clipboard()
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles PasteToolStripMenuItem.Click
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click
        Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CloseAllToolStripMenuItem.Click
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub PaymentToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PaymentToolStripMenuItem.Click
        Movement()
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(sender As Object, e As EventArgs)
        ViewMovements()
    End Sub

    Private Sub TransferFeesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransferFeesToolStripMenuItem.Click
        TransferFees()
    End Sub

    Private Sub UserDetailsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles UserDetailsToolStripMenuItem1.Click
        Users()
    End Sub

    Private Sub AuditLogToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AuditLogToolStripMenuItem.Click
        Audit()
    End Sub

    Private Sub InternalOmnibusSwitchToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InternalOmnibusSwitchToolStripMenuItem.Click
        Treasury()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        NewAbout = New FrmAbout
        NewAbout.MdiParent = Me
        m_ChildFormNumber += 1
        NewAbout.Show()
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Treasury()
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Movement()
    End Sub

    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        ViewMovements()
    End Sub

    Private Sub ToolStripButton4_Click(sender As Object, e As EventArgs) Handles ToolStripButton4.Click
        CutOff()
    End Sub

    Private Sub ToolStripButton5_Click(sender As Object, e As EventArgs) Handles ToolStripButton5.Click
        TransferFees()
    End Sub

    Private Sub ToolStripButtonInt_Click(sender As Object, e As EventArgs) Handles ToolStripButtonInt.Click
        Templates()
    End Sub

    Private Sub ToolStripButtonFee_Click(sender As Object, e As EventArgs) Handles ToolStripButtonFee.Click
        Swiftcodes()
    End Sub

    Private Sub MDIParent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Me.Visible = False
            If Not ClsIMS.LiveDB Then
                Me.BackgroundImage = Nothing
                MsgBox("Please Note: This is the UAT database. If you need the live version, please contact systems support", vbCritical)
            End If
            NewUsersLogin = New FrmUsersPassword
            NewUsersLogin.ShowDialog()

            If ClsIMS.UserCanLogin Then
                NewUsersLogin.Close()
                If ClsIMS.LiveDB() Then
                    Me.Text = "FLOW"
                Else
                    Me.Text = "FLOW UAT"
                End If
                Me.Visible = True
                Me.Focus()
                TimerInActivity.Interval = IIf(IsNumeric(ClsIMS.UserOptInActivity), ClsIMS.UserOptInActivity * 60000, 1800000)
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameUsers, "Version: " & ClsIMS.FLOWVersion)
            Else
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("There has been a problem logging into FLOW. Please contact systems support for assistance" & vbNewLine & Environment.UserDomainName & "\" & Environment.UserName & ",PC:" & Environment.MachineName & ", err:" & Err.Description, vbCritical, "Cannot log in")
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameUsers, "Login issue for: " & Environment.UserDomainName & "\" & Environment.UserName & ",PC:" & Environment.MachineName & ", err:" & Err.Description)
        End Try
    End Sub

    Private Sub ViewBalancesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewBalancesToolStripMenuItem.Click
        Balances()
    End Sub

    Private Sub ToolStripButton7_Click(sender As Object, e As EventArgs) Handles ToolStripButton7.Click
        Balances()
    End Sub

    Private Sub RefreshOpenFromGrid()
        If Not NewPaymentForm Is Nothing Then
            If NewPaymentForm.Visible Then
                ClsIMS.RefreshMovementGrid(NewPaymentForm.dgvCash, "")
            End If
        ElseIf Not NewViewPay Is Nothing Then
            If NewViewPay.Visible Then
                NewViewPay.GridWithWhereClause()
                NewViewPay.LoadFilters()
            End If
        End If
    End Sub

    Private Sub IndexToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IndexToolStripMenuItem.Click
        System.Diagnostics.Process.Start(ClsIMS.GetFilePath("Documents") & "Cash Movements User Guide.docx")
    End Sub

    Private Sub TechnicalGuideToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TechnicalGuideToolStripMenuItem.Click
        System.Diagnostics.Process.Start(ClsIMS.GetFilePath("Documents") & "Cash Movements Technical Detail.docx")
    End Sub

    Private Sub TimerInActivity_Tick(sender As Object, e As EventArgs) Handles TimerInActivity.Tick
        TimerInActivity.Stop()
        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCashMovements, "Closed down application for " & ClsIMS.FullUserName & " due to in activity")
        End
    End Sub

    Private Sub OptionsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles OptionsToolStripMenuItem1.Click
        Options()
    End Sub

    Private Sub ToolStripButton9_Click(sender As Object, e As EventArgs) Handles ToolStripButton9.Click
        AutoSwifts()
    End Sub

    Private Sub IntermediaryCodesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IntermediaryCodesToolStripMenuItem.Click
        Intermediary()
    End Sub

    Private Sub SystemSettingsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SystemSettingsToolStripMenuItem.Click
        SystemSettings()
    End Sub

    Private Sub ReceiveDeliverFreeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReceiveDeliverFreeToolStripMenuItem.Click
        ReceiveDeliver()
    End Sub

    Private Sub ToolStripButton11_Click(sender As Object, e As EventArgs) Handles ToolStripButton11.Click
        ReceiveDeliver()
    End Sub

    Private Sub AutomaticSwiftsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AutomaticSwiftsToolStripMenuItem.Click
        AutoSwifts()
    End Sub

    Private Sub ToolStripButton12_Click(sender As Object, e As EventArgs) Handles ToolStripButton12.Click
        CorpAction()
    End Sub

    Private Sub ToolStripButton8_Click(sender As Object, e As EventArgs) Handles ToolStripButton8.Click
        ReadSwiftForm()
    End Sub

    Private Sub ToolStripButton13_Click(sender As Object, e As EventArgs) Handles ToolStripButton13.Click
        ConfirmReceipt()
    End Sub

    Private Sub ConfirmExternalReceiptsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfirmExternalReceiptsToolStripMenuItem.Click
        ConfirmReceipt()
    End Sub

    Private Sub FeesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FeesToolStripMenuItem.Click
        Fees()
    End Sub

    Private Sub CutOffToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CutOffToolStripMenuItem.Click
        CutOff()
    End Sub

    Private Sub TransferFeesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TransferFeesToolStripMenuItem1.Click
        TransferFees()
    End Sub

    Private Sub TemplatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TemplatesToolStripMenuItem.Click
        Templates()
    End Sub

    Private Sub SwiftCodesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SwiftCodesToolStripMenuItem1.Click
        Swiftcodes()
    End Sub

    Private Sub IntermediaryCodesToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles IntermediaryCodesToolStripMenuItem1.Click
        Intermediary()
    End Sub

    Private Sub FeesToolStripMenuItem1_Click(sender As Object, e As EventArgs)
        Fees()
    End Sub

    Private Sub ToolStripButton10_Click(sender As Object, e As EventArgs) Handles ToolStripButton10.Click
        Intermediary()
    End Sub

    Private Sub ToolStripButton14_Click(sender As Object, e As EventArgs) Handles ToolStripButton14.Click
        Fees()
    End Sub

    Private Sub ViewBalancesToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ViewBalancesToolStripMenuItem2.Click
        Balances()
    End Sub

    Private Sub ReadSwiftToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ReadSwiftToolStripMenuItem1.Click
        ReadSwiftForm()
    End Sub

    Private Sub ConfirmExternalReceiptsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ConfirmExternalReceiptsToolStripMenuItem1.Click
        ConfirmReceipt()
    End Sub

    Private Sub InternalOmnibusSwitchTermDepositsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InternalOmnibusSwitchTermDepositsToolStripMenuItem.Click
        TreasuryTermDeposits
    End Sub

    Private Sub ToolStripButton6_Click(sender As Object, e As EventArgs) Handles ToolStripButton6.Click
        TreasuryTermDeposits()
    End Sub

    Private Sub FeesToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles FeesToolStripMenuItem2.Click
        Fees()
    End Sub

    Private Sub CorporateActionsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CorporateActionsToolStripMenuItem.Click
        CorpAction
    End Sub

    Private Sub ToolStripButton15_Click(sender As Object, e As EventArgs) Handles ToolStripButton15.Click
        RiskAssessment
    End Sub

    Private Sub RiskAssessmentToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RiskAssessmentToolStripMenuItem.Click
        RiskAssessment()
    End Sub

    Private Sub ToolStripButton16_Click(sender As Object, e As EventArgs) Handles ToolStripButton16.Click
        CashTransactionComments()
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs)
        PasswordGenerator()
    End Sub

    Private Sub CashTransactionCommentsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CashTransactionCommentsToolStripMenuItem.Click
        CashTransactionComments()
    End Sub

    Private Sub ToolStripButton17_Click(sender As Object, e As EventArgs) Handles ToolStripButton17.Click
        PasswordGenerator()
    End Sub

    Private Sub ToolStripButton18_Click(sender As Object, e As EventArgs) Handles ToolStripButton18.Click
        FeesPaid()
    End Sub

    Private Sub FeesPaidToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FeesPaidToolStripMenuItem.Click
        FeesPaid()
    End Sub

    Private Sub TreasuryManagementToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TreasuryManagementToolStripMenuItem.Click
        TreasuryManagement()
    End Sub

    Private Sub ToolStripButton19_Click(sender As Object, e As EventArgs) Handles ToolStripButton19.Click
        TreasuryManagement()
    End Sub

    Private Sub IMSReconciliationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IMSReconciliationToolStripMenuItem.Click
        Rec()
    End Sub

    Private Sub ToolStripButton20_Click(sender As Object, e As EventArgs) Handles ToolStripButton20.Click
        Rec()
    End Sub

    Private Sub SwiftAccountStatementsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SwiftAccountStatementsToolStripMenuItem.Click
        SwiftAccountStatement
    End Sub

    Private Sub ToolStripButton21_Click(sender As Object, e As EventArgs) Handles ToolStripButton21.Click
        SwiftAccountStatement()
    End Sub

    Private Sub BatchProcessingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatchProcessingToolStripMenuItem.Click
        BatchProcessing()
    End Sub

    Private Sub ToolStripButton22_Click(sender As Object, e As EventArgs) Handles ToolStripButton22.Click
        BatchProcessing()
    End Sub

    Private Sub ToolStripButton23_Click(sender As Object, e As EventArgs) Handles ToolStripButton23.Click
        ComplianceWatchList
    End Sub

    Private Sub ComplianceWatchlistToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ComplianceWatchlistToolStripMenuItem.Click
        ComplianceWatchList()
    End Sub

    Private Sub ReleaseIMSDocNumbersToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReleaseIMSDocNumbersToolStripMenuItem.Click
        ReleaseDocNumbers()
    End Sub

    Private Sub RunClientReportsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunClientReportsToolStripMenuItem.Click
        SendClientReports()
    End Sub

    Private Sub CASSAccountsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CASSAccountsToolStripMenuItem.Click
        CASSAccounts()
    End Sub

    Private Sub ToolStripButton24_Click(sender As Object, e As EventArgs) Handles ToolStripButton24.Click
        CASSAccounts()
    End Sub

    Private Sub ClientEmailAddressesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles  ClientEmailsToolStripMenuItem.Click
        ClientEmailAddresses()
    End Sub

    Private Sub ClientStatementGeneratorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClientStatementGeneratorToolStripMenuItem.Click
        ClientStatementReportGenerator()
    End Sub

    Private Sub BritanniaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BritanniaToolStripMenuItem.Click
        BritanniaGlobalMarketsRec()
    End Sub

    Private Sub ToolStripButton26_Click(sender As Object, e As EventArgs) Handles ToolStripButton26.Click
        BritanniaGlobalMarketsRec()
    End Sub

    Private Sub ToolStripButton27_Click(sender As Object, e As EventArgs) Handles ToolStripButton27.Click
        ClientStatementReportGenerator()
    End Sub

    Private Sub ToolStripButton25_Click(sender As Object, e As EventArgs) Handles ToolStripButton25.Click
        ClientEmailAddresses()
    End Sub

    Private Sub PasswordGeneratorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PasswordGeneratorToolStripMenuItem.Click
        PasswordGenerator()
    End Sub

    Private Sub GPPMappingCodesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GPPMappingCodesToolStripMenuItem.Click
        GPPMappingCodes()
    End Sub

    Private Sub ToolStripButton28_Click(sender As Object, e As EventArgs) Handles ToolStripButton28.Click
        GPPMappingCodes()
    End Sub
End Class
