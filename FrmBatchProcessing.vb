﻿Imports Microsoft.Office

Public Class FrmBatchProcessing
    Private Const cDefaultFilePath As String = "\\EgnyteDrive\DolfinGroup\Shared\Dolfin\Business Development\Sales\Clients\Private\"
    Private Const cPaymentSSIName As String = "PaymentSSI"
    Dim BatchConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dsNew As New DataSet, dsBS As New DataSet, dsBN As New DataSet, dsSBS As New DataSet, dsSBN As New DataSet
    Dim cboColumn As New DataGridViewComboBoxColumn
    Dim txtTextBox As New DataGridViewTextBoxColumn
    Dim CmdTemplate As DataGridViewButtonColumn
    Dim Chkbox As New DataGridViewCheckBoxColumn, ChkboxSelect As New DataGridViewCheckBoxColumn
    Dim TmpValue As Integer
    Dim TmpText As String = ""
    Dim SelectAll As Boolean = False

    Private Function ValidPayments() As Boolean
        Dim varBankName As String = ""
        Dim varResponse As Integer
        ValidPayments = True

        If CboBPType.SelectedValue = 0 Then
            For i As Integer = 0 To dgvbp.Rows.Count - 1
                If Not IsDGVRowEmpty(i, dgvbp) Then
                    If IIf(IsDBNull(dgvbp.Rows(i).Cells("Payment Ref").Value), "", dgvbp.Rows(i).Cells("Payment Ref").Value) = "" Then
                        MsgBox("Row " & i + 1 & " must have a payment reference")
                        ValidPayments = False
                    ElseIf Not IsNumeric(dgvbp.Rows(i).Cells("Amount").Value) Then
                        MsgBox("Row " & i + 1 & " must have an amount")
                        ValidPayments = False
                    ElseIf Not IsNumeric(dgvbp.Rows(i).Cells("Relationship").Value) Then
                        MsgBox("Row " & i + 1 & " must have a relationship selected")
                        ValidPayments = False
                    ElseIf Not IsNumeric(dgvbp.Rows(i).Cells("Type").Value) Then
                        MsgBox("Row " & i + 1 & " must have a type selected")
                        ValidPayments = False
                    ElseIf IIf(IsDBNull(dgvbp.Rows(i).Cells("Business Name").Value), "", dgvbp.Rows(i).Cells("Business Name").Value) = "" And IIf(IsDBNull(dgvbp.Rows(i).Cells("Surname").Value), "", dgvbp.Rows(i).Cells("Surname").Value) = "" Then
                        MsgBox("Row " & i + 1 & " must have an business name or surname")
                        ValidPayments = False
                    ElseIf Not IsNumeric(dgvbp.Rows(i).Cells("Country").Value) Then
                        MsgBox("Row " & i + 1 & " must have a country selected for the beneficiary")
                        ValidPayments = False
                    ElseIf IIf(IsDBNull(dgvbp.Rows(i).Cells("Bank Swift").Value), "", dgvbp.Rows(i).Cells("Bank Swift").Value) = "" Then
                        MsgBox("Row " & i + 1 & " must have a bank swift/Sort Code")
                        ValidPayments = False
                    ElseIf IsDBNull(dgvbp.Rows(i).Cells("Bank IBAN").Value) Then
                        MsgBox("Row " & i + 1 & " must have a bank IBAN")
                        ValidPayments = False
                    ElseIf Not IsNumeric(dgvbp.Rows(i).Cells("Bank Country").Value) Then
                        MsgBox("Row " & i + 1 & " must have a bank country selected for the beneficiary")
                        ValidPayments = False
                    ElseIf String.IsNullOrEmpty(dgvbp.Rows(i).Cells("Bank Name").Value) Then
                        MsgBox("Row " & i + 1 & " must have a bank name selected", vbCritical)
                        ValidPayments = False
                    End If

                    If ValidPayments And Not ValidSwift(dgvbp.Rows(i).Cells("Bank Swift").Value) Then
                        If Len(Replace(dgvbp.Rows(i).Cells("Bank Swift").Value, "-", "")) <> 6 Then
                            MsgBox("Row " & i + 1 & " does not seem to have a have a valid bank swift or sort code. Do you want to continue the request?", vbQuestion + vbYesNo)
                            If varResponse = vbNo Then
                                ValidPayments = False
                            End If
                        End If
                    End If

                    If ValidPayments And Not ValidIBAN(dgvbp.Rows(i).Cells("Bank IBAN").Value) Then
                        If Len(dgvbp.Rows(i).Cells("Bank IBAN").Value) <> 8 Then
                            MsgBox("Row " & i + 1 & " does not seem to have a have a valid bank/subbank IBAN. Do you want to continue the request?", vbQuestion + vbYesNo)
                            If varResponse = vbNo Then
                                ValidPayments = False
                            End If
                        End If
                    End If

                    If ValidPayments And cboBPPayType.SelectedValue = 2 Then
                        If Not ChkBPKYCsanctions.Checked Then
                            varResponse = MsgBox("The sanctions check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                            If varResponse = vbNo Then
                                ValidPayments = False
                            End If
                        End If
                        If Not ChkBPKYCPEP.Checked Then
                            varResponse = MsgBox("The PEP check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                            If varResponse = vbNo Then
                                ValidPayments = False
                            End If
                        End If
                        If Not ChkBPKYCCDD.Checked Then
                            varResponse = MsgBox("The CDD check is not ticked as being completed. Do you want to continue the request?", vbQuestion + vbYesNo)
                            If varResponse = vbNo Then
                                ValidPayments = False
                            End If
                        End If
                    End If
                End If
            Next
        ElseIf CboBPType.SelectedValue = 1 And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorisepaymentsteam) And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseCompliance) And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseComplianceLevel2) Then
            MsgBox("You do not have permission to authorise payments team payments", vbExclamation)
            ValidPayments = False
        ElseIf CboBPType.SelectedValue = 2 And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgt) And Not ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgtLevel2) Then
            MsgBox("You do not have permission to authorise account management payments", vbExclamation)
            ValidPayments = False
        ElseIf CboBPType.SelectedValue = 3 And Not (ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise3rdPartyPayments)) Then
            MsgBox("You do not have permission to authorise 3rd party management payments", vbExclamation)
            ValidPayments = False
        End If

    End Function

    Public Function IsDGVRowEmpty(ByRef varRow As Integer, ByRef dataGridView As DataGridView) As Boolean
        Dim isEmpty As Boolean
        isEmpty = False
        For i As Integer = 0 To dgvbp.Columns.Count - 1
            If String.IsNullOrEmpty(IIf(IsDBNull(dgvbp.Rows(varRow).Cells(i).Value), "", dgvbp.Rows(varRow).Cells(i).Value)) Then
                isEmpty = True
            ElseIf dgvbp.Columns(i).Name = "Add Fee" Then
                isEmpty = True
            Else
                isEmpty = False
                Exit For
            End If
        Next
        Return isEmpty
    End Function

    Private Sub clearColumns()
        dgvbp.Columns.Clear()
    End Sub

    Private Sub ClearForm()
        cboBPPayType.Text = ""
        CboBPPortfolio.Text = ""
        CboBPCCY.Text = ""
        dtBPTransDate.Value = Now
        dtBPSettleDate.Value = Now
        CboBPOrderAccount.Text = ""
        txtBPOrderSwift.Text = ""
        txtBPOrderIBAN.Text = ""
        cboBPOrderVOCode.Text = ""
        txtBPOrderBrokerName.Text = ""
        txtBPOrderBalance.Text = ""
        ChkBPKYCsanctions.Checked = False
        ChkBPKYCPEP.Checked = False
        ChkBPKYCCDD.Checked = False
        txtBPKYCIssuesDisclosed.Text = ""
        cboBPFeeCCY.Text = ""
        txtBPAmountFee.Text = ""
        cboBPFeeType.Text = ""
        txtBPOrderBalance.Text = ""
    End Sub

    Private Sub resetForm()
        ClsPay = New ClsPayment
        ClsPayAuth = New ClsPayment
        txtBPFilePath.Text = cDefaultFilePath
        BPFolderBrowserDialog1.SelectedPath = cDefaultFilePath
        ClsPayAuth.GetTempDirectory(ClsIMS.GetFilePath("Default"))
        ExtractAssociatedIconEx()
        ExtractAssociatedIconEx1()
    End Sub

    Private Sub clearRow(ByVal varRow As Integer)
        For i As Integer = 0 To dgvbp.Columns.Count - 1
            If dgvbp.Rows(varRow).Cells(i).OwningColumn.Name = "Add Fee" Then
                dgvbp.Rows(varRow).Cells(i).Value = True
            Else
                dgvbp.Rows(varRow).Cells(i).Value = Nothing
            End If
        Next
    End Sub

    Private Sub LoadBatchPaymentGrid(ByVal varBPType As Integer)
        Dim varSQL As String = ""
        Try
            dgvbp.DataSource = Nothing
            clearColumns()
            BatchConn = ClsIMS.GetNewOpenConnection
            If varBPType = 0 Then
                dsNew = New DataSet
                Dim CmdLoad As New DataGridViewButtonColumn()
                CmdLoad.HeaderText = "Load Template"
                CmdLoad.Text = "Load Template"
                CmdLoad.Name = "CmdTemplate"
                'CmdLoad.UseColumnTextForButtonValue = True
                dgvbp.Columns.Insert(0, CmdLoad)
                dgvbp.Refresh()

                Dim CmdCopyPrev As New DataGridViewButtonColumn()
                CmdCopyPrev.HeaderText = "Copy Previous Row"
                CmdCopyPrev.Text = "Copy Previous Row"
                CmdCopyPrev.Name = "CmdCopyPrev"
                'CmdCopyPrev.UseColumnTextForButtonValue = True
                dgvbp.Columns.Insert(0, CmdCopyPrev)
                dgvbp.Refresh()

                Dim CmdClearRow As New DataGridViewButtonColumn()
                CmdClearRow.HeaderText = "Clear Row"
                CmdClearRow.Text = "Clear Row"
                CmdClearRow.Name = "CmdTemplate"
                'CmdClearRow.UseColumnTextForButtonValue = True
                dgvbp.Columns.Insert(0, CmdClearRow)

                dgvbp.Columns(0).Width = 120
                dgvbp.Columns(1).Width = 120
                dgvbp.Columns(2).Width = 120

                dgvbp.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                dgvbp.Refresh()
                AddTxtColumnToDGV("Payment Ref", 120, 3)
                AddTxtColumnToDGV("Amount", 80, 4)
                dsNew = New DataSet
                AddCboColumnToDGV(dsNew, "select pty_id, pty_description from DolfinPaymentRARangePayType group by pty_id, pty_description", "Relationship", 150, 5)
                dsNew = New DataSet
                AddCboColumnToDGV(dsNew, "Select BeneficiaryTypeID, BeneficiaryType from vwDolfinPaymentBeneficiaryType order by BeneficiaryType", "Type", 80, 6)
                AddTxtColumnToDGV("Business Name", 150, 7)
                AddTxtColumnToDGV("First Name", 150, 8)
                AddTxtColumnToDGV("Middle Name", 150, 9)
                AddTxtColumnToDGV("Surname", 150, 10)
                dsNew = New DataSet
                AddCboColumnToDGV(dsNew, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name", "Country", 150, 11)
                dsBN = New DataSet
                AddCboColumnToDGV(dsBN, "Select SC_BankName,SC_BankName as BankName from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_BankName order by SC_BankName", "Bank Name", 300, 12)
                dsBS = New DataSet
                AddCboColumnToDGV(dsBS, "Select SC_SwiftCode as ID,SC_SwiftCode from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName union select Pa_Swift as ID,Pa_Swift from vwDolfinPaymentTemplates where isnull(Pa_Swift,'')<>'' group by Pa_Swift order by ID", "Bank Swift", 120, 13)
                AddTxtColumnToDGV("Bank IBAN", 200, 14)
                dsNew = New DataSet
                AddCboColumnToDGV(dsNew, "Select Ctry_ID, Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name", "Bank Country", 150, 15)
                dsNew = New DataSet
                'AddCboColumnToDGV(dsNew, "Select VO_ID, VO_Code from vwDolfinPaymentVOCodes order by VO_Code", "VO_ID", 80, 16)
                AddTxtColumnToDGV("Bank BIK", 150, 16)
                AddTxtColumnToDGV("Bank INN", 100, 17)
                dsSBN = New DataSet
                AddCboColumnToDGV(dsSBN, "Select SC_BankName,SC_BankName as BankName from vwDolfinPaymentSwiftCodes SC_Source not in ('UPLOAD') and where isnull(SC_BankName,'')<>'' group by SC_BankName order by SC_BankName", "Sub Bank Name", 300, 18)
                dsSBS = New DataSet
                AddCboColumnToDGV(dsSBS, "Select SC_SwiftCode as ID,SC_SwiftCode from vwDolfinPaymentSwiftCodes SC_Source not in ('UPLOAD') and where isnull(SC_BankName,'')<>'' group by SC_SwiftCode,SC_BankName union select Pa_Swift as ID,Pa_Swift from vwDolfinPaymentTemplates where isnull(Pa_Swift,'')<>'' group by Pa_Swift order by ID", "Sub Bank Swift", 120, 19)
                AddTxtColumnToDGV("Sub Bank IBAN", 200, 20)
                dsNew = New DataSet
                AddCboColumnToDGV(dsNew, "Select Ctry_ID,Ctry_Name from DolfinPaymentRARangeCountry order by Ctry_Name", "Sub Bank Country", 150, 21)
                AddTxtColumnToDGV("Sub Bank BIK", 150, 22)
                AddTxtColumnToDGV("Sub Bank INN", 100, 23)
                AddChkColumnToDGV("Add Fee", 60, 24)
                AddTxtColumnToDGV("Comments", 500, 25)
            Else
                varSQL = "Select cast(0 as bit) as SelectPay,P_PaymentIDOther as BatchID,P_OrderRef,P_Status as Status,PR_RequestType,P_PaymentType,P_Portfolio,P_transdate,P_SettleDate,P_Amount,P_CCY,P_OrderAccount,P_OrderName,P_OrderSwift,P_OrderIBAN,
                                P_BenRelationship,P_BenType,P_BenName,P_BenFirstName,P_BenMiddleName,P_BenSurname,P_BenCountry,PR_CheckedBankName,P_BenSwift,P_BenIBAN,PR_CheckedBankCountry,P_Comments,0 as [Add Fee], P_ID as ID
                                from vwDolfinPaymentGridView where P_StatusID in (" & IIf(varBPType = 1, "29", IIf(varBPType = 2, "33", "2")) & ") And PaymentRequested = 1 And isnull(P_PaymentIDOther,'')<>'' 
                                order by p_portfolio, p_ccy, p_OrderAccount"

                dgvbp.DataSource = Nothing
                dgvdata = New SqlClient.SqlDataAdapter(varSQL, BatchConn)
                dsNew = New DataSet
                dgvdata.Fill(dsNew, "vwDolfinPaymentGridViewLitePaymentRequestAll")
                dgvbp.AutoGenerateColumns = True
                dgvbp.DataSource = dsNew.Tables("vwDolfinPaymentGridViewLitePaymentRequestAll")
                dgvbp.Columns("SelectPay").Width = 60
                dgvbp.Columns("Add Fee").Visible = False
                dgvbp.Columns("ID").Visible = False
            End If

            For Each column In dgvbp.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
            Next
            dgvbp.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvbp.Refresh()
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvbp.DataSource = Nothing

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem With viewing batch processing fees grids (LoadBatchPaymentGrid)")
        End Try
    End Sub

    Private Sub GridReadOnly()
        For Each column In dgvbp.Columns
            If column.name <> "SelectPay" Then
                column.readonly = True
            End If
        Next
    End Sub

    Private Sub AddTxtColumnToDGV(ByVal varName As String, ByVal varWidth As Integer, ByVal varColumn As Integer)
        txtTextBox = New DataGridViewTextBoxColumn
        txtTextBox.Name = varName
        txtTextBox.Width = varWidth
        dgvbp.Columns.Insert(varColumn, txtTextBox)
    End Sub

    Private Sub AddChkColumnToDGV(ByVal varName As String, ByVal varWidth As Integer, ByVal varColumn As Integer)
        Chkbox = New DataGridViewCheckBoxColumn
        Chkbox.Name = varName
        Chkbox.Width = varWidth
        dgvbp.Columns.Insert(varColumn, Chkbox)
    End Sub


    Private Sub AddCboColumnToDGV(ByRef ds As DataSet, ByVal varSQL As String, ByVal varName As String, ByVal varWidth As Integer, ByVal varColumn As Integer)
        dgvdata = New SqlClient.SqlDataAdapter(varSQL, BatchConn)
        dgvdata.Fill(ds, varName)
        ds.Tables(varName).PrimaryKey = New DataColumn() {ds.Tables(varName).Columns(0)}
        cboColumn = New DataGridViewComboBoxColumn
        cboColumn.HeaderText = varName
        cboColumn.DataSource = ds.Tables(varName)
        cboColumn.ValueMember = ds.Tables(varName).Columns(0).ColumnName
        cboColumn.DisplayMember = ds.Tables(varName).Columns(1).ColumnName
        cboColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        cboColumn.DataPropertyName = varName
        cboColumn.FlatStyle = FlatStyle.Flat

        dgvbp.Columns.Insert(varColumn, cboColumn)
        dgvbp.Columns(varColumn).Name = varName
        dgvbp.Columns(varName).Width = varWidth
    End Sub

    Private Sub LoadGrid()
        If CboBPType.SelectedValue = 0 And IsNumeric(CboBPPortfolio.SelectedValue) And IsNumeric(CboBPCCY.SelectedValue) And IsNumeric(CboBPOrderAccount.SelectedValue) Then
            LoadBatchPaymentGrid(CboBPType.SelectedValue)
            cmdSave.Text = "Save Payments"
            GrpBenDetails.Text = "Beneficiary Details"
        ElseIf CboBPType.SelectedValue <> 0 Then
            LoadBatchPaymentGrid(CboBPType.SelectedValue)
            cmdSave.Text = "Authorise Payments"
            GrpBenDetails.Text = "Beneficiary Details (Select top left of grid to select all rows)"
        End If
    End Sub

    Private Sub FrmBatchProcessing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateComboboxWithData(CboBPType, "Select 0,'Enter Batch Payments' union Select 1,'Authorise Batch Payments Team' union Select 2,'Authorise Batch Account Mgt' union Select 3,'Authorise Batch 3rd Party'")
        ClsIMS.PopulateComboboxWithData(cboBPFeeType, "Select 0,'Std Fee' union Select 1,'Rate'")

        ModRMS.DoubleBuffered(dgvbp, True)
    End Sub

    Private Sub CboBPPortfolio_Leave(sender As Object, e As EventArgs) Handles CboBPPortfolio.Leave
        If IsNumeric(CboBPPortfolio.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(CboBPCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & CboBPPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            ClsIMS.PopulateComboboxWithData(cboBPFeeCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & CboBPPortfolio.SelectedValue & " group by CR_Code,CR_Name1 order by CR_Name1")
            CboBPOrderAccount.DataSource = Nothing
        End If
    End Sub

    Private Sub CboBPCCY_Leave(sender As Object, e As EventArgs) Handles CboBPCCY.Leave
        PopulateRUBForm()
        If IsNumeric(CboBPCCY.SelectedValue) Then
            ClsIMS.PopulateComboboxWithData(CboBPOrderAccount, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and PF_Code = " & CboBPPortfolio.SelectedValue & " and cr_code = " & CboBPCCY.SelectedValue & " group by t_Code,t_name1 order by t_Name1")
        End If
    End Sub

    Private Sub PopulateRUBForm()
        If CboBPCCY.Text = "RUB" Then
            lblBPOrderVOCode.Visible = True
            cboBPOrderVOCode.Visible = True
            cboBPOrderVOCode.DataSource = Nothing
            cboBPOrderVOCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboBPOrderVOCode.AutoCompleteSource = AutoCompleteSource.ListItems
        Else
            lblBPOrderVOCode.Visible = False
            cboBPOrderVOCode.Visible = False
            ClsIMS.PopulateFormCombo(cboBPOrderVOCode)
            cboBPOrderVOCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboBPOrderVOCode.AutoCompleteSource = AutoCompleteSource.ListItems
        End If
    End Sub

    Private Sub dgvbp_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dgvbp.EditingControlShowing
        If TypeOf e.Control Is DataGridViewComboBoxEditingControl Then
            CType(e.Control, ComboBox).DropDownStyle = ComboBoxStyle.DropDown
            'CType(e.Control, ComboBox).AutoCompleteSource = AutoCompleteSource.ListItems
            'CType(e.Control, ComboBox).AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        End If
    End Sub

    Private Sub dgvbp_CellValidated(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.CellValidated
        UpdateBankDetailsOnGrid(dgvbp, e.RowIndex, e.ColumnIndex)
    End Sub

    Public Sub ChangeType(ByVal varRow As Integer, ByVal varCol As Integer)
        If Not IsDBNull(dgvbp.Rows(varRow).Cells(varCol).EditedFormattedValue) Then
            TmpText = dgvbp.Rows(varRow).Cells(varCol).EditedFormattedValue
        End If

        If CboBPType.SelectedValue = 0 Then
            If IsNumeric(CboBPCCY.SelectedValue) Then
                PopulateRUBGrid(varRow)
            End If

            If Not IsNumeric(dgvbp.Rows(varRow).Cells("Type").Value) Then
                dgvbp.Rows(varRow).Cells("Business Name").Value = DBNull.Value
                dgvbp.Rows(varRow).Cells("Business Name").ReadOnly = False
                dgvbp.Rows(varRow).Cells("Business Name").Style.BackColor = Color.White
                dgvbp.Rows(varRow).Cells("First Name").Value = DBNull.Value
                dgvbp.Rows(varRow).Cells("First Name").ReadOnly = False
                dgvbp.Rows(varRow).Cells("First Name").Style.BackColor = Color.White
                dgvbp.Rows(varRow).Cells("Middle Name").Value = DBNull.Value
                dgvbp.Rows(varRow).Cells("Middle Name").ReadOnly = False
                dgvbp.Rows(varRow).Cells("Middle Name").Style.BackColor = Color.White
                dgvbp.Rows(varRow).Cells("Surname").Value = DBNull.Value
                dgvbp.Rows(varRow).Cells("Surname").ReadOnly = False
                dgvbp.Rows(varRow).Cells("Surname").Style.BackColor = Color.White
            Else
                If dgvbp.Rows(varRow).Cells("Type").Value = 1 Then
                    dgvbp.Rows(varRow).Cells("Business Name").ReadOnly = False
                    dgvbp.Rows(varRow).Cells("Business Name").Style.BackColor = Color.White
                    dgvbp.Rows(varRow).Cells("First Name").Value = DBNull.Value
                    dgvbp.Rows(varRow).Cells("First Name").ReadOnly = True
                    dgvbp.Rows(varRow).Cells("First Name").Style.BackColor = Color.LightGray
                    dgvbp.Rows(varRow).Cells("Middle Name").Value = DBNull.Value
                    dgvbp.Rows(varRow).Cells("Middle Name").ReadOnly = True
                    dgvbp.Rows(varRow).Cells("Middle Name").Style.BackColor = Color.LightGray
                    dgvbp.Rows(varRow).Cells("Surname").Value = DBNull.Value
                    dgvbp.Rows(varRow).Cells("Surname").ReadOnly = True
                    dgvbp.Rows(varRow).Cells("Surname").Style.BackColor = Color.LightGray
                ElseIf dgvbp.Rows(varRow).Cells("Type").Value = 0 Then
                    dgvbp.Rows(varRow).Cells("Business Name").Value = DBNull.Value
                    dgvbp.Rows(varRow).Cells("Business Name").ReadOnly = True
                    dgvbp.Rows(varRow).Cells("Business Name").Style.BackColor = Color.LightGray
                    dgvbp.Rows(varRow).Cells("First Name").ReadOnly = False
                    dgvbp.Rows(varRow).Cells("First Name").Style.BackColor = Color.White
                    dgvbp.Rows(varRow).Cells("Middle Name").ReadOnly = False
                    dgvbp.Rows(varRow).Cells("Middle Name").Style.BackColor = Color.White
                    dgvbp.Rows(varRow).Cells("Surname").ReadOnly = False
                    dgvbp.Rows(varRow).Cells("Surname").Style.BackColor = Color.White
                End If
            End If
        End If
    End Sub

    Private Sub PopulateRUBGrid(ByVal varRow As Integer)
        If CboBPCCY.Text = "RUB" Then
            dgvbp.Rows(varRow).Cells("Bank BIK").ReadOnly = False
            dgvbp.Rows(varRow).Cells("Bank BIK").Style.BackColor = Color.White
            dgvbp.Rows(varRow).Cells("Bank INN").ReadOnly = False
            dgvbp.Rows(varRow).Cells("Bank INN").Style.BackColor = Color.White
            dgvbp.Rows(varRow).Cells("Sub Bank BIK").ReadOnly = False
            dgvbp.Rows(varRow).Cells("Sub Bank BIK").Style.BackColor = Color.White
            dgvbp.Rows(varRow).Cells("Sub Bank INN").ReadOnly = False
            dgvbp.Rows(varRow).Cells("Sub Bank INN").Style.BackColor = Color.White
        Else
            dgvbp.Rows(varRow).Cells("Bank BIK").ReadOnly = True
            dgvbp.Rows(varRow).Cells("Bank BIK").Style.BackColor = Color.LightGray
            dgvbp.Rows(varRow).Cells("Bank INN").ReadOnly = True
            dgvbp.Rows(varRow).Cells("Bank INN").Style.BackColor = Color.LightGray
            dgvbp.Rows(varRow).Cells("Sub Bank BIK").ReadOnly = True
            dgvbp.Rows(varRow).Cells("Sub Bank BIK").Style.BackColor = Color.LightGray
            dgvbp.Rows(varRow).Cells("Sub Bank INN").ReadOnly = True
            dgvbp.Rows(varRow).Cells("Sub Bank INN").Style.BackColor = Color.LightGray
        End If
    End Sub

    Private Sub PopulateBatchPaymentTable(ByRef TblBP As DataTable)
        'On Error Resume Next

        PopulateBatchTable(TblBP)
        Dim bprow As DataRow

        If CboBPType.SelectedValue = 0 Then
            For varRow = 0 To dgvbp.RowCount - 1
                If Not IsDGVRowEmpty(varRow, dgvbp) Then
                    bprow = TblBP.NewRow()
                    bprow("P_ID") = varRow
                    bprow("P_PaymentType") = IIf(cboBPPayType.SelectedValue = -2, "3rd Party Payment", IIf(cboBPPayType.SelectedValue = -1, "Payment Services Request", cboBPPayType.Text))
                    bprow("P_PortfolioCode") = CboBPPortfolio.SelectedValue
                    bprow("P_Portfolio") = CboBPPortfolio.Text
                    bprow("P_TransDate") = dtBPTransDate.Value
                    bprow("P_SettleDate") = dtBPSettleDate.Value
                    bprow("P_Amount") = dgvbp.Rows(varRow).Cells("Amount").Value
                    bprow("P_CCYCode") = CboBPCCY.SelectedValue
                    bprow("P_CCY") = CboBPCCY.Text
                    bprow("P_OrderBrokerCode") = txtBPOrderBrokerCode.Text
                    bprow("P_OrderAccountCode") = CboBPOrderAccount.SelectedValue
                    bprow("P_OrderAccount") = CboBPOrderAccount.Text
                    bprow("P_OrderName") = txtBPOrderBrokerName.Text
                    bprow("P_OrderSwift") = txtBPOrderSwift.Text
                    bprow("P_OrderIBAN") = txtBPOrderIBAN.Text
                    bprow("P_OrderVO_ID") = IIf(IsNumeric(cboBPOrderVOCode.SelectedValue), cboBPOrderVOCode.SelectedValue, 0)
                    bprow("P_OrderOther") = dgvbp.Rows(varRow).Cells("Payment Ref").Value
                    bprow("P_BenTypeID") = IIf(IsNumeric(dgvbp.Rows(varRow).Cells("Type").Value), dgvbp.Rows(varRow).Cells("Type").Value, 0)
                    bprow("P_BenType") = dgvbp.Rows(varRow).Cells("Type").FormattedValue
                    bprow("P_BenName") = dgvbp.Rows(varRow).Cells("Business Name").Value
                    bprow("P_BenFirstname") = dgvbp.Rows(varRow).Cells("First Name").Value
                    bprow("P_BenMiddlename") = dgvbp.Rows(varRow).Cells("Middle Name").Value
                    bprow("P_BenSurname") = dgvbp.Rows(varRow).Cells("Surname").Value
                    bprow("P_BenCountryID") = IIf(IsNumeric(dgvbp.Rows(varRow).Cells("Country").Value), dgvbp.Rows(varRow).Cells("Country").Value, 0)
                    bprow("P_BenCountry") = dgvbp.Rows(varRow).Cells("Country").FormattedValue
                    bprow("P_BenSwift") = dgvbp.Rows(varRow).Cells("Bank Swift").Value
                    bprow("P_BenIBAN") = dgvbp.Rows(varRow).Cells("Bank IBAN").Value
                    bprow("P_BenBIK") = dgvbp.Rows(varRow).Cells("Bank BIK").Value
                    bprow("P_BenINN") = dgvbp.Rows(varRow).Cells("Bank INN").Value
                    bprow("P_BenRelationshipID") = IIf(IsNumeric(dgvbp.Rows(varRow).Cells("Relationship").Value), dgvbp.Rows(varRow).Cells("Relationship").Value, 0)
                    bprow("P_BenRelationship") = dgvbp.Rows(varRow).Cells("Relationship").FormattedValue
                    bprow("P_BenSubBankName") = dgvbp.Rows(varRow).Cells("Sub Bank Name").Value
                    bprow("P_BenSubBankCountryID") = IIf(IsNumeric(dgvbp.Rows(varRow).Cells("Sub Bank Country").Value), dgvbp.Rows(varRow).Cells("Sub Bank Country").Value, 0)
                    bprow("P_BenSubBankCountry") = dgvbp.Rows(varRow).Cells("Sub Bank Country").FormattedValue
                    bprow("P_BenSubBankSwift") = dgvbp.Rows(varRow).Cells("Sub Bank Swift").Value
                    bprow("P_BenSubBankIBAN") = dgvbp.Rows(varRow).Cells("Sub Bank IBAN").Value
                    bprow("P_BenSubBankBIK") = dgvbp.Rows(varRow).Cells("Sub Bank BIK").Value
                    bprow("P_BenSubBankINN") = dgvbp.Rows(varRow).Cells("Sub Bank INN").Value
                    bprow("P_RequestTypeID") = IIf(cboBPPayType.SelectedValue = -2, 2, IIf(cboBPPayType.SelectedValue = -1, 6, cboBPPayType.SelectedValue))
                    bprow("P_RequestType") = IIf(cboBPPayType.SelectedValue = -2, "3rd Party Payment", IIf(cboBPPayType.SelectedValue = -1, "Payment Services Request", cboBPPayType.Text))
                    bprow("P_RequestRate") = IIf(cboBPFeeType.SelectedValue = 0, DBNull.Value, IIf(IsNumeric(txtBPAmountFee.Text), txtBPAmountFee.Text, 0))
                    bprow("P_RequestFee") = IIf(cboBPFeeType.SelectedValue = 0, IIf(IsNumeric(txtBPAmountFee.Text), txtBPAmountFee.Text, 0), DBNull.Value)
                    bprow("P_RequestFeeCCY") = IIf(cboBPFeeType.SelectedValue = 0, cboBPFeeCCY.Text, DBNull.Value)
                    bprow("P_RequestKYCFolder") = txtBPFilePath.Text
                    bprow("P_RequestCheckedBankName") = dgvbp.Rows(varRow).Cells("Bank Name").Value
                    bprow("P_RequestCheckedBankCountryID") = IIf(IsNumeric(dgvbp.Rows(varRow).Cells("Bank Country").Value), dgvbp.Rows(varRow).Cells("Bank Country").Value, 0)
                    bprow("P_RequestCheckedBankCountry") = dgvbp.Rows(varRow).Cells("Bank Country").FormattedValue
                    bprow("P_RequestCheckedSanctions") = ChkBPKYCsanctions.Checked
                    bprow("P_RequestCheckedPEP") = ChkBPKYCPEP.Checked
                    bprow("P_RequestCheckedCDD") = ChkBPKYCCDD.Checked
                    bprow("P_RequestIssuesDisclosed") = txtBPKYCIssuesDisclosed.Text
                    bprow("P_AddFee") = dgvbp.Rows(varRow).Cells("Add Fee").Value
                    bprow("P_Comments") = dgvbp.Rows(varRow).Cells("Comments").Value
                    bprow("P_Username") = ClsIMS.FullUserName
                    TblBP.Rows.Add(bprow)
                End If
            Next
        Else
            For varRow = 0 To dgvbp.RowCount - 1
                If dgvbp.Rows(varRow).Cells("SelectPay").Value Then
                    bprow = TblBP.NewRow()
                    bprow("P_ID") = dgvbp.Rows(varRow).Cells("ID").Value
                    bprow("P_Username") = ClsIMS.FullUserName
                    TblBP.Rows.Add(bprow)
                End If
            Next
        End If

    End Sub

    Private Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim TblBP As New DataTable
        Dim varBatchID As Double
        Dim varResponse As Integer

        If ValidPayments() Then
            If CboBPType.SelectedValue = 0 Then
                PopulateBatchPaymentTable(TblBP)
                Dim TmpFilePath As String = ClsPayAuth.PaymentFilePath
                If ClsIMS.AddBatchPayments(CboBPType.SelectedValue, TblBP, varBatchID) Then
                    ClsPayAuth = New ClsPayment
                    TblBP(0)("P_ID") = varBatchID
                    ClsPayAuth.BatchPaymentTable = TblBP
                    ClsPayAuth.SelectedPaymentID = varBatchID
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut)
                    ClsPayAuth.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam)
                    ClsPayAuth.Portfolio = TblBP(0)("P_Portfolio")
                    ClsPayAuth.CCY = TblBP(0)("P_CCY")
                    ClsPayAuth.PaymentFilePath = TmpFilePath
                    ClsPay = ClsPayAuth
                    ClsPayAuth.ConfirmDirectory()

                    If cboBPPayType.SelectedValue = 2 Or cboBPPayType.SelectedValue = -2 Then
                        FrmEmail.ShowDialog()
                    End If
                    MsgBox("The batch payments have been booked", MsgBoxStyle.Information, "Payments booked")
                    resetForm()
                    ClearForm()
                    clearColumns()
                Else
                    MsgBox("There was a problem saving the batch payments. Please contact systems support For assistance", vbCritical, "No Save")
                End If
            Else
                varResponse = MsgBox("Are you sure you want to authorise these payments?", vbQuestion + vbYesNo, "Authorise Payments")
                If varResponse = vbYes Then
                    PopulateBatchPaymentTable(TblBP)
                    ClsIMS.AddBatchPayments(CboBPType.SelectedValue, TblBP, varBatchID)
                    LoadGrid()
                End If
            End If
        End If
    End Sub

    Private Sub dgvbp_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.CellEndEdit
        If CboBPType.SelectedValue = 0 And dgvbp.Columns.Contains("Type") Then
            Dim varColName As String = dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name

            If IsNumeric(dgvbp.Rows(e.RowIndex).Cells("Type").Value) And IsNumeric(dgvbp.Rows(e.RowIndex).Cells("Amount").Value) Then
                If cboBPPayType.SelectedValue = 2 Then '3rd party
                    dgvbp.Rows(e.RowIndex).Cells("Comments").Value = "Payment to " & IIf(dgvbp.Rows(e.RowIndex).Cells("Type").Value = 0, dgvbp.Rows(e.RowIndex).Cells("First name").Value & " " & dgvbp.Rows(e.RowIndex).Cells("Middle Name").Value & " " & dgvbp.Rows(e.RowIndex).Cells("Surname").Value, dgvbp.Rows(e.RowIndex).Cells("Business Name").Value) & " for " & IIf(IsNumeric(dgvbp.Rows(e.RowIndex).Cells("Amount").Value), dgvbp.Rows(e.RowIndex).Cells("Amount").Value, 0) & " " & CboBPCCY.Text
                ElseIf cboBPPayType.SelectedValue = 6 Then 'pay services
                    dgvbp.Rows(e.RowIndex).Cells("Comments").Value = "Payment to " & IIf(dgvbp.Rows(e.RowIndex).Cells("Type").Value = 0, dgvbp.Rows(e.RowIndex).Cells("First name").Value & " " & dgvbp.Rows(e.RowIndex).Cells("Middle Name").Value & " " & dgvbp.Rows(e.RowIndex).Cells("Surname").Value, dgvbp.Rows(e.RowIndex).Cells("Business Name").Value) & " for " & dgvbp.Rows(e.RowIndex).Cells("Amount").Value & " " & CboBPCCY.Text
                End If
            End If
            ChangeType(e.RowIndex, e.ColumnIndex)
        End If
    End Sub

    Public Sub CopyRow(ByVal varRow As Integer)
        For i As Integer = 1 To dgvbp.ColumnCount - 1
            dgvbp.Rows(varRow).Cells(i).Value = dgvbp.Rows(varRow - 1).Cells(i).Value
        Next
    End Sub

    Public Sub GetTemplateDetails(ByVal varRow As Integer, ByVal varTemplateNameId As Double)
        Dim Lst As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select top 1 Pa_BeneficiaryTypeID, Pa_BeneficiaryType, Pa_Name, Pa_Firstname, Pa_Middlename, Pa_Surname, Pa_Swift, Pa_IBAN, Pa_Address1, Pa_Address2, Pa_County, Pa_Zip, Pa_CountryID, Pa_Country, Pa_RelationshipID, Pa_Relationship from vwDolfinPaymentTemplates where Pa_Id = " & varTemplateNameId & " and Pa_TemplateSelected = 2 order by Pa_ID Desc")

        If Not Lst Is Nothing Then
            If IsNumeric(Lst.Item("Pa_RelationshipID")) Then
                dgvbp.Rows(varRow).Cells("Relationship").Value = CInt(Lst.Item("Pa_RelationshipID"))
            End If
            If IsNumeric(Lst.Item("Pa_BeneficiaryTypeID")) Then
                dgvbp.Rows(varRow).Cells("Type").Value = CInt(Lst.Item("Pa_BeneficiaryTypeID"))
                If CInt(Lst.Item("Pa_BeneficiaryTypeID")) = 0 Then
                    dgvbp.Rows(varRow).Cells("First Name").Value = Lst.Item("Pa_Firstname").ToString
                    dgvbp.Rows(varRow).Cells("Middle Name").Value = Lst.Item("Pa_Middlename").ToString
                    dgvbp.Rows(varRow).Cells("Surname").Value = Lst.Item("Pa_Surname").ToString
                Else
                    dgvbp.Rows(varRow).Cells("Business Name").Value = Lst.Item("Pa_Name").ToString
                End If
            End If
            If Lst.Item("Pa_Swift").ToString <> "" Then
                dgvbp.Rows(varRow).Cells("Bank Swift").Value = Lst.Item("Pa_Swift").ToString
            End If
            dgvbp.Rows(varRow).Cells("Bank IBAN").Value = Lst.Item("Pa_IBAN").ToString
            If IsNumeric(Lst.Item("Pa_CountryID")) Then
                dgvbp.Rows(varRow).Cells("Country").Value = CInt(Lst.Item("Pa_CountryID").ToString)
            End If
        End If

        Lst = ClsIMS.GetDataToList("select Pa_Name,Pa_Swift,Pa_IBAN,Pa_CountryID,Pa_Country from vwDolfinPaymentTemplates where Pa_Id = " & varTemplateNameId & " and Pa_TemplateSelected = 3")
        If Not Lst Is Nothing Then
            dgvbp.Rows(varRow).Cells("Sub Bank Name").Value = Lst.Item("Pa_Name").ToString
            If Lst.Item("Pa_Swift").ToString <> "" Then
                dgvbp.Rows(varRow).Cells("Sub Bank Swift").Value = Lst.Item("Pa_Swift").ToString
            End If
            dgvbp.Rows(varRow).Cells("Sub Bank IBAN").Value = Lst.Item("Pa_IBAN").ToString
            If IsNumeric(Lst.Item("Pa_CountryID")) Then
                dgvbp.Rows(varRow).Cells("Sub Bank Country").Value = CInt(Lst.Item("Pa_CountryID").ToString)
            End If
        End If

        Lst = ClsIMS.GetDataToList("select Pa_Name,Pa_Swift,Pa_IBAN,Pa_CountryID,Pa_Country from vwDolfinPaymentTemplates where Pa_Id = " & varTemplateNameId & " and Pa_TemplateSelected = 4")
        If Not Lst Is Nothing Then
            If Not dgvbp.Rows(varRow).Cells("Bank Name").Value Is Nothing Then
                If dgvbp.Rows(varRow).Cells("Bank Name").Value = "" Then
                    dgvbp.Rows(varRow).Cells("Bank Name").Value = Lst.Item("Pa_Name").ToString
                End If
            End If
            If Not dgvbp.Rows(varRow).Cells("Bank Swift").Value Is Nothing Then
                If dgvbp.Rows(varRow).Cells("Bank Swift").Value = "" And Lst.Item("Pa_Swift").ToString <> "" Then
                    dgvbp.Rows(varRow).Cells("Bank Swift").Value = Lst.Item("Pa_Swift").ToString
                End If
            End If
            If Not dgvbp.Rows(varRow).Cells("Bank IBAN").Value Is Nothing Then
                If dgvbp.Rows(varRow).Cells("Bank IBAN").Value = "" Then
                    dgvbp.Rows(varRow).Cells("Bank IBAN").Value = Lst.Item("Pa_IBAN").ToString
                End If
            End If
            If Not dgvbp.Rows(varRow).Cells("Country").Value Is Nothing Then
                If Not IsNumeric(dgvbp.Rows(varRow).Cells("Country").Value) And IsNumeric(Lst.Item("Pa_CountryID")) Then
                    dgvbp.Rows(varRow).Cells("Country").Value = CInt(Lst.Item("Pa_CountryID").ToString)
                End If
            End If
        End If
    End Sub

    Private Sub dgvbp_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.CellEnter
        dgvbp.BeginEdit(True)
    End Sub

    Private Sub CboBPOrderAccount_Leave(sender As Object, e As EventArgs) Handles CboBPOrderAccount.Leave
        If cboBPPayType.SelectedValue >= 0 Then
            LoadGrid()
        End If
        GetFee()
    End Sub

    Private Sub CboBPType_Leave(sender As Object, e As EventArgs) Handles CboBPType.Leave
        ClsIMS.PopulateComboboxWithData(cboBPPayType, "Select -2 as prt_Code,'<Load 3rd Party Template>' as prt_description union Select -1 as prt_Code,'<Load Payment Services Request Template>' as prt_description union Select prt_Code,prt_description from vwDolfinPaymentRequestType where prt_Code in (2,6) order by prt_description")
    End Sub

    Private Sub cboPayType_Leave(sender As Object, e As EventArgs) Handles cboBPPayType.Leave
        If CboBPType.SelectedValue = 0 Then
            If IsNumeric(cboBPPayType.SelectedValue) Then
                If cboBPPayType.SelectedValue = 2 Or cboBPPayType.SelectedValue = -2 Then '3rd party
                    ClsIMS.PopulateComboboxWithData(CboBPPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                    ChkBPKYCsanctions.Checked = False
                    ChkBPKYCPEP.Checked = False
                    ChkBPKYCCDD.Checked = False
                ElseIf cboBPPayType.SelectedValue = 6 Or cboBPPayType.SelectedValue = -1 Then 'payment services
                    ClsIMS.PopulateComboboxWithData(CboBPPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and b_shortcut1 in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
                    ChkBPKYCsanctions.Checked = True
                    ChkBPKYCPEP.Checked = True
                    ChkBPKYCCDD.Checked = True
                End If
            End If
        End If
    End Sub

    Private Sub CboBPOrderAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboBPOrderAccount.SelectedIndexChanged
        If IsNumeric(CboBPPortfolio.SelectedValue) And IsNumeric(CboBPCCY.SelectedValue) And IsNumeric(CboBPOrderAccount.SelectedValue) Then
            Dim lst As Dictionary(Of String, String) = ClsIMS.GetDataToList("Select b_code,b_name1,b_swiftaddress,t_iban from vwdolfinpaymentselectaccount where pf_code = " & CboBPPortfolio.SelectedValue & " and cr_code = " & CboBPCCY.SelectedValue & " and t_code = " & CboBPOrderAccount.SelectedValue)
            If Not lst Is Nothing Then
                txtBPOrderBrokerCode.Text = lst("b_code").ToString()
                txtBPOrderBrokerName.Text = lst("b_name1").ToString()
                txtBPOrderSwift.Text = lst("b_swiftaddress").ToString()
                txtBPOrderIBAN.Text = lst("t_iban").ToString()
                dgvbp.Select()
            End If
        End If
    End Sub

    Private Sub dgvbp_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvbp.DataError
        If e.Exception.Message = "DataGridViewCom boBoxCell value is not valid" Then
            e.ThrowException = False
        End If
    End Sub

    Private Sub CboBPType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboBPType.SelectedIndexChanged
        resetForm()
        ClearForm()
        clearColumns()
        TabBatch.Enabled = True
        cboBPPayType.Enabled = True
        dgvbp.ReadOnly = False
        If IsNumeric(CboBPType.SelectedValue) Then
            If CboBPType.SelectedValue <> 0 Then
                TabBatch.Enabled = False
                cboBPPayType.Enabled = False
                LoadGrid()
                GridReadOnly()
            End If
        End If
    End Sub

    Private Sub dgvbp_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.RowEnter
        If CboBPType.SelectedValue = 0 Then
            dgvbp.Rows(e.RowIndex).Cells("Add Fee").Value = True
        End If
    End Sub

    Public Sub ExtractAssociatedIconEx()
        Try
            LVFiles.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If IO.Directory.Exists(ClsPayAuth.PaymentFilePath) Then

                LVFiles.SmallImageList = ImageFileList
                LVFiles.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New IO.DirectoryInfo(ClsPayAuth.PaymentFilePath)

                Dim item As ListViewItem
                LVFiles.BeginUpdate()
                Dim file As IO.FileInfo
                For Each file In dir.GetFiles()
                    If Strings.InStr(1, file.Name, cPaymentSSIName, vbTextCompare) = 0 Then
                        ' Set a default icon for the file.
                        Dim iconForFile As Icon = SystemIcons.WinLogo
                        item = New ListViewItem(file.Name, 1)

                        ' Check to see if the image collection contains an image
                        ' for this extension, using the extension as a key.
                        If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                            ' If not, add the image to the image list.
                            'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                            ImageFileList.Images.Add(file.Extension, iconForFile)
                        End If
                        item.ImageKey = file.Extension
                        If Not (InStr(1, item.Text, "Search", vbTextCompare) <> 0 And InStr(1, item.Text, ".html", vbTextCompare) <> 0) Then
                            LVFiles.Items.Add(item)
                        End If
                    End If
                Next file
                LVFiles.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx - Payment Request")
        End Try
    End Sub

    Public Sub ExtractAssociatedIconEx1()
        Try
            LVFiles1.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If IO.Directory.Exists(ClsPayAuth.PaymentFilePath) Then

                LVFiles1.SmallImageList = ImageFileList
                LVFiles1.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New IO.DirectoryInfo(ClsPayAuth.PaymentFilePath)

                Dim item As ListViewItem
                LVFiles1.BeginUpdate()
                Dim file As IO.FileInfo
                For Each file In dir.GetFiles(cPaymentSSIName & "*.*")
                    ' Set a default icon for the file.
                    Dim iconForFile As Icon = SystemIcons.WinLogo
                    item = New ListViewItem(file.Name, 1)

                    ' Check to see if the image collection contains an image
                    ' for this extension, using the extension as a key.
                    If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                        ' If not, add the image to the image list.
                        'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                        ImageFileList.Images.Add(file.Extension, iconForFile)
                    End If
                    item.ImageKey = file.Extension
                    If Not (InStr(1, item.Text, "Search", vbTextCompare) <> 0 And InStr(1, item.Text, ".html", vbTextCompare) <> 0) Then
                        LVFiles1.Items.Add(item)
                    End If

                Next file
                LVFiles1.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx - Payment Request")
        End Try
    End Sub

    Private Sub LVFiles_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles LVFiles.ItemMouseHover
        LVFiles.ContextMenuStrip = ContextMenuStripFiles
    End Sub

    Private Sub LVFiles1_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles LVFiles1.ItemMouseHover
        LVFiles1.ContextMenuStrip = ContextMenuStripFiles1
    End Sub

    Private Sub LVFiles_DragEnter(sender As Object, e As DragEventArgs) Handles LVFiles.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles1_DragEnter(sender As Object, e As DragEventArgs) Handles LVFiles1.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles_DragLeave(sender As Object, e As EventArgs) Handles LVFiles.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub LVFiles1_DragLeave(sender As Object, e As EventArgs) Handles LVFiles1.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub LVFiles_DragDrop(sender As Object, e As DragEventArgs) Handles LVFiles.DragDrop
        If IO.Directory.Exists(ClsPayAuth.PaymentFilePath) Then
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    If Not IO.File.Exists(ClsPayAuth.PaymentFilePath & Dir(MyFiles(i))) Then
                        IO.File.Copy(MyFiles(i), ClsPayAuth.PaymentFilePath & Dir(MyFiles(i)))
                    End If
                Next
                ExtractAssociatedIconEx()
            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Interop.Outlook.MailItem) Then
                                mi.SaveAs(ClsPayAuth.PaymentFilePath & "M" & MsgCount & "_" & mi.SenderName.ToString & "_" & mi.ReceivedTime.ToString("yyyy-MM-dd HHmm") & ".msg")
                                MsgCount = MsgCount + 1
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub LVFiles1_DragDrop(sender As Object, e As DragEventArgs) Handles LVFiles1.DragDrop
        If IO.Directory.Exists(ClsPayAuth.PaymentFilePath) Then
            Dim varFiles As String() = IO.Directory.GetFiles(ClsPayAuth.PaymentFilePath, cPaymentSSIName & "*.*")

            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    IO.File.Copy(MyFiles(i), ClsPayAuth.PaymentFilePath & cPaymentSSIName & varFiles.Length & IO.Path.GetExtension(MyFiles(i)))
                Next
                ExtractAssociatedIconEx1()
            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Interop.Outlook.MailItem) Then
                                If MsgCount < 2 Then
                                    mi.SaveAs(ClsPayAuth.PaymentFilePath & cPaymentSSIName & IIf(varFiles.Length = 0, "", varFiles.Length) & ".msg")
                                    MsgCount = MsgCount + 1
                                End If
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx1()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub OpenFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            ShellExecute(ClsPayAuth.PaymentFilePath & LVFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub RemoveFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            IO.File.Delete(ClsPayAuth.PaymentFilePath & LVFiles.SelectedItems(0).Text)
        End If
        ExtractAssociatedIconEx()
    End Sub

    Private Sub cmdBPBrowse_Click(sender As Object, e As EventArgs) Handles cmdBPBrowse.Click
        If (BPFolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            txtBPFilePath.Text = BPFolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub cboFeeType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBPFeeType.SelectedIndexChanged
        If IsNumeric(cboBPFeeType.SelectedValue) Then
            If cboBPFeeType.SelectedValue = 0 Then
                lblPRAmountFee.Text = "Fee:"
            Else
                lblPRAmountFee.Text = "Rate:"
            End If
        End If
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        ExtractAssociatedIconEx()
    End Sub

    Private Sub dgvbp_CellLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.CellLeave
        Dim varBankName As String = ""
        Dim varColName As String = dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name

        If CboBPType.SelectedValue = 0 Then
            If varColName = "Bank Swift" Then
                AddSwiftValue(dsBS, dsBN, e.RowIndex, e.ColumnIndex, "Bank Name", "Bank Swift", "Bank IBAN", "Bank Country")
            ElseIf varColName = "Sub Bank Swift" Then
                AddSwiftValue(dsSBS, dsSBN, e.RowIndex, e.ColumnIndex, "Sub Bank Name", "Sub Bank Swift", "Sub Bank IBAN", "Sub Bank Country")
            ElseIf varColName = "Amount" Then
                If IsNumeric(dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                    dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = CDbl(dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).Value).ToString("#,##0.00")
                End If
            End If
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvbp, 0)
        Cursor = Cursors.Default
    End Sub


    Private Sub AddSwiftValue(ByVal dsSwift As DataSet, ByVal dsName As DataSet, ByVal varRow As Integer, ByVal varCol As Integer, ByVal varBankName As String, ByVal varColName As String, ByVal varBankIBANName As String, ByVal varBankCountryName As String)
        Dim varReturnedBankName As String = ""

        dgvbp.Rows(varRow).Cells(varBankName).Value = ""
        Dim varValue As String = Replace(dgvbp.Rows(varRow).Cells(varCol).EditedFormattedValue, "-", "")
        If varValue = "" Then
            varValue = Replace(dgvbp.Rows(varRow).Cells(varCol).Value, "-", "")
        End If

        If varValue <> "" Then
            If AddValueToDGVComboBox(dsSwift, varValue, varColName) Then
                dgvbp.Rows(varRow).Cells(varColName).Value = varValue
            End If
        End If

        'API check
        If String.IsNullOrEmpty(dgvbp.Rows(varRow).Cells(varBankName).EditedFormattedValue) And Len(Replace(dgvbp.Rows(varRow).Cells(varColName).Value, "-", "")) = 6 And Len(dgvbp.Rows(varRow).Cells(varBankIBANName).Value) = 8 Then
            If CheckBankNameDetails(Replace(dgvbp.Rows(varRow).Cells(varColName).EditedFormattedValue, "-", ""), dgvbp.Rows(varRow).Cells(varBankIBANName).Value, varReturnedBankName) Then
                If varReturnedBankName <> "" Then
                    varColName = varBankName
                    AddValueToDGVComboBox(dsName, varReturnedBankName, varColName)
                    dgvbp.Rows(varRow).Cells(varBankName).Value = varReturnedBankName
                    dgvbp.Rows(varRow).Cells(varBankCountryName).Value = 232
                End If
            Else
                MsgBox(varReturnedBankName, vbCritical)
            End If
        End If
    End Sub

    Private Function AddValueToDGVComboBox(ByVal ds As DataSet, ByRef varValue As String, ByRef varColName As String)
        AddValueToDGVComboBox = False
        If Not ds.Tables(varColName).Rows.Contains(varValue) Then
            Dim row As DataRow
            row = ds.Tables(varColName).NewRow
            row(0) = varValue
            row(1) = varValue
            ds.Tables(varColName).Rows.InsertAt(row, ds.Tables(varColName).Rows.Count + 1)
            AddValueToDGVComboBox = True
        End If
    End Function

    Private Sub cboBPPayType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBPPayType.SelectedIndexChanged
        Dim varFullTemplatePath As String = ""
        If IsNumeric(cboBPPayType.SelectedValue) Then
            If CboBPType.SelectedValue = 0 And cboBPPayType.SelectedValue < 0 Then
                If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
                    varFullTemplatePath = OpenFileDialog1.FileName
                End If
                ReadBatchTemplateXL(dgvbp, varFullTemplatePath)
            ElseIf CboBPType.SelectedValue <> 0 Then
                LoadGrid()
            End If
        End If
    End Sub

    Private Sub dgvbp_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvbp.CellFormatting
        If CboBPType.SelectedValue <> 0 Then
            If dgvbp.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "Status" Then
                FormatGridStatus(dgvbp, e.RowIndex, e.ColumnIndex)
            End If
        End If
    End Sub

    Private Sub dgvbp_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvbp.CellMouseClick
        If CboBPType.SelectedValue <> 0 Then
            If e.RowIndex = -1 And e.ColumnIndex = -1 Then
                SelectAll = Not SelectAll
                dgvbp.CurrentCell = dgvbp.Rows(0).Cells("BatchID")
                For i As Integer = 0 To dgvbp.Rows.Count - 2
                    dgvbp.Rows(i).Cells("SelectPay").Value = SelectAll
                Next
            End If
        End If
    End Sub

    Public Function ReadBatchTemplateXL(ByVal dgv As DataGridView, ByVal FullFilePathXL As String) As Boolean
        Dim xlApp As Interop.Excel.Application = Nothing
        Dim xlWorkBook As Interop.Excel.Workbook = Nothing
        Dim xlWorkSheet As Interop.Excel.Worksheet = Nothing
        Dim varRow As Integer
        Dim CtryCodeString As String
        Try
            xlApp = New Interop.Excel.Application
            xlApp.Workbooks.Open(FullFilePathXL)
            xlWorkSheet = xlApp.ActiveWorkbook.ActiveSheet

            varRow = 2
            While xlWorkSheet.Cells(varRow, 1).value <> ""
                If varRow = 2 Then
                    PopulateOrderGrid(dgv, xlWorkSheet.Cells(varRow, 1).Value, xlWorkSheet.Cells(varRow, 2).Value)
                    If IsDate(xlWorkSheet.Cells(varRow, 5).Value) Then
                        dtBPTransDate.Value = xlWorkSheet.Cells(varRow, 5).Value
                        dtBPSettleDate.Value = xlWorkSheet.Cells(varRow, 5).Value
                    End If
                End If
                dgvbp.Rows.Add()
                dgv.Rows(varRow - 2).Cells("Payment Ref").Value = xlWorkSheet.Cells(varRow, 3).Value
                If IsNumeric(xlWorkSheet.Cells(varRow, 4).Value) Then
                    dgv.Rows(varRow - 2).Cells("Amount").Value = CDbl(xlWorkSheet.Cells(varRow, 4).Value).ToString("#,##0.00")
                Else
                    dgv.Rows(varRow - 2).Cells("Amount").Value = 0
                End If
                dgv.Rows(varRow - 2).Cells("Relationship").Value = 5
                dgv.Rows(varRow - 2).Cells("Type").Value = IIf(xlWorkSheet.Cells(varRow, 6).Value = "Business", 1, 0)
                If dgv.Rows(varRow - 2).Cells("Type").Value = 0 Then
                    If Text.Split(" ").Length < 1 Then
                        dgv.Rows(varRow - 2).Cells("Surname").Value = xlWorkSheet.Cells(varRow, 7).Value
                    ElseIf Text.Split(" ").Length = 1 Then
                        dgv.Rows(varRow - 2).Cells("First Name").Value = Strings.Left(xlWorkSheet.Cells(varRow, 7).Value, InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare) - 1)
                        dgv.Rows(varRow - 2).Cells("Surname").Value = Strings.Right(xlWorkSheet.Cells(varRow, 7).Value, Len(xlWorkSheet.Cells(varRow, 7).Value) - InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare))
                    ElseIf Text.Split(" ").Length = 2 Then
                        dgv.Rows(varRow - 2).Cells("First Name").Value = Strings.Left(xlWorkSheet.Cells(varRow, 7).Value, InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare) - 1)
                        dgv.Rows(varRow - 2).Cells("Middle Name").Value = xlWorkSheet.Cells(varRow, 7).Value
                        dgv.Rows(varRow - 2).Cells("Surname").Value = Strings.Right(xlWorkSheet.Cells(varRow, 7).Value, Len(xlWorkSheet.Cells(varRow, 7).Value) - InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare))
                    Else
                        dgv.Rows(varRow - 2).Cells("First Name").Value = Strings.Left(xlWorkSheet.Cells(varRow, 7).Value, InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare) - 1)
                        dgv.Rows(varRow - 2).Cells("Surname").Value = Strings.Right(xlWorkSheet.Cells(varRow, 7).Value, Len(xlWorkSheet.Cells(varRow, 7).Value) - InStr(1, xlWorkSheet.Cells(varRow, 7).Value, " ", vbTextCompare))
                    End If
                Else
                    dgv.Rows(varRow - 2).Cells("Business Name").Value = xlWorkSheet.Cells(varRow, 7).Value
                End If
                ChangeType(varRow - 2, dgv.Rows(varRow - 2).Cells("Type").ColumnIndex)

                CtryCodeString = ClsIMS.GetSQLItem("Select Ctry_ID from DolfinPaymentRARangeCountry where Ctry_Name = '" & xlWorkSheet.Cells(varRow, 8).Value & "'")
                If IsNumeric(CtryCodeString) Then
                    dgv.Rows(varRow - 2).Cells("Country").Value = CInt(CtryCodeString)
                End If
                dgv.Rows(varRow - 2).Cells("Bank IBAN").Value = xlWorkSheet.Cells(varRow, 10).Value
                CtryCodeString = ClsIMS.GetSQLItem("Select Ctry_ID from DolfinPaymentRARangeCountry where Ctry_Name = '" & xlWorkSheet.Cells(varRow, 11).Value & "'")
                If IsNumeric(CtryCodeString) Then
                    dgv.Rows(varRow - 2).Cells("Bank Country").Value = CInt(CtryCodeString)
                End If
                If xlWorkSheet.Cells(varRow, 9).Value <> "" Then
                    dgv.Rows(varRow - 2).Cells("Bank Swift").Value = xlWorkSheet.Cells(varRow, 9).Value
                    AddSwiftValue(dsBS, dsBN, varRow - 2, dgv.Rows(varRow - 2).Cells("Bank Swift").ColumnIndex, "Bank Name", "Bank Swift", "Bank IBAN", "Bank Country")
                    UpdateBankDetailsOnGrid(dgvbp, varRow - 2, dgv.Rows(varRow - 2).Cells("Bank Swift").ColumnIndex)
                End If

                dgv.Rows(varRow - 2).Cells("Sub Bank IBAN").Value = xlWorkSheet.Cells(varRow, 13).Value
                CtryCodeString = ClsIMS.GetSQLItem("Select Ctry_ID from DolfinPaymentRARangeCountry where Ctry_Name = '" & xlWorkSheet.Cells(varRow, 14).Value & "'")
                If IsNumeric(CtryCodeString) Then
                    dgv.Rows(varRow - 2).Cells("Sub Bank Country").Value = CInt(CtryCodeString)
                End If
                If xlWorkSheet.Cells(varRow, 12).Value <> "" Then
                    dgv.Rows(varRow - 2).Cells("Sub Bank Swift").Value = xlWorkSheet.Cells(varRow, 12).Value
                    AddSwiftValue(dsSBS, dsSBN, varRow - 2, dgv.Rows(varRow - 2).Cells("Sub Bank Swift").ColumnIndex, "Sub Bank Name", "Sub Bank Swift", "Sub Bank IBAN", "Sub Bank Country")
                    UpdateBankDetailsOnGrid(dgvbp, varRow - 2, dgv.Rows(varRow - 2).Cells("Sub Bank Swift").ColumnIndex)
                End If

                dgv.Rows(varRow - 2).Cells("VO_ID").Value = xlWorkSheet.Cells(varRow, 15).Value
                dgv.Rows(varRow - 2).Cells("Bank BIK").Value = xlWorkSheet.Cells(varRow, 16).Value
                dgv.Rows(varRow - 2).Cells("Bank INN").Value = xlWorkSheet.Cells(varRow, 17).Value
                dgv.Rows(varRow - 2).Cells("Sub Bank BIK").Value = xlWorkSheet.Cells(varRow, 18).Value
                dgv.Rows(varRow - 2).Cells("Sub Bank INN").Value = xlWorkSheet.Cells(varRow, 19).Value
                dgv.Rows(varRow - 2).Cells("Comments").Value = xlWorkSheet.Cells(varRow, 20).Value
                dgv.Rows(varRow - 2).Cells("Add Fee").Value = True
                varRow = varRow + 1
            End While

            xlApp.ActiveWorkbook.Close()
            xlApp.Quit()
            ReadBatchTemplateXL = True
            xlApp = Nothing
        Catch ex As Exception
            ReadBatchTemplateXL = False
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameOmnibusSwitch, Err.Description & " - error in ReadBatchTemplateXL")
            xlApp = Nothing
        End Try
    End Function

    Public Sub PopulateOrderGrid(ByVal dgv As DataGridView, ByVal ClientID As String, ByVal PayCCY As String)
        Dim Lst As Dictionary(Of String, String) = ClsIMS.GetDataToList("select top 1 sa.pf_code, sa.CR_Code,t_code,sa.portfolio,sa.cr_name1,sa.t_name1 from [dbo].[vwDolfinPaymentClientPortfolio] cp inner join [dbo].[vwDolfinPaymentSelectAccount] sa on cp.pf_code = sa.pf_code
                                                                        where cust_code = '" & ClientID & "' and cr_name1 = '" & PayCCY & "' and brokerclass= 'BANK'
                                                                        order by case when cp.pf_sname1 like '%execution%' then 0 else 1 end, balance desc")

        If Not Lst Is Nothing Then
            CboBPPortfolio.DataSource = Nothing
            CboBPCCY.DataSource = Nothing
            cboBPFeeCCY.DataSource = Nothing
            CboBPOrderAccount.DataSource = Nothing
            If cboBPPayType.SelectedValue = -2 Then
                ClsIMS.PopulateComboboxWithData(CboBPPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and b_shortcut1 not in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
            ElseIf cboBPPayType.SelectedValue = -1 Then
                ClsIMS.PopulateComboboxWithData(CboBPPortfolio, "Select 0 as pf_Code, '' as Portfolio union Select pf_Code,Portfolio from vwDolfinPaymentSelectAccount where BranchCode = " & ClsIMS.UserLocationCode & " and b_shortcut1 in ('BBPaySvc') and b_class In (1,25,37,39) order by Portfolio")
            End If
            CboBPPortfolio.SelectedValue = Lst.Item("pf_code")
            CboBPPortfolio.Text = Lst.Item("portfolio")
            ClsIMS.PopulateComboboxWithData(CboBPCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & Lst.Item("pf_code") & " group by CR_Code,CR_Name1 order by CR_Name1")
            CboBPCCY.SelectedValue = Lst.Item("CR_Code")
            ClsIMS.PopulateComboboxWithData(cboBPFeeCCY, "Select CR_Code,CR_Name1 from vwDolfinPaymentSelectAccount where pf_code = " & Lst.Item("pf_code") & " group by CR_Code,CR_Name1 order by CR_Name1")
            ClsIMS.PopulateComboboxWithData(CboBPOrderAccount, "select t_Code,t_Name1 from vwDolfinPaymentSelectAccount where b_class in (1,25,37,39) and PF_Code = " & Lst.Item("pf_code") & " and cr_code = " & Lst.Item("CR_Code") & " group by t_Code,t_name1 order by t_Name1")

            CboBPOrderAccount.SelectedValue = Lst.Item("t_code")
            CboBPOrderAccount.Text = Lst.Item("t_name1")
            CboBPPortfolio.SelectedValue = Lst.Item("pf_code")
            CboBPPortfolio.Text = Lst.Item("portfolio")
            LoadGrid()
            GetFee()
        End If
    End Sub

    Private Sub GetFee()
        If IsNumeric(CboBPPortfolio.SelectedValue) And IsNumeric(CboBPOrderAccount.SelectedValue) Then
            txtBPOrderBalance.Text = ClsIMS.GetBalance(CboBPPortfolio.SelectedValue, CboBPOrderAccount.SelectedValue).ToString("#,##0.00")
            ColorBalanceTextBox(txtBPOrderBalance)
            cboBPFeeType.SelectedValue = 0
            If CboBPCCY.SelectedValue = 6 Then
                Dim varFee As String = ClsIMS.GetSQLItem("select convert(nvarchar(20),TF_Fee) + ',' + TF_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & CboBPPortfolio.SelectedValue)
                If varFee <> "" Then
                    cboBPFeeCCY.Text = Strings.Right(varFee, Len(varFee) - InStr(1, varFee, ",", vbTextCompare))
                    If IsNumeric(Strings.Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1)) Then
                        txtBPAmountFee.Text = CInt(Strings.Left(varFee, InStr(1, varFee, ",", vbTextCompare) - 1))
                    End If
                End If
            Else
                Dim varIntFee As String = ClsIMS.GetSQLItem("select convert(nvarchar(20),TF_Int_Fee) + ',' + TF_Int_CCY from vwDolfinPaymentTransferFees where TF_PortfolioCode = " & CboBPPortfolio.SelectedValue)
                If varIntFee <> "" Then
                    cboBPFeeCCY.Text = Strings.Right(varIntFee, Len(varIntFee) - InStr(1, varIntFee, ",", vbTextCompare))
                    If IsNumeric(Strings.Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1)) Then
                        txtBPAmountFee.Text = CInt(Strings.Left(varIntFee, InStr(1, varIntFee, ",", vbTextCompare) - 1))
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub UpdateBankDetailsOnGrid(ByVal dgv As DataGridView, ByVal varRow As Integer, ByVal varcol As Integer)
        Dim TitleName As String = "", TmpText As String = ""
        Dim varColName As String = dgv.Rows(varRow).Cells(varcol).OwningColumn.Name
        Dim Lst As Dictionary(Of String, String)

        If CboBPType.SelectedValue = 0 And dgvbp.Columns.Contains("Type") Then
            If Len(Replace(dgv.Rows(varRow).Cells("Bank Swift").Value, "-", "")) <> 6 Then
                If InStr(1, varColName, "Bank Name", vbTextCompare) <> 0 Or InStr(1, varColName, "Swift", vbTextCompare) <> 0 Then
                    If Strings.Left(varColName, 9) = "Sub Bank " Then
                        TitleName = "Sub Bank "
                    ElseIf Strings.Left(varColName, 5) = "Bank " Then
                        TitleName = "Bank "
                    End If

                    If InStr(1, varColName, "Bank Name", vbTextCompare) <> 0 Then
                        Lst = ClsIMS.GetDataToList("Select Ctry_ID, SC_SwiftCode from vwDolfinPaymentSwiftCodes sc inner join DolfinPaymentRARangeCountry c On sc.SC_CountryID = c.Ctry_ID where SC_BankName = '" & dgv.Rows(varRow).Cells(varcol).Value & "'")
                        If Not Lst Is Nothing Then
                            dgv.Rows(varRow).Cells(TitleName & "Swift").Value = Lst("SC_SwiftCode").ToString()
                            If IsNumeric(Lst("Ctry_ID")) Then
                                dgv.Rows(varRow).Cells(TitleName & "Country").Value = CInt(Lst("Ctry_ID"))
                            End If
                        End If
                    Else
                        Lst = ClsIMS.GetDataToList("Select Ctry_ID, SC_BankName from vwDolfinPaymentSwiftCodes sc inner join DolfinPaymentRARangeCountry c On sc.SC_CountryID = c.Ctry_ID where SC_SwiftCode = '" & dgv.Rows(varRow).Cells(varcol).Value & "'")
                        If Not Lst Is Nothing Then
                            dgv.Rows(varRow).Cells(TitleName & "Name").Value = Lst("SC_BankName").ToString()
                            If IsNumeric(Lst("Ctry_ID")) Then
                                dgv.Rows(varRow).Cells(TitleName & "Country").Value = CInt(Lst("Ctry_ID"))
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub dgvbp_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvbp.CellClick
        If e.RowIndex >= 0 And e.ColumnIndex = 0 Then
            clearRow(e.RowIndex)
        ElseIf e.RowIndex >= 0 And e.ColumnIndex = 1 Then
            If e.RowIndex > 0 Then
                clearRow(e.RowIndex)
                CopyRow(e.RowIndex)
                dgvbp.Update()
            Else
                dgvbp.EndEdit()
            End If
        ElseIf e.RowIndex >= 0 And e.ColumnIndex = 2 Then
            ClsBP.PortfolioCode = CboBPPortfolio.SelectedValue
            ClsBP.PortfolioName = CboBPPortfolio.Text
            ClsBP.CCYCode = CboBPCCY.SelectedValue
            ClsBP.CCYName = CboBPCCY.Text
            NewBPTemplate = New FrmBatchProcessingTemplate
            NewBPTemplate.ShowDialog()
            clearRow(e.RowIndex)
            GetTemplateDetails(e.RowIndex, ClsBP.TemplateID)
            dgvbp.Update()
        End If
        If e.RowIndex >= 0 Then
            ChangeType(e.RowIndex, e.ColumnIndex)
        End If
    End Sub
End Class
