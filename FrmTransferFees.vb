﻿Public Class FrmTransferFees
    Dim TransConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataCCY As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim cboCCYColumn As New DataGridViewComboBoxColumn

    Private Sub TFLoadGrid(Optional ByVal varFilter As String = "")
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            If varFilter = "" Then
                varSQL = "select * from vwDolfinPaymentTransferFees where branchCode = " & ClsIMS.UserLocationCode & " order by Portfolio"
            Else
                varSQL = "select * from vwDolfinPaymentTransferFees where branchCode = " & ClsIMS.UserLocationCode & " and pf_Code in (" & varFilter & ") order by Portfolio"
            End If

            dgvTrans.DataSource = Nothing
            TransConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TransConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentTransferFees")
            dgvTrans.AutoGenerateColumns = True
            dgvTrans.DataSource = ds.Tables("vwDolfinPaymentTransferFees")

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'EUR' as TF_CCY union Select 'GBP' as TF_CCY union Select 'USD' as TF_CCY", TransConn)
            dgvdataCCY.Fill(ds, "CCY")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "CCY"
            cboCCYColumn.DataPropertyName = "TF_CCY"
            cboCCYColumn.DataSource = ds.Tables("CCY")
            cboCCYColumn.ValueMember = ds.Tables("CCY").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            dgvTrans.Columns.Insert(6, cboCCYColumn)

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'EUR' as TF_CCY union Select 'GBP' as TF_CCY union Select 'USD' as TF_CCY", TransConn)
            dgvdataCCY.Fill(ds, "Int CCY")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "Int CCY"
            cboCCYColumn.DataPropertyName = "TF_Int_CCY"
            cboCCYColumn.DataSource = ds.Tables("Int CCY")
            cboCCYColumn.ValueMember = ds.Tables("Int CCY").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            dgvTrans.Columns.Insert(9, cboCCYColumn)

            dgvTrans.Columns(0).Width = 400
            dgvTrans.Columns(0).ReadOnly = True
            dgvTrans.Columns(1).Visible = False
            dgvTrans.Columns(2).Visible = False
            dgvTrans.Columns(3).Visible = False
            dgvTrans.Columns(4).Visible = False
            dgvTrans.Columns(5).HeaderText = "External Fee"
            dgvTrans.Columns(5).Width = 100
            dgvTrans.Columns(6).Width = 100
            dgvTrans.Columns(6).HeaderText = "External CCY"
            dgvTrans.Columns(7).Visible = False
            dgvTrans.Columns(8).Width = 100
            dgvTrans.Columns(8).HeaderText = "External International Fee"
            dgvTrans.Columns(9).Width = 100
            dgvTrans.Columns(9).HeaderText = "External International CCY"
            dgvTrans.Columns(10).HeaderText = "Modified By"
            dgvTrans.Columns(11).HeaderText = "Modified Time"

            dgvTrans.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTrans.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvTrans.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing transfer fees grid")
        End Try
    End Sub

    Private Sub FrmTransferFees_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        TFLoadGrid()
    End Sub

    Private Sub FrmTransferFees_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If TransConn.State = ConnectionState.Open Then
            TransConn.Close()
        End If
    End Sub

    Private Sub FrmTransferFees_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateFormCombo(cboTF)
    End Sub

    Private Sub cboTF_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTF.SelectedIndexChanged
        If IsNumeric(cboTF.SelectedValue) Then
            TFLoadGrid(cboTF.SelectedValue.ToString)
        End If
    End Sub

    Private Sub dgvTrans_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTrans.RowLeave
        Dim varID As Integer

        If dgvTrans.EndEdit Then
            Try
                varID = dgvTrans.Rows(e.RowIndex).Cells(2).Value
                Dim varFee = IIf(IsDBNull(dgvTrans.Rows(e.RowIndex).Cells(5).Value), 0, dgvTrans.Rows(e.RowIndex).Cells(5).Value)
                Dim varIntFee = IIf(IsDBNull(dgvTrans.Rows(e.RowIndex).Cells(8).Value), 0, dgvTrans.Rows(e.RowIndex).Cells(8).Value)

                If IsNumeric(varFee) And IsNumeric(varIntFee) Then
                    Dim varDelSQL As String = "delete from DolfinPaymentTransferFees where TF_PortfolioCode = " & varID & " and TF_Fee = 0 and TF_Int_Fee = 0"
                    ClsIMS.ExecuteString(TransConn, varDelSQL)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTransferFees, varDelSQL)

                    If varFee = 0 And varIntFee = 0 Then
                        Dim varSQL As String = "delete from DolfinPaymentTransferFees where TF_PortfolioCode = " & varID
                        ClsIMS.ExecuteString(TransConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTransferFees, varSQL)
                    End If
                End If

                If IsNumeric(varFee) Then
                    If varFee <> 0 Then
                        Dim CheckID As String = ClsIMS.GetSQLItem("select TF_PortfolioCode from vwDolfinPaymentTransferFees where PF_Code = " & varID)
                        If CheckID = "" Then
                            ClsIMS.ExecuteString(TransConn, "insert into vwDolfinPaymentTransferFees(TF_PortfolioCode) values (" & varID & ")")
                        End If

                        Dim varCCY As String = dgvTrans.Rows(e.RowIndex).Cells(4).Value
                        If Not IsDBNull(varCCY) Then
                            Dim varSQL As String = "update vwDolfinPaymentTransferFees set TF_CCY = '" & varCCY & "', TF_Fee = " & varFee & ",TF_UserModifiedBy = '" & ClsIMS.FullUserName & "' where PF_Code = " & varID
                            ClsIMS.ExecuteString(TransConn, varSQL)
                            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTransferFees, varSQL)
                        End If
                    End If
                End If

                If IsNumeric(varIntFee) Then
                    If varIntFee <> 0 Then
                        Dim CheckID As String = ClsIMS.GetSQLItem("select TF_PortfolioCode from vwDolfinPaymentTransferFees where PF_Code = " & varID)
                        If CheckID = "" Then
                            ClsIMS.ExecuteString(TransConn, "insert into vwDolfinPaymentTransferFees(TF_PortfolioCode) values (" & varID & ")")
                        End If

                        Dim varIntCCY As String = dgvTrans.Rows(e.RowIndex).Cells(7).Value
                        If Not IsDBNull(varIntCCY) Then
                            Dim varSQL As String = "update vwDolfinPaymentTransferFees set TF_Int_CCY = '" & varIntCCY & "', TF_Int_Fee = " & varIntFee & ",TF_UserModifiedBy = '" & ClsIMS.FullUserName & "' where PF_Code = " & varID
                            ClsIMS.ExecuteString(TransConn, varSQL)
                            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTransferFees, varSQL)
                        End If
                    End If
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with updating Transacion fees ID: " & varID)
            End Try
        End If
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        TFLoadGrid()
        cboTF.Text = ""
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvTrans, 0)
        Cursor = Cursors.Default
    End Sub
End Class