﻿Public Class FrmTreasuryAllocationTermDeposits
    Dim TATDConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataTitles As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim MoveColA As New DataGridViewTextBoxColumn
    Dim TotalColA As New DataGridViewTextBoxColumn
    Dim MoveColB As New DataGridViewTextBoxColumn
    Dim TotalColB As New DataGridViewTextBoxColumn
    Dim TotalRow As New DataGridViewRow
    Dim CompletedCellEdit As Boolean = False
    Dim varCurrentRow As Integer = 0
    Dim varSingles As Integer = vbNo

    Private Sub TATDLoadGrid(Optional ByVal varWhereClauseFilter As String = "")
        Dim varSQL As String = ""
        Dim dgvColumnHeaderStyle As New DataGridViewCellStyle()
        dgvColumnHeaderStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Dim Boldstyle As New DataGridViewCellStyle()

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "Select 0,0,0 from vwDolfinPaymentSelectAccountTreasuryTermDeposits where BrokerA = '12ghfgvfhg'"

            dgvTATDTitles.DataSource = Nothing
            TATDConn = ClsIMS.GetNewOpenConnection
            dgvdataTitles = New SqlClient.SqlDataAdapter(varSQL, TATDConn)
            ds = New DataSet
            dgvdataTitles.Fill(ds, "Titles")
            dgvTATDTitles.AutoGenerateColumns = True
            dgvTATDTitles.DataSource = ds.Tables("Titles")

            dgvTATDTitles.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Raised

            dgvTATDTitles.ColumnHeadersDefaultCellStyle = dgvColumnHeaderStyle
            dgvTATDTitles.Columns(0).Width = 400
            dgvTATDTitles.Columns(0).HeaderText = ""
            dgvTATDTitles.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATDTitles.Columns(1).Width = 300
            dgvTATDTitles.Columns(1).HeaderText = CboTATDInstA.Text
            dgvTATDTitles.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATDTitles.Columns(2).Width = 300
            dgvTATDTitles.Columns(2).HeaderText = CboTATDInstB.Text
            dgvTATDTitles.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable

            varSQL = "exec DolfinPaymentSelectAccountByDateTermDeposits '" & dtTransDate.Value.ToString("yyyy-MM-dd") & "'," & CboTATDInstA.SelectedValue & "," & CboTATDInstB.SelectedValue & "," & cboTATDCCY.SelectedValue

            dgvTATD.DataSource = Nothing
            TATDConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TATDConn)
            ds = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(ds, "vwDolfinPaymentSelectAccountTreasuryTermDeposits")
            dgvTATD.AutoGenerateColumns = True
            dgvTATD.DataSource = ds.Tables("vwDolfinPaymentSelectAccountTreasuryTermDeposits")

            TotalRow = New DataGridViewRow

            dgvTATD.Columns(0).HeaderText = "Portfolio"
            dgvTATD.Columns(0).Width = 400
            dgvTATD.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(0).ReadOnly = True
            dgvTATD.Columns(1).HeaderText = "Current Balance"
            dgvTATD.Columns(1).Width = 100
            dgvTATD.Columns(1).ReadOnly = True
            dgvTATD.Columns(1).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(1).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(2).HeaderText = "Movement"
            dgvTATD.Columns(2).Width = 100
            dgvTATD.Columns(2).DefaultCellStyle.ForeColor = Color.Blue
            dgvTATD.Columns(2).DefaultCellStyle.BackColor = Color.LightYellow
            dgvTATD.Columns(2).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(2).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(3).HeaderText = "New Balance"
            dgvTATD.Columns(3).Width = 100
            dgvTATD.Columns(3).ReadOnly = True
            dgvTATD.Columns(3).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(3).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(4).HeaderText = "Current Balance"
            dgvTATD.Columns(4).Width = 100
            dgvTATD.Columns(4).ReadOnly = True
            dgvTATD.Columns(4).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(4).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(5).HeaderText = "Movement"
            dgvTATD.Columns(5).Width = 100
            dgvTATD.Columns(5).DefaultCellStyle.ForeColor = Color.Blue
            dgvTATD.Columns(5).ReadOnly = True
            dgvTATD.Columns(5).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(5).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(6).HeaderText = "New Balance"
            dgvTATD.Columns(6).Width = 100
            dgvTATD.Columns(6).ReadOnly = True
            dgvTATD.Columns(6).HeaderCell.Style = dgvColumnHeaderStyle
            dgvTATD.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            dgvTATD.Columns(6).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvTATD.Columns(7).Visible = False
            dgvTATD.Columns(8).Visible = False
            dgvTATD.Columns(9).Visible = False
            dgvTATD.Columns(10).Visible = False
            dgvTATD.Columns(11).Visible = False
            dgvTATD.Columns(12).Visible = False
            dgvTATD.Columns(13).Visible = False
            dgvTATD.Columns(14).Visible = False
            dgvTATD.Columns(15).Visible = False
            dgvTATD.Columns(16).Visible = False
            dgvTATD.Columns(17).Visible = False
            dgvTATD.Columns(18).Visible = False
            dgvTATD.Columns(19).Visible = False
            dgvTATD.Columns(20).Visible = False
            dgvTATD.Columns(21).Visible = False
            dgvTATD.Columns(22).Visible = False
            dgvTATD.Columns(23).Visible = False
            dgvTATD.Columns(24).Visible = False
            dgvTATD.Columns(25).Visible = False
            dgvTATD.Columns(26).Visible = False
            dgvTATD.Columns(27).Visible = False
            dgvTATD.Columns(28).Visible = False

            Boldstyle.Font = New Font(dgvTATD.Font, FontStyle.Bold)
            dgvTATD.Rows(dgvTATD.Rows.Count - 1).DefaultCellStyle.BackColor = Color.LightGray

            dgvTATD.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTATD.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
        End Try
    End Sub

    Public Sub RunGrid()
        ClearResults()
        If cboTATDCCY.Text = "" Or CboTATDInstA.Text = "" Or CboTATDInstB.Text = "" Then
            MsgBox("Please complete all criteria to run treasury options", vbInformation, "Complete Criteria")
        ElseIf CboTATDInstA.Text = CboTATDInstB.Text Then
            MsgBox("Please select different institutions to run treasury options", vbInformation, "Different Institutions Required")
        Else
            TATDLoadGrid()
        End If
    End Sub

    Private Sub CmdTARefreshgrid_Click(sender As Object, e As EventArgs) Handles CmdTATDRefreshgrid.Click
        TATDLoadGrid()
    End Sub

    Private Sub FrmTreasuryAllocation_Disposed(sender As Object, e As EventArgs) Handles MyBase.Disposed
        ClearResults()
        If TATDConn.State = ConnectionState.Open Then
            TATDConn.Close()
        End If
    End Sub

    Private Sub dgvTATD_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTATD.CellEndEdit
        Dim varNum As Double
        Dim varBalA As Double, varBalB As Double

        If Not IsDBNull(dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
            If dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value <> "" Then
                varNum = BasicCalculator(dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                If varNum <> 0 Then
                    dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = CDbl(varNum).ToString("#,##0.00")
                    varBalA = dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex - 1).Value
                    varBalB = dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 2).Value
                    If ValidAmount(varNum, CDbl(varBalA), CDbl(varBalB), e.ColumnIndex, e.RowIndex) Then
                        dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 1).Value = (CDbl(varBalA) + varNum).ToString("#,##0.00")
                        dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = (-varNum).ToString("#,##0.00")
                        dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 4).Value = (CDbl(varBalB) + -varNum).ToString("#,##0.00")
                    Else
                        dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
                        dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
                    End If
                Else
                    dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
                    dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
                End If
            End If
        Else
            dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = ""
            dgvTATD.Rows(e.RowIndex).Cells(e.ColumnIndex + 3).Value = ""
        End If
        Refreshtotals(e.ColumnIndex, e.RowIndex)
        CompletedCellEdit = True
    End Sub

    Private Sub Refreshtotals(ByVal vColNum As Integer, vRowNum As Integer)
        If IsDBNull(dgvTATD.Rows(vRowNum).Cells(vColNum).Value) Then
            dgvTATD.Rows(vRowNum).Cells(vColNum + 1).Value = dgvTATD.Rows(vRowNum).Cells(vColNum - 1).Value
            dgvTATD.Rows(vRowNum).Cells(vColNum + 4).Value = dgvTATD.Rows(vRowNum).Cells(vColNum + 2).Value
        ElseIf dgvTATD.Rows(vRowNum).Cells(vColNum).Value = "" Or dgvTATD.Rows(vRowNum).Cells(vColNum).Value = "0" Then
            dgvTATD.Rows(vRowNum).Cells(vColNum + 1).Value = dgvTATD.Rows(vRowNum).Cells(vColNum - 1).Value
            dgvTATD.Rows(vRowNum).Cells(vColNum + 4).Value = dgvTATD.Rows(vRowNum).Cells(vColNum + 2).Value
        End If

        dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(vColNum).Value = SumColumnTotal(vColNum).ToString("#,##0.00")
        dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(vColNum + 1).Value = SumColumnTotal(vColNum + 1).ToString("#,##0.00")
        dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(vColNum + 3).Value = SumColumnTotal(vColNum + 3).ToString("#,##0.00")
        dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(vColNum + 4).Value = SumColumnTotal(vColNum + 4).ToString("#,##0.00")
    End Sub
    Private Function SumColumnTotal(ByVal vColnum As Integer) As Double
        SumColumnTotal = 0
        For i As Integer = 0 To Me.dgvTATD.Rows.Count - 2
            If IsNumeric(dgvTATD.Rows(i).Cells(vColnum).Value) Then
                SumColumnTotal = SumColumnTotal + dgvTATD.Rows(i).Cells(vColnum).Value
            End If
        Next
    End Function

    Private Function ValidAmount(ByVal varNum As Double, varBalA As Double, varBalB As Double, vColNum As Integer, vRowNum As Integer) As Boolean
        ValidAmount = True
        If varNum > 0 Then
            If varBalB < varNum Then
                ValidAmount = False
                MsgBox("Amount is greater than the balance available in institution B", vbCritical, "Not enough in balance B")
            ElseIf Not IsEnoughCashInAccounts(Math.Abs(varNum), vColNum - 1, vRowNum) Then
                ValidAmount = False
                MsgBox("There is not enough cash in the alternative accounts to do this transfer", vbCritical, "Cannot transfer")
            End If
        ElseIf varNum < 0 Then
            If varBalA < Math.Abs(varNum) Then
                ValidAmount = False
                MsgBox("Amount is greater than the balance available in institution A", vbCritical, "Not enough in balance A")
            ElseIf Not IsEnoughCashInAccounts(Math.Abs(varNum), vColNum + 2, vRowNum) Then
                ValidAmount = False
                MsgBox("There is not enough cash in the alternative accounts to do this transfer", vbCritical, "Cannot transfer")
            End If
        Else
            ValidAmount = False
        End If
    End Function

    Private Function IsEnoughCashInAccounts(ByVal vCash As Double, ByVal vColnum As Integer, ByVal vRow As Integer) As Boolean
        Dim vSumColumnTotal As Double = 0

        IsEnoughCashInAccounts = False
        For i As Integer = 0 To Me.dgvTATD.Rows.Count - 2
            If IsNumeric(dgvTATD.Rows(i).Cells(vColnum).Value) Then
                If i <> vRow And dgvTATD.Rows(i).Cells(vColnum).Value > 0 Then
                    vSumColumnTotal = vSumColumnTotal + dgvTATD.Rows(i).Cells(vColnum).Value
                End If
            End If
        Next

        If vCash <= vSumColumnTotal Then
            IsEnoughCashInAccounts = True
        End If
    End Function

    Public Sub ClearResults()
        TimerGetResults.Enabled = False
        txtTATDFileName.Text = ""
        txtTATDStatus.Text = ""
        txtTATDMessage.Text = ""
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        Dim Msg As Integer = vbYes

        If ValidMovements() Then
            If dtTransDate.Value.Date <> Now.Date Then
                Msg = MsgBox("The movements will be pre-dated in IMS for " & dtTransDate.Value.Date & vbNewLine & "Is this correct?", vbYesNo + vbQuestion, "Are you sure")
            End If

            If Msg = vbYes Then
                ClearResults()
                ClsPayTreasury.SubmitTreasuryTransactions(dgvTATD, dtTransDate.Value.Date)
                TimerGetResults.Enabled = True
                If Not ClsPay.PaymentFilePath Is Nothing Then
                    If Dir(ClsPayTreasury.PaymentFilePath) <> "" Then
                        txtTATDFileName.Text = Dir(ClsPayTreasury.PaymentFilePath)
                        txtTATDStatus.Text = "Sent"
                        txtTATDStatus.ForeColor = Color.Black
                        txtTATDFileName.ForeColor = Color.Black
                        txtTATDMessage.ForeColor = Color.Black
                    End If
                End If
            End If
        End If
    End Sub

    Private Function PopulateResults(ByRef varErrors As Integer, ByRef varErrorDesc As String) As Boolean
        PopulateResults = False

        If txtTATDFileName.Text <> "" Then
            If ClsIMS.GetTAStatus(txtTATDFileName.Text, varErrors, varErrorDesc) Then
                If varErrors = 0 Then
                    txtTATDStatus.Text = ClsIMS.GetStatusType(cStatusSwiftAck)
                    txtTATDStatus.ForeColor = Color.DarkGreen
                    txtTATDFileName.ForeColor = Color.DarkGreen
                    txtTATDMessage.ForeColor = Color.DarkGreen
                Else
                    txtTATDStatus.Text = ClsIMS.GetStatusType(cStatusIMSErr)
                    txtTATDStatus.ForeColor = Color.DarkRed
                    txtTATDFileName.ForeColor = Color.DarkRed
                    txtTATDMessage.ForeColor = Color.DarkRed
                End If
                txtTATDMessage.Text = varErrorDesc
                PopulateResults = True
            End If
        End If
    End Function

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        ClearResults()
        cboTATDCCY.SelectedValue = -1
        CboTATDInstA.SelectedValue = -1
        CboTATDInstB.SelectedValue = -1
        TATDLoadGrid()
    End Sub

    Private Function ValidMovements() As Boolean
        ValidMovements = True
        If varSingles = vbNo Then
            If dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(1).Value <> dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(3).Value Then
                ValidMovements = False
                MsgBox("Total Amounts do not balance for institution A", vbCritical, "Amounts do not balance")
            ElseIf dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(4).Value <> dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(6).Value Then
                ValidMovements = False
                MsgBox("Total Amounts do not balance for institution B", vbCritical, "Amounts do not balance")
            ElseIf dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(2).Value <> dgvTATD.Rows(dgvTATD.Rows.Count - 1).Cells(5).Value Then
                ValidMovements = False
                MsgBox("Movement Amounts do not balance", vbCritical, "Amounts do not balance")
            End If
        End If
    End Function

    Private Sub FrmTreasuryAllocation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateFormCombo(cboTATDCCY)
        cboTATDCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboTATDCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateFormCombo(CboTATDInstA)
        CboTATDInstA.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboTATDInstA.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvTATD, 7)
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvTATD_KeyPress(sender As Object, e As KeyPressEventArgs) Handles dgvTATD.KeyPress
        Dim Characters As String = ChrW(Keys.Tab)
        Dim Characters2 As String = ChrW(Keys.Enter)

        If InStr(Characters, e.KeyChar) = 1 Then
            If dgvTATD.CurrentRow.Index = dgvTATD.Rows.Count - 2 Then
                dgvTATD.CurrentCell = dgvTATD.Rows(0).Cells(2)
            Else
                dgvTATD.CurrentCell = dgvTATD.Rows(dgvTATD.CurrentRow.Index + 1).Cells(2)
            End If
        ElseIf InStr(Characters2, e.KeyChar) = 1 Then
            If dgvTATD.CurrentRow.Index = dgvTATD.Rows.Count - 1 Then
                dgvTATD.CurrentCell = dgvTATD.Rows(0).Cells(2)
            Else
                dgvTATD.CurrentCell = dgvTATD.Rows(dgvTATD.CurrentRow.Index).Cells(2)
            End If
        End If
    End Sub

    Private Sub dgvTATD_SelectionChanged(sender As Object, e As EventArgs) Handles dgvTATD.SelectionChanged
        If CompletedCellEdit Then
            MoveCell()
        End If
        dgvTATD.Columns(3).Selected = False
    End Sub

    Private Sub MoveCell()
        CompletedCellEdit = False
        If dgvTATD.CurrentRow.Index = dgvTATD.Rows.Count - 1 Then
            dgvTATD.CurrentCell = dgvTATD.Rows(0).Cells(2)
        Else
            dgvTATD.CurrentCell = dgvTATD.Rows(dgvTATD.CurrentRow.Index).Cells(2)
        End If
    End Sub

    Private Sub TimerGetResults_Tick(sender As Object, e As EventArgs) Handles TimerGetResults.Tick
        Dim varErrors As Integer = 0
        Dim varErrorDesc As String = ""

        Cursor = Cursors.WaitCursor
        If PopulateResults(varErrors, varErrorDesc) Then
            TimerGetResults.Enabled = False
            If varErrors <> 0 Then
                MsgBox("There has been a problem inporting the transactions into IFT" & vbNewLine & "It is possible some of the transactions were successful" & vbNewLine & vbNewLine & "Please contact systems support with the filename: " & txtTATDFileName.Text, MsgBoxStyle.Critical)
            Else
                TATDLoadGrid()
            End If
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub CboTATDInstA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboTATDInstA.SelectedIndexChanged
        Dim WhereClause As String = ""

        If IsNumeric(CboTATDInstA.SelectedValue) Then
            If CboTATDInstA.SelectedValue = 287 Then
                WhereClause = "where branchCodeA = " & ClsIMS.UserLocationCode & " and BrokerCodeA = 426"
            ElseIf CboTATDInstA.SelectedValue = 426 Then
                WhereClause = "where branchCodeA = " & ClsIMS.UserLocationCode & " and BrokerCodeA = 287"
            ElseIf CboTATDInstA.SelectedValue = 338 Then
                WhereClause = "where branchCodeA = " & ClsIMS.UserLocationCode & " and BrokerCodeA in (222,425)"
            ElseIf CboTATDInstA.SelectedValue = 222 Then
                WhereClause = "where branchCodeA = " & ClsIMS.UserLocationCode & " and BrokerCodeA in (338,425)"
            ElseIf CboTATDInstA.SelectedValue = 425 Then
                WhereClause = "where branchCodeA = " & ClsIMS.UserLocationCode & " and BrokerCodeA in (222,338)"
            End If

            'ClsIMS.PopulateComboboxWithData(CboTATDInstB, "select BrokerCodeA,BrokerA from vwDolfinPaymentSelectAccountTreasuryTermDeposits " & WhereClause & " group by BrokerCodeA,BrokerA order by BrokerA")
            ClsIMS.PopulateComboboxWithData(CboTATDInstB, "select BrokerCodeA,BrokerA from dbo.vwTermDepositBrokers group by BrokerCodeA,BrokerA order by BrokerA")
            CboTATDInstB.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            CboTATDInstB.AutoCompleteSource = AutoCompleteSource.ListItems
        End If
    End Sub

    Private Sub dtTransDate_ValueChanged(sender As Object, e As EventArgs) Handles dtTransDate.ValueChanged
        RunGrid()
    End Sub

    Private Sub FrmTreasuryAllocationTermDeposits_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        ClsPayTreasury.ClearClass()
    End Sub

    Private Sub ChkAllowSingles_CheckedChanged(sender As Object, e As EventArgs) Handles ChkAllowSingles.CheckedChanged
        If ChkAllowSingles.Checked Then
            varSingles = MsgBox("Are you sure you want to allow one directional moves?", vbYesNo + vbQuestion, "Are you sure?")
        Else
            varSingles = vbNo
        End If
        If varSingles = vbNo Then
            ChkAllowSingles.Checked = False
        End If
    End Sub
End Class