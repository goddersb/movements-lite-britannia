﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFeeSchedules
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmFeeSchedules))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.dgvFS = New System.Windows.Forms.DataGridView()
        Me.lblfeeTitle = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.CmdNewFS = New System.Windows.Forms.Button()
        Me.cboFSPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.ChkIsEAM = New System.Windows.Forms.CheckBox()
        Me.ChkTransfers = New System.Windows.Forms.CheckBox()
        Me.ChkMGT = New System.Windows.Forms.CheckBox()
        Me.ChkNoTemplate = New System.Windows.Forms.CheckBox()
        Me.ChkCustody = New System.Windows.Forms.CheckBox()
        Me.ChkInvVisa = New System.Windows.Forms.CheckBox()
        Me.ChkTransactions = New System.Windows.Forms.CheckBox()
        Me.ChkTemplates = New System.Windows.Forms.CheckBox()
        Me.ChkActivity = New System.Windows.Forms.CheckBox()
        Me.cboFSDestination = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboFSTemplates = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cmdResetFilter = New System.Windows.Forms.Button()
        Me.cboFSFeeStatus = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboFSRM2 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.cboFSRM1 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvFS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmdRefresh)
        Me.GroupBox1.Controls.Add(Me.dgvFS)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 161)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1617, 700)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Fees"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Maroon
        Me.Label2.Location = New System.Drawing.Point(6, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(293, 13)
        Me.Label2.TabIndex = 38
        Me.Label2.Text = "Double click on row divider to action fee schedules available"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Location = New System.Drawing.Point(1496, 13)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(115, 29)
        Me.cmdRefresh.TabIndex = 36
        Me.cmdRefresh.Text = "Refresh Schedules"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'dgvFS
        '
        Me.dgvFS.AllowUserToAddRows = False
        Me.dgvFS.AllowUserToDeleteRows = False
        Me.dgvFS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFS.Location = New System.Drawing.Point(7, 48)
        Me.dgvFS.MultiSelect = False
        Me.dgvFS.Name = "dgvFS"
        Me.dgvFS.ReadOnly = True
        Me.dgvFS.Size = New System.Drawing.Size(1604, 641)
        Me.dgvFS.TabIndex = 37
        '
        'lblfeeTitle
        '
        Me.lblfeeTitle.AutoSize = True
        Me.lblfeeTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfeeTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblfeeTitle.Location = New System.Drawing.Point(12, 9)
        Me.lblfeeTitle.Name = "lblfeeTitle"
        Me.lblfeeTitle.Size = New System.Drawing.Size(152, 24)
        Me.lblfeeTitle.TabIndex = 9
        Me.lblfeeTitle.Text = "Fee Schedules"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox2.Controls.Add(Me.CmdNewFS)
        Me.GroupBox2.Controls.Add(Me.cboFSPortfolio)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(19, 48)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(662, 91)
        Me.GroupBox2.TabIndex = 36
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Fees Criteria"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(26, 46)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 46
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'CmdNewFS
        '
        Me.CmdNewFS.Location = New System.Drawing.Point(537, 46)
        Me.CmdNewFS.Name = "CmdNewFS"
        Me.CmdNewFS.Size = New System.Drawing.Size(112, 29)
        Me.CmdNewFS.TabIndex = 35
        Me.CmdNewFS.Text = "New Fee Schedule"
        Me.CmdNewFS.UseVisualStyleBackColor = True
        '
        'cboFSPortfolio
        '
        Me.cboFSPortfolio.FormattingEnabled = True
        Me.cboFSPortfolio.Location = New System.Drawing.Point(71, 19)
        Me.cboFSPortfolio.Name = "cboFSPortfolio"
        Me.cboFSPortfolio.Size = New System.Drawing.Size(578, 21)
        Me.cboFSPortfolio.TabIndex = 31
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Portfolio:"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox3.Controls.Add(Me.GroupBox4)
        Me.GroupBox3.Controls.Add(Me.cboFSDestination)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.cboFSTemplates)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.cmdResetFilter)
        Me.GroupBox3.Controls.Add(Me.cboFSFeeStatus)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.cboFSRM2)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.CmdFilter)
        Me.GroupBox3.Controls.Add(Me.cboFSRM1)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(835, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(801, 143)
        Me.GroupBox3.TabIndex = 37
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Fees Filter Criteria"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.ChkIsEAM)
        Me.GroupBox4.Controls.Add(Me.ChkTransfers)
        Me.GroupBox4.Controls.Add(Me.ChkMGT)
        Me.GroupBox4.Controls.Add(Me.ChkNoTemplate)
        Me.GroupBox4.Controls.Add(Me.ChkCustody)
        Me.GroupBox4.Controls.Add(Me.ChkInvVisa)
        Me.GroupBox4.Controls.Add(Me.ChkTransactions)
        Me.GroupBox4.Controls.Add(Me.ChkTemplates)
        Me.GroupBox4.Controls.Add(Me.ChkActivity)
        Me.GroupBox4.Location = New System.Drawing.Point(15, 101)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(774, 33)
        Me.GroupBox4.TabIndex = 53
        Me.GroupBox4.TabStop = False
        '
        'ChkIsEAM
        '
        Me.ChkIsEAM.AutoSize = True
        Me.ChkIsEAM.Location = New System.Drawing.Point(616, 12)
        Me.ChkIsEAM.Name = "ChkIsEAM"
        Me.ChkIsEAM.Size = New System.Drawing.Size(60, 17)
        Me.ChkIsEAM.TabIndex = 51
        Me.ChkIsEAM.Text = "Is EAM"
        Me.ChkIsEAM.UseVisualStyleBackColor = True
        '
        'ChkTransfers
        '
        Me.ChkTransfers.AutoSize = True
        Me.ChkTransfers.Location = New System.Drawing.Point(230, 12)
        Me.ChkTransfers.Name = "ChkTransfers"
        Me.ChkTransfers.Size = New System.Drawing.Size(70, 17)
        Me.ChkTransfers.TabIndex = 43
        Me.ChkTransfers.Text = "Transfers"
        Me.ChkTransfers.UseVisualStyleBackColor = True
        '
        'ChkMGT
        '
        Me.ChkMGT.AutoSize = True
        Me.ChkMGT.Location = New System.Drawing.Point(9, 12)
        Me.ChkMGT.Name = "ChkMGT"
        Me.ChkMGT.Size = New System.Drawing.Size(50, 17)
        Me.ChkMGT.TabIndex = 40
        Me.ChkMGT.Text = "MGT"
        Me.ChkMGT.UseVisualStyleBackColor = True
        '
        'ChkNoTemplate
        '
        Me.ChkNoTemplate.AutoSize = True
        Me.ChkNoTemplate.Location = New System.Drawing.Point(523, 12)
        Me.ChkNoTemplate.Name = "ChkNoTemplate"
        Me.ChkNoTemplate.Size = New System.Drawing.Size(87, 17)
        Me.ChkNoTemplate.TabIndex = 50
        Me.ChkNoTemplate.Text = "No Template"
        Me.ChkNoTemplate.UseVisualStyleBackColor = True
        '
        'ChkCustody
        '
        Me.ChkCustody.AutoSize = True
        Me.ChkCustody.Location = New System.Drawing.Point(67, 12)
        Me.ChkCustody.Name = "ChkCustody"
        Me.ChkCustody.Size = New System.Drawing.Size(64, 17)
        Me.ChkCustody.TabIndex = 41
        Me.ChkCustody.Text = "Custody"
        Me.ChkCustody.UseVisualStyleBackColor = True
        '
        'ChkInvVisa
        '
        Me.ChkInvVisa.AutoSize = True
        Me.ChkInvVisa.Location = New System.Drawing.Point(453, 12)
        Me.ChkInvVisa.Name = "ChkInvVisa"
        Me.ChkInvVisa.Size = New System.Drawing.Size(64, 17)
        Me.ChkInvVisa.TabIndex = 49
        Me.ChkInvVisa.Text = "Inv Visa"
        Me.ChkInvVisa.UseVisualStyleBackColor = True
        '
        'ChkTransactions
        '
        Me.ChkTransactions.AutoSize = True
        Me.ChkTransactions.Location = New System.Drawing.Point(137, 12)
        Me.ChkTransactions.Name = "ChkTransactions"
        Me.ChkTransactions.Size = New System.Drawing.Size(87, 17)
        Me.ChkTransactions.TabIndex = 42
        Me.ChkTransactions.Text = "Transactions"
        Me.ChkTransactions.UseVisualStyleBackColor = True
        '
        'ChkTemplates
        '
        Me.ChkTemplates.AutoSize = True
        Me.ChkTemplates.Location = New System.Drawing.Point(306, 12)
        Me.ChkTemplates.Name = "ChkTemplates"
        Me.ChkTemplates.Size = New System.Drawing.Size(75, 17)
        Me.ChkTemplates.TabIndex = 44
        Me.ChkTemplates.Text = "Templates"
        Me.ChkTemplates.UseVisualStyleBackColor = True
        '
        'ChkActivity
        '
        Me.ChkActivity.AutoSize = True
        Me.ChkActivity.Checked = True
        Me.ChkActivity.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkActivity.Location = New System.Drawing.Point(387, 12)
        Me.ChkActivity.Name = "ChkActivity"
        Me.ChkActivity.Size = New System.Drawing.Size(60, 17)
        Me.ChkActivity.TabIndex = 45
        Me.ChkActivity.Text = "Activity"
        Me.ChkActivity.UseVisualStyleBackColor = True
        '
        'cboFSDestination
        '
        Me.cboFSDestination.FormattingEnabled = True
        Me.cboFSDestination.Location = New System.Drawing.Point(505, 76)
        Me.cboFSDestination.Name = "cboFSDestination"
        Me.cboFSDestination.Size = New System.Drawing.Size(166, 21)
        Me.cboFSDestination.TabIndex = 51
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(436, 79)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 52
        Me.Label7.Text = "Destination:"
        '
        'cboFSTemplates
        '
        Me.cboFSTemplates.FormattingEnabled = True
        Me.cboFSTemplates.Location = New System.Drawing.Point(77, 49)
        Me.cboFSTemplates.Name = "cboFSTemplates"
        Me.cboFSTemplates.Size = New System.Drawing.Size(380, 21)
        Me.cboFSTemplates.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 48
        Me.Label6.Text = "Templates:"
        '
        'cmdResetFilter
        '
        Me.cmdResetFilter.Location = New System.Drawing.Point(677, 73)
        Me.cmdResetFilter.Name = "cmdResetFilter"
        Me.cmdResetFilter.Size = New System.Drawing.Size(112, 24)
        Me.cmdResetFilter.TabIndex = 46
        Me.cmdResetFilter.Text = "Reset Filter"
        Me.cmdResetFilter.UseVisualStyleBackColor = True
        '
        'cboFSFeeStatus
        '
        Me.cboFSFeeStatus.FormattingEnabled = True
        Me.cboFSFeeStatus.Location = New System.Drawing.Point(77, 22)
        Me.cboFSFeeStatus.Name = "cboFSFeeStatus"
        Me.cboFSFeeStatus.Size = New System.Drawing.Size(380, 21)
        Me.cboFSFeeStatus.TabIndex = 38
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 39
        Me.Label5.Text = "Status:"
        '
        'cboFSRM2
        '
        Me.cboFSRM2.FormattingEnabled = True
        Me.cboFSRM2.Location = New System.Drawing.Point(505, 49)
        Me.cboFSRM2.Name = "cboFSRM2"
        Me.cboFSRM2.Size = New System.Drawing.Size(166, 21)
        Me.cboFSRM2.TabIndex = 36
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(466, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "RM2:"
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(677, 22)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 39)
        Me.CmdFilter.TabIndex = 35
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'cboFSRM1
        '
        Me.cboFSRM1.FormattingEnabled = True
        Me.cboFSRM1.Location = New System.Drawing.Point(505, 22)
        Me.cboFSRM1.Name = "cboFSRM1"
        Me.cboFSRM1.Size = New System.Drawing.Size(166, 21)
        Me.cboFSRM1.TabIndex = 31
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(466, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "RM1:"
        '
        'FrmFeeSchedules
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1648, 862)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblfeeTitle)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFeeSchedules"
        Me.Text = "Fee Schedules"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvFS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblfeeTitle As Label
    Friend WithEvents cmdRefresh As Button
    Friend WithEvents dgvFS As DataGridView
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cboFSPortfolio As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CmdNewFS As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cboFSFeeStatus As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboFSRM2 As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents CmdFilter As Button
    Friend WithEvents cboFSRM1 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ChkActivity As CheckBox
    Friend WithEvents ChkTemplates As CheckBox
    Friend WithEvents ChkTransfers As CheckBox
    Friend WithEvents ChkTransactions As CheckBox
    Friend WithEvents ChkCustody As CheckBox
    Friend WithEvents ChkMGT As CheckBox
    Friend WithEvents cmdResetFilter As Button
    Friend WithEvents cboFSTemplates As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents ChkInvVisa As CheckBox
    Friend WithEvents ChkNoTemplate As CheckBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents cboFSDestination As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents ChkIsEAM As CheckBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
