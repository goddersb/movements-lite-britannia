'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class DolfinPaymentCutOffTime
    Public Property CO_BrokerCode As Integer
    Public Property CO_CCYCode As Integer
    Public Property CO_CutOffTime As String
    Public Property CO_CutOffTimeZone As String
    Public Property CO_UserModifiedBy As String
    Public Property CO_UserModifiedTime As Nullable(Of Date)

End Class
