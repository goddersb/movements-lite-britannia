﻿Imports System.Data.SqlClient

Public Class FrmClientStatementGenerator

    Dim sqlConn As SqlConnection
    Dim dgvdata As SqlDataAdapter
    Dim ds As New DataSet

    Private Sub FrmClientStatementGenerator_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ClsIMS.PopulateComboboxWithData(cboPortfolioName, "Select DISTINCT PF_Code, [Name] from _Dolfin_vRpt_BalanceCheckerResult order by Name")
        cboPortfolioName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPortfolioName.AutoCompleteSource = AutoCompleteSource.ListItems
        dtFromDatePicker.Value = DateAdd(DateInterval.Month, -1, Now())
        dtToDatePicker.Value = ClsIMS.GetSQLItem("select dbo.DolfinPaymentAddBusinessDays(getdate(), -1)")

        dtFromPicker.Value = ClsIMS.GetSQLItem(" select DATEADD(month, DATEDIFF(month, 0, dbo.DolfinPaymentAddBusinessDays(getdate(), -1)), 0)")
        dtToPicker.Value = ClsIMS.GetSQLItem("Select dbo.DolfinPaymentAddBusinessDays(getdate(), -1)")

        dtToDatePickerRefresh.Value = dtToPicker.Value

        ClsIMS.PopulateComboboxWithData(cboPortfolio, "Select PF_Code, portfolio from _Dolfin_PortfolioList where PF_Active = 1 order by portfolio")
        cboPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboStatus, "Select 0,[ActionStatus] From [dbo].[_Dolfin_BalanceCheckLog] cl group by [ActionStatus] order by [ActionStatus]")
        cboStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboStatus.AutoCompleteSource = AutoCompleteSource.ListItems

        ClsIMS.PopulateComboboxWithData(cboFilterLog, "Select 0,'' union Select 1,'Ad-Hoc Only' union Select 2,'Last Quarter Only' union Select 3,'Last Quarter Email clients'")
        cboFilterLog.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboFilterLog.AutoCompleteSource = AutoCompleteSource.ListItems

        DtQtrFrom.Value = ClsIMS.GetSQLItem("Select dbo.DolfinPaymentGetStartQuarterDate(getdate(),-1)")
        DtQtrTo.Value = ClsIMS.GetSQLItem("Select dbo.DolfinPaymentGetEndQuarterDate(getdate(),-1)")

        LoadBalanceCheckerGrid(dtFromDatePicker.Value, dtToDatePicker.Value)

    End Sub

    Private Sub LoadBalanceCheckerGrid(Optional fromDate As DateTime = Nothing, Optional toDate As DateTime = Nothing, Optional whereFilter as String = Nothing)
        Dim varSql As String = ""

        Try
            Cursor = Cursors.WaitCursor

            If fromDate = Nothing
                fromDate = New DateTime(now.Year, now.Month, 1)
                dtFromDatePicker.Value = fromDate
            End If


            If toDate = Nothing
                toDate = New DateTime(now.Year, now.Month, now.Day,23,59,59)
                dtToDatePicker.Value = DateAdd(DateInterval.Day, -1, toDate)
            End If

            If whereFilter <> "" Then
                varSql = $"Select [PF_Code],[Name],[PF_SName1],[PF_ShortCut],[Ccy],[BalFromMovement],[BalFromCashRpt],[BalDiff],[FromDate],[ToDate],[ActionStatus],[ActionedBy],[DateActioned]
                                      FROM [dbo].[_Dolfin_vRpt_BalanceCheckerResult] " & whereFilter & "  AND FromDate >= '" & fromDate.ToString("yyyy-MM-dd") & "' AND ToDate <= '" & toDate.ToString("yyyy-MM-dd") & "' Order By ToDate desc,PF_ShortCut"
            Else
                varSql = $"Select [PF_Code],[Name],[PF_SName1],[PF_ShortCut],[Ccy],[BalFromMovement],[BalFromCashRpt],[BalDiff],[FromDate],[ToDate],[ActionStatus],[ActionedBy],[DateActioned]
                                      FROM [dbo].[_Dolfin_vRpt_BalanceCheckerResult] Where (([ActionStatus] = 'Outstanding' AND FromDate >= '{fromDate:yyyy-MM-dd 00:00:00}' AND ToDate <= '{toDate:yyyy-MM-dd 23:59:59}')
                                        OR ([ActionStatus] = 'Fixed' AND cast(DateActioned as date) = cast(getdate() as date))) Order By ToDate desc,PF_ShortCut"
                'OR ([ActionStatus] = 'Fixed'  AND DateActioned > dateadd(DAY, datediff(day, 0, getdate()-7),0)))                      
            End If


            dgvBalanceChecker.DataSource = Nothing
            dgvBalanceChecker.Columns.Clear
            sqlConn = ClsIMS.GetNewOpenConnection

            Dim btnFix As New DataGridViewButtonColumn
            btnFix.Name = "Refresh"
            btnFix.Text = "Refresh"
            btnFix.UseColumnTextForButtonValue = true
            btnFix.FlatStyle = FlatStyle.Popup
            dgvBalanceChecker.Columns.Add(btnFix)

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "BalanceChecks")
            dgvBalanceChecker.AutoGenerateColumns = True
            dgvBalanceChecker.DataSource = ds.Tables("BalanceChecks")

            dgvBalanceChecker.Columns("PF_Code").Visible = False

            dgvBalanceChecker.Columns("Name").HeaderText = "Portfolio Name"
            dgvBalanceChecker.Columns("Name").Width = 200
            dgvBalanceChecker.Columns("Name").ReadOnly = True

            dgvBalanceChecker.Columns("PF_Shortcut").HeaderText = "Shortcut"
            dgvBalanceChecker.Columns("PF_Shortcut").Width = 100
            dgvBalanceChecker.Columns("PF_Shortcut").ReadOnly = True

            dgvBalanceChecker.Columns("Ccy").HeaderText = "Ccy"
            dgvBalanceChecker.Columns("Ccy").Width = 75
            dgvBalanceChecker.Columns("Ccy").ReadOnly = True

            dgvBalanceChecker.Columns("BalFromMovement").HeaderText = "Balance From Movement"
            dgvBalanceChecker.Columns("BalFromMovement").Width = 150
            dgvBalanceChecker.Columns("BalFromMovement").ReadOnly = True
            dgvBalanceChecker.Columns("BalFromMovement").DefaultCellStyle.Format = "##,0.00"
            dgvBalanceChecker.Columns("BalFromMovement").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgvBalanceChecker.Columns("BalFromCashRpt").HeaderText = "Balance From Cash Report"
            dgvBalanceChecker.Columns("BalFromCashRpt").Width = 150
            dgvBalanceChecker.Columns("BalFromCashRpt").ReadOnly = True
            dgvBalanceChecker.Columns("BalFromCashRpt").DefaultCellStyle.Format = "##,0.00"
            dgvBalanceChecker.Columns("BalFromCashRpt").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgvBalanceChecker.Columns("BalDiff").DefaultCellStyle.Format = "##,0.00"
            dgvBalanceChecker.Columns("BalDiff").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            dgvBalanceChecker.Columns("FromDate").HeaderText = "From Date"
            dgvBalanceChecker.Columns("FromDate").Width = 75
            dgvBalanceChecker.Columns("FromDate").ReadOnly = True

            dgvBalanceChecker.Columns("ToDate").HeaderText = "To Date"
            dgvBalanceChecker.Columns("ToDate").Width = 75
            dgvBalanceChecker.Columns("ToDate").ReadOnly = True

            dgvBalanceChecker.Columns("ActionStatus").HeaderText = "Status"
            dgvBalanceChecker.Columns("ActionStatus").Width = 150
            dgvBalanceChecker.Columns("ActionStatus").ReadOnly = True

            dgvBalanceChecker.Columns("ActionedBy").HeaderText = "Actioned By"
            dgvBalanceChecker.Columns("ActionedBy").Width = 100
            dgvBalanceChecker.Columns("ActionedBy").ReadOnly = True

            dgvBalanceChecker.Columns("DateActioned").HeaderText = "Date Actioned"
            dgvBalanceChecker.Columns("DateActioned").Width = 100
            dgvBalanceChecker.Columns("DateActioned").ReadOnly = True

            dgvBalanceChecker.Refresh()
            Cursor = Cursors.Default

        Catch ex As Exception

            Cursor = Cursors.Default
            dgvBalanceChecker.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - Problem With viewing balance checker results")

        End Try
    End Sub

    Private Sub btnRunBalanceChecker_Click(sender As Object, e As EventArgs) Handles btnRunBalanceChecker.Click

        If Not IsNothing(cboPortfolio.SelectedValue) Then
            Dim pfCode As String = IIf(IsNothing(cboPortfolio.SelectedValue), 0, cboPortfolio.SelectedValue)
            Dim fromDate As DateTime = dtFromPicker.Value
            Dim toDate As DateTime = dtToPicker.Value
            Dim source As String = "ReportGenerator"
            If ChkRefresh.Checked Then
                BalanceRefresh(pfCode, fromDate, toDate)
            End If
            runBalanceChecker(pfCode, fromDate, toDate, source)
        Else
            MsgBox("Please select a portfolio to run the report.", vbInformation, "Select portfolio to continue")
        End If

    End Sub

    Private Sub runBalanceChecker(pfCode As String, fromDate As DateTime, toDate As DateTime, source As String)

        Dim cmd As New SqlCommand("_Dolfin_RptBalanceChecker2", ClsIMS.GetNewOpenConnection)

        Try

            Cursor = Cursors.WaitCursor
            _dgvBalanceCheckResults.DataSource = Nothing

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@PFCode", SqlDbType.Int)
            cmd.Parameters("@PFCode").Value = pfCode
            cmd.Parameters.Add("@FromDate", SqlDbType.DateTime)
            cmd.Parameters("@FromDate").Value = fromDate
            cmd.Parameters.Add("@ToDate", SqlDbType.DateTime)
            cmd.Parameters("@ToDate").Value = toDate
            cmd.Parameters.Add("@Source", SqlDbType.VarChar)
            cmd.Parameters("@Source").Value = source
            cmd.Parameters.Add("@IsReCheck", SqlDbType.Bit)
            cmd.Parameters("@IsReCheck").Value = 0

            cmd.CommandTimeout = 10000
            cmd.ExecuteNonQuery()

            Dim user As String = $"RMSCAPITAL\{Environment.UserName}"

            Dim varSql As String = $"Select [CheckLogID] From [dbo].[_Dolfin_BalanceCheckLog] Where 1 = 1 AND [ActionStatus] = 'Outstanding' AND [PF_Code] = {pfCode} AND [toDate] BETWEEN '{fromDate.ToString("yyyy-MM-dd")}' AND '{toDate.ToString("yyyy-MM-dd 23:59:59")}'"

            If ClsIMS.GetSQLItem(varSql) = "" Then
                RunAndSendReport(pfCode, fromDate, toDate)
            Else
                LoadBalanceCheckResultsGrid(pfCode, fromDate, toDate, user)
                Dim varResponse As Integer = MsgBox("There are incorrect report balances still within this date range for this portfolio (displayed in grid)." & vbNewLine & "Would you still like to send the report to your inbox?", vbYesNo + vbQuestion, "Send Report?")
                If varResponse = vbYes Then
                    RunAndSendReport(pfCode, fromDate, toDate)
                Else
                    MsgBox("Please fix the imbalancing issues, re-run the report and then the report will be sent to your inbox", vbInformation, "Fix Imbalancing issues")
                End If
            End If

        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - Problem running _Dolfin_RptBalanceCheckers")
        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameClientStatementGenerator, $"exec dbo._Dolfin_RptBalanceChecker {pfCode} {fromDate} {toDate} {source}")
        End Try
        Cursor = Cursors.Default
    End Sub

    Public Sub RunAndSendReport(pfCode As String, fromDate As DateTime, toDate As DateTime)
        ClsIMS.AddJobToReportQueue(pfCode, fromDate, toDate, ClsIMS.GetMyEmailAddress(), True)
        MsgBox("Report job has now been queued and will be sent to your email address within the next few minutes", vbInformation, "Email sent")
        ClsIMS.RunSQLJob("SSISDB - Run Single Client Statement")
    End Sub

    Private Sub LoadBalanceCheckResultsGrid(pfCode As String, fromDate As DateTime, toDate As DateTime, user As String)

        Try
            Cursor = Cursors.WaitCursor

            Dim varSql As String = $"Select [CheckLogID], cl.[PF_Code], [PF_ShortCut], [Portfolio], [Ccy], [BalFromMovement], [BalFromCashRpt], [BalDiff], [fromDate], [toDate], [StartTime], [EndTime], [CheckSource], [ActionStatus], [ActionedBy], [DateActioned]
                                      From [dbo].[_Dolfin_BalanceCheckLog] cl left join vwDolfinPaymentClientPortfolio p on cl.pf_code = p.pf_code Where 1 = 1 AND [ActionStatus] = 'Outstanding' AND [toDate] BETWEEN '{fromDate.ToString("yyyy-MM-dd")}' AND '{toDate.ToString("yyyy-MM-dd")}' "

            If pfCode <> "" Then
                varSql = varSql & " And cl.[PF_Code] = " & pfCode
            End If

            varSql = varSql & " Order By [toDate] desc, Portfolio"

            dgvBalanceCheckResults.DataSource = Nothing
            sqlConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "BalanceChecks")
            dgvBalanceCheckResults.AutoGenerateColumns = True
            dgvBalanceCheckResults.DataSource = ds.Tables("BalanceChecks")

            dgvBalanceCheckResults.Columns("CheckLogID").Visible = False
            dgvBalanceCheckResults.Columns("PF_Code").Visible = False

            dgvBalanceCheckResults.Columns("PF_Shortcut").HeaderText = "Portfolio"
            dgvBalanceCheckResults.Columns("PF_Shortcut").Width = 100
            dgvBalanceCheckResults.Columns("PF_Shortcut").ReadOnly = True

            dgvBalanceCheckResults.Columns("Ccy").HeaderText = "Ccy"
            dgvBalanceCheckResults.Columns("Ccy").Width = 75
            dgvBalanceCheckResults.Columns("Ccy").ReadOnly = True

            dgvBalanceCheckResults.Columns("BalFromMovement").HeaderText = "Balance From Movement"
            dgvBalanceCheckResults.Columns("BalFromMovement").Width = 150
            dgvBalanceCheckResults.Columns("BalFromMovement").ReadOnly = True
            dgvBalanceCheckResults.Columns("BalFromMovement").DefaultCellStyle.Format = "##,0.00"

            dgvBalanceChecker.Columns("BalFromCashRpt").HeaderText = "Balance From Cash Report"
            dgvBalanceCheckResults.Columns("BalFromCashRpt").Width = 150
            dgvBalanceCheckResults.Columns("BalFromCashRpt").ReadOnly = True
            dgvBalanceCheckResults.Columns("BalFromCashRpt").DefaultCellStyle.Format = "##,0.00"

            dgvBalanceCheckResults.Columns("BalDiff").DefaultCellStyle.Format = "##,0.00"

            dgvBalanceCheckResults.Columns("FromDate").HeaderText = "From Date"
            dgvBalanceCheckResults.Columns("FromDate").Width = 75
            dgvBalanceCheckResults.Columns("FromDate").ReadOnly = True

            dgvBalanceCheckResults.Columns("ToDate").HeaderText = "To Date"
            dgvBalanceCheckResults.Columns("ToDate").Width = 75
            dgvBalanceCheckResults.Columns("ToDate").ReadOnly = True

            dgvBalanceCheckResults.Columns("StartTime").Visible = False
            dgvBalanceCheckResults.Columns("EndTime").Visible = False

            dgvBalanceCheckResults.Columns("CheckSource").HeaderText = "Source"
            dgvBalanceCheckResults.Columns("CheckSource").Width = 100
            dgvBalanceCheckResults.Columns("CheckSource").ReadOnly = True

            dgvBalanceCheckResults.Columns("ActionStatus").HeaderText = "Status"
            dgvBalanceCheckResults.Columns("ActionStatus").Width = 150
            dgvBalanceCheckResults.Columns("ActionStatus").ReadOnly = True

            dgvBalanceCheckResults.Columns("ActionedBy").HeaderText = "Actioned By"
            dgvBalanceCheckResults.Columns("ActionedBy").Width = 100
            dgvBalanceCheckResults.Columns("ActionedBy").ReadOnly = True

            dgvBalanceCheckResults.Columns("DateActioned").HeaderText = "Date Actioned"
            dgvBalanceCheckResults.Columns("DateActioned").Width = 100
            dgvBalanceCheckResults.Columns("DateActioned").ReadOnly = True

            dgvBalanceChecker.Refresh()
            Cursor = Cursors.Default

        Catch ex As Exception

            Cursor = Cursors.Default
            dgvBalanceChecker.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - Problem With viewing balance checker results")

        End Try
    End Sub

    Private Sub LoadQtrLog(ByVal WhereClause As String)

        Try
            Cursor = Cursors.WaitCursor

            Dim varSql As String = $"Select * from vwDolfinPaymentReportScheduleLog "

            If WhereClause <> "" Then
                varSql = varSql & WhereClause
            End If

            varSql = varSql & "  order by RSL_DateAdded Desc"

            dgvBalanceCheckResults.DataSource = Nothing
            sqlConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlDataAdapter(varSql, sqlConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentReportScheduleLog")
            dgvBalanceCheckResults.AutoGenerateColumns = True
            dgvBalanceCheckResults.DataSource = ds.Tables("vwDolfinPaymentReportScheduleLog")

            dgvBalanceChecker.Refresh()
            Cursor = Cursors.Default

        Catch ex As Exception

            Cursor = Cursors.Default
            dgvBalanceChecker.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - Problem With viewing LoadQtrLog")

        End Try
    End Sub

    Private Sub dgvBalanceChecker_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBalanceChecker.CellContentClick

        Dim pfCode As String = ""
        Dim fromDate As String = ""
        Dim toDate As String = ""

        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            pfCode = dgvBalanceChecker.Rows(e.RowIndex).Cells(1).Value
            fromDate = dgvBalanceChecker.Rows(e.RowIndex).Cells(9).Value
            toDate = dgvBalanceChecker.Rows(e.RowIndex).Cells(10).Value
            BalanceRefresh(pfCode, fromDate, toDate)
            LoadBalanceCheckerGrid(dtFromDatePicker.Value, dtToDatePicker.Value, GetBalanceCheckerGridWhereClause())
        End If

    End Sub

    Private Sub BalanceRefresh(ByVal pfCode As String, ByVal fromDate As String, ByVal toDate As String)
        Dim cmd As New SqlCommand("_Dolfin_RptBalanceChecker2", sqlConn)

        Try
            Cursor = Cursors.WaitCursor
            _dgvBalanceCheckResults.DataSource = Nothing

            'cboPortfolioName.SelectedValue = pfCode

            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@PFCode", SqlDbType.Int)
            cmd.Parameters("@PFCode").Value = pfCode
            cmd.Parameters.Add("@FromDate", SqlDbType.DateTime)
            cmd.Parameters("@FromDate").Value = fromDate
            cmd.Parameters.Add("@ToDate", SqlDbType.DateTime)
            cmd.Parameters("@ToDate").Value = toDate
            cmd.Parameters.Add("@Source", SqlDbType.VarChar)
            cmd.Parameters("@Source").Value = "ReportGenerator"
            cmd.Parameters.Add("@IsReCheck", SqlDbType.Bit)
            cmd.Parameters("@IsReCheck").Value = 1

            cmd.CommandTimeout = 10000
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameClientStatementGenerator, Err.Description & " - Problem running BalanceFixer")
        Finally
            If cmd IsNot Nothing Then cmd.Dispose()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameClientStatementGenerator, $"exec BalanceFixer {pfCode} {fromDate} {toDate} ReportGenerator")
        End Try
        Cursor = Cursors.Default
    End Sub

    Private Function GetBalanceCheckerGridWhereClause() As String
        Dim varWhereClauseFilter As String = "where"

        If cboPortfolioName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and Pf_Code = " & Str(cboPortfolioName.SelectedValue)
        End If
        If cboStatus.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ActionStatus = '" & cboStatus.Text & "' "
        End If

        GetBalanceCheckerGridWhereClause = Replace(varWhereClauseFilter, "where and ", "where ")
    End Function

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles cmdFilter.Click
        LoadBalanceCheckerGridWithFilter()
    End Sub

    Public Sub LoadBalanceCheckerGridWithFilter()
        If cboPortfolioName.Text = "" And cboStatus.Text = "" Then
            LoadBalanceCheckerGrid(dtFromDatePicker.Value, dtToDatePicker.Value)
        Else
            LoadBalanceCheckerGrid(dtFromDatePicker.Value, dtToDatePicker.Value, GetBalanceCheckerGridWhereClause())
        End If
    End Sub

    Private Sub CmdGenerateReports_Click(sender As Object, e As EventArgs) Handles CmdGenerateReports.Click
        Dim vbResponse As Integer = vbNo

        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientSendQtr) Then
            LoadBalanceCheckResultsGrid("", DtQtrFrom.Value, DtQtrTo.Value, ClsIMS.FullUserName)

            If dgvBalanceCheckResults.RowCount > 0 Then
                vbResponse = MsgBox("There are incorrect report balances still within this quarter run (displayed in grid)." & vbNewLine & vbNewLine & "The reports will only be generated for items not in the imbalances grid" & vbNewLine & vbNewLine & "Are you sure you want to continue to generate last quarter's client statements? This will also send some sample statements to your inbox", vbQuestion + vbYesNo, "Generate Statements?")
            Else
                vbResponse = MsgBox("Are you sure you want to generate last quarter's client statements? This will also send some sample statements to your inbox", vbQuestion + vbYesNo, "Generate Statements?")
            End If

            If vbResponse = vbYes Then
                ClsIMS.AddQuarterlyJobToReportQueue(0, DtQtrFrom.Value, DtQtrTo.Value, ClsIMS.GetMyEmailAddress(), True)
                ClsIMS.RunSQLJob("SSISDB - Run Quarterly Client Statement")
                MsgBox("The sample reports are now queued and will now be sent to your inbox after the full generation of client statements." & vbNewLine & "1. Please check the sample reports" & vbNewLine & "2. Confirm all client names and email addresses are up-to-date" & vbNewLine & "3. Return to FLOW once you are ready to upload the quarterly reports to MyDolfin", vbInformation, "Generating Reports")

            End If
        Else
            MsgBox("You are not authorised to send quarterly reports", vbCritical, "Cannot send quarterly reports")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientSendQtr, "Refused entry to send quarterly reports")
        End If

    End Sub

    Private Sub CmdSendReports_Click(sender As Object, e As EventArgs) Handles CmdSendReports.Click
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientSendQtr) Then
            Dim vbResponse As Integer = MsgBox("Are you sure you want to activate last quarter's missing client statements in MyDolfin?", vbQuestion + vbYesNo, "Activate Statements?")
            If vbResponse = vbYes Then
                MsgBox("The reports will now be activated in MyDolfin and emails will be sent to clients to let them know their quarterly reports are available." & vbNewLine & "Please check some sample client accounts in MyDolfin to confirm they have been activated", vbInformation, "Activated Quarterly Reports")
                ClsIMS.RunSQLJob("SSISDB - Run Quarterly Client Statement Portal")
            End If
        Else
            MsgBox("You are not authorised to activate quarterly reports", vbCritical, "Cannot activate reports")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientSendQtr, "Refused entry to activate reports")
        End If
    End Sub

    Public Sub RunReportsLog()
        Dim WhereClause As String = "Where "

        If cboFilterLog.SelectedValue = 0 Then
            WhereClause = ""
        ElseIf cboFilterLog.SelectedValue = 1 Then
            WhereClause = WhereClause & "RSL_RfID = 8 "
        ElseIf cboFilterLog.SelectedValue = 2 Then
            WhereClause = WhereClause & "RSL_RfID = 3 and RSL_FromDate = '" & DtQtrFrom.Value.ToString("yyyy-MM-dd") & "' and RSL_ToDate = '" & DtQtrTo.Value.ToString("yyyy-MM-dd") & "' "
        ElseIf cboFilterLog.SelectedValue = 3 Then
            WhereClause = WhereClause & "RSL_RfID = 3 and RSL_FromDate = '" & DtQtrFrom.Value.ToString("yyyy-MM-dd") & "' and RSL_ToDate = '" & DtQtrTo.Value.ToString("yyyy-MM-dd") & "' and RSL_SendEmailToClient = 1"
        End If

        LoadQtrLog(WhereClause)
    End Sub


    Private Sub CmdReportsLog_Click(sender As Object, e As EventArgs) Handles CmdReportsLog.Click
        RunReportsLog()
    End Sub

    Private Sub CmdResetGrid_Click(sender As Object, e As EventArgs) Handles CmdResetGrid.Click
        cboPortfolioName.Text = ""
        cboStatus.Text = ""
        LoadBalanceCheckerGrid()
    End Sub

    Private Sub CmdExportEmailsToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportEmailsToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvBalanceChecker, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdExportStatmentGridToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportStatmentGridToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvBalanceCheckResults, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdGenerateReportsUnSent_Click(sender As Object, e As EventArgs) Handles CmdGenerateReportsUnSent.Click
        Dim vbResponse As Integer = vbNo

        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientSendQtr) Then
            LoadBalanceCheckResultsGrid("", DtQtrFrom.Value, DtQtrTo.Value, ClsIMS.FullUserName)

            If dgvBalanceCheckResults.RowCount > 0 Then
                vbResponse = MsgBox("There are incorrect report balances still within this quarter run (displayed in grid)." & vbNewLine & vbNewLine & "The reports will only be generated for items NOT already generated (Quarterly portfolios not in the log for this quarter)" & vbNewLine & vbNewLine & "Are you sure you want to continue to generate last quarter's missing client statements? This will also send some sample statements to your inbox", vbQuestion + vbYesNo, "Generate Missing Statements?")
            Else
                vbResponse = MsgBox("Are you sure you want to generate last quarter's missing client statements? This will also send some sample statements to your inbox", vbQuestion + vbYesNo, "Generate Missing Statements?")
            End If

            If vbResponse = vbYes Then
                ClsIMS.AddQuarterlyJobToReportQueue(1, DtQtrFrom.Value, DtQtrTo.Value, ClsIMS.GetMyEmailAddress(), True)
                ClsIMS.RunSQLJob("SSISDB - Run Quarterly Client Statement")
                MsgBox("The sample reports are now queued and will now be sent to your inbox after the full generation of client statements." & vbNewLine & "1. Please check the sample reports" & vbNewLine & "2. Confirm all client names and email addresses are up-to-date" & vbNewLine & "3. Return to FLOW once you are ready to upload the quarterly reports to MyDolfin", vbInformation, "Generating Reports")
            End If
        Else
            MsgBox("You are not authorised to send quarterly reports", vbCritical, "Cannot send quarterly reports")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientSendQtr, "Refused entry to send quarterly reports")
        End If
    End Sub

    Private Sub CmdRefreshFix_Click(sender As Object, e As EventArgs) Handles CmdRefreshFix.Click
        For i As Integer = 0 To dgvBalanceChecker.RowCount - 1
            If dgvBalanceChecker.Rows(i).Cells("ToDate").Value = dtToDatePickerRefresh.Value And dgvBalanceChecker.Rows(i).Cells("ActionStatus").Value = "Outstanding" Then
                BalanceRefresh(dgvBalanceChecker.Rows(i).Cells("pf_code").Value, dgvBalanceChecker.Rows(i).Cells("FromDate").Value, dgvBalanceChecker.Rows(i).Cells("ToDate").Value)
            End If
        Next

        LoadBalanceCheckerGridWithFilter()
    End Sub

    Private Sub CmdSendEmail_Click(sender As Object, e As EventArgs) Handles CmdSendEmail.Click
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserClientSendQtr) Then
            cboFilterLog.SelectedValue = 3
            RunReportsLog()

            Dim vbResponse As Integer = MsgBox("Grid is currently showing all the clients that will now be emailed the template." & vbNewLine & "Are you sure you want to send the template email to all the ticked clients in the report's log for this quarter?", vbQuestion + vbYesNo, "Send emails?")

            If vbResponse = vbYes Then
                ClsIMS.RunSQLJob("SSISDB - Run Quarterly Client Statement Portal Emails")
                MsgBox("Email are now being sent", vbInformation, "Sending emails")
            End If
        Else
            MsgBox("You are not authorised to send quarterly reports", vbCritical, "Cannot send quarterly reports")
            ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameClientSendQtr, "Refused entry to send quarterly reports")
        End If
    End Sub


    Private Sub dgvBalanceCheckResults_CellMouseUp(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvBalanceCheckResults.CellMouseUp
        On Error Resume Next
        If e.ColumnIndex = dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_SendEmailToClient").OwningColumn.Index Then
            dgvBalanceCheckResults.EndEdit()
        End If
    End Sub

    Private Sub dgvBalanceCheckResults_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBalanceCheckResults.CellValueChanged
        On Error Resume Next
        If IsNumeric(dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_SendEmailToClient").OwningColumn.Index) Then
            If e.ColumnIndex = dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_SendEmailToClient").OwningColumn.Index Then
                ClsIMS.UpdateSendToEmailTickbox(dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_ID").Value, dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_SendEmailToClient").Value)
            End If
        End If
    End Sub

    Private Sub dgvBalanceCheckResults_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBalanceCheckResults.CellDoubleClick
        On Error Resume Next
        If e.ColumnIndex = dgvBalanceCheckResults.Rows(e.RowIndex).Cells("RSL_SendEmailToClient").OwningColumn.Index Then
            dgvBalanceCheckResults.EndEdit()
        End If
    End Sub
End Class