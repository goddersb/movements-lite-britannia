﻿Public Class ClsFeeSchedule
    Private varSelectedAuthoriseOption As Integer = 0
    Private varID As Double = 0
    Private varPortfolioCode As Double = 0
    Private varPortfolioName As String = ""
    Private varCCYCode As Double = 0
    Private varCCYName As String = ""
    Private varStartDate As Date = Now
    Private varEndDate As Date = DateAdd(DateInterval.Day, +1, Now)
    Private varDestinationID As Double = 1
    Private varDestination As String = ""
    Private varPayFreqID As Double = 1
    Private varPayFreq As String = ""
    Private varMGTID As Double = 0
    Private varCFID As Double = 0
    Private varTFID As Double = 0
    Private varTSID As Double = 0
    Private varClientName As String = ""
    Private varPortfolioShortcut As String = ""
    Private varRM1 As String = ""
    Private varRM2 As String = ""
    Private varFilePath As String = ""
    Private varIsTemplate As Boolean = False
    Private varTemplateName As String = ""
    Private varTemplateLinkedID As String = ""
    Private varTemplateLinkedName As String = ""
    Private varIsEAM As Boolean = False
    Private varPortfolioCodeRedirect As Double = 0


    Property SelectedAuthoriseOption() As Integer
        Get
            Return varSelectedAuthoriseOption
        End Get
        Set(value As Integer)
            varSelectedAuthoriseOption = value
        End Set
    End Property

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property

    Property StartDate() As Date
        Get
            Return varStartDate
        End Get
        Set(value As Date)
            varStartDate = value
        End Set
    End Property

    Property EndDate() As Date
        Get
            Return varEndDate
        End Get
        Set(value As Date)
            varEndDate = value
        End Set
    End Property

    Property DestinationID() As Double
        Get
            Return varDestinationID
        End Get
        Set(value As Double)
            varDestinationID = value
        End Set
    End Property

    Property Destination() As String
        Get
            Return varDestination
        End Get
        Set(value As String)
            varDestination = value
        End Set
    End Property

    Property PayFreqID() As Double
        Get
            Return varPayFreqID
        End Get
        Set(value As Double)
            varPayFreqID = value
        End Set
    End Property

    Property PayFreq() As String
        Get
            Return varPayFreq
        End Get
        Set(value As String)
            varPayFreq = value
        End Set
    End Property

    Property MGTID() As Double
        Get
            Return varMGTID
        End Get
        Set(value As Double)
            varMGTID = value
        End Set
    End Property

    Property CFID() As Double
        Get
            Return varCFID
        End Get
        Set(value As Double)
            varCFID = value
        End Set
    End Property

    Property TFID() As Double
        Get
            Return varTFID
        End Get
        Set(value As Double)
            varTFID = value
        End Set
    End Property

    Property TSID() As Double
        Get
            Return varTSID
        End Get
        Set(value As Double)
            varTSID = value
        End Set
    End Property

    Property ClientName() As String
        Get
            Return varClientName
        End Get
        Set(value As String)
            varClientName = value
        End Set
    End Property

    Property PortfolioShortcut() As String
        Get
            Return varPortfolioShortcut
        End Get
        Set(value As String)
            varPortfolioShortcut = value
        End Set
    End Property

    Property RM1() As String
        Get
            Return varRM1
        End Get
        Set(value As String)
            varRM1 = value
        End Set
    End Property

    Property RM2() As String
        Get
            Return varRM2
        End Get
        Set(value As String)
            varRM2 = value
        End Set
    End Property

    Property FilePath() As String
        Get
            Return varFilePath
        End Get
        Set(value As String)
            varFilePath = value
        End Set
    End Property


    Property IsTemplate() As Boolean
        Get
            Return varIsTemplate
        End Get
        Set(value As Boolean)
            varIsTemplate = value
        End Set
    End Property

    Property TemplateName() As String
        Get
            Return varTemplateName
        End Get
        Set(value As String)
            varTemplateName = value
        End Set
    End Property

    Property TemplateLinkedID() As Double
        Get
            Return varTemplateLinkedID
        End Get
        Set(value As Double)
            varTemplateLinkedID = value
        End Set
    End Property

    Property TemplateLinkedName() As String
        Get
            Return varTemplateLinkedName
        End Get
        Set(value As String)
            varTemplateLinkedName = value
        End Set
    End Property

    Property IsEAM() As Boolean
        Get
            Return varIsEAM
        End Get
        Set(value As Boolean)
            varIsEAM = value
        End Set
    End Property

    Property PortfolioCodeRedirect() As Double
        Get
            Return varPortfolioCodeRedirect
        End Get
        Set(value As Double)
            varPortfolioCodeRedirect = value
        End Set
    End Property

    Public Sub ConfirmDirectory()
        Try
            If Not varIsTemplate Then
                varFilePath = ClsIMS.GetSQLFunction("select dbo.DolfinPaymentGetFeeSchedulePath (" & varPortfolioCode & "," & varCCYCode & ") ")

                If Not System.IO.Directory.Exists(varFilePath) Then
                    System.IO.Directory.CreateDirectory(varFilePath)
                End If
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - clsfeeschedule.ConfirmDirectory: ")
        End Try
    End Sub
End Class
