﻿Public Class ClsMgtFee
    Private varID As Double = 0
    Private varChargeableCCYCode As Double = 0
    Private varChargeableCCYName As String = ""
    Private varTypeID As Double = 0
    Private varCCYEqvCode As Double = 0
    Private varMinCharge As Double = 0
    Private varMaxCharge As Double = 0
    Private varMinMaintCharge As Double = 0
    Private varTemplateID As Double = 0
    Private varTemplateName As String = ""

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property ChargeableCCYCode() As Double
        Get
            Return varChargeableCCYCode
        End Get
        Set(value As Double)
            varChargeableCCYCode = value
        End Set
    End Property

    Property ChargeableCCYName() As String
        Get
            Return varChargeableCCYName
        End Get
        Set(value As String)
            varChargeableCCYName = value
        End Set
    End Property

    Property TypeID() As Double
        Get
            Return varTypeID
        End Get
        Set(value As Double)
            varTypeID = value
        End Set
    End Property

    Property CCYEqvCode() As Double
        Get
            Return varCCYEqvCode
        End Get
        Set(value As Double)
            varCCYEqvCode = value
        End Set
    End Property

    Property MinCharge() As Double
        Get
            Return varMinCharge
        End Get
        Set(value As Double)
            varMinCharge = value
        End Set
    End Property

    Property MaxCharge() As Double
        Get
            Return varMaxCharge
        End Get
        Set(value As Double)
            varMaxCharge = value
        End Set
    End Property

    Property MinMaintCharge() As Double
        Get
            Return varMinMaintCharge
        End Get
        Set(value As Double)
            varMinMaintCharge = value
        End Set
    End Property

    Property TemplateID() As Double
        Get
            Return varTemplateID
        End Get
        Set(value As Double)
            varTemplateID = value
        End Set
    End Property

    Property TemplateName() As String
        Get
            Return varTemplateName
        End Get
        Set(value As String)
            varTemplateName = value
        End Set
    End Property
End Class
