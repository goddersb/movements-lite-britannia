﻿Public Class FrmRec
    Dim RecConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dsRec As New DataSet
    Dim dgvView As DataView

    Public Sub RunRec()
        Try
            Dim varSQL As String = "exec DolfinPaymentBankIMSRecRange '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "','" & dtToDate.Value.ToString("yyyy-MM-dd") & "'," & IIf(ChkReCalcRec.Checked, 1, 0) & ",'" & ClsIMS.FullUserName & "'"

            Cursor = Cursors.WaitCursor
            dgvRec.DataSource = Nothing
            RecConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, RecConn)
            dsRec = New DataSet
            dgvdata.SelectCommand.CommandTimeout = 1000
            dgvdata.Fill(dsRec, "TblRec")

            dgvRec.AutoGenerateColumns = True
            dgvRec.DataSource = dsRec.Tables("TblRec")

            dgvView = New DataView(dsRec.Tables("TblRec"))

            FormatGrid()

            For Each column In dgvRec.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
                If column.name = "Notes" Then
                    column.DefaultCellStyle.backColor = Color.LightYellow
                End If
            Next

            dgvRec.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvRec.Refresh()
            PopulateFilters()
            ClearFilter()
            LblTotals.Text = "Total Rows: " & dgvRec.Rows.Count
            Cursor = Cursors.Default
            GroupBoxFilter.Visible = True

        Catch ex As Exception
            dgvRec.DataSource = Nothing
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub FormatGrid()
        dgvRec.Columns("ID").Width = 50
        dgvRec.Columns("MatchStatus").Width = 100
        dgvRec.Columns("MatchStatusDesc").Width = 150
        dgvRec.Columns("BankBroker").Width = 80
        dgvRec.Columns("IMSBroker").Width = 80
        dgvRec.Columns("BankCCY").Width = 50
        dgvRec.Columns("IMSCCY").Width = 50
        dgvRec.Columns("BankQuantity").DefaultCellStyle.Format = "##,0"
        dgvRec.Columns("IMSQuantity").DefaultCellStyle.Format = "##,0"
        dgvRec.Columns("QuantityDiff").DefaultCellStyle.Format = "##,0"
        dgvRec.Columns("BankAmount").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("IMSAmount").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("AmountDiff").DefaultCellStyle.Format = "##,0.00"
        dgvRec.Columns("Notes").Width = 500
    End Sub

    Private Sub cmdRunRec_Click(sender As Object, e As EventArgs) Handles cmdRunRec.Click
        RunRec()
    End Sub

    Private Sub FrmRec_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvRec, True)
        dtFromDate.Value = ClsIMS.GetSQLFunction("select dbo.dolfinpaymentaddbusinessdays(getdate(),-1)")
        dtToDate.Value = dtFromDate.Value
    End Sub

    Private Sub PopulateFilters()
        ClsIMS.PopulateComboboxWithData(cboFilterStatus, "Select 0,MatchStatus from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by MatchStatus order by MatchStatus")
        ClsIMS.PopulateComboboxWithData(cboFilterDate, "Select 0,Rec_RunDate from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by Rec_RunDate order by Rec_RunDate")
        ClsIMS.PopulateComboboxWithData(cboFilterStatusDesc, "Select 0,MatchStatusDesc from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by MatchStatusDesc order by MatchStatusDesc")
        ClsIMS.PopulateComboboxWithData(cboFilterBroker, "Select 0,BankBroker from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by BankBroker union Select 0,IMSBroker from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by IMSBroker order by BankBroker")
        ClsIMS.PopulateComboboxWithData(cboFilterCCY, "Select 0,BankCCY from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by BankCCY union Select 0,IMSCCY from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by IMSCCY order by BankCCY")
        ClsIMS.PopulateComboboxWithData(cboFilterAccountNo, "Select 0,BankAccountNo from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by BankAccountNo union Select 0,IMSAccountNo from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by IMSAccountNo order by BankAccountNo")
        ClsIMS.PopulateComboboxWithData(cboFilterAccountName, "Select 0,BankAccountName from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by BankAccountName union Select 0,IMSAccountName from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by IMSAccountName order by BankAccountName")
        ClsIMS.PopulateComboboxWithData(cboFilterISIN, "Select 0,BankISIN from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by BankISIN union Select 0,IMSISIN from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' group by IMSISIN order by BankISIN")
        ClsIMS.PopulateComboboxWithData(cboFilterBankType, "Select 0,BankType from vwDolfinPaymentBankIMSRec where Banksd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' and isnull(BankType,'')<>'' group by BankType order by BankType")
        ClsIMS.PopulateComboboxWithData(cboFilterIMSType, "Select 0,IMSType from vwDolfinPaymentBankIMSRec where IMSsd between '" & dtFromDate.Value.ToString("yyyy-MM-dd") & "' And '" & dtToDate.Value.ToString("yyyy-MM-dd") & "' and isnull(IMSType,'')<>'' group by IMSType order by IMSType")
    End Sub

    Private Sub dgvRec_CellMouseDown(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRec.CellMouseDown
        If e.Button = Windows.Forms.MouseButtons.Right Then
            'Dim ht As DataGridView.HitTestInfo
            'ht = Me.dgvRec.HitTest(e.X, e.Y)
            'If ht.Type = DataGridViewHitTestType.RowHeader Then
            mnuSelect.Show(dgvRec, e.X, e.Y)
            'dgvRec.ContextMenuStrip = mnuSelect
            'mnuSelect.Items(0).Text = "This is row " + ht.RowIndex.ToString()
            'End If
        ElseIf e.RowIndex = -1 And e.ColumnIndex > -1 Then
            dgvRec.Sort(dgvRec.Columns(e.ColumnIndex), 0)
        End If
    End Sub

    Public Function CheckMatch() As Integer
        'Dim varBankBrokerA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("BankBroker").Value), "", dgvRec.SelectedRows(0).Cells("BankBroker").Value)
        'Dim varBankBrokerB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("BankBroker").Value), "", dgvRec.SelectedRows(1).Cells("BankBroker").Value)
        'Dim varIMSBrokerA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("IMSBroker").Value), "", dgvRec.SelectedRows(0).Cells("IMSBroker").Value)
        'Dim varIMSBrokerB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("IMSBroker").Value), "", dgvRec.SelectedRows(1).Cells("IMSBroker").Value)
        'Dim varBankCCYA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("BankCCY").Value), "", dgvRec.SelectedRows(0).Cells("BankCCY").Value)
        'Dim varBankCCYB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("BankCCY").Value), "", dgvRec.SelectedRows(1).Cells("BankCCY").Value)
        'Dim varIMSCCYA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("IMSCCY").Value), "", dgvRec.SelectedRows(0).Cells("IMSCCY").Value)
        'Dim varIMSCCYB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("IMSCCY").Value), "", dgvRec.SelectedRows(1).Cells("IMSCCY").Value)
        'Dim varBankAccA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("BankAccountNo").Value), "", dgvRec.SelectedRows(0).Cells("BankAccountNo").Value)
        'Dim varBankAccB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("BankAccountNo").Value), "", dgvRec.SelectedRows(1).Cells("BankAccountNo").Value)
        'Dim varIMSAccA As String = IIf(IsDBNull(dgvRec.SelectedRows(0).Cells("IMSAccountNo").Value), "", dgvRec.SelectedRows(0).Cells("IMSAccountNo").Value)
        'Dim varIMSAccB As String = IIf(IsDBNull(dgvRec.SelectedRows(1).Cells("IMSAccountNo").Value), "", dgvRec.SelectedRows(1).Cells("IMSAccountNo").Value)
        ''
        'If varBankBrokerA <> varIMSBrokerB Or varBankBrokerB <> varIMSBrokerA Then
        ' varResponse = MsgBox("The brokers do not match. Are you sure you want to continue?", vbQuestion + vbYesNo, "Are you sure?")
        'End If
        'If varResponse = vbYes And varBankCCYA <> varIMSCCYB Or varBankCCYB <> varIMSCCYA Then
        ' varResponse = MsgBox("The Currencies do not match. Are you sure you want to continue?", vbQuestion + vbYesNo, "Are you sure?")
        'End If
        'If varResponse = vbYes And varBankAccA <> varIMSAccB Or varBankAccB <> varIMSAccA Then
        'varResponse = MsgBox("The Accounts do not match. Are you sure you want to continue?", vbQuestion + vbYesNo, "Are you sure?")
        'End If
        CheckMatch = vbYes
    End Function

    Private Function PopulateList() As DataTable
        On Error Resume Next
        Dim TblList As New DataTable

        AddDataColumn(TblList, "Num_List", "System.Int32")
        AddDataColumn(TblList, "Text_List", "System.String")

        Dim varRow As DataRow

        For i As Integer = 0 To dgvRec.SelectedRows.Count - 1
            varRow = TblList.NewRow()
            If Not IsDBNull(dgvRec.SelectedRows(i).Cells("SwiftReadID").Value) Then
                If dgvRec.SelectedRows(i).Cells("SwiftReadID").Value <> 0 Then
                    varRow("Num_List") = dgvRec.SelectedRows(i).Cells("SwiftReadID").Value
                ElseIf dgvRec.SelectedRows(i).Cells("IMSIDCode").Value <> 0 Then
                    varRow("Num_List") = dgvRec.SelectedRows(i).Cells("IMSIDCode").Value
                End If
            ElseIf Not IsDBNull(dgvRec.SelectedRows(i).Cells("IMSIDCode").Value) Then
                If dgvRec.SelectedRows(i).Cells("IMSIDCode").Value <> 0 Then
                    varRow("Num_List") = dgvRec.SelectedRows(i).Cells("IMSIDCode").Value
                End If
            End If
            varRow("Text_List") = ""
            TblList.Rows.Add(varRow)
        Next
        PopulateList = TblList
    End Function

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvRec, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub dgvRec_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvRec.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvRec, e.RowIndex)
        End If
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varFilterStr As String = ""

        If cboFilterStatus.Text <> "" Then
            varFilterStr = varFilterStr & " And MatchStatus = '" & cboFilterStatus.Text & "'"
        End If
        If cboFilterDate.Text <> "" Then
            varFilterStr = varFilterStr & " And Rec_Rundate = '" & CDate(cboFilterDate.Text).ToString("yyyy-MM-dd") & "'"
        End If
        If cboFilterStatusDesc.Text <> "" Then
            varFilterStr = varFilterStr & " And MatchStatusDesc = '" & cboFilterStatusDesc.Text & "'"
        End If
        If cboFilterBroker.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankBroker = '" & cboFilterBroker.Text & "' or IMSBroker = '" & cboFilterBroker.Text & "')"
        End If
        If cboFilterCCY.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankCCY = '" & cboFilterCCY.Text & "' or IMSCCY = '" & cboFilterCCY.Text & "')"
        End If
        If cboFilterAccountNo.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankAccountNo = '" & cboFilterAccountNo.Text & "' or IMSAccountNo = '" & cboFilterAccountNo.Text & "')"
        End If
        If cboFilterAccountName.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankAccountName = '" & cboFilterAccountName.Text & "' or IMSAccountName = '" & cboFilterAccountName.Text & "')"
        End If
        If cboFilterISIN.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankCCY = '" & cboFilterISIN.Text & "' or IMSCCY = '" & cboFilterISIN.Text & "')"
        End If

        If cboFilterIMSType.Text <> "" And cboFilterBankType.Text <> "" Then
            varFilterStr = varFilterStr & " and (BankType = '" & cboFilterBankType.Text & "' or IMSType = '" & cboFilterIMSType.Text & "')"
        ElseIf cboFilterBankType.Text <> "" Then
            varFilterStr = varFilterStr & " and BankType = '" & cboFilterBankType.Text & "'"
        ElseIf cboFilterIMSType.Text <> "" Then
            varFilterStr = varFilterStr & " and IMSType = '" & cboFilterIMSType.Text & "'"
        End If



        If varFilterStr <> "" Then
            dgvView = New DataView(dsRec.Tables("TblRec"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "OrderColumn Asc", DataViewRowState.CurrentRows)
            dgvRec.DataSource = dgvView
        Else
            dgvView = New DataView(dsRec.Tables("TblRec"), "", "OrderColumn Asc", DataViewRowState.CurrentRows)
            dgvRec.DataSource = dgvView
        End If
        LblTotals.Text = "Total Rows: " & dgvRec.Rows.Count
    End Sub

    Private Sub cmdClearFilter_Click(sender As Object, e As EventArgs) Handles cmdClearFilter.Click
        ClearFilter()
    End Sub

    Private Sub ClearFilter()
        cboFilterStatus.Text = ""
        cboFilterBroker.Text = ""
        cboFilterCCY.Text = ""
        cboFilterAccountNo.Text = ""
        cboFilterAccountName.Text = ""
        cboFilterISIN.Text = ""
        cboFilterBankType.Text = ""
        cboFilterIMSType.Text = ""
        dgvView = New DataView(dsRec.Tables("TblRec"), "", "OrderColumn Asc", DataViewRowState.CurrentRows)
        dgvRec.DataSource = dgvView
    End Sub

    Private Sub dgvRec_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRec.CellEndEdit
        If e.ColumnIndex = dgvRec.Rows(e.RowIndex).Cells("Notes").ColumnIndex Then
            If Not IsDBNull(dgvRec.Rows(e.RowIndex).Cells("Notes").Value) Then
                ClsIMS.ExecuteString(RecConn, "update DolfinPaymentBankIMSReconciliation set rec_notes = '" & Strings.Left(dgvRec.Rows(e.RowIndex).Cells("Notes").Value, 1000) & "' where rec_id = " & dgvRec.Rows(e.RowIndex).Cells("Id").Value)
            End If
        End If
    End Sub

    Private Sub mnuUnMatch_Click(sender As Object, e As EventArgs) Handles mnuUnMatch.Click
        Dim varResponse As Integer = CheckMatch()
        If varResponse = vbYes Then
            Dim TblList As DataTable = Nothing

            TblList = PopulateList()
            ClsIMS.BankIMSRecManualMatch(1, TblList)
            ChkReCalcRec.Checked = True
            RunRec()
        End If
    End Sub

    Private Sub mnuMatch_Click(sender As Object, e As EventArgs) Handles mnuMatch.Click
        Dim varResponse As Integer = CheckMatch()
        If varResponse = vbYes Then
            Dim TblList As DataTable = Nothing

            TblList = PopulateList()
            ClsIMS.BankIMSRecManualMatch(0, TblList)
            ChkReCalcRec.Checked = False
            RunRec()
        End If
    End Sub

    Private Sub dgvRec_CellToolTipTextNeeded(sender As Object, e As DataGridViewCellToolTipTextNeededEventArgs) Handles dgvRec.CellToolTipTextNeeded
        Dim varTotal As Double = 0, varBankAmt As Double = 0, varIMSAmt As Double = 0

        For Each cell As DataGridViewCell In dgvRec.SelectedCells
            If IsNumeric(cell.Value) Then
                If cell.ColumnIndex = dgvRec.Rows(0).Cells("BankAmount").ColumnIndex Then
                    varBankAmt += cell.Value
                ElseIf cell.ColumnIndex = dgvRec.Rows(0).Cells("IMSAmount").ColumnIndex Then
                    varIMSAmt += cell.Value
                End If

                If cell.ColumnIndex = dgvRec.Rows(0).Cells("BankAmount").ColumnIndex Or cell.ColumnIndex = dgvRec.Rows(0).Cells("IMSAmount").ColumnIndex Then
                    varTotal += cell.Value
                End If
            End If
        Next

        If varTotal <> 0 Then
            e.ToolTipText = "Hightlighted cells: Bank: " & varBankAmt.ToString("#,##0.00") & ", IMS: " & varIMSAmt.ToString("#,##0.00") & ", Total: " & varTotal.ToString("#,##0.00") & ", Diff: " & (varBankAmt - varIMSAmt).ToString("#,##0.00")
        End If
    End Sub

    Private Sub dgvRec_RowHeaderMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRec.RowHeaderMouseClick
        If Not IsDBNull(dgvRec.SelectedRows(0).Cells("IDLink").Value) Then
            If dgvRec.SelectedRows(0).Cells("MatchStatus").Value = "Manual Match" Then
                For i As Integer = 0 To dgvRec.Rows.Count - 1
                    If Not IsDBNull(dgvRec.Rows(i).Cells("IDLink").Value) Then
                        If dgvRec.Rows(i).Cells("IDLink").Value = dgvRec.SelectedRows(0).Cells("IDLink").Value Then
                            dgvRec.Rows(i).Selected = True
                        End If
                    End If
                Next
            End If
        End If
    End Sub
End Class