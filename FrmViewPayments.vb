﻿Public Class FrmViewPayments
    Private Sub dgvViewCash_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvViewCash.RowHeaderMouseDoubleClick
        Dim FrmAuth As New FrmPaymentAuthorise
        Dim varPayID As Double
        Try
            If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise) Then
                ClsPayAuth = New ClsPayment
                ClsPayAuth.GetGridValuesintoClassPayment(ClsPayAuth, dgvViewCash)
                FrmAuth.ShowDialog()
                ClsPay = ClsPayAuth
                If ClsPayAuth.SelectedAuthoriseOption = 3 Then
                    varPayID = ClsPay.SelectedPaymentID
                    If NewPaymentForm Is Nothing Then
                        NewPaymentForm = New FrmPayment
                        NewPaymentForm.MdiParent = MdiParent
                        NewPaymentForm.Show()
                        ClsPay.SelectedPaymentID = varPayID
                        ClsPay = ClsPayAuth
                        ClsPay.GetGridValuesintoClass(dgvViewCash)
                        NewPaymentForm.EditPayment()
                    Else
                        ClsPay.SelectedPaymentID = varPayID
                        NewPaymentForm.Activate()
                        NewPaymentForm.EditPayment()
                    End If
                End If
                GridWithWhereClause()
                LoadFilters()
            Else
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameViewMovements, "blocked user from attempting to authorise movements for ID: " & ClsPay.SelectedPaymentID)
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameViewMovements, Err.Description & "- error with attempting authorisation for ID: " & ClsPay.SelectedPaymentID)
        End Try
    End Sub


    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        ClsIMS.RefreshPaymentGrid(dgvViewCash)
        cboViewID.SelectedIndex = -1
        cboViewStatus.SelectedIndex = -1
        cboViewPayType.SelectedIndex = -1
        cboViewPortfolio.SelectedIndex = -1
        cboViewTransDate.SelectedIndex = -1
        cboViewSettleDate.SelectedIndex = -1
        cboViewCCY.SelectedIndex = -1
        cboViewOrderRef.SelectedIndex = -1
        cboViewOrderAccount.SelectedIndex = -1
        cboViewOrderName.SelectedIndex = -1
        cboViewBenRef.SelectedIndex = -1
        cboViewBenAccount.SelectedIndex = -1
        cboViewBenName.SelectedIndex = -1
        cboViewEnteredBy.SelectedIndex = -1
        cboViewAuthorisedBy.SelectedIndex = -1
    End Sub

    Public Sub GridWithWhereClause()
        Dim varWhereClauseFilter As String = ""

        Cursor = Cursors.WaitCursor
        If cboViewID.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_ID in (" & cboViewID.Text & ")"
        End If

        If cboViewStatus.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_Status in ('" & cboViewStatus.Text & "')"
        End If

        If cboViewPayType.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_PaymentType in ('" & cboViewPayType.Text & "')"
        End If

        If cboViewPortfolio.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_Portfolio in ('" & cboViewPortfolio.Text & "')"
        End If

        If cboViewTransDate.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_TransDate in ('" & CDate(cboViewTransDate.Text).ToString("yyyy-MM-dd") & "')"
        End If

        If cboViewSettleDate.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_SettleDate in ('" & CDate(cboViewSettleDate.Text).ToString("yyyy-MM-dd") & "')"
        End If

        If cboViewCCY.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_CCY in ('" & cboViewCCY.Text & "')"
        End If

        If cboViewOrderRef.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_OrderRef in ('" & cboViewOrderRef.Text & "')"
        End If

        If cboViewOrderAccount.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_OrderAccount in ('" & cboViewOrderAccount.Text & "')"
        End If

        If cboViewOrderName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_OrderName in ('" & cboViewOrderName.Text & "')"
        End If

        If cboViewBenRef.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_BenRef in ('" & cboViewBenRef.Text & "')"
        End If

        If cboViewBenAccount.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_BenAccount in ('" & cboViewBenAccount.Text & "')"
        End If

        If cboViewBenName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_BenName in '(" & cboViewBenName.Text & "')"
        End If

        If cboViewEnteredBy.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_Username in ('" & cboViewEnteredBy.Text & "')"
        End If

        If cboViewAuthorisedBy.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and p_AuthorisedUsername in ('" & cboViewAuthorisedBy.Text & "')"
        End If

        If Strings.Left(varWhereClauseFilter, 4) = " and" Then
            ClsIMS.RefreshViewGrid(dgvViewCash, "where " & Strings.Right(varWhereClauseFilter, Strings.Len(varWhereClauseFilter) - 4))
        Else
            ClsIMS.RefreshViewGrid(dgvViewCash)
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        GridWithWhereClause()
        LoadFilters()
    End Sub

    Public Sub FormatCbo()
        cboViewID.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewID.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewStatus.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewStatus.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewPayType.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewPayType.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewTransDate.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewTransDate.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewSettleDate.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewSettleDate.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewOrderRef.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewOrderRef.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewOrderAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewOrderAccount.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewOrderName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewOrderName.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewBenRef.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewBenRef.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewBenAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewBenAccount.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewBenName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewBenName.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewEnteredBy.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewEnteredBy.AutoCompleteSource = AutoCompleteSource.ListItems
        cboViewAuthorisedBy.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboViewAuthorisedBy.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdViewSubmit.Click
        ClsPay.SubmitTransactions(dgvViewCash, cboPayCheck.SelectedValue)
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvViewCash, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub FrmViewPayments_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvViewCash, True)
        TimerViewRefresh.Enabled = True
        ChkViewRefresh.Checked = ClsIMS.UserOptRefresh
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSubmit) Then
            cmdViewSubmit.Visible = True
        Else
            cmdViewSubmit.Visible = False
        End If

        If cboPayCheck.Items.Count = 0 Then
            ClsIMS.PopulatecboPayCheck(cboPayCheck)
        End If
        cboPayCheck.SelectedValue = -1

        RunRefresh()
        ClsPay.FormatViewGrid(dgvViewCash)
        FormatCbo()
    End Sub

    Private Sub RunRefresh()
        Dim SendIMS As Boolean = False, CheckSwiftAck As Boolean = False, CheckSwiftPaid As Boolean = False, CheckPending As Boolean = False

        Cursor = Cursors.WaitCursor
        'ClsIMS.UpdateMovementStatus()
        'ClsIMS.CheckStatusGridView(dgvViewCash, SendIMS, CheckSwiftAck, CheckSwiftPaid, CheckPending)
        'If CheckSwiftAck Or CheckSwiftPaid Then
        'ClsIMS.UpdateSwiftStatus(CheckSwiftAck, CheckSwiftPaid, cboSwiftCheck.SelectedValue)
        'End If

        'If CheckPending Then
        'ClsIMS.UpdatePendingMovements()
        'End If

        GridWithWhereClause()
        LoadFilters()
        Cursor = Cursors.Default
    End Sub

    Public Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub


    Public Sub LoadFilters()
        PopulateCbo(cboViewID, "Select 0,NULL as P_ID union Select 1,P_ID from vwDolfinPaymentGridViewAll group by P_ID order by P_ID")
        PopulateCbo(cboViewStatus, "Select 0,NULL as P_Status union Select 1,P_Status from vwDolfinPaymentGridViewAll group by P_Status order by P_Status")
        PopulateCbo(cboViewPayType, "Select 0,NULL as P_PaymentType union Select 1,P_PaymentType from vwDolfinPaymentGridViewAll group by P_PaymentType order by P_PaymentType")
        PopulateCbo(cboViewPortfolio, "Select 0,NULL as p_Portfolio union Select 1,p_Portfolio from vwDolfinPaymentGridViewAll group by p_Portfolio order by p_Portfolio")
        PopulateCbo(cboViewTransDate, "Select 0,NULL as p_TransDate union Select 1,p_TransDate from vwDolfinPaymentGridViewAll group by p_TransDate order by p_TransDate desc")
        PopulateCbo(cboViewSettleDate, "Select 0,NULL as p_SettleDate union Select 1,p_SettleDate from vwDolfinPaymentGridViewAll group by p_SettleDate order by p_SettleDate desc")
        PopulateCbo(cboViewCCY, "Select 0,NULL as p_CCY union Select 1,p_CCY from vwDolfinPaymentGridViewAll group by p_CCY order by p_CCY")
        PopulateCbo(cboViewOrderRef, "Select 0,NULL as p_OrderRef union Select 1,p_OrderRef from vwDolfinPaymentGridViewAll group by p_OrderRef order by p_OrderRef")
        PopulateCbo(cboViewOrderAccount, "Select 0,NULL as p_OrderAccount union Select 1,p_OrderAccount from vwDolfinPaymentGridViewAll group by p_OrderAccount order by p_OrderAccount")
        PopulateCbo(cboViewOrderName, "Select 0,NULL as P_OrderName union Select 1,P_OrderName from vwDolfinPaymentGridViewAll group by P_OrderName order by P_OrderName")
        PopulateCbo(cboViewBenRef, "Select 0,NULL as p_BenRef union Select 1,p_BenRef from vwDolfinPaymentGridViewAll group by p_BenRef order by p_BenRef")
        PopulateCbo(cboViewBenAccount, "Select 0,NULL as p_BenAccount union Select 1,p_BenAccount from vwDolfinPaymentGridViewAll group by p_BenAccount order by p_BenAccount")
        PopulateCbo(cboViewBenName, "Select 0,NULL as P_BenName union Select 1,P_BenName from vwDolfinPaymentGridViewAll group by P_BenName order by P_BenName")
        PopulateCbo(cboViewEnteredBy, "Select 0,NULL as p_Username union Select 1,p_Username from vwDolfinPaymentGridViewAll group by p_Username order by p_Username")
        PopulateCbo(cboViewAuthorisedBy, "Select 0,NULL as p_AuthorisedUsername union Select 1,p_AuthorisedUsername from vwDolfinPaymentGridViewAll group by p_AuthorisedUsername order by p_AuthorisedUsername")
    End Sub

    Private Sub dgvViewCash_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvViewCash.CellFormatting
        If e.ColumnIndex = 2 Then
            FormatGridStatus(dgvViewCash, e.RowIndex)
        End If
    End Sub

    Private Sub TimerViewRefresh_Tick(sender As Object, e As EventArgs) Handles TimerViewRefresh.Tick
        RunRefresh()
    End Sub

    Private Sub FrmViewPayments_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        TimerViewRefresh.Enabled = False
    End Sub

    Private Sub ChkViewRefresh_CheckedChanged(sender As Object, e As EventArgs) Handles ChkViewRefresh.CheckedChanged
        If ChkViewRefresh.Checked Then
            TimerViewRefresh.Enabled = True
        Else
            TimerViewRefresh.Enabled = False
        End If
    End Sub

    Private Sub FrmViewPayments_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        RunRefresh()
    End Sub

    Private Sub dgvViewCash_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvViewCash.CellMouseClick
        'If dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "SwiftFile" Or dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "IMSFile" Then
        'If IO.File.Exists(ClsIMS.GetFilePath("Buffer") & dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
        'Process.Start(ClsIMS.GetFilePath("Buffer") & dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
        'End If
        'ElseIf dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "PaidReceivedInPayFile" Or dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "PaidReceivedOutPayFile" Then
        ' If IO.File.Exists(ClsIMS.GetFilePath("SWIFTImport") & dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
        'Process.Start(ClsIMS.GetFilePath("SWIFTImport") & dgvViewCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
        'End If
        'End If
    End Sub
End Class