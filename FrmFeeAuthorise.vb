﻿Public Class FrmFeeAuthorise
    Private Sub FrmFeeAuthorise_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPSAuthID.Text = ClsFS.ID
        txtPSAuthPortfolio.Text = ClsFS.PortfolioName
        txtPSAuthCCY.Text = ClsFS.CCYName
        txtPSAuthShortcut.Text = ClsFS.PortfolioShortcut
        ChkMgtFee.Checked = ClsFS.MGTID
        ChkCF.Checked = ClsFS.CFID
        ChkTF.Checked = ClsFS.TFID
        ChkTS.Checked = ClsFS.TSID
        txtClientName.Text = ClsFS.ClientName
        txtRM1.Text = ClsFS.RM1
        txtRM2.Text = ClsFS.RM2
        Rb5.Enabled = False
        If ClsFS.IsTemplate Then
            Rb5.Enabled = True
        End If
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer
        Dim NewTemplateName As String = ""

        ClsFS.SelectedAuthoriseOption = 0
        If Rb1.Checked Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseFeeSchedules) Then
                varResponse = MsgBox("Are you sure you want to authorise this fee schedule?", vbQuestion + vbYesNo, "Authorise fee schedule")
                If varResponse = vbYes Then
                    ClsFS.SelectedAuthoriseOption = 1
                End If
            Else
                varResponse = MsgBox("You are not authorised to authorise this fee schedule", vbExclamation, "No Access")
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to authorise fee schedule")
            End If
        ElseIf Rb2.Checked Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseFeeSchedules) Then
                varResponse = MsgBox("Are you sure you want to unauthorise this fee schedule?", vbQuestion + vbYesNo, "Unauthorise fee schedule")
                If varResponse = vbYes Then
                    ClsFS.SelectedAuthoriseOption = 2
                End If
            Else
                varResponse = MsgBox("You are not authorised to unauthorise this fee schedule", vbExclamation, "No Access")
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to unauthorise fee schedule")
            End If
        ElseIf Rb3.Checked Then
            varResponse = MsgBox("Are you sure you want to edit fee schedule?", vbQuestion + vbYesNo, "Edit fee schedule")
            If varResponse = vbYes Then
                ClsFS.SelectedAuthoriseOption = 3
            End If
        ElseIf Rb4.Checked Then
            If (ClsIMS.GetSQLItem("select top 1 FS_ID from vwDolfinPaymentFeeSchedulesLinks where TemplateLinkedID = " & ClsFS.ID) = "" And ClsFS.IsTemplate) Or Not ClsFS.IsTemplate Then
                If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseFeeSchedules) Then
                    varResponse = MsgBox("Are you sure you want to delete fee schedule?", vbQuestion + vbYesNo, "delete fee schedule")
                    If varResponse = vbYes Then
                        ClsFS.SelectedAuthoriseOption = 4
                    End If
                Else
                    varResponse = MsgBox("You are not authorised to delete this fee schedule", vbExclamation, "No Access")
                    ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Refused entry to delete fee schedule")
                End If
            Else
                varResponse = MsgBox("You cannot delete this template as it has fee schedules linked to it. Delete individual associated fee schedules before deletion of this template", vbExclamation, "Cannot delete template")
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameFees, "Cannot delete linked template: FS_ID:" & ClsFS.ID)
            End If
        ElseIf Rb5.Checked Then
            varResponse = MsgBox("Are you sure you want to copy fee schedule?", vbQuestion + vbYesNo, "copy fee schedule")
            If varResponse = vbYes Then
                NewTemplateName = InputBox("Please write the new name of the template here...", "Enter Name", ClsFS.TemplateName & "_1")
                If NewTemplateName <> "" Then
                    ClsFS.SelectedAuthoriseOption = 5
                End If
            End If
        End If

        If ClsFS.SelectedAuthoriseOption <> 0 Then
            ClsIMS.ActionMovementFS(ClsFS.SelectedAuthoriseOption, ClsFS.ID, IIf(ClsFS.SelectedAuthoriseOption = 5, NewTemplateName, ClsIMS.FullUserName))
        End If

        If varResponse = vbYes Then
            Me.Close()
        End If
    End Sub
End Class