﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFeeAuthorise
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.ChkTS = New System.Windows.Forms.CheckBox()
        Me.txtPSAuthShortcut = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ChkTF = New System.Windows.Forms.CheckBox()
        Me.ChkCF = New System.Windows.Forms.CheckBox()
        Me.ChkMgtFee = New System.Windows.Forms.CheckBox()
        Me.txtRM2 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtRM1 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtClientName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPSAuthPortfolio = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtPSAuthCCY = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.txtPSAuthID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Rb4 = New System.Windows.Forms.RadioButton()
        Me.Rb1 = New System.Windows.Forms.RadioButton()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.Rb3 = New System.Windows.Forms.RadioButton()
        Me.Rb2 = New System.Windows.Forms.RadioButton()
        Me.Rb5 = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CheckBox1)
        Me.GroupBox1.Controls.Add(Me.ChkTS)
        Me.GroupBox1.Controls.Add(Me.txtPSAuthShortcut)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ChkTF)
        Me.GroupBox1.Controls.Add(Me.ChkCF)
        Me.GroupBox1.Controls.Add(Me.ChkMgtFee)
        Me.GroupBox1.Controls.Add(Me.txtRM2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtRM1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtClientName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtPSAuthPortfolio)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.txtPSAuthCCY)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblportfoliofee)
        Me.GroupBox1.Controls.Add(Me.txtPSAuthID)
        Me.GroupBox1.Location = New System.Drawing.Point(20, 37)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(422, 314)
        Me.GroupBox1.TabIndex = 57
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Summary Details"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Enabled = False
        Me.CheckBox1.Location = New System.Drawing.Point(108, 199)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(76, 17)
        Me.CheckBox1.TabIndex = 102
        Me.CheckBox1.Text = "Template?"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'ChkTS
        '
        Me.ChkTS.AutoSize = True
        Me.ChkTS.Enabled = False
        Me.ChkTS.Location = New System.Drawing.Point(108, 176)
        Me.ChkTS.Name = "ChkTS"
        Me.ChkTS.Size = New System.Drawing.Size(92, 17)
        Me.ChkTS.TabIndex = 101
        Me.ChkTS.Text = "Transfer Fee?"
        Me.ChkTS.UseVisualStyleBackColor = True
        '
        'txtPSAuthShortcut
        '
        Me.txtPSAuthShortcut.Enabled = False
        Me.txtPSAuthShortcut.Location = New System.Drawing.Point(293, 28)
        Me.txtPSAuthShortcut.Name = "txtPSAuthShortcut"
        Me.txtPSAuthShortcut.Size = New System.Drawing.Size(106, 20)
        Me.txtPSAuthShortcut.TabIndex = 100
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(252, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 99
        Me.Label1.Text = "Code:"
        '
        'ChkTF
        '
        Me.ChkTF.AutoSize = True
        Me.ChkTF.Enabled = False
        Me.ChkTF.Location = New System.Drawing.Point(108, 153)
        Me.ChkTF.Name = "ChkTF"
        Me.ChkTF.Size = New System.Drawing.Size(109, 17)
        Me.ChkTF.TabIndex = 98
        Me.ChkTF.Text = "Transaction Fee?"
        Me.ChkTF.UseVisualStyleBackColor = True
        '
        'ChkCF
        '
        Me.ChkCF.AutoSize = True
        Me.ChkCF.Enabled = False
        Me.ChkCF.Location = New System.Drawing.Point(108, 130)
        Me.ChkCF.Name = "ChkCF"
        Me.ChkCF.Size = New System.Drawing.Size(91, 17)
        Me.ChkCF.TabIndex = 97
        Me.ChkCF.Text = "Custody Fee?"
        Me.ChkCF.UseVisualStyleBackColor = True
        '
        'ChkMgtFee
        '
        Me.ChkMgtFee.AutoSize = True
        Me.ChkMgtFee.Enabled = False
        Me.ChkMgtFee.Location = New System.Drawing.Point(108, 107)
        Me.ChkMgtFee.Name = "ChkMgtFee"
        Me.ChkMgtFee.Size = New System.Drawing.Size(115, 17)
        Me.ChkMgtFee.TabIndex = 96
        Me.ChkMgtFee.Text = "Management Fee?"
        Me.ChkMgtFee.UseVisualStyleBackColor = True
        '
        'txtRM2
        '
        Me.txtRM2.Enabled = False
        Me.txtRM2.Location = New System.Drawing.Point(108, 278)
        Me.txtRM2.Name = "txtRM2"
        Me.txtRM2.Size = New System.Drawing.Size(291, 20)
        Me.txtRM2.TabIndex = 95
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(66, 278)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 94
        Me.Label6.Text = "RM 2:"
        '
        'txtRM1
        '
        Me.txtRM1.Enabled = False
        Me.txtRM1.Location = New System.Drawing.Point(108, 252)
        Me.txtRM1.Name = "txtRM1"
        Me.txtRM1.Size = New System.Drawing.Size(291, 20)
        Me.txtRM1.TabIndex = 93
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(66, 255)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 92
        Me.Label4.Text = "RM 1:"
        '
        'txtClientName
        '
        Me.txtClientName.Enabled = False
        Me.txtClientName.Location = New System.Drawing.Point(108, 226)
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Size = New System.Drawing.Size(291, 20)
        Me.txtClientName.TabIndex = 91
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 229)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 90
        Me.Label3.Text = "Client Name:"
        '
        'txtPSAuthPortfolio
        '
        Me.txtPSAuthPortfolio.Enabled = False
        Me.txtPSAuthPortfolio.Location = New System.Drawing.Point(108, 54)
        Me.txtPSAuthPortfolio.Name = "txtPSAuthPortfolio"
        Me.txtPSAuthPortfolio.Size = New System.Drawing.Size(291, 20)
        Me.txtPSAuthPortfolio.TabIndex = 86
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(54, 57)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblauthportfolio.TabIndex = 85
        Me.lblauthportfolio.Text = "Portfolio:"
        '
        'txtPSAuthCCY
        '
        Me.txtPSAuthCCY.Enabled = False
        Me.txtPSAuthCCY.Location = New System.Drawing.Point(108, 80)
        Me.txtPSAuthCCY.Name = "txtPSAuthCCY"
        Me.txtPSAuthCCY.Size = New System.Drawing.Size(72, 20)
        Me.txtPSAuthCCY.TabIndex = 84
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(71, 83)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "CCY:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Enabled = False
        Me.lblportfoliofee.Location = New System.Drawing.Point(179, 80)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 76
        '
        'txtPSAuthID
        '
        Me.txtPSAuthID.AcceptsReturn = True
        Me.txtPSAuthID.AcceptsTab = True
        Me.txtPSAuthID.BackColor = System.Drawing.Color.FloralWhite
        Me.txtPSAuthID.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPSAuthID.Enabled = False
        Me.txtPSAuthID.Font = New System.Drawing.Font("Bernard MT Condensed", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPSAuthID.Location = New System.Drawing.Point(86, 19)
        Me.txtPSAuthID.Multiline = True
        Me.txtPSAuthID.Name = "txtPSAuthID"
        Me.txtPSAuthID.Size = New System.Drawing.Size(103, 29)
        Me.txtPSAuthID.TabIndex = 50
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label2.Location = New System.Drawing.Point(102, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(220, 24)
        Me.Label2.TabIndex = 56
        Me.Label2.Text = "Fee Schedule Options"
        '
        'Rb4
        '
        Me.Rb4.AutoSize = True
        Me.Rb4.Location = New System.Drawing.Point(56, 433)
        Me.Rb4.Name = "Rb4"
        Me.Rb4.Size = New System.Drawing.Size(125, 17)
        Me.Rb4.TabIndex = 55
        Me.Rb4.TabStop = True
        Me.Rb4.Text = "Delete Fee Schedule"
        Me.Rb4.UseVisualStyleBackColor = True
        '
        'Rb1
        '
        Me.Rb1.AutoSize = True
        Me.Rb1.Location = New System.Drawing.Point(56, 364)
        Me.Rb1.Name = "Rb1"
        Me.Rb1.Size = New System.Drawing.Size(177, 17)
        Me.Rb1.TabIndex = 54
        Me.Rb1.TabStop = True
        Me.Rb1.Text = "Authorise Fee Schedule (Active)"
        Me.Rb1.UseVisualStyleBackColor = True
        '
        'cmdContinue
        '
        Me.cmdContinue.Location = New System.Drawing.Point(20, 497)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(422, 23)
        Me.cmdContinue.TabIndex = 53
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'Rb3
        '
        Me.Rb3.AutoSize = True
        Me.Rb3.Location = New System.Drawing.Point(56, 410)
        Me.Rb3.Name = "Rb3"
        Me.Rb3.Size = New System.Drawing.Size(112, 17)
        Me.Rb3.TabIndex = 52
        Me.Rb3.TabStop = True
        Me.Rb3.Text = "Edit Fee Schedule"
        Me.Rb3.UseVisualStyleBackColor = True
        '
        'Rb2
        '
        Me.Rb2.AutoSize = True
        Me.Rb2.Location = New System.Drawing.Point(56, 387)
        Me.Rb2.Name = "Rb2"
        Me.Rb2.Size = New System.Drawing.Size(263, 17)
        Me.Rb2.TabIndex = 58
        Me.Rb2.TabStop = True
        Me.Rb2.Text = "Unauthorise Fee Schedule (Pending Authorisation)"
        Me.Rb2.UseVisualStyleBackColor = True
        '
        'Rb5
        '
        Me.Rb5.AutoSize = True
        Me.Rb5.Location = New System.Drawing.Point(56, 456)
        Me.Rb5.Name = "Rb5"
        Me.Rb5.Size = New System.Drawing.Size(118, 17)
        Me.Rb5.TabIndex = 59
        Me.Rb5.TabStop = True
        Me.Rb5.Text = "Copy Fee Schedule"
        Me.Rb5.UseVisualStyleBackColor = True
        '
        'FrmFeeAuthorise
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(459, 547)
        Me.Controls.Add(Me.Rb5)
        Me.Controls.Add(Me.Rb2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Rb4)
        Me.Controls.Add(Me.Rb1)
        Me.Controls.Add(Me.cmdContinue)
        Me.Controls.Add(Me.Rb3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmFeeAuthorise"
        Me.Text = "Authorise Fee Schedule"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtPSAuthPortfolio As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtPSAuthCCY As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents txtPSAuthID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Rb4 As RadioButton
    Friend WithEvents Rb1 As RadioButton
    Friend WithEvents cmdContinue As Button
    Friend WithEvents Rb3 As RadioButton
    Friend WithEvents txtRM2 As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtRM1 As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtClientName As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents ChkTF As CheckBox
    Friend WithEvents ChkCF As CheckBox
    Friend WithEvents ChkMgtFee As CheckBox
    Friend WithEvents Rb2 As RadioButton
    Friend WithEvents txtPSAuthShortcut As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents ChkTS As CheckBox
    Friend WithEvents CheckBox1 As CheckBox
    Friend WithEvents Rb5 As RadioButton
End Class
