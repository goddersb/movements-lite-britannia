﻿Imports System.ComponentModel

Public Class FrmFeesPaid
    Dim FeesPaidConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dsFP As New DataSet
    Dim dsAccounts As New DataSet
    Dim dgvCheckbox As New DataGridViewCheckBoxColumn, dgvCheckboxClientOnly As New DataGridViewCheckBoxColumn
    Dim dgvdataAccounts As New SqlClient.SqlDataAdapter
    Dim cboAccountsColumn As New DataGridViewComboBoxColumn
    Dim AccountCCYRateColumn As New DataGridViewTextBoxColumn
    Dim dv As DataView, dgvView As DataView
    Dim varDept As String

    Private Sub AssignColours()
        For i = 0 To dgvF.RowCount - 1
            FormatGridStatus(dgvF, i, dgvF.Rows(i).Cells("PaidStatus").ColumnIndex)
            If cboFView.SelectedValue = 1 Or cboFView.SelectedValue = 3 Then
                ChangeBalanceColor(i, "Balance", "Payment Amount")
                ChangeBalanceColor(i, "Total Balance Chargeable CCY", "Payment Amount")
                ChangeBalanceColor(i, "Total Balance All CCY", "Payment Amount")
            End If
        Next
    End Sub

    Private Function ValidatedCriteria()
        ValidatedCriteria = True

        If Not IsNumeric(cboFType.SelectedValue) Then
            MsgBox("Please enter the Fees Paid type", vbInformation, "Fee Type Required")
            ValidatedCriteria = False
        ElseIf Not IsNumeric(cboFView.SelectedValue) Then
            MsgBox("Please enter the Fees Paid view", vbInformation, "Fee View Required")
            ValidatedCriteria = False
        ElseIf Not IsNumeric(cboFDateYear.SelectedValue) And cboFView.SelectedValue <> 2 Then
            MsgBox("Please enter the Date type year", vbInformation, "Date Type Year Required")
            ValidatedCriteria = False
        ElseIf Not IsNumeric(cboFDate.SelectedValue) And cboFView.SelectedValue <> 2 Then
            MsgBox("Please enter the Date type", vbInformation, "Date Type Required")
            ValidatedCriteria = False
        ElseIf cboFView.SelectedValue = -1 And Not ClsIMS.CanUser(ClsIMS.FullUserName, "U_PayFeesPaid") Then
            MsgBox("You are not authorised to crystalise fees", vbExclamation, "Not Authoirised")
            ValidatedCriteria = False
        ElseIf cboFView.SelectedValue = -1 Then
            Dim varResponse As Integer = MsgBox("This option will crystalise/re-crystalise the Pending Authorisation fees ONLY for this date range. Are you sure you want to run crystalisation?", vbQuestion + vbYesNo, "Are you sure?")
            If varResponse = vbNo Then
                ValidatedCriteria = False
            End If
        End If
    End Function

    Private Sub AllView()
        dgvF.Columns("PaidStatus").ReadOnly = True
        dgvF.Columns("Balance").ReadOnly = True
        dgvF.Columns("StartDate").ReadOnly = True
        dgvF.Columns("EndDate").ReadOnly = True
        dgvF.Columns("Portfolio").ReadOnly = True
        dgvF.Columns("CCY PF").ReadOnly = True
        dgvF.Columns("CCY Chargeable").ReadOnly = True
        dgvF.Columns("Fee in Charged Curncy").ReadOnly = True
        dgvF.Columns("VAT Amount").Visible = True
        dgvF.Columns("Total Chargeable Amount").ReadOnly = True
        dgvF.Columns("Destination").ReadOnly = True
        dgvF.Columns("PayFreq").ReadOnly = True
        dgvF.Columns("PaidFeePeriod").ReadOnly = True
        dgvF.Columns("PaidYTD").ReadOnly = True
        dgvF.Columns("AveragePortfolioValue").ReadOnly = True
        dgvF.Columns("PortfolioAUM_PF").ReadOnly = True
        dgvF.Columns("PortfolioAUM_PF").HeaderText = "End Period AUM PF"
        dgvF.Columns("PortfolioCash_PF").ReadOnly = True
        dgvF.Columns("PortfolioCash_PF").HeaderText = "End Period Cash PF"
        dgvF.Columns("DaysAUM").ReadOnly = True
        dgvF.Columns("MinimumCharge").ReadOnly = True
        dgvF.Columns("MaximumCharge").ReadOnly = True
        dgvF.Columns("pf_code").Visible = False
        'dgvF.Columns("cr_code").Visible = False
        dgvF.Columns("Fee_ID").Visible = True
        dgvF.Columns("Desk").Visible = False
        dgvF.Columns("BranchCode").Visible = False
    End Sub

    Private Sub FinanceViewEstimated()
        dgvF.Columns("Balance").Visible = False
        dgvF.Columns("Total Balance Chargeable CCY").Visible = False
        dgvF.Columns("Total Balance All CCY").Visible = False
        dgvF.Columns("Client Code").Visible = True
        dgvF.Columns("Customer Name").Visible = True
        dgvF.Columns("Portfolio Type").Visible = True
        dgvF.Columns("Management Type").Visible = True
        If Not dgvF.Columns("VAT %") Is Nothing Then
            dgvF.Columns("VAT %").Visible = True
        End If
        dgvF.Columns("Customer Tax Domicile").Visible = True
        dgvF.Columns("Fee in GBP").Visible = True
        If Not dgvF.Columns("VAT Amount GBP") Is Nothing Then
            dgvF.Columns("VAT Amount GBP").Visible = True
        End If
        dgvF.Columns("GBPRate").Visible = True
        dgvF.Columns("Total Amount in GBP").Visible = True
        dgvF.Columns("EAM").Visible = True
        dgvF.Columns("TemplateLinkedName").Visible = True
        dgvF.Columns("RM1").Visible = True
        dgvF.Columns("RM2").Visible = True
        dgvF.Columns("DefaultAccountCode").Visible = False
        dgvF.Columns("OrderAccountName").Visible = False
        dgvF.Columns("BenAccountName").Visible = False
    End Sub

    Private Sub FinanceViewActual()
        dgvF.Columns("Client Only").Visible = False
        dgvF.Columns("Balance").Visible = False
        dgvF.Columns("Client Code").Visible = True
        dgvF.Columns("Customer Name").Visible = True
        dgvF.Columns("Portfolio Type").Visible = True
        dgvF.Columns("Management Type").Visible = True
        If Not dgvF.Columns("VAT %") Is Nothing Then
            dgvF.Columns("VAT %").Visible = True
        End If
        dgvF.Columns("Customer Tax Domicile").Visible = True
        dgvF.Columns("Fee in GBP").Visible = True
        If Not dgvF.Columns("VAT Amount GBP") Is Nothing Then
            dgvF.Columns("VAT Amount GBP").Visible = True
        End If
        dgvF.Columns("GBPRate").Visible = True
        dgvF.Columns("GBPRateInv").Visible = True
        dgvF.Columns("Payment Amount GBP").Visible = True
        dgvF.Columns("Total Amount in GBP").Visible = True
        dgvF.Columns("EAM").Visible = True
        dgvF.Columns("TemplateLinkedName").Visible = True
        dgvF.Columns("RM1").Visible = True
        dgvF.Columns("RM2").Visible = True
        dgvF.Columns("DefaultAccountCode").Visible = False
        dgvF.Columns("OrderAccountName").Visible = True
        dgvF.Columns("BenAccountName").Visible = True
        dgvF.Columns("pf_code1").Visible = False
        dgvF.Columns("Total Balance Chargeable CCY").Visible = False
        dgvF.Columns("Total Balance All CCY").Visible = False
        dgvF.Columns("Fee_AuthorisedDate").Visible = False
        dgvF.Columns("Fee_AuthorisedBy").Visible = False
        dgvF.Columns("Fee_PaidBy").Visible = False
        dgvF.Columns("Fee_PaidConfirmedDate").Visible = False
        dgvF.Columns("Fee_PaidConfirmedBy").Visible = False
    End Sub

    Private Sub OperationsView()
        dgvF.Columns("Client Only").Width = 40
        dgvF.Columns("Balance").Visible = True
        dgvF.Columns("Total Balance Chargeable CCY").ReadOnly = True
        dgvF.Columns("Total Balance All CCY").ReadOnly = True
        dgvF.Columns("Total Balance All CCY").Frozen = True
        dgvF.Columns("Balance").ReadOnly = True
        dgvF.Columns("Client Code").Visible = False
        dgvF.Columns("Customer Name").Visible = False
        dgvF.Columns("Portfolio Type").Visible = False
        dgvF.Columns("Management Type").Visible = False
        If Not dgvF.Columns("VAT %") Is Nothing Then
            dgvF.Columns("VAT %").Visible = False
        End If
        dgvF.Columns("Customer Tax Domicile").Visible = False
        dgvF.Columns("Fee in GBP").Visible = False
        dgvF.Columns("GBPRate").Visible = False
        If Not dgvF.Columns("VAT Amount GBP") Is Nothing Then
            dgvF.Columns("VAT Amount GBP").Visible = False
        End If
        dgvF.Columns("Total Amount in GBP").Visible = False
        dgvF.Columns("EAM").Visible = False
        dgvF.Columns("TemplateLinkedName").Visible = True
        dgvF.Columns("RM1").Visible = False
        dgvF.Columns("RM2").Visible = False
        dgvF.Columns("DefaultAccountCode").Visible = False
        dgvF.Columns("OrderAccountName").Visible = False
        dgvF.Columns("BenAccountName").Visible = False
        'dgvF.Columns("Account CCY Rate").Visible = True
        dgvF.Columns("pf_code1").Visible = False
    End Sub

    Private Sub CRMView()
        dgvF.Columns("Balance").Visible = False
        dgvF.Columns("Client Code").Visible = True
        dgvF.Columns("Customer Name").Visible = True
        dgvF.Columns("Management Type").Visible = True
        If Not dgvF.Columns("VAT %") Is Nothing Then
            dgvF.Columns("VAT %").Visible = True
        End If
        dgvF.Columns("Customer Tax Domicile").Visible = True
        dgvF.Columns("Fee in GBP").Visible = True
        If Not dgvF.Columns("VAT Amount GBP") Is Nothing Then
            dgvF.Columns("VAT Amount GBP").Visible = True
        End If
        dgvF.Columns("GBPRate").Visible = True
        dgvF.Columns("Total Amount in GBP").Visible = True
        dgvF.Columns("EAM").Visible = True
        dgvF.Columns("TemplateLinkedName").Visible = True
        dgvF.Columns("RM1").Visible = True
        dgvF.Columns("RM2").Visible = True
        dgvF.Columns("DefaultAccountCode").Visible = False
        dgvF.Columns("OrderAccountName").Visible = False
        dgvF.Columns("BenAccountName").Visible = False
        'dgvF.Columns("Account CCY Rate").Visible = False
    End Sub

    Private Sub ColumnWidths()
        dgvF.Columns("PaidStatus").Width = 120
        dgvF.Columns("StartDate").Width = 80
        dgvF.Columns("EndDate").Width = 80
        dgvF.Columns("Portfolio").Width = 350
        dgvF.Columns("CCY PF").Width = 80
        dgvF.Columns("CCY Chargeable").Width = 80
        dgvF.Columns("Fee in Charged Curncy").Width = 120
        dgvF.Columns("Total Chargeable Amount").Width = 120
        dgvF.Columns("Destination").Width = 80
        dgvF.Columns("PayFreq").Width = 80
        dgvF.Columns("Notes").Width = 500
        dgvF.Columns("Client Code").Width = 100
        dgvF.Columns("Customer Name").Width = 150
        dgvF.Columns("Management Type").Width = 120
        dgvF.Columns("VAT %").Width = 80
        dgvF.Columns("Management Type").Width = 120
        dgvF.Columns("Customer Tax Domicile").Width = 120
        dgvF.Columns("Fee in GBP").Width = 100
        dgvF.Columns("VAT Amount").Width = 100
        dgvF.Columns("VAT Amount GBP").Width = 100
        dgvF.Columns("Total Amount in GBP").Width = 100
        dgvF.Columns("EAM").Width = 150
        dgvF.Columns("TemplateLinkedName").Width = 200
        dgvF.Columns("RM1").Width = 120
        dgvF.Columns("RM2").Width = 120
        dgvF.Columns("OrderAccountName").Width = 150
        dgvF.Columns("BenAccountName").Width = 150
        dgvF.Columns("AveragePortfolioValue").Width = 120
        dgvF.Columns("PortfolioAUM_PF").Width = 120
        dgvF.Columns("PortfolioCash_PF").Width = 120
        dgvF.Columns("DaysAUM").Width = 120
    End Sub

    Public Sub CheckFeesReady()
        lbllastcrystalised.Text = ""
        If IsNumeric(cboFView.SelectedValue) And IsNumeric(cboFType.SelectedValue) And cboFDate.Text <> "" And cboFDateYear.Text <> "" Then
            If cboFView.SelectedValue <> 2 Then
                Dim varDateString As String = getDateString()
                If varDateString <> "" Then
                    Dim varGotDataTime As String = ClsIMS.GetSQLItem("Select top 1 max(UpdatedDate) From vwDolfinPaymentFeesPaid Where FeeTypeID = " & cboFType.SelectedValue & " And StartDate = " & Microsoft.VisualBasic.Strings.Left(varDateString, 12) & " And EndDate = " & Microsoft.VisualBasic.Strings.Right(varDateString, 12))
                    If varGotDataTime = "" Then
                        MsgBox("The fees for this date range are not ready to be authorised yet. Please crystalise fees for this date range before selecting fees to be authorised", vbCritical, "Crystalise Fees")
                    Else
                        lbllastcrystalised.Text = "Last Cyrstalised: " & varGotDataTime
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub LoadGridFees()
        Dim varSQL As String = ""
        Dim varDateString As String = ""
        Try
            If ValidatedCriteria() Then
                varDept = ClsIMS.GetSQLItem("Select U_Department From vwDolfinPaymentUsers Where u_username = '" & ClsIMS.FullUserName & "'")

                'Can only selected ops if in ops dept or has permissions for paying fees U_PayFeesPaid
                If cboFView.SelectedValue = 1 And varDept <> cboFView.Text And ClsIMS.CanUser(ClsIMS.FullUserName, "U_PayFeesPaid") Then
                    varDept = "Operations"
                Else
                    varDept = cboFView.Text
                End If

                'varDept = "Finance"
                lblforcastMsg.Visible = False

                varDateString = getDateString()
                If cboFView.SelectedValue <> 2 Then
                    lblforcastMsg.Visible = IIf(Microsoft.VisualBasic.Strings.Mid(varDateString, 15, 10) > Now.Date, True, False)
                Else
                    lblforcastMsg.Visible = IIf(dtFTo.Value >= Now.Date, True, False)
                End If

                If cboFView.SelectedValue = -1 Then
                    ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "update dolfinpaymentfeespaidparameters set FeeTypeID = " & cboFType.SelectedValue & ", StartDate = " & Microsoft.VisualBasic.Strings.Left(varDateString, 12) & ",EndDate = " & Microsoft.VisualBasic.Strings.Right(varDateString, 12) & ", UpdatedDate = getdate()")
                    If cboFType.SelectedValue = 0 Then
                        ClsIMS.RunSQLJob("IMSPlus - Crystalise Management Fees")
                    ElseIf cboFType.SelectedValue = 1 Then
                        ClsIMS.RunSQLJob("IMSPlus - Crystalise Custody Fees")
                    End If
                    MsgBox("Job has been started to crystalise the fees. Once it has completed for the dates selected, you will be able to run those dates for CRM", vbInformation, "Crystalising Data")
                Else
                    If varDept = "CRM" Then
                        'varSQL = "Select PaidStatus,PF_Code,PF_Shortcut,Portfolio,StartDate,EndDate,[Client Code],[Management Type],[CCY PF],[CCY Chargeable],[Fee in Charged Curncy],
                        '               [VAT Amount],[Total Chargeable Amount],[AdjustmentAmt],[Payment Amount],GBPRate,GBPRateInv,GBPPaymentAmt,[Fee in GBP],[VAT Amount GBP],[Total Amount in GBP],[VAT %],[Customer Tax Domicile],EAM,Destination,PayFreq, 
                        'TemplateLinkedName,RM1,RM2,AveragePortfolioValue,[PortfolioAUM_PF],[PortfolioCash_PF],DaysAUM,PaidFeePeriod,PaidYTD,MinimumCharge,MaximumCharge,PortfolioStartDate,Desk,Notes,FeesPaid_ID,
                        'OrderAccountName,BenAccountName,Fee_ID,BranchCode,Balance,[Customer Name],DefaultAccountCode,convert(nvarchar(20), UpdatedDate,120) as UpdatedDate
                        'From vwDolfinPaymentFeesPaid 
                        varSQL = "Select * From vwDolfinPaymentFeesPaidCRM Where FeeTypeID = " & cboFType.SelectedValue & " And StartDate = " & Microsoft.VisualBasic.Strings.Left(varDateString, 12) & " And EndDate = " & Microsoft.VisualBasic.Strings.Right(varDateString, 12) & " And BranchCode = " & ClsIMS.UserLocationCode &
                                    " Order By Portfolio"
                    ElseIf cboFView.SelectedValue = 2 Then
                        varSQL = "exec " & ClsIMS.IMSDB & ".dbo._Dolfin_Calc" & Replace(cboFType.Text, " ", "") & "ByDateAll " & varDateString & "," & ClsIMS.UserLocationCode & ",0," & IIf(ChkShowEstimated.Checked, 1, 0) & "," & IIf(IsNothing(cboFPortfolio.SelectedValue) Or cboFPortfolio.SelectedValue = 0, "null", cboFPortfolio.SelectedValue)
                    ElseIf cboFView.SelectedValue = 3 Then
                        varSQL = "exec " & ClsIMS.IMSDB & ".dbo._Dolfin_Calc" & Replace(cboFType.Text, " ", "") & "ByDateAllHistory " & varDateString & "," & ClsIMS.UserLocationCode & "," & IIf(ChkShowEstimated.Checked, 1, 0)
                    Else
                        varSQL = "exec " & ClsIMS.IMSDB & ".dbo._Dolfin_Calc" & Replace(cboFType.Text, " ", "") & "ByDateAllHistory " & varDateString & "," & ClsIMS.UserLocationCode & "," & IIf(ChkShowEstimated.Checked, 1, 0)
                    End If

                    Dim LoadedGrid As Boolean = False
                    Cursor = Cursors.WaitCursor

                    If dgvF.Rows.Count > 0 Then
                        LoadedGrid = True
                    End If

                    dgvF.DataSource = Nothing
                    dgvF.Refresh()
                    FeesPaidConn = ClsIMS.GetNewOpenConnection
                    dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesPaidConn)
                    dsFP = New DataSet
                    dgvdata.SelectCommand.CommandTimeout = 3000
                    dgvdata.Fill(dsFP, "FeesPaid")

                    If dsFP.Tables("FeesPaid").Rows.Count > 0 Then
                        If cboFView.SelectedValue <> 2 Then
                            If cboFDate.SelectedValue > 0 Then
                                dsFP.Tables("FeesPaid").DefaultView.RowFilter = "PayFreq = 'Monthly'"
                            Else
                                dsFP.Tables("FeesPaid").DefaultView.RowFilter = "PayFreq = 'Quarterly'"
                            End If
                        Else
                            dsFP.Tables("FeesPaid").DefaultView.RowFilter = "BranchCode = " & ClsIMS.UserLocationCode
                        End If

                        dgvF.AutoGenerateColumns = True
                        dgvF.DataSource = dsFP.Tables("FeesPaid")

                        dgvView = New DataView(dsFP.Tables("FeesPaid"))

                        dgvCheckbox.HeaderText = ""
                        dgvCheckbox.Width = 30

                        If Not LoadedGrid Then
                            dgvF.Columns.Insert(0, dgvCheckbox)
                            dgvF.Columns(0).Name = "SelectFee"
                        End If

                        If varDept = "Operations" Then
                            varSQL = "Select t_code As DefaultAccountCode, t_name1, t_shortcut1, pf_code, cr_name1 from vwdolfinpaymentselectaccount"
                            dgvdataAccounts = New SqlClient.SqlDataAdapter(varSQL, FeesPaidConn)
                            dsAccounts = New DataSet
                            dgvdataAccounts.Fill(dsAccounts, "Accounts")
                            cboAccountsColumn = New DataGridViewComboBoxColumn
                            cboAccountsColumn.HeaderText = "Accounts"
                            cboAccountsColumn.DataPropertyName = "DefaultAccountCode"
                            cboAccountsColumn.DataSource = dsAccounts.Tables("Accounts")
                            cboAccountsColumn.ValueMember = dsAccounts.Tables("Accounts").Columns(0).ColumnName
                            cboAccountsColumn.DisplayMember = dsAccounts.Tables("Accounts").Columns(1).ColumnName
                            cboAccountsColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                            cboAccountsColumn.FlatStyle = FlatStyle.Flat

                            dv = New DataView(dsAccounts.Tables("Accounts"))

                            dgvF.Columns.Insert(3, cboAccountsColumn)
                            dgvF.Columns(3).Name = "AccountDetail"
                            dgvF.Columns("AccountDetail").Width = 250
                            dgvF.Columns("AccountDetail").DefaultCellStyle.BackColor = Color.LightYellow
                        End If

                        ColumnWidths()
                        AllView()

                        If varDept = "CRM" Then
                            CRMView()
                        ElseIf cboFView.SelectedValue = 2 Then
                            FinanceViewEstimated()
                        ElseIf cboFView.SelectedValue = 3 Then
                            FinanceViewActual()
                        Else
                            OperationsView()
                        End If


                        For Each column In dgvF.Columns
                            column.sortmode = DataGridViewColumnSortMode.NotSortable
                            If IsNumeric(dgvF.Rows(0).Cells(column.index).Value) And column.index <> 2 And column.name <> "Fee_ID" Then
                                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                                column.DefaultCellStyle.Format = "##,0.00"
                                If column.name = "Balance" Then
                                    column.DefaultCellStyle.ForeColor = Color.DarkGreen
                                ElseIf column.name = "Adjustment" Then
                                    column.DefaultCellStyle.backColor = Color.LightYellow
                                ElseIf column.name = "GBPRate" Then
                                    column.DefaultCellStyle.Format = "##,0.0000000"
                                ElseIf column.name = "Payment Amount" Then
                                    column.HeaderCell.Style.Font = New Font(dgvF.Font, FontStyle.Bold)
                                    column.DefaultCellStyle.Font = New Font(dgvF.Font, FontStyle.Bold)
                                End If
                            ElseIf IsDate(dgvF.Rows(0).Cells(column.index).Value) Then
                                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                                column.DefaultCellStyle.Format = "dd-MMM-yyyy"
                            Else
                                If column.name = "Notes" Then
                                    column.DefaultCellStyle.backColor = Color.LightYellow
                                End If
                                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                            End If
                        Next

                        AssignColours()


                        'dgvF.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                        dgvF.Refresh()

                        cboFilterStatus.Text = ""
                        cboFilterDestination.Text = ""
                        cboFilterRM1.Text = ""
                        cboFilterRM2.Text = ""
                        GroupBoxFilter.Visible = True
                        GroupBoxAction.Visible = True
                        ChkSelectAll.Visible = True
                        ChkSelectAll.Checked = False
                        ClearFilter()

                        If varDept <> "CRM" Then
                            ChkSelectAllClientOnly.Visible = True
                            ChkSelectAllClientOnly.Checked = False

                            If ClsIMS.UserLocationCode = 47 Then
                                ChkSelectAllClientOnly.Checked = True
                            End If
                        End If
                    Else
                        MsgBox("No data has been found for the date selected.", vbExclamation, "Check Fees")
                    End If
                End If
            End If
            Cursor = Cursors.Default
        Catch ex As Exception
            dgvF.DataSource = Nothing
            Cursor = Cursors.Default
        End Try
    End Sub

    Private Function getDateString() As String
        getDateString = ""
        Select Case cboFDate.SelectedValue
            Case -1
                getDateString = "'" & cboFDateYear.Text & "-01-01','" & cboFDateYear.Text & "-03-31'"
            Case -2
                getDateString = "'" & cboFDateYear.Text & "-04-01','" & cboFDateYear.Text & "-06-30'"
            Case -3
                getDateString = "'" & cboFDateYear.Text & "-07-01','" & cboFDateYear.Text & "-09-30'"
            Case -4
                getDateString = "'" & cboFDateYear.Text & "-10-01','" & cboFDateYear.Text & "-12-31'"
            Case Else
                If cboFDateYear.Text <> "" Then
                    'getDateString = "'" & cboFDateYear.Text & "-" & cboFDate.SelectedValue & "-01','" & DateAdd(DateInterval.Day, -1, IIf(cboFDate.SelectedValue = 12, cboFDateYear.Text + 1, cboFDateYear.Text) & "-" & IIf(cboFDate.SelectedValue = 12, 0, cboFDate.SelectedValue) + 1 & "-01").ToString("yyyy-MM-dd") & "'"
                    getDateString = "'" & CDate(cboFDateYear.Text & "-" & cboFDate.SelectedValue & "-01").ToString("yyyy-MM-dd") & "','" & DateAdd(DateInterval.Day, -1, IIf(cboFDate.SelectedValue = 12, cboFDateYear.Text + 1, cboFDateYear.Text) & "-" & IIf(cboFDate.SelectedValue = 12, 0, cboFDate.SelectedValue) + 1 & "-01").ToString("yyyy-MM-dd") & "'"
                Else
                    getDateString = "'" & dtFFrom.Value.ToString("yyyy-MM-dd") & "','" & dtFTo.Value.ToString("yyyy-MM-dd") & "'"
                End If
        End Select

    End Function


    Private Sub PopulateDates()
        ClsIMS.PopulateComboboxWithData(cboFType, "Select FeeTypeID, FeeType_Description from vwDolfinPaymentFeeType")
        ClsIMS.PopulateComboboxWithData(cboFDate, "SELECT -MONTH(DATEADD(MM, s.number, CONVERT(DATETIME, 0))) AS [Num],'Quarter ' + DATENAME(QUARTER, DATEADD(Q, s.number, CONVERT(DATETIME, 0))) AS [DateName]
                                                    FROM master.dbo.spt_values s WHERE [type] = 'P' AND s.number BETWEEN 0 AND 3
                                                    Union All
                                                    SELECT MONTH(DATEADD(MM, s.number, CONVERT(DATETIME, 0))) AS [Num],DATENAME(MONTH, DATEADD(MM, s.number, CONVERT(DATETIME, 0))) AS [DateName]
                                                    FROM master.dbo.spt_values s WHERE [type] = 'P' AND s.number BETWEEN 0 AND 11")

        ClsIMS.PopulateComboboxWithData(cboFDateYear, "SELECT Yr,Yr FROM (SELECT TOP 10 2016 + ROW_NUMBER() OVER (ORDER BY [type]) AS Yr FROM master.dbo.spt_values) Years WHERE Yr <= YEAR(GETDATE())")
        ClsIMS.PopulateComboboxWithData(cboFView, "Select -1,'Crystalise Fees' union select 0,'CRM' union Select 1,'Operations' union select 2, 'Finance (Estimated)' union select 3, 'Finance (Actual)'")
        ClsIMS.PopulateComboboxWithData(cboFPortfolio, "Select 0 as pf_code,'' as pf_shortcut1 union select pf_code, pf_shortcut1 from vwDolfinPaymentFeeSchedulesLinks group by pf_code, pf_shortcut1 order by pf_shortcut1")
        ClsIMS.PopulateComboboxWithData(cboFilterStatus, "select 0,'' union select S_ID,S_Status FROM [DolfinPayment].[dbo].[DolfinPaymentStatus] where S_ID in (1,2,5,13,37)")
        ClsIMS.PopulateComboboxWithData(cboFilterRM1, "Select 0,ISNULL(RM1,'') as RM1 from vwDolfinPaymentFeeSchedulesLinks group by ISNULL(RM1,'') order by ISNULL(RM1,'')")
        ClsIMS.PopulateComboboxWithData(cboFilterRM2, "Select 0,ISNULL(RM2,'') as RM2 from vwDolfinPaymentFeeSchedulesLinks group by ISNULL(RM2,'') order by ISNULL(RM2,'')")
        ClsIMS.PopulateComboboxWithData(cboFilterDestination, "Select 0,ISNULL(FS_Destination,'') from vwDolfinPaymentFeeSchedulesLinks group by ISNULL(FS_Destination,'') order by ISNULL(FS_Destination,'')")
        ClsIMS.PopulateComboboxWithData(cboFilterDesk, "Select 0,'China' union Select 1,'CRM'")
        ClsIMS.PopulateComboboxWithData(cboFilterAmt, "Select 0, 'Under 10 (Local CCY)' union select 1, 'Over 10 (Local CCY)'")
        ClsIMS.PopulateComboboxWithData(cboFilterRef, "select p.p_orderref,p.p_orderref from DolfinPaymentFeeHistory inner join vwdolfinpayment p on fee_paymentid = p_id group by p.p_orderref order by p.p_orderref")
        ClsIMS.PopulateComboboxWithData(cboFilterAccount, "select t.t_name1,t.t_name1 from DolfinPaymentFeeHistory inner join titles t on fee_accountcode = t_code group by t.t_name1 order by t.t_name1")
        ClsIMS.PopulateComboboxWithData(cboFilterType, "Select 0,'' union Select 1,'Client Only'")
        ClsIMS.PopulateComboboxWithData(cboFilterTemplate, "select TemplateLinkedID,TemplateLinkedName from vwDolfinPaymentFeeSchedulesLinks where isnull(TemplateLinkedName,'') <> '' group by TemplateLinkedID,TemplateLinkedName order by TemplateLinkedName")
        ClsIMS.PopulateComboboxWithData(cboFilterPortfolio, "Select 0 as pf_code,'' as portfolio union select pf_code, Portfolio from vwDolfinPaymentFeesPaid group by pf_code, Portfolio order by portfolio")
    End Sub


    Private Sub FrmFeesPaid_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvF, True)
        PopulateDates()
    End Sub

    Private Sub cmdGetFees_Click(sender As Object, e As EventArgs) Handles cmdGetFees.Click
        LoadGridFees()
    End Sub

    Private Function GetPortfolioSourceCode(ByVal varDataRow As Integer) As Integer
        GetPortfolioSourceCode = dgvF.Rows(varDataRow).Cells("pf_code").Value
        If Not IsDBNull(dgvF.Rows(varDataRow).Cells("PortfolioRedirectCode").Value) Then
            If dgvF.Rows(varDataRow).Cells("PortfolioRedirectCode").Value <> 0 Then
                GetPortfolioSourceCode = dgvF.Rows(varDataRow).Cells("PortfolioRedirectCode").Value
            End If
        End If
    End Function


    Private Sub dgvF_CellLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvF.CellLeave
        'On Error Resume Next
        If dgvF.SelectedCells.Count > 0 Then
            If Not dgvF.SelectedCells(0).Value Is Nothing Then
                If Not IsDBNull(dgvF.SelectedCells(0).Value) Then
                    If dgvF.SelectedCells(0).GetType.ToString() = "System.Windows.Forms.DataGridViewComboBoxCell" Then
                        dgvF.Rows(e.RowIndex).Cells("Balance").Value = ClsIMS.GetBalance(GetPortfolioSourceCode(e.RowIndex), dgvF.Rows(e.RowIndex).Cells("AccountDetail").Value)
                        'Dim varCCY As String = ClsIMS.GetSQLItem("select cr_name1 from titles inner join currencies on t_currency = cr_code where t_code = " & dgvF.Rows(e.RowIndex).Cells("AccountDetail").Value)
                        'Dim varCCYRate As String = ClsIMS.GetCCYRate(Now, varCCY, dgvF.Rows(e.RowIndex).Cells("CCY Chargeable").Value)
                        'If IsNumeric(varCCYRate) Then
                        'dgvF.Rows(e.RowIndex).Cells("Account CCY Rate").Value = CDbl(varCCYRate)
                        'dgvF.Rows(e.RowIndex).Cells("Payment Amount").Value = dgvF.Rows(e.RowIndex).Cells("Total Chargeable Amount").Value * CDbl(varCCYRate)
                        'dgvF.Rows(e.RowIndex).Cells("Account CCY Rate").Style.ForeColor = Color.DarkRed
                        'End If
                        ChangeBalanceColor(e.RowIndex, "Balance", "Payment Amount")
                    End If
                End If
            End If
        End If
    End Sub

    Public Sub ChangeBalanceColor(ByVal varRow As Integer, ByVal BalanceColumnName As String, ByVal BalanceColumnCheckName As String)
        If Not (dgvF.Rows(varRow).Cells(BalanceColumnName).Value) Is Nothing Then
            If Not IsDBNull(dgvF.Rows(varRow).Cells(BalanceColumnName).Value) Then
                If dgvF.Rows(varRow).Cells(BalanceColumnName).Value < dgvF.Rows(varRow).Cells(BalanceColumnCheckName).Value Then
                    dgvF.Rows(varRow).Cells(BalanceColumnName).Style.ForeColor = Color.DarkRed
                Else
                    dgvF.Rows(varRow).Cells(BalanceColumnName).Style.ForeColor = Color.DarkGreen
                End If
            End If
        End If
    End Sub

    Private Sub dgvF_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles dgvF.CurrentCellDirtyStateChanged
        On Error Resume Next
        If dgvF.SelectedCells(0).GetType.ToString() = "System.Windows.Forms.DataGridViewComboBoxCell" Then
            dgvF.CommitEdit(DataGridViewDataErrorContexts.Commit)
            dgvF.EndEdit()
        End If
    End Sub

    Private Sub dgvF_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvF.CellBeginEdit
        On Error Resume Next
        If dgvF(e.ColumnIndex, e.RowIndex).OwningColumn.Name = "AccountDetail" Then
            Dim dgvCbo As DataGridViewComboBoxCell = TryCast(dgvF(e.ColumnIndex, e.RowIndex), DataGridViewComboBoxCell)
            If dgvCbo IsNot Nothing Then
                dv = New DataView(dsAccounts.Tables("Accounts"))
            End If
            If Not IsDBNull(dgvF.SelectedCells(0).Value) Then
                dv.RowFilter = "pf_code = " & GetPortfolioSourceCode(e.RowIndex) & " And cr_name1 = '" & dgvF.Rows(e.RowIndex).Cells("CCY Chargeable").Value & "'"
                dgvCbo.DataSource = dv
            Else
                dv.RowFilter = "pf_code = 0"
                dgvCbo.DataSource = dv
            End If
        End If
    End Sub

    Private Sub dgvF_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvF.CellEndEdit
        On Error Resume Next
        If dgvF(e.ColumnIndex, e.RowIndex).OwningColumn.Name = "Adjustment" Then
            dgvF(e.ColumnIndex + 1, e.RowIndex).Value = dgvF(e.ColumnIndex, e.RowIndex).Value + dgvF(e.ColumnIndex - 1, e.RowIndex).Value
        ElseIf dgvF(e.ColumnIndex, e.RowIndex).OwningColumn.Name = "AccountDetail" Then
            Dim dgvCbo As DataGridViewComboBoxCell = TryCast(dgvF(e.ColumnIndex, e.RowIndex), DataGridViewComboBoxCell)
            If dgvCbo IsNot Nothing Then
                dv = New DataView(dsAccounts.Tables("Accounts"))
                dgvCbo.DataSource = dv
            End If
        End If
    End Sub

    Private Sub dgvF_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles dgvF.DataError
        e.Cancel = True
    End Sub

    Private Function PopulateFeesPaid(ByRef TblFeesPaid As DataTable) As Boolean
        Dim feespaidrow As DataRow

        On Error Resume Next
        PopulateFeesPaid = False
        AddDataColumn(TblFeesPaid, "Fee_ID", "System.Int32")
        AddDataColumn(TblFeesPaid, "FeesPaid_ID", "System.Int32")
        AddDataColumn(TblFeesPaid, "Fee_TypeID", "System.Int32")
        AddDataColumn(TblFeesPaid, "Fee_PortfolioCode", "System.Int32")
        AddDataColumn(TblFeesPaid, "Fee_StartDate", "System.DateTime")
        AddDataColumn(TblFeesPaid, "Fee_EndDate", "System.DateTime")
        AddDataColumn(TblFeesPaid, "Fee_AccountCode", "System.Int32")
        AddDataColumn(TblFeesPaid, "Fee_Charged_CCY", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_VATAmount", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Amount", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Adjustment", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Paid", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_AccountCCYRate", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_GBPRate", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Charged_GBP", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_VATAmount_GBP", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Amount_GBP", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_Destination", "System.String")
        AddDataColumn(TblFeesPaid, "Fee_Freq", "System.String")
        AddDataColumn(TblFeesPaid, "Fee_TemplateName", "System.String")
        AddDataColumn(TblFeesPaid, "Fee_AveragePortfolioValue", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_EndPeriodAUMPF", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_EndPeriodCashPF", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_DaysAUM", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_MinimumCharge", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_MaximumCharge", "System.Double")
        AddDataColumn(TblFeesPaid, "Fee_ClientOnly", "System.Boolean")
        AddDataColumn(TblFeesPaid, "Fee_Notes", "System.String")
        AddDataColumn(TblFeesPaid, "Fee_PortfolioCodeRedirect", "System.Int32")

        For i As Integer = 0 To dgvF.RowCount - 1
            If Not dgvF.Rows(i).Cells(0).Value Is Nothing Then
                If dgvF.Rows(i).Cells(0).Value Then
                    If dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusPendingAuth) Or
                        dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusReady) Or
                        dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusGenerateInvoice) Then
                        feespaidrow = TblFeesPaid.NewRow()
                        feespaidrow("Fee_ID") = dgvF.Rows(i).Cells("Fee_ID").Value
                        feespaidrow("FeesPaid_ID") = dgvF.Rows(i).Cells("FeesPaid_ID").Value
                        feespaidrow("Fee_TypeID") = cboFType.SelectedValue
                        feespaidrow("Fee_PortfolioCode") = dgvF.Rows(i).Cells("pf_code").Value
                        feespaidrow("Fee_StartDate") = CDate(dgvF.Rows(i).Cells("StartDate").Value)
                        feespaidrow("Fee_EndDate") = CDate(dgvF.Rows(i).Cells("EndDate").Value)
                        feespaidrow("Fee_AccountCode") = dgvF.Rows(i).Cells("AccountDetail").Value
                        feespaidrow("Fee_Charged_CCY") = dgvF.Rows(i).Cells("Fee in Charged Curncy").Value
                        feespaidrow("Fee_VATAmount") = dgvF.Rows(i).Cells("VAT Amount").Value
                        feespaidrow("Fee_Amount") = dgvF.Rows(i).Cells("Total Chargeable Amount").Value
                        feespaidrow("Fee_Adjustment") = dgvF.Rows(i).Cells("Adjustment").Value
                        feespaidrow("Fee_Paid") = dgvF.Rows(i).Cells("Payment Amount").Value
                        feespaidrow("Fee_AccountCCYRate") = 0
                        feespaidrow("Fee_GBPRate") = dgvF.Rows(i).Cells("GBPRate").Value
                        feespaidrow("Fee_Charged_GBP") = dgvF.Rows(i).Cells("Fee in GBP").Value
                        feespaidrow("Fee_VATAmount_GBP") = dgvF.Rows(i).Cells("VAT Amount GBP").Value
                        feespaidrow("Fee_Amount_GBP") = dgvF.Rows(i).Cells("Total Amount in GBP").Value
                        feespaidrow("Fee_Destination") = dgvF.Rows(i).Cells("Destination").Value
                        feespaidrow("Fee_Freq") = dgvF.Rows(i).Cells("PayFreq").Value
                        feespaidrow("Fee_TemplateName") = dgvF.Rows(i).Cells("TemplateLinkedName").Value
                        feespaidrow("Fee_AveragePortfolioValue") = dgvF.Rows(i).Cells("AveragePortfolioValue").Value
                        feespaidrow("Fee_EndPeriodAUMPF") = dgvF.Rows(i).Cells("PortfolioAUM_PF").Value
                        feespaidrow("Fee_EndPeriodCashPF") = dgvF.Rows(i).Cells("PortfolioCash_PF").Value
                        feespaidrow("Fee_DaysAUM") = dgvF.Rows(i).Cells("DaysAUM").Value
                        feespaidrow("Fee_MinimumCharge") = dgvF.Rows(i).Cells("MinimumCharge").Value
                        feespaidrow("Fee_MaximumCharge") = dgvF.Rows(i).Cells("MaximumCharge").Value
                        feespaidrow("Fee_ClientOnly") = dgvF.Rows(i).Cells("Client Only").Value
                        feespaidrow("Fee_Notes") = Strings.Left(dgvF.Rows(i).Cells("Notes").Value, 2000)
                        feespaidrow("Fee_PortfolioCodeRedirect") = dgvF.Rows(i).Cells("PortfolioRedirectCode").Value
                        TblFeesPaid.Rows.Add(feespaidrow)
                        PopulateFeesPaid = True
                    End If
                End If
            End If
        Next
    End Function

    Private Function SelectedAuthFee(ByRef SelectFeeCount As Integer) As Boolean
        SelectedAuthFee = False
        For i As Integer = 0 To dgvF.RowCount - 1
            If Not dgvF.Rows(i).Cells("SelectFee").Value Is Nothing And Not dgvF.Rows(i).Cells("PaidStatus").Value Is Nothing Then
                If VarType(dgvF.Rows(i).Cells("SelectFee").Value) = vbBoolean Then
                    If dgvF.Rows(i).Cells("SelectFee").Value Then
                        If dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusPendingAuth) Then
                            SelectedAuthFee = True
                        Else
                            SelectedAuthFee = False
                            Exit For
                        End If
                    End If
                End If
            End If
        Next
    End Function

    Private Function SelectedUnAuthFee(ByRef SelectFeeCount As Integer) As Boolean
        SelectedUnAuthFee = False
        For i As Integer = 0 To dgvF.RowCount - 1
            If Not dgvF.Rows(i).Cells("SelectFee").Value Is Nothing And Not dgvF.Rows(i).Cells("PaidStatus").Value Is Nothing Then
                If VarType(dgvF.Rows(i).Cells("SelectFee").Value) = vbBoolean Then
                    If dgvF.Rows(i).Cells("SelectFee").Value Then
                        SelectFeeCount = SelectFeeCount + 1
                        If dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusReady) Or dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusGenerateInvoice) Then
                            SelectedUnAuthFee = True
                        Else
                            SelectedUnAuthFee = False
                            Exit For
                        End If
                    End If
                End If
            End If
        Next
    End Function


    Private Function SelectedPayFee(ByRef SelectFeeCount As Integer) As Boolean
        SelectedPayFee = False
        For i As Integer = 0 To dgvF.RowCount - 1
            If Not dgvF.Rows(i).Cells("SelectFee").Value Is Nothing And Not dgvF.Rows(i).Cells("PaidStatus").Value Is Nothing Then
                If VarType(dgvF.Rows(i).Cells("SelectFee").Value) = vbBoolean Then
                    If dgvF.Rows(i).Cells("SelectFee").Value Then
                        SelectFeeCount = SelectFeeCount + 1
                        If dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusReady) Or dgvF.Rows(i).Cells("PaidStatus").Value = ClsIMS.GetStatusType(cStatusGenerateInvoice) Then
                            SelectedPayFee = True
                        Else
                            SelectedPayFee = False
                            Exit For
                        End If
                    End If
                End If
            End If
        Next
    End Function


    Private Sub CmdPay_Click(sender As Object, e As EventArgs) Handles CmdPay.Click
        Dim TblFeesPaid As New DataTable
        Dim SelectFeeCount As Integer = 0

        If ClsIMS.CanUser(ClsIMS.FullUserName, "U_PayFeesPaid") Then
            If SelectedPayFee(SelectFeeCount) Then
                If SelectFeeCount < 21 Then
                    Dim varResponse As Integer = MsgBox("Are you sure you want to pay the selected fees? (" & CStr(SelectFeeCount) & " items selected)", vbQuestion + vbYesNo, "Please confirm payments")
                    If varResponse = vbYes Then
                        If PopulateFeesPaid(TblFeesPaid) Then
                            If ClsIMS.UpdateFeesPaid(TblFeesPaid) Then
                                MsgBox("Fees have now been allocated to be paid", vbInformation, "Fees paid")
                            Else
                                MsgBox("There has been a problem allocating the fees to be paid. Please check the audit trail and request systems support to investigate", vbCritical, "Problem saving fees")
                            End If
                            LoadGridFees()
                        End If
                    End If
                Else
                    MsgBox("Only 20 items are allowed to be processed per batch. If you want to select more, please tick and send batches of 20 items", vbInformation, "20 items in batches only")
                End If
            Else
                MsgBox("Please Select the fee(s) you want To action against by ticking the box Next To the fee" & vbNewLine & "NOTE: Only ready fees can be selected to be paid", vbInformation, "Select fee(s) before action")
            End If
        Else
            MsgBox("You do not have permission to pay fees. Please contact systems support for permissions", vbCritical, "Not authorised")
        End If
    End Sub

    Private Sub CmdAuthFee_Click(sender As Object, e As EventArgs) Handles CmdAuthFee.Click
        Dim TblFeesPaid As New DataTable
        Dim SelectFeeCount As Integer = 0

        If ClsIMS.CanUser(ClsIMS.FullUserName, "U_AuthoriseFeesPaid") Then
            If SelectedAuthFee(SelectFeeCount) Then
                Dim varResponse As Integer = MsgBox("Are you sure you want to authorise the selected fees? (" & CStr(SelectFeeCount) & " items selected)", vbQuestion + vbYesNo, "Please confirm payments")
                If varResponse = vbYes Then
                    If PopulateFeesPaid(TblFeesPaid) Then
                        If ClsIMS.AuthoriseFeesPaid(TblFeesPaid, True) Then
                            MsgBox("Fees have now been authorised")
                        Else
                            MsgBox("There has been a problem authorising the fees. Please check the audit trail and request systems support to investigate", vbCritical, "Problem authorising fees")
                        End If
                    LoadGridFees()
                    End If
                End If
            Else
                MsgBox("Please select the fee(s) you want to action against by ticking the box next to the fee" & vbNewLine & "NOTE: Only pending authorisations can be selected to be authorised", vbInformation, "Select fee(s) before action")
            End If
        Else
            MsgBox("You do not have permission to authorise fees. Please contact systems support for permissions", vbCritical, "Not authorised")
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvF, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub ChkSelectAll_CheckedChanged(sender As Object, e As EventArgs) Handles ChkSelectAll.CheckedChanged
        For i = 0 To dgvF.RowCount - 1
            dgvF.Rows(i).Cells(0).Value = ChkSelectAll.Checked
        Next
    End Sub

    Private Sub CmdUnAuthFee_Click(sender As Object, e As EventArgs) Handles CmdUnAuthFee.Click
        Dim TblFeesPaid As New DataTable
        Dim SelectFeeCount As Integer = 0

        If ClsIMS.CanUser(ClsIMS.FullUserName, "U_AuthoriseFeesPaid") Then
            If SelectedUnAuthFee(SelectFeeCount) Then
                Dim varResponse As Integer = MsgBox("Are you sure you want to un-authorise the selected fees? (" & CStr(SelectFeeCount) & " items selected)", vbQuestion + vbYesNo, "Please confirm payments")
                If varResponse = vbYes Then
                    If PopulateFeesPaid(TblFeesPaid) Then
                        ClsIMS.AuthoriseFeesPaid(TblFeesPaid, False)
                        LoadGridFees()
                    End If
                End If
            Else
                MsgBox("Please select the fee(s) you want to action against by ticking the box next to the fee" & vbNewLine & "NOTE: Only pending authorisations can be selected to be authorised", vbInformation, "Select fee(s) before action")
            End If
        Else
            MsgBox("You do not have permission to authorise fees. Please contact systems support for permissions", vbCritical, "Not authorised")
        End If
    End Sub

    Private Sub cmdClearFilter_Click(sender As Object, e As EventArgs) Handles cmdClearFilter.Click
        ClearFilter()
        LoadGridFees()
    End Sub

    Private Sub ShowDateRange(ByVal varShow As Boolean)
        lblFfrom.Visible = varShow
        dtFFrom.Visible = varShow
        lblFTo.Visible = varShow
        dtFTo.Visible = varShow
        lblFPortfolio.Visible = varShow
        cboFPortfolio.Visible = varShow
        lblFDate.Visible = Not varShow
        cboFDate.Visible = Not varShow
        lblFDateYear.Visible = Not varShow
        cboFDateYear.Visible = Not varShow
    End Sub

    Private Sub cboFView_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFView.SelectedIndexChanged
        If IsNumeric(cboFView.SelectedValue) Then
            If cboFView.SelectedValue = 2 Then
                ShowDateRange(True)
            Else
                ShowDateRange(False)
            End If
            CheckFeesReady()
        End If
    End Sub

    Private Sub ChkSelectAllClientOnly_CheckedChanged(sender As Object, e As EventArgs) Handles ChkSelectAllClientOnly.CheckedChanged
        On Error Resume Next
        For i = 0 To dgvF.RowCount - 1
            dgvF.Rows(i).Cells("Client Only").Value = ChkSelectAllClientOnly.Checked
        Next
    End Sub

    Private Sub ClearFilter()
        cboFilterStatus.Text = ""
        cboFilterDestination.Text = ""
        cboFilterRM1.Text = ""
        cboFilterRM2.Text = ""
        cboFilterDesk.Text = ""
        cboFilterAmt.Text = ""
        cboFilterRef.Text = ""
        cboFilterAccount.Text = ""
        cboFilterType.Text = ""
        cboFilterPortfolio.Text = ""
    End Sub


    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varFilterStr As String = ""

        If cboFilterStatus.Text <> "" Then
            varFilterStr = varFilterStr & " And PaidStatus = '" & cboFilterStatus.Text & "'"
        End If
        If cboFilterDestination.Text <> "" Then
            varFilterStr = varFilterStr & " and Destination = '" & cboFilterDestination.Text & "'"
        End If
        If cboFilterRM1.Text <> "" Then
            varFilterStr = varFilterStr & " and RM1 = '" & cboFilterRM1.Text & "'"
        End If
        If cboFilterRM2.Text <> "" Then
            varFilterStr = varFilterStr & " and RM2 = '" & cboFilterRM2.Text & "'"
        End If
        If cboFilterDesk.Text <> "" Then
            varFilterStr = varFilterStr & " and Desk = '" & cboFilterDesk.Text & "'"
        End If
        If cboFilterAmt.Text = "Under 10 (Local CCY)" Then
            varFilterStr = varFilterStr & " and [Payment Amount] <= 10"
        ElseIf cboFilterAmt.Text = "Over 10 (Local CCY)" Then
            varFilterStr = varFilterStr & " and [Payment Amount] > 10"
        End If
        If cboFilterRef.Text <> "" Then
            varFilterStr = varFilterStr & " and OrderRef = '" & cboFilterRef.Text & "'"
        End If
        If cboFilterAccount.Text <> "" Then
            varFilterStr = varFilterStr & " and OrderAccountName = '" & cboFilterAccount.Text & "'"
        End If
        If cboFilterType.Text <> "" Then
            varFilterStr = varFilterStr & " and [Client Only] = 1"
        End If
        If cboFilterPortfolio.Text <> "" Then
            varFilterStr = varFilterStr & " and Portfolio = '" & cboFilterPortfolio.Text & "'"
        End If
        If cboFDate.SelectedValue > 0 Then
            varFilterStr = varFilterStr & " And PayFreq = 'Monthly' and BranchCode = " & ClsIMS.UserLocationCode
        Else
            varFilterStr = varFilterStr & " and PayFreq = 'Quarterly' and BranchCode = " & ClsIMS.UserLocationCode
        End If

        If cboFilterTemplate.Text <> "" Then
            varFilterStr = varFilterStr & " and TemplateLinkedName = '" & cboFilterTemplate.Text & "'"
        End If

        If varFilterStr <> "" Then
            dgvView = New DataView(dsFP.Tables("FeesPaid"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "portfolio Asc", DataViewRowState.CurrentRows)
            dgvF.DataSource = dgvView
        Else
            dgvView = New DataView(dsFP.Tables("FeesPaid"), "", "portfolio Asc", DataViewRowState.CurrentRows)
            dgvF.DataSource = dgvView
        End If
        AssignColours()
    End Sub

    Private Sub dgvF_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvF.RowHeaderMouseDoubleClick
        ClsFP = New ClsFeesPaid
        ClsFP.ID = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("fee_id").Value), 0, dgvF.Rows(e.RowIndex).Cells("fee_id").Value)
        ClsFP.PortfolioCode = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("pf_code").Value), 0, dgvF.Rows(e.RowIndex).Cells("pf_code").Value)
        ClsFP.PortfolioName = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("portfolio").Value), "", dgvF.Rows(e.RowIndex).Cells("portfolio").Value)
        ClsFP.CCYCode = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("cr_code").Value), 0, dgvF.Rows(e.RowIndex).Cells("cr_code").Value)
        ClsFP.CCYName = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("ccy chargeable").Value), "", dgvF.Rows(e.RowIndex).Cells("ccy chargeable").Value)
        ClsFP.StartDate = dgvF.Rows(e.RowIndex).Cells("StartDate").Value
        ClsFP.EndDate = dgvF.Rows(e.RowIndex).Cells("EndDate").Value
        If dgvF.Columns.Contains("OrderRef") Then
            If dgvF.Rows(e.RowIndex).Cells("OrderRef").Visible Then
                ClsFP.Reference = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("OrderRef").Value), "", dgvF.Rows(e.RowIndex).Cells("OrderRef").Value)
            Else
                ClsFP.Reference = "CRM"
            End If
        Else
            ClsFP.Reference = "CRM"
        End If
        ClsFP.Type = cboFType.SelectedValue
        ClsFP.TypeName = cboFType.Text
        ClsFP.AccountName = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("OrderAccountName").Value), "", dgvF.Rows(e.RowIndex).Cells("OrderAccountName").Value)
        ClsFP.LastPaymentAmount = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("Payment Amount").Value), "", dgvF.Rows(e.RowIndex).Cells("Payment Amount").Value)
        ClsFP.Destination = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("Destination").Value), "", dgvF.Rows(e.RowIndex).Cells("Destination").Value)
        ClsFP.PortfolioCodeRedirect = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("PortfolioRedirectCode").Value), 0, dgvF.Rows(e.RowIndex).Cells("PortfolioRedirectCode").Value)
        ClsFP.PortfolioNameRedirect = IIf(IsDBNull(dgvF.Rows(e.RowIndex).Cells("PortfolioRedirect").Value), 0, dgvF.Rows(e.RowIndex).Cells("PortfolioRedirect").Value)
        Dim NewFPO = New FrmFeesPaidOption
        NewFPO.ShowDialog()
    End Sub

    Private Sub cboFDate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFDate.SelectedIndexChanged
        CheckFeesReady()
    End Sub

    Private Sub cmdMultrees_Click(sender As Object, e As EventArgs) Handles cmdMultrees.Click
        Cursor = Cursors.WaitCursor
        ClsFP.CreateMultreesExportXL(dgvF)
        Cursor = Cursors.Default
    End Sub

    Private Sub cboFType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFType.SelectedIndexChanged
        CheckFeesReady()
    End Sub

    Private Sub cboFDateYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFDateYear.SelectedIndexChanged
        CheckFeesReady()
    End Sub

End Class