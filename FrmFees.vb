﻿Imports System.ComponentModel

Public Class FrmFees
    Dim FeesConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataCCY As New SqlClient.SqlDataAdapter
    Dim dgvdataException As New SqlClient.SqlDataAdapter
    Dim dgvdataCFException As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim dsMGTRange As New DataSet
    Dim dsCCY As New DataSet
    Dim dsCFRange As New DataSet
    Dim dsCFAssetType As New DataSet
    Dim dsCFSK As New DataSet
    Dim dsCFSKClearer As New DataSet
    Dim dsTFSK As New DataSet
    Dim dsTFSKFixed As New DataSet
    Dim dsTFSKClearer As New DataSet
    Dim dsTFRange As New DataSet
    Dim dsException As New DataSet
    Dim dsExceptionGrp As New DataSet
    Dim dsCFException As New DataSet
    Dim dsCFExceptionGrp As New DataSet
    Dim cboCFSKColumn As New DataGridViewComboBoxColumn
    Dim cboTFSKColumn As New DataGridViewComboBoxColumn
    Dim cboExceptionGroupColumn As New DataGridViewComboBoxColumn
    Dim cboExceptionColumn As New DataGridViewComboBoxColumn
    Dim cboCFAssetTypeColumn As New DataGridViewComboBoxColumn
    Dim cboCFExceptionGroupColumn As New DataGridViewComboBoxColumn
    Dim cboCFExceptionColumn As New DataGridViewComboBoxColumn
    Dim cboCCYColumn As New DataGridViewComboBoxColumn

    Private Sub EditMgtFee()
        cboMGTChargeableCCY.SelectedValue = ClsMgtFees.ChargeableCCYCode
        cboMgtType.SelectedValue = ClsMgtFees.TypeID
        cboMGTCCYEqv.SelectedValue = ClsMgtFees.CCYEqvCode
        txtMgtMin.Text = ClsMgtFees.MinCharge
        txtMgtMax.Text = ClsMgtFees.MaxCharge
        txtMgtMaintMin.Text = ClsMgtFees.MinMaintCharge
    End Sub

    Private Sub EditCF()
        cboCFChargeableCCY.SelectedValue = ClsCF.ChargeableCCYCode
        cboCFType.SelectedValue = ClsCF.TypeID
        cboCFCCYEqv.SelectedValue = ClsCF.CCYEqvCode
        txtCFMin.Text = ClsCF.MinCharge
        txtCFMax.Text = ClsCF.MaxCharge
        txtCFMaintMin.Text = ClsCF.MinMaintCharge
    End Sub

    Private Sub EditTF()
        cboTFType.SelectedValue = ClsTF.TypeID
        cboTFCCYEqv.SelectedValue = ClsTF.CCYEqvCode
        txtTFTicketFee.Text = ClsTF.TicketFee
        cboTFTicketFeeCCY.SelectedValue = ClsTF.TicketFeeCCY
        txtTFTransFee.Text = ClsTF.TransFee
        cboTFTransFeeCCY.SelectedValue = ClsTF.TransFeeCCY
        txtTFFOPFee.Text = ClsTF.FOPFee
        cboTFFOPFeeCCY.SelectedValue = ClsTF.FOPFeeCCY
        ChkCoverBrokerage.Checked = ClsTF.CoverBrokerage
        txtTFMin.Text = ClsTF.MinCharge
        txtTFMax.Text = ClsTF.MaxCharge
    End Sub

    Private Sub EditFS()
        DtFSStart.Value = ClsFS.StartDate
        DtFSEnd.Value = ClsFS.EndDate
        cboFSDestination.SelectedValue = ClsFS.DestinationID
        cboFSDestination.Text = ClsFS.Destination
        cboFSPayFreq.SelectedValue = ClsFS.PayFreqID
        cboFSPayFreq.Text = ClsFS.PayFreq
        cboFSIsEAM.SelectedValue = IIf(ClsFS.IsEAM, 1, 0)
        GrpFees.Enabled = True
        GrpFSDetails.Enabled = True
        TabPageMgt.Enabled = True
        TabPageCustody.Enabled = True
        TabPageTransactions.Enabled = True
        If ClsFS.TemplateLinkedID <> 0 Then
            ChkTemplateName.Checked = False
            cboTemplateName.Enabled = False
            cboTemplateName.SelectedValue = ClsFS.TemplateLinkedID
            cboTemplateName.Text = ClsFS.TemplateLinkedName
            GrpFSDetails.Enabled = False
            MGTFeeEnable(False)
            CFFeeEnable(False)
            TFFeeEnable(False)
        End If
        If ClsFS.IsTemplate <> 0 Then
            ChkTemplateName.Checked = False
            cboPortfolio.Text = ClsFS.TemplateName
            cboPortfolio.Enabled = False
        End If
    End Sub

    Private Sub LoadFees()
        If IsNumeric(ClsFS.PortfolioCode) Then
            LoadGridMGTFees()
            LoadGridCustodyFees()
            LoadGridTransactionFees()
            If IsNumeric(ClsFS.PortfolioCode) Then
                If ClsFS.PortfolioCode <> 0 Then
                    LoadGridTransferFees(ClsFS.PortfolioCode, ClsFS.CCYCode)
                    LoadGridEAMClients(ClsFS.PortfolioCode())
                End If
            End If

            Call PopulateCbo(cboMGTChargeableCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies order by CR_Name1")
            Call PopulateCbo(cboMgtType, "Select FS_TypeID, FS_Type from DolfinPaymentFeeSchedulesTypes order by FS_Type")
            Call PopulateCbo(cboMGTCCYEqv, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,7,17) order by CR_Name1")
            Call PopulateCbo(cboCFChargeableCCY, "Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies order by CR_Name1")
            Call PopulateCbo(cboCFType, "Select FS_TypeID, FS_Type from DolfinPaymentFeeSchedulesTypes order by FS_Type")
            Call PopulateCbo(cboCFCCYEqv, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,7,17) order by CR_Name1")
            Call PopulateCbo(cboTFType, "Select FS_TypeID, FS_Type from DolfinPaymentFeeSchedulesTypes order by FS_Type")
            Call PopulateCbo(cboTFCCYEqv, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,7,17) order by CR_Name1")
            Call PopulateCbo(cboTFTicketFeeCCY, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,17) order by CR_Name1")
            Call PopulateCbo(cboTFTransFeeCCY, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,17) order by CR_Name1")
            Call PopulateCbo(cboTFFOPFeeCCY, "Select 0 as CR_Code,'' as CR_Name1 union Select CR_Code, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,6,17) order by CR_Name1")

            If ClsFS.ID = 0 Then
                DtFSStart.Value = Now
                DtFSEnd.Value = DateAdd(DateInterval.Year, 1, Now)
                txtMgtMin.Text = 0
                txtMgtMax.Text = cEndAmt
                txtCFMin.Text = 0
                txtCFMax.Text = cEndAmt
                txtTFMin.Text = 0
                txtTFMax.Text = cEndAmt
                txtMgtMaintMin.Text = 0
                txtCFMaintMin.Text = 0
                cboTFTicketFeeCCY.SelectedValue = 6
                cboTFTransFeeCCY.SelectedValue = 6
                cboTFFOPFeeCCY.SelectedValue = 6

                If ClsFS.PortfolioCode >= 0 Then
                    GrpFees.Enabled = True
                Else
                    GrpFees.Enabled = False
                End If
            Else
                EditMgtFee()
                EditCF()
                EditTF()
                EditFS()
            End If
        End If
    End Sub

    Public Sub RefreshMGTRanges()
        dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows.Clear()
        Dim row As DataRow
        row = dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").NewRow
        row("From Amount") = 0
        row("To Amount") = cEndAmt
        row("Commission %") = "0.0"
        dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows.Add(row)
    End Sub

    Public Sub RefreshCFRanges()
        dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Clear()
        Dim row As DataRow
        row = dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").NewRow
        row("Asset Type") = 0
        row("From Amount") = 0
        row("To Amount") = cEndAmt
        row("Commission %") = "0.0"
        dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Add(row)
    End Sub

    Public Sub RefreshTFRanges()
        dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows.Clear()
        Dim row As DataRow
        row = dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").NewRow
        row("Asset Type") = 0
        row("From Amount") = 0
        row("To Amount") = cEndAmt
        row("Commission %") = "0.0"
        dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows.Add(row)
    End Sub


    Public Sub LoadFeesMGT(ByVal varID As Double)
        If varID <> 0 Then
            Dim Lst = ClsIMS.GetDataToList("select MGT_ChargeableCCYCode,MGT_TypeID,MGT_MinCharge,MGT_MaxCharge,MGT_MinMaintCharge,MGT_CCYEqvCode from vwDolfinPaymentManagementFeeLinks where MGT_ID = " & varID)
            If Not Lst Is Nothing Then
                ChkMgtFees.Checked = True
                'this should always be portfolioccy
                'cboMGTChargeableCCY.SelectedValue = Lst.Item("MGT_ChargeableCCYCode").ToString
                cboMgtType.SelectedValue = Lst.Item("MGT_TypeID").ToString
                txtMgtMin.Text = Lst.Item("MGT_MinCharge").ToString
                txtMgtMax.Text = Lst.Item("MGT_MaxCharge").ToString
                txtMgtMaintMin.Text = Lst.Item("MGT_MinMaintCharge").ToString
                cboMGTCCYEqv.SelectedValue = Lst.Item("MGT_CCYEqvCode").ToString

                LoadGridMGTRangesData(varID)
                LoadGridMGTExceptionData(varID)
            Else
                RefreshMGTRanges()
                dgvMgtExcGrp.Rows.Clear()
                dgvMgtExcIns.Rows.Clear()
                ChkMgtFees.Checked = False
            End If
        Else
            RefreshMGTRanges()
            dgvMgtExcGrp.Rows.Clear()
            dgvMgtExcIns.Rows.Clear()
            ChkMgtFees.Checked = False
        End If
    End Sub

    Public Sub LoadFeesCF(ByVal varID As Double)
        If varID <> 0 Then
            Dim Lst = ClsIMS.GetDataToList("Select CF_TypeID, CF_ChargeableCCYCode,CF_CCYEqvCode,CF_MinCharge,CF_MaxCharge,CF_MinMaintCharge from vwDolfinPaymentCustodyFeeLinks where CF_ID = " & varID)
            If Not Lst Is Nothing Then
                ChkCF.Checked = True
                cboCFType.SelectedValue = Lst.Item("CF_TypeID").ToString
                'this should always be portfolioccy
                'cboCFChargeableCCY.SelectedValue = Lst.Item("CF_ChargeableCCYCode").ToString
                cboCFCCYEqv.SelectedValue = Lst.Item("CF_CCYEqvCode").ToString
                txtCFMin.Text = Lst.Item("CF_MinCharge").ToString
                txtCFMax.Text = Lst.Item("CF_MaxCharge").ToString
                txtCFMaintMin.Text = Lst.Item("CF_MinMaintCharge").ToString

                LoadGridCFRangesData(varID)
                LoadGridCFSafekeepingData(varID)
                LoadGridCFExceptionData(varID)
            Else
                'dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Clear()
                RefreshCFRanges()
                dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping").Rows.Clear()
                dgvCFExcGrp.Rows.Clear()
                dgvCFExcIns.Rows.Clear()
                ChkCF.Checked = False
            End If
        Else
            'dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Clear()
            RefreshCFRanges()
            dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping").Rows.Clear()
            dgvCFExcGrp.Rows.Clear()
            dgvCFExcIns.Rows.Clear()
            ChkCF.Checked = False
        End If
    End Sub

    Public Sub LoadFeesTF(ByVal varID As Double)
        If varID <> 0 Then
            Dim Lst = ClsIMS.GetDataToList("Select TF_TypeID,TF_CCYEqvCode,TF_TicketFee,TF_TicketFeeCCYCode,TF_TransFee,TF_TransFeeCCYCode,TF_FOPFee,TF_FOPFeeCCYCode,TF_CoverBrokerage,TF_MinCharge,TF_MaxCharge from vwDolfinPaymentTransactionFeeLinks where TF_ID = " & varID)
            If Not Lst Is Nothing Then
                ChkTF.Checked = True
                If IsNumeric(Lst.Item("TF_TypeID")) Then
                    cboTFType.SelectedValue = Lst.Item("TF_TypeID")
                End If
                If IsNumeric(Lst.Item("TF_CCYEqvCode")) Then
                    cboTFCCYEqv.SelectedValue = Lst.Item("TF_CCYEqvCode")
                End If
                If IsNumeric(Lst.Item("TF_TicketFee")) Then
                    txtTFTicketFee.Text = Lst.Item("TF_TicketFee").ToString
                End If
                If IsNumeric(Lst.Item("TF_TicketFeeCCYCode")) Then
                    cboTFTicketFeeCCY.SelectedValue = Lst.Item("TF_TicketFeeCCYCode")
                End If
                If IsNumeric(Lst.Item("TF_TransFee")) Then
                    txtTFTransFee.Text = Lst.Item("TF_TransFee").ToString
                End If
                If IsNumeric(Lst.Item("TF_TransFeeCCYCode")) Then
                    cboTFTransFeeCCY.SelectedValue = Lst.Item("TF_TransFeeCCYCode")
                End If
                If IsNumeric(Lst.Item("TF_FOPFee")) Then
                    txtTFFOPFee.Text = Lst.Item("TF_FOPFee").ToString
                End If
                If IsNumeric(Lst.Item("TF_FOPFeeCCYCode")) Then
                    cboTFFOPFeeCCY.SelectedValue = Lst.Item("TF_FOPFeeCCYCode")
                End If
                If IsNumeric(Lst.Item("TF_CoverBrokerage")) Then
                    ChkCoverBrokerage.Checked = Lst.Item("TF_CoverBrokerage")
                End If
                If IsNumeric(Lst.Item("TF_MinCharge")) Then
                    txtTFMin.Text = Lst.Item("TF_MinCharge").ToString
                End If
                If IsNumeric(Lst.Item("TF_MaxCharge")) Then
                    txtTFMax.Text = Lst.Item("TF_MaxCharge").ToString
                End If

                LoadGridTFRangesData(varID)
                LoadGridTFSafekeepingData(varID)
                LoadGridTFSafekeepingDataFixed(varID)
                LoadGridTFExceptionData(varID)
            Else
                RefreshTFRanges()
                dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping").Rows.Clear()
                dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed").Rows.Clear()
                ChkTF.Checked = False
            End If
        Else
            RefreshTFRanges()
            dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping").Rows.Clear()
            dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed").Rows.Clear()
            ChkTF.Checked = False
        End If
    End Sub

    Public Sub LoadFeesFS(ByVal varID As Double)
        If varID <> 0 Then
            Dim Lst = ClsIMS.GetDataToList("Select FS_StartDate, FS_EndDate, FS_DestinationID, FS_Destination, FS_PayFreqID, FS_PayFreq from vwDolfinPaymentFeeSchedulesLinks where FS_ID = " & varID)
            If Not Lst Is Nothing Then
                DtFSStart.Value = Lst.Item("FS_StartDate").ToString
                DtFSEnd.Value = Lst.Item("FS_EndDate").ToString
                cboFSDestination.SelectedValue = Lst.Item("FS_DestinationID").ToString
                cboFSDestination.Text = Lst.Item("FS_Destination").ToString
                cboFSPayFreq.SelectedValue = Lst.Item("FS_PayFreqID").ToString
                cboFSPayFreq.Text = Lst.Item("FS_PayFreq").ToString
            End If
        End If
    End Sub

    Private Sub LoadGridMGTFees(Optional ByVal varFilter As String = "")
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            If ClsMgtFees.ID <> 0 Then
                varSQL = "Select MGT_RangeFrom As [From Amount], MGT_RangeTo As [To Amount], MGT_Commission As [Commission %] from vwDolfinPaymentManagementFeeRanges where Mgt_ID = " & ClsMgtFees.ID & " order by mgt_order,[From Amount]"
            Else
                varSQL = "Select 0 As [From Amount], " & cEndAmt & " As [To Amount], 0.0 As [Commission %]"
            End If

            dgvMgtRange.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsMGTRange = New DataSet
            dgvdata.Fill(dsMGTRange, "vwDolfinPaymentManagementFeeRanges")
            dgvMgtRange.AutoGenerateColumns = True
            dgvMgtRange.DataSource = dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges")

            dgvMgtRange.Columns("From Amount").ReadOnly = True
            dgvMgtRange.Columns("From Amount").DefaultCellStyle.BackColor = Color.LightGray
            dgvMgtRange.Columns("From Amount").Width = 140
            dgvMgtRange.Columns("From Amount").DefaultCellStyle.Format = "##,0"
            dgvMgtRange.Columns("To Amount").Width = 140
            dgvMgtRange.Columns("To Amount").DefaultCellStyle.Format = "##,0"
            dgvMgtRange.Columns("Commission %").Width = 100

            For Each column In dgvMgtRange.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            Next

            dgvMgtRange.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvMgtRange.Refresh()

            varSQL = "Select 0 As TC_Code, '' as TC_Name union select TC_Code, TC_Name from vwDolfinPaymentTitleCategory where TC_Code not in (7,8) order by TC_Name"
            dgvdataException = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsExceptionGrp = New DataSet
            dgvdataException.Fill(dsExceptionGrp, "InstrumentGroup")
            cboExceptionColumn = New DataGridViewComboBoxColumn
            cboExceptionColumn.HeaderText = "Instrument Group"
            cboExceptionColumn.DataSource = dsExceptionGrp.Tables("InstrumentGroup")
            cboExceptionColumn.ValueMember = dsExceptionGrp.Tables("InstrumentGroup").Columns(0).ColumnName
            cboExceptionColumn.DisplayMember = dsExceptionGrp.Tables("InstrumentGroup").Columns(1).ColumnName
            cboExceptionColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboExceptionColumn.FlatStyle = FlatStyle.Flat
            If dgvMgtExcGrp.Columns.Count > 0 Then
                dgvMgtExcGrp.Columns.RemoveAt(0)
            End If
            dgvMgtExcGrp.Columns.Insert(0, cboExceptionColumn)
            dgvMgtExcGrp.Columns(0).Width = 380

            For Each column In dgvMgtExcGrp.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
            Next

            dgvMgtExcGrp.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvMgtExcGrp.Refresh()

            varSQL = "select 0 as t_code, '' as t_name1 union select t_code, t_name1 from titles where t_type not in (0,6,7,8,13,19) order by t_name1"
            dgvdataException = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsException = New DataSet
            dgvdataException.Fill(dsException, "Instrument")
            cboExceptionColumn = New DataGridViewComboBoxColumn
            cboExceptionColumn.HeaderText = "Instrument"
            cboExceptionColumn.DataSource = dsException.Tables("Instrument")
            cboExceptionColumn.ValueMember = dsException.Tables("Instrument").Columns(0).ColumnName
            cboExceptionColumn.DisplayMember = dsException.Tables("Instrument").Columns(1).ColumnName
            cboExceptionColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboExceptionColumn.FlatStyle = FlatStyle.Flat
            If dgvMgtExcIns.Columns.Count > 0 Then
                dgvMgtExcIns.Columns.RemoveAt(0)
            End If
            dgvMgtExcIns.Columns.Insert(0, cboExceptionColumn)

            dgvMgtExcIns.Columns(0).Width = 380

            For Each column In dgvMgtExcIns.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
            Next

            dgvMgtExcIns.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvMgtExcIns.Refresh()

            LoadGridMGTExceptionData(ClsMgtFees.ID)

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvMgtRange.DataSource = Nothing

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with viewing management fees grids")
        End Try
    End Sub

    Private Sub LoadGridCustodyFees(Optional ByVal varFilter As String = "")
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor

            If ClsCF.ID <> 0 Then
                varSQL = "select CF_TType as [Asset Type], CF_RangeFrom as [From Amount], CF_RangeTo as [To Amount], CF_Commission as [Commission %] from vwDolfinPaymentcustodyFeeRanges where CF_ID = " & ClsCF.ID & " order by cf_order,[Asset Type],[From Amount]"
            Else
                varSQL = "select 0 as [Asset Type], 0 as [From Amount]," & cEndAmt & " as [To Amount], 0.0 as [Commission %] order by [Asset Type],[From Amount]"
            End If

            dgvCFRange.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFRange = New DataSet
            dgvdata.Fill(dsCFRange, "vwDolfinPaymentCustodyFeeRanges")
            dgvCFRange.AutoGenerateColumns = True
            dgvCFRange.DataSource = dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges")

            dgvCFRange.Columns("Asset Type").Visible = False
            dgvCFRange.Columns("From Amount").ReadOnly = True
            dgvCFRange.Columns("From Amount").DefaultCellStyle.BackColor = Color.LightGray
            dgvCFRange.Columns("From Amount").Width = 140
            dgvCFRange.Columns("To Amount").Width = 140
            dgvCFRange.Columns("Commission %").Width = 100

            varSQL = "select -1 as [Asset Type], '' as TC_Name union select 0 as [Asset Type], 'Default' as TC_Name union select TC_Code as [Asset Type], TC_Name from vwDolfinPaymentTitleCategory where TC_Code not in (7,8) order by TC_Name"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFAssetType = New DataSet
            dgvdata.Fill(dsCFAssetType, "Asset Type")
            cboCFAssetTypeColumn = New DataGridViewComboBoxColumn
            cboCFAssetTypeColumn.HeaderText = "Asset Type"
            cboCFAssetTypeColumn.DataPropertyName = "Asset Type"
            cboCFAssetTypeColumn.DataSource = dsCFAssetType.Tables("Asset Type")
            cboCFAssetTypeColumn.ValueMember = dsCFAssetType.Tables("Asset Type").Columns(0).ColumnName
            cboCFAssetTypeColumn.DisplayMember = dsCFAssetType.Tables("Asset Type").Columns(1).ColumnName
            cboCFAssetTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFAssetTypeColumn.FlatStyle = FlatStyle.Flat
            If dgvCFRange.Columns(0).Name = "" Then
                dgvCFRange.Columns.RemoveAt(0)
            End If
            dgvCFRange.Columns.Insert(0, cboCFAssetTypeColumn)
            dgvCFRange.Columns(0).Width = 120
            dgvCFRange.Columns(1).Width = 100
            dgvCFRange.Columns(1).DefaultCellStyle.Format = "##,0"
            dgvCFRange.Columns(2).Width = 100
            dgvCFRange.Columns(2).DefaultCellStyle.Format = "##,0"
            dgvCFRange.Columns(3).Width = 100
            dgvCFRange.Columns(3).DefaultCellStyle.Format = "##,0"
            dgvCFRange.Columns(4).Width = 95

            For Each column In dgvCFRange.Columns
                If column.index > 0 Then
                    column.sortmode = DataGridViewColumnSortMode.NotSortable
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvCFRange.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCFRange.Refresh()

            If ClsCF.ID <> 0 Then
                varSQL = "select CF_ClearerCode as Safekeeping, CF_Commission as Commission,CF_MovementFee as [Movement Fee],CF_MovementFeeCCYCode as [Movement CCY] from vwDolfinPaymentCustodySafekeeping where CF_ID = " & ClsCF.ID ' & " order by GRT_Name"
            Else
                varSQL = "select null as Safekeeping, 0.0 as Commission,0 as [Movement Fee], 6 as [Movement CCY]"
            End If

            dgvCFSK.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFSK = New DataSet
            dgvdata.Fill(dsCFSK, "vwDolfinPaymentCustodySafekeeping")
            dgvCFSK.AutoGenerateColumns = True
            dgvCFSK.DataSource = dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping")

            dgvCFSK.Columns("Safekeeping").Visible = False
            dgvCFSK.Columns("Commission").Width = 70
            dgvCFSK.Columns("Movement Fee").Width = 70

            varSQL = "Select null As Safekeeping, null as ClearerName union Select GRT_Code as Safekeeping, GRT_Name as ClearerName from General_Ref_Table where GRT_Code >= 1000 group by GRT_Code,GRT_Name order by ClearerName"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFSKClearer = New DataSet
            dgvdata.Fill(dsCFSKClearer, "Safekeeping")
            cboCFSKColumn = New DataGridViewComboBoxColumn
            cboCFSKColumn.HeaderText = "Safe keeping"
            cboCFSKColumn.DataPropertyName = "Safekeeping"
            cboCFSKColumn.DataSource = dsCFSKClearer.Tables("Safekeeping")
            cboCFSKColumn.ValueMember = dsCFSKClearer.Tables("Safekeeping").Columns(0).ColumnName
            cboCFSKColumn.DisplayMember = dsCFSKClearer.Tables("Safekeeping").Columns(1).ColumnName
            cboCFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFSKColumn.FlatStyle = FlatStyle.Flat
            If dgvCFSK.Columns(0).Name = "" Then
                dgvCFSK.Columns.RemoveAt(0)
            End If
            dgvCFSK.Columns.Insert(0, cboCFSKColumn)
            dgvCFSK.Columns(0).Width = 250

            For Each column In dgvCFSK.Columns
                If column.index > 0 Then
                    column.sortmode = DataGridViewColumnSortMode.NotSortable
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select CR_Code as [Movement CCY], CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,17,6)", FeesConn)
            dsCCY = New DataSet
            dgvdataCCY.Fill(dsCCY, "vwDolfinPaymentCurrencies")
            cboCFSKColumn = New DataGridViewComboBoxColumn
            cboCFSKColumn.HeaderText = "Movement CCY"
            cboCFSKColumn.DataPropertyName = "Movement CCY"
            cboCFSKColumn.DataSource = dsCCY.Tables("vwDolfinPaymentCurrencies")
            cboCFSKColumn.ValueMember = dsCCY.Tables("vwDolfinPaymentCurrencies").Columns(0).ColumnName
            cboCFSKColumn.DisplayMember = dsCCY.Tables("vwDolfinPaymentCurrencies").Columns(1).ColumnName
            cboCFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFSKColumn.FlatStyle = FlatStyle.Flat
            dgvCFSK.Columns.Insert(4, cboCFSKColumn)
            dgvCFSK.Columns("Movement CCY").Visible = False

            dgvCFSK.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCFSK.Refresh()

            varSQL = "select 0 as TC_Code, '' as TC_Name union select TC_Code, TC_Name from vwDolfinPaymentTitleCategory where TC_Code not in (7,8) order by TC_Name"
            dgvdataCFException = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFExceptionGrp = New DataSet
            dgvdataCFException.Fill(dsCFExceptionGrp, "InstrumentGroup")
            cboCFExceptionColumn = New DataGridViewComboBoxColumn
            cboCFExceptionColumn.HeaderText = "Instrument Group"
            cboCFExceptionColumn.DataSource = dsCFExceptionGrp.Tables("InstrumentGroup")
            cboCFExceptionColumn.ValueMember = dsCFExceptionGrp.Tables("InstrumentGroup").Columns(0).ColumnName
            cboCFExceptionColumn.DisplayMember = dsCFExceptionGrp.Tables("InstrumentGroup").Columns(1).ColumnName
            cboCFExceptionColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFExceptionColumn.FlatStyle = FlatStyle.Flat
            If dgvCFExcGrp.Columns.Count > 0 Then
                dgvCFExcGrp.Columns.RemoveAt(0)
            End If
            dgvCFExcGrp.Columns.Insert(0, cboCFExceptionColumn)
            dgvCFExcGrp.Columns(0).Width = 380

            For Each column In dgvCFExcGrp.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
            Next

            dgvCFExcGrp.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCFExcGrp.Refresh()

            varSQL = "select 0 as t_code, '' as t_name1 union select t_code, t_name1 from titles where t_type not in (0,6,7,8,13,19) order by t_name1"
            dgvdataCFException = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFException = New DataSet
            dgvdataCFException.Fill(dsCFException, "Instrument")
            cboCFExceptionColumn = New DataGridViewComboBoxColumn
            cboCFExceptionColumn.HeaderText = "Instrument"
            cboCFExceptionColumn.DataSource = dsCFException.Tables("Instrument")
            cboCFExceptionColumn.ValueMember = dsCFException.Tables("Instrument").Columns(0).ColumnName
            cboCFExceptionColumn.DisplayMember = dsCFException.Tables("Instrument").Columns(1).ColumnName
            cboCFExceptionColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFExceptionColumn.FlatStyle = FlatStyle.Flat
            If dgvCFExcIns.Columns.Count > 0 Then
                dgvCFExcIns.Columns.RemoveAt(0)
            End If
            dgvCFExcIns.Columns.Insert(0, cboCFExceptionColumn)

            dgvCFExcIns.Columns(0).Width = 380

            For Each column In dgvCFExcIns.Columns
                column.sortmode = DataGridViewColumnSortMode.NotSortable
            Next

            dgvCFExcIns.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCFExcIns.Refresh()

            LoadGridCFExceptionData(ClsCF.ID)

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvCFRange.DataSource = Nothing

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with viewing custody fees grids")
        End Try
    End Sub

    Private Sub LoadGridTransactionFees(Optional ByVal varFilter As String = "")
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            If ClsTF.ID <> 0 Then
                varSQL = "select TF_TType as [Asset Type], TF_RangeFrom as [From Amount], TF_RangeTo as [To Amount], TF_Commission as [Commission %] from vwDolfinPaymentTransactionFeeRanges where TF_ID = " & ClsTF.ID & " order by TF_order,[Asset Type],[From Amount]"
            Else
                varSQL = "select 0 as [Asset Type], 0 as [From Amount]," & cEndAmt & " as [To Amount], 0.0 as [Commission %]"
            End If

            dgvTFRange.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsTFRange = New DataSet
            dgvdata.Fill(dsTFRange, "vwDolfinPaymentTransactionFeeRanges")
            dgvTFRange.AutoGenerateColumns = True
            dgvTFRange.DataSource = dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges")

            dgvTFRange.Columns("Asset Type").Visible = False
            dgvTFRange.Columns("From Amount").ReadOnly = True
            dgvTFRange.Columns("From Amount").DefaultCellStyle.BackColor = Color.LightGray
            dgvTFRange.Columns("From Amount").Width = 140
            dgvTFRange.Columns("To Amount").Width = 140
            dgvTFRange.Columns("Commission %").Width = 100

            '----------------------------------------------------------------------------------
            varSQL = "select 0 as [Asset Type], '' as TFT_Description union select TFT_ID as [Asset Type], TFT_Description from DolfinPaymentTransactionFeeTypes order by TFT_Description"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsCFAssetType = New DataSet
            dgvdata.Fill(dsCFAssetType, "Asset Type")
            cboCFAssetTypeColumn = New DataGridViewComboBoxColumn
            cboCFAssetTypeColumn.HeaderText = "Asset Type"
            cboCFAssetTypeColumn.DataPropertyName = "Asset Type"
            cboCFAssetTypeColumn.DataSource = dsCFAssetType.Tables("Asset Type")
            cboCFAssetTypeColumn.ValueMember = dsCFAssetType.Tables("Asset Type").Columns(0).ColumnName
            cboCFAssetTypeColumn.DisplayMember = dsCFAssetType.Tables("Asset Type").Columns(1).ColumnName
            cboCFAssetTypeColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCFAssetTypeColumn.FlatStyle = FlatStyle.Flat
            If dgvTFRange.Columns(0).Name = "" Then
                dgvTFRange.Columns.RemoveAt(0)
            End If
            dgvTFRange.Columns.Insert(0, cboCFAssetTypeColumn)
            dgvTFRange.Columns(0).Width = 170
            dgvTFRange.Columns(1).Width = 100
            dgvTFRange.Columns(1).DefaultCellStyle.Format = "##,0"
            dgvTFRange.Columns(2).Width = 100
            dgvTFRange.Columns(2).DefaultCellStyle.Format = "##,0"
            dgvTFRange.Columns(3).Width = 95
            dgvCFRange.Columns(3).DefaultCellStyle.Format = "##,0"

            For Each column In dgvTFRange.Columns
                If column.index > 0 Then
                    column.sortmode = DataGridViewColumnSortMode.NotSortable
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvTFRange.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTFRange.Refresh()

            '----------------------------------------------------------------------------------
            If ClsTF.ID <> 0 Then
                varSQL = "select TF_ClearerCode as Safekeeping, TF_Commission as Commission, TF_TicketFee as TicketFee,TF_TicketFeeCCYCode as TicketFeeCCY from vwDolfinPaymentTransactionFeeSafekeeping where TF_ID = " & ClsTF.ID '& " order by GRT_Name"
            Else
                varSQL = "select null as Safekeeping, null as Commission, 0 as TicketFee, 6 as TicketFeeCCY"
            End If

            dgvTFSK.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsTFSK = New DataSet
            dgvdata.Fill(dsTFSK, "vwDolfinPaymentTransactionFeeSafekeeping")
            dgvTFSK.AutoGenerateColumns = True
            dgvTFSK.DataSource = dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping")

            dgvTFSK.Columns("Safekeeping").Visible = False
            dgvTFSK.Columns("Commission").Width = 70
            dgvTFSK.Columns("TicketFee").Width = 70

            varSQL = "Select null As Safekeeping, null as ClearerName union Select GRT_Code as Safekeeping, GRT_Name as ClearerName from General_Ref_Table where GRT_Code >= 1000 group by GRT_Code,GRT_Name order by ClearerName"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsTFSKClearer = New DataSet
            dgvdata.Fill(dsTFSKClearer, "Safekeeping")
            cboTFSKColumn = New DataGridViewComboBoxColumn
            cboTFSKColumn.HeaderText = "Safe keeping (Variable)"
            cboTFSKColumn.DataPropertyName = "Safekeeping"
            cboTFSKColumn.DataSource = dsTFSKClearer.Tables("Safekeeping")
            cboTFSKColumn.ValueMember = dsTFSKClearer.Tables("Safekeeping").Columns(0).ColumnName
            cboTFSKColumn.DisplayMember = dsTFSKClearer.Tables("Safekeeping").Columns(1).ColumnName
            cboTFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboTFSKColumn.FlatStyle = FlatStyle.Flat
            If dgvTFSK.Columns(0).Name = "" Then
                dgvTFSK.Columns.RemoveAt(0)
            End If
            dgvTFSK.Columns.Insert(0, cboTFSKColumn)
            dgvTFSK.Columns(0).Width = 250

            For Each column In dgvTFSK.Columns
                If column.index > 0 Then
                    column.sortmode = DataGridViewColumnSortMode.NotSortable
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next

            dgvdata = New SqlClient.SqlDataAdapter("Select CR_Code as TicketFeeCCY, CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,17,6)", FeesConn)
            dsCCY = New DataSet
            dgvdata.Fill(dsCCY, "vwDolfinPaymentCurrenciesTF")
            cboTFSKColumn = New DataGridViewComboBoxColumn
            cboTFSKColumn.HeaderText = "TicketFee CCY"
            cboTFSKColumn.DataPropertyName = "TicketFeeCCY"
            cboTFSKColumn.DataSource = dsCCY.Tables("vwDolfinPaymentCurrenciesTF")
            cboTFSKColumn.ValueMember = dsCCY.Tables("vwDolfinPaymentCurrenciesTF").Columns(0).ColumnName
            cboTFSKColumn.DisplayMember = dsCCY.Tables("vwDolfinPaymentCurrenciesTF").Columns(1).ColumnName
            cboTFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboTFSKColumn.FlatStyle = FlatStyle.Flat
            dgvTFSK.Columns.Insert(4, cboTFSKColumn)
            dgvTFSK.Columns("TicketFeeCCY").Visible = False



            dgvTFSK.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTFSK.Refresh()

            '----------------------------------------------------------------------------------
            If ClsTF.ID <> 0 Then
                varSQL = "select TFF_ClearerCode as Safekeeping, TFF_Commission as Commission, TFF_TicketFee as TicketFee,TFF_TicketFeeCCYCode as TicketFeeCCY from vwDolfinPaymentTransactionFeeSafekeepingFixed where TF_ID = " & ClsTF.ID ' & " order by GRT_Name"
            Else
                varSQL = "select null as Safekeeping, null as Commission, 0 as TicketFee, 6 as TicketFeeCCY"
            End If

            dgvTFSKFixed.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsTFSKFixed = New DataSet
            dgvdata.Fill(dsTFSKFixed, "vwDolfinPaymentTransactionFeeSafekeepingFixed")
            dgvTFSKFixed.AutoGenerateColumns = True
            dgvTFSKFixed.DataSource = dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed")

            dgvTFSKFixed.Columns("Safekeeping").Visible = False
            dgvTFSKFixed.Columns("Commission").Width = 70
            dgvTFSKFixed.Columns("TicketFee").Width = 70

            varSQL = "Select null As Safekeeping, null as ClearerName union Select GRT_Code as Safekeeping, GRT_Name as ClearerName from General_Ref_Table where lower(left(GRT_Name,10))='zz custody' group by GRT_Code,GRT_Name order by ClearerName"
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            dsTFSKClearer = New DataSet
            dgvdata.Fill(dsTFSKClearer, "SafekeepingFixed")
            cboTFSKColumn = New DataGridViewComboBoxColumn
            cboTFSKColumn.HeaderText = "Safe keeping (Fixed)"
            cboTFSKColumn.DataPropertyName = "Safekeeping"
            cboTFSKColumn.DataSource = dsTFSKClearer.Tables("SafekeepingFixed")
            cboTFSKColumn.ValueMember = dsTFSKClearer.Tables("SafekeepingFixed").Columns(0).ColumnName
            cboTFSKColumn.DisplayMember = dsTFSKClearer.Tables("SafekeepingFixed").Columns(1).ColumnName
            cboTFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboTFSKColumn.FlatStyle = FlatStyle.Flat
            If dgvTFSKFixed.Columns(0).Name = "" Then
                dgvTFSKFixed.Columns.RemoveAt(0)
            End If
            dgvTFSKFixed.Columns.Insert(0, cboTFSKColumn)
            dgvTFSKFixed.Columns(0).Width = 250

            For Each column In dgvTFSKFixed.Columns
                If column.index > 0 Then
                    column.sortmode = DataGridViewColumnSortMode.NotSortable
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            Next


            dgvdata = New SqlClient.SqlDataAdapter("Select CR_Code as [TicketFeeCCY], CR_Name1 from vwDolfinPaymentCurrencies where CR_Code in (2,17,6)", FeesConn)
            dsCCY = New DataSet
            dgvdata.Fill(dsCCY, "vwDolfinPaymentCurrenciesTFF")
            cboTFSKColumn = New DataGridViewComboBoxColumn
            cboTFSKColumn.HeaderText = "TicketFee CCY"
            cboTFSKColumn.DataPropertyName = "TicketFeeCCY"
            cboTFSKColumn.DataSource = dsCCY.Tables("vwDolfinPaymentCurrenciesTFF")
            cboTFSKColumn.ValueMember = dsCCY.Tables("vwDolfinPaymentCurrenciesTFF").Columns(0).ColumnName
            cboTFSKColumn.DisplayMember = dsCCY.Tables("vwDolfinPaymentCurrenciesTFF").Columns(1).ColumnName
            cboTFSKColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboTFSKColumn.FlatStyle = FlatStyle.Flat
            dgvTFSKFixed.Columns.Insert(4, cboTFSKColumn)
            dgvTFSKFixed.Columns("TicketFeeCCY").Visible = False




            dgvTFSKFixed.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTFSKFixed.Refresh()

            LoadGridTFExceptionData(ClsTF.ID)

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvTFRange.DataSource = Nothing

            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with viewing transaction fees grids")
        End Try
    End Sub

    Public Sub LoadGridMGTExceptionData(ByVal varID As Double)
        Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select MGT_TType from vwDolfinPaymentManagementFeeExceptions where MGT_ID = " & varID & " and isnull(MGT_TCode,'')=''")

        dgvMgtExcGrp.Rows.Clear()
        If Not Lst Is Nothing Then
            For i As Integer = 0 To Lst.Count - 1
                dgvMgtExcGrp.Rows.Add()
                dgvMgtExcGrp.Rows(i).Cells(0).Value = CInt(Lst.Item(i))
            Next
        End If

        dgvMgtExcIns.Rows.Clear()
        Lst = ClsIMS.GetMultipleDataToList("select MGT_TCode from vwDolfinPaymentManagementFeeExceptions where MGT_ID = " & varID & " and isnull(MGT_TType,'')=''")
        If Not Lst Is Nothing Then
            For i As Integer = 0 To Lst.Count - 1
                dgvMgtExcIns.Rows.Add()
                dgvMgtExcIns.Rows(i).Cells(0).Value = CInt(Lst.Item(i))
            Next
        End If
    End Sub

    Public Sub LoadGridCFExceptionData(ByVal varID As Double)
        Try
            Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select CF_TType from vwDolfinPaymentCustodyFeeExceptions where CF_ID = " & varID & " and isnull(CF_TCode,'')=''")
            dgvCFExcGrp.Rows.Clear()
            If Not Lst Is Nothing Then
                For i As Integer = 0 To Lst.Count - 1
                    dgvCFExcGrp.Rows.Add()
                    dgvCFExcGrp.Rows(i).Cells(0).Value = CInt(Lst.Item(i))
                Next
            End If

            dgvCFExcIns.Rows.Clear()
            Lst = ClsIMS.GetMultipleDataToList("select CF_TCode from vwDolfinPaymentCustodyFeeExceptions where CF_ID = " & varID & " and isnull(CF_TType,'')=''")
            If Not Lst Is Nothing Then
                For i As Integer = 0 To Lst.Count - 1
                    dgvCFExcIns.Rows.Add()
                    dgvCFExcIns.Rows(i).Cells(0).Value = CInt(Lst.Item(i))
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with LoadGridCFExceptionData")
        End Try
    End Sub

    Public Sub LoadGridTFExceptionData(ByVal varID As Double)
        'do not delete
    End Sub

    Public Sub LoadGridMGTRangesData(ByVal varID As Double)
        Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select MGT_RangeFrom as [From Amount], MGT_RangeTo as [To Amount], MGT_Commission as [Commission %] from vwDolfinPaymentManagementFeeRanges where Mgt_ID = " & varID & " order by mgt_order")

        Try
            If Not Lst Is Nothing Then
                dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows.Clear()
                For i As Integer = 0 To Lst.Count - 1 Step 3
                    Dim row As DataRow
                    row = dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").NewRow
                    row("From Amount") = CInt(Lst.Item(i))
                    row("To Amount") = CInt(Lst.Item(i + 1))
                    row("Commission %") = Lst.Item(i + 2)
                    dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows.Add(row)
                    dsMGTRange.AcceptChanges()
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridMGTRangesData")
        End Try
    End Sub

    Public Sub LoadGridCFRangesData(ByVal varID As Double)
        Try
            Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select CF_TType as [Asset Type], CF_RangeFrom as [From Amount], CF_RangeTo as [To Amount], CF_Commission as [Commission %] from vwDolfinPaymentcustodyFeeRanges where CF_ID = " & varID & " order by cf_order")

            dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Clear()
            If Not Lst Is Nothing Then
                Dim varPos As Integer = 1
                For i As Integer = 0 To Lst.Count - 1 Step 4
                    Dim row As DataRow
                    row = dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").NewRow
                    row("Asset Type") = CInt(Lst.Item(i))
                    row("From Amount") = CInt(Lst.Item(i + 1))
                    row("To Amount") = CInt(Lst.Item(i + 2))
                    row("Commission %") = Lst.Item(i + 3)
                    dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.Add(row)
                    dsCFRange.AcceptChanges()
                    varPos = varPos + 1
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridCFRangesData")
        End Try
    End Sub

    Public Sub LoadGridTFRangesData(ByVal varID As Double)
        Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("Select TF_TType As [Asset Type], TF_RangeFrom As [From Amount], TF_RangeTo As [To Amount], TF_Commission As [Commission %] from vwDolfinPaymentTransactionFeeRanges where TF_ID = " & varID & " order by TF_order")

        Try
            dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows.Clear()
            If Not Lst Is Nothing Then
                Dim varPos As Integer = 1
                For i As Integer = 0 To Lst.Count - 1 Step 4
                    Dim row As DataRow
                    row = dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").NewRow
                    row("Asset Type") = CInt(Lst.Item(i))
                    row("From Amount") = CInt(Lst.Item(i + 1))
                    row("To Amount") = CInt(Lst.Item(i + 2))
                    row("Commission %") = Lst.Item(i + 3)
                    dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows.Add(row)
                    dsTFRange.AcceptChanges()
                    varPos = varPos + 1
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridTFRangesData")
        End Try
    End Sub

    Public Sub LoadGridCFSafekeepingData(ByVal varID As Double)
        Try
            Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select CF_ClearerCode as Safekeeping, CF_Commission as Commission,CF_MovementFee as [Movement Fee],CF_MovementFeeCCYCode as [Movement CCY] from vwDolfinPaymentCustodySafekeeping where CF_ID = " & varID)
            If Not Lst Is Nothing Then
                Dim varPos As Integer = 1
                dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping").Rows.Clear()
                For i As Integer = 0 To Lst.Count - 1 Step 4
                    Dim row As DataRow
                    row = dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping").NewRow
                    row("Safekeeping") = CInt(Lst.Item(i))
                    row("Commission") = CDbl(Lst.Item(i + 1))
                    row("Movement Fee") = CInt(Lst.Item(i + 2))
                    row("Movement CCY") = CInt(Lst.Item(i + 3))
                    dsCFSK.Tables("vwDolfinPaymentCustodySafekeeping").Rows.Add(row)
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridCFSafekeepingData")
        End Try
    End Sub

    Public Sub LoadGridTFSafekeepingData(ByVal varID As Double)
        Try
            Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select TF_ClearerCode as Safekeeping, TF_Commission as Commission, TF_TicketFee as TicketFee, TF_TicketFeeCCYCode as TicketFeeCCY from vwDolfinPaymentTransactionFeeSafekeeping where TF_ID = " & varID)
            If Not Lst Is Nothing Then
                Dim varPos As Integer = 1
                dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping").Rows.Clear()
                For i As Integer = 0 To Lst.Count - 1 Step 4
                    Dim row As DataRow
                    row = dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping").NewRow
                    row("Safekeeping") = CInt(Lst.Item(i))
                    row("Commission") = CDbl(Lst.Item(i + 1))
                    row("TicketFee") = CDbl(Lst.Item(i + 2))
                    row("TicketFeeCCY") = CDbl(Lst.Item(i + 3))
                    dsTFSK.Tables("vwDolfinPaymentTransactionFeeSafekeeping").Rows.Add(row)
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridTFSafekeepingData")
        End Try
    End Sub

    Public Sub LoadGridTFSafekeepingDataFixed(ByVal varID As Double)
        Try
            Dim Lst As List(Of String) = ClsIMS.GetMultipleDataToList("select TFF_ClearerCode as Safekeeping, TFF_Commission as Commission, TFF_TicketFee as TicketFee, TFF_TicketFeeCCYCode as TicketFeeCCY from vwDolfinPaymentTransactionFeeSafekeepingFixed where TF_ID = " & varID)
            If Not Lst Is Nothing Then
                Dim varPos As Integer = 1
                dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed").Rows.Clear()
                For i As Integer = 0 To Lst.Count - 1 Step 4
                    Dim row As DataRow
                    row = dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed").NewRow
                    row("Safekeeping") = CInt(Lst.Item(i))
                    row("Commission") = CDbl(Lst.Item(i + 1))
                    row("TicketFee") = CDbl(Lst.Item(i + 2))
                    row("TicketFeeCCY") = CDbl(Lst.Item(i + 3))
                    dsTFSKFixed.Tables("vwDolfinPaymentTransactionFeeSafekeepingFixed").Rows.Add(row)
                Next
            End If
        Catch Ex As Exception
            Cursor = Cursors.Default
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with LoadGridTFSafekeepingDataFixed")
        End Try
    End Sub

    Private Sub LoadGridTransferFees(ByVal varPortfolioCode As Double, varCCYCode As Double)
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "select * from vwDolfinPaymentTransferFees where PF_Code = " & varPortfolioCode & " order by Portfolio"

            dgvTrans.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentTransferFees")
            dgvTrans.AutoGenerateColumns = True
            dgvTrans.DataSource = ds.Tables("vwDolfinPaymentTransferFees")

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'EUR' as TF_CCY union Select 'GBP' as TF_CCY", FeesConn)
            dgvdataCCY.Fill(ds, "CCY")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "CCY"
            cboCCYColumn.DataPropertyName = "TF_CCY"
            cboCCYColumn.DataSource = ds.Tables("CCY")
            cboCCYColumn.ValueMember = ds.Tables("CCY").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            dgvTrans.Columns.Insert(6, cboCCYColumn)

            dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'EUR' as TF_CCY union Select 'GBP' as TF_CCY", FeesConn)
            dgvdataCCY.Fill(ds, "Int CCY")
            cboCCYColumn = New DataGridViewComboBoxColumn
            cboCCYColumn.HeaderText = "Int CCY"
            cboCCYColumn.DataPropertyName = "TF_Int_CCY"
            cboCCYColumn.DataSource = ds.Tables("Int CCY")
            cboCCYColumn.ValueMember = ds.Tables("Int CCY").Columns(0).ColumnName
            cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboCCYColumn.FlatStyle = FlatStyle.Flat
            dgvTrans.Columns.Insert(9, cboCCYColumn)

            dgvTrans.Columns(0).Width = 400
            dgvTrans.Columns(0).ReadOnly = True
            dgvTrans.Columns(1).Visible = False
            dgvTrans.Columns(2).Visible = False
            dgvTrans.Columns(3).Visible = False
            dgvTrans.Columns(4).Visible = False
            dgvTrans.Columns(5).HeaderText = "External Fee"
            dgvTrans.Columns(5).Width = 100
            dgvTrans.Columns(6).Width = 100
            dgvTrans.Columns(6).HeaderText = "External CCY"
            dgvTrans.Columns(7).Visible = False
            dgvTrans.Columns(8).Width = 100
            dgvTrans.Columns(8).HeaderText = "External International Fee"
            dgvTrans.Columns(9).Width = 100
            dgvTrans.Columns(9).HeaderText = "External International CCY"
            dgvTrans.Columns(10).HeaderText = "Modified By"
            dgvTrans.Columns(11).HeaderText = "Modified Time"

            dgvTrans.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvTrans.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvTrans.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing transfer fees grid")
        End Try
    End Sub

    Private Sub LoadGridEAMClients(ByVal varPortfolioCode As Double)
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "exec DolfinPaymentGetEAMClients " & varPortfolioCode

            dgvEAMClients.DataSource = Nothing
            FeesConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, FeesConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentGetEAMClients")
            dgvEAMClients.AutoGenerateColumns = True
            dgvEAMClients.DataSource = ds.Tables("vwDolfinPaymentGetEAMClients")

            If Not ds.Tables("vwDolfinPaymentGetEAMClients") Is Nothing Then
                dgvEAMClients.Columns("ClientCode").Width = 100
                dgvEAMClients.Columns("ClientName").Width = 200
                dgvEAMClients.Columns("ID").Width = 60
                dgvEAMClients.Columns("ShortCode").Width = 100
                dgvEAMClients.Columns("Portfolio").Width = 250
                dgvEAMClients.Columns("PortfolioCCY").Width = 80
                dgvEAMClients.Columns("PortfolioType").Width = 100
                dgvEAMClients.Columns("RM1").Width = 120
                dgvEAMClients.Columns("RM2").Width = 120

                dgvEAMClients.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
                dgvEAMClients.Refresh()
            End If
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvEAMClients.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with viewing EAM clients grid")
        End Try
    End Sub

    Private Sub AssignCbo()
        cboPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        CboCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        cboMgtType.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboMgtType.AutoCompleteSource = AutoCompleteSource.ListItems
    End Sub

    Private Sub FrmFees_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim varPortfolioCode As Double, varCCYCode As Double
        varPortfolioCode = ClsFS.PortfolioCode
        varCCYCode = ClsFS.CCYCode
        GrpFees.Enabled = False

        Call AssignCbo()
        ModRMS.DoubleBuffered(dgvMgtRange, True)
        ModRMS.DoubleBuffered(dgvMgtExcGrp, True)
        ModRMS.DoubleBuffered(dgvCFRange, True)
        ModRMS.DoubleBuffered(dgvTrans, True)
        ModRMS.DoubleBuffered(dgvEAMClients, True)

        GrpMgtFees.Enabled = IIf(ClsFS.MGTID <> 0, True, False)
        ChkMgtFees.Checked = IIf(ClsFS.MGTID <> 0, True, False)
        GrpCF.Enabled = IIf(ClsFS.CFID <> 0, True, False)
        ChkCF.Checked = IIf(ClsFS.CFID <> 0, True, False)
        GrpTF.Enabled = IIf(ClsFS.TFID <> 0, True, False)
        ChkTF.Checked = IIf(ClsFS.TFID <> 0, True, False)

        Call PopulateCbo(cboFSDestination, "Select FS_DestinationID,FS_Destination FROM DolfinPaymentFeeSchedulesDestination order by FS_Destination")
        Call PopulateCbo(cboFSPayFreq, "Select FS_PayFreqID,FS_PayFreq FROM DolfinPaymentFeeSchedulesPayFreq order by FS_PayFreqID")
        Call PopulateCbo(cboFSIsEAM, "Select 0,'No' union Select 1, 'Yes'")

        If ClsFS.ID <> 0 Then
            Call PopulateCbo(cboTemplateName, "select fs_id,FS_TemplateName from [dbo].[vwDolfinPaymentFeeSchedulesLinks] where not fs_id is null and FS_Template=1 order by FS_Template desc, Portfolio")
            Call PopulateCbo(cboPortfolio, "Select -1 as pf_Code,'' as Portfolio union Select 0 as pf_Code, '<New Template>' as Portfolio union Select pf_Code,Portfolio from [dbo].[vwDolfinPaymentFeeSchedulesLinks] group by pf_Code, Portfolio order by Portfolio")
            ClsFS.PortfolioCode = varPortfolioCode
            cboPortfolio.SelectedValue = varPortfolioCode
            'Call PopulateCbo(CboCCY, "Select CR_Code, CR_Name1 from [dbo].[vwDolfinPaymentSelectAccount] sa left join [dbo].[vwDolfinPaymentFeeSchedules] FS On sa.PF_Code = FS.FS_PortfolioCode And sa.CR_Code = FS.FS_CCYCode where pf_code = " & ClsFS.PortfolioCode & " group by CR_Code, CR_Name1 order by CR_Name1")
            ClsFS.CCYCode = varCCYCode
            CboCCY.SelectedValue = varCCYCode
            'GrpSelect.Enabled = False
            'cboTemplateName.Enabled = True 
            Call PopulateCbo(cboPortfolioRedirect, "select pf_code,portfolio from vwdolfinpaymentClientportfolio where cust_id = (select cupo_custid from cuporel where cupo_pfcode = " & cboPortfolio.SelectedValue & ") and pf_code <> " & cboPortfolio.SelectedValue & " order by portfolio")
            cboPortfolioRedirect.SelectedValue = ClsFS.PortfolioCodeRedirect
            If ClsFS.PortfolioCodeRedirect <> 0 Then
                ChkRedirectFee.Checked = True
            End If
            LoadFees()
        Else
            Call PopulateCbo(cboPortfolio, "Select -1 as pf_Code,'' as Portfolio union Select 0 as pf_Code, '<New Template>' as Portfolio union Select pf_Code,Portfolio from [dbo].[vwDolfinPaymentPortfolio] group by pf_Code, Portfolio order by Portfolio")
            ClsFS.PortfolioCode = varPortfolioCode
            cboPortfolio.SelectedValue = varPortfolioCode
            If varPortfolioCode = 0 Then
                ChkTemplateName.Checked = True
            Else
                Call PopulateCbo(cboTemplateName, "select fs_id,FS_TemplateName from [dbo].[vwDolfinPaymentFeeSchedulesLinks] where not fs_id is null and FS_Template=1 order by FS_Template desc, Portfolio")
            End If
            GrpSelect.Enabled = True
            MGTFeeEnable(True)
            CFFeeEnable(True)
            TFFeeEnable(True)
        End If
        RefreshFiles()
    End Sub

    Private Sub RefreshFiles()
        Try
            ClsFS.ConfirmDirectory()
            ExtractAssociatedIconEx()
            PopulateBrowserTreeView()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - RefreshFiles")
        End Try
    End Sub

    Public Sub PopulateBrowserTreeView()
        Try
            TreeViewFiles.Nodes.Clear()
            TreeViewFiles.ImageList = ImageListFiles
            TreeViewFiles.Nodes.Add(IO.Path.GetPathRoot(ClsFS.FilePath))
            TreeViewFiles.ImageIndex = 1

            Dim filepath As String = Replace(ClsFS.FilePath, IO.Path.GetPathRoot(ClsFS.FilePath) & "\", "")
            Dim directoryname As String

            Dim rootnode As TreeNode = TreeViewFiles.Nodes(0)
            rootnode.ImageIndex = 0
            rootnode.SelectedImageIndex = 0
            Dim parentnode As TreeNode = TreeViewFiles.Nodes(0)

            Dim NodeLevel As Integer = 0
            While filepath <> ""
                If InStr(1, filepath, "\", vbTextCompare) <> 0 Then
                    directoryname = Strings.Left(filepath, InStr(1, filepath, "\", vbTextCompare) - 1)
                    If NodeLevel = 0 Then
                        parentnode = rootnode.Nodes.Add(directoryname)
                    Else
                        parentnode = parentnode.Nodes.Add(directoryname)
                    End If
                    filepath = Replace(filepath, directoryname & "\", "")
                Else
                    directoryname = filepath
                    If NodeLevel = 0 Then
                        parentnode = rootnode.Nodes.Add(directoryname)
                    Else
                        parentnode = parentnode.Nodes.Add(directoryname)
                    End If
                    filepath = ""
                End If
                parentnode.ImageIndex = 1
                parentnode.SelectedImageIndex = 1
                NodeLevel = NodeLevel + 1
            End While
            TreeViewFiles.ExpandAll()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - PopulateBrowserTreeView")
        End Try
    End Sub

    Public Sub ExtractAssociatedIconEx()
        'Try
        On Error Resume Next
        LVFiles.Items.Clear()

        Dim ImageFileList As ImageList
        ImageFileList = New ImageList()

        If IO.Directory.Exists(ClsFS.FilePath) Then

            LVFiles.SmallImageList = ImageFileList
            LVFiles.View = View.SmallIcon

            ' Get the payment file path directory.
            Dim dir As New IO.DirectoryInfo(ClsFS.FilePath)

            Dim item As ListViewItem
            LVFiles.BeginUpdate()
            Dim file As IO.FileInfo
            For Each file In dir.GetFiles()
                ' Set a default icon for the file.
                Dim iconForFile As Icon = SystemIcons.WinLogo
                item = New ListViewItem(file.Name, 1)

                ' Check to see if the image collection contains an image
                ' for this extension, using the extension as a key.
                If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                    ' If not, add the image to the image list.
                    iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                    ImageFileList.Images.Add(file.Extension, iconForFile)
                End If
                item.ImageKey = file.Extension
                LVFiles.Items.Add(item)

            Next file
            LVFiles.EndUpdate()
        End If
        'Catch ex As Exception
        '   ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - ExtractAssociatedIconEx: ")
        'End Try
    End Sub

    Private Sub LVFiles_DragDrop(sender As Object, e As DragEventArgs) Handles LVFiles.DragDrop
        RefreshFiles()
        If IO.Directory.Exists(ClsFS.FilePath) Then
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    If Not IO.File.Exists(ClsFS.FilePath & Dir(MyFiles(i))) Then
                        IO.File.Copy(MyFiles(i), ClsFS.FilePath & Dir(MyFiles(i)))
                    End If
                Next
                ExtractAssociatedIconEx()
            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Microsoft.Office.Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Microsoft.Office.Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Microsoft.Office.Interop.Outlook.MailItem) Then
                                mi.SaveAs(ClsFS.FilePath & "M" & MsgCount & "_" & mi.SenderName.ToString & "_" & mi.ReceivedTime.ToString("yyyy-MM-dd HHmm") & ".msg")
                                MsgCount = MsgCount + 1
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub LVFiles_DragEnter(sender As Object, e As DragEventArgs) Handles LVFiles.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles_DragLeave(sender As Object, e As EventArgs) Handles LVFiles.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub

    Private Sub FrmFees_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If FeesConn.State = ConnectionState.Open Then
            FeesConn.Close()
        End If
    End Sub

    Private Sub cboPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboPortfolio.SelectedIndexChanged
        If IsNumeric(cboPortfolio.SelectedValue) Then
            ClsFS.PortfolioCode = cboPortfolio.SelectedValue
            Dim lstPortfolioInfo As Dictionary(Of String, String) = ClsIMS.GetDataToList("select Name,PF_ShortCut1,RM1,RM2 from vwDolfinPaymentClientPortfolio where pf_code = " & ClsFS.PortfolioCode)
            If Not lstPortfolioInfo Is Nothing Then
                ClsFS.ClientName = lstPortfolioInfo.Item("Name").ToString
                ClsFS.PortfolioShortcut = lstPortfolioInfo.Item("PF_ShortCut1").ToString
                ClsFS.RM1 = lstPortfolioInfo.Item("RM1").ToString
                ClsFS.RM2 = lstPortfolioInfo.Item("RM2").ToString
                lblClientName.Text = ClsFS.ClientName
                lblshortcut.Text = ClsFS.PortfolioShortcut
                lblRM1.Text = ClsFS.RM1
                lblRM2.Text = ClsFS.RM2
            End If

            If cboPortfolio.SelectedValue >= 0 Then
                If cboPortfolio.SelectedValue > 0 Then
                    Call PopulateCbo(CboCCY, "Select PF_Currency, PF_CCY from [dbo].[vwDolfinPaymentClientPortfolio] where PF_Code = " & ClsFS.PortfolioCode & " group by PF_Currency, PF_CCY order by PF_CCY")
                    CboCCY.SelectedIndex = 0
                ElseIf cboPortfolio.SelectedValue = 0 Then
                    lblClientName.Text = ""
                    lblshortcut.Text = ""
                    lblRM1.Text = ""
                    lblRM2.Text = ""
                End If
                LoadFees()
            End If
        End If
    End Sub

    Private Sub CboCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboCCY.SelectedIndexChanged
        If IsNumeric(CboCCY.SelectedValue) Then
            ClsFS.CCYCode = CboCCY.SelectedValue
            LoadFees()
        End If
    End Sub

    Private Sub dgvMgtRange_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMgtRange.CellEndEdit
        If Not IsDBNull(dgvMgtRange.Rows(e.RowIndex).Cells(0).Value) Then
            If dgvMgtRange.Rows(dgvMgtRange.Rows.Count - 2).Cells(1).Value <> cEndAmt Then
                If dgvMgtRange.Rows(dgvMgtRange.Rows.Count - 2).Cells(1).Value < cEndAmt Then
                    If dgvMgtRange.Rows(dgvMgtRange.Rows.Count - 2).Cells(1).Value > dgvMgtRange.Rows(dgvMgtRange.Rows.Count - 2).Cells(0).Value Then
                        Dim row As DataRow
                        row = dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").NewRow
                        row("From Amount") = dgvMgtRange.Rows(e.RowIndex).Cells(1).Value + 1
                        row("To Amount") = cEndAmt
                        row("Commission %") = "0.0"
                        dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows.Add(row)
                    Else
                        MsgBox("Please enter the 'To Amount' greater than 'From Amount'", vbInformation)
                    End If
                End If
            Else
                If dgvMgtRange.Rows(e.RowIndex).Cells(1).Value > dgvMgtRange.Rows(e.RowIndex).Cells(0).Value Then
                    If e.RowIndex < dgvMgtRange.Rows.Count - 2 Then
                        dsMGTRange.Tables("vwDolfinPaymentManagementFeeRanges").Rows(e.RowIndex + 1)("From Amount") = dgvMgtRange.Rows(e.RowIndex).Cells(1).Value + 1
                    End If
                Else
                    MsgBox("Please enter the 'To Amount' greater than 'From Amount'", vbInformation)
                End If
            End If
        Else
            MsgBox("Change the 'To amount' value for range changes", vbExclamation)
            dgvMgtRange.Rows.RemoveAt(e.RowIndex)
        End If
    End Sub

    Private Sub CmdSaveAll_Click(sender As Object, e As EventArgs) Handles CmdSaveAll.Click
        Dim TblFeeSchedules As New DataTable, TblTransferFees As New DataTable
        Dim TblMgtFee As New DataTable, TblMgtFeeRanges As New DataTable, TblMgtFeeExceptions As New DataTable
        Dim TblCF As New DataTable, TblCFRanges As New DataTable, TblCFExceptions As New DataTable, TblCFSK As New DataTable
        Dim TblTF As New DataTable, TblTFRanges As New DataTable, TblTFSK As New DataTable, TblTFSKFixed As New DataTable
        Dim varActionFeeSchedule As Integer, varActionMGTFees As Integer, varActionCF As Integer, varActionTF As Integer


        If ValidatedFees() Then
            Call PopulateMgtFee(TblMgtFee)
            Call PopulateMgtFeeRanges(TblMgtFeeRanges)
            Call PopulateMgtFeeExceptions(TblMgtFeeExceptions)
            Call PopulateCF(TblCF)
            Call PopulateCFRanges(TblCFRanges)
            Call PopulateCFExceptions(TblCFExceptions)
            Call PopulateCFSafekeeping(TblCFSK)
            Call PopulateTF(TblTF)
            Call PopulateTFRanges(TblTFRanges)
            Call PopulateTFSafekeeping(TblTFSK)
            Call PopulateTFSafekeepingFixed(TblTFSKFixed)

            If ChkMgtFees.Checked Then
                If ClsMgtFees.ID = 0 Then
                    varActionMGTFees = 1
                Else
                    varActionMGTFees = 2
                End If
            Else
                varActionMGTFees = 3
            End If

            If ChkCF.Checked Then
                If ClsCF.ID = 0 Then
                    varActionCF = 1
                Else
                    varActionCF = 2
                End If
            Else
                varActionCF = 3
            End If

            If ChkTF.Checked Then
                If ClsTF.ID = 0 Then
                    varActionTF = 1
                Else
                    varActionTF = 2
                End If
            Else
                varActionTF = 3
            End If

            PopulateTransferFees(TblTransferFees)
            PopulateFeeSchedules(TblFeeSchedules)

            If ClsFS.ID = 0 Then
                varActionFeeSchedule = 1
            Else
                varActionFeeSchedule = 2
            End If

            If ClsIMS.UpdateFeeSchedule(varActionFeeSchedule, TblFeeSchedules, TblTransferFees, varActionMGTFees, TblMgtFee, TblMgtFeeRanges, TblMgtFeeExceptions,
                                            varActionCF, TblCF, TblCFRanges, TblCFExceptions, TblCFSK, varActionTF, TblTF, TblTFRanges, TblTFSK, TblTFSKFixed) Then
                MsgBox("Fees have been saved to the database", vbInformation)
            Else
                MsgBox("There has been a problem saving the fee schedule to the database. Please contact systems support for assistance", vbCritical)
            End If
            Me.Close()
        End If

    End Sub

    Private Sub PopulateMgtFee(ByRef TblMgtFee As DataTable)
        On Error Resume Next
        AddDataColumn(TblMgtFee, "id", "System.Int32")
        AddDataColumn(TblMgtFee, "ChargeableCCYCode", "System.Int32")
        AddDataColumn(TblMgtFee, "TypeID", "System.Int32")
        AddDataColumn(TblMgtFee, "CCYEqvCode", "System.Int32")
        AddDataColumn(TblMgtFee, "MinCharge", "System.Int32")
        AddDataColumn(TblMgtFee, "MaxCharge", "System.Int32")
        AddDataColumn(TblMgtFee, "MinMaintCharge", "System.Int32")

        Dim mgtrow As DataRow
        mgtrow = TblMgtFee.NewRow()
        ClsMgtFees.ChargeableCCYCode = cboMGTChargeableCCY.SelectedValue
        ClsMgtFees.TypeID = cboMgtType.SelectedValue
        ClsMgtFees.CCYEqvCode = cboMGTCCYEqv.SelectedValue
        ClsMgtFees.MinCharge = txtMgtMin.Text
        ClsMgtFees.MaxCharge = txtMgtMax.Text
        ClsMgtFees.MinMaintCharge = txtMgtMaintMin.Text

        mgtrow("id") = ClsMgtFees.ID
        mgtrow("ChargeableCCYCode") = ClsMgtFees.ChargeableCCYCode
        mgtrow("TypeID") = ClsMgtFees.TypeID
        mgtrow("CCYEqvCode") = ClsMgtFees.CCYEqvCode
        mgtrow("MinCharge") = ClsMgtFees.MinCharge
        mgtrow("MaxCharge") = ClsMgtFees.MaxCharge
        mgtrow("MinMaintCharge") = ClsMgtFees.MinMaintCharge

        TblMgtFee.Rows.Add(mgtrow)
    End Sub

    Private Sub PopulateMgtFeeRanges(ByRef TblMgtFeeRanges As DataTable)
        On Error Resume Next
        AddDataColumn(TblMgtFeeRanges, "id", "System.Int32")
        AddDataColumn(TblMgtFeeRanges, "order", "System.Int32")
        AddDataColumn(TblMgtFeeRanges, "from", "System.Int32")
        AddDataColumn(TblMgtFeeRanges, "to", "System.Int32")
        AddDataColumn(TblMgtFeeRanges, "commission", "System.Double")

        For i As Integer = 0 To dgvMgtRange.RowCount - 1
            If IsNumeric(dgvMgtRange.Rows(i).Cells(0).Value) Then
                Dim mgtrow As DataRow
                mgtrow = TblMgtFeeRanges.NewRow()
                mgtrow("id") = ClsMgtFees.ID
                mgtrow("order") = i
                mgtrow("from") = dgvMgtRange.Rows(i).Cells(0).Value
                mgtrow("to") = dgvMgtRange.Rows(i).Cells(1).Value
                mgtrow("commission") = dgvMgtRange.Rows(i).Cells(2).Value
                TblMgtFeeRanges.Rows.Add(mgtrow)
            End If
        Next
    End Sub

    Private Sub PopulateMgtFeeExceptions(ByRef TblMgtFeeExceptions As DataTable)
        On Error Resume Next
        AddDataColumn(TblMgtFeeExceptions, "id", "System.Int32")
        AddDataColumn(TblMgtFeeExceptions, "tcode", "System.Int32")
        AddDataColumn(TblMgtFeeExceptions, "ttype", "System.Int32")

        For i As Integer = 0 To dgvMgtExcGrp.RowCount - 1
            If IsNumeric(dgvMgtExcGrp.Rows(i).Cells(0).Value) Then
                Dim mgtrow As DataRow
                mgtrow = TblMgtFeeExceptions.NewRow()
                mgtrow("id") = ClsMgtFees.ID
                mgtrow("tcode") = DBNull.Value
                mgtrow("ttype") = dgvMgtExcGrp.Rows(i).Cells(0).Value
                TblMgtFeeExceptions.Rows.Add(mgtrow)
            End If
        Next

        For i As Integer = 0 To dgvMgtExcIns.RowCount - 1
            If IsNumeric(dgvMgtExcIns.Rows(i).Cells(0).Value) Then
                Dim mgtrow As DataRow
                mgtrow = TblMgtFeeExceptions.NewRow()
                mgtrow("id") = ClsMgtFees.ID
                mgtrow("tcode") = dgvMgtExcIns.Rows(i).Cells(0).Value
                mgtrow("ttype") = DBNull.Value
                TblMgtFeeExceptions.Rows.Add(mgtrow)
            End If
        Next
    End Sub

    Private Sub PopulateCF(ByRef TblCF As DataTable)
        On Error Resume Next
        AddDataColumn(TblCF, "id", "System.Int32")
        AddDataColumn(TblCF, "ChargeableCCYCode", "System.Int32")
        AddDataColumn(TblCF, "TypeID", "System.Int32")
        AddDataColumn(TblCF, "CCYEqvCode", "System.Int32")
        AddDataColumn(TblCF, "MinCharge", "System.Int32")
        AddDataColumn(TblCF, "MaxCharge", "System.Int32")
        AddDataColumn(TblCF, "MinMaintCharge", "System.Int32")

        Dim cfrow As DataRow
        cfrow = TblCF.NewRow()
        ClsCF.ChargeableCCYCode = cboCFChargeableCCY.SelectedValue
        ClsCF.TypeID = cboCFType.SelectedValue
        ClsCF.CCYEqvCode = cboCFCCYEqv.SelectedValue
        ClsCF.MinCharge = txtCFMin.Text
        ClsCF.MaxCharge = txtCFMax.Text
        ClsCF.MinMaintCharge = txtCFMaintMin.Text

        cfrow("id") = ClsCF.ID
        cfrow("ChargeableCCYCode") = ClsCF.ChargeableCCYCode
        cfrow("TypeID") = ClsCF.TypeID
        cfrow("CCYEqvCode") = ClsCF.CCYEqvCode
        cfrow("MinCharge") = ClsCF.MinCharge
        cfrow("MaxCharge") = ClsCF.MaxCharge
        cfrow("MinMaintCharge") = ClsCF.MinMaintCharge

        TblCF.Rows.Add(cfrow)
    End Sub

    Private Sub PopulateCFRanges(ByRef TblCFRanges As DataTable)
        On Error Resume Next
        AddDataColumn(TblCFRanges, "id", "System.Int32")
        AddDataColumn(TblCFRanges, "order", "System.Int32")
        AddDataColumn(TblCFRanges, "assettype", "System.Int32")
        AddDataColumn(TblCFRanges, "from", "System.Int32")
        AddDataColumn(TblCFRanges, "to", "System.Int32")
        AddDataColumn(TblCFRanges, "commission", "System.Double")

        For i As Integer = 0 To dgvCFRange.RowCount - 1
            If Not dgvCFRange.Rows(i).Cells(0).Value Is Nothing Then
                Dim cfrow As DataRow
                cfrow = TblCFRanges.NewRow()
                cfrow("id") = ClsCF.ID
                cfrow("order") = i
                cfrow("assettype") = IIf(dgvCFRange.Rows(i).Cells(0).Value Is Nothing, 0, dgvCFRange.Rows(i).Cells(0).Value)
                If dgvCFRange.Rows(i).Cells(3).Value < 10 Then
                    cfrow("from") = dgvCFRange.Rows(i).Cells("From Amount").Value
                    cfrow("to") = dgvCFRange.Rows(i).Cells("To Amount").Value
                    cfrow("commission") = dgvCFRange.Rows(i).Cells("Commission %").Value
                ElseIf dgvCFRange.Rows(i).Cells(4).Value < 10 Then
                    cfrow("from") = dgvCFRange.Rows(i).Cells("From Amount").Value
                    cfrow("to") = dgvCFRange.Rows(i).Cells("To Amount").Value
                    cfrow("commission") = dgvCFRange.Rows(i).Cells("Commission %").Value
                End If
                TblCFRanges.Rows.Add(cfrow)
            End If
        Next
    End Sub

    Private Sub PopulateCFExceptions(ByRef TblCFExceptions As DataTable)
        On Error Resume Next
        AddDataColumn(TblCFExceptions, "id", "System.Int32")
        AddDataColumn(TblCFExceptions, "tcode", "System.Int32")
        AddDataColumn(TblCFExceptions, "ttype", "System.Int32")

        For i As Integer = 0 To dgvCFExcGrp.RowCount - 1
            If Not dgvCFExcGrp.Rows(i).Cells(0).Value Is Nothing Then
                If IsNumeric(dgvCFExcGrp.Rows(i).Cells(0).Value) Then
                    Dim cfrow As DataRow
                    cfrow = TblCFExceptions.NewRow()
                    cfrow("id") = ClsCF.ID
                    cfrow("tcode") = DBNull.Value
                    cfrow("ttype") = dgvCFExcGrp.Rows(i).Cells(0).Value
                    TblCFExceptions.Rows.Add(cfrow)
                End If
            End If
        Next

        For i As Integer = 0 To dgvCFExcIns.RowCount - 1
            If Not dgvCFExcIns.Rows(i).Cells(0).Value Is Nothing Then
                If IsNumeric(dgvCFExcIns.Rows(i).Cells(0).Value) Then
                    Dim cfrow As DataRow
                    cfrow = TblCFExceptions.NewRow()
                    cfrow("id") = ClsCF.ID
                    cfrow("tcode") = dgvCFExcIns.Rows(i).Cells(0).Value
                    cfrow("ttype") = DBNull.Value
                    TblCFExceptions.Rows.Add(cfrow)
                End If
            End If
        Next
    End Sub

    Private Sub PopulateCFSafekeeping(ByRef TblCFSK As DataTable)
        On Error Resume Next
        AddDataColumn(TblCFSK, "id", "System.Int32")
        AddDataColumn(TblCFSK, "sk", "System.Int32")
        AddDataColumn(TblCFSK, "commission", "System.Double")
        AddDataColumn(TblCFSK, "movementfee", "System.Double")
        AddDataColumn(TblCFSK, "movementccycode", "System.Int32")

        For i As Integer = 0 To dgvCFSK.RowCount - 1
            If Not dgvCFSK.Rows(i).Cells(0).Value Is Nothing Then
                If IsNumeric(dgvCFSK.Rows(i).Cells(0).Value) Then
                    If dgvCFSK.Rows(i).Cells(0).Value > 0 Then
                        Dim cfrow As DataRow
                        cfrow = TblCFSK.NewRow()
                        cfrow("id") = ClsCF.ID
                        cfrow("sk") = dgvCFSK.Rows(i).Cells(0).Value
                        cfrow("commission") = IIf(IsDBNull(dgvCFSK.Rows(i).Cells("Commission").Value), 0, dgvCFSK.Rows(i).Cells("Commission").Value)
                        cfrow("movementfee") = IIf(IsDBNull(dgvCFSK.Rows(i).Cells("movement fee").Value), 0, dgvCFSK.Rows(i).Cells("movement fee").Value)
                        cfrow("movementccycode") = IIf(IsDBNull(dgvCFSK.Rows(i).Cells("movement CCY").Value), 6, dgvCFSK.Rows(i).Cells("movement CCY").Value)
                        TblCFSK.Rows.Add(cfrow)
                    End If
                End If
            End If
        Next
    End Sub

    Private Sub PopulateTF(ByRef TblTF As DataTable)
        On Error Resume Next
        AddDataColumn(TblTF, "id", "System.Int32")
        AddDataColumn(TblTF, "TypeID", "System.Int32")
        AddDataColumn(TblTF, "CCYEqvCode", "System.Int32")
        AddDataColumn(TblTF, "TicketFee", "System.Int32")
        AddDataColumn(TblTF, "TicketFeeCCY", "System.Int32")
        AddDataColumn(TblTF, "TransFee", "System.Int32")
        AddDataColumn(TblTF, "TransFeeCCY", "System.Int32")
        AddDataColumn(TblTF, "FOPFee", "System.Int32")
        AddDataColumn(TblTF, "FOPFeeCCY", "System.Int32")
        AddDataColumn(TblTF, "CoverBrokerage", "System.Boolean")
        AddDataColumn(TblTF, "MinCharge", "System.Int32")
        AddDataColumn(TblTF, "MaxCharge", "System.Int32")

        Dim tfrow As DataRow
        tfrow = TblTF.NewRow()
        ClsTF.TypeID = cboTFType.SelectedValue
        ClsTF.CCYEqvCode = cboTFCCYEqv.SelectedValue
        ClsTF.TicketFee = txtTFTicketFee.Text
        ClsTF.TicketFeeCCY = cboTFTicketFeeCCY.SelectedValue
        ClsTF.TransFee = txtTFTransFee.Text
        ClsTF.TransFeeCCY = cboTFTransFeeCCY.SelectedValue
        ClsTF.FOPFee = txtTFFOPFee.Text
        ClsTF.FOPFeeCCY = cboTFFOPFeeCCY.SelectedValue
        ClsTF.CoverBrokerage = ChkCoverBrokerage.Checked
        ClsTF.MinCharge = txtTFMin.Text
        ClsTF.MaxCharge = txtTFMax.Text

        tfrow("id") = ClsTF.ID
        tfrow("TypeID") = ClsTF.TypeID
        tfrow("CCYEqvCode") = ClsTF.CCYEqvCode
        tfrow("TicketFee") = ClsTF.TicketFee
        tfrow("TicketFeeCCY") = ClsTF.TicketFeeCCY
        tfrow("TransFee") = ClsTF.TransFee
        tfrow("TransFeeCCY") = ClsTF.TransFeeCCY
        tfrow("FOPFee") = ClsTF.FOPFee
        tfrow("FOPFeeCCY") = ClsTF.FOPFeeCCY
        tfrow("CoverBrokerage") = ClsTF.CoverBrokerage
        tfrow("MinCharge") = ClsTF.MinCharge
        tfrow("MaxCharge") = ClsTF.MaxCharge

        TblTF.Rows.Add(tfrow)
    End Sub

    Private Sub PopulateTFRanges(ByRef TblTFRanges As DataTable)
        Try
            AddDataColumn(TblTFRanges, "id", "System.Int32")
            AddDataColumn(TblTFRanges, "order", "System.Int32")
            AddDataColumn(TblTFRanges, "assettype", "System.Int32")
            AddDataColumn(TblTFRanges, "from", "System.Int32")
            AddDataColumn(TblTFRanges, "to", "System.Int32")
            AddDataColumn(TblTFRanges, "commission", "System.Double")

            For i As Integer = 0 To dgvTFRange.RowCount - 1
                If Not dgvTFRange.Rows(i).Cells(0).Value Is Nothing Then
                    If IsNumeric(dgvTFRange.Rows(i).Cells(0).Value) Then
                        If dgvTFRange.Rows(i).Cells(0).Value > 0 Then
                            Dim tfrow As DataRow
                            tfrow = TblTFRanges.NewRow()
                            tfrow("id") = ClsTF.ID
                            tfrow("order") = i
                            tfrow("assettype") = IIf(dgvTFRange.Rows(i).Cells(0).Value Is Nothing, 1, dgvTFRange.Rows(i).Cells("asset type").Value)
                            If dgvTFRange.Rows(i).Cells("commission %").Value < 10 Then
                                tfrow("from") = dgvTFRange.Rows(i).Cells("From Amount").Value
                                tfrow("to") = dgvTFRange.Rows(i).Cells("To Amount").Value
                                tfrow("commission") = dgvTFRange.Rows(i).Cells("commission %").Value
                            ElseIf dgvTFRange.Rows(i).Cells(4).Value < 10 Then
                                tfrow("from") = dgvTFRange.Rows(i).Cells("From Amount").Value
                                tfrow("to") = dgvTFRange.Rows(i).Cells("To Amount").Value
                                tfrow("commission") = dgvTFRange.Rows(i).Cells("commission %").Value
                            End If
                            TblTFRanges.Rows.Add(tfrow)
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with PopulateTFRanges: " & ClsTF.ID, 0)
        End Try
    End Sub

    Private Sub PopulateTFSafekeeping(ByRef TblTFSK As DataTable)
        Try
            AddDataColumn(TblTFSK, "id", "System.Int32")
            AddDataColumn(TblTFSK, "sk", "System.Int32")
            AddDataColumn(TblTFSK, "commission", "System.Double")
            AddDataColumn(TblTFSK, "ticketfee", "System.Double")
            AddDataColumn(TblTFSK, "ticketfeeccycode", "System.Int32")

            For i As Integer = 0 To dgvTFSK.RowCount - 1
                If Not dgvTFSK.Rows(i).Cells(0).Value Is Nothing Then
                    If IsNumeric(dgvTFSK.Rows(i).Cells(0).Value) Then
                        If dgvTFSK.Rows(i).Cells(0).Value > 0 Then
                            Dim cfrow As DataRow
                            cfrow = TblTFSK.NewRow()
                            cfrow("id") = ClsTF.ID
                            cfrow("sk") = dgvTFSK.Rows(i).Cells(0).Value
                            cfrow("commission") = IIf(IsDBNull(dgvTFSK.Rows(i).Cells("Commission").Value), 0, dgvTFSK.Rows(i).Cells("Commission").Value)
                            cfrow("ticketfee") = IIf(IsDBNull(dgvTFSK.Rows(i).Cells("TicketFee").Value), 0, dgvTFSK.Rows(i).Cells("TicketFee").Value)
                            cfrow("ticketfeeccycode") = IIf(IsDBNull(dgvTFSK.Rows(i).Cells("TicketFeeCCY").Value), 0, dgvTFSK.Rows(i).Cells("TicketFeeCCY").Value)
                            TblTFSK.Rows.Add(cfrow)
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with PopulateTFSafekeeping: " & ClsTF.ID, 0)
        End Try
    End Sub

    Private Sub PopulateTFSafekeepingFixed(ByRef TblTFSKFixed As DataTable)
        Try
            AddDataColumn(TblTFSKFixed, "id", "System.Int32")
            AddDataColumn(TblTFSKFixed, "sk", "System.Int32")
            AddDataColumn(TblTFSKFixed, "commission", "System.Double")
            AddDataColumn(TblTFSKFixed, "ticketfee", "System.Double")
            AddDataColumn(TblTFSKFixed, "ticketfeeccycode", "System.Int32")

            For i As Integer = 0 To dgvTFSKFixed.RowCount - 1
                If Not dgvTFSKFixed.Rows(i).Cells(0).Value Is Nothing Then
                    If IsNumeric(dgvTFSKFixed.Rows(i).Cells(0).Value) Then
                        If dgvTFSKFixed.Rows(i).Cells(0).Value > 0 Then
                            Dim cfrow As DataRow
                            cfrow = TblTFSKFixed.NewRow()
                            cfrow("id") = ClsTF.ID
                            cfrow("sk") = dgvTFSKFixed.Rows(i).Cells(0).Value
                            cfrow("commission") = IIf(IsDBNull(dgvTFSKFixed.Rows(i).Cells("Commission").Value), 0, dgvTFSKFixed.Rows(i).Cells("Commission").Value)
                            cfrow("ticketfee") = IIf(IsDBNull(dgvTFSKFixed.Rows(i).Cells("TicketFee").Value), 0, dgvTFSKFixed.Rows(i).Cells("TicketFee").Value)
                            cfrow("ticketfeeccycode") = IIf(IsDBNull(dgvTFSKFixed.Rows(i).Cells("TicketFeeCCY").Value), 0, dgvTFSKFixed.Rows(i).Cells("TicketFeeCCY").Value)
                            TblTFSKFixed.Rows.Add(cfrow)
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameFees, Err.Description & " - Problem with PopulateTFSafekeepingFixed: " & ClsTF.ID, 0)
        End Try
    End Sub

    Public Sub PopulateTransferFees(ByRef TblTransferFees As DataTable)
        On Error Resume Next
        AddDataColumn(TblTransferFees, "portfoliocode", "System.Int32")
        AddDataColumn(TblTransferFees, "TF_CCY", "System.String")
        AddDataColumn(TblTransferFees, "TF_Fee", "System.Int32")
        AddDataColumn(TblTransferFees, "TF_Int_CCY", "System.String")
        AddDataColumn(TblTransferFees, "TF_Int_Fee", "System.Int32")

        If IsNumeric(cboPortfolio.SelectedValue) And cboPortfolio.SelectedValue <> 0 Then
            Dim tfrow As DataRow
            tfrow = TblTransferFees.NewRow()
            tfrow("portfoliocode") = cboPortfolio.SelectedValue
            tfrow("TF_CCY") = IIf(IsDBNull(dgvTrans.Rows(0).Cells(4).Value), 0, dgvTrans.Rows(0).Cells(4).Value)
            tfrow("TF_Fee") = IIf(IsDBNull(dgvTrans.Rows(0).Cells(5).Value), 0, dgvTrans.Rows(0).Cells(5).Value)
            tfrow("TF_Int_CCY") = IIf(IsDBNull(dgvTrans.Rows(0).Cells(7).Value), 0, dgvTrans.Rows(0).Cells(7).Value)
            tfrow("TF_Int_Fee") = IIf(IsDBNull(dgvTrans.Rows(0).Cells(8).Value), 0, dgvTrans.Rows(0).Cells(8).Value)
            TblTransferFees.Rows.Add(tfrow)
        End If
    End Sub

    Public Sub SaveTransferFees()
        Dim varID As Integer

        Try
            If IsNumeric(cboPortfolio.SelectedValue) And cboPortfolio.SelectedValue <> 0 Then
                Dim TF_Fee As Double = IIf(IsDBNull(dgvTrans.Rows(0).Cells(5).Value), 0, dgvTrans.Rows(0).Cells(5).Value)
                Dim TF_CCY As String = IIf(IsDBNull(dgvTrans.Rows(0).Cells(4).Value), 0, dgvTrans.Rows(0).Cells(4).Value)
                Dim TF_Int_Fee As Double = IIf(IsDBNull(dgvTrans.Rows(0).Cells(8).Value), 0, dgvTrans.Rows(0).Cells(8).Value)
                Dim TF_Int_CCY As String = IIf(IsDBNull(dgvTrans.Rows(0).Cells(7).Value), 0, dgvTrans.Rows(0).Cells(7).Value)
                ClsIMS.UpdateTransferFee(cboPortfolio.SelectedValue, TF_CCY, TF_Fee, TF_Int_CCY, TF_Int_Fee)
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with updating Transacion fees ID: " & varID, cboPortfolio.SelectedValue)
        End Try
    End Sub

    Public Sub PopulateFeeSchedules(ByRef TblFeeSchedules As DataTable)
        On Error Resume Next
        AddDataColumn(TblFeeSchedules, "fsid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "portfoliocode", "System.Int32")
        AddDataColumn(TblFeeSchedules, "ccycode", "System.Int32")
        AddDataColumn(TblFeeSchedules, "startdate", "System.DateTime")
        AddDataColumn(TblFeeSchedules, "enddate", "System.DateTime")
        AddDataColumn(TblFeeSchedules, "destinationid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "payfreqid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "mgtid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "cfid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "tfid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "filepath", "System.String")
        AddDataColumn(TblFeeSchedules, "template", "System.Boolean")
        AddDataColumn(TblFeeSchedules, "templatename", "System.String")
        AddDataColumn(TblFeeSchedules, "templateid", "System.Int32")
        AddDataColumn(TblFeeSchedules, "IsEAM", "System.Boolean")
        AddDataColumn(TblFeeSchedules, "portfoliocoderedirect", "System.Int32")

        If cboPortfolio.SelectedValue = 0 Then
            ClsFS.IsTemplate = 1
            If ClsFS.ID = 0 Then
                ClsFS.TemplateName = cboTemplateName.Text
            Else
                ClsFS.TemplateName = cboPortfolio.Text
            End If
            ClsFS.TemplateLinkedName = ""
            ClsFS.TemplateLinkedID = 0

        ElseIf cboTemplateName.Text = "" Then
            ClsFS.TemplateLinkedID = 0
        End If

        ClsFS.StartDate = DtFSStart.Value
        ClsFS.EndDate = DtFSEnd.Value
        ClsFS.DestinationID = cboFSDestination.SelectedValue
        ClsFS.PayFreqID = cboFSPayFreq.SelectedValue
        ClsFS.IsEAM = cboFSIsEAM.SelectedValue
        ClsFS.PortfolioCodeRedirect = cboPortfolioRedirect.SelectedValue

        Dim fsrow As DataRow
        fsrow = TblFeeSchedules.NewRow()
        fsrow("fsid") = ClsFS.ID
        fsrow("portfoliocode") = ClsFS.PortfolioCode
        fsrow("ccycode") = ClsFS.CCYCode
        fsrow("startdate") = ClsFS.StartDate
        fsrow("enddate") = ClsFS.EndDate
        fsrow("destinationid") = ClsFS.DestinationID
        fsrow("payfreqid") = ClsFS.PayFreqID
        fsrow("IsEAM") = ClsFS.IsEAM
        fsrow("portfoliocoderedirect") = ClsFS.PortfolioCodeRedirect
        fsrow("mgtid") = ClsMgtFees.ID
        fsrow("cfid") = ClsCF.ID
        fsrow("tfid") = ClsTF.ID
        fsrow("filepath") = ClsFS.FilePath
        fsrow("template") = ClsFS.IsTemplate
        fsrow("templatename") = ClsFS.TemplateName
        fsrow("templateid") = ClsFS.TemplateLinkedID
        TblFeeSchedules.Rows.Add(fsrow)
    End Sub

    Public Function ValidatedFees() As Boolean
        ValidatedFees = True

        If Not IsNumeric(cboFSDestination.SelectedValue) Then
            MsgBox("Please enter the destination of fees", vbExclamation)
            ValidatedFees = False
        ElseIf Not IsNumeric(cboFSPayFreq.SelectedValue) Then
            MsgBox("Please enter the pay frequency name", vbExclamation)
            ValidatedFees = False
        ElseIf cboPortfolio.SelectedValue = 0 And cboTemplateName.Text = "" And ClsFS.IsTemplate = False Then
            MsgBox("Please enter the template name", vbExclamation)
            ValidatedFees = False
        ElseIf ChkMgtFees.Checked And Not IsNumeric(cboMgtType.SelectedValue) Then
            MsgBox("Please enter the type of management fee required", vbExclamation)
            ValidatedFees = False
        ElseIf ChkCF.Checked And Not IsNumeric(cboCFType.SelectedValue) Then
            MsgBox("Please enter the type of custody fee required", vbExclamation)
            ValidatedFees = False
        ElseIf ChkTF.Checked And Not IsNumeric(cboTFType.SelectedValue) Then
            MsgBox("Please enter the type of transaction fee required", vbExclamation)
            ValidatedFees = False
        End If
    End Function

    Private Function ValidMgtFee() As Boolean
        ValidMgtFee = True
    End Function

    Private Sub dgvMgtRange_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs)
        If Not IsNumeric(e.FormattedValue) And e.FormattedValue <> "" Then
            MsgBox("Numbers only", vbExclamation)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtMgtMin_Validating(sender As Object, e As CancelEventArgs) Handles txtMgtMin.Validating
        If Not IsNumeric(sender.text) And sender.text <> "" Then
            MsgBox("Numbers only", vbExclamation)
            e.Cancel = True
        End If
    End Sub

    Private Sub txtMgtMax_Validating(sender As Object, e As CancelEventArgs) Handles txtMgtMax.Validating
        If Not IsNumeric(sender.text) And sender.text <> "" Then
            MsgBox("Numbers only", vbExclamation)
            e.Cancel = True
        End If
    End Sub

    Private Sub LVFiles_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles LVFiles.ItemMouseHover
        LVFiles.ContextMenuStrip = ContextMenuStripFiles
    End Sub

    Private Sub OpenFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            ShellExecute(ClsFS.FilePath & LVFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub RemoveFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            IO.File.Delete(ClsFS.FilePath & LVFiles.SelectedItems(0).Text)
            ExtractAssociatedIconEx()
        End If
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        ExtractAssociatedIconEx()
    End Sub

    Private Sub ChkMgtFees_CheckedChanged(sender As Object, e As EventArgs) Handles ChkMgtFees.CheckedChanged
        If ChkMgtFees.Checked Then
            GrpMgtFees.Enabled = True
        Else
            'Dim varResponse As Integer = MsgBox("Are you sure you want to remove management fees for this client?", vbQuestion + vbYesNo)
            'If varResponse = vbYes Then
            GrpMgtFees.Enabled = False
            'End If
        End If
        cboMGTChargeableCCY.Text = CboCCY.Text
    End Sub

    Private Sub ChkCF_CheckedChanged(sender As Object, e As EventArgs) Handles ChkCF.CheckedChanged
        If ChkCF.Checked Then
            GrpCF.Enabled = True
            cboCFChargeableCCY.SelectedValue = ClsFS.CCYCode
        Else
            'Dim varResponse As Integer = MsgBox("Are you sure you want to remove custody fees for this client?", vbQuestion + vbYesNo)
            'If varResponse = vbYes Then
            GrpCF.Enabled = False
            'End If
        End If
        cboCFChargeableCCY.Text = CboCCY.Text
    End Sub

    Private Sub ChkTF_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTF.CheckedChanged
        If ChkTF.Checked Then
            GrpTF.Enabled = True
        Else
            'Dim varResponse As Integer = MsgBox("Are you sure you want to remove custody fees for this client?", vbQuestion + vbYesNo)
            'If varResponse = vbYes Then
            GrpTF.Enabled = False
            'End If
        End If
    End Sub

    Private Sub dgvCFRange_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCFRange.CellEndEdit
        If dgvCFRange.CurrentCell.ColumnIndex < 3 Then
            If dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Or IsNumeric(dgvCFRange.Rows(e.RowIndex).Cells(0).Value) Then
                If e.RowIndex > 0 And dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Then
                    dgvCFRange.Rows(e.RowIndex).Cells(0).Value = -1
                ElseIf dgvCFRange.Rows(e.RowIndex).Cells(0).Value = 0 And IsDBNull(dgvCFRange.Rows(e.RowIndex).Cells(2).Value) Then
                    dgvCFRange.Rows(e.RowIndex).Cells(0).Value = -1
                ElseIf dgvCFRange.Rows(e.RowIndex).Cells(0).Value <> 0 And IsDBNull(dgvCFRange.Rows(e.RowIndex).Cells(2).Value) Then
                    If CheckNewAssetType(dgvCFRange.Rows(e.RowIndex).Cells(0).Value) Then
                        dgvCFRange.Rows(e.RowIndex).Cells(1).Value = 0
                        dgvCFRange.Rows(e.RowIndex).Cells(2).Value = cEndAmt
                        dgvCFRange.Rows(e.RowIndex).Cells(3).Value = "0.0"
                    Else
                        dgvCFRange.Rows(e.RowIndex).Cells(0).Value = -1
                    End If
                ElseIf dgvCFRange.Rows(e.RowIndex).Cells(2).Value < cEndAmt And IsNumeric(IIf(dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing, 0, dgvCFRange.Rows(e.RowIndex).Cells(0).Value)) Then
                    If dgvCFRange.Rows(e.RowIndex).Cells(2).Value Is Nothing Then
                        If Not (dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing And e.ColumnIndex = 0 And e.RowIndex > 0) Then
                            AddCustodyRow(e.RowIndex, e.RowIndex)
                            UpdateCustodyComboValue(e.RowIndex)
                        End If
                    ElseIf Not dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing And dgvCFRange.Rows(e.RowIndex).Cells(0).Value = dgvCFRange.Rows(e.RowIndex + 1).Cells(0).Value And Not dgvCFRange.Rows(e.RowIndex + 1).Cells(2).Value Is Nothing Then
                        If dgvCFRange.Rows(e.RowIndex).Cells(2).Value > dgvCFRange.Rows(e.RowIndex).Cells(1).Value Then
                            dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows(e.RowIndex + 1)("From Amount") = dgvCFRange.Rows(e.RowIndex).Cells(2).Value + 1
                        End If
                    Else
                        AddCustodyRow(e.RowIndex, e.RowIndex + 1)
                        UpdateCustodyComboValue(e.RowIndex)
                    End If
                End If
            ElseIf Not dgvCFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Then
                AddCustodyRow(e.RowIndex, e.RowIndex)
            End If
        End If
    End Sub

    Public Sub AddCustodyRow(ByVal varRow As Integer, ByVal varPosition As Integer)
        Try
            Dim row As DataRow
            row = dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").NewRow
            row("Asset Type") = dgvCFRange.Rows(varRow).Cells(0).Value.ToString
            row("From Amount") = dgvCFRange.Rows(varRow).Cells(1).Value.ToString
            row("To Amount") = dgvCFRange.Rows(varRow).Cells(2).Value.ToString
            row("Commission %") = dgvCFRange.Rows(varRow).Cells(3).Value.ToString
            dsCFRange.Tables("vwDolfinPaymentCustodyFeeRanges").Rows.InsertAt(row, varPosition - 1)

            dgvCFRange.Rows(varRow + 1).Cells(0).Value = IIf(dgvCFRange.Rows(varRow + 1).Cells(0).Value Is Nothing, 0, dgvCFRange.Rows(varRow + 1).Cells(0).Value)
            dgvCFRange.Rows(varRow + 1).Cells(1).Value = dgvCFRange.Rows(varRow + 1).Cells(2).Value + 1
            dgvCFRange.Rows(varRow + 1).Cells(2).Value = cEndAmt
            dgvCFRange.Rows(varRow + 1).Cells(3).Value = "0.0"
        Catch ex As Exception

        End Try
    End Sub

    Public Sub AddTransactionRow(ByVal varRow As Integer, ByVal varPosition As Integer)
        Try
            Dim row As DataRow
            row = dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").NewRow
            row("Asset Type") = dgvTFRange.Rows(varRow).Cells(0).Value.ToString
            row("From Amount") = dgvTFRange.Rows(varRow).Cells(1).Value.ToString
            row("To Amount") = dgvTFRange.Rows(varRow).Cells(2).Value.ToString
            row("Commission %") = dgvTFRange.Rows(varRow).Cells(3).Value.ToString
            dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows.InsertAt(row, varPosition - 1)

            dgvTFRange.Rows(varRow + 1).Cells(0).Value = IIf(dgvTFRange.Rows(varRow + 1).Cells(0).Value Is Nothing, 0, dgvTFRange.Rows(varRow + 1).Cells(0).Value)
            dgvTFRange.Rows(varRow + 1).Cells(1).Value = dgvTFRange.Rows(varRow + 1).Cells(2).Value + 1
            dgvTFRange.Rows(varRow + 1).Cells(2).Value = cEndAmt
            dgvTFRange.Rows(varRow + 1).Cells(3).Value = "0.0"
        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateCustodyComboValue(ByVal varRow As Integer)
        Try
            Dim combo As DataGridViewComboBoxCell
            combo = CType(dgvCFRange("", varRow + 1), DataGridViewComboBoxCell)
            combo.Value = IIf(dgvCFRange.Rows(varRow).Cells(0).Value Is Nothing, 0, dgvCFRange.Rows(varRow).Cells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub UpdateTransactionComboValue(ByVal varRow As Integer)
        Try
            Dim combo As DataGridViewComboBoxCell
            combo = CType(dgvTFRange("", varRow + 1), DataGridViewComboBoxCell)
            combo.Value = IIf(dgvTFRange.Rows(varRow).Cells(0).Value Is Nothing, 0, dgvTFRange.Rows(varRow).Cells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Public Function CheckNewAssetTypeold(ByVal varAssetTypeSelected As Integer)
        CheckNewAssetTypeold = False
        If varAssetTypeSelected <> 0 Then
            CheckNewAssetTypeold = True
            For i As Integer = 0 To dgvCFRange.Rows.Count - 1
                If Not IsDBNull(dgvCFRange.Rows(i).Cells(0).Value) Then
                    If dgvCFRange.Rows(i).Cells(0).Value = varAssetTypeSelected Then
                        CheckNewAssetTypeold = False
                    End If
                End If
            Next
        End If
    End Function

    Public Function CheckNewAssetType(ByVal varAssetTypeSelected As Integer)
        Dim CountAsset As Integer = 0

        If varAssetTypeSelected <> 0 Then
            For i As Integer = 0 To dgvCFRange.Rows.Count - 1
                If Not IsDBNull(dgvCFRange.Rows(i).Cells(0).Value) Then
                    If dgvCFRange.Rows(i).Cells(0).Value = varAssetTypeSelected Then
                        CountAsset = CountAsset + 1
                    End If
                End If
            Next
        End If
        If CountAsset > 1 Then
            CheckNewAssetType = False
        Else
            CheckNewAssetType = True
        End If
    End Function

    Public Function CheckNewTransactionAssetType(ByVal varAssetTypeSelected As Integer)
        Dim CountAsset As Integer = 0

        If varAssetTypeSelected <> 0 Then
            For i As Integer = 0 To dgvTFRange.Rows.Count - 1
                If Not IsDBNull(dgvTFRange.Rows(i).Cells(0).Value) Then
                    If dgvTFRange.Rows(i).Cells(0).Value = varAssetTypeSelected Then
                        CountAsset = CountAsset + 1
                    End If
                End If
            Next
        End If
        If CountAsset > 1 Then
            CheckNewTransactionAssetType = False
        Else
            CheckNewTransactionAssetType = True
        End If
    End Function

    Private Sub dgvCFRange_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvCFRange.CellBeginEdit
        If e.RowIndex = 0 And e.ColumnIndex = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvCFRange_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCFRange.CellFormatting
        If e.RowIndex = 0 And e.ColumnIndex = 0 Then
            e.Value = "Default"
        End If
    End Sub

    Private Sub dgvCFRange_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvCFRange.CellValidating
        If e.ColumnIndex = 0 And e.FormattedValue = "" Then
            'e.Cancel = True
        ElseIf e.ColumnIndex > 1 And e.FormattedValue <> "" Then
            If Not IsNumeric(e.FormattedValue) Then
                MsgBox("Numbers only", vbExclamation)
                e.Cancel = True
            ElseIf IsDBNull(dgvCFRange.Rows(e.RowIndex).Cells(1).Value) Then
                e.Cancel = True
            ElseIf e.ColumnIndex = 2 And (e.FormattedValue > cEndAmt Or e.FormattedValue < dgvCFRange.Rows(e.RowIndex).Cells(1).Value) Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub cboCFChargeableCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCFChargeableCCY.SelectedIndexChanged
        If IsNumeric(cboCFChargeableCCY.SelectedValue) Then
            ClsCF.ChargeableCCYCode = cboCFChargeableCCY.SelectedValue
        End If
    End Sub

    Private Sub dgvCFRange_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvCFRange.UserDeletingRow
        If e.Row.Index = 0 Then
            e.Cancel = True
        Else
            dgvCFRange.Rows(e.Row.Index - 1).Cells(2).Value = cEndAmt
        End If
    End Sub

    Private Sub cboMGTChargeableCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMGTChargeableCCY.SelectedIndexChanged
        If IsNumeric(cboMGTChargeableCCY.SelectedValue) Then
            ClsMgtFees.ChargeableCCYCode = cboMGTChargeableCCY.SelectedValue
        End If
    End Sub

    Public Sub MGTFeeEnable(ByVal varEnabled As Boolean)
        ChkMgtFees.Enabled = varEnabled
        cboMGTChargeableCCY.Enabled = varEnabled
        cboMgtType.Enabled = varEnabled
        txtMgtMin.Enabled = varEnabled
        txtMgtMax.Enabled = varEnabled
        txtMgtMaintMin.Enabled = varEnabled
        cboMGTCCYEqv.Enabled = varEnabled
        dgvMgtRange.ReadOnly = Not varEnabled
        dgvMgtExcGrp.ReadOnly = Not varEnabled
        dgvMgtExcIns.ReadOnly = Not varEnabled
    End Sub

    Public Sub CFFeeEnable(ByVal varEnabled As Boolean)
        ChkCF.Enabled = varEnabled
        cboCFChargeableCCY.Enabled = varEnabled
        cboCFType.Enabled = varEnabled
        txtCFMin.Enabled = varEnabled
        txtCFMax.Enabled = varEnabled
        txtCFMaintMin.Enabled = varEnabled
        cboCFCCYEqv.Enabled = varEnabled
        dgvCFRange.ReadOnly = Not varEnabled
        dgvCFSK.ReadOnly = Not varEnabled
        dgvCFExcGrp.ReadOnly = Not varEnabled
        dgvCFExcIns.ReadOnly = Not varEnabled
    End Sub

    Public Sub TFFeeEnable(ByVal varEnabled As Boolean)
        ChkTF.Enabled = varEnabled
        cboTFType.Enabled = varEnabled
        cboTFCCYEqv.Enabled = varEnabled
        txtTFTicketFee.Enabled = varEnabled
        cboTFTicketFeeCCY.Enabled = varEnabled
        txtTFTransFee.Enabled = varEnabled
        cboTFTransFeeCCY.Enabled = varEnabled
        txtTFFOPFee.Enabled = varEnabled
        cboTFFOPFeeCCY.Enabled = varEnabled
        ChkCoverBrokerage.Enabled = varEnabled
        txtTFMin.Enabled = varEnabled
        txtTFMax.Enabled = varEnabled
        dgvTFSK.ReadOnly = Not varEnabled
        dgvTFSKFixed.ReadOnly = Not varEnabled
    End Sub

    Private Sub ChkTemplateName_CheckedChanged(sender As Object, e As EventArgs) Handles ChkTemplateName.CheckedChanged
        If ClsFS.IsTemplate = 0 Then
            cboTemplateName.Enabled = True
            If ChkTemplateName.Checked And IsNumeric(cboTemplateName.SelectedValue) Then
                GrpFSDetails.Enabled = False
                MGTFeeEnable(False)
                CFFeeEnable(False)
                TFFeeEnable(False)
            ElseIf IsNumeric(cboTemplateName.SelectedValue) Then
                cboTemplateName.Text = ""
                GrpFSDetails.Enabled = True
                MGTFeeEnable(True)
                CFFeeEnable(True)
                TFFeeEnable(True)
            End If
        Else
            cboTemplateName.Enabled = False
        End If
    End Sub

    Private Sub cboTemplateName_Leave(sender As Object, e As EventArgs) Handles cboTemplateName.Leave
        If IsNumeric(cboTemplateName.SelectedValue) Then
            ChkTemplateName.Checked = True
            ClsFS.TemplateLinkedID = cboTemplateName.SelectedValue
            Dim Lst = ClsIMS.GetDataToList("select MGTID,CFID,TFID from vwDolfinPaymentFeeSchedulesLinks where FS_ID = " & cboTemplateName.SelectedValue)
            If Not Lst Is Nothing Then
                LoadFeesMGT(Lst("MGTID"))
                LoadFeesCF(Lst("CFID"))
                LoadFeesTF(Lst("TFID"))
                LoadFeesFS(cboTemplateName.SelectedValue)
                GrpFSDetails.Enabled = False
                MGTFeeEnable(False)
                CFFeeEnable(False)
                TFFeeEnable(False)
            End If
        End If
    End Sub

    Private Sub dgvTF_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvTFRange.CellBeginEdit
        If e.RowIndex = 0 And e.ColumnIndex = 0 Then
            'e.Cancel = True
        End If
    End Sub

    Private Sub dgvTF_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvTFRange.CellFormatting
        If e.RowIndex = 0 And e.ColumnIndex = 0 Then
            'e.Value = ""
        End If
    End Sub

    Private Sub dgvTF_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvTFRange.CellValidating
        If e.ColumnIndex = 0 And ChkTF.Checked And e.FormattedValue = "" Then
            e.Cancel = True
        ElseIf e.ColumnIndex > 1 And e.FormattedValue <> "" Then
            If Not IsNumeric(e.FormattedValue) Then
                MsgBox("Numbers only", vbExclamation)
                e.Cancel = True
            ElseIf IsDBNull(dgvTFRange.Rows(e.RowIndex).Cells(1).Value) Then
                e.Cancel = True
            ElseIf e.ColumnIndex = 2 And (e.FormattedValue > cEndAmt Or e.FormattedValue < dgvTFRange.Rows(e.RowIndex).Cells(1).Value) Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub dgvTFRange_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvTFRange.UserDeletingRow
        If e.Row.Index = 0 Then
            e.Cancel = True
        Else
            dgvTFRange.Rows(e.Row.Index - 1).Cells(2).Value = cEndAmt
        End If
    End Sub

    Private Sub dgvTFRange_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTFRange.CellEndEdit
        If dgvTFRange.CurrentCell.ColumnIndex < 3 Then
            If dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Or IsNumeric(dgvTFRange.Rows(e.RowIndex).Cells(0).Value) Then
                If e.RowIndex > 0 And dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Then
                    dgvTFRange.Rows(e.RowIndex).Cells(0).Value = 0
                ElseIf dgvTFRange.Rows(e.RowIndex).Cells(0).Value = 0 And IsDBNull(dgvTFRange.Rows(e.RowIndex).Cells(2).Value) Then
                    dgvTFRange.Rows(e.RowIndex).Cells(0).Value = 0
                ElseIf dgvTFRange.Rows(e.RowIndex).Cells(0).Value <> 0 And IsDBNull(dgvTFRange.Rows(e.RowIndex).Cells(2).Value) Then
                    If CheckNewTransactionAssetType(dgvTFRange.Rows(e.RowIndex).Cells(0).Value) Then
                        dgvTFRange.Rows(e.RowIndex).Cells(1).Value = 0
                        dgvTFRange.Rows(e.RowIndex).Cells(2).Value = cEndAmt
                        dgvTFRange.Rows(e.RowIndex).Cells(3).Value = "0.0"
                    Else
                        dgvTFRange.Rows(e.RowIndex).Cells(0).Value = 0
                    End If
                ElseIf dgvTFRange.Rows(e.RowIndex).Cells(2).Value < cEndAmt And IsNumeric(IIf(dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing, 0, dgvTFRange.Rows(e.RowIndex).Cells(0).Value)) Then
                    If dgvTFRange.Rows(e.RowIndex).Cells(2).Value Is Nothing Then
                        If Not (dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing And e.ColumnIndex = 0 And e.RowIndex > 0) Then
                            AddTransactionRow(e.RowIndex, e.RowIndex + 1)
                            UpdateTransactionComboValue(e.RowIndex)
                        End If
                    ElseIf Not dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing And Not dgvTFRange.Rows(e.RowIndex + 1).Cells(2).Value Is Nothing Then
                        If dgvTFRange.Rows(e.RowIndex).Cells(2).Value > dgvTFRange.Rows(e.RowIndex).Cells(1).Value Then
                            dsTFRange.Tables("vwDolfinPaymentTransactionFeeRanges").Rows(e.RowIndex + 1)("From Amount") = dgvTFRange.Rows(e.RowIndex).Cells(2).Value + 1
                        Else
                            AddTransactionRow(e.RowIndex, e.RowIndex + 1)
                            UpdateTransactionComboValue(e.RowIndex)
                        End If
                    Else
                        AddTransactionRow(e.RowIndex, e.RowIndex + 1)
                        UpdateTransactionComboValue(e.RowIndex)
                    End If
                End If
            ElseIf Not dgvTFRange.Rows(e.RowIndex).Cells(0).Value Is Nothing Then
                AddTransactionRow(e.RowIndex, e.RowIndex)
            End If
        End If
    End Sub

    Private Sub cboTemplateName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboTemplateName.SelectedIndexChanged
        If InStr(1, LCase(cboTemplateName.Text), "custody", vbTextCompare) <> 0 Then
            TabCFCustody.Select()
        Else
            TabMgt.Select()
        End If
    End Sub

    Private Sub ChkRedirectFee_CheckedChanged(sender As Object, e As EventArgs) Handles ChkRedirectFee.CheckedChanged
        cboPortfolioRedirect.Enabled = False
        If IsNumeric(cboPortfolio.SelectedValue) Then
            If ChkRedirectFee.Checked And ClsFS.IsTemplate = 0 And cboPortfolio.SelectedValue <> 0 Then
                cboPortfolioRedirect.Enabled = True
            Else
                cboPortfolioRedirect.SelectedValue = 0
                cboPortfolioRedirect.Text = ""
                ChkRedirectFee.Checked = False
            End If
        End If
    End Sub

    Private Sub txtMgtMaintMin_Validating(sender As Object, e As CancelEventArgs) Handles txtMgtMaintMin.Validating
        If Not IsNumeric(sender.text) And sender.text <> "" Then
            MsgBox("Numbers only", vbExclamation)
            e.Cancel = True
        End If
    End Sub
End Class