﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGppMappingCodes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmGppMappingCodes))
        Me.lblGpp = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.cboClearerCode = New System.Windows.Forms.ComboBox()
        Me.lblClearerCode = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboCCY = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.lblClearer = New System.Windows.Forms.Label()
        Me.cboClearer = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.lblBrokerName = New System.Windows.Forms.Label()
        Me.cboBroker = New System.Windows.Forms.ComboBox()
        Me.dgvGppMappingCodes = New System.Windows.Forms.DataGridView()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvGppMappingCodes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblGpp
        '
        Me.lblGpp.AutoSize = True
        Me.lblGpp.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGpp.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblGpp.Location = New System.Drawing.Point(12, 9)
        Me.lblGpp.Name = "lblGpp"
        Me.lblGpp.Size = New System.Drawing.Size(117, 24)
        Me.lblGpp.TabIndex = 50
        Me.lblGpp.Text = "GPP Codes"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.cboClearerCode)
        Me.ViewGroupBox.Controls.Add(Me.lblClearerCode)
        Me.ViewGroupBox.Controls.Add(Me.Label3)
        Me.ViewGroupBox.Controls.Add(Me.cboCCY)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.lblClearer)
        Me.ViewGroupBox.Controls.Add(Me.cboClearer)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.lblBrokerName)
        Me.ViewGroupBox.Controls.Add(Me.cboBroker)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 45)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(639, 86)
        Me.ViewGroupBox.TabIndex = 51
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Criteria"
        '
        'cboClearerCode
        '
        Me.cboClearerCode.FormattingEnabled = True
        Me.cboClearerCode.Location = New System.Drawing.Point(355, 51)
        Me.cboClearerCode.Name = "cboClearerCode"
        Me.cboClearerCode.Size = New System.Drawing.Size(98, 21)
        Me.cboClearerCode.TabIndex = 60
        '
        'lblClearerCode
        '
        Me.lblClearerCode.AutoSize = True
        Me.lblClearerCode.Location = New System.Drawing.Point(281, 54)
        Me.lblClearerCode.Name = "lblClearerCode"
        Me.lblClearerCode.Size = New System.Drawing.Size(68, 13)
        Me.lblClearerCode.TabIndex = 59
        Me.lblClearerCode.Text = "Clearer Code"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(281, 29)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "CCY"
        '
        'cboCCY
        '
        Me.cboCCY.FormattingEnabled = True
        Me.cboCCY.Location = New System.Drawing.Point(355, 24)
        Me.cboCCY.Name = "cboCCY"
        Me.cboCCY.Size = New System.Drawing.Size(98, 21)
        Me.cboCCY.TabIndex = 57
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(588, 19)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 54
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'lblClearer
        '
        Me.lblClearer.AutoSize = True
        Me.lblClearer.Location = New System.Drawing.Point(6, 54)
        Me.lblClearer.Name = "lblClearer"
        Me.lblClearer.Size = New System.Drawing.Size(71, 13)
        Me.lblClearer.TabIndex = 43
        Me.lblClearer.Text = "Clearer Name"
        '
        'cboClearer
        '
        Me.cboClearer.FormattingEnabled = True
        Me.cboClearer.Location = New System.Drawing.Point(81, 51)
        Me.cboClearer.Name = "cboClearer"
        Me.cboClearer.Size = New System.Drawing.Size(194, 21)
        Me.cboClearer.TabIndex = 42
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(470, 53)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 29
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(470, 19)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 32)
        Me.CmdFilter.TabIndex = 31
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'lblBrokerName
        '
        Me.lblBrokerName.AutoSize = True
        Me.lblBrokerName.Location = New System.Drawing.Point(6, 27)
        Me.lblBrokerName.Name = "lblBrokerName"
        Me.lblBrokerName.Size = New System.Drawing.Size(69, 13)
        Me.lblBrokerName.TabIndex = 30
        Me.lblBrokerName.Text = "Broker Name"
        '
        'cboBroker
        '
        Me.cboBroker.FormattingEnabled = True
        Me.cboBroker.Location = New System.Drawing.Point(81, 24)
        Me.cboBroker.Name = "cboBroker"
        Me.cboBroker.Size = New System.Drawing.Size(194, 21)
        Me.cboBroker.TabIndex = 29
        '
        'dgvGppMappingCodes
        '
        Me.dgvGppMappingCodes.AllowUserToOrderColumns = True
        Me.dgvGppMappingCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGppMappingCodes.Location = New System.Drawing.Point(12, 138)
        Me.dgvGppMappingCodes.Name = "dgvGppMappingCodes"
        Me.dgvGppMappingCodes.Size = New System.Drawing.Size(1280, 660)
        Me.dgvGppMappingCodes.TabIndex = 54
        '
        'FrmGppMappingCodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1299, 801)
        Me.Controls.Add(Me.dgvGppMappingCodes)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.Controls.Add(Me.lblGpp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmGppMappingCodes"
        Me.Text = "GPP Mapping Codes"
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvGppMappingCodes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblGpp As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboCCY As ComboBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents lblClearer As Label
    Friend WithEvents cboClearer As ComboBox
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CmdFilter As Button
    Friend WithEvents lblBrokerName As Label
    Friend WithEvents cboBroker As ComboBox
    Friend WithEvents dgvGppMappingCodes As DataGridView
    Friend WithEvents lblClearerCode As Label
    Friend WithEvents cboClearerCode As ComboBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
