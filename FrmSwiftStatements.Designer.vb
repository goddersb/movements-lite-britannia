﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSwiftStatements
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSwiftStatements))
        Me.dgvAccounts = New System.Windows.Forms.DataGridView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtFromDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtToDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboType = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboCCY = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboAccounts = New System.Windows.Forms.ComboBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ViewGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvAccounts
        '
        Me.dgvAccounts.AllowUserToAddRows = False
        Me.dgvAccounts.AllowUserToDeleteRows = False
        Me.dgvAccounts.AllowUserToOrderColumns = True
        Me.dgvAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAccounts.Location = New System.Drawing.Point(12, 128)
        Me.dgvAccounts.Name = "dgvAccounts"
        Me.dgvAccounts.ReadOnly = True
        Me.dgvAccounts.Size = New System.Drawing.Size(1622, 740)
        Me.dgvAccounts.TabIndex = 46
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 7)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(316, 24)
        Me.Label13.TabIndex = 45
        Me.Label13.Text = "SWIFT Bank Account Statements"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.Label4)
        Me.ViewGroupBox.Controls.Add(Me.dtFromDate)
        Me.ViewGroupBox.Controls.Add(Me.Label3)
        Me.ViewGroupBox.Controls.Add(Me.dtToDate)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Controls.Add(Me.cboType)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboCCY)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboAccounts)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 45)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(878, 77)
        Me.ViewGroupBox.TabIndex = 44
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Criteria"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 13)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "Most recent date up to"
        '
        'dtFromDate
        '
        Me.dtFromDate.Location = New System.Drawing.Point(127, 20)
        Me.dtFromDate.Name = "dtFromDate"
        Me.dtFromDate.Size = New System.Drawing.Size(115, 20)
        Me.dtFromDate.TabIndex = 61
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 13)
        Me.Label3.TabIndex = 60
        Me.Label3.Text = "Most recent date up to"
        '
        'dtToDate
        '
        Me.dtToDate.Location = New System.Drawing.Point(127, 48)
        Me.dtToDate.Name = "dtToDate"
        Me.dtToDate.Size = New System.Drawing.Size(115, 20)
        Me.dtToDate.TabIndex = 59
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(420, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 13)
        Me.Label1.TabIndex = 58
        Me.Label1.Text = "Balance Type"
        '
        'cboType
        '
        Me.cboType.FormattingEnabled = True
        Me.cboType.Location = New System.Drawing.Point(505, 46)
        Me.cboType.Name = "cboType"
        Me.cboType.Size = New System.Drawing.Size(179, 21)
        Me.cboType.TabIndex = 57
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(255, 23)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 56
        Me.Label8.Text = "CCY"
        '
        'cboCCY
        '
        Me.cboCCY.FormattingEnabled = True
        Me.cboCCY.Location = New System.Drawing.Point(289, 20)
        Me.cboCCY.Name = "cboCCY"
        Me.cboCCY.Size = New System.Drawing.Size(69, 21)
        Me.cboCCY.TabIndex = 55
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(830, 18)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 54
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(702, 18)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 49)
        Me.CmdFilter.TabIndex = 31
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(370, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Account"
        '
        'cboAccounts
        '
        Me.cboAccounts.FormattingEnabled = True
        Me.cboAccounts.Location = New System.Drawing.Point(423, 19)
        Me.cboAccounts.Name = "cboAccounts"
        Me.cboAccounts.Size = New System.Drawing.Size(261, 21)
        Me.cboAccounts.TabIndex = 29
        '
        'FrmSwiftStatements
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1646, 880)
        Me.Controls.Add(Me.dgvAccounts)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmSwiftStatements"
        Me.Text = "Swift Intra-day Bank Account Statements"
        CType(Me.dgvAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvAccounts As DataGridView
    Friend WithEvents Label13 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents CmdFilter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboAccounts As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cboCCY As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboType As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents dtToDate As DateTimePicker
    Friend WithEvents Label4 As Label
    Friend WithEvents dtFromDate As DateTimePicker
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
