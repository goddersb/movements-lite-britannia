﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTreasuryAllocationTermDeposits
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTreasuryAllocationTermDeposits))
        Me.ViewResultsGroupBox = New System.Windows.Forms.GroupBox()
        Me.txtTATDFileName = New System.Windows.Forms.TextBox()
        Me.txtTATDStatus = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTATDMessage = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.ChkAllowSingles = New System.Windows.Forms.CheckBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CboTATDInstB = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CboTATDInstA = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboTATDCCY = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.CmdTATDRefreshgrid = New System.Windows.Forms.Button()
        Me.dgvTATDTitles = New System.Windows.Forms.DataGridView()
        Me.dgvTATD = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtTransDate = New System.Windows.Forms.DateTimePicker()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.TimerGetResults = New System.Windows.Forms.Timer(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.ViewResultsGroupBox.SuspendLayout()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvTATDTitles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvTATD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ViewResultsGroupBox
        '
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTATDFileName)
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTATDStatus)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label4)
        Me.ViewResultsGroupBox.Controls.Add(Me.txtTATDMessage)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label3)
        Me.ViewResultsGroupBox.Controls.Add(Me.Label5)
        Me.ViewResultsGroupBox.Location = New System.Drawing.Point(636, 15)
        Me.ViewResultsGroupBox.Name = "ViewResultsGroupBox"
        Me.ViewResultsGroupBox.Size = New System.Drawing.Size(447, 107)
        Me.ViewResultsGroupBox.TabIndex = 44
        Me.ViewResultsGroupBox.TabStop = False
        Me.ViewResultsGroupBox.Text = "Export Movements Results"
        '
        'txtTATDFileName
        '
        Me.txtTATDFileName.BackColor = System.Drawing.Color.Beige
        Me.txtTATDFileName.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTATDFileName.Location = New System.Drawing.Point(65, 19)
        Me.txtTATDFileName.Name = "txtTATDFileName"
        Me.txtTATDFileName.ReadOnly = True
        Me.txtTATDFileName.Size = New System.Drawing.Size(347, 13)
        Me.txtTATDFileName.TabIndex = 61
        Me.txtTATDFileName.TabStop = False
        '
        'txtTATDStatus
        '
        Me.txtTATDStatus.BackColor = System.Drawing.Color.Beige
        Me.txtTATDStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTATDStatus.Location = New System.Drawing.Point(65, 38)
        Me.txtTATDStatus.Name = "txtTATDStatus"
        Me.txtTATDStatus.ReadOnly = True
        Me.txtTATDStatus.Size = New System.Drawing.Size(347, 13)
        Me.txtTATDStatus.TabIndex = 60
        Me.txtTATDStatus.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 59
        Me.Label4.Text = "Status:"
        '
        'txtTATDMessage
        '
        Me.txtTATDMessage.BackColor = System.Drawing.Color.Beige
        Me.txtTATDMessage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTATDMessage.Location = New System.Drawing.Point(65, 60)
        Me.txtTATDMessage.Multiline = True
        Me.txtTATDMessage.Name = "txtTATDMessage"
        Me.txtTATDMessage.ReadOnly = True
        Me.txtTATDMessage.Size = New System.Drawing.Size(356, 41)
        Me.txtTATDMessage.TabIndex = 58
        Me.txtTATDMessage.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 60)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 57
        Me.Label3.Text = "Message:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "File Name:"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.ChkAllowSingles)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CboTATDInstB)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.CboTATDInstA)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Controls.Add(Me.cboTATDCCY)
        Me.ViewGroupBox.Controls.Add(Me.Label14)
        Me.ViewGroupBox.Controls.Add(Me.CmdTATDRefreshgrid)
        Me.ViewGroupBox.Location = New System.Drawing.Point(21, 12)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(609, 110)
        Me.ViewGroupBox.TabIndex = 43
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Movements Criteria"
        '
        'ChkAllowSingles
        '
        Me.ChkAllowSingles.AutoSize = True
        Me.ChkAllowSingles.Location = New System.Drawing.Point(185, 82)
        Me.ChkAllowSingles.Name = "ChkAllowSingles"
        Me.ChkAllowSingles.Size = New System.Drawing.Size(157, 17)
        Me.ChkAllowSingles.TabIndex = 58
        Me.ChkAllowSingles.Text = "Allow one-directional moves"
        Me.ChkAllowSingles.UseVisualStyleBackColor = True
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(564, 12)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 4
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(447, 72)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 26)
        Me.CmdRefreshView.TabIndex = 5
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CboTATDInstB
        '
        Me.CboTATDInstB.FormattingEnabled = True
        Me.CboTATDInstB.Location = New System.Drawing.Point(185, 55)
        Me.CboTATDInstB.Name = "CboTATDInstB"
        Me.CboTATDInstB.Size = New System.Drawing.Size(256, 21)
        Me.CboTATDInstB.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(117, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 57
        Me.Label2.Text = "Institution B"
        '
        'CboTATDInstA
        '
        Me.CboTATDInstA.FormattingEnabled = True
        Me.CboTATDInstA.Location = New System.Drawing.Point(185, 28)
        Me.CboTATDInstA.Name = "CboTATDInstA"
        Me.CboTATDInstA.Size = New System.Drawing.Size(256, 21)
        Me.CboTATDInstA.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(117, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 55
        Me.Label1.Text = "Institution A"
        '
        'cboTATDCCY
        '
        Me.cboTATDCCY.FormattingEnabled = True
        Me.cboTATDCCY.Location = New System.Drawing.Point(42, 29)
        Me.cboTATDCCY.Name = "cboTATDCCY"
        Me.cboTATDCCY.Size = New System.Drawing.Size(69, 21)
        Me.cboTATDCCY.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 33)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(28, 13)
        Me.Label14.TabIndex = 53
        Me.Label14.Text = "CCY"
        '
        'CmdTATDRefreshgrid
        '
        Me.CmdTATDRefreshgrid.Location = New System.Drawing.Point(446, 29)
        Me.CmdTATDRefreshgrid.Name = "CmdTATDRefreshgrid"
        Me.CmdTATDRefreshgrid.Size = New System.Drawing.Size(112, 37)
        Me.CmdTATDRefreshgrid.TabIndex = 3
        Me.CmdTATDRefreshgrid.Text = "Filter Data Grid"
        Me.CmdTATDRefreshgrid.UseVisualStyleBackColor = True
        '
        'dgvTATDTitles
        '
        Me.dgvTATDTitles.AllowUserToDeleteRows = False
        Me.dgvTATDTitles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTATDTitles.Location = New System.Drawing.Point(21, 130)
        Me.dgvTATDTitles.Name = "dgvTATDTitles"
        Me.dgvTATDTitles.ReadOnly = True
        Me.dgvTATDTitles.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTATDTitles.Size = New System.Drawing.Size(1062, 20)
        Me.dgvTATDTitles.TabIndex = 46
        Me.dgvTATDTitles.TabStop = False
        '
        'dgvTATD
        '
        Me.dgvTATD.AllowUserToAddRows = False
        Me.dgvTATD.AllowUserToDeleteRows = False
        Me.dgvTATD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTATD.Location = New System.Drawing.Point(21, 148)
        Me.dgvTATD.Name = "dgvTATD"
        Me.dgvTATD.Size = New System.Drawing.Size(1062, 614)
        Me.dgvTATD.TabIndex = 45
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(690, 774)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 13)
        Me.Label6.TabIndex = 64
        Me.Label6.Text = "Trans Date"
        '
        'dtTransDate
        '
        Me.dtTransDate.Location = New System.Drawing.Point(756, 770)
        Me.dtTransDate.Name = "dtTransDate"
        Me.dtTransDate.Size = New System.Drawing.Size(144, 20)
        Me.dtTransDate.TabIndex = 63
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Location = New System.Drawing.Point(926, 768)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(157, 24)
        Me.cmdSubmit.TabIndex = 62
        Me.cmdSubmit.Text = "Submit transactions"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'TimerGetResults
        '
        Me.TimerGetResults.Interval = 5000
        '
        'FrmTreasuryAllocationTermDeposits
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Beige
        Me.ClientSize = New System.Drawing.Size(1102, 798)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dtTransDate)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.dgvTATDTitles)
        Me.Controls.Add(Me.dgvTATD)
        Me.Controls.Add(Me.ViewResultsGroupBox)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmTreasuryAllocationTermDeposits"
        Me.Text = "TreasuryAllocation (TermDeposits)"
        Me.ViewResultsGroupBox.ResumeLayout(False)
        Me.ViewResultsGroupBox.PerformLayout()
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvTATDTitles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvTATD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ViewResultsGroupBox As GroupBox
    Friend WithEvents txtTATDFileName As TextBox
    Friend WithEvents txtTATDStatus As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTATDMessage As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CboTATDInstB As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CboTATDInstA As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboTATDCCY As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents CmdTATDRefreshgrid As Button
    Friend WithEvents dgvTATDTitles As DataGridView
    Friend WithEvents dgvTATD As DataGridView
    Friend WithEvents Label6 As Label
    Friend WithEvents dtTransDate As DateTimePicker
    Friend WithEvents cmdSubmit As Button
    Friend WithEvents TimerGetResults As Timer
    Friend WithEvents ChkAllowSingles As CheckBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
