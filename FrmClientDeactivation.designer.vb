﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmClientDeactivation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmClientDeactivation))
        Me.cmdUpdate = New System.Windows.Forms.Button()
        Me.cboClientList = New System.Windows.Forms.ComboBox()
        Me.dgvWatchList = New System.Windows.Forms.DataGridView()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpComplianceDetail = New System.Windows.Forms.GroupBox()
        Me.chkIsAllClient = New System.Windows.Forms.CheckBox()
        Me.dtpReportDate = New System.Windows.Forms.DateTimePicker()
        Me.cmdExtApprove = New System.Windows.Forms.Button()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        CType(Me.dgvWatchList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUpdate.Location = New System.Drawing.Point(774, 94)
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(187, 29)
        Me.cmdUpdate.TabIndex = 0
        Me.cmdUpdate.Text = "Deactivate"
        Me.cmdUpdate.UseVisualStyleBackColor = True
        '
        'cboClientList
        '
        Me.cboClientList.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboClientList.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboClientList.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClientList.FormattingEnabled = True
        Me.cboClientList.Location = New System.Drawing.Point(12, 97)
        Me.cboClientList.Name = "cboClientList"
        Me.cboClientList.Size = New System.Drawing.Size(497, 24)
        Me.cboClientList.TabIndex = 1
        '
        'dgvWatchList
        '
        Me.dgvWatchList.AllowUserToAddRows = False
        Me.dgvWatchList.AllowUserToDeleteRows = False
        Me.dgvWatchList.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvWatchList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvWatchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWatchList.Location = New System.Drawing.Point(12, 188)
        Me.dgvWatchList.Name = "dgvWatchList"
        Me.dgvWatchList.ReadOnly = True
        Me.dgvWatchList.Size = New System.Drawing.Size(1142, 392)
        Me.dgvWatchList.TabIndex = 2
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdRefresh.Location = New System.Drawing.Point(967, 94)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(111, 29)
        Me.cmdRefresh.TabIndex = 3
        Me.cmdRefresh.Text = "Reset"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 22.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(427, 36)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Client Watchlist Maintenance"
        '
        'grpComplianceDetail
        '
        Me.grpComplianceDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpComplianceDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpComplianceDetail.Location = New System.Drawing.Point(5, 158)
        Me.grpComplianceDetail.Name = "grpComplianceDetail"
        Me.grpComplianceDetail.Size = New System.Drawing.Size(1155, 430)
        Me.grpComplianceDetail.TabIndex = 7
        Me.grpComplianceDetail.TabStop = False
        Me.grpComplianceDetail.Text = "Compliance Inactive Clients (last 180 days)"
        '
        'chkIsAllClient
        '
        Me.chkIsAllClient.AutoSize = True
        Me.chkIsAllClient.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsAllClient.Location = New System.Drawing.Point(677, 99)
        Me.chkIsAllClient.Name = "chkIsAllClient"
        Me.chkIsAllClient.Size = New System.Drawing.Size(89, 21)
        Me.chkIsAllClient.TabIndex = 8
        Me.chkIsAllClient.Text = "All Client?"
        Me.chkIsAllClient.UseVisualStyleBackColor = True
        '
        'dtpReportDate
        '
        Me.dtpReportDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpReportDate.Location = New System.Drawing.Point(515, 98)
        Me.dtpReportDate.Name = "dtpReportDate"
        Me.dtpReportDate.Size = New System.Drawing.Size(156, 23)
        Me.dtpReportDate.TabIndex = 9
        '
        'cmdExtApprove
        '
        Me.cmdExtApprove.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdExtApprove.Location = New System.Drawing.Point(774, 129)
        Me.cmdExtApprove.Name = "cmdExtApprove"
        Me.cmdExtApprove.Size = New System.Drawing.Size(187, 29)
        Me.cmdExtApprove.TabIndex = 10
        Me.cmdExtApprove.Text = "Ext. Approve"
        Me.cmdExtApprove.UseVisualStyleBackColor = True
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1039, 125)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 33)
        Me.CmdExportToExcel.TabIndex = 56
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'FrmClientDeactivation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1166, 592)
        Me.Controls.Add(Me.CmdExportToExcel)
        Me.Controls.Add(Me.cmdExtApprove)
        Me.Controls.Add(Me.dtpReportDate)
        Me.Controls.Add(Me.chkIsAllClient)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdRefresh)
        Me.Controls.Add(Me.dgvWatchList)
        Me.Controls.Add(Me.cboClientList)
        Me.Controls.Add(Me.cmdUpdate)
        Me.Controls.Add(Me.grpComplianceDetail)
        Me.Name = "FrmClientDeactivation"
        Me.Text = "Client Watchlist"
        CType(Me.dgvWatchList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cmdUpdate As Button
    Friend WithEvents cboClientList As ComboBox
    Friend WithEvents dgvWatchList As DataGridView
    Friend WithEvents cmdRefresh As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents grpComplianceDetail As GroupBox
    Friend WithEvents chkIsAllClient As CheckBox
    Friend WithEvents dtpReportDate As DateTimePicker
    Friend WithEvents cmdExtApprove As Button
    Friend WithEvents CmdExportToExcel As Button
End Class
