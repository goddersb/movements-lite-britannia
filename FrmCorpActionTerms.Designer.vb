﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCorpActionTerms
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCorpActionTerms))
        Me.TabTerms = New System.Windows.Forms.TabControl()
        Me.TabEvent = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dgvCATerms = New System.Windows.Forms.DataGridView()
        Me.TabElections = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgvCAPortfolio = New System.Windows.Forms.DataGridView()
        Me.TabInfo = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dgvCAInfo = New System.Windows.Forms.DataGridView()
        Me.lblfeeTitle = New System.Windows.Forms.Label()
        Me.txtCAMV = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCAEvent = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCAName = New System.Windows.Forms.TextBox()
        Me.lblauthportfolio = New System.Windows.Forms.Label()
        Me.txtCAISIN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LblElected = New System.Windows.Forms.Label()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.txtCAPositionDifference = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCAIMSPosition = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtCASettledPosition = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtCARecDate = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCAExDate = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCACustodianDeadline = New System.Windows.Forms.TextBox()
        Me.txtCADolfinDeadline = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCACustodian = New System.Windows.Forms.TextBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.TabTerms.SuspendLayout()
        Me.TabEvent.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvCATerms, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabElections.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvCAPortfolio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInfo.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvCAInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabTerms
        '
        Me.TabTerms.Controls.Add(Me.TabEvent)
        Me.TabTerms.Controls.Add(Me.TabElections)
        Me.TabTerms.Controls.Add(Me.TabInfo)
        Me.TabTerms.Location = New System.Drawing.Point(12, 238)
        Me.TabTerms.Name = "TabTerms"
        Me.TabTerms.SelectedIndex = 0
        Me.TabTerms.Size = New System.Drawing.Size(1217, 428)
        Me.TabTerms.TabIndex = 0
        '
        'TabEvent
        '
        Me.TabEvent.BackColor = System.Drawing.Color.FloralWhite
        Me.TabEvent.Controls.Add(Me.GroupBox2)
        Me.TabEvent.Location = New System.Drawing.Point(4, 22)
        Me.TabEvent.Name = "TabEvent"
        Me.TabEvent.Padding = New System.Windows.Forms.Padding(3)
        Me.TabEvent.Size = New System.Drawing.Size(1209, 402)
        Me.TabEvent.TabIndex = 0
        Me.TabEvent.Text = "Event Options"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvCATerms)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1196, 390)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options available for event"
        '
        'dgvCATerms
        '
        Me.dgvCATerms.AllowUserToAddRows = False
        Me.dgvCATerms.AllowUserToDeleteRows = False
        Me.dgvCATerms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCATerms.Location = New System.Drawing.Point(2, 19)
        Me.dgvCATerms.MultiSelect = False
        Me.dgvCATerms.Name = "dgvCATerms"
        Me.dgvCATerms.ReadOnly = True
        Me.dgvCATerms.Size = New System.Drawing.Size(1161, 365)
        Me.dgvCATerms.TabIndex = 41
        '
        'TabElections
        '
        Me.TabElections.BackColor = System.Drawing.Color.FloralWhite
        Me.TabElections.Controls.Add(Me.GroupBox3)
        Me.TabElections.Location = New System.Drawing.Point(4, 22)
        Me.TabElections.Name = "TabElections"
        Me.TabElections.Padding = New System.Windows.Forms.Padding(3)
        Me.TabElections.Size = New System.Drawing.Size(1209, 402)
        Me.TabElections.TabIndex = 1
        Me.TabElections.Text = "Elections"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvCAPortfolio)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1196, 390)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Select drop down to select event per portfolio"
        '
        'dgvCAPortfolio
        '
        Me.dgvCAPortfolio.AllowUserToAddRows = False
        Me.dgvCAPortfolio.AllowUserToDeleteRows = False
        Me.dgvCAPortfolio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCAPortfolio.Location = New System.Drawing.Point(2, 19)
        Me.dgvCAPortfolio.MultiSelect = False
        Me.dgvCAPortfolio.Name = "dgvCAPortfolio"
        Me.dgvCAPortfolio.Size = New System.Drawing.Size(1161, 365)
        Me.dgvCAPortfolio.TabIndex = 40
        '
        'TabInfo
        '
        Me.TabInfo.BackColor = System.Drawing.Color.FloralWhite
        Me.TabInfo.Controls.Add(Me.GroupBox4)
        Me.TabInfo.Location = New System.Drawing.Point(4, 22)
        Me.TabInfo.Name = "TabInfo"
        Me.TabInfo.Size = New System.Drawing.Size(1209, 402)
        Me.TabInfo.TabIndex = 2
        Me.TabInfo.Text = "More Information"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvCAInfo)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1196, 390)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "More Information (MT568)"
        '
        'dgvCAInfo
        '
        Me.dgvCAInfo.AllowUserToAddRows = False
        Me.dgvCAInfo.AllowUserToDeleteRows = False
        Me.dgvCAInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCAInfo.Location = New System.Drawing.Point(2, 19)
        Me.dgvCAInfo.MultiSelect = False
        Me.dgvCAInfo.Name = "dgvCAInfo"
        Me.dgvCAInfo.ReadOnly = True
        Me.dgvCAInfo.Size = New System.Drawing.Size(1161, 365)
        Me.dgvCAInfo.TabIndex = 41
        '
        'lblfeeTitle
        '
        Me.lblfeeTitle.AutoSize = True
        Me.lblfeeTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfeeTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblfeeTitle.Location = New System.Drawing.Point(12, 21)
        Me.lblfeeTitle.Name = "lblfeeTitle"
        Me.lblfeeTitle.Size = New System.Drawing.Size(232, 24)
        Me.lblfeeTitle.TabIndex = 11
        Me.lblfeeTitle.Text = "Corporate Action Terms"
        '
        'txtCAMV
        '
        Me.txtCAMV.Location = New System.Drawing.Point(76, 100)
        Me.txtCAMV.Name = "txtCAMV"
        Me.txtCAMV.Size = New System.Drawing.Size(334, 20)
        Me.txtCAMV.TabIndex = 106
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 105
        Me.Label3.Text = "M/V:"
        '
        'txtCAEvent
        '
        Me.txtCAEvent.Location = New System.Drawing.Point(76, 74)
        Me.txtCAEvent.Name = "txtCAEvent"
        Me.txtCAEvent.Size = New System.Drawing.Size(334, 20)
        Me.txtCAEvent.TabIndex = 104
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 103
        Me.Label2.Text = "CA Event:"
        '
        'txtCAName
        '
        Me.txtCAName.Location = New System.Drawing.Point(76, 22)
        Me.txtCAName.Name = "txtCAName"
        Me.txtCAName.Size = New System.Drawing.Size(334, 20)
        Me.txtCAName.TabIndex = 102
        '
        'lblauthportfolio
        '
        Me.lblauthportfolio.AutoSize = True
        Me.lblauthportfolio.Location = New System.Drawing.Point(32, 25)
        Me.lblauthportfolio.Name = "lblauthportfolio"
        Me.lblauthportfolio.Size = New System.Drawing.Size(38, 13)
        Me.lblauthportfolio.TabIndex = 101
        Me.lblauthportfolio.Text = "Name:"
        '
        'txtCAISIN
        '
        Me.txtCAISIN.Location = New System.Drawing.Point(76, 48)
        Me.txtCAISIN.Name = "txtCAISIN"
        Me.txtCAISIN.Size = New System.Drawing.Size(334, 20)
        Me.txtCAISIN.TabIndex = 100
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(39, 51)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 99
        Me.Label5.Text = "ISIN:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LblElected)
        Me.GroupBox1.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox1.Controls.Add(Me.txtCAPositionDifference)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtCAIMSPosition)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtCASettledPosition)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtCARecDate)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtCAExDate)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCACustodianDeadline)
        Me.GroupBox1.Controls.Add(Me.txtCADolfinDeadline)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtCACustodian)
        Me.GroupBox1.Controls.Add(Me.txtCAMV)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtCAISIN)
        Me.GroupBox1.Controls.Add(Me.txtCAEvent)
        Me.GroupBox1.Controls.Add(Me.lblauthportfolio)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCAName)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 58)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1213, 151)
        Me.GroupBox1.TabIndex = 107
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Corporate Action Event Details"
        '
        'LblElected
        '
        Me.LblElected.AutoSize = True
        Me.LblElected.Location = New System.Drawing.Point(866, 132)
        Me.LblElected.Name = "LblElected"
        Me.LblElected.Size = New System.Drawing.Size(82, 13)
        Me.LblElected.TabIndex = 124
        Me.LblElected.Text = "Last elected by:"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1167, 108)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 123
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'txtCAPositionDifference
        '
        Me.txtCAPositionDifference.Location = New System.Drawing.Point(840, 100)
        Me.txtCAPositionDifference.Name = "txtCAPositionDifference"
        Me.txtCAPositionDifference.Size = New System.Drawing.Size(125, 20)
        Me.txtCAPositionDifference.TabIndex = 122
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(735, 103)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 13)
        Me.Label11.TabIndex = 121
        Me.Label11.Text = "Position Difference:"
        '
        'txtCAIMSPosition
        '
        Me.txtCAIMSPosition.Location = New System.Drawing.Point(840, 74)
        Me.txtCAIMSPosition.Name = "txtCAIMSPosition"
        Me.txtCAIMSPosition.Size = New System.Drawing.Size(125, 20)
        Me.txtCAIMSPosition.TabIndex = 120
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(735, 77)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(96, 13)
        Me.Label10.TabIndex = 119
        Me.Label10.Text = "IMS Position Total:"
        '
        'txtCASettledPosition
        '
        Me.txtCASettledPosition.Location = New System.Drawing.Point(840, 48)
        Me.txtCASettledPosition.Name = "txtCASettledPosition"
        Me.txtCASettledPosition.Size = New System.Drawing.Size(125, 20)
        Me.txtCASettledPosition.TabIndex = 118
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(721, 51)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(110, 13)
        Me.Label9.TabIndex = 117
        Me.Label9.Text = "Settled Position Total:"
        '
        'txtCARecDate
        '
        Me.txtCARecDate.Location = New System.Drawing.Point(840, 22)
        Me.txtCARecDate.Name = "txtCARecDate"
        Me.txtCARecDate.Size = New System.Drawing.Size(125, 20)
        Me.txtCARecDate.TabIndex = 116
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(775, 25)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 13)
        Me.Label8.TabIndex = 115
        Me.Label8.Text = "Rec Date:"
        '
        'txtCAExDate
        '
        Me.txtCAExDate.Location = New System.Drawing.Point(553, 100)
        Me.txtCAExDate.Name = "txtCAExDate"
        Me.txtCAExDate.Size = New System.Drawing.Size(148, 20)
        Me.txtCAExDate.TabIndex = 114
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(419, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 13)
        Me.Label1.TabIndex = 107
        Me.Label1.Text = "Custodian Deadline Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(496, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 113
        Me.Label4.Text = "Ex Date:"
        '
        'txtCACustodianDeadline
        '
        Me.txtCACustodianDeadline.Location = New System.Drawing.Point(553, 48)
        Me.txtCACustodianDeadline.Name = "txtCACustodianDeadline"
        Me.txtCACustodianDeadline.Size = New System.Drawing.Size(148, 20)
        Me.txtCACustodianDeadline.TabIndex = 108
        '
        'txtCADolfinDeadline
        '
        Me.txtCADolfinDeadline.Location = New System.Drawing.Point(553, 74)
        Me.txtCADolfinDeadline.Name = "txtCADolfinDeadline"
        Me.txtCADolfinDeadline.Size = New System.Drawing.Size(148, 20)
        Me.txtCADolfinDeadline.TabIndex = 112
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(490, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(57, 13)
        Me.Label6.TabIndex = 109
        Me.Label6.Text = "Custodian:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(439, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(108, 13)
        Me.Label7.TabIndex = 111
        Me.Label7.Text = "Dolfin Deadline Date:"
        '
        'txtCACustodian
        '
        Me.txtCACustodian.Location = New System.Drawing.Point(553, 22)
        Me.txtCACustodian.Name = "txtCACustodian"
        Me.txtCACustodian.Size = New System.Drawing.Size(148, 20)
        Me.txtCACustodian.TabIndex = 110
        '
        'FrmCorpActionTerms
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1241, 678)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblfeeTitle)
        Me.Controls.Add(Me.TabTerms)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCorpActionTerms"
        Me.Text = "FrmCorpActionTerms"
        Me.TabTerms.ResumeLayout(False)
        Me.TabEvent.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvCATerms, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabElections.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvCAPortfolio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInfo.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvCAInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabTerms As TabControl
    Friend WithEvents TabEvent As TabPage
    Friend WithEvents TabElections As TabPage
    Friend WithEvents lblfeeTitle As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents dgvCATerms As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents dgvCAPortfolio As DataGridView
    Friend WithEvents txtCAMV As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCAEvent As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtCAName As TextBox
    Friend WithEvents lblauthportfolio As Label
    Friend WithEvents txtCAISIN As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TabInfo As TabPage
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents dgvCAInfo As DataGridView
    Friend WithEvents txtCAPositionDifference As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtCAIMSPosition As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtCASettledPosition As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtCARecDate As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtCAExDate As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtCACustodianDeadline As TextBox
    Friend WithEvents txtCADolfinDeadline As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents txtCACustodian As TextBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents LblElected As Label
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
