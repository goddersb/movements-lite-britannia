﻿Public Class FrmPaymentRequestViewSwift
    Private Sub FrmPaymentRequestViewSwift_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If ClsPR.SelectedAuthoriseOption = 1 Then
            txtSwiftView.Text = ClsIMS.GetSwiftFileByID(ClsPR.ID)
        ElseIf ClsPR.SelectedAuthoriseOption = 2 Then
            Dim varConfirmSwiftFile = ClsIMS.GetSQLItem("select SR_FileName from dolfinpaymentswiftread where sr_relatedreference = '" & ClsPR.OrderRef & "'")
            If varConfirmSwiftFile <> "" Then
                Dim swiftstream As New IO.FileStream(ClsIMS.GetFilePath("SWIFTImport") & varConfirmSwiftFile, IO.FileMode.Open, IO.FileAccess.Read)
                Dim objSwiftReader = New IO.StreamReader(swiftstream)

                If Not objSwiftReader Is Nothing Then
                    While objSwiftReader.Peek() >= 0
                        txtSwiftView.Text = txtSwiftView.Text & objSwiftReader.ReadLine() & vbNewLine
                    End While
                End If
                objSwiftReader = Nothing
            Else
                Me.Close()
                MsgBox("No confirmation swift is available for this payment yet", vbInformation)
            End If
        End If
    End Sub
End Class