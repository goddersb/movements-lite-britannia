﻿Public Class FrmSystemSettings
    Dim SSConn As New SqlClient.SqlConnection
    Dim FEdgvdata As New SqlClient.SqlDataAdapter
    Dim FPdgvdata As New SqlClient.SqlDataAdapter
    Dim STdgvdata As New SqlClient.SqlDataAdapter
    Dim PTdgvdata As New SqlClient.SqlDataAdapter
    Dim MTdgvdata As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet

    Private Sub FrmSystemSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "select * from vwDolfinPaymentFileExport"
            dgvFE.DataSource = Nothing
            SSConn = ClsIMS.GetNewOpenConnection
            FEdgvdata = New SqlClient.SqlDataAdapter(varSQL, SSConn)
            ds = New DataSet
            FEdgvdata.Fill(ds, "vwDolfinPaymentFileExport")
            dgvFE.AutoGenerateColumns = True
            dgvFE.DataSource = ds.Tables("vwDolfinPaymentFileExport")
            dgvFE.Columns("FE_ID").Width = 60
            dgvFE.Columns("FE_ID").ReadOnly = True
            dgvFE.Columns("FE_ID").DefaultCellStyle.BackColor = Color.LightGray
            dgvFE.Columns("FE_Key").Width = 120
            dgvFE.Columns("FE_Key").ReadOnly = True
            dgvFE.Columns("FE_Key").DefaultCellStyle.BackColor = Color.LightGray
            dgvFE.Columns("FE_Name").Width = 120
            dgvFE.Columns("FE_ExportBuffer").Width = 100
            dgvFE.Columns("FE_UserModifiedBy").Visible = False
            dgvFE.Columns("FE_UserModifiedTime").Visible = False
            dgvFE.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvFE.Refresh()

            varSQL = "select Path_ID,Path_Name,'...' as Path_PathFinder,Path_FilePath,Path_UserModifiedBy,Path_UserModifiedTime from vwDolfinPaymentFilePath"
            dgvFP.DataSource = Nothing
            SSConn = ClsIMS.GetNewOpenConnection
            FPdgvdata = New SqlClient.SqlDataAdapter(varSQL, SSConn)
            ds = New DataSet
            FPdgvdata.Fill(ds, "vwDolfinPaymentFilePath")
            dgvFP.AutoGenerateColumns = True
            dgvFP.DataSource = ds.Tables("vwDolfinPaymentFilePath")
            dgvFP.Columns("Path_ID").Width = 60
            dgvFP.Columns("Path_ID").ReadOnly = True
            dgvFP.Columns("Path_ID").DefaultCellStyle.BackColor = Color.LightGray
            dgvFP.Columns("Path_Name").Width = 100
            dgvFP.Columns("Path_Name").ReadOnly = True
            dgvFP.Columns("Path_Name").DefaultCellStyle.BackColor = Color.LightGray
            dgvFP.Columns("Path_FilePath").Width = 1000
            dgvFP.Columns("Path_PathFinder").Width = 17
            dgvFP.Columns("Path_PathFinder").ReadOnly = True
            dgvFP.Columns("Path_PathFinder").HeaderText = ""
            dgvFP.Columns("Path_UserModifiedBy").Visible = False
            dgvFP.Columns("Path_UserModifiedTime").Visible = False
            dgvFP.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvFP.Refresh()

            varSQL = "select * from vwDolfinPaymentStatus"
            dgvST.DataSource = Nothing
            SSConn = ClsIMS.GetNewOpenConnection
            STdgvdata = New SqlClient.SqlDataAdapter(varSQL, SSConn)
            ds = New DataSet
            STdgvdata.Fill(ds, "vwDolfinPaymentStatus")
            dgvST.AutoGenerateColumns = True
            dgvST.DataSource = ds.Tables("vwDolfinPaymentStatus")
            dgvST.Columns("S_ID").Width = 60
            dgvST.Columns("S_ID").ReadOnly = True
            dgvST.Columns("S_ID").DefaultCellStyle.BackColor = Color.LightGray
            dgvST.Columns("S_Status").Width = 300
            dgvST.Columns("S_UserModifiedBy").Visible = False
            dgvST.Columns("S_UserModifiedTime").Visible = False
            dgvST.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvST.Refresh()

            varSQL = "select * from vwDolfinPaymentType"
            dgvPT.DataSource = Nothing
            SSConn = ClsIMS.GetNewOpenConnection
            PTdgvdata = New SqlClient.SqlDataAdapter(varSQL, SSConn)
            ds = New DataSet
            PTdgvdata.Fill(ds, "vwDolfinPaymentType")
            dgvPT.AutoGenerateColumns = True
            dgvPT.DataSource = ds.Tables("vwDolfinPaymentType")
            dgvPT.Columns("PT_ID").Width = 60
            dgvPT.Columns("PT_ID").ReadOnly = True
            dgvPT.Columns("PT_ID").DefaultCellStyle.BackColor = Color.LightGray
            dgvPT.Columns("PT_PaymentType").Width = 250
            dgvPT.Columns("PT_ShortCode").Width = 100
            dgvPT.Columns("PT_UserModifiedBy").Visible = False
            dgvPT.Columns("PT_UserModifiedTime").Visible = False
            dgvPT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvPT.Refresh()


            varSQL = "select * from vwDolfinPaymentMessageType"
            dgvMT.DataSource = Nothing
            SSConn = ClsIMS.GetNewOpenConnection
            MTdgvdata = New SqlClient.SqlDataAdapter(varSQL, SSConn)
            ds = New DataSet
            MTdgvdata.Fill(ds, "vwDolfinPaymentMessageType")
            dgvMT.AutoGenerateColumns = True
            dgvMT.DataSource = ds.Tables("vwDolfinPaymentMessageType")
            dgvMT.Columns("M_ID").Width = 60
            dgvMT.Columns("M_ID").ReadOnly = True
            dgvMT.Columns("M_ID").DefaultCellStyle.BackColor = Color.LightGray
            dgvMT.Columns("M_MessageType").Width = 100
            dgvMT.Columns("M_UserModifiedBy").Visible = False
            dgvMT.Columns("M_UserModifiedTime").Visible = False
            dgvMT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvMT.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvFE.DataSource = Nothing
            dgvFP.DataSource = Nothing
            dgvPT.DataSource = Nothing
            dgvMT.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSystemSettings, Err.Description & " - Problem with viewing system settings")
        End Try
    End Sub

    Private Sub dgvFE_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFE.RowLeave
        If dgvFE.EndEdit Then
            Dim varID As Integer
            Dim varName As String = ""
            Dim varBuffer As Boolean = False

            Try
                varID = dgvFE.Rows(e.RowIndex).Cells(0).Value
                varName = dgvFE.Rows(e.RowIndex).Cells(2).Value
                varBuffer = dgvFE.Rows(e.RowIndex).Cells(3).Value

                If IsNumeric(varID) Then
                    If varID <> 0 Then
                        Dim varSQL As String = "update vwDolfinPaymentFileExport set FE_Name = '" & varName & "', FE_ExportBuffer = " & IIf(varBuffer, 1, 0) & ",FE_UserModifiedBy = '" & ClsIMS.FullUserName & "',FE_UserModifiedTime = getdate() where FE_ID = " & varID
                        ClsIMS.ExecuteString(SSConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSystemSettings, varSQL)
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub dgvFP_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFP.RowLeave
        If dgvFP.EndEdit Then
            Dim varID As Integer
            Dim varFilePath As String = ""

            Try
                varID = dgvFP.Rows(e.RowIndex).Cells(0).Value
                varFilePath = dgvFP.Rows(e.RowIndex).Cells(3).Value

                If IsNumeric(varID) Then
                    If varID <> 0 Then
                        Dim varSQL As String = "update vwDolfinPaymentFilePath set Path_FilePath = '" & IIf(Strings.Right(varFilePath, 1) <> "\", varFilePath & "\", varFilePath) & "',Path_UserModifiedBy = '" & ClsIMS.FullUserName & "',Path_UserModifiedTime = getdate()  where Path_ID = " & varID
                        ClsIMS.ExecuteString(SSConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSystemSettings, varSQL)
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub dgvMT_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvMT.RowLeave
        If dgvMT.EndEdit Then
            Dim varID As Integer
            Dim varMessageType As String = ""
            Dim varIncoming As Boolean = False

            Try
                varID = dgvMT.Rows(e.RowIndex).Cells(0).Value
                varMessageType = dgvMT.Rows(e.RowIndex).Cells(1).Value
                varIncoming = dgvMT.Rows(e.RowIndex).Cells(2).Value

                If IsNumeric(varID) Then
                    If varID <> 0 Then
                        Dim varSQL As String = "update vwDolfinPaymentMessageType set M_MessageType = '" & varMessageType & "', M_Incoming = " & IIf(varIncoming, 1, 0) & ",M_UserModifiedBy = '" & ClsIMS.FullUserName & "',M_UserModifiedTime = getdate()  where M_ID = " & varID
                        ClsIMS.ExecuteString(SSConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSystemSettings, varSQL)
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub dgvPT_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPT.RowLeave
        If dgvPT.EndEdit Then
            Dim varID As Integer
            Dim varPayType As String = "", varPayShortcut As String = "", varIMSINCode As String = "", varIMSOUTCode As String = "", varCom As String = ""
            Dim varDefSendIMS As Boolean = True, varDefSendSwift As Boolean = True

            Try
                varID = dgvPT.Rows(e.RowIndex).Cells(0).Value
                varPayType = dgvPT.Rows(e.RowIndex).Cells(1).Value
                varPayShortcut = dgvPT.Rows(e.RowIndex).Cells(2).Value
                varDefSendIMS = dgvPT.Rows(e.RowIndex).Cells(3).Value
                varDefSendSwift = dgvPT.Rows(e.RowIndex).Cells(4).Value
                varIMSINCode = dgvPT.Rows(e.RowIndex).Cells(6).Value
                varIMSOUTCode = dgvPT.Rows(e.RowIndex).Cells(7).Value
                varCom = IIf(IsDBNull(dgvPT.Rows(e.RowIndex).Cells(8).Value), "", dgvPT.Rows(e.RowIndex).Cells(8).Value)

                If IsNumeric(varID) Then
                    If varID <> 0 Then
                        Dim varSQL As String = "update vwDolfinPaymentType set PT_PaymentType = '" & varPayType & "'," &
                                                    "PT_DefaultSendIMS = " & IIf(varDefSendIMS, 1, 0) & "," &
                                                    "PT_DefaultSendSWIFT = " & IIf(varDefSendSwift, 1, 0) & "," &
                                                    "PT_IMSPayINCode = '" & varIMSINCode & "'," &
                                                    "PT_IMSPayOUTCode = '" & varIMSOUTCode & "'," &
                                                    "PT_AutoCommentHeader = '" & varCom & "',PT_UserModifiedBy = '" & ClsIMS.FullUserName & "',PT_UserModifiedTime = getdate() where PT_ID = " & varID
                        ClsIMS.ExecuteString(SSConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSystemSettings, varSQL)
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub dgvST_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvST.RowLeave
        If dgvST.EndEdit Then
            Dim varID As Integer
            Dim varStatusType As String = ""

            Try
                varID = dgvST.Rows(e.RowIndex).Cells(0).Value
                varStatusType = dgvST.Rows(e.RowIndex).Cells(1).Value

                If IsNumeric(varID) Then
                    If varID <> 0 Then
                        Dim varSQL As String = "update vwDolfinPaymentStatus set S_Status = '" & varStatusType & "',S_UserModifiedBy = '" & ClsIMS.FullUserName & "',S_UserModifiedTime = getdate()  where S_ID = " & varID
                        ClsIMS.ExecuteString(SSConn, varSQL)
                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSystemSettings, varSQL)
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub FrmSystemSettings_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If SSConn.State = ConnectionState.Open Then
            SSConn.Close()
        End If
        MsgBox("After changing system settings, please close and re-open the application to confirm changes", vbInformation, "Close/Re-Open")
    End Sub

    Private Sub dgvFP_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFP.CellContentClick
        If e.ColumnIndex = 3 Then

        End If
    End Sub
End Class