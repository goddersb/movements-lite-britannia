﻿Public Class FrmConfirmReceiptsOptions
    Private Sub FrmConfirmReceiptsOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtCRMID.Text = ClsCRM.ID
        txtCRMType.Text = ClsCRM.Type
        txtCRMPortfolio.Text = ClsCRM.PortfolioName
        txtCRMCCY.Text = ClsCRM.CCYName
        txtCRMSender.Text = ClsCRM.SenderName
        txtCRMBankName.Text = ClsCRM.BankName
        txtCRMAmt.Text = ClsCRM.Amount
        txtCRMFNotes.Text = ClsCRM.Notes

        If ClsCRM.Type = "MANUAL" Then
            Rb1.Text = "Authorise MANUAL funds"
            Rb2.Text = "Remove MANUAL Funds"
        ElseIf ClsCRM.Type = "SWIFT" Then
            Rb1.Text = "Send SWIFT funds to IMS"
            Rb2.Text = "Manually confirm SWIFT funds"
        End If
        Rb3.Text = "View Audit Trail"
        ExtractAssociatedIconEx()
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer
        Dim SelectedAuthoriseOption As Integer = 0

        If Rb1.Checked Then
            If ClsCRM.Type = "MANUAL" Then
                If ClsCRM.Status = "Pending Payments Team" Then
                    If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorisePaymentsTeam) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseCompliance) Or
                            ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseComplianceLevel2) Then
                        varResponse = MsgBox("Are you sure you want to authorise these incoming funds?", vbQuestion + vbYesNo)
                        If varResponse = vbYes Then
                            SelectedAuthoriseOption = 1
                        End If
                    Else
                        MsgBox("You are not authorised to authorise these incoming funds", vbExclamation, "Cannot authorise incoming funds")
                    End If
                Else
                    MsgBox("Only pending payment team funds can be authorised", vbExclamation, "Cannot authorise incoming funds")
                End If
            ElseIf ClsCRM.Type = "SWIFT" Then
                'If Not IsDBNull(dgvCER.Rows(e.RowIndex).Cells("PF_Code").Value) Then
                'If dgvCER.Rows(e.RowIndex).Cells("status").Value <> ClsIMS.GetStatusType(cStatusCustodianConfirmAcknowledged) Then
                'If ClsIMS.ValidAccountFound(dgvCER.Rows(e.RowIndex).Cells("PF_Code").Value, dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value) Then
                'Dim varResponse As Integer = MsgBox("Are you sure you want to assign " & dgvCER.Rows(e.RowIndex).Cells("Amount").Value & " " & dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value & " from " & dgvCER.Rows(e.RowIndex).Cells("BeneficiaryName").Value & " to this portfolio?", vbQuestion + vbYesNo, "Confirm external receipt")
                'If varResponse = vbYes Then
                'ClsIMS.ConfirmReceipt(dgvCER.Rows(e.RowIndex).Cells("SR_ID").Value, dgvCER.Rows(e.RowIndex).Cells("BeneficiaryName").Value, dgvCER.Rows(e.RowIndex).Cells("PF_Code").Value, dgvCER.Rows(e.RowIndex).Cells("TradeDate").Value,
                'dgvCER.Rows(e.RowIndex).Cells("SettleDate").Value, dgvCER.Rows(e.RowIndex).Cells("Amount").Value, dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value)
                '
                'End If
                '      Else
                'MsgBox("Please confirm this portfolio has a Lloyds " & dgvCER.Rows(e.RowIndex).Cells("AmountCCY").Value & " account available in IMS for this money to be assigned to", vbExclamation)
                '     End If
                'Else
                ''   MsgBox("This payment has already been assigned", vbCritical)
                'End If
                'Else
                '   MsgBox("Please assign a portfolio to this payment to continue", vbExclamation)
                'End If
                SelectedAuthoriseOption = 3
            End If
        ElseIf Rb2.Checked Then
            If ClsCRM.Type = "MANUAL" Then
                If ClsCRM.Status <> "Confirm Acknowledged" Then
                    If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgt) Or
                            ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgtLevel2) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseCompliance) Or
                            ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseComplianceLevel2) Or ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorisePaymentsTeam) Then
                        varResponse = MsgBox("Are you sure you want to remove these incoming funds?", vbQuestion + vbYesNo)
                        If varResponse = vbYes Then
                            SelectedAuthoriseOption = 2
                        End If
                    Else
                        MsgBox("You are not authorised to remove these incoming funds", vbExclamation, "Cannot remove incoming funds")
                    End If
                Else
                    MsgBox("Confirm Acknowledged funds cannot be removed", vbExclamation, "Cannot remove incoming funds")
                End If
            ElseIf ClsCRM.Type = "SWIFT" Then
                If ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise) Then
                    varResponse = MsgBox("Are you sure you want to apply this money manually into IMS?", vbQuestion + vbYesNo)
                    If varResponse = vbYes Then
                        SelectedAuthoriseOption = 4
                    End If
                Else
                    MsgBox("You are not authorised to authorise moving these incoming funds manually", vbExclamation, "Cannot authorise manual incoming funds")
                End If
            End If
        ElseIf Rb3.Checked Then
            ClsGV.GridOption = 4
            ClsGV.ID = ClsCRM.ID
            Dim FrmAuditView As New FrmGridView
            FrmAuditView.ShowDialog()
        End If

        If SelectedAuthoriseOption <> 0 Then
            ClsIMS.ActionMovementCR(SelectedAuthoriseOption, ClsCRM.ID, ClsIMS.FullUserName)
        End If

        If varResponse = vbYes Then
            Me.Close()
        End If
    End Sub

    Public Sub ExtractAssociatedIconEx()
        Try
            CRMFFiles.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If IO.Directory.Exists(ClsCRM.FilePath) Then

                CRMFFiles.SmallImageList = ImageFileList
                CRMFFiles.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New IO.DirectoryInfo(ClsCRM.FilePath)

                Dim item As ListViewItem
                CRMFFiles.BeginUpdate()
                Dim file As IO.FileInfo
                For Each file In dir.GetFiles()
                    ' Set a default icon for the file.
                    Dim iconForFile As Icon = SystemIcons.WinLogo
                    item = New ListViewItem(file.Name, 1)

                    ' Check to see if the image collection contains an image
                    ' for this extension, using the extension as a key.
                    If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                        ' If not, add the image to the image list.
                        'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                        ImageFileList.Images.Add(file.Extension, iconForFile)
                    End If
                    item.ImageKey = file.Extension
                    CRMFFiles.Items.Add(item)
                Next file
                CRMFFiles.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx - Payment Request")
        End Try
    End Sub

    Private Sub CRMFFiles_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles CRMFFiles.MouseDoubleClick
        If CRMFFiles.SelectedItems.Count > 0 Then
            ShellExecute(ClsCRM.FilePath & CRMFFiles.SelectedItems(0).Text)
        End If
    End Sub
End Class