﻿Public Class FrmCorpAction
    Dim CAConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataRestrictionList As New SqlClient.SqlDataAdapter
    Dim CmdSwift As DataGridViewButtonColumn
    Dim cboRestrictionListColumn As DataGridViewComboBoxColumn
    Dim ds As New DataSet
    Dim dgvViewVoluntary As DataView, dgvViewMandatory As DataView, dgvViewPayment As DataView

    Private Sub LoadGridCA(ByVal varFirstLoad As Boolean, Optional ByVal varFilter As String = "")
        Dim varSQL As String
        Dim varFilterStr As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsLinks "

            varFilterStr = ""
            If varFilter <> "" Then
                varFilterStr = varFilter & " and [M/V] = 'Voluntary' "
            Else
                varFilterStr = "where [M/V] = 'Voluntary' and (NOT (ISNULL(S_ID, 0) IN (13, 31))) "
            End If

            If Not ChkArchive.Checked Then
                varFilterStr = varFilterStr & " and [S_ID] <> 28 "
            End If

            dgvCAVoluntary.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsVoluntary")
            dgvCAVoluntary.AutoGenerateColumns = True

            dgvViewVoluntary = New DataView(ds.Tables("vwDolfinPaymentCorporateActionsVoluntary"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "DeadlineDate Asc", DataViewRowState.CurrentRows)
            dgvCAVoluntary.DataSource = dgvViewVoluntary

            If varFirstLoad Then
                Dim CmdSwift As New DataGridViewButtonColumn()
                CmdSwift.HeaderText = "View Swift"
                CmdSwift.Text = "View Swift"
                CmdSwift.Name = "CmdSwift"
                CmdSwift.UseColumnTextForButtonValue = True
                dgvCAVoluntary.Columns.Insert(0, CmdSwift)
            End If

            FormatDGV(dgvCAVoluntary, 1)
            'ColourChangeCells(dgvCAVoluntary)

            dgvCAVoluntary.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCAVoluntary.Refresh()

            '--------------------------------------------------------------------------------------------------------------------------------------------
            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsLinks "

            varFilterStr = ""
            If varFilter <> "" Then
                varFilterStr = varFilterStr & varFilter & " and [M/V] = 'Mandatory' "
            Else
                varFilterStr = "where [M/V] = 'Mandatory' and (NOT (ISNULL(S_ID, 0) IN (13, 31))) "
            End If

            If Not ChkArchive.Checked Then
                varFilterStr = varFilterStr & " and [S_ID] <> 28 "
            End If
            'varSQL = varSQL & " Order by isnull(ExDate,'2050-01-01'), isnull(EarliestPayDate,'2050-01-01'), SR_FileDateTime desc"


            dgvCAMandatory.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsMandatory")
            dgvCAMandatory.AutoGenerateColumns = True

            dgvViewMandatory = New DataView(ds.Tables("vwDolfinPaymentCorporateActionsMandatory"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "ExDate Asc,EarliestPayDate Asc,SR_FileDateTime desc", DataViewRowState.CurrentRows)
            dgvCAMandatory.DataSource = dgvViewMandatory

            If varFirstLoad Then
                Dim CmdSwift As New DataGridViewButtonColumn()
                CmdSwift.HeaderText = "View Swift"
                CmdSwift.Text = "View Swift"
                CmdSwift.Name = "CmdSwift"
                CmdSwift.UseColumnTextForButtonValue = True
                dgvCAMandatory.Columns.Insert(0, CmdSwift)
            End If

            FormatDGV(dgvCAMandatory, 2)

            dgvCAMandatory.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCAMandatory.Refresh()

            '--------------------------------------------------------------------------------------------------------------------------------------------
            varSQL = "SELECT top 1000 * FROM vwDolfinPaymentCorporateActionsPaymentsLinks "

            varFilterStr = ""
            If varFilter <> "" Then
                varFilterStr = varFilter
            Else
                varFilterStr = varFilterStr & "where [S_ID] <> 28 and PostingDate1 > CurrentDate"
            End If

            If Not ChkArchive.Checked And varFilter <> "" Then
                varFilterStr = varFilterStr & " and [S_ID] <> 28 "
            End If

            'varSQL = varSQL & " Order by isnull(DeadlineDate,'2050-01-01'), isnull(EarliestPayDate,'2050-01-01'), SRCA_FileDateTime desc"


            dgvCAPayment.DataSource = Nothing
            CAConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL & varFilterStr, CAConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCorporateActionsPaymentsLinks")
            dgvCAPayment.AutoGenerateColumns = True

            'dgvViewPayment = New DataView(ds.Tables("vwDolfinPaymentCorporateActionsPaymentsLinks"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "", DataViewRowState.CurrentRows)
            dgvViewPayment = New DataView(ds.Tables("vwDolfinPaymentCorporateActionsPaymentsLinks"), "", "", DataViewRowState.CurrentRows)
            dgvCAPayment.DataSource = dgvViewPayment

            If varFirstLoad Then
                Dim CmdSwift As New DataGridViewButtonColumn()
                CmdSwift.HeaderText = "View Swift"
                CmdSwift.Text = "View Swift"
                CmdSwift.Name = "CmdSwift"
                CmdSwift.UseColumnTextForButtonValue = True
                dgvCAPayment.Columns.Insert(0, CmdSwift)
            End If

            FormatDGV(dgvCAPayment, 3)

            dgvCAPayment.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvCAPayment.Refresh()

            '--------------------------------------------------------------------------------------------------------------------------------------------
            LoadBlackListGrid()


            Cursor = Cursors.Default
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCorpAction, Err.Description & " - Problem with viewing corporate actions grid")
            Cursor = Cursors.Default
            dgvCAVoluntary.DataSource = Nothing
            dgvCAMandatory.DataSource = Nothing
        End Try
    End Sub

    Public Sub LoadBlackListGrid()
        Dim varSQL As String = "select isnull(isin,ticker) as InstrumentTicker, t_name1 as InstrumentName from DolfinOrders.[dbo].[TblRestrictedInstruments] 
                                left join titles on isnull(isin,ticker) collate SQL_Latin1_General_CP1253_CI_AS = case when isin is not null then t_isin else left(t_shortcut1,charindex(' ',t_shortcut1)) end 
                                where TemporaryRestriction = 1"

        dgvCABlacklist.DataSource = Nothing
        CAConn = ClsIMS.GetNewOpenConnection
        dgvdata = New SqlClient.SqlDataAdapter(varSQL, CAConn)
        ds = New DataSet
        dgvdata.Fill(ds, "TblRestrictedInstruments")
        dgvCABlacklist.AutoGenerateColumns = True
        dgvCABlacklist.DataSource = ds.Tables("TblRestrictedInstruments")

        'Dim varColSQL As String = "SELECT left(InstrumentTicker,charindex(' ',InstrumentTicker)) as InstrumentTicker,InstrumentTicker as InstrumentTickerName FROM vwDolfinPaymentCorporateActionsBlacklist"
        Dim varColSQL As String = "Select Case when charindex(' ',InstrumentTicker)=0 and len(InstrumentTicker)=12 then InstrumentTicker else case when left(InstrumentTicker,charindex(' ',InstrumentTicker))<>'' then left(InstrumentTicker,charindex(' ',InstrumentTicker)) else InstrumentTicker end end as InstrumentTicker,InstrumentTicker as InstrumentTickerName 
                                    FROM vwDolfinPaymentCorporateActionsBlacklist 
                                    order by Case when charindex(' ',InstrumentTicker)=0 and len(InstrumentTicker)=12 then InstrumentTicker else case when left(InstrumentTicker,charindex(' ',InstrumentTicker))<>'' then left(InstrumentTicker,charindex(' ',InstrumentTicker)) else InstrumentTicker end end"
        dgvdataRestrictionList = New SqlClient.SqlDataAdapter(varColSQL, CAConn)
        dgvdataRestrictionList.Fill(ds, "vwDolfinPaymentCorporateActionsBlacklist")
        cboRestrictionListColumn = New DataGridViewComboBoxColumn
        cboRestrictionListColumn.HeaderText = "Instrument Ticker"
        cboRestrictionListColumn.DataPropertyName = "InstrumentTicker"
        cboRestrictionListColumn.DataSource = ds.Tables("vwDolfinPaymentCorporateActionsBlacklist")
        cboRestrictionListColumn.ValueMember = ds.Tables("vwDolfinPaymentCorporateActionsBlacklist").Columns(0).ColumnName
        cboRestrictionListColumn.DisplayMember = ds.Tables("vwDolfinPaymentCorporateActionsBlacklist").Columns(1).ColumnName
        cboRestrictionListColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
        cboRestrictionListColumn.FlatStyle = FlatStyle.Flat
        dgvCABlacklist.Columns.Insert(0, cboRestrictionListColumn)
        dgvCABlacklist.Columns(0).Width = 150
        dgvCABlacklist.Columns("InstrumentName").Width = 250

        dgvCABlacklist.Columns("InstrumentTicker").Visible = False

        dgvCABlacklist.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
        dgvCABlacklist.Refresh()
    End Sub

    Public Sub FormatDGV(ByVal dgv As DataGridView, ByVal GridID As Integer)
        dgv.Columns("Status").Width = 120
        dgv.Columns("CA ID").Width = 60
        dgv.Columns("Name").Width = 200
        dgv.Columns("ISIN").Width = 100
        dgv.Columns("CA Event").Width = 140
        dgv.Columns("M/V").Width = 80
        dgv.Columns("Custodian").Width = 80
        dgv.Columns("DeadlineDate").HeaderText = "Custodian Deadline Date"
        dgv.Columns("DeadlineDate").Width = 150
        dgv.Columns("DolfinDeadlineDate").HeaderText = "Dolfin Deadline Date"
        dgv.Columns("DolfinDeadlineDate").Width = 150
        If GridID = 1 Or GridID = 2 Then
            dgv.Columns("Settled Position").Width = 150
            dgv.Columns("Comments1").Width = 250
            dgv.Columns("Comments2").Width = 250
            dgv.Columns("CAC_ID").Visible = False
            dgv.Columns("S_ID").Visible = False
            dgv.Columns("sr_filename").Visible = False
            dgv.Columns("sr_filedatetime").Visible = False
            dgv.Columns("sr_reference").Visible = False
            dgv.Columns("CheckDeadline").Visible = False
            dgv.Columns("CheckExDate").Visible = False
            dgv.Columns("CheckEvent").Visible = False
            dgv.Columns("ChangeEvent").Visible = False
            dgv.Columns("ChangeDeadlineDate").Visible = False
            dgv.Columns("ChangeDolfinDeadlineDate").Visible = False
            dgv.Columns("ChangeEarliestPayDate").Visible = False
            dgv.Columns("ChangeExDate").Visible = False
            dgv.Columns("ChangeRecDate").Visible = False
            dgv.Columns("ChangeSettledPosition").Visible = False
            dgv.Columns("PreviousSRID").Visible = False
            dgv.Columns("Pay ID").Visible = False
            dgv.Columns("T_Code").Visible = False
            If GridID = 2 Then
                dgv.Columns("DeadlineDate").Visible = False
                dgv.Columns("DolfinDeadlineDate").Visible = False
            End If
        Else
            dgv.Columns("Posted Amount").Width = 150
            dgv.Columns("Posted Amount CCY").Width = 150
            dgv.Columns("Taxed Amount").Width = 150
            dgvCAPayment.Columns("CAC_ID").Visible = False
            dgvCAPayment.Columns("S_ID").Visible = False
            dgvCAPayment.Columns("SRCA_filename").Visible = False
            dgvCAPayment.Columns("SRCA_filedatetime").Visible = False
            dgvCAPayment.Columns("SRCA_ID").Visible = False
        End If
    End Sub

    Public Sub ColourChangeCells(ByVal varRow As Integer, ByVal varCol As Integer, ByVal dgv As DataGridView)
        Try
            If varRow = 0 And varCol = 0 And dgv.Rows(varRow).Cells("CheckEvent").Value = 1 Then
                dgv.Rows(varRow).DefaultCellStyle.BackColor = Color.MistyRose
            Else
                dgv.Rows(varRow).DefaultCellStyle.BackColor = Color.Empty
            End If
            If varCol = 0 Then
                If dgv.Rows(varRow).Cells("ChangeEvent").Value = 1 Then
                    dgv.Rows(varRow).Cells("CA Event").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("CA Event").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeDeadlineDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("DeadlineDate").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("DeadlineDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeDolfinDeadlineDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("DolfinDeadlineDate").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("DolfinDeadlineDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeEarliestPayDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("EarliestPayDate").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("EarliestPayDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeExDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("ExDate").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("ExDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeRecDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("RecDate").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("RecDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("ChangeSettledPosition").Value = 1 Then
                    dgv.Rows(varRow).Cells("Settled Position").Style.BackColor = Color.Wheat
                Else
                    dgv.Rows(varRow).Cells("Settled Position").Style.BackColor = Color.Empty
                End If
                '--------------------'
                If dgv.Rows(varRow).Cells("CheckDeadline").Value = 1 Then
                    dgv.Rows(varRow).Cells("DeadlineDate").Style.BackColor = Color.PaleTurquoise
                Else
                    dgv.Rows(varRow).Cells("DeadlineDate").Style.BackColor = Color.Empty
                End If
                If dgv.Rows(varRow).Cells("CheckExDate").Value = 1 Then
                    dgv.Rows(varRow).Cells("ExDate").Style.BackColor = Color.MediumAquamarine
                Else
                    dgv.Rows(varRow).Cells("ExDate").Style.BackColor = Color.Empty
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs)
        Dim varWhereClauseFilter As String = ""

        Cursor = Cursors.WaitCursor
        If cboCAID.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [CA ID] in (" & cboCAID.Text & ")"
        End If

        If cboCAISIN.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and ISIN in ('" & cboCAISIN.Text & "')"
        End If

        If cboCAEvent.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [CA Event] in ('" & cboCAEvent.Text & "')"
        End If

        If cboCACustodian.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [Custodian] in ('" & cboCACustodian.Text & "')"
        End If

        If cboCAStatus.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [Status] in ('" & cboCAStatus.Text & "')"
        End If

        If cboCAName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [Name] in ('" & cboCAName.Text & "')"
        End If

        If cboCAPayID.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and [PAY ID] in (" & cboCAPayID.Text & ")"
        End If

        If cboCADate.Text <> "" Then
            If cboCADate.Text = "Ex Date" Then
                varWhereClauseFilter = varWhereClauseFilter & " and convert(nvarchar(10),[ExDate],120) = convert(nvarchar(10),'" & dtCADate.Value.ToString("yyyy-MM-dd") & "',120)"
            ElseIf cboCADate.Text = "Deadline Date" Then
                varWhereClauseFilter = varWhereClauseFilter & " and convert(nvarchar(10),[DeadlineDate],120) = convert(nvarchar(10),'" & dtCADate.Value.ToString("yyyy-MM-dd") & "',120)"
            ElseIf cboCADate.Text = "Dolfin Deadline Date" Then
                varWhereClauseFilter = varWhereClauseFilter & " and convert(nvarchar(10),[DolfinDeadlineDate],120) = convert(nvarchar(10),'" & dtCADate.Value.ToString("yyyy-MM-dd") & "',120)"
            ElseIf cboCADate.Text = "Record Date" Then
                varWhereClauseFilter = varWhereClauseFilter & " and convert(nvarchar(10),[RecDate],120) = convert(nvarchar(10),'" & dtCADate.Value.ToString("yyyy-MM-dd") & "',120)"
            End If
        End If

        If Strings.Left(varWhereClauseFilter, 4) = " and" Then
            LoadGridCA(False, "where " & Strings.Right(varWhereClauseFilter, Strings.Len(varWhereClauseFilter) - 4))
        Else
            LoadGridCA(False)
        End If
        Cursor = Cursors.Default
    End Sub


    Public Sub PopulateCbo(ByRef cbo As ComboBox, ByVal varSQL As String)
        Dim TempValue As String = ""
        TempValue = cbo.Text
        ClsIMS.PopulateComboboxWithData(cbo, varSQL)
        If TempValue = "" Then
            cbo.SelectedIndex = -1
        Else
            cbo.Text = TempValue
        End If
    End Sub

    Public Sub LoadFilters()
        PopulateCbo(cboCAID, "Select 0,NULL as [CA ID] union Select 1,[CA ID] from vwDolfinPaymentCorporateActionsLinks group by [CA ID] order by [CA ID]")
        PopulateCbo(cboCAISIN, "Select 0,NULL as [ISIN] union Select 1,[ISIN] from vwDolfinPaymentCorporateActionsLinks group by [ISIN] order by [ISIN]")
        PopulateCbo(cboCAEvent, "Select 0,NULL as [CA Event] union Select 1,[CA Event] from vwDolfinPaymentCorporateActionsLinks group by [CA Event] order by [CA Event]")
        PopulateCbo(cboCACustodian, "Select 0,NULL as [Custodian] union Select 1,[Custodian] from vwDolfinPaymentCorporateActionsLinks group by [Custodian] order by [Custodian]")
        PopulateCbo(cboCAStatus, "Select 0,NULL as [Status] union Select 1,[Status] from vwDolfinPaymentCorporateActionsLinks group by [Status] order by [Status]")
        PopulateCbo(cboCAName, "Select 0,NULL as [Name] union Select 1,[Name] from vwDolfinPaymentCorporateActionsLinks group by [Name] order by [Name]")
        PopulateCbo(cboCAPayID, "Select 0,NULL as [PAY ID] union Select 1,[PAY ID] from vwDolfinPaymentCorporateActionsPaymentsLinks group by [PAY ID] order by [PAY ID]")
        cboCADate.Items.Add("")
        cboCADate.Items.Add("Ex Date")
        cboCADate.Items.Add("Deadline Date")
        cboCADate.Items.Add("Dolfin Deadline Date")
        cboCADate.Items.Add("Record Date")
    End Sub

    Private Sub FrmCorpAction_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If CAConn.State = ConnectionState.Open Then
            CAConn.Close()
        End If
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs)
        LoadGridCA(False)
        cboCAID.SelectedIndex = -1
        cboCAISIN.SelectedIndex = -1
        cboCAEvent.SelectedIndex = -1
        cboCACustodian.SelectedIndex = -1
        cboCAStatus.SelectedIndex = -1
        cboCAName.SelectedIndex = -1
        cboCADate.SelectedIndex = -1
    End Sub

    Private Sub dgvCAMandatory_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCAMandatory.CellClick
        If e.RowIndex >= 0 And e.ColumnIndex = 0 Then
            ClsIMS.OpenSwiftFile(dgvCAMandatory.Rows(e.RowIndex).Cells("sr_filename").Value)
        End If
    End Sub

    Private Sub dgvCAVoluntary_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCAVoluntary.CellClick
        If e.RowIndex >= 0 And e.ColumnIndex = 0 Then
            ClsIMS.OpenSwiftFile(dgvCAVoluntary.Rows(e.RowIndex).Cells("sr_filename").Value)
        End If
    End Sub

    Private Sub dgvCAMandatory_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCAMandatory.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvCAMandatory, e.RowIndex)
        End If
        ColourChangeCells(e.RowIndex, e.ColumnIndex, dgvCAMandatory)
    End Sub

    Private Sub dgvCAVoluntary_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCAVoluntary.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvCAVoluntary, e.RowIndex)
        End If
        ColourChangeCells(e.RowIndex, e.ColumnIndex, dgvCAVoluntary)
    End Sub

    Private Sub dgvCAPayment_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCAPayment.CellClick
        If e.RowIndex >= 0 And e.ColumnIndex = 0 Then
            ClsIMS.OpenSwiftFile(dgvCAPayment.Rows(e.RowIndex).Cells("srca_filename").Value)
        End If
    End Sub

    Private Sub dgvCAMandatory_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCAMandatory.RowHeaderMouseDoubleClick
        ClsCA = New ClsCorpAction
        ClsIMS.GetReaderItemIntoCAClass(dgvCAMandatory.Rows(dgvCAMandatory.SelectedRows.Item(0).Index).Cells(2).Value)
        Dim FrmCAAuth = New FrmCorpActionAuthorise
        FrmCAAuth.ShowDialog()
    End Sub

    Private Sub dgvCAPayment_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCAPayment.RowHeaderMouseDoubleClick
        ClsCA = New ClsCorpAction
        ClsIMS.GetReaderItemIntoCAClass(dgvCAPayment.Rows(dgvCAPayment.SelectedRows.Item(0).Index).Cells(2).Value)
        Dim FrmCAAuth = New FrmCorpActionAuthorise
        FrmCAAuth.ShowDialog()
    End Sub

    Private Sub dgvCAVoluntary_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCAVoluntary.RowHeaderMouseDoubleClick
        ClsCA = New ClsCorpAction
        ClsIMS.GetReaderItemIntoCAClass(dgvCAVoluntary.Rows(dgvCAVoluntary.SelectedRows.Item(0).Index).Cells(2).Value)
        Dim FrmCAAuth = New FrmCorpActionAuthorise
        FrmCAAuth.ShowDialog()
    End Sub

    Private Sub dgvCAPayment_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCAPayment.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvCAPayment, e.RowIndex)
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs)
        Dim varFullFilePath As String = ""
        Cursor = Cursors.WaitCursor
        If TabCA.SelectedIndex = 0 Then
            ExportGridToExcelFromForm(dgvCAVoluntary, 0)
        ElseIf TabCA.SelectedIndex = 1 Then
            ExportGridToExcelFromForm(dgvCAMandatory, 0)
        ElseIf TabCA.SelectedIndex = 2 Then
            ExportGridToExcelFromForm(dgvCAPayment, 0)
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub FrmCorpAction_Load(sender As Object, e As EventArgs) Handles Me.Load
        ModRMS.DoubleBuffered(dgvCAVoluntary, True)
        ModRMS.DoubleBuffered(dgvCAMandatory, True)
        ModRMS.DoubleBuffered(dgvCAPayment, True)
        ModRMS.DoubleBuffered(dgvCABlacklist, True)
        LoadFilters()
        LoadGridCA(True)
    End Sub


    Private Sub dgvCABlacklist_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvCABlacklist.UserDeletingRow
        Dim varResponse As Integer = MsgBox("Are you sure you want to delete this instrument from the blacklist?", vbQuestion + vbYesNo, "Are you sure?")
        If varResponse = vbYes Then
            ClsIMS.RetrictionListRemove(e.Row.Cells(0).Value)
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvCABlacklist_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCABlacklist.RowLeave
        If dgvCABlacklist.EndEdit Then
            If Not IsDBNull(dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value) Then
                If dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value <> "" Then
                    ClsIMS.RetrictionListAdd(dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value)
                    dgvCABlacklist.Rows(e.RowIndex).Cells("InstrumentName").Value = ClsIMS.GetSQLItem("Select top 1 t_name1 from titles where (left(t_shortcut1,charindex(' ',t_shortcut1)) = '" & dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value & "' or t_shortcut1 = '" & dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value & "' or t_isin = '" & dgvCABlacklist.Rows(e.RowIndex).Cells(0).Value & "')")
                End If
            End If
        End If
    End Sub
End Class
