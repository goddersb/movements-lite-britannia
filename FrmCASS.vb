﻿Public Class FrmCASS

    Dim TransConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataCCY As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim cboCCYColumn As New DataGridViewComboBoxColumn
    Dim dgvView As DataView

    Private Sub CASSLoadGrid(ByVal dgv As DataGridView, ByVal varOption As Integer)
        Dim varSQL As String

        Try
            Cursor = Cursors.WaitCursor
            varSQL = "exec DolfinPaymentCASSBankAccountsRefresh " & varOption

            dgv.DataSource = Nothing
            TransConn = ClsIMS.GetNewOpenConnection
            dgvdata = New SqlClient.SqlDataAdapter(varSQL, TransConn)
            'ds = New DataSet

            dgv.AutoGenerateColumns = True
            If varOption = 0 Then
                dgvdata.Fill(ds, "DolfinPaymentCASSBankAccounts")
                dgv.DataSource = ds.Tables("DolfinPaymentCASSBankAccounts")

                'dgv.Columns(0).ReadOnly = True
                'dgv.Columns(1).ReadOnly = True
                'dgv.Columns(2).ReadOnly = True
                'dgv.Columns(3).ReadOnly = True
                'dgv.Columns(4).ReadOnly = True

                dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'Client Account' as Category Union Select 'Firm Account' as Category Union Select 'Private Bank' as Catgory Union Select 'Dummy Account' as Category", TransConn)
                dgvdataCCY.Fill(ds, "Category")
                cboCCYColumn = New DataGridViewComboBoxColumn
                cboCCYColumn.HeaderText = "Category"
                cboCCYColumn.DataPropertyName = "Category"
                cboCCYColumn.DataSource = ds.Tables("Category")
                cboCCYColumn.ValueMember = ds.Tables("Category").Columns(0).ColumnName
                cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                cboCCYColumn.FlatStyle = FlatStyle.Flat
                dgv.Columns.Insert(0, cboCCYColumn)
                dgv.Columns(0).Width = 120
                dgv.Columns(1).Visible = False
                dgv.Columns(2).Width = 350

                dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'Unassigned' as CASS union Select 'Yes' as CASS union Select 'No' as CASS", TransConn)
                dgvdataCCY.Fill(ds, "CASS")
                cboCCYColumn = New DataGridViewComboBoxColumn
                cboCCYColumn.HeaderText = "CASS"
                cboCCYColumn.DataPropertyName = "CASS"
                cboCCYColumn.DataSource = ds.Tables("CASS")
                cboCCYColumn.ValueMember = ds.Tables("CASS").Columns(0).ColumnName
                cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                cboCCYColumn.FlatStyle = FlatStyle.Flat
                dgv.Columns.Insert(3, cboCCYColumn)
                dgv.Columns(4).Visible = False

                dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'Yes' as BoardOfResolutions union Select 'No' as BoardOfResolutions", TransConn)
                dgvdataCCY.Fill(ds, "BoardOfResolutions")
                cboCCYColumn = New DataGridViewComboBoxColumn
                cboCCYColumn.HeaderText = "Board Of Resolutions"
                cboCCYColumn.DataPropertyName = "BoardOfResolutions"
                cboCCYColumn.DataSource = ds.Tables("BoardOfResolutions")
                cboCCYColumn.ValueMember = ds.Tables("BoardOfResolutions").Columns(0).ColumnName
                cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                cboCCYColumn.FlatStyle = FlatStyle.Flat
                dgv.Columns.Insert(5, cboCCYColumn)
                dgv.Columns(6).Visible = False

                dgvdataCCY = New SqlClient.SqlDataAdapter("Select 'Yes' as Active union Select 'No' as Active", TransConn)
                dgvdataCCY.Fill(ds, "Active")
                cboCCYColumn = New DataGridViewComboBoxColumn
                cboCCYColumn.HeaderText = "Active"
                cboCCYColumn.DataPropertyName = "Active"
                cboCCYColumn.DataSource = ds.Tables("Active")
                cboCCYColumn.ValueMember = ds.Tables("Active").Columns(0).ColumnName
                cboCCYColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                cboCCYColumn.FlatStyle = FlatStyle.Flat
                dgv.Columns.Insert(7, cboCCYColumn)
                dgv.Columns(8).Visible = False
                dgv.Columns(9).Width = 80
                dgv.Columns(10).Width = 100
                dgv.Columns(11).Width = 130
                dgv.Columns(12).Width = 100
                dgv.Columns(17).Width = 130
                dgv.Columns(18).HeaderText = "Activation Date (dd/MM/yyyy)"
                dgv.Columns(18).DefaultCellStyle.Format = "dd/MM/yyyy"
                dgv.Columns(19).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgv.Columns(19).HeaderText = "De-activation Date (dd/MM/yyyy)"
                dgv.Columns(19).DefaultCellStyle.Format = "dd/MM/yyyy"
                dgv.Columns(19).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgv.Columns(20).Width = 400
                dgv.Columns(20).DefaultCellStyle.BackColor = Color.LightYellow

                PopulateComboFilters()
            Else
                dgvdata.Fill(ds, "DolfinPaymentDupAccounts")
                dgv.DataSource = ds.Tables("DolfinPaymentDupAccounts")
                'dgvView = New DataView(ds.Tables("DolfinPaymentDupAccounts"))

                dgv.Columns(0).ReadOnly = True
                dgv.Columns(1).ReadOnly = True
                dgv.Columns(2).ReadOnly = True
                dgv.Columns(3).ReadOnly = True
                dgv.Columns(0).Width = 180
                dgv.Columns(1).Width = 60
                dgv.Columns(2).Width = 350
                dgv.Columns(3).Width = 130
            End If

            dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgv.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgv.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameTransferFees, Err.Description & " - Problem with viewing CASS bank accounts grid")
        End Try
    End Sub

    Private Sub PopulateComboFilters()
        Dim View = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"))
        Dim distinctValues As New DataTable
        distinctValues = View.ToTable(True, "BankAccountName")
        cboAccounts.DataSource = distinctValues
        cboAccounts.ValueMember = "BankAccountName"
        cboAccounts.DisplayMember = "BankAccountName"
        cboAccounts.Text = ""

        View = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"))
        distinctValues = View.ToTable(True, "Category")
        cboCategory.DataSource = distinctValues
        cboCategory.ValueMember = "Category"
        cboCategory.DisplayMember = "Category"
        cboCategory.Text = ""

        View = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"))
        distinctValues = View.ToTable(True, "DataSource")
        cboDatasource.DataSource = distinctValues
        cboDatasource.ValueMember = "DataSource"
        cboDatasource.DisplayMember = "DataSource"
        cboDatasource.Text = ""

        View = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"))
        distinctValues = View.ToTable(True, "CASS")
        cboCASS.DataSource = distinctValues
        cboCASS.ValueMember = "CASS"
        cboCASS.DisplayMember = "CASS"
        cboCASS.Text = ""

        View = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"))
        distinctValues = View.ToTable(True, "Active")
        cboActive.DataSource = distinctValues
        cboActive.ValueMember = "Active"
        cboActive.DisplayMember = "Active"
        cboActive.Text = ""
    End Sub

    Private Sub FrmCASS_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvAcc, True)
        CASSLoadGrid(dgvAcc, 0)
        ModRMS.DoubleBuffered(dgvDups, True)
        CASSLoadGrid(dgvDups, 1)
    End Sub

    Private Sub dgvAcc_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvAcc.CellFormatting
        If e.ColumnIndex = 13 Then
            FormatGridStatus(dgvAcc, e.RowIndex, 13)
        End If
    End Sub

    Private Sub dgvDups_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvDups.CellFormatting
        If e.ColumnIndex = 1 Then
            FormatGridStatus(dgvDups, e.RowIndex)
        End If
    End Sub

    Private Sub dgvAcc_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAcc.RowLeave
        If dgvAcc.IsCurrentRowDirty And dgvAcc.EndEdit Then
            Try
                If Not IsDBNull(dgvAcc.Rows(e.RowIndex).Cells(7).Value) Then
                    Dim varCategory As String = dgvAcc.Rows(e.RowIndex).Cells(0).Value
                    Dim varCASS As String = dgvAcc.Rows(e.RowIndex).Cells(2).Value
                    Dim varBOR As String = dgvAcc.Rows(e.RowIndex).Cells(3).Value
                    Dim varActive As String = dgvAcc.Rows(e.RowIndex).Cells(4).Value
                    Dim varBankCCY As String = dgvAcc.Rows(e.RowIndex).Cells(5).Value
                    Dim varBankSwiftcode As String = dgvAcc.Rows(e.RowIndex).Cells(6).Value
                    Dim varBankAccountNo As String = dgvAcc.Rows(e.RowIndex).Cells(7).Value
                    Dim varActivationDate As Date = IIf(IsDate(dgvAcc.Rows(e.RowIndex).Cells(14).Value), dgvAcc.Rows(e.RowIndex).Cells(14).Value, Nothing)
                    Dim varDeActivationDate As Date = IIf(IsDate(dgvAcc.Rows(e.RowIndex).Cells(15).Value), dgvAcc.Rows(e.RowIndex).Cells(15).Value, Nothing)
                    Dim varComments As String = IIf(IsDBNull(dgvAcc.Rows(e.RowIndex).Cells(16).Value), "", dgvAcc.Rows(e.RowIndex).Cells(16).Value)

                    Dim varSQL As String = "update DolfinPaymentCASSBankAccounts set " &
                    "AccountTypeID = " & IIf(varCategory = "Client Account", 0, IIf(varCategory = "Firm Account", 1, IIf(varCategory = "Private Bank", 2, IIf(varCategory = "Dummy Account", 3, 0)))) &
                    ",CASS = " & IIf(varCASS = "Unassigned", -1, IIf(varCASS = "Yes", 1, 0)) &
                    ",BoardOfResolutions = " & IIf(varBOR = "Yes", 1, 0) &
                    ",Active = " & IIf(varActive = "Yes", 1, 0) &
                    ",ActivationDate = " & IIf(varActivationDate < DateAdd(DateInterval.Year, -100, Now()), "null", "'" & varActivationDate.ToString("yyyy-MM-dd") & "'") &
                    ",DeactivationDate = " & IIf(varDeActivationDate < DateAdd(DateInterval.Year, -100, Now()), "null", "'" & varDeActivationDate.ToString("yyyy-MM-dd") & "'") &
                    ",Comments = '" & varComments & "'" &
                    ",LastModifiedBy = '" & ClsIMS.FullUserName & "',LastModifiedDate = getdate() " &
                    "where BankAccountNo = '" & varBankAccountNo & "' and BankCCY = '" & varBankCCY & "' and BankSwiftCode = '" & varBankSwiftcode & "'"

                    ClsIMS.ExecuteString(TransConn, varSQL)
                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCASSAccounts, varSQL)
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCASSAccounts, Err.Description & " - Problem with updating CASS Accounts")
            End Try
        End If
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varFilterStr As String = ""

        If cboAccounts.Text <> "" Then
            varFilterStr = varFilterStr & " and BankAccountName = '" & cboAccounts.Text & "'"
        End If
        If cboCategory.Text <> "" Then
            varFilterStr = varFilterStr & " and Category = '" & cboCategory.Text & "'"
        End If
        If cboDatasource.Text <> "" Then
            varFilterStr = varFilterStr & " and DataSource = '" & cboDatasource.Text & "'"
        End If
        If cboCASS.Text <> "" Then
            varFilterStr = varFilterStr & " and CASS = '" & cboCASS.Text & "'"
        End If
        If cboActive.Text <> "" Then
            varFilterStr = varFilterStr & " and Active = '" & cboActive.Text & "'"
        End If

        If varFilterStr <> "" Then
            dgvView = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"), Microsoft.VisualBasic.Strings.Right(varFilterStr, Len(varFilterStr) - 5), "", DataViewRowState.CurrentRows)
            dgvAcc.DataSource = dgvView
        Else
            dgvView = New DataView(ds.Tables("DolfinPaymentCASSBankAccounts"), "", "", DataViewRowState.CurrentRows)
            dgvAcc.DataSource = dgvView
        End If
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvAcc, 0)
        Cursor = Cursors.Default
    End Sub

End Class