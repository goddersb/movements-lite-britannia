﻿Public Class ClsEmailExchange
    Private Const cOutlookEmailPath As String = "https://outlook.office365.com/ews/exchange.asmx"
    Private Const cApprovalEmailAddress As String = "payments.im.gm@britannia.com"
    Private Const cApprovalEmailPassword As String = "F.w3F!'-5rVU&+'"
    Private Const cApprovalEmailFirstLine As String = "Payment Authorisation is required for the below. Kindly confirm payment is correct and follow below instructions"
    Private Const cApprovalEmailFirstLineFOP As String = "Please enrich these free of payment bookings in FLOW"

    Private Const cOperationsFilesEmailAddress As String = "OperationsFiles@britannia.com"
    Private Const cOperationsFilesEmailPassword As String = "$clock)it(8Sk"

    Function SendApproveMovementEmailMM(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal CCMe As Boolean) As Boolean
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""
            Dim FinanceMsg As String = ""

            SendApproveMovementEmailMM = True

            If Not ClsPayAuth.PaymentRequestedMMFirmMove Then
                FinanceMsg = "FINANCE TO MAKE THIS INSTRUCTION FOR THE CLIENT (Only reply yes to this mail when instrcution has been completed)"
            End If

            CreateBody = "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40""><head><meta http-equiv=Content-Type content=""text/html; charset=us-ascii""><meta name=Generator content=""Microsoft Word 14 (filtered medium)""><style><!--/* Font Definitions */" &
            "@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;} @font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";}a:link, span.MsoHyperlink	{mso-style-priority:99;	color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority:99;	color:purple;	text-decoration:underline;}" &
            "p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;	mso-margin-bottom-alt:auto;	margin-left:0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}span.EmailStyle18	{mso-style-type:personal;	font-family:""Arial"",""sans-serif"";	color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align:baseline;}span.link	{mso-style-name:link;}" &
            "span.txt	{mso-style-name:txt;}span.EmailStyle21	{mso-style-type:personal-compose;	font-family:""Calibri"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-size:10.0pt;}@page WordSection1	{size:612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v:ext=""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v:ext=""edit""><o:idmap v:ext=""edit"" data=""1"" /></o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" &
            "<p><span style='font-family:""Calibri"",""sans-serif""'>" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" &
            "<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left width=1908 style='width:1471.3pt;border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'><tr>" &
            "<td width=141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Payment details below:<o:p></o:p></span></b></p></td>" &
            "<td width=1767 colspan=8 valign=top style='width:1325.35pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;color:red;font-family:""Arial"",""sans-serif""'>" & FinanceMsg & "<o:p></o:p></span></p></td></tr>" &
            "<tr><td width = 141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>" &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Money Market ID:<o:p></o:p></span></b></p></td>" &
            "<td width = 1767 colspan=8 valign=top style='width:1325.35pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.SelectedPaymentID & "<o:p></o:p></span></p></td></tr>"

            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Type", ClsPayAuth.PaymentType)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Portfolio", ClsPayAuth.Portfolio)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Buy/Sell", IIf(ClsPayAuth.PaymentRequestedMMBuySell = 0, "Buy", "Sell"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Instrument", ClsPayAuth.PaymentRequestedMMInstrument)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Amount", ClsPayAuth.Amount)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "CCY", ClsPayAuth.CCY)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Start Date", ClsPayAuth.PaymentRequestedMMStartDate.ToString("dd/MM/yyyy"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "End Date", IIf(ClsPayAuth.PaymentRequestedMMDurationType = 0, "N/A", ClsPayAuth.PaymentRequestedMMEndDate.ToString("dd/MM/yyyy")))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Send Swift", ClsPayAuth.SendSwift)
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Send IMS", ClsPayAuth.SendIMS)

            CreateBody = CreateBody & "</table><span style ='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal>&nbsp;</p><p class=MsoNormal><u>Email Request<o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf
            CreateBody = CreateBody & "<p>" & Replace(ClsPayAuth.PaymentRequestedEmail, vbCrLf, "</p><p>") & "</p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<u>Instructions<o:p></o:p></u></p><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>"

            If FinanceMsg <> "" Then
                CreateBody = CreateBody & "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;color:red;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>FINANCE &#8211; Only reply yes when payment has been made<o:p></o:p></b></p>" & vbCrLf
            End If

            CreateBody = CreateBody & "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>" & vbCrLf

            CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>Cash Movements</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" & vbCrLf &
            "<p style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span></span><span class=txt><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p>" & vbCrLf &
            "<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href="" http://www.dolfin.com"">" & vbCrLf &
            "<span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span>" & vbCrLf &
            "<span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf &
            "<br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr><tr><td width=507 colspan=2 style='width:379.95pt;padding:0cm 0cm 0cm 0cm'></td><td style='border:none;padding:0cm 0cm 0cm 0cm' width=189><p class='MsoNormal'>&nbsp;</p></td></tr><tr height=0><td width=189 style='border:none'></td><td width=318 style='border:none'></td><td width=189 style='border:none'></td></tr></table>" & vbCrLf &
            "<p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif"";mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></p></div></body></html>"

            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsIMS.getPaymentFilePath(ClsPayAuth.SelectedPaymentID)
                If IO.Directory.Exists(vFilePath) Then
                    If ClsPayAuth.PaymentFilePath <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                            Next
                        End If
                    End If
                End If
            End If

            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.SelectedPaymentID)
            EmailMsg.Subject = "**MONEY MARKETS - AUTHORISATION REQUIRED** - Batch order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY

            'Dim varCutOff As String = ClsIMS.GetCutOff(ClsPayAuth.BatchPaymentTable(0)("P_OrderBrokerCode"), ClsPayAuth.BatchPaymentTable(0)("P_CCYCode"))
            'If varCutOff <> "" Then
            ' EmailMsg.Subject = EmailMsg.Subject & ", CutOff Time: " & varCutOff
            'End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If


            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailMM", ClsPayAuth.SelectedPaymentID)
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.SelectedPaymentID)

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
            MsgBox("There is a problem sending the email via this exchange. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            SendApproveMovementEmailMM = False
        Catch ex As Exception
            If InStr(ex.Message, "The process cannot access the file", vbTextCompare) <> 0 Then
                MsgBox("This email cannot be sent as the attachment is currently open. Please close the attachment then try again", vbCritical, "Close attachment")
            Else
                MsgBox("There is a problem sending the email. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            End If
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailBatchComplaince", ClsPayAuth.SelectedPaymentID)
            SendApproveMovementEmailMM = False
        End Try
    End Function

    Function SendApproveMovementEmailbak(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal varAbovethreshold As Boolean, ByVal CCMe As Boolean) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""

            CreateBody = CreateBody & "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbCrLf & "<meta name=""Generator"" content=""Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf & "o\:* {behavior:url(#default#VML);}" & vbCrLf & "w\:* {behavior:url(#default#VML);}" & vbCrLf & ".shape {behavior:url(#default#VML);}" & vbCrLf & "</style><![endif]--><style><!--" & vbCrLf & "/* Font Definitions */" & vbCrLf & "@font-face" & vbCrLf & vbTab & "{font-family:""Cambria Math"";" & vbCrLf & vbTab & "panose-1:2 4 5 3 5 4 6 3 2 4;}" & vbCrLf & "@font-face" & vbCrLf & vbTab & "{font-family:Calibri;" & vbCrLf & vbTab & "panose-1:2 15 5 2 2 2 4 3 2 4;}" & vbCrLf & "/* Style Definitions */" & vbCrLf &
                "p.MsoNormal, li.MsoNormal, div.MsoNormal" & vbCrLf & vbTab & "{margin:0cm;" & vbCrLf & vbTab & "margin-bottom:.0001pt;" & vbCrLf & vbTab & "font-size:11.0pt;" & vbCrLf & vbTab & "font-family:""Calibri"",sans-serif;" & vbCrLf & vbTab & "mso-fareast-language:EN-US;}" & vbCrLf & "a:link, span.MsoHyperlink" & vbCrLf & vbTab & "{mso-style-priority:99;" & vbCrLf & vbTab & "color:#0563C1;" & vbCrLf & vbTab & "text-decoration:underline;}" & vbCrLf & "a:visited, span.MsoHyperlinkFollowed" & vbCrLf & vbTab & "{mso-style-priority:99;" & vbCrLf & vbTab & "color:#954F72;" & vbCrLf & vbTab & "text-decoration:underline;}" & vbCrLf & "p" & vbCrLf & vbTab & "{mso-style-priority:99;" & vbCrLf & vbTab & "mso-margin-top-alt:auto;" & vbCrLf & vbTab & "margin-right:0cm;" & vbCrLf & vbTab & "mso-margin-bottom-alt:auto;" & vbCrLf & vbTab & "margin-left:0cm;" & vbCrLf & vbTab & "font-size:12.0pt;" & vbCrLf & vbTab & "font-family:""Times New Roman"",serif;}" & vbCrLf & "span.EmailStyle17" & vbCrLf & vbTab & "{mso-style-type:personal-compose;" & vbCrLf & vbTab &
                "font-family:""Calibri"",sans-serif;" & vbCrLf & vbTab & "color:windowtext;}" & vbCrLf & ".MsoChpDefault" & vbCrLf & vbTab & "{mso-style-type:export-only;" & vbCrLf & vbTab & "font-family:""Calibri"",sans-serif;" & vbCrLf & vbTab & "mso-fareast-language:EN-US;}" & vbCrLf & "@page WordSection1" & vbCrLf & vbTab & "{size:612.0pt 792.0pt;" & vbCrLf & vbTab & "margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf & "div.WordSection1" & vbCrLf & vbTab & "{page:WordSection1;}" & vbCrLf & "--></style><!--[if gte mso 9]><xml>" & vbCrLf & "<o:shapedefaults v:ext=""edit"" spidmax=""1026"" />" & vbCrLf & "</xml><![endif]--><!--[if gte mso 9]><xml>" & vbCrLf & "<o:shapelayout v:ext=""edit"">" & vbCrLf & "<o:idmap v:ext=""edit"" data=""1"" />" & vbCrLf & "</o:shapelayout></xml><![endif]-->" & vbCrLf & "</head>" & vbCrLf & "<body lang=""EN-GB"" link=""#0563C1"" vlink=""#954F72"">" & vbCrLf & "<div class=""WordSection1"">" & vbCrLf &
            "<p><img width=""746"" height=""48"" style=""width:7.7708in;height:.5in"" id=""Picture_x0020_1"" src=""cid:image001.jpg@01D28157.8E758510""><b><span style=""font-family:&quot;Calibri&quot;,sans-serif""><o:p></o:p></span></b></p>" & vbCrLf &
            "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" & vbCrLf

            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut)) Or (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)) Or (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) Then
                If ClsPayAuth.BeneficiaryTemplateName = "" Then
                    CreateBody = CreateBody & "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">Template Name: NEW PAYMENT BENEFICIARY</span> <br>" & vbCrLf
                Else
                    CreateBody = CreateBody & "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">Template Name: " & ClsPayAuth.BeneficiaryTemplateName & "</span> <br>" & vbCrLf
                End If
            End If

            CreateBody = CreateBody & "<p><span style=""font-family:&quot;Calibri&quot;,sans-serif"">ID: " & ClsPayAuth.SelectedPaymentID & "</span> <br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Movement Type: " & ClsPayAuth.PaymentType & "</span>" & vbCrLf & "<br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Portfolio:  " & ClsPayAuth.Portfolio & "</span>" & vbCrLf & "<br>" & vbCrLf

            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And ClsPayAuth.BeneficiaryPortfolio <> "" Then
                CreateBody = CreateBody & "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Portfolio Beneficiary: " & ClsPayAuth.BeneficiaryPortfolio & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If

            CreateBody = CreateBody & "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">CCY: " & ClsPayAuth.CCY & "</span> <br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Amount: " & ClsPayAuth.Amount.ToString("#,##0.00") & "</span> <br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Transaction Date: " & ClsPayAuth.TransDate & "</span>" & vbCrLf & "<br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Settlement Date: " & ClsPayAuth.SettleDate & "</span>" & vbCrLf & "<br>" & vbCrLf &
            "<span style=""font-family:&quot;Calibri&quot;,sans-serif"">Order Ref: " & ClsPayAuth.OrderRef & "</span> <br>" & vbCrLf

            If ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPayAuth.OrderAccount <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order Account: " & ClsPayAuth.OrderAccount & "</span>" & vbCrLf & "<br>" & vbCrLf
            ElseIf (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And ClsPayAuth.OrderName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order Account: " & ClsPayAuth.OrderName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta)) And
                    ClsPayAuth.OrderName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order Name: " & ClsPayAuth.OrderName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.OrderSwift <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order Swift: " & ClsPayAuth.OrderSwift & "</span><br>" & vbCrLf
            End If
            If ClsPayAuth.OrderIBAN <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order IBAN: " & ClsPayAuth.OrderIBAN & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.OrderOther <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Order Ref/Other: " & ClsPayAuth.OrderOther & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryRef <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Ref: " & ClsPayAuth.BeneficiaryRef & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If

            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta)) And ClsPayAuth.BeneficiaryAccount <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Account: " & ClsPayAuth.BeneficiaryAccount & "</span>" & vbCrLf & "<br>" & vbCrLf
            ElseIf (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta)) And
                    ClsPayAuth.BeneficiaryName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Account: " & ClsPayAuth.BeneficiaryName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta)) And ClsPayAuth.BeneficiaryName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Name: " & ClsPayAuth.BeneficiaryName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySwift <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Swift: " & ClsPayAuth.BeneficiarySwift & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryIBAN <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary IBAN: " & ClsPayAuth.BeneficiaryIBAN & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If

            If ClsPayAuth.BeneficiarySubBankName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Intermediary Name: " & ClsPayAuth.BeneficiarySubBankName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankSwift <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Intermediary Swift: " & ClsPayAuth.BeneficiarySubBankSwift & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankIBAN <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Intermediary IBAN: " & ClsPayAuth.BeneficiarySubBankIBAN & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankBIK <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Intermediary BIK: " & ClsPayAuth.BeneficiarySubBankBIK & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankINN <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Intermediary INN: " & ClsPayAuth.BeneficiarySubBankINN & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If

            If ClsPayAuth.BeneficiaryBankName <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Bank Name: " & ClsPayAuth.BeneficiaryBankName & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryBankSwift <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Bank Swift: " & ClsPayAuth.BeneficiaryBankSwift & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryBankIBAN <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Beneficiary Bank IBAN: " & ClsPayAuth.BeneficiaryBankIBAN & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If
            If ClsPayAuth.SendSwift <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif"">Send Swift: " & ClsPayAuth.SendSwift & "</span>" & vbCrLf & "<br>" & vbCrLf
            End If

            If ClsPayAuth.Comments <> "" Then
                CreateBody = CreateBody & "<span style="" font-family:&quot;Calibri&quot;,sans-serif;background:yellow;mso-highlight:yellow"">Comments: " & ClsPayAuth.Comments & "</span><o:p></o:p></p>" & vbCrLf
            End If

            'CreateBody = CreateBody & "<p class="" MsoNormal""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            '"<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; COLOR: rgb(33,33,33); LINE-HEIGHT: 22px"">" & vbCrLf &
            ''"<span class="" link email signature_email-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"">Cash Movements</span>" & vbCrLf & "<br>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:12px; MARGIN-BOTTOM: 18px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 16px"">" & vbCrLf & "<img class="" sig-logo"" style="" HEIGHT:40px; WIDTH: 161px"" border=""0"" alt=""Dolfin"" src=""cid:dolfin_reimagine_6d49e680-7c79-42e7-a355-8e71cc10f3df.jpg"" width=""161"" height=""40"">" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 0px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; LINE-HEIGHT: 22px"">" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 8px""></span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "</span><br>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 10px"">T</span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "&nbsp;&#43;44 20 3700 3888</span> </p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf &
            '"<a class="" link signature_website-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"" href=""http://www.dolfin.com"">dolfin.com</a>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf & "<br>" & vbCrLf & "<font color=""#424242""><small>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is" & vbCrLf & " authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf & "<br>" & vbCrLf & "The content of this email and any attachments is confidential, may be privileged, is subject to copyright and should only be read, copied and used by the intended recipient. If you are not the intended recipient please notify us by return email or telephone" & vbCrLf &
            '" and erase all copies and do not disclose the email Or any part of it to any person. We may monitor email traffic data And also the content of emails for security And regulatory compliance purposes.</small></font>" & vbCrLf & "</p>" & vbCrLf & "<p></p>" & vbCrLf & "</body>" & vbCrLf & "</html>" & vbCrLf

            CreateBody = CreateBody & "<p class="" MsoNormal ""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            " < p Class="" MsoNormal ""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you agree payment please reply all And state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<font color="" #424242 ><small>This email and the information it contains is confidential and may also be proprietary or legally privileged. Therefore, if you receive this message in error, please notify the sender and delete it from your system. Additionally, you must not, directly or indirectly, use, disclose, distribute, copy or store this message or any attachment(s) or any part of it if you are not the intended recipient. This message is not intended as an offer, recommendation or solicitation to buy or sell, nor is it an official confirmation of terms, actual or proposed, and no representation or warranty is made that this information is complete or accurate. Any views or opinions expressed do not necessarily represent those of Britannia Global Markets Limited. Britannia Global Markets Limited is authorised and regulated by the Financial Conduct Authority in the United Kingdom: FRN 114159. Britannia Global Markets Limited is registered in England and Wales with registered number 01969442 with its registered office at Level 29, 52 Lime Street, London, EC3M 7AF, United Kingdom. Although we take reasonable precaution to ensure no viruses are present in this email, we cannot accept responsibility for loss or damage arising from the receipt or use of this email or any attachment(s).</small></font></p><p></p></body></html>"

            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            'attempt at adding voting options
            'Dim myProp As New Microsoft.Exchange.WebServices.Data.ExtendedPropertyDefinition(Microsoft.Exchange.WebServices.Data.DefaultExtendedPropertySet.Common, 34080, Microsoft.Exchange.WebServices.Data.MapiPropertyType.Binary)
            'Dim foundItem As Microsoft.Exchange.WebServices.Data.Item = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, New Microsoft.Exchange.WebServices.Data.ItemView(10))(0)
            'EmailMsg = Microsoft.Exchange.WebServices.Data.EmailMessage.Bind(Service, foundItem.Id, New Microsoft.Exchange.WebServices.Data.PropertySet(myProp))
            'Dim bytes As Byte() = DirectCast(EmailMsg(myProp), Byte())

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsIMS.getPaymentFilePath(ClsPayAuth.SelectedPaymentID)
                If IO.Directory.Exists(vFilePath) Then
                    If ClsPayAuth.PaymentFilePath <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                            Next
                        End If
                    End If
                End If
            End If

            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.SelectedPaymentID)
            If varAbovethreshold Then
                EmailMsg.Subject = "**ABOVE THRESHOLD - PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            Else
                EmailMsg.Subject = "**PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            End If

            Dim varCutOff As String = ClsIMS.GetCutOff(ClsPayAuth.OrderBrokerCode, ClsPayAuth.CCYCode)
            If varCutOff <> "" Then
                EmailMsg.Subject = EmailMsg.Subject & ", CutOff Time: " & varCutOff
            End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If

            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailbak", ClsPayAuth.SelectedPaymentID)
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.SelectedPaymentID)

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailbak", ClsPayAuth.SelectedPaymentID)
        End Try
        Return Service
    End Function

    Function SendApproveMovementEmail(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal varAbovethreshold As Boolean, ByVal CCMe As Boolean) As Boolean
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""
            Dim FinanceMsg As String = ""
            Dim varOrderName As String = "", varBeneficiaryName As String = ""

            SendApproveMovementEmail = True
            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)) Then
                If Not ClsPayAuth.SendSwift Then
                    FinanceMsg = "FINANCE TO MAKE THIS PAYMENT (Only reply yes to this mail when payment has been made)"
                End If
            End If

            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescIn)) Then
                If Not ClsPayAuth.SendSwift Then
                    FinanceMsg = "FINANCE TO MAKE THIS TRANSFER from client account to payment services account (Only reply yes to this mail when payment has been made)"
                End If
            End If

            If ClsPayAuth.OrderBrokerCode = 222 Then
                varOrderName = "Britannia Global Markets Ltd - Client Account"
            Else
                varOrderName = ClsPayAuth.OrderName
            End If

            If ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And ClsPayAuth.BeneficiaryBrokerCode = 222 Then
                varBeneficiaryName = "Britannia Global Markets Ltd - Client Account"
            ElseIf ClsPayAuth.BeneficiaryTypeID = 0 And ClsPayAuth.BeneficiarySurname <> "" Then
                varBeneficiaryName = ClsPayAuth.BeneficiaryFirstName & " " & ClsPayAuth.BeneficiaryMiddleName & " " & ClsPayAuth.BeneficiarySurname
            Else
                varBeneficiaryName = ClsPayAuth.BeneficiaryName
            End If

            CreateBody = CreateBody & "<html xmlns:v="" urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv="" Content-Type"" content="" text/html; charset=utf-8"">" & vbCrLf & "<meta name="" Generator"" content="" Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf &
            "o\* {behaviorurl(#default#VML);}w\:* {behavior:url(#default#VML);}.shape {behavior:url(#default#VML);}</style><![endif]--><style><!--" & vbCrLf &
            "/* Font Definitions */@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;}@font-face	{font-family:Tahoma;	panose-1:2 11 6 4 3 5 4 4 2 4;}@font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}" & vbCrLf &
            "@font-face	{font-family:""Segoe UI"";	panose-1:2 11 5 2 4 2 4 2 2 3;}" & vbCrLf &
            "/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}a: link, span.MsoHyperlink	{mso-style-priority: 99;" & vbCrLf &
            "color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority: 99;	color:purple;	text-decoration:underline;}p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;" & vbCrLf &
            "mso-margin-bottom-altauto;	margin-left0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}p.MsoAcetate, li.MsoAcetate, div.MsoAcetate	{mso-style-priority:99;	mso-style-link:""Balloon Text Char"";	margin:0cm;" & vbCrLf &
            "margin-bottom:.0001pt;	font-size:8.0pt;	font-family:""Tahoma"",""sans-serif"";	mso-fareast-language:EN-US;}span.EmailStyle17	{mso-style-type: personal-compose;	font-family: ""Arial"",""sans-serif"";	font-variant:normal!important;" & vbCrLf &
            "color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align: baseline;}span.link	{mso-style-name:link;}span.txt	{mso-style-name:txt;}span.BalloonTextChar	{mso-style-name:""Balloon Text Char"";	mso-style-priority:99;" & vbCrLf &
            "mso-style-link:""Balloon Text"";	font-family:""Tahoma"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-family: ""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}@page WordSection1	{size: 612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf &
            "div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v: ext = ""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v: ext = ""edit""><o:idmap v: ext = ""edit"" data=""1"" />" & vbCrLf &
            "</o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" & vbCrLf

            CreateBody = CreateBody & "<p><span style="" font-family:&quot;Calibri&quot;,sans-serif"">" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><table class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'><tr>" & vbCrLf &
            "<td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf


            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment details below:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border:solid windowtext 1.0pt;border-left:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;color:red;font-family:""Arial"",""sans-serif""'>" & FinanceMsg & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Template Name:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut)) Or (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut)) Or (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) Then
                If ClsPayAuth.BeneficiaryTemplateName = "" Then
                    CreateBody = CreateBody & "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>NEW PAYMENT BENEFICIARY<o:p></o:p></span></p></td></tr>" & vbCrLf
                Else
                    CreateBody = CreateBody & "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryTemplateName & "<o:p></o:p></span></p></td></tr>" & vbCrLf
                End If
            End If
            CreateBody = CreateBody & "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf


            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment ID:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.SelectedPaymentID & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Type:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.PaymentType & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Portfolio:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.Portfolio & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            If (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And ClsPayAuth.BeneficiaryPortfolio <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Portfolio Beneficiary:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryPortfolio & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>CCY:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.CCY & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Amount:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.Amount.ToString("#,##0.00") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Transaction Date:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.TransDate & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Settlement Date:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.SettleDate & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Send Swift:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & IIf(ClsPayAuth.SendSwift, "Yes", "No") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Reference:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.OrderRef & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            If ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPayAuth.OrderAccount <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Order Account:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.OrderAccount & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            ElseIf (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And
                ClsPayAuth.OrderName <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Order Account:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varOrderName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And ClsPayAuth.OrderName <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Order Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varOrderName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.OrderSwift <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Order Swift:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.OrderSwift & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.OrderIBAN <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Order IBAN:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.OrderIBAN & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.OrderOther <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Reference:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.OrderOther & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryRef <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Ref:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryRef & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And ClsPayAuth.BeneficiaryAccount <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Account:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryAccount & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            ElseIf (ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And (ClsPayAuth.BeneficiaryName <> "" Or ClsPayAuth.BeneficiarySurname <> "") Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Account:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varBeneficiaryName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If (ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPayAuth.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther)) And (ClsPayAuth.BeneficiaryName <> "" Or ClsPayAuth.BeneficiarySurname <> "") Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & varBeneficiaryName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySwift <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Swift:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySwift & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryIBAN <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary IBAN:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryIBAN & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            If ClsPayAuth.BeneficiarySubBankName <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Intermediary Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySubBankName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankSwift <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Intermediary Swift:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySubBankSwift & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankIBAN <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Intermediary IBAN:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySubBankIBAN & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankBIK <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Intermediary BIK:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySubBankBIK & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiarySubBankINN <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Intermediary INN:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiarySubBankINN & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            If ClsPayAuth.BeneficiaryBankName <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Bank Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryBankName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryBankSwift <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Bank Swift:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryBankSwift & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If
            If ClsPayAuth.BeneficiaryBankIBAN <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Beneficiary Bank IBAN:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BeneficiaryBankIBAN & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            If ClsPayAuth.Comments <> "" Then
                CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Comments:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
                    "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.Comments & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
                    "<tr><td width= 236 colspan=2 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf
            End If

            CreateBody = CreateBody & "<p class="" MsoNormal ""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            " < p Class="" MsoNormal ""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you agree payment please reply all And state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<font color="" #424242 ><small>This email and the information it contains is confidential and may also be proprietary or legally privileged. Therefore, if you receive this message in error, please notify the sender and delete it from your system. Additionally, you must not, directly or indirectly, use, disclose, distribute, copy or store this message or any attachment(s) or any part of it if you are not the intended recipient. This message is not intended as an offer, recommendation or solicitation to buy or sell, nor is it an official confirmation of terms, actual or proposed, and no representation or warranty is made that this information is complete or accurate. Any views or opinions expressed do not necessarily represent those of Britannia Global Markets Limited. Britannia Global Markets Limited is authorised and regulated by the Financial Conduct Authority in the United Kingdom: FRN 114159. Britannia Global Markets Limited is registered in England and Wales with registered number 01969442 with its registered office at Level 29, 52 Lime Street, London, EC3M 7AF, United Kingdom. Although we take reasonable precaution to ensure no viruses are present in this email, we cannot accept responsibility for loss or damage arising from the receipt or use of this email or any attachment(s).</small></font></p><p></p></body></html>"


            'CreateBody = CreateBody & "<u>Instructions<o:p></o:p></u></p><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf &
            '"<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            '"<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>" & vbCrLf

            'If FinanceMsg <> "" Then
            '    CreateBody = CreateBody & "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;color:red;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>FINANCE &#8211; Only reply yes when payment has been made<o:p></o:p></b></p>" & vbCrLf
            'End If

            'CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            '"<span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>Cash Movements</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" & vbCrLf &
            '"<p style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            '"<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span></span><span class=txt><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p>" & vbCrLf &
            '"<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href="" http://www.dolfin.com"">" & vbCrLf &
            '"<span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span>" & vbCrLf &
            '"<span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf &
            '"<br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span>" & vbCrLf &
            '"<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr><tr><td width=507 colspan=2 style='width:379.95pt;padding:0cm 0cm 0cm 0cm'></td><td style='border:none;padding:0cm 0cm 0cm 0cm' width=189><p class='MsoNormal'>&nbsp;</p></td></tr><tr height=0><td width=189 style='border:none'></td><td width=318 style='border:none'></td><td width=189 style='border:none'></td></tr></table>" & vbCrLf &
            '"<p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif"";mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></p></div></body></html>"


            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            'attempt at adding voting options
            'Dim myProp As New Microsoft.Exchange.WebServices.Data.ExtendedPropertyDefinition(Microsoft.Exchange.WebServices.Data.DefaultExtendedPropertySet.Common, 34080, Microsoft.Exchange.WebServices.Data.MapiPropertyType.Binary)
            'Dim foundItem As Microsoft.Exchange.WebServices.Data.Item = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, New Microsoft.Exchange.WebServices.Data.ItemView(10))(0)
            'EmailMsg = Microsoft.Exchange.WebServices.Data.EmailMessage.Bind(Service, foundItem.Id, New Microsoft.Exchange.WebServices.Data.PropertySet(myProp))
            'Dim bytes As Byte() = DirectCast(EmailMsg(myProp), Byte())

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsPayAuth.PaymentFilePathRemote
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "SendApproveMovementEmail - FLOW - Attempting to attached files to email from " & vFilePath, ClsPayAuth.SelectedPaymentID)
                If IO.Directory.Exists(vFilePath) Then
                    If ClsPayAuth.PaymentFilePathRemote <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                If Len(varFiles(i)) > 260 Then
                                    Dim NewFilenameNoExt As String = IO.Path.GetFileNameWithoutExtension(Replace(varFiles(i), vFilePath, ""))
                                    Dim NewFilenameExt As String = IO.Path.GetExtension(Replace(varFiles(i), vFilePath, ""))
                                    Dim NewFilename As String = Left(NewFilenameNoExt, 260 - (Len(vFilePath) + Len(NewFilenameExt) + 20)) & NewFilenameExt
                                    IO.File.Move(varFiles(i), vFilePath & NewFilename)
                                    EmailMsg.Attachments.AddFileAttachment(vFilePath & NewFilename)
                                Else
                                    EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                                End If
                                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "SendApproveMovementEmail - FLOW - Files attached for email in " & vFilePath, ClsPayAuth.SelectedPaymentID)
                            Next
                        Else
                            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmail - FLOW - No files found in " & vFilePath, ClsPayAuth.SelectedPaymentID)
                        End If
                    Else
                        ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmail - FLOW - Directory in class is empty " & vFilePath, ClsPayAuth.SelectedPaymentID)
                    End If
                Else
                    ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmail - FLOW - No directory found for " & vFilePath, ClsPayAuth.SelectedPaymentID)
                End If
            End If

            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.SelectedPaymentID)
            If varAbovethreshold Then
                EmailMsg.Subject = "**ABOVE THRESHOLD - PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            Else
                EmailMsg.Subject = "**PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            End If

            Dim varCutOff As String = ClsIMS.GetCutOff(ClsPayAuth.OrderBrokerCode, ClsPayAuth.CCYCode)
            If varCutOff <> "" Then
                EmailMsg.Subject = EmailMsg.Subject & ", CutOff Time: " & varCutOff
            End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If


            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmail", ClsPayAuth.SelectedPaymentID)
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.SelectedPaymentID)

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
            MsgBox("There is a problem sending the email via this exchange. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            SendApproveMovementEmail = False
        Catch ex As Exception
            If InStr(ex.Message, "The process cannot access the file", vbTextCompare) <> 0 Then
                MsgBox("This email cannot be sent as the attachment is currently open. Please close the attachment then try again", vbCritical, "Close attachment")
            Else
                MsgBox("There is a problem sending the email. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            End If
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmail", ClsPayAuth.SelectedPaymentID)
            SendApproveMovementEmail = False
        End Try
    End Function

    Function SendApproveMovementEmailBatch(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal varAbovethreshold As Boolean, ByVal CCMe As Boolean) As Boolean
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""
            Dim FinanceMsg As String = ""


            SendApproveMovementEmailBatch = True
            If ClsPayAuth.BatchPaymentTable(0)("P_CCYCode") = 6 Then
                FinanceMsg = "FINANCE TO MAKE THESE PAYMENTS (Only reply yes to this mail when payments have been made)"
            End If

            CreateBody = "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40""><head><meta http-equiv=Content-Type content=""text/html; charset=us-ascii""><meta name=Generator content=""Microsoft Word 14 (filtered medium)""><style><!--/* Font Definitions */" &
            "@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;} @font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";}a:link, span.MsoHyperlink	{mso-style-priority:99;	color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority:99;	color:purple;	text-decoration:underline;}" &
            "p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;	mso-margin-bottom-alt:auto;	margin-left:0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}span.EmailStyle18	{mso-style-type:personal;	font-family:""Arial"",""sans-serif"";	color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align:baseline;}span.link	{mso-style-name:link;}" &
            "span.txt	{mso-style-name:txt;}span.EmailStyle21	{mso-style-type:personal-compose;	font-family:""Calibri"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-size:10.0pt;}@page WordSection1	{size:612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v:ext=""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v:ext=""edit""><o:idmap v:ext=""edit"" data=""1"" /></o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" &
            "<p><span style='font-family:""Calibri"",""sans-serif""'>" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" &
            "<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left width=1908 style='width:1471.3pt;border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'><tr>" &
            "<td width=141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Payment details below:<o:p></o:p></span></b></p></td>" &
            "<td width=1767 colspan=8 valign=top style='width:1325.35pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;color:red;font-family:""Arial"",""sans-serif""'><o:p>" & FinanceMsg & "</o:p></span></p></td></tr>" &
            "<tr><td width = 141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>" &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Payment Batch ID:<o:p></o:p></span></b></p></td>" &
            "<td width = 1767 colspan=8 valign=top style='width:1325.35pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(0)("P_ID") & "<o:p></o:p></span></p></td></tr>"

            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Type", ClsPayAuth.BatchPaymentTable(0)("P_PaymentType"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Portfolio", ClsPayAuth.BatchPaymentTable(0)("P_Portfolio"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "CCY", ClsPayAuth.BatchPaymentTable(0)("P_CCY"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Transaction Date", CDate(ClsPayAuth.BatchPaymentTable(0)("P_TransDate")).ToString("dd/MM/yyyy"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Settlement Date", CDate(ClsPayAuth.BatchPaymentTable(0)("P_SettleDate")).ToString("dd/MM/yyyy"))
            If FinanceMsg = "" Then
                SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Send Swift", "Yes")
            Else
                SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Send Swift", "No")
            End If
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Order Account", ClsPayAuth.BatchPaymentTable(0)("P_OrderAccount"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Order Name", ClsPayAuth.BatchPaymentTable(0)("P_OrderName"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Order Swift", ClsPayAuth.BatchPaymentTable(0)("P_OrderSwift"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Order IBAN", ClsPayAuth.BatchPaymentTable(0)("P_OrderIBAN"))
            SendApproveMovementEmailBatchBeneficiaryRowTitle(False, CreateBody)
            For i = 0 To ClsPayAuth.BatchPaymentTable.Rows.Count - 1
                SendApproveMovementEmailBatchBeneficiaryRow(False, CreateBody, i)
            Next

            CreateBody = CreateBody & "<p class="" MsoNormal ""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            " < p Class="" MsoNormal ""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you agree payment please reply all And state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<font color="" #424242 ><small>This email and the information it contains is confidential and may also be proprietary or legally privileged. Therefore, if you receive this message in error, please notify the sender and delete it from your system. Additionally, you must not, directly or indirectly, use, disclose, distribute, copy or store this message or any attachment(s) or any part of it if you are not the intended recipient. This message is not intended as an offer, recommendation or solicitation to buy or sell, nor is it an official confirmation of terms, actual or proposed, and no representation or warranty is made that this information is complete or accurate. Any views or opinions expressed do not necessarily represent those of Britannia Global Markets Limited. Britannia Global Markets Limited is authorised and regulated by the Financial Conduct Authority in the United Kingdom: FRN 114159. Britannia Global Markets Limited is registered in England and Wales with registered number 01969442 with its registered office at Level 29, 52 Lime Street, London, EC3M 7AF, United Kingdom. Although we take reasonable precaution to ensure no viruses are present in this email, we cannot accept responsibility for loss or damage arising from the receipt or use of this email or any attachment(s).</small></font></p><p></p></body></html>"


            'CreateBody = CreateBody & "</table><p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif""'><br>&nbsp;<br><u>Instructions</u> <u><o:p></o:p></u></span></p><div style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" &
            '"<table cellspacing = 0 cellpadding=0 hspace=0 vspace=0 align=left><tr><td valign=top align=left style='padding-top:0cm;padding-right:9.0pt;padding-bottom:0cm;padding-left:9.0pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" &
            '"<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>"

            'If FinanceMsg <> "" Then
            '    CreateBody = CreateBody & " <p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;color:red;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>FINANCE &#8211; Only reply yes when ALL of the payments have been made<o:p></o:p></b></p>" & vbCrLf
            'End If

            'CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            '"<span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>Cash Movements</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" & vbCrLf &
            '"<p style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            '"<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span></span><span class=txt><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p>" & vbCrLf &
            '"<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href="" http://www.dolfin.com"">" & vbCrLf &
            '"<span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span>" & vbCrLf &
            '"<span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf &
            '"<br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span>" & vbCrLf &
            '"<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr><tr><td width=507 colspan=2 style='width:379.95pt;padding:0cm 0cm 0cm 0cm'></td><td style='border:none;padding:0cm 0cm 0cm 0cm' width=189><p class='MsoNormal'>&nbsp;</p></td></tr><tr height=0><td width=189 style='border:none'></td><td width=318 style='border:none'></td><td width=189 style='border:none'></td></tr></table>" & vbCrLf &
            '"<p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif"";mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></p></div></body></html>"

            'CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>Cash Movements</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" &
            '"<p style ='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href=""%20http:/www.dolfin.com""><span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p>" &
            '"<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span><span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br><br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr></table></div>" &
            '"<p class=MsoNormal>&nbsp;<o:p></o:p></p><p class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif""'><o:p>&nbsp;</o:p></span></p></div>" &
            '"<P style=""FONT-SIZE: 12pt; FONT-FAMILY: ARIAL; TEXT-TRANSFORM: uppercase""><FONT color=#091924>David Harper<BR></FONT><FONT color=#f82f38>Systems Engineer<BR></FONT><FONT color=#7a9099>D +44 20 3700 3888</FONT> | <FONT color=#7a9099>T +44 20 3883 1800</FONT></P><P style=""FONT-SIZE: 12pt; FONT-FAMILY: ARIAL; "

            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsIMS.getPaymentFilePath(ClsPayAuth.BatchPaymentTable(0)("P_ID"))
                If IO.Directory.Exists(vFilePath) Then
                    If ClsPayAuth.PaymentFilePath <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                            Next
                        End If
                    End If
                End If
            End If

            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            If varAbovethreshold Then
                EmailMsg.Subject = "**ABOVE THRESHOLD - BATCH PAYMENT AUTHORISATION REQUIRED** - Batch Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.BatchPaymentTable(0)("P_ID") & ", CCY: " & ClsPayAuth.BatchPaymentTable(0)("P_CCY")
            Else
                EmailMsg.Subject = "**BATCH PAYMENT AUTHORISATION REQUIRED** - Batch Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.BatchPaymentTable(0)("P_ID") & ", CCY: " & ClsPayAuth.BatchPaymentTable(0)("P_CCY")
            End If

            Dim varCutOff As String = ClsIMS.GetCutOff(ClsPayAuth.BatchPaymentTable(0)("P_OrderBrokerCode"), ClsPayAuth.BatchPaymentTable(0)("P_CCYCode"))
            If varCutOff <> "" Then
                EmailMsg.Subject = EmailMsg.Subject & ", CutOff Time: " & varCutOff
            End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If


            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailBatch", ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.BatchPaymentTable(0)("P_ID"))

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
            MsgBox("There is a problem sending the email via this exchange. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            SendApproveMovementEmailBatch = False
        Catch ex As Exception
            If InStr(ex.Message, "The process cannot access the file", vbTextCompare) <> 0 Then
                MsgBox("This email cannot be sent as the attachment is currently open. Please close the attachment then try again", vbCritical, "Close attachment")
            Else
                MsgBox("There is a problem sending the email. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            End If
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailBatch", ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            SendApproveMovementEmailBatch = False
        End Try
    End Function

    Function SendApproveMovementEmailBatchCompliance(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal varAbovethreshold As Boolean, ByVal CCMe As Boolean) As Boolean
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""

            SendApproveMovementEmailBatchCompliance = True

            CreateBody = "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40""><head><meta http-equiv=Content-Type content=""text/html; charset=us-ascii""><meta name=Generator content=""Microsoft Word 14 (filtered medium)""><style><!--/* Font Definitions */" &
            "@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;} @font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";}a:link, span.MsoHyperlink	{mso-style-priority:99;	color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority:99;	color:purple;	text-decoration:underline;}" &
            "p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;	mso-margin-bottom-alt:auto;	margin-left:0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}span.EmailStyle18	{mso-style-type:personal;	font-family:""Arial"",""sans-serif"";	color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align:baseline;}span.link	{mso-style-name:link;}" &
            "span.txt	{mso-style-name:txt;}span.EmailStyle21	{mso-style-type:personal-compose;	font-family:""Calibri"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-size:10.0pt;}@page WordSection1	{size:612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v:ext=""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v:ext=""edit""><o:idmap v:ext=""edit"" data=""1"" /></o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" &
            "<p><span style='font-family:""Calibri"",""sans-serif""'>" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" &
            "<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left width=1908 style='width:1471.3pt;border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'><tr>" &
            "<td width=141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Payment details below:<o:p></o:p></span></b></p></td>" &
            "<td width=1767 colspan=8 valign=top style='width:1325.35pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;color:red;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td></tr>" &
            "<tr><td width = 141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'>" &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Payment Batch ID:<o:p></o:p></span></b></p></td>" &
            "<td width = 1767 colspan=8 valign=top style='width:1325.35pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(0)("P_ID") & "<o:p></o:p></span></p></td></tr>"

            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Type", ClsPayAuth.BatchPaymentTable(0)("P_PaymentType"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Portfolio", ClsPayAuth.BatchPaymentTable(0)("P_Portfolio"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "CCY", ClsPayAuth.BatchPaymentTable(0)("P_CCY"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Transaction Date", CDate(ClsPayAuth.BatchPaymentTable(0)("P_TransDate")).ToString("dd/MM/yyyy"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Settlement Date", CDate(ClsPayAuth.BatchPaymentTable(0)("P_SettleDate")).ToString("dd/MM/yyyy"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Beneficiary Sanction Check", ClsPayAuth.BatchPaymentTable(0)("P_RequestCheckedSanctions"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Payment Beneficiary is not a PEP", ClsPayAuth.BatchPaymentTable(0)("P_RequestCheckedPEP"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Customer Due Diligence Performed", ClsPayAuth.BatchPaymentTable(0)("P_RequestCheckedCDD"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Issues Disclosed/Payment Rationale", ClsPayAuth.BatchPaymentTable(0)("P_RequestIssuesDisclosed"))
            SendApproveMovementEmailBatchOrderRowTitleAndValue(CreateBody, "Link to KYC folder", ClsPayAuth.BatchPaymentTable(0)("P_RequestKYCFolder"))

            SendApproveMovementEmailBatchBeneficiaryRowTitle(True, CreateBody)
            For i = 0 To ClsPayAuth.BatchPaymentTable.Rows.Count - 1
                SendApproveMovementEmailBatchBeneficiaryRow(True, CreateBody, i)
            Next

            CreateBody = CreateBody & "</table><p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif""'><br>&nbsp;<br><u>Instructions</u> <u><o:p></o:p></u></span></p><div style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" &
            "<table cellspacing = 0 cellpadding=0 hspace=0 vspace=0 align=left><tr><td valign=top align=left style='padding-top:0cm;padding-right:9.0pt;padding-bottom:0cm;padding-left:9.0pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>"

            CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>Cash Movements</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" & vbCrLf &
            "<p style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span></span><span class=txt><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p>" & vbCrLf &
            "<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href="" http://www.dolfin.com"">" & vbCrLf &
            "<span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span>" & vbCrLf &
            "<span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf &
            "<br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr><tr><td width=507 colspan=2 style='width:379.95pt;padding:0cm 0cm 0cm 0cm'></td><td style='border:none;padding:0cm 0cm 0cm 0cm' width=189><p class='MsoNormal'>&nbsp;</p></td></tr><tr height=0><td width=189 style='border:none'></td><td width=318 style='border:none'></td><td width=189 style='border:none'></td></tr></table>" & vbCrLf &
            "<p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif"";mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></p></div></body></html>"

            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsIMS.getPaymentFilePath(ClsPayAuth.BatchPaymentTable(0)("P_ID"))
                If IO.Directory.Exists(vFilePath) Then
                    If ClsPayAuth.PaymentFilePath <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                            Next
                        End If
                    End If
                End If
            End If

            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            If varAbovethreshold Then
                EmailMsg.Subject = "**PAYMENTS TEAM - ABOVE THRESHOLD - BATCH PAYMENT AUTHORISATION REQUIRED** - Batch Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.BatchPaymentTable(0)("P_ID") & ", CCY: " & ClsPayAuth.BatchPaymentTable(0)("P_CCY")
            Else
                EmailMsg.Subject = "**PAYMENTS TEAM - BATCH PAYMENT AUTHORISATION REQUIRED** - Batch Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.BatchPaymentTable(0)("P_ID") & ", CCY: " & ClsPayAuth.BatchPaymentTable(0)("P_CCY")
            End If

            Dim varCutOff As String = ClsIMS.GetCutOff(ClsPayAuth.BatchPaymentTable(0)("P_OrderBrokerCode"), ClsPayAuth.BatchPaymentTable(0)("P_CCYCode"))
            If varCutOff <> "" Then
                EmailMsg.Subject = EmailMsg.Subject & ", CutOff Time: " & varCutOff
            End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If

            If ClsIMS.LiveDB() Then
                'EmailMsg.CcRecipients.Add("frances.martin@dolfin.com")
                'EmailMsg.CcRecipients.Add("guile.pang@dolfin.com")
            End If

            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailBatchCompliance", ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.BatchPaymentTable(0)("P_ID"))

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
            MsgBox("There is a problem sending the email via this exchange. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            SendApproveMovementEmailBatchCompliance = False
        Catch ex As Exception
            If InStr(ex.Message, "The process cannot access the file", vbTextCompare) <> 0 Then
                MsgBox("This email cannot be sent as the attachment is currently open. Please close the attachment then try again", vbCritical, "Close attachment")
            Else
                MsgBox("There is a problem sending the email. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            End If
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailBatchComplaince", ClsPayAuth.BatchPaymentTable(0)("P_ID"))
            SendApproveMovementEmailBatchCompliance = False
        End Try
    End Function

    Private Sub SendApproveMovementEmailBatchOrderRowTitleAndValue(ByRef CreateBody As String, ByVal varTitle As String, ByVal varValue As String)
        CreateBody = CreateBody & "<tr> <td width = 141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>" & varTitle & ":<o:p></o:p></span></b></p></td>" &
            "<td width = 1807 colspan=8 valign=top style='width:1325.35pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif""'>" & varValue & "<o:p></o:p></span></p></td></tr>"
    End Sub

    Private Sub SendApproveMovementEmailBatchBeneficiaryRowTitle(ByRef varComplaince As String, ByRef CreateBody As String)
        CreateBody = CreateBody & "<tr><td width=141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Beneficiary Amt<o:p></o:p></span></b></p></td>" &
            "<td width = 189 valign=top style='width:5.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Name<o:p></o:p></span></b></p></td>" &
            "<td width = 142 valign=top style='width:106.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Country<o:p></o:p></span></b></p></td>" &
            "<td width = 85 valign=top style='width:63.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Swift<o:p></o:p></span></b></p></td>" &
            "<td width = 123 valign=top style='width:92.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben IBAN<o:p></o:p></span></b></p></td>" &
            "<td width = 123 valign=top style='width:3.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Sub Bank Swift<o:p></o:p></span></b></p></td>" &
            "<td width = 123 valign=top style='width:3.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Sub Bank IBAN<o:p></o:p></span></b></p></td>"

        If varComplaince Then
            CreateBody = CreateBody & "<td width = 387 valign=top style='width:290.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Bank Name<o:p></o:p></span></b></p></td>" &
            "<td width = 614 valign=top style='width:460.7pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Bank Country<o:p></o:p></span></b></p></td></tr>"
        Else
            CreateBody = CreateBody & "<td width = 387 valign=top style='width:290.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Ben Payment reference<o:p></o:p></span></b></p></td>" &
            "<td width = 614 valign=top style='width:460.7pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Comments<o:p></o:p></span></b></p></td></tr>"
        End If
    End Sub

    Private Sub SendApproveMovementEmailBatchBeneficiaryRow(ByRef varComplaince As String, ByRef CreateBody As String, ByRef varRow As Integer)
        Dim varName As String = ""

        If ClsPayAuth.BatchPaymentTable(varRow)("P_BenTypeID") = 0 Then
            varName = ClsPayAuth.BatchPaymentTable(varRow)("P_BenFirstname") & " " & ClsPayAuth.BatchPaymentTable(varRow)("P_BenMiddlename") & " " & ClsPayAuth.BatchPaymentTable(varRow)("P_BenSurname")
        Else
            varName = ClsPayAuth.BatchPaymentTable(varRow)("P_BenName")
        End If

        CreateBody = CreateBody & "<tr><td width= 141 valign=top style='width:105.95pt;border:solid windowtext 1.0pt;border-top:none;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal align=center style='text-align:center;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & CDbl(ClsPayAuth.BatchPaymentTable(varRow)("P_Amount")).ToString("#,##0.00") & "<o:p></o:p></span></p></td>" &
        "<td width= 189 valign=top style='width:5.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & varName & "<o:p></o:p></span></p></td>" &
        "<td width= 142 valign=top style='width:106.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_BenCountry") & "<o:p></o:p></span></p></td>" &
        "<td width= 85 valign=top style='width:63.8pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_BenSwift") & "<o:p></o:p></span></p></td>" &
        "<td width= 123 valign=top style='width:92.2pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_BenIBAN") & "<o:p></o:p></span></p></td>" &
        "<td width= 123 valign=top style='width:3.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_BenSubBankSwift") & "<o:p></o:p></span></p></td>" &
        "<td width= 123 valign=top style='width:3.0cm;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_BenSubBankIBAN") & "<o:p></o:p></span></p></td>"

        If varComplaince Then
            CreateBody = CreateBody & "<td width= 387 valign=top style='width:290.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_RequestCheckedBankName") & "<o:p></o:p></span></p></td>" &
            "<td width= 614 valign=top style='width:460.7pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_RequestCheckedBankCountry") & "<o:p></o:p></span></p></td></tr>"
        Else
            CreateBody = CreateBody & "<td width= 387 valign=top style='width:290.6pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_OrderOther") & "<o:p></o:p></span></p></td>" &
            "<td width= 614 valign=top style='width:460.7pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 3pt 0cm 3pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:7.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.BatchPaymentTable(varRow)("P_Comments") & "<o:p></o:p></span></p></td></tr>"
        End If

    End Sub

    Function SendApproveMovementEmailCompliance(ByVal varAttachments As Boolean, ByVal LstRecipients As List(Of String), ByVal varBody As String, ByVal CCMe As Boolean) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""

            CreateBody = CreateBody & "<html xmlns:v="" urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv="" Content-Type"" content="" text/html; charset=utf-8"">" & vbCrLf & "<meta name="" Generator"" content="" Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf &
            "o\* {behaviorurl(#default#VML);}w\:* {behavior:url(#default#VML);}.shape {behavior:url(#default#VML);}</style><![endif]--><style><!--" & vbCrLf &
            "/* Font Definitions */@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;}@font-face	{font-family:Tahoma;	panose-1:2 11 6 4 3 5 4 4 2 4;}@font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}" & vbCrLf &
            "@font-face	{font-family:""Segoe UI"";	panose-1:2 11 5 2 4 2 4 2 2 3;}" & vbCrLf &
            "/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}a: link, span.MsoHyperlink	{mso-style-priority: 99;" & vbCrLf &
            "color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority: 99;	color:purple;	text-decoration:underline;}p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;" & vbCrLf &
            "mso-margin-bottom-altauto;	margin-left0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}p.MsoAcetate, li.MsoAcetate, div.MsoAcetate	{mso-style-priority:99;	mso-style-link:""Balloon Text Char"";	margin:0cm;" & vbCrLf &
            "margin-bottom:.0001pt;	font-size:8.0pt;	font-family:""Tahoma"",""sans-serif"";	mso-fareast-language:EN-US;}span.EmailStyle17	{mso-style-type: personal-compose;	font-family: ""Arial"",""sans-serif"";	font-variant:normal!important;" & vbCrLf &
            "color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align: baseline;}span.link	{mso-style-name:link;}span.txt	{mso-style-name:txt;}span.BalloonTextChar	{mso-style-name:""Balloon Text Char"";	mso-style-priority:99;" & vbCrLf &
            "mso-style-link:""Balloon Text"";	font-family:""Tahoma"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-family: ""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}@page WordSection1	{size: 612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf &
            "div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v: ext = ""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v: ext = ""edit""><o:idmap v: ext = ""edit"" data=""1"" />" & vbCrLf &
            "</o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" & vbCrLf

            CreateBody = CreateBody & "<p><span style="" font-family:&quot;Calibri&quot;,sans-serif"">" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><table class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'><tr>" & vbCrLf &
            "<td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Type:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border:solid windowtext 1.0pt;border-left:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.PaymentType & IIf(ClsPayAuth.BeneficiaryRelationship <> "", " - " & ClsPayAuth.BeneficiaryRelationship, "") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Portfolio:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.Portfolio & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Amount:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.Amount.ToString("#,##0.00") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment CCY:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.CCY & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Beneficiary Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & IIf(ClsPayAuth.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort), ClsPayAuth.BeneficiaryPortfolio, IIf(ClsPayAuth.BeneficiaryTypeID = 0, ClsPayAuth.BeneficiaryFirstName & " " & ClsPayAuth.BeneficiaryMiddleName & " " & ClsPayAuth.BeneficiarySurname, ClsPayAuth.BeneficiaryName)) & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Beneficiary Bank Name:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.PaymentRequestedCheckedBankName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Beneficiary Sanction Check:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & IIf(ClsPayAuth.PaymentRequestedCheckedSantions, "Yes", "No") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Payment Beneficiary is not a PEP:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & IIf(ClsPayAuth.PaymentRequestedCheckedPEP, "Yes", "No") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Customer Due Diligence Performed:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & IIf(ClsPayAuth.PaymentRequestedCheckedCDD, "Yes", "No") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Issues Disclosed/Payment Rationale:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsPayAuth.PaymentRequestedIssuesDisclosed & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Link to KYC folder:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & Replace("", " ", "%20") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "</table><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u>Email Request<o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p>" & Replace(varBody, vbCrLf, "</p><p>") & "</p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p class="" MsoNormal ""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<small>This email and the information it contains is confidential and may also be proprietary or legally privileged. Therefore, if you receive this message in error, please notify the sender and delete it from your system. Additionally, you must not, directly or indirectly, use, disclose, distribute, copy or store this message or any attachment(s) or any part of it if you are not the intended recipient. This message is not intended as an offer, recommendation or solicitation to buy or sell, nor is it an official confirmation of terms, actual or proposed, and no representation or warranty is made that this information is complete or accurate. Any views or opinions expressed do not necessarily represent those of Britannia Global Markets Limited. Britannia Global Markets Limited is authorised and regulated by the Financial Conduct Authority in the United Kingdom: FRN 114159. Britannia Global Markets Limited is registered in England and Wales with registered number 01969442 with its registered office at Level 29, 52 Lime Street, London, EC3M 7AF, United Kingdom. Although we take reasonable precaution to ensure no viruses are present in this email, we cannot accept responsibility for loss or damage arising from the receipt or use of this email or any attachment(s).</small></font></p><p></p></body></html>"

            'CreateBody = CreateBody & "<p Class="" MsoNormal""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you agree payment please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you disagree payment please reply all and state 'No'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            '"<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; COLOR: rgb(33,33,33); LINE-HEIGHT: 22px"">" & vbCrLf &
            '"<span class="" link email signature_email-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"">Cash Movements</span>" & vbCrLf & "<br>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:12px; MARGIN-BOTTOM: 18px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 16px"">" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 0px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; LINE-HEIGHT: 22px"">" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 8px""></span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "</span><br>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 10px"">T</span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "&nbsp;&#43;44 20 3700 3888</span> </p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf &
            '"<a class="" link signature_website-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"" href=""http://www.dolfin.com"">dolfin.com</a>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf & "<br>" & vbCrLf & "<font color=""#424242""><small>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London W1J 8HA. Britannia Global Markets Ltd is" & vbCrLf & " authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf & "<br>" & vbCrLf & "The content of this email and any attachments is confidential, may be privileged, is subject to copyright and should only be read, copied and used by the intended recipient. If you are not the intended recipient please notify us by return email or telephone" & vbCrLf &
            '" and erase all copies and do not disclose the email Or any part of it to any person. We may monitor email traffic data And also the content of emails for security And regulatory compliance purposes.</small></font>" & vbCrLf & "</p>" & vbCrLf & "<p></p>" & vbCrLf & "</body>" & vbCrLf & "</html>" & vbCrLf


            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            'attempt at adding voting options
            'Dim myProp As New Microsoft.Exchange.WebServices.Data.ExtendedPropertyDefinition(Microsoft.Exchange.WebServices.Data.DefaultExtendedPropertySet.Common, 34080, Microsoft.Exchange.WebServices.Data.MapiPropertyType.Binary)
            'Dim foundItem As Microsoft.Exchange.WebServices.Data.Item = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, New Microsoft.Exchange.WebServices.Data.ItemView(10))(0)
            'EmailMsg = Microsoft.Exchange.WebServices.Data.EmailMessage.Bind(Service, foundItem.Id, New Microsoft.Exchange.WebServices.Data.PropertySet(myProp))
            'Dim bytes As Byte() = DirectCast(EmailMsg(myProp), Byte())

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            If varAttachments Then
                Dim vFilePath As String = ClsPayAuth.TempPaymentFilePath
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "SendApproveMovementEmailCompliance - FLOW - Attempting to attached files to email from " & vFilePath, ClsPayAuth.SelectedPaymentID)
                If IO.Directory.Exists(vFilePath) Then
                    If vFilePath <> "" Then
                        Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                        If varFiles.Count > 0 Then
                            For i As Integer = 0 To varFiles.Count - 1
                                EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "SendApproveMovementEmailCompliance - FLOW - " & varFiles(i) & " attached from " & vFilePath, ClsPayAuth.SelectedPaymentID)
                            Next
                        Else
                            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmailCompliance - FLOW - No files (filecount) found in " & vFilePath, ClsPayAuth.SelectedPaymentID)
                        End If
                    Else
                        ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmailCompliance - FLOW - File path blank - " & vFilePath, ClsPayAuth.SelectedPaymentID)
                    End If
                Else
                    ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, "SendApproveMovementEmailCompliance - FLOW - No directory found for " & vFilePath, ClsPayAuth.SelectedPaymentID)
                End If
            End If


            Dim varLatestOrderRef As String = ClsIMS.GetRef(ClsPayAuth.SelectedPaymentID)
            If ClsPayAuth.PaymentRequestedType = 8 Then
                EmailMsg.Subject = "**ACCOUNT MGT - PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            Else
                EmailMsg.Subject = "**PAYMENTS TEAM - PAYMENT AUTHORISATION REQUIRED** - Order Ref: " & varLatestOrderRef & ", ID: " & ClsPayAuth.SelectedPaymentID & ", CCY: " & ClsPayAuth.CCY
            End If

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If
            'add ops and alex to payments team emails
            If ClsIMS.LiveDB() Then
                'EmailMsg.CcRecipients.Add("dgahagan@britannia.com")
            End If
            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailCompliance - FLOW", ClsPayAuth.SelectedPaymentID)
            ClsIMS.UpdateSendApproveMovementEmail(ClsPayAuth.SelectedPaymentID)
        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailCompliance", ClsPayAuth.SelectedPaymentID)
        End Try
        Return Service
    End Function


    Function CheckApproveMovementEmail(ByVal SelectedPaymentID As Double, ByVal varSendSwift As Boolean, ByVal varSendIMS As Boolean, ByVal varAbovethreshold As Boolean, ByVal varModifiedTime As Date, ByVal varCompliance As Boolean) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim SearchFilterCollection As New List(Of Microsoft.Exchange.WebServices.Data.SearchFilter)
            SearchFilterCollection.Add(New Microsoft.Exchange.WebServices.Data.SearchFilter.ContainsSubstring(Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, SelectedPaymentID))

            ' Create the search filter.
            Dim searchFilter As Microsoft.Exchange.WebServices.Data.SearchFilter = New Microsoft.Exchange.WebServices.Data.SearchFilter.SearchFilterCollection(Microsoft.Exchange.WebServices.Data.LogicalOperator.Or, SearchFilterCollection.ToArray())


            ' Create a view with a page size of 50.
            Dim view As New Microsoft.Exchange.WebServices.Data.ItemView(1)

            'Identify the Subject and DateTimeReceived properties to return.
            'Indicate that the base property will be the item identifier
            view.PropertySet = (New Microsoft.Exchange.WebServices.Data.PropertySet(Microsoft.Exchange.WebServices.Data.BasePropertySet.IdOnly, Microsoft.Exchange.WebServices.Data.ItemSchema.Subject, Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived))

            ' Order the search results by the DateTimeReceived in descending order.
            view.OrderBy.Add(Microsoft.Exchange.WebServices.Data.ItemSchema.DateTimeReceived, Microsoft.Exchange.WebServices.Data.SortDirection.Descending)

            ' Set the traversal to shallow. (Shallow is the default option; other options are Associated and SoftDeleted.)
            view.Traversal = Microsoft.Exchange.WebServices.Data.ItemTraversal.Shallow

            ' Send the request to search the Inbox and get the results.
            Dim findResults As Microsoft.Exchange.WebServices.Data.FindItemsResults(Of Microsoft.Exchange.WebServices.Data.Item) = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, searchFilter, view)

            ' Process each item.
            For Each item As Microsoft.Exchange.WebServices.Data.Item In findResults.Items
                If TypeOf item Is Microsoft.Exchange.WebServices.Data.EmailMessage Then
                    item.Load()

                    Dim varEmailSender As String = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).Sender.Address.ToString
                    Dim varEmailSentTime As Date = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).DateTimeSent

                    If varEmailSentTime >= varModifiedTime Then
                        If InStr(1, varEmailSender, "@", vbTextCompare) <> 0 Then
                            Dim EmailSenderDomainName As String = Environment.UserDomainName & "\" & Left(varEmailSender, InStr(1, varEmailSender, "@", vbTextCompare) - 1)
                            Dim varMainBody As String = TryCast(item, Microsoft.Exchange.WebServices.Data.EmailMessage).Body.ToString
                            Dim varMainBodyCheckFirstLine As String = Left(varMainBody, InStr(1, varMainBody, "OPS PaymentApproval", vbTextCompare) - 1)
                            Dim Approved As Boolean = False
                            Dim LengthCheckMail As Double = Len(varMainBodyCheckFirstLine) 'varMainBody.IndexOf(cApprovalEmailFirstLine, StringComparison.OrdinalIgnoreCase)

                            If LengthCheckMail > 0 Then
                                If Not varCompliance And Not varAbovethreshold And ClsIMS.CanUser(EmailSenderDomainName, cUserAuthorise) Then
                                    If (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then 'Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("ok", StringComparison.OrdinalIgnoreCase) >= 0) And (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approve", StringComparison.OrdinalIgnoreCase) >= 0 Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approved", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                        ClsIMS.ActionMovement(1, SelectedPaymentID, varSendSwift, varSendIMS, EmailSenderDomainName, "CheckApproveMovementEmail - Authorised transaction")
                                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised transaction " & SelectedPaymentID & " by " & EmailSenderDomainName, SelectedPaymentID)
                                        Approved = True
                                    End If
                                ElseIf varCompliance And (ClsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or ClsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam)) Then
                                    If (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then 'Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("ok", StringComparison.OrdinalIgnoreCase) >= 0) And (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approve", StringComparison.OrdinalIgnoreCase) >= 0 Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approved", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                        ClsIMS.ActionMovementPR(4, SelectedPaymentID, EmailSenderDomainName, "CheckApproveMovementEmail - Authorised transaction (Payments team)")
                                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Authorised transaction (Payments team) " & SelectedPaymentID & " by " & EmailSenderDomainName, SelectedPaymentID)
                                        Approved = True
                                    End If
                                ElseIf Not varCompliance And varAbovethreshold Or (varAbovethreshold And ClsIMS.CanUser(EmailSenderDomainName, cUserAboveThreshold)) Then
                                    If (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("yes", StringComparison.OrdinalIgnoreCase) >= 0) Then 'Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("ok", StringComparison.OrdinalIgnoreCase) >= 0) And (Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approve", StringComparison.OrdinalIgnoreCase) >= 0 Or Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("approved", StringComparison.OrdinalIgnoreCase) >= 0) Then
                                        ClsIMS.ActionMovement(1, SelectedPaymentID, varSendSwift, varSendIMS, EmailSenderDomainName, "CheckApproveMovementEmail - Above threshold")
                                        ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Above Threshold - Authorised transaction " & SelectedPaymentID & " by " & EmailSenderDomainName, SelectedPaymentID)
                                        Approved = True
                                    End If
                                End If

                                If varCompliance And (ClsIMS.CanUser(EmailSenderDomainName, cUserAuthoriseCompliance) Or ClsIMS.CanUser(EmailSenderDomainName, cUserAuthorisePaymentsTeam)) And Not Approved And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("No", StringComparison.OrdinalIgnoreCase) >= 0 Then
                                    ClsIMS.ActionMovementPR(5, SelectedPaymentID, EmailSenderDomainName, "CheckApproveMovementEmail - Payment Team refused transaction")
                                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Payments team refused transaction " & SelectedPaymentID & " by " & EmailSenderDomainName, SelectedPaymentID)
                                ElseIf Not varCompliance And ClsIMS.CanUser(EmailSenderDomainName, cUserAuthorise) And Not Approved And Left(varMainBodyCheckFirstLine, LengthCheckMail).IndexOf("No", StringComparison.OrdinalIgnoreCase) >= 0 Then
                                    ClsIMS.ActionMovement(3, SelectedPaymentID, varSendSwift, varSendIMS, EmailSenderDomainName, "CheckApproveMovementEmail - Refused transaction")
                                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Refused transaction " & SelectedPaymentID & " by " & EmailSenderDomainName, SelectedPaymentID)
                                End If
                            End If
                        End If
                    End If
                End If
            Next

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in CheckApproveMovementEmail", SelectedPaymentID)
        End Try
        Return Service
    End Function

    Public Function SendReceiveDeliverEmail(ByVal varBody As String, ByVal varInstructions As String, varComplaince As Boolean, ByVal varID As Double) As String
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing
        Dim LstGetRecipients As New List(Of String)
        Dim varEmailAddress As String = ""

        Try
            Dim CreateBody As String = ""

            SendReceiveDeliverEmail = True

            Dim cFirstLine As String = ""

            If varComplaince Then
                cFirstLine = cApprovalEmailFirstLine
            Else
                cFirstLine = cApprovalEmailFirstLineFOP
            End If

            CreateBody = "<html xmlns:v=""urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40""><head><meta http-equiv=Content-Type content=""text/html; charset=us-ascii""><meta name=Generator content=""Microsoft Word 14 (filtered medium)""><style><!--/* Font Definitions */" &
            "@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;} @font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";}a:link, span.MsoHyperlink	{mso-style-priority:99;	color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority:99;	color:purple;	text-decoration:underline;}" &
            "p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;	mso-margin-bottom-alt:auto;	margin-left:0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}span.EmailStyle18	{mso-style-type:personal;	font-family:""Arial"",""sans-serif"";	color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align:baseline;}span.link	{mso-style-name:link;}" &
            "span.txt	{mso-style-name:txt;}span.EmailStyle21	{mso-style-type:personal-compose;	font-family:""Calibri"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-size:10.0pt;}@page WordSection1	{size:612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v:ext=""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v:ext=""edit""><o:idmap v:ext=""edit"" data=""1"" /></o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" &
            "<p><span style='font-family:""Calibri"",""sans-serif""'>" & cFirstLine & "<o:p></o:p></span></p>" &
            "<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left width=1908 style='width:1471.3pt;border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'><tr>" &
            "<td width=200 valign=top style='width:5.0cm;border:solid windowtext 1.0pt;background:#D9D9D9;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><span style='font-size:8.0pt;font-family:""Arial"",""sans-serif"";color:#254061'>Free of Payment Details<o:p></o:p></span></b></p></td>" &
            "<td width=1767 colspan=8 valign=top style='width:1325.35pt;border:solid windowtext 1.0pt;border-left:none;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:8.0pt;color:red;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td></tr>"

            CreateBody = CreateBody & varBody

            CreateBody = CreateBody & "</table><p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif""'><br>&nbsp;<br><u>Instructions</u> <u><o:p></o:p></u></span></p><div style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" &
            "<table cellspacing = 0 cellpadding=0 hspace=0 vspace=0 align=left><tr><td valign=top align=left style='padding-top:0cm;padding-right:9.0pt;padding-bottom:0cm;padding-left:9.0pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b>" & varInstructions & "<o:p></o:p></b></p>" &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><b><o:p></o:p></b></p>"

            CreateBody = CreateBody & "<p style='margin-bottom:7.5pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span class=link><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#E53E30'>FLOW</span></span><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#212121'><o:p></o:p></span></p>" & vbCrLf &
            "<p style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:16.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br><span class=txt><span style='color:#BCBEC0'>T</span></span><span class=txt><span style='color:#818285'>&nbsp;+44 20 3700 3888</span></span><o:p></o:p></span></p>" & vbCrLf &
            "<p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><a href="" http://www.dolfin.com"">" & vbCrLf &
            "<span style='color:#E53E30;text-decoration:none'>dolfin.com</span></a><o:p></o:p></span></p><p style='mso-margin-top-alt:7.5pt;margin-right:0cm;margin-bottom:7.5pt;margin-left:0cm;line-height:10.5pt;mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:paragraph;mso-element-anchor-horizontal:column;mso-height-rule:exactly'><span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><br></span>" & vbCrLf &
            "<span style='font-size:7.5pt;font-family:""Avenir LT 35 Light"",""sans-serif"";color:#424242'>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London, England, W1J 8HA. Britannia Global Markets Ltd is authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf &
            "<br>The content Of this email And any attachments is confidential, may be privileged, Is subject To copyright And should only be read, copied And used by the intended recipient. If you are Not the intended recipient please notify us by Return email Or telephone And Erase all copies And Do Not disclose the email Or any part Of it To any person. We may monitor email traffic data And also the content Of emails For security And regulatory compliance purposes.</span>" & vbCrLf &
            "<span style='font-size:10.5pt;font-family:""Avenir LT 35 Light"",""sans-serif""'><o:p></o:p></span></p></td></tr><tr><td width=507 colspan=2 style='width:379.95pt;padding:0cm 0cm 0cm 0cm'></td><td style='border:none;padding:0cm 0cm 0cm 0cm' width=189><p class='MsoNormal'>&nbsp;</p></td></tr><tr height=0><td width=189 style='border:none'></td><td width=318 style='border:none'></td><td width=189 style='border:none'></td></tr></table>" & vbCrLf &
            "<p Class=MsoNormal><span style='font-size:12.0pt;font-family:""Times New Roman"",""serif"";mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></p></div></body></html>"

            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            Dim varBranchCodeStr As String = ClsIMS.GetSQLItem("select BranchCode from vwDolfinPaymentReceiveDeliverGridView where prd_id = " & varID)
            Dim varBranchCode As Double

            If Not IsNumeric(varBranchCodeStr) Then
                varBranchCode = ClsIMS.UserLocationCode
            Else
                varBranchCode = CDbl(varBranchCodeStr)
            End If


            If varComplaince Then
                If varBranchCode = 47 Then
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliancepaymentsmalta')")
                Else
                    LstGetRecipients = ClsIMS.GetMultipleDataToList("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('paymentsteam')")
                    varEmailAddress = ClsIMS.GetSQLItem("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('compliance')")
                    If varEmailAddress <> "" Then
                        EmailMsg.CcRecipients.Add(varEmailAddress)
                    End If
                End If
            Else
                If varBranchCode = 47 Then
                    varEmailAddress = ClsIMS.GetSQLItem("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('middleofficemalta')")
                    If varEmailAddress <> "" Then
                        LstGetRecipients.Add(varEmailAddress)
                    End If
                Else
                    varEmailAddress = ClsIMS.GetSQLItem("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('middleoffice')")
                    If varEmailAddress <> "" Then
                        LstGetRecipients.Add(varEmailAddress)
                    End If
                End If
            End If
            EmailMsg.ToRecipients.Add(LstGetRecipients(0))

            Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
            If MyEmailAddress <> "" Then
                EmailMsg.CcRecipients.Add(MyEmailAddress)
            End If

            varEmailAddress = ClsIMS.GetSQLItem("Select U_EmailAddress from vwDolfinPaymentUsers where isnull(U_EmailAddress,'')<>'' and U_Username in ('paymentapproval')")
            If varEmailAddress <> "" Then
                EmailMsg.CcRecipients.Add(varEmailAddress)
            End If


            Dim vFilePath As String = ClsPayAuth.PaymentFilePath
            If IO.Directory.Exists(vFilePath) Then
                If vFilePath <> "" Then
                    Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                    If varFiles.Count > 0 Then
                        For i As Integer = 0 To varFiles.Count - 1
                            EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                        Next
                    End If
                End If
            End If

            EmailMsg.Subject = "**FOP BOOKINGS IN FLOW** ID:" & varID

            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstGetRecipients(0) & " from SendReceiveDeliverEmail", 0)

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
            MsgBox("There is a problem sending the email via this exchange. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            SendReceiveDeliverEmail = False
        Catch ex As Exception
            If InStr(ex.Message, "The process cannot access the file", vbTextCompare) <> 0 Then
                MsgBox("This email cannot be sent as the attachment is currently open. Please close the attachment then try again", vbCritical, "Close attachment")
            Else
                MsgBox("There is a problem sending the email. Please contact systems support for assistance", vbCritical, "Problem sending mail")
            End If
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendReceiveDeliverEmail", 0)
            SendReceiveDeliverEmail = False
        End Try
    End Function

    Sub CorporateActionsAddAppt()
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing
        Dim Lst As Dictionary(Of String, String)

        Try
            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceFlags = Microsoft.Exchange.WebServices.Data.TraceFlags.None
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cOperationsFilesEmailAddress, cOperationsFilesEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Lst = ClsIMS.GetDataToList("select cac_id,cac_name,cac_reference,cac_relatedreference,cac_accountno,cac_description,cac_swiftcode,cac_filename,cac_indicator1,cac_indicator2,cac_isin,cast(cac_settledamount as money) as cac_settledamount,cac_exdate
                                        from DolfinPaymentCorporateActionsCalendar where cac_runtype = 1 and cac_entereddate is null")

            For i = 0 To Lst.Count / 13
                If Not Lst Is Nothing Then
                    Dim appt As New Microsoft.Exchange.WebServices.Data.Appointment(Service)
                    appt.Subject = "ExDate: " & Lst.Item("cac_description_" & i.ToString).ToString & " - " & Lst.Item("cac_name_" & i.ToString).ToString & " (" & Lst.Item("cac_isin_" & i.ToString).ToString & ")"
                    appt.Body = "Filename: " & Lst.Item("cac_filename_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Swift Code: " & Lst.Item("cac_swiftcode_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Account No: " & Lst.Item("cac_accountno_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Amount: " & Lst.Item("cac_settledamount_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Reference: " & Lst.Item("cac_reference_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Related Reference: " & Lst.Item("cac_relatedreference_" & i.ToString).ToString
                    appt.Start = New DateTime(CDate(Lst.Item("cac_exdate_" & i.ToString)).Year, CDate(Lst.Item("cac_exdate_" & i.ToString)).Month, CDate(Lst.Item("cac_exdate_" & i.ToString)).Day, 9, 0, 0)
                    appt.End = appt.Start.AddMinutes(1)

                    appt.Subject = "test"
                    appt.Start = New DateTime(Now.Year, Now.Month, Now.Day, 9, 0, 0)
                    appt.End = appt.Start.AddMinutes(1)
                    appt.Save()
                    ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "update DolfinPaymentCorporateActionsCalendar set cac_entereddate = getdate(),cac_enteredby = 'SwiftRead' where cac_id = " & Lst.Item("cac_id").ToString)
                End If
            Next

            Lst = ClsIMS.GetDataToList("select cac_id,cac_name,cac_accountno,cac_description,cac_swiftcode,cac_isin,cac_dolfindeadlinedate,cast(cac_settledamount as money) as cac_settledamount
                                        from DolfinPaymentCorporateActionsCalendar where cac_runtype = 2 and cac_entereddate is null")

            For i = 1 To Lst.Count / 8
                If Not Lst Is Nothing Then
                    Dim appt As New Microsoft.Exchange.WebServices.Data.Appointment(Service)
                    appt.Subject = "DEADLINE DAY: " & Lst.Item("cac_description_" & i.ToString).ToString & " - " & Lst.Item("cac_name_" & i.ToString).ToString & " (" & Lst.Item("cac_isin_" & i.ToString).ToString & ")"
                    appt.Body = "Filename: " & Lst.Item("sr_filename_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Swift Code: " & Lst.Item("cac_swiftcode_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Account No: " & Lst.Item("cac_settledamount_" & i.ToString).ToString & vbNewLine & vbNewLine &
                    "Amount: " & Lst.Item("sr_settledamount_" & i.ToString).ToString & vbNewLine & vbNewLine
                    appt.Start = New DateTime(CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Year, CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Month, CDate(Lst.Item("cac_dolfindeadlinedate_" & i.ToString)).Day, 9, 0, 0)
                    appt.End = appt.Start.AddMinutes(1)
                    appt.Save()
                    ClsIMS.ExecuteString(ClsIMS.GetNewOpenConnection, "update DolfinPaymentCorporateActionsCalendar set cac_entereddate = getdate(),cac_enteredby = 'SwiftRead' where cac_id = " & Lst.Item("cac_id").ToString)
                End If
            Next

        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in CorporateActionsAddAppt")
        End Try

    End Sub

    Function SendApproveMovementEmailReceipts(ByVal LstRecipients As List(Of String), ByVal CCMe As Boolean) As Microsoft.Exchange.WebServices.Data.ExchangeService
        Dim Service As Microsoft.Exchange.WebServices.Data.ExchangeService = Nothing

        Try
            Dim CreateBody As String = ""

            CreateBody = CreateBody & "<html xmlns:v="" urn:schemas-microsoft-com:vml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:w=""urn:schemas-microsoft-com:office:word"" xmlns:m=""http://schemas.microsoft.com/office/2004/12/omml"" xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf & "<head>" & vbCrLf & "<meta http-equiv="" Content-Type"" content="" text/html; charset=utf-8"">" & vbCrLf & "<meta name="" Generator"" content="" Microsoft Word 15 (filtered medium)"">" & vbCrLf & "<!--[if !mso]><style>v\:* {behavior:url(#default#VML);}" & vbCrLf &
            "o\* {behaviorurl(#default#VML);}w\:* {behavior:url(#default#VML);}.shape {behavior:url(#default#VML);}</style><![endif]--><style><!--" & vbCrLf &
            "/* Font Definitions */@font-face	{font-family:Calibri;	panose-1:2 15 5 2 2 2 4 3 2 4;}@font-face	{font-family:Tahoma;	panose-1:2 11 6 4 3 5 4 4 2 4;}@font-face	{font-family:""Avenir LT 35 Light"";	panose-1:2 11 3 3 2 0 0 2 0 3;}" & vbCrLf &
            "@font-face	{font-family:""Segoe UI"";	panose-1:2 11 5 2 4 2 4 2 2 3;}" & vbCrLf &
            "/* Style Definitions */p.MsoNormal, li.MsoNormal, div.MsoNormal	{margin:0cm;	margin-bottom:.0001pt;	font-size:11.0pt;	font-family:""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}a: link, span.MsoHyperlink	{mso-style-priority: 99;" & vbCrLf &
            "color:blue;	text-decoration:underline;}a:visited, span.MsoHyperlinkFollowed	{mso-style-priority: 99;	color:purple;	text-decoration:underline;}p	{mso-style-priority:99;	mso-margin-top-alt:auto;	margin-right:0cm;" & vbCrLf &
            "mso-margin-bottom-altauto;	margin-left0cm;	font-size:12.0pt;	font-family:""Times New Roman"",""serif"";}p.MsoAcetate, li.MsoAcetate, div.MsoAcetate	{mso-style-priority:99;	mso-style-link:""Balloon Text Char"";	margin:0cm;" & vbCrLf &
            "margin-bottom:.0001pt;	font-size:8.0pt;	font-family:""Tahoma"",""sans-serif"";	mso-fareast-language:EN-US;}span.EmailStyle17	{mso-style-type: personal-compose;	font-family: ""Arial"",""sans-serif"";	font-variant:normal!important;" & vbCrLf &
            "color:windowtext;	text-transform:none;	text-decoration:none none;	vertical-align: baseline;}span.link	{mso-style-name:link;}span.txt	{mso-style-name:txt;}span.BalloonTextChar	{mso-style-name:""Balloon Text Char"";	mso-style-priority:99;" & vbCrLf &
            "mso-style-link:""Balloon Text"";	font-family:""Tahoma"",""sans-serif"";}.MsoChpDefault	{mso-style-type:export-only;	font-family: ""Calibri"",""sans-serif"";	mso-fareast-language:EN-US;}@page WordSection1	{size: 612.0pt 792.0pt;	margin:72.0pt 72.0pt 72.0pt 72.0pt;}" & vbCrLf &
            "div.WordSection1	{page:WordSection1;}--></style><!--[if gte mso 9]><xml><o:shapedefaults v: ext = ""edit"" spidmax=""1026"" /></xml><![endif]--><!--[if gte mso 9]><xml><o:shapelayout v: ext = ""edit""><o:idmap v: ext = ""edit"" data=""1"" />" & vbCrLf &
            "</o:shapelayout></xml><![endif]--></head><body lang=EN-GB link=blue vlink=purple><div class=WordSection1>" & vbCrLf

            CreateBody = CreateBody & "<p><span style="" font-family:&quot;Calibri&quot;,sans-serif"">" & cApprovalEmailFirstLine & "<o:p></o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><table class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'><tr>" & vbCrLf &
            "<td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Incoming Funds ID:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border:solid windowtext 1.0pt;border-left:none;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.ID & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Swift Code:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.SwiftCode & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Bank Name:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.BankName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Sender Name:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.SenderName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Portfolio:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'>" & vbCrLf &
            "<p Class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.PortfolioName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width=236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Amount:<o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.Amount.ToString("#,##0.00") & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>CCY:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.CCYName & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "<tr><td width= 236 valign=top style='width:176.8pt;border:solid windowtext 1.0pt;border-top:none;background:#F2F2F2;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf

            CreateBody = CreateBody & "<span style='font-size:9.0pt;font-family:""Segoe UI"",""sans-serif"";color:#172B4D;background:#F4F5F7'>Notes:</span><span style='font-size:9.0pt;font-family:""Arial"",""sans-serif""'><o:p></o:p></span></p></td><td width=425 valign=top style='width:500pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:white;padding:0cm 5.4pt 0cm 5.4pt'><p class=MsoNormal style='mso-element:frame;mso-element-frame-hspace:9.0pt;mso-element-wrap:around;mso-element-anchor-vertical:page;mso-element-anchor-horizontal:margin;mso-element-top:50.75pt;mso-height-rule:exactly'>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'>" & ClsCRM.Notes & "<o:p></o:p></span></p></td></tr>" & vbCrLf &
            "</table><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p>" & vbCrLf &
            "<p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal>" & vbCrLf &
            "<span style='font-size:10.0pt;font-family:""Arial"",""sans-serif""'><o:p>&nbsp;</o:p></span></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p><p class=MsoNormal><u><o:p></o:p></u></p><p class=MsoNormal><u><o:p><span style='text-decoration:none'>&nbsp;</span></o:p></u></p>" & vbCrLf

            CreateBody = CreateBody & "<p class="" MsoNormal ""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            " < p Class="" MsoNormal ""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you agree payment please reply all And state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            "<p class="" MsoNormal ""><b>If you disagree payment please reply all and state 'No I do not approve'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            "<font color="" #424242 ><small>This email and the information it contains is confidential and may also be proprietary or legally privileged. Therefore, if you receive this message in error, please notify the sender and delete it from your system. Additionally, you must not, directly or indirectly, use, disclose, distribute, copy or store this message or any attachment(s) or any part of it if you are not the intended recipient. This message is not intended as an offer, recommendation or solicitation to buy or sell, nor is it an official confirmation of terms, actual or proposed, and no representation or warranty is made that this information is complete or accurate. Any views or opinions expressed do not necessarily represent those of Britannia Global Markets Limited. Britannia Global Markets Limited is authorised and regulated by the Financial Conduct Authority in the United Kingdom: FRN 114159. Britannia Global Markets Limited is registered in England and Wales with registered number 01969442 with its registered office at Level 29, 52 Lime Street, London, EC3M 7AF, United Kingdom. Although we take reasonable precaution to ensure no viruses are present in this email, we cannot accept responsibility for loss or damage arising from the receipt or use of this email or any attachment(s).</small></font></p><p></p></body></html>"


            'CreateBody = CreateBody & "<p Class="" MsoNormal""><u>Instructions<o:p></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><u><o:p><span style="" text-decoration:none"">&nbsp;</span></o:p></u></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you agree incoming funds please reply all and state 'Yes'<o:p></o:p></b></p>" & vbCrLf &
            '"<p class="" MsoNormal""><b>If you disagree incoming funds please reply all and state 'No'<o:p></o:p></b></p>" & vbCrLf & "</div>" & vbCrLf &
            '"<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; COLOR: rgb(33,33,33); LINE-HEIGHT: 22px"">" & vbCrLf &
            '"<span class="" link email signature_email-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"">Cash Movements</span>" & vbCrLf & "<br>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:12px; MARGIN-BOTTOM: 18px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 16px"">" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 0px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; LINE-HEIGHT: 22px"">" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 8px""></span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "</span><br>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(188,190,192); DISPLAY: inline; MARGIN-RIGHT: 10px"">T</span>" & vbCrLf & "<span class="" txt office-sep sep"" style="" COLOR:rgb(129,130,133); DISPLAY: inline"">" & vbCrLf & "&nbsp;&#43;44 20 3700 3888</span> </p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf &
            '"<a class="" link signature_website-target sig-hide"" style="" TEXT-DECORATION:none; COLOR: rgb(229,62,48); DISPLAY: inline"" href=""http://www.dolfin.com"">dolfin.com</a>" & vbCrLf & "</p>" & vbCrLf & "<p style="" FONT-SIZE:14px; MARGIN-BOTTOM: 10px; FONT-FAMILY: Avenir LT 35 Light, Avenir, Segoe UI, Verdana, Geneva, sans-serif; MARGIN-TOP: 10px; LINE-HEIGHT: 14px"">" & vbCrLf & "<br>" & vbCrLf & "<font color=""#424242""><small>Britannia Global Markets Ltd is a private limited company registered in England and Wales with registration number 07431519 and whose registered office is at 50 Berkeley Street, London W1J 8HA. Britannia Global Markets Ltd is" & vbCrLf & " authorised and regulated by the Financial Conduct Authority.<br>" & vbCrLf & "<br>" & vbCrLf & "The content of this email and any attachments is confidential, may be privileged, is subject to copyright and should only be read, copied and used by the intended recipient. If you are not the intended recipient please notify us by return email or telephone" & vbCrLf &
            '" and erase all copies and do not disclose the email Or any part of it to any person. We may monitor email traffic data And also the content of emails for security And regulatory compliance purposes.</small></font>" & vbCrLf & "</p>" & vbCrLf & "<p></p>" & vbCrLf & "</body>" & vbCrLf & "</html>" & vbCrLf


            Service = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2010)
            Service.TraceEnabled = True
            Service.UseDefaultCredentials = False
            Service.Credentials = New Microsoft.Exchange.WebServices.Data.WebCredentials(cApprovalEmailAddress, cApprovalEmailPassword)
            Service.Url = New Uri(cOutlookEmailPath)

            Dim EmailMsg As New Microsoft.Exchange.WebServices.Data.EmailMessage(Service)

            'attempt at adding voting options
            'Dim myProp As New Microsoft.Exchange.WebServices.Data.ExtendedPropertyDefinition(Microsoft.Exchange.WebServices.Data.DefaultExtendedPropertySet.Common, 34080, Microsoft.Exchange.WebServices.Data.MapiPropertyType.Binary)
            'Dim foundItem As Microsoft.Exchange.WebServices.Data.Item = Service.FindItems(Microsoft.Exchange.WebServices.Data.WellKnownFolderName.Inbox, New Microsoft.Exchange.WebServices.Data.ItemView(10))(0)
            'EmailMsg = Microsoft.Exchange.WebServices.Data.EmailMessage.Bind(Service, foundItem.Id, New Microsoft.Exchange.WebServices.Data.PropertySet(myProp))
            'Dim bytes As Byte() = DirectCast(EmailMsg(myProp), Byte())

            For varemailitem = 0 To LstRecipients.Count - 1
                EmailMsg.ToRecipients.Add(LstRecipients(varemailitem))
            Next

            Dim vFilePath As String = ClsCRM.FilePath
            If IO.Directory.Exists(vFilePath) Then
                If ClsCRM.FilePath <> "" Then
                    Dim varFiles() As String = IO.Directory.GetFiles(vFilePath)
                    If varFiles.Count > 0 Then
                        For i As Integer = 0 To varFiles.Count - 1
                            EmailMsg.Attachments.AddFileAttachment(varFiles(i))
                        Next
                    End If
                End If
            End If


            EmailMsg.Subject = "**PAYMENTS TEAM - PAYMENT AUTHORISATION REQUIRED** - Incoming Funds - IN:" & ClsCRM.ID & ", CCY: " & ClsCRM.CCYName

            If CCMe Then
                Dim MyEmailAddress As String = ClsIMS.GetMyEmailAddress()
                If MyEmailAddress <> "" Then
                    EmailMsg.CcRecipients.Add(MyEmailAddress)
                End If
            End If

            EmailMsg.Body = New Microsoft.Exchange.WebServices.Data.MessageBody(CreateBody)
            EmailMsg.Send()
            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameEmail, "Emailed " & LstRecipients(0) & " and possibly others for authorisation in SendApproveMovementEmailCompliance", ClsCRM.ID)
        Catch ex As Microsoft.Exchange.WebServices.Data.AutodiscoverLocalException
            'Seems that user does not work on Exchange 2007 SP1 or later?
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameEmail, Err.Description & " - error in SendApproveMovementEmailCompliance", ClsCRM.ID)
        End Try
        Return Service
    End Function


End Class
