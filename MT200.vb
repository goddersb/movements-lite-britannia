﻿Public Class MT200

    Public Property ClientSwiftCode As String
    Public Property OrderSwiftCode As String
    Public Property OrderRef As String
    Public Property SettleDate As String
    Public Property Amount As Double?
    Public Property Ccy As String
    Public Property BenIBAN As String
    Public Property OrderIBAN As String
    Public Property OrderOther As String
    Public Property SwiftUrgent As Boolean
    Public Property Email As String
    Public Property Ftp As Boolean?

End Class
