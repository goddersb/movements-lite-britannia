﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmBatchProcessing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmBatchProcessing))
        Me.lblpaymentTitle = New System.Windows.Forms.Label()
        Me.LblPayType = New System.Windows.Forms.Label()
        Me.CboBPType = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboBPPayType = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GrpBenDetails = New System.Windows.Forms.GroupBox()
        Me.dgvbp = New System.Windows.Forms.DataGridView()
        Me.cmdSave = New System.Windows.Forms.Button()
        Me.TabBatch = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GrpBPOrder = New System.Windows.Forms.GroupBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cboBPFeeType = New System.Windows.Forms.ComboBox()
        Me.txtBPAmountFee = New System.Windows.Forms.TextBox()
        Me.lblPRAmountFee = New System.Windows.Forms.Label()
        Me.cboBPFeeCCY = New System.Windows.Forms.ComboBox()
        Me.lblTemFeeCCY = New System.Windows.Forms.Label()
        Me.txtBPOrderBrokerCode = New System.Windows.Forms.TextBox()
        Me.txtBPOrderBrokerName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBPOrderIBAN = New System.Windows.Forms.TextBox()
        Me.txtBPOrderSwift = New System.Windows.Forms.TextBox()
        Me.lblOrderIBAN = New System.Windows.Forms.Label()
        Me.lblOrderSwift = New System.Windows.Forms.Label()
        Me.lblSettleDate = New System.Windows.Forms.Label()
        Me.dtBPTransDate = New System.Windows.Forms.DateTimePicker()
        Me.lblBPOrderVOCode = New System.Windows.Forms.Label()
        Me.cboBPOrderVOCode = New System.Windows.Forms.ComboBox()
        Me.lblBPBalance = New System.Windows.Forms.Label()
        Me.txtBPOrderBalance = New System.Windows.Forms.TextBox()
        Me.CboBPOrderAccount = New System.Windows.Forms.ComboBox()
        Me.lblOrderAccount = New System.Windows.Forms.Label()
        Me.lblportfoliofee = New System.Windows.Forms.Label()
        Me.lblportfolio = New System.Windows.Forms.Label()
        Me.CboBPPortfolio = New System.Windows.Forms.ComboBox()
        Me.CboBPCCY = New System.Windows.Forms.ComboBox()
        Me.lblCCY = New System.Windows.Forms.Label()
        Me.lblTradeDate = New System.Windows.Forms.Label()
        Me.dtBPSettleDate = New System.Windows.Forms.DateTimePicker()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.LVFiles1 = New System.Windows.Forms.ListView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LVFiles = New System.Windows.Forms.ListView()
        Me.GrpKYC = New System.Windows.Forms.GroupBox()
        Me.cmdBPBrowse = New System.Windows.Forms.Button()
        Me.txtBPFilePath = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ChkBPKYCsanctions = New System.Windows.Forms.CheckBox()
        Me.txtBPKYCIssuesDisclosed = New System.Windows.Forms.TextBox()
        Me.ChkBPKYCPEP = New System.Windows.Forms.CheckBox()
        Me.ChkBPKYCCDD = New System.Windows.Forms.CheckBox()
        Me.BPFolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.ContextMenuStripFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripFiles1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox2.SuspendLayout()
        Me.GrpBenDetails.SuspendLayout()
        CType(Me.dgvbp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabBatch.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GrpBPOrder.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GrpKYC.SuspendLayout()
        Me.ContextMenuStripFiles.SuspendLayout()
        Me.ContextMenuStripFiles1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblpaymentTitle
        '
        Me.lblpaymentTitle.AutoSize = True
        Me.lblpaymentTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpaymentTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblpaymentTitle.Location = New System.Drawing.Point(12, 9)
        Me.lblpaymentTitle.Name = "lblpaymentTitle"
        Me.lblpaymentTitle.Size = New System.Drawing.Size(172, 24)
        Me.lblpaymentTitle.TabIndex = 8
        Me.lblpaymentTitle.Text = "Batch Processing"
        '
        'LblPayType
        '
        Me.LblPayType.AutoSize = True
        Me.LblPayType.Location = New System.Drawing.Point(12, 22)
        Me.LblPayType.Name = "LblPayType"
        Me.LblPayType.Size = New System.Drawing.Size(81, 13)
        Me.LblPayType.TabIndex = 59
        Me.LblPayType.Text = "Batch Request:"
        '
        'CboBPType
        '
        Me.CboBPType.FormattingEnabled = True
        Me.CboBPType.Location = New System.Drawing.Point(99, 19)
        Me.CboBPType.Name = "CboBPType"
        Me.CboBPType.Size = New System.Drawing.Size(312, 21)
        Me.CboBPType.TabIndex = 53
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboBPPayType)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.CboBPType)
        Me.GroupBox2.Controls.Add(Me.LblPayType)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 45)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1608, 54)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Request Type"
        '
        'cboBPPayType
        '
        Me.cboBPPayType.BackColor = System.Drawing.Color.White
        Me.cboBPPayType.FormattingEnabled = True
        Me.cboBPPayType.Location = New System.Drawing.Point(483, 19)
        Me.cboBPPayType.Name = "cboBPPayType"
        Me.cboBPPayType.Size = New System.Drawing.Size(315, 21)
        Me.cboBPPayType.TabIndex = 130
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(422, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(55, 13)
        Me.Label14.TabIndex = 131
        Me.Label14.Text = "Pay Type:"
        '
        'GrpBenDetails
        '
        Me.GrpBenDetails.Controls.Add(Me.dgvbp)
        Me.GrpBenDetails.Location = New System.Drawing.Point(16, 354)
        Me.GrpBenDetails.Name = "GrpBenDetails"
        Me.GrpBenDetails.Size = New System.Drawing.Size(1608, 419)
        Me.GrpBenDetails.TabIndex = 10
        Me.GrpBenDetails.TabStop = False
        Me.GrpBenDetails.Text = "Beneficiary Details"
        '
        'dgvbp
        '
        Me.dgvbp.AllowUserToResizeRows = False
        Me.dgvbp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvbp.Location = New System.Drawing.Point(6, 19)
        Me.dgvbp.MultiSelect = False
        Me.dgvbp.Name = "dgvbp"
        Me.dgvbp.ShowCellErrors = False
        Me.dgvbp.ShowRowErrors = False
        Me.dgvbp.Size = New System.Drawing.Size(1596, 394)
        Me.dgvbp.TabIndex = 101
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(1391, 779)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(233, 29)
        Me.cmdSave.TabIndex = 11
        Me.cmdSave.Text = "Save Payments"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'TabBatch
        '
        Me.TabBatch.Controls.Add(Me.TabPage1)
        Me.TabBatch.Controls.Add(Me.TabPage2)
        Me.TabBatch.Location = New System.Drawing.Point(16, 105)
        Me.TabBatch.Name = "TabBatch"
        Me.TabBatch.SelectedIndex = 0
        Me.TabBatch.Size = New System.Drawing.Size(1608, 243)
        Me.TabBatch.TabIndex = 12
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage1.Controls.Add(Me.GrpBPOrder)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1600, 217)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabOrder"
        '
        'GrpBPOrder
        '
        Me.GrpBPOrder.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GrpBPOrder.Controls.Add(Me.CmdExportToExcel)
        Me.GrpBPOrder.Controls.Add(Me.Label32)
        Me.GrpBPOrder.Controls.Add(Me.cboBPFeeType)
        Me.GrpBPOrder.Controls.Add(Me.txtBPAmountFee)
        Me.GrpBPOrder.Controls.Add(Me.lblPRAmountFee)
        Me.GrpBPOrder.Controls.Add(Me.cboBPFeeCCY)
        Me.GrpBPOrder.Controls.Add(Me.lblTemFeeCCY)
        Me.GrpBPOrder.Controls.Add(Me.txtBPOrderBrokerCode)
        Me.GrpBPOrder.Controls.Add(Me.txtBPOrderBrokerName)
        Me.GrpBPOrder.Controls.Add(Me.Label1)
        Me.GrpBPOrder.Controls.Add(Me.txtBPOrderIBAN)
        Me.GrpBPOrder.Controls.Add(Me.txtBPOrderSwift)
        Me.GrpBPOrder.Controls.Add(Me.lblOrderIBAN)
        Me.GrpBPOrder.Controls.Add(Me.lblOrderSwift)
        Me.GrpBPOrder.Controls.Add(Me.lblSettleDate)
        Me.GrpBPOrder.Controls.Add(Me.dtBPTransDate)
        Me.GrpBPOrder.Controls.Add(Me.lblBPOrderVOCode)
        Me.GrpBPOrder.Controls.Add(Me.cboBPOrderVOCode)
        Me.GrpBPOrder.Controls.Add(Me.lblBPBalance)
        Me.GrpBPOrder.Controls.Add(Me.txtBPOrderBalance)
        Me.GrpBPOrder.Controls.Add(Me.CboBPOrderAccount)
        Me.GrpBPOrder.Controls.Add(Me.lblOrderAccount)
        Me.GrpBPOrder.Controls.Add(Me.lblportfoliofee)
        Me.GrpBPOrder.Controls.Add(Me.lblportfolio)
        Me.GrpBPOrder.Controls.Add(Me.CboBPPortfolio)
        Me.GrpBPOrder.Controls.Add(Me.CboBPCCY)
        Me.GrpBPOrder.Controls.Add(Me.lblCCY)
        Me.GrpBPOrder.Controls.Add(Me.lblTradeDate)
        Me.GrpBPOrder.Controls.Add(Me.dtBPSettleDate)
        Me.GrpBPOrder.Location = New System.Drawing.Point(9, 6)
        Me.GrpBPOrder.Name = "GrpBPOrder"
        Me.GrpBPOrder.Size = New System.Drawing.Size(1589, 205)
        Me.GrpBPOrder.TabIndex = 13
        Me.GrpBPOrder.TabStop = False
        Me.GrpBPOrder.Text = "Order Details"
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1539, 154)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(44, 45)
        Me.CmdExportToExcel.TabIndex = 178
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(36, 143)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(55, 13)
        Me.Label32.TabIndex = 177
        Me.Label32.Text = "Fee Type:"
        '
        'cboBPFeeType
        '
        Me.cboBPFeeType.BackColor = System.Drawing.Color.White
        Me.cboBPFeeType.FormattingEnabled = True
        Me.cboBPFeeType.Location = New System.Drawing.Point(96, 140)
        Me.cboBPFeeType.Name = "cboBPFeeType"
        Me.cboBPFeeType.Size = New System.Drawing.Size(90, 21)
        Me.cboBPFeeType.TabIndex = 171
        '
        'txtBPAmountFee
        '
        Me.txtBPAmountFee.Location = New System.Drawing.Point(237, 140)
        Me.txtBPAmountFee.Name = "txtBPAmountFee"
        Me.txtBPAmountFee.Size = New System.Drawing.Size(75, 20)
        Me.txtBPAmountFee.TabIndex = 173
        Me.txtBPAmountFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPRAmountFee
        '
        Me.lblPRAmountFee.AutoSize = True
        Me.lblPRAmountFee.Location = New System.Drawing.Point(203, 143)
        Me.lblPRAmountFee.Name = "lblPRAmountFee"
        Me.lblPRAmountFee.Size = New System.Drawing.Size(28, 13)
        Me.lblPRAmountFee.TabIndex = 176
        Me.lblPRAmountFee.Text = "Fee:"
        '
        'cboBPFeeCCY
        '
        Me.cboBPFeeCCY.BackColor = System.Drawing.Color.White
        Me.cboBPFeeCCY.FormattingEnabled = True
        Me.cboBPFeeCCY.Location = New System.Drawing.Point(355, 139)
        Me.cboBPFeeCCY.Name = "cboBPFeeCCY"
        Me.cboBPFeeCCY.Size = New System.Drawing.Size(78, 21)
        Me.cboBPFeeCCY.TabIndex = 174
        '
        'lblTemFeeCCY
        '
        Me.lblTemFeeCCY.AutoSize = True
        Me.lblTemFeeCCY.Location = New System.Drawing.Point(318, 143)
        Me.lblTemFeeCCY.Name = "lblTemFeeCCY"
        Me.lblTemFeeCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblTemFeeCCY.TabIndex = 175
        Me.lblTemFeeCCY.Text = "CCY:"
        '
        'txtBPOrderBrokerCode
        '
        Me.txtBPOrderBrokerCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtBPOrderBrokerCode.Enabled = False
        Me.txtBPOrderBrokerCode.Location = New System.Drawing.Point(638, 66)
        Me.txtBPOrderBrokerCode.Name = "txtBPOrderBrokerCode"
        Me.txtBPOrderBrokerCode.Size = New System.Drawing.Size(39, 20)
        Me.txtBPOrderBrokerCode.TabIndex = 98
        Me.txtBPOrderBrokerCode.Tag = ""
        Me.txtBPOrderBrokerCode.Visible = False
        '
        'txtBPOrderBrokerName
        '
        Me.txtBPOrderBrokerName.BackColor = System.Drawing.SystemColors.Window
        Me.txtBPOrderBrokerName.Enabled = False
        Me.txtBPOrderBrokerName.Location = New System.Drawing.Point(96, 114)
        Me.txtBPOrderBrokerName.Name = "txtBPOrderBrokerName"
        Me.txtBPOrderBrokerName.Size = New System.Drawing.Size(337, 20)
        Me.txtBPOrderBrokerName.TabIndex = 96
        Me.txtBPOrderBrokerName.Tag = "ControlAddress"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(52, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 97
        Me.Label1.Text = "Name:"
        '
        'txtBPOrderIBAN
        '
        Me.txtBPOrderIBAN.BackColor = System.Drawing.SystemColors.Window
        Me.txtBPOrderIBAN.Enabled = False
        Me.txtBPOrderIBAN.Location = New System.Drawing.Point(504, 116)
        Me.txtBPOrderIBAN.Name = "txtBPOrderIBAN"
        Me.txtBPOrderIBAN.Size = New System.Drawing.Size(226, 20)
        Me.txtBPOrderIBAN.TabIndex = 93
        Me.txtBPOrderIBAN.Tag = ""
        '
        'txtBPOrderSwift
        '
        Me.txtBPOrderSwift.BackColor = System.Drawing.SystemColors.Window
        Me.txtBPOrderSwift.Enabled = False
        Me.txtBPOrderSwift.Location = New System.Drawing.Point(504, 91)
        Me.txtBPOrderSwift.Name = "txtBPOrderSwift"
        Me.txtBPOrderSwift.Size = New System.Drawing.Size(226, 20)
        Me.txtBPOrderSwift.TabIndex = 92
        Me.txtBPOrderSwift.Tag = ""
        '
        'lblOrderIBAN
        '
        Me.lblOrderIBAN.AutoSize = True
        Me.lblOrderIBAN.Location = New System.Drawing.Point(463, 119)
        Me.lblOrderIBAN.Name = "lblOrderIBAN"
        Me.lblOrderIBAN.Size = New System.Drawing.Size(35, 13)
        Me.lblOrderIBAN.TabIndex = 95
        Me.lblOrderIBAN.Text = "IBAN:"
        '
        'lblOrderSwift
        '
        Me.lblOrderSwift.AutoSize = True
        Me.lblOrderSwift.Location = New System.Drawing.Point(463, 94)
        Me.lblOrderSwift.Name = "lblOrderSwift"
        Me.lblOrderSwift.Size = New System.Drawing.Size(33, 13)
        Me.lblOrderSwift.TabIndex = 94
        Me.lblOrderSwift.Text = "Swift:"
        '
        'lblSettleDate
        '
        Me.lblSettleDate.AutoSize = True
        Me.lblSettleDate.Location = New System.Drawing.Point(435, 69)
        Me.lblSettleDate.Name = "lblSettleDate"
        Me.lblSettleDate.Size = New System.Drawing.Size(63, 13)
        Me.lblSettleDate.TabIndex = 91
        Me.lblSettleDate.Text = "Settle Date:"
        '
        'dtBPTransDate
        '
        Me.dtBPTransDate.Location = New System.Drawing.Point(96, 62)
        Me.dtBPTransDate.Name = "dtBPTransDate"
        Me.dtBPTransDate.Size = New System.Drawing.Size(128, 20)
        Me.dtBPTransDate.TabIndex = 90
        '
        'lblBPOrderVOCode
        '
        Me.lblBPOrderVOCode.AutoSize = True
        Me.lblBPOrderVOCode.Location = New System.Drawing.Point(594, 42)
        Me.lblBPOrderVOCode.Name = "lblBPOrderVOCode"
        Me.lblBPOrderVOCode.Size = New System.Drawing.Size(53, 13)
        Me.lblBPOrderVOCode.TabIndex = 89
        Me.lblBPOrderVOCode.Text = "VO Code:"
        Me.lblBPOrderVOCode.Visible = False
        '
        'cboBPOrderVOCode
        '
        Me.cboBPOrderVOCode.FormattingEnabled = True
        Me.cboBPOrderVOCode.Location = New System.Drawing.Point(652, 39)
        Me.cboBPOrderVOCode.Name = "cboBPOrderVOCode"
        Me.cboBPOrderVOCode.Size = New System.Drawing.Size(78, 21)
        Me.cboBPOrderVOCode.TabIndex = 88
        Me.cboBPOrderVOCode.Tag = "ControlAddressIntermediary"
        Me.cboBPOrderVOCode.Visible = False
        '
        'lblBPBalance
        '
        Me.lblBPBalance.AutoSize = True
        Me.lblBPBalance.Location = New System.Drawing.Point(456, 146)
        Me.lblBPBalance.Name = "lblBPBalance"
        Me.lblBPBalance.Size = New System.Drawing.Size(49, 13)
        Me.lblBPBalance.TabIndex = 69
        Me.lblBPBalance.Text = "Balance:"
        '
        'txtBPOrderBalance
        '
        Me.txtBPOrderBalance.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtBPOrderBalance.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtBPOrderBalance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBPOrderBalance.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtBPOrderBalance.Location = New System.Drawing.Point(511, 147)
        Me.txtBPOrderBalance.Name = "txtBPOrderBalance"
        Me.txtBPOrderBalance.ReadOnly = True
        Me.txtBPOrderBalance.Size = New System.Drawing.Size(128, 13)
        Me.txtBPOrderBalance.TabIndex = 68
        Me.txtBPOrderBalance.TabStop = False
        Me.txtBPOrderBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CboBPOrderAccount
        '
        Me.CboBPOrderAccount.FormattingEnabled = True
        Me.CboBPOrderAccount.Location = New System.Drawing.Point(96, 89)
        Me.CboBPOrderAccount.Name = "CboBPOrderAccount"
        Me.CboBPOrderAccount.Size = New System.Drawing.Size(337, 21)
        Me.CboBPOrderAccount.TabIndex = 66
        '
        'lblOrderAccount
        '
        Me.lblOrderAccount.AutoSize = True
        Me.lblOrderAccount.Location = New System.Drawing.Point(40, 92)
        Me.lblOrderAccount.Name = "lblOrderAccount"
        Me.lblOrderAccount.Size = New System.Drawing.Size(50, 13)
        Me.lblOrderAccount.TabIndex = 67
        Me.lblOrderAccount.Text = "Account:"
        '
        'lblportfoliofee
        '
        Me.lblportfoliofee.AutoSize = True
        Me.lblportfoliofee.Location = New System.Drawing.Point(322, 4)
        Me.lblportfoliofee.Name = "lblportfoliofee"
        Me.lblportfoliofee.Size = New System.Drawing.Size(0, 13)
        Me.lblportfoliofee.TabIndex = 65
        '
        'lblportfolio
        '
        Me.lblportfolio.AutoSize = True
        Me.lblportfolio.Location = New System.Drawing.Point(42, 40)
        Me.lblportfolio.Name = "lblportfolio"
        Me.lblportfolio.Size = New System.Drawing.Size(48, 13)
        Me.lblportfolio.TabIndex = 64
        Me.lblportfolio.Text = "Portfolio:"
        '
        'CboBPPortfolio
        '
        Me.CboBPPortfolio.FormattingEnabled = True
        Me.CboBPPortfolio.Location = New System.Drawing.Point(96, 35)
        Me.CboBPPortfolio.Name = "CboBPPortfolio"
        Me.CboBPPortfolio.Size = New System.Drawing.Size(337, 21)
        Me.CboBPPortfolio.TabIndex = 54
        '
        'CboBPCCY
        '
        Me.CboBPCCY.FormattingEnabled = True
        Me.CboBPCCY.Location = New System.Drawing.Point(504, 37)
        Me.CboBPCCY.Name = "CboBPCCY"
        Me.CboBPCCY.Size = New System.Drawing.Size(78, 21)
        Me.CboBPCCY.TabIndex = 55
        '
        'lblCCY
        '
        Me.lblCCY.AutoSize = True
        Me.lblCCY.Location = New System.Drawing.Point(467, 40)
        Me.lblCCY.Name = "lblCCY"
        Me.lblCCY.Size = New System.Drawing.Size(31, 13)
        Me.lblCCY.TabIndex = 63
        Me.lblCCY.Text = "CCY:"
        '
        'lblTradeDate
        '
        Me.lblTradeDate.AutoSize = True
        Me.lblTradeDate.Location = New System.Drawing.Point(27, 64)
        Me.lblTradeDate.Name = "lblTradeDate"
        Me.lblTradeDate.Size = New System.Drawing.Size(64, 13)
        Me.lblTradeDate.TabIndex = 60
        Me.lblTradeDate.Text = "Trade Date:"
        '
        'dtBPSettleDate
        '
        Me.dtBPSettleDate.Location = New System.Drawing.Point(504, 65)
        Me.dtBPSettleDate.Name = "dtBPSettleDate"
        Me.dtBPSettleDate.Size = New System.Drawing.Size(128, 20)
        Me.dtBPSettleDate.TabIndex = 58
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage2.Controls.Add(Me.GroupBox7)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.GrpKYC)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1600, 217)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabKYC"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.LVFiles1)
        Me.GroupBox7.Location = New System.Drawing.Point(939, 117)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(655, 95)
        Me.GroupBox7.TabIndex = 109
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Payment SSI File Attachments"
        '
        'LVFiles1
        '
        Me.LVFiles1.AllowDrop = True
        Me.LVFiles1.BackColor = System.Drawing.SystemColors.Window
        Me.LVFiles1.BackgroundImage = CType(resources.GetObject("LVFiles1.BackgroundImage"), System.Drawing.Image)
        Me.LVFiles1.FullRowSelect = True
        Me.LVFiles1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LVFiles1.Location = New System.Drawing.Point(6, 15)
        Me.LVFiles1.MultiSelect = False
        Me.LVFiles1.Name = "LVFiles1"
        Me.LVFiles1.Size = New System.Drawing.Size(642, 73)
        Me.LVFiles1.TabIndex = 42
        Me.LVFiles1.UseCompatibleStateImageBehavior = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LVFiles)
        Me.GroupBox1.Location = New System.Drawing.Point(939, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(655, 105)
        Me.GroupBox1.TabIndex = 108
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Compliance File Attachments"
        '
        'LVFiles
        '
        Me.LVFiles.AllowDrop = True
        Me.LVFiles.BackColor = System.Drawing.SystemColors.Window
        Me.LVFiles.BackgroundImage = CType(resources.GetObject("LVFiles.BackgroundImage"), System.Drawing.Image)
        Me.LVFiles.FullRowSelect = True
        Me.LVFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LVFiles.Location = New System.Drawing.Point(6, 19)
        Me.LVFiles.MultiSelect = False
        Me.LVFiles.Name = "LVFiles"
        Me.LVFiles.Size = New System.Drawing.Size(642, 74)
        Me.LVFiles.TabIndex = 41
        Me.LVFiles.UseCompatibleStateImageBehavior = False
        '
        'GrpKYC
        '
        Me.GrpKYC.Controls.Add(Me.cmdBPBrowse)
        Me.GrpKYC.Controls.Add(Me.txtBPFilePath)
        Me.GrpKYC.Controls.Add(Me.Label34)
        Me.GrpKYC.Controls.Add(Me.Label3)
        Me.GrpKYC.Controls.Add(Me.ChkBPKYCsanctions)
        Me.GrpKYC.Controls.Add(Me.txtBPKYCIssuesDisclosed)
        Me.GrpKYC.Controls.Add(Me.ChkBPKYCPEP)
        Me.GrpKYC.Controls.Add(Me.ChkBPKYCCDD)
        Me.GrpKYC.Location = New System.Drawing.Point(11, 6)
        Me.GrpKYC.Name = "GrpKYC"
        Me.GrpKYC.Size = New System.Drawing.Size(922, 205)
        Me.GrpKYC.TabIndex = 107
        Me.GrpKYC.TabStop = False
        Me.GrpKYC.Text = "Please confirm and tick the following:"
        '
        'cmdBPBrowse
        '
        Me.cmdBPBrowse.Location = New System.Drawing.Point(819, 21)
        Me.cmdBPBrowse.Name = "cmdBPBrowse"
        Me.cmdBPBrowse.Size = New System.Drawing.Size(97, 23)
        Me.cmdBPBrowse.TabIndex = 170
        Me.cmdBPBrowse.Text = "Browse"
        Me.cmdBPBrowse.UseVisualStyleBackColor = True
        '
        'txtBPFilePath
        '
        Me.txtBPFilePath.Location = New System.Drawing.Point(71, 21)
        Me.txtBPFilePath.Name = "txtBPFilePath"
        Me.txtBPFilePath.Size = New System.Drawing.Size(742, 20)
        Me.txtBPFilePath.TabIndex = 168
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(14, 24)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(51, 13)
        Me.Label34.TabIndex = 169
        Me.Label34.Text = "File Path:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(733, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(183, 13)
        Me.Label3.TabIndex = 107
        Me.Label3.Text = "Issues Disclosed/Payment Rationale:"
        '
        'ChkBPKYCsanctions
        '
        Me.ChkBPKYCsanctions.AutoSize = True
        Me.ChkBPKYCsanctions.Location = New System.Drawing.Point(13, 55)
        Me.ChkBPKYCsanctions.Name = "ChkBPKYCsanctions"
        Me.ChkBPKYCsanctions.Size = New System.Drawing.Size(449, 17)
        Me.ChkBPKYCsanctions.TabIndex = 103
        Me.ChkBPKYCsanctions.Text = "Confirmed payees and country of payees does not have sanctions against all benefi" &
    "ciaries"
        Me.ChkBPKYCsanctions.UseVisualStyleBackColor = True
        '
        'txtBPKYCIssuesDisclosed
        '
        Me.txtBPKYCIssuesDisclosed.Location = New System.Drawing.Point(13, 128)
        Me.txtBPKYCIssuesDisclosed.Multiline = True
        Me.txtBPKYCIssuesDisclosed.Name = "txtBPKYCIssuesDisclosed"
        Me.txtBPKYCIssuesDisclosed.Size = New System.Drawing.Size(903, 71)
        Me.txtBPKYCIssuesDisclosed.TabIndex = 106
        '
        'ChkBPKYCPEP
        '
        Me.ChkBPKYCPEP.AutoSize = True
        Me.ChkBPKYCPEP.Location = New System.Drawing.Point(13, 78)
        Me.ChkBPKYCPEP.Name = "ChkBPKYCPEP"
        Me.ChkBPKYCPEP.Size = New System.Drawing.Size(473, 17)
        Me.ChkBPKYCPEP.TabIndex = 104
        Me.ChkBPKYCPEP.Text = "Confirmed PEP (Politically Exposed Person check) has been completed against all b" &
    "eneficiaries"
        Me.ChkBPKYCPEP.UseVisualStyleBackColor = True
        '
        'ChkBPKYCCDD
        '
        Me.ChkBPKYCCDD.AutoSize = True
        Me.ChkBPKYCCDD.Location = New System.Drawing.Point(13, 101)
        Me.ChkBPKYCCDD.Name = "ChkBPKYCCDD"
        Me.ChkBPKYCCDD.Size = New System.Drawing.Size(465, 17)
        Me.ChkBPKYCCDD.TabIndex = 105
        Me.ChkBPKYCCDD.Text = "Confirmed CDD Customer Due Diligence checks has been completed against all benefi" &
    "ciaries"
        Me.ChkBPKYCCDD.UseVisualStyleBackColor = True
        '
        'ContextMenuStripFiles
        '
        Me.ContextMenuStripFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenFileToolStripMenuItem, Me.RemoveFileToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStripFiles.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles.Size = New System.Drawing.Size(139, 70)
        '
        'OpenFileToolStripMenuItem
        '
        Me.OpenFileToolStripMenuItem.Name = "OpenFileToolStripMenuItem"
        Me.OpenFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.OpenFileToolStripMenuItem.Text = "&Open File"
        '
        'RemoveFileToolStripMenuItem
        '
        Me.RemoveFileToolStripMenuItem.Name = "RemoveFileToolStripMenuItem"
        Me.RemoveFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RemoveFileToolStripMenuItem.Text = "&Remove File"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ContextMenuStripFiles1
        '
        Me.ContextMenuStripFiles1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.ToolStripMenuItem3})
        Me.ContextMenuStripFiles1.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles1.Size = New System.Drawing.Size(139, 70)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem1.Text = "&Open File"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem2.Text = "&Remove File"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem3.Text = "Refresh"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'FrmBatchProcessing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1630, 809)
        Me.Controls.Add(Me.TabBatch)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.GrpBenDetails)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblpaymentTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmBatchProcessing"
        Me.Text = "FrmBatchProcessing"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GrpBenDetails.ResumeLayout(False)
        CType(Me.dgvbp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabBatch.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GrpBPOrder.ResumeLayout(False)
        Me.GrpBPOrder.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GrpKYC.ResumeLayout(False)
        Me.GrpKYC.PerformLayout()
        Me.ContextMenuStripFiles.ResumeLayout(False)
        Me.ContextMenuStripFiles1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblpaymentTitle As Label
    Friend WithEvents LblPayType As Label
    Friend WithEvents CboBPType As ComboBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GrpBenDetails As GroupBox
    Friend WithEvents dgvbp As DataGridView
    Friend WithEvents cmdSave As Button
    Friend WithEvents cboBPPayType As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents TabBatch As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GrpBPOrder As GroupBox
    Friend WithEvents Label32 As Label
    Friend WithEvents cboBPFeeType As ComboBox
    Friend WithEvents txtBPAmountFee As TextBox
    Friend WithEvents lblPRAmountFee As Label
    Friend WithEvents cboBPFeeCCY As ComboBox
    Friend WithEvents lblTemFeeCCY As Label
    Friend WithEvents txtBPOrderBrokerCode As TextBox
    Friend WithEvents txtBPOrderBrokerName As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBPOrderIBAN As TextBox
    Friend WithEvents txtBPOrderSwift As TextBox
    Friend WithEvents lblOrderIBAN As Label
    Friend WithEvents lblOrderSwift As Label
    Friend WithEvents lblSettleDate As Label
    Friend WithEvents dtBPTransDate As DateTimePicker
    Friend WithEvents lblBPOrderVOCode As Label
    Friend WithEvents cboBPOrderVOCode As ComboBox
    Friend WithEvents lblBPBalance As Label
    Friend WithEvents txtBPOrderBalance As TextBox
    Friend WithEvents CboBPOrderAccount As ComboBox
    Friend WithEvents lblOrderAccount As Label
    Friend WithEvents lblportfoliofee As Label
    Friend WithEvents lblportfolio As Label
    Friend WithEvents CboBPPortfolio As ComboBox
    Friend WithEvents CboBPCCY As ComboBox
    Friend WithEvents lblCCY As Label
    Friend WithEvents lblTradeDate As Label
    Friend WithEvents dtBPSettleDate As DateTimePicker
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents GrpKYC As GroupBox
    Friend WithEvents ChkBPKYCsanctions As CheckBox
    Friend WithEvents txtBPKYCIssuesDisclosed As TextBox
    Friend WithEvents ChkBPKYCPEP As CheckBox
    Friend WithEvents ChkBPKYCCDD As CheckBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents LVFiles1 As ListView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents LVFiles As ListView
    Friend WithEvents Label3 As Label
    Friend WithEvents cmdBPBrowse As Button
    Friend WithEvents txtBPFilePath As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents BPFolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents ContextMenuStripFiles As ContextMenuStrip
    Friend WithEvents OpenFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStripFiles1 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
End Class
