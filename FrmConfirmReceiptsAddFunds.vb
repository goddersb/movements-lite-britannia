﻿Imports Microsoft.Office
Public Class FrmConfirmReceiptsAddFunds
    Dim TempDir As String = ""

    Private Function ValidFunds() As Boolean
        ValidFunds = True
        If Not IsNumeric(txtCRMFAmt.Text) Then
            MsgBox("Please enter a valid amount", vbExclamation)
            ValidFunds = False
        ElseIf txtCRMFSender.Text = "" Then
            MsgBox("Please enter sender name", vbExclamation)
            ValidFunds = False
        ElseIf cboCRMFCCY.text = "" Then
            MsgBox("Please enter ccy", vbExclamation)
            ValidFunds = False
        ElseIf crmfFiles.Items.Count < 1 Then
            MsgBox("Please enter a funds SSI email or file attachment", vbExclamation)
            ValidFunds = False
        End If
    End Function

    Private Sub CmdAddFunds_Click(sender As Object, e As EventArgs) Handles CmdAddFunds.Click
        Dim varNewID As Double = 0
        Dim varFilePath As String = ""

        If ValidFunds() Then
            ClsCRM.TradeDate = dtCRMFTradeDate.Value
            ClsCRM.SettleDate = dtCRMFSettleDate.Value
            ClsCRM.SenderName = txtCRMFSender.Text
            ClsCRM.CCYCode = cboCRMFCCY.SelectedValue
            ClsCRM.Amount = txtCRMFAmt.Text
            If cboCRMFBankName.Text <> "" Or cboCRMFSwiftCode.Text <> "" Then
                If Rb1.Checked Then
                    ClsCRM.SwiftCode = cboCRMFBankName.SelectedValue
                    ClsCRM.BankName = cboCRMFBankName.Text
                Else
                    ClsCRM.SwiftCode = cboCRMFSwiftCode.Text
                End If
            Else
                ClsCRM.SwiftCode = ""
                ClsCRM.BankName = ""
            End If

            ClsCRM.FilePath = ClsIMS.GetFilePath("ConfirmReceiptsManual")
            ClsCRM.Notes = txtCRMFNotes.Text

            ClsIMS.UpdateManualFunds(varNewID, varFilePath)
            If varNewID = 0 Then
                MsgBox("There is a problem adding the incoming funds. Either try to enter again or contact systems support for assistance", vbCritical, "Funds not added")
            Else
                ConfirmCorrectDirectoryCreated(TempDir, varFilePath)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub FrmConfirmReceiptsAddFunds_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Cursor = Cursors.WaitCursor
        ClsIMS.PopulateComboboxWithData(cboCRMFCCY, "select cr_code,cr_name1 from vwDolfinPaymentSelectAccount where pf_code=288 and b_code=222 group by cr_code,cr_name1 order by cr_name1")
        ClsIMS.PopulateComboboxWithData(cboCRMFBankName, "Select SC_SwiftCode, SC_BankName + space(10) + '(' + SC_SwiftCode + ')' as SC_BankName from vwDolfinPaymentSwiftCodes group by SC_SwiftCode,  SC_BankName + space(10) + '(' + SC_SwiftCode + ')' order by SC_BankName")
        TempDir = GetNewTempDirectory(ClsIMS.GetFilePath("ConfirmReceiptsManual"))
        ExtractAssociatedIconEx()
        Cursor = Cursors.Default
    End Sub


    Public Sub ExtractAssociatedIconEx()
        Try
            CRMFFiles.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If IO.Directory.Exists(TempDir) Then

                CRMFFiles.SmallImageList = ImageFileList
                CRMFFiles.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New IO.DirectoryInfo(TempDir)

                Dim item As ListViewItem
                CRMFFiles.BeginUpdate()
                Dim file As IO.FileInfo
                For Each file In dir.GetFiles()
                    ' Set a default icon for the file.
                    Dim iconForFile As Icon = SystemIcons.WinLogo
                        item = New ListViewItem(file.Name, 1)

                    ' Check to see if the image collection contains an image
                    ' for this extension, using the extension as a key.
                    If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                        ' If not, add the image to the image list.
                        'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                        ImageFileList.Images.Add(file.Extension, iconForFile)
                    End If
                    item.ImageKey = file.Extension
                    CRMFFiles.Items.Add(item)
                Next file
                CRMFFiles.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx - Payment Request")
        End Try
    End Sub

    Private Sub CRMFFiles_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles CRMFFiles.ItemMouseHover
        CRMFFiles.ContextMenuStrip = ContextMenuStripFiles
    End Sub

    Private Sub CRMFFiles_DragEnter(sender As Object, e As DragEventArgs) Handles CRMFFiles.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub CRMFFiles_DragLeave(sender As Object, e As EventArgs) Handles CRMFFiles.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub CRMFFiles_DragDrop(sender As Object, e As DragEventArgs) Handles CRMFFiles.DragDrop
        If IO.Directory.Exists(TempDir) Then
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    If Not IO.File.Exists(TempDir & Dir(MyFiles(i))) Then
                        If Len(Dir(MyFiles(i))) > 80 Then
                            MsgBox("Only named files less than 80 characters are allowed.", vbInformation, "Shorten name of file")
                        Else
                            IO.File.Copy(MyFiles(i), TempDir & Dir(MyFiles(i)))
                            ExtractAssociatedIconEx()
                        End If
                    End If
                Next

            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Interop.Outlook.MailItem) Then
                                mi.SaveAs(TempDir & "M" & MsgCount & "_" & mi.SenderName.ToString & "_" & mi.ReceivedTime.ToString("yyyy-MM-dd HHmm") & ".msg")
                                MsgCount = MsgCount + 1
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub OpenFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenFileToolStripMenuItem.Click
        If CRMFFiles.SelectedItems.Count > 0 Then
            ShellExecute(TempDir & CRMFFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub RemoveFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveFileToolStripMenuItem.Click
        If CRMFFiles.SelectedItems.Count > 0 Then
            IO.File.Delete(TempDir & CRMFFiles.SelectedItems(0).Text)
        End If
        ExtractAssociatedIconEx()
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        ExtractAssociatedIconEx()
    End Sub

    Private Sub CRMFFiles_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles CRMFFiles.MouseDoubleClick
        If CRMFFiles.SelectedItems.Count > 0 Then
            ShellExecute(TempDir & CRMFFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub Rb1_CheckedChanged(sender As Object, e As EventArgs) Handles Rb1.CheckedChanged
        RadioButtonAction()
    End Sub

    Private Sub Rb2_CheckedChanged(sender As Object, e As EventArgs) Handles Rb2.CheckedChanged
        RadioButtonAction()
    End Sub

    Private Sub RadioButtonAction()
        If Rb1.Checked Then
            cboCRMFSwiftCode.Enabled = False
            cboCRMFBankName.Enabled = True
            cboCRMFSwiftCode.Text = ""
        Else
            If cboCRMFSwiftCode.Items.Count = 0 Then
                Cursor = Cursors.WaitCursor
                ClsIMS.PopulateComboboxWithData(cboCRMFSwiftCode, "Select 0, SC_SwiftCode from vwDolfinPaymentSwiftCodes group by SC_SwiftCode order by SC_SwiftCode")
                Cursor = Cursors.Default
            End If
            cboCRMFBankName.Text = ""
            cboCRMFSwiftCode.Enabled = True
            cboCRMFBankName.Enabled = False
        End If
    End Sub
End Class