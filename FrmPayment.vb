﻿Imports System.IO
Imports Microsoft.Office

Public Class FrmPayment
    Dim ts As New TimeSpan
    Dim varLblBroker As Label
    Dim TempDirectory As String = ""
    Dim varBeneficiaryAddressID As Integer = 0
    Dim varBeneficiarySubBankAddressID As Integer = 0
    Dim varBeneficiaryBankAddressID As Integer = 0
    Dim varUserLocationCode As Integer = 0
    Dim varPopulateExtraFormDetailsFromSwiftCode As Boolean = False
    Dim loadEditForm As Boolean
    Dim originalStyle As Integer = -1
    Dim enableFormLevelDoubleBuffering As Boolean = True
    Private _filePaths As New List(Of FilePaths)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        _filePaths = ClsIMS.FetchFilePaths()

    End Sub

    ''' <summary>
    ''' Code to handle the form flickering.
    ''' </summary>
    ''' <returns></returns>
    Protected Overrides ReadOnly Property CreateParams() As CreateParams

        Get
            If originalStyle = -1 Then originalStyle = MyBase.CreateParams.ExStyle
            Dim cp As CreateParams = MyBase.CreateParams

            If enableFormLevelDoubleBuffering Then
                cp.ExStyle = cp.ExStyle Or &H2000000
            Else
                cp.ExStyle = originalStyle
            End If

            Return cp
        End Get

    End Property

    Private Sub FrmPayment_Load(sender As Object, e As EventArgs) Handles Me.Load

        ToggleAntiFlicker(True)
        PicPayType.Image = ImageListTickCross.Images(2)
        ModRMS.DoubleBuffered(dgvCash, True)
        ModRMS.DoubleBuffered(dgvHistory, True)
        ChkCashRefresh.Checked = ClsIMS.UserOptRefresh
        If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSubmit) Then
            cmdSubmit.Visible = True
        Else
            cmdSubmit.Visible = False
        End If
        AddToolTips()
        ClsIMS.RefreshPaymentGrid(dgvCash)
        RefreshNewItem()

    End Sub

    Public Sub SelectPaymentOption()
        txtID.Text = ""
        txtAmount.Text = ""
        txtComments.Text = ""

        ClsPay.DeleteFileDirectory(TempDirectory)
        TempDirectory = ""
        ClsPay.SelectedPaymentID = 0
        ClsPay.PortfolioFeeID = 0
        ClsPay.PortfolioFeeDue = False
        ChkBenTemplate.Checked = False
        If ClsPay.SelectedPaymentOption = 1 Then
            Changeform(True)
        ElseIf ClsPay.SelectedPaymentOption = 2 Then
            ClsPay.SelectedOrder = 1
            CboOrderAccount.SelectedValue = CboOrderAccount.SelectedValue
            CboOrderAccount.Text = CboOrderAccount.Text
            ClsPay.OrderAccountCode = ClsPay.OrderAccountCode
            CboOrderName.SelectedValue = CboOrderName.SelectedValue
            CboOrderName.Text = CboOrderName.Text
            clearBen()
            clearBenBank()
            clearBenSubBank()
            OrderMessages()
        ElseIf ClsPay.SelectedPaymentOption = 3 Then
            clearOrder()
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                CboBenAccount.SelectedValue = CboBenAccount.SelectedValue
                CboBenAccount.Text = CboBenAccount.Text
                ClsPay.BeneficiaryBrokerCode = ClsPay.BeneficiaryBrokerCode
                CboBenName.SelectedValue = CboBenName.SelectedValue
                CboBenName.Text = CboBenName.Text
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                ClsIMS.PopulateComboboxWithData(CboBenAccount, "select 0,'' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress group by Pa_TemplateName order by Pa_TemplateName")
            End If

            BenMessages()
        End If

        ClsPay = New ClsPayment
        ClsPayAuth = New ClsPayment

        CboBenName.Enabled = True
        FormControlsEnable(2, True)
        FormControlsEnable(3, True)
        FormControlsEnable(4, True)

        ClsPay.PaymentFilePath = ""
        RefreshFiles()
    End Sub

    Private Sub ClearPaymentDetails()
        CboPayType.Text = ""
        CboPortfolio.Text = ""
        ClsPay.Portfolio = ""
        CboCCY.Text = ""
        cboBenCCY.Text = ""
        txtAmount.Text = ""
        txtExchRate.Text = ""
        dtTransDate.Value = Now
        dtSettleDate.Value = Now
        txtComments.Text = ""
        CCYDefaults(True)
        ClearOrderMessages()
        ClearBenMessages()
    End Sub

    Private Sub clearOrder()
        CboOrderAccount.Text = ""
        CboOrderName.Text = ""
        ClearOrderAddress()
    End Sub

    Private Sub ClearOrderAddress()
        txtOrderAddress1.Text = ""
        txtOrderAddress2.Text = ""
        txtOrderZip.Text = ""
        cboOrderCountry.Text = ""
        txtOrderSwift.Text = ""
        txtOrderIBAN.Text = ""
        txtOrderBalance.Text = ""
        txtOrderBalanceInc.Text = ""
        txtOrderBalancePostInc.Text = ""
        cboOrderVOCode.Text = ""
        txtIMSRef.Text = ""
        PicOrderSwift.Image = Nothing
        PicOrderIBAN.Image = Nothing
    End Sub
    Private Sub ClearOrderAddressForSwift()
        txtOrderAddress1.Text = ""
        txtOrderAddress2.Text = ""
        txtOrderZip.Text = ""
        cboOrderCountry.Text = ""
    End Sub

    Private Sub clearBen()
        CboBenAccount.Text = ""
        CboBenName.Text = ""
        txtbenfirstname.Text = ""
        txtbenmiddlename.Text = ""
        txtbensurname.Text = ""
        ClearBenAddress()
    End Sub

    Private Sub ClearBenAddress()
        txtBenAddress1.Text = ""
        txtBenAddress2.Text = ""
        txtbencounty.Text = ""
        txtBenZip.Text = ""
        cboBenType.Text = ""
        cbobenrelationship.Text = ""
        cboBenCountry.Text = ""
        txtBenSwift.Text = ""
        txtBenIBAN.Text = ""
        txtBenOther1.Text = ""
        txtBenBalance.Text = ""
        txtBenBalanceInc.Text = ""
        txtBenBalancePostInc.Text = ""
        txtBenBIK.Text = ""
        txtBenINN.Text = ""
        PicBenSwift.Image = Nothing
        PicBenIBAN.Image = Nothing
    End Sub
    Private Sub ClearBenAddressForSwift()
        txtBenAddress1.Text = ""
        txtBenAddress2.Text = ""
        txtBenZip.Text = ""
    End Sub
    Private Sub clearBenSubBank()
        txtBenSubBankAddress1.Text = ""
        txtBenSubBankAddress2.Text = ""
        txtBenSubBankZip.Text = ""
        cboBenSubBankCountry.Text = ""
        txtBenSubBankSwift.Text = ""
        txtBenSubBankIBAN.Text = ""
        txtBenSubBankOther1.Text = ""
        txtBenSubBankBIK.Text = ""
        txtBenSubbankINN.Text = ""
        PicBenSubBankSwift.Image = Nothing
        PicBenSubBankIBAN.Image = Nothing
    End Sub

    Private Sub clearBenBank()
        txtBenBankAddress1.Text = ""
        txtBenBankAddress2.Text = ""
        txtBenBankZip.Text = ""
        cboBenBankCountry.Text = ""
        txtBenBankSwift.Text = ""
        txtBenBankIBAN.Text = ""
        txtBenBankOther1.Text = ""
        txtBenBankBIK.Text = ""
        txtBenBankINN.Text = ""
        PicBenBankSwift.Image = Nothing
        PicBenBankIBAN.Image = Nothing
    End Sub
    Private Sub clearSubBenBankAddressForSwift()
        txtBenSubBankAddress1.Text = ""
        txtBenSubBankAddress2.Text = ""
        txtBenSubBankZip.Text = ""
        cboBenSubBankCountry.Text = ""
    End Sub

    Private Sub clearBenBankAddressForSwift()
        txtBenBankAddress1.Text = ""
        txtBenBankAddress2.Text = ""
        txtBenBankZip.Text = ""
        cboBenBankCountry.Text = ""
    End Sub

    Private Sub ManageTabs()
        TabMovement.TabPages.Remove(BeneficiaryTab)
        TabMovement.TabPages.Remove(OrderTab)
        TabMovement.TabPages.Insert(0, BeneficiaryTab)
        TabMovement.TabPages.Insert(0, OrderTab)
        TabMovement.SelectedTab = OrderTab
    End Sub

    Private Function IsValidMovementDetails() As Boolean
        IsValidMovementDetails = True
        If CboPayType.Text = "" Then
            MsgBox("Please enter a movement type", vbInformation)
            IsValidMovementDetails = False
        ElseIf (CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescInPort) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
            CboPortfolio.Text = "") Then
            MsgBox("Please enter a portfolio", vbInformation)
            IsValidMovementDetails = False
        ElseIf CboCCY.Text = "" Then
            MsgBox("Please enter a currency", vbInformation)
            IsValidMovementDetails = False
        ElseIf Not IsNumeric(txtAmount.Text) Then
            MsgBox("Please enter an amount to pay", vbInformation)
            IsValidMovementDetails = False
        ElseIf CDbl(txtAmount.Text) <= 0 Then
            MsgBox("You can only move positive amounts from the accounts", vbInformation)
            IsValidMovementDetails = False
        ElseIf CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankCharges) And
            CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankChargesCovChg) And
            CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankInterestCreditOrdOnly) And
            CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankInterestDebitOrdOnly) And
            CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankInterestCreditBenOnly) And
            CboPortfolio.Text <> ClsIMS.GetPaymentOtherType(cPayDescInOtherBankInterestDebitBenOnly) Then
            If dtSettleDate.Value.Date < Now.Date Then
                MsgBox("Please adjust settlement date to a current or future date", vbInformation)
                IsValidMovementDetails = False
            ElseIf dtSettleDate.Value.Date < dtTransDate.Value.Date Then
                MsgBox("Please adjust settlement date to an equal or later date than the transaction date", vbInformation)
                IsValidMovementDetails = False
            End If
        End If
    End Function

    Private Function IsValidOrderDetails() As Boolean
        IsValidOrderDetails = True
        If TabMovement.TabPages(0).Visible Then
            If (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInOther)) Then
                If ClsPay.OtherID <> cPayDescInOtherIntTra And ClsPay.OtherID <> cPayDescInOtherNoticetoReceive And ClsPay.OtherID <> cPayDescInOtherStatementRequest Then
                    If CboOrderAccount.Text = "" Then
                        MsgBox("Please enter an order account", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf txtOrderRef.Text = "" Then
                        MsgBox("Please enter an order ref", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf Len(CboOrderName.Text) > 150 Then
                        MsgBox("The order name must be 150 characters Or less", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf Len(txtOrderAddress1.Text) > 150 Then
                        MsgBox("The order address 1 must be 150 characters Or less", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf Len(txtOrderAddress2.Text) > 150 Then
                        MsgBox("The order address 2 must be 150 characters Or less", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf Len(txtOrderZip.Text) > 20 Then
                        MsgBox("The order zip must be 20 characters Or less", vbInformation)
                        IsValidOrderDetails = False
                    ElseIf ClsPay.OrderAccountNumber = "" Then
                        MsgBox("The order account number must be populated in IMS for this order account (Free field 1)", vbInformation)
                        IsValidOrderDetails = False
                    End If
                End If
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPort) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                If CboOrderAccount.Text = "" Then
                    MsgBox("Please enter an order portfolio", vbInformation)
                    IsValidOrderDetails = False
                ElseIf CboOrderName.Text = "" Then
                    MsgBox("Please enter an order account", vbInformation)
                    IsValidOrderDetails = False
                ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPortLoan) And Not IsNumeric(txtOrderOther1.Text) Then
                    MsgBox("Please enter a valid percentage rate", vbInformation)
                    IsValidOrderDetails = False
                End If
            End If
        End If
    End Function

    Private Function IsValidBenDetails() As Boolean
        IsValidBenDetails = True
        If TabMovement.TabPages(1).Visible Then
            If ClsIMS.PaymentSendsSwift(CboPayType.Text) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExClientOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) And ClsPay.BeneficiaryAccountNumber = "" Then
                MsgBox("The beneficiary account number must be populated in IMS for this beneficiary account (Free field 1)", vbInformation)
                IsValidBenDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn)) And CboBenAccount.Text = "" Then
                MsgBox("Please enter an beneficiary account", vbInformation)
                IsValidBenDetails = False
            ElseIf ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
                If ClsPay.OtherID <> cPayDescInOtherIntTra And (CboBenName.Text = "" And txtbensurname.Text = "") Then
                    MsgBox("Please enter an beneficiary name", vbInformation)
                    IsValidBenDetails = False
                ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut)) And cboBenType.Text = "" Then
                    MsgBox("The beneficiary type must be entered", vbInformation)
                    IsValidBenDetails = False
                ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut)) And cbobenrelationship.Text = "" Then
                    MsgBox("The beneficiary relationship must be entered", vbInformation)
                    IsValidBenDetails = False
                ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut)) And cboBenCountry.Text = "" Then
                    MsgBox("The beneficiary country must be entered", vbInformation)
                    IsValidBenDetails = False
                ElseIf txtBenSwift.Text = "" And txtBenBankSwift.Text = "" Then
                    MsgBox("The beneficiary account swift must be entered", vbInformation)
                    IsValidBenDetails = False
                ElseIf txtBenIBAN.Text = "" And txtBenBankIBAN.Text = "" Then
                    MsgBox("The beneficiary account IBAN must be entered", vbInformation)
                    IsValidBenDetails = False
                ElseIf ClsPay.OtherID = cPayDescInOtherNoticetoReceive And txtBenOther1.Text = "" Then
                    MsgBox("Please enter associated reference of instrument this notice is being sent for", vbInformation)
                    IsValidBenDetails = False
                End If
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPort) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                If CboBenAccount.Text = "" Then
                    MsgBox("Please enter an beneficiary portfolio", vbInformation)
                    IsValidBenDetails = False
                ElseIf CboBenName.Text = "" Then
                    MsgBox("Please enter an beneficiary account", vbInformation)
                    IsValidBenDetails = False
                End If
            End If
        End If
    End Function

    Private Function IsValidBenSubBankDetails() As Boolean
        IsValidBenSubBankDetails = True
        If CboBenSubBankName.Text <> "" Then
            If Len(CboBenSubBankName.Text) > 150 Then
                MsgBox("The beneficiary intermediary bank name must be 150 characters Or less", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenSubBankAddress1.Text <> "" And Len(txtBenSubBankAddress1.Text) > 150 Then
                MsgBox("The beneficiary intermediary bank address 1 must be 150 characters Or less", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenSubBankAddress2.Text <> "" And Len(txtBenSubBankAddress2.Text) > 150 Then
                MsgBox("The beneficiary intermediary bank address 2 must be 150 characters Or less", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenSubBankZip.Text <> "" And Len(txtBenSubBankZip.Text) > 20 Then
                MsgBox("The beneficiary intermediary bank zip must be 20 characters Or less", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And Not ValidSwift(txtBenSubBankSwift.Text) Then
                MsgBox("The beneficiary intermediary bank swift must be valid And either 8 Or 11 characters", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenSubBankSwift.Text = "" Then
                MsgBox("The beneficiary intermediary bank swift must be entered", vbInformation)
                IsValidBenSubBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenSubBankOther1.Text <> "" And Len(txtBenSubBankOther1.Text) > 100 Then
                MsgBox("The beneficiary intermediary bank other field must be 100 characters Or less", vbInformation)
                IsValidBenSubBankDetails = False
            End If
        End If
    End Function

    Private Function IsValidBenBankDetails() As Boolean
        IsValidBenBankDetails = True
        If CboBenBankName.Text <> "" Then
            If Len(CboBenBankName.Text) > 150 Then
                MsgBox("The account with institution name must be 150 characters Or less", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenBankAddress1.Text <> "" And Len(txtBenBankAddress1.Text) > 150 Then
                MsgBox("The account with institution address 1 must be 150 characters Or less", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenBankAddress2.Text <> "" And Len(txtBenBankAddress2.Text) > 150 Then
                MsgBox("The account with institution address 2 must be 150 characters Or less", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenBankZip.Text <> "" And Len(txtBenBankZip.Text) > 20 Then
                MsgBox("The account with institution zip must be 20 characters Or less", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And (Not ValidSwift(txtBenBankSwift.Text) And Not ValidSwift(txtBenSwift.Text)) Then
                MsgBox("The account with institution swift must be valid And either 8 Or 11 characters", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And (txtBenBankSwift.Text = "" And txtBenSwift.Text = "") Then
                MsgBox("The account with institution swift must be entered", vbInformation)
                IsValidBenBankDetails = False
            ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And txtBenBankOther1.Text <> "" And Len(txtBenBankOther1.Text) > 100 Then
                MsgBox("The account with institution other field must be 100 characters Or less", vbInformation)
                IsValidBenBankDetails = False
            End If
        End If
    End Function

    Private Function IsValidExtraCheckDetails() As Boolean
        IsValidExtraCheckDetails = True
        If ClsPay.Status = ClsIMS.GetStatusType(cStatusIMSAck) Or ClsPay.Status = ClsIMS.GetStatusType(cStatusSwiftAck) Or ClsPay.Status = ClsIMS.GetStatusType(cStatusPaidAck) Then
            MsgBox("This transaction has already been processed and acknowledged by IMS/swift. You cannot edit it.", MsgBoxStyle.Exclamation)
            IsValidExtraCheckDetails = False
        ElseIf ClsPay.Status = ClsIMS.GetStatusType(cStatusSent) Then
            MsgBox("This transaction has already been sent. You cannot edit it.", MsgBoxStyle.Exclamation)
            IsValidExtraCheckDetails = False
        ElseIf ClsPay.Status = ClsIMS.GetStatusType(cStatusAwaitingIMS) Or ClsPay.Status = ClsIMS.GetStatusType(cStatusAwaitingSwift) Then
            MsgBox("This transaction is awaiting responses. You cannot edit it.", MsgBoxStyle.Exclamation)
            IsValidExtraCheckDetails = False
        ElseIf ClsPay.Status = ClsIMS.GetStatusType(cStatusCancelled) Then
            MsgBox("This transaction has been cancelled." & vbNewLine & "You cannot edit it but you can use the copy function to create a new transaction with the same details then re-send.", MsgBoxStyle.Exclamation)
            IsValidExtraCheckDetails = False
        ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And (ClsPay.PortfolioIsCommission) And txtIMSRef.Text = "" Then
            MsgBox("This transaction requires the IMS ref to be populated with a valid DocNo.", MsgBoxStyle.Exclamation)
            IsValidExtraCheckDetails = False
            'ElseIf (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn)) And CutOffTimePassed() Then
            'MsgBox("You have now passed the cut off time for this payment." & vbNewLine & "The cut off time for this broker was at " & ClsPay.CutOffTimeGMT & " GMT" & vbNewLine & "Please adjust the settlement date", MsgBoxStyle.Exclamation)
            'IsValidExtraCheckDetails = False
            'ElseIf PendingAmountMoreThanBalance() Then
            '    MsgBox("Pending amounts and transaction amount is greater than the available balance in the order account", MsgBoxStyle.Exclamation)
            '   IsValidExtraCheckDetails = False
        End If
    End Function

    Private Function IsValidRUBDetails() As Boolean
        IsValidRUBDetails = True
        If CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExIn) Then
            If ClsPay.CCY = "RUB" Then
                If cboOrderVOCode.Text = "" Then
                    MsgBox("Please enter a VO code for this russian payment", MsgBoxStyle.Exclamation)
                    IsValidRUBDetails = False
                End If
            End If
        End If
    End Function

    Private Function ValidatedForm() As Boolean
        ValidatedForm = True
        If Not IsValidExtraCheckDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidMovementDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidOrderDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidBenDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidBenSubBankDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidBenBankDetails() Then
            ValidatedForm = False
        ElseIf Not IsValidRUBDetails() Then
            ValidatedForm = False
        End If
    End Function

    Private Function CutOffTimePassed() As Boolean
        CutOffTimePassed = False
        If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
            If dtSettleDate.Value.Date = Now.Date Then
                ClsIMS.GetCutOff(ClsPay.OrderBrokerCode, ClsPay.CCYCode)
                If ClsPay.CutOffTime <> "" Then
                    If CDate(dtSettleDate.Value.Date & " " & ClsPay.CutOffTime) < ConvertTimeZone(ClsPay.CutOffTimeZone).DateTime Then
                        CutOffTimePassed = True
                    End If
                End If
            End If
        End If
    End Function

    Public Sub AddToolTips()
        MovementToolTip.SetToolTip(Me.LblPayType, "Select type of transaction (Internal Client to Client,Institutional Moves, Management fees, Pay out or receipts)")
        MovementToolTip.SetToolTip(Me.CboPayType, "Select type of transaction (Internal Client to Client,Institutional Moves, Management fees, Pay out or receipts)")

        Dim varTip As String = "Internal Client to Client - Does not send a swift - allows internal movement between clients within the same institution" & vbNewLine
        varTip = varTip & "Internal Institutional Account Move - Sends a swift message for accounts setup for the portfolio selected - From order to beneficiary ('BANK', 'PRIVATE BANKING' and 'Custodian' types can pay out)" & vbNewLine
        varTip = varTip & "Internal Management Fee - Sends a swift message for accounts setup for internal money movements between the client and the firm" & vbNewLine
        varTip = varTip & "External Client Account Move - Sends a swift message for accounts setup for the client selected - From client's portfolio to other other portfolio same ccy ('BANK', 'PRIVATE BANKING' and 'Custodian' types can pay out). Movement can be instructed by the client. " & vbNewLine
        varTip = varTip & "External Client Payment - Sends a swift message to pay an external entity i.e. bank, person, business (Only 'BANK' types can pay out). Similar to External payment but payment is instructed by the client" & vbNewLine
        varTip = varTip & "External Payment - Sends a swift message to pay an external entity i.e. bank, person, business (Only 'BANK' types can pay out)" & vbNewLine
        varTip = varTip & "External Receipt - Does not send a swift - Select for incoming funds into the selected portfolio"
        varTip = varTip & "Management Fee - Sends a swift - Select for payments between client to fund and visa versa"

        MovementToolTip.SetToolTip(Me.PicPayType, varTip)
        MovementToolTip.SetToolTip(Me.lblportfolio, "Select portfolio from IMS to move cash from/to")
        MovementToolTip.SetToolTip(Me.CboPortfolio, "Select portfolio from IMS to move cash from/to")
        MovementToolTip.SetToolTip(Me.lblcomments, "This comments section is used For internal purposes only. To send a message to the beneficiary, please use order other box")
        MovementToolTip.SetToolTip(Me.txtComments, "This comments section is used For internal purposes only. To send a message to the beneficiary, please use order other box")
        MovementToolTip.SetToolTip(Me.lblCCY, "Select currency of payment/receipt")
        MovementToolTip.SetToolTip(Me.CboCCY, "Select currency of payment/receipt")
        MovementToolTip.SetToolTip(Me.lblAmount, "Amount to be paid/received")
        MovementToolTip.SetToolTip(Me.txtAmount, "Amount to be paid/received")
        MovementToolTip.SetToolTip(Me.lblTransDate, "Date of transaction")
        MovementToolTip.SetToolTip(Me.dtTransDate, "Date of transaction")
        MovementToolTip.SetToolTip(Me.lblSettleDate, "Date transaction is to be settled")
        MovementToolTip.SetToolTip(Me.dtSettleDate, "Date transaction is to be settled")

        MovementToolTip.SetToolTip(Me.lblOrderAccount, "Payment out from this account")
        MovementToolTip.SetToolTip(Me.CboOrderAccount, "Payment out from this account")
        MovementToolTip.SetToolTip(Me.lblOrderName, "Payment out from this account name")
        MovementToolTip.SetToolTip(Me.CboOrderName, "Payment out from this account name")
        MovementToolTip.SetToolTip(Me.lblOrderBalance, "Balance available in the account selected")
        MovementToolTip.SetToolTip(Me.txtOrderBalance, "Balance available in the account selected")
        MovementToolTip.SetToolTip(Me.lblorderref, "Order Reference - This is auto-generated but you can edit it")
        MovementToolTip.SetToolTip(Me.txtOrderRef, "Order Reference - This is auto-generated but you can edit it")
        MovementToolTip.SetToolTip(Me.lblOrderAddress1, "Payment out first line address")
        MovementToolTip.SetToolTip(Me.txtOrderAddress1, "Payment out first line address")
        MovementToolTip.SetToolTip(Me.lblOrderAddress2, "Payment out second line address")
        MovementToolTip.SetToolTip(Me.txtOrderAddress2, "Payment out second line address")
        MovementToolTip.SetToolTip(Me.lblOrderZip, "Payment out post code/zip")
        MovementToolTip.SetToolTip(Me.txtOrderZip, "Payment out post code/zip")
        MovementToolTip.SetToolTip(Me.lblOrderSwift, "Ordering institution swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.txtOrderSwift, "Ordering institution swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.lblOrderIBAN, "Ordering institution IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.txtOrderIBAN, "Ordering institution IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.lblOrderOther1, "Remittance Information - message you want to send To beneficiary from the ordering party")
        MovementToolTip.SetToolTip(Me.txtOrderOther1, "Remittance Information - message you want to send To beneficiary from the ordering party")

        MovementToolTip.SetToolTip(Me.lblBenAccount, "Account to receive money")
        MovementToolTip.SetToolTip(Me.CboBenAccount, "Account to receive money")
        MovementToolTip.SetToolTip(Me.lblBenName, "Account name to receive money")
        MovementToolTip.SetToolTip(Me.CboBenName, "Account name to receive money")
        MovementToolTip.SetToolTip(Me.lblBenBalance, "Balance available in the receving account selected")
        MovementToolTip.SetToolTip(Me.txtBenBalance, "Balance available in the receving account selected")
        MovementToolTip.SetToolTip(Me.lblBenRef, "Beneficiary Reference for internal purposes only")
        MovementToolTip.SetToolTip(Me.txtBenRef, "Beneficiary Reference for internal purposes only")
        MovementToolTip.SetToolTip(Me.lblBenAddress1, "Receiving party first line address")
        MovementToolTip.SetToolTip(Me.txtBenAddress1, "Receiving party first line address")
        MovementToolTip.SetToolTip(Me.lblBenAddress2, "Receiving party second line address")
        MovementToolTip.SetToolTip(Me.txtBenAddress2, "Receiving party second line address")
        MovementToolTip.SetToolTip(Me.lblBenZip, "Receiving party post code/zip")
        MovementToolTip.SetToolTip(Me.txtBenZip, "Receiving party post code/zip")
        MovementToolTip.SetToolTip(Me.lblBenSwift, "Beneficiary institution swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.txtBenSwift, "Beneficiary institution swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.lblBenIBAN, "Beneficiary institution IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.txtBenIBAN, "Beneficiary institution IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.lblBenOther1, "Beneficiary free field for internal purposes only")
        MovementToolTip.SetToolTip(Me.txtBenOther1, "Beneficiary free field for internal purposes only")

        MovementToolTip.SetToolTip(Me.lblBenBankName, "intermediary bank name for money to be paid To")
        MovementToolTip.SetToolTip(Me.CboBenBankName, "intermediary bank name for money to be paid To")
        MovementToolTip.SetToolTip(Me.lblBenBankAddress1, "intermediary bank first line address")
        MovementToolTip.SetToolTip(Me.txtBenBankAddress1, "intermediary bank first line address")
        MovementToolTip.SetToolTip(Me.lblBenBankAddress2, "intermediary bank second line address")
        MovementToolTip.SetToolTip(Me.txtBenBankAddress2, "intermediary bank second line address")
        MovementToolTip.SetToolTip(Me.lblBenBankZip, "intermediary bank post code/zip")
        MovementToolTip.SetToolTip(Me.txtBenBankZip, "intermediary bank post code/zip")
        MovementToolTip.SetToolTip(Me.lblBenBankSwift, "intermediary bank swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.txtBenBankSwift, "intermediary bank swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.lblBenBankIBAN, "intermediary bank IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.txtBenBankIBAN, "intermediary bank IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.lblBenBankOther1, "intermediary bank free field for internal purposes only")
        MovementToolTip.SetToolTip(Me.txtBenBankOther1, "intermediary bank free field for internal purposes only")

        MovementToolTip.SetToolTip(Me.lblBenBankName, "Account with institution name for money to be paid To")
        MovementToolTip.SetToolTip(Me.CboBenBankName, "Account with institution bank name for money to be paid To")
        MovementToolTip.SetToolTip(Me.lblBenBankAddress1, "Account with institution bank first line address")
        MovementToolTip.SetToolTip(Me.txtBenBankAddress1, "Account with institution bank first line address")
        MovementToolTip.SetToolTip(Me.lblBenBankAddress2, "Account with institution bank second line address")
        MovementToolTip.SetToolTip(Me.txtBenBankAddress2, "Account with institution bank second line address")
        MovementToolTip.SetToolTip(Me.lblBenBankZip, "Account with institution bank post code/zip")
        MovementToolTip.SetToolTip(Me.txtBenBankZip, "Account with institution bank post code/zip")
        MovementToolTip.SetToolTip(Me.lblBenBankSwift, "Account with institution bank swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.txtBenBankSwift, "Account with institution bank swift code. This is restricted To 8 Or 11 characters, no spaces, BIC format only")
        MovementToolTip.SetToolTip(Me.lblBenBankIBAN, "Account with institution bank IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.txtBenBankIBAN, "Account with institution bank IBAN code. This is restricted To 34 characters")
        MovementToolTip.SetToolTip(Me.lblBenBankOther1, "Account with institution bank free field for internal purposes only")
        MovementToolTip.SetToolTip(Me.txtBenBankOther1, "Account with institution bank free field for internal purposes only")

        MovementToolTip.SetToolTip(Me.cmdRefresh, "Click To refresh cash movement grid for latest updates")
        MovementToolTip.SetToolTip(Me.cmdAddCash, "Click To add/edit current movement details on the form")
        MovementToolTip.SetToolTip(Me.dgvCash, "Cash movements grid which show transactions in 'Pending/Ready' status")
        MovementToolTip.SetToolTip(Me.cmdSubmit, "Send cash movements to appropriate destinations i.e. Swift/IMS")
    End Sub

    Private Sub ClearOrderMessages()
        lblordermsg1.Text = ""
        lblordermsg1.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg2.Text = ""
        lblordermsg2.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg3.Text = ""
        lblordermsg3.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg4.Text = ""
        lblordermsg4.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg5.Text = ""
        lblordermsg5.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg6.Text = ""
        lblordermsg6.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg7.Text = ""
        lblordermsg7.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblordermsg8.Text = ""
        lblordermsg8.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        TimerBroker.Enabled = False
    End Sub

    Private Sub ClearBenMessages()
        lblbenmsg1.Text = ""
        lblbenmsg1.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblbenmsg2.Text = ""
        lblbenmsg2.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
        lblbenmsg3.Text = ""
        lblbenmsg3.LinkArea = New System.Windows.Forms.LinkArea(0, 0)
    End Sub

    Private Function PopulateMessage(ByVal SelectedOrder As Boolean, ByVal Msg As String, ByVal GoodMsg As Boolean, ByVal LinkStart As Integer, ByVal LinkEnd As Integer, ByVal varLink As String) As LinkLabel
        PopulateMessage = lblordermsg1
        If SelectedOrder Then
            If lblordermsg1.Text = "" Then
                lblordermsg1.Text = Msg
                If varLink <> "" Then
                    lblordermsg1.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg1
                If GoodMsg Then
                    lblordermsg1.ForeColor = Color.DarkGreen
                Else
                    lblordermsg1.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg2.Text = "" Then
                lblordermsg2.Text = Msg
                If varLink <> "" Then
                    lblordermsg2.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg2
                If GoodMsg Then
                    lblordermsg2.ForeColor = Color.DarkGreen
                Else
                    lblordermsg2.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg3.Text = "" Then
                lblordermsg3.Text = Msg
                If varLink <> "" Then
                    lblordermsg3.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg3
                If GoodMsg Then
                    lblordermsg3.ForeColor = Color.DarkGreen
                Else
                    lblordermsg3.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg4.Text = "" Then
                lblordermsg4.Text = Msg
                If varLink <> "" Then
                    lblordermsg4.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg4
                If GoodMsg Then
                    lblordermsg4.ForeColor = Color.DarkGreen
                Else
                    lblordermsg4.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg5.Text = "" Then
                lblordermsg5.Text = Msg
                If varLink <> "" Then
                    lblordermsg5.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg5
                If GoodMsg Then
                    lblordermsg5.ForeColor = Color.DarkGreen
                Else
                    lblordermsg5.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg6.Text = "" Then
                lblordermsg6.Text = Msg
                If varLink <> "" Then
                    lblordermsg6.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg6
                If GoodMsg Then
                    lblordermsg6.ForeColor = Color.DarkGreen
                Else
                    lblordermsg6.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg7.Text = "" Then
                lblordermsg7.Text = Msg
                If varLink <> "" Then
                    lblordermsg7.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg7
                If GoodMsg Then
                    lblordermsg7.ForeColor = Color.DarkGreen
                Else
                    lblordermsg7.ForeColor = Color.DarkRed
                End If
            ElseIf lblordermsg8.Text = "" Then
                lblordermsg8.Text = Msg
                If varLink <> "" Then
                    lblordermsg8.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblordermsg8
                If GoodMsg Then
                    lblordermsg8.ForeColor = Color.DarkGreen
                Else
                    lblordermsg8.ForeColor = Color.DarkRed
                End If
            End If
        Else
            If lblbenmsg1.Text = "" Then
                lblbenmsg1.Text = Msg
                If varLink <> "" Then
                    lblbenmsg1.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblbenmsg1
                If GoodMsg Then
                    lblbenmsg1.ForeColor = Color.DarkGreen
                Else
                    lblbenmsg1.ForeColor = Color.DarkRed
                End If
            ElseIf lblbenmsg2.Text = "" Then
                lblbenmsg2.Text = Msg
                If varLink <> "" Then
                    lblbenmsg2.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblbenmsg2
                If GoodMsg Then
                    lblbenmsg2.ForeColor = Color.DarkGreen
                Else
                    lblbenmsg2.ForeColor = Color.DarkRed
                End If
            ElseIf lblbenmsg3.Text = "" Then
                lblbenmsg3.Text = Msg
                If varLink <> "" Then
                    lblbenmsg3.Links.Add(LinkStart, LinkEnd, varLink)
                End If
                PopulateMessage = lblbenmsg3
                If GoodMsg Then
                    lblbenmsg3.ForeColor = Color.DarkGreen
                Else
                    lblbenmsg3.ForeColor = Color.DarkRed
                End If
            End If
        End If
    End Function

    Public Sub EditPayment()

        loadEditForm = True
        ToggleAntiFlicker(True)

        Dim varCustId As Integer = ClsPay.CustomerId
        Dim varCustName As String = ClsPay.CustomerName
        Dim varPaymentType As String = ClsPay.PaymentType
        Dim varPortfolioCode As Integer = ClsPay.PortfolioCode
        Dim varPortfolio As String = ClsPay.Portfolio
        Dim varBenPortfolioCode As Integer = ClsPay.BeneficiaryPortfolioCode
        Dim varBenPortfolio As String = ClsPay.BeneficiaryPortfolio
        Dim varCCYCode As Integer = ClsPay.CCYCode
        Dim varCCY As String = ClsPay.CCY
        Dim varOrderBrokerCode As Integer = ClsPay.OrderBrokerCode
        Dim varBenBrokerCode As Integer = ClsPay.BeneficiaryBrokerCode
        Dim varOrderAccountCode As Integer = ClsPay.OrderAccountCode
        Dim varBenAccountCode As Integer = ClsPay.BeneficiaryAccountCode
        Dim varOrderAccount As String = ClsPay.OrderAccount
        Dim varOrderAccountNumber As String = ClsPay.OrderAccountNumber
        Dim varBenAccount As String = ClsPay.BeneficiaryAccount
        Dim varOrderName As String = ClsPay.OrderName
        Dim varBenName As String = ClsPay.BeneficiaryName
        Dim varBenSubBankName As String = ClsPay.BeneficiarySubBankName
        Dim varBenBankName As String = ClsPay.BeneficiaryBankName
        Dim varBenCCYCode As Integer = ClsPay.BeneficiaryCCYCode
        Dim varBenCCY As String = ClsPay.BeneficiaryCCY
        Dim varBeneficiaryTemplateName As String = ClsPay.BeneficiaryTemplateName
        Dim varOtherID As Integer = ClsPay.OtherID
        Dim varOrderVOCodeID As Integer = ClsPay.OrderVOCodeID
        Dim varIMSRef As String = ClsPay.IMSRef
        Dim varPaymentRequested As Boolean = ClsPay.PaymentRequested
        Dim varEmailRequest As String = ClsPay.PaymentRequestedEmail
        Dim varRequestBeneficiaryName As String = ClsPay.PaymentRequestedBeneficiaryName
        Dim varPaymentRequestedRate As Double = ClsPay.PaymentRequestedRate
        Dim varExchangeRate As Double = ClsPay.ExchangeRate
        Dim varUserLocationCodeStr As String = ClsIMS.GetSQLItem("select BranchCode from vwdolfinpaymentclientportfolioall where pf_code = " & varPortfolioCode)
        Dim varUserLocationCode As Integer = ClsIMS.UserLocationCode

        If IsNumeric(varUserLocationCodeStr) Then
            varUserLocationCode = CInt(varUserLocationCodeStr)
        End If

        varBeneficiaryAddressID = ClsPay.BeneficiaryAddressID
        varBeneficiarySubBankAddressID = ClsPay.BeneficiarySubBankAddressID
        varBeneficiaryBankAddressID = ClsPay.BeneficiaryBankAddressID
        ClsPay.SelectedTemplateName = ClsPay.BeneficiaryTemplateName

        If ClsPay.SelectedPaymentID <> 0 Then
            txtID.Text = "#" & ClsPay.SelectedPaymentID
        Else
            txtID.Text = ""
        End If

        CboPayType.SelectedIndex = -1
        CboPayType.SelectedIndex = ClsPay.PopulatePayOptions(varPaymentType)
        CboPayType.Text = varPaymentType
        ClsPay.PaymentType = CboPayType.Text

        ClsIMS.PopulateFormCombo(CboPortfolio, varCCYCode, varPortfolioCode,, varUserLocationCode)

        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
            (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And varOtherID <> cPayDescInOtherNoticetoReceive And varOtherID <> cPayDescInOtherStatementRequest) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                CboPortfolio.Text = ClsIMS.GetPaymentOtherType(varOtherID)
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                CboPortfolio.SelectedValue = varCustId
                CboPortfolio.Text = varCustName
            Else
                CboPortfolio.SelectedValue = -1
                CboPortfolio.Text = ""
            End If

            ClsIMS.PopulateFormCombo(CboCCY, varPortfolioCode,,, varUserLocationCode)
            CboCCY.SelectedValue = varCCYCode
            CboCCY.Text = varCCY
            ClsIMS.PopulateFormCombo(cboBenCCY, varPortfolioCode,,, varUserLocationCode)
            cboBenCCY.SelectedValue = varBenCCYCode
            cboBenCCY.Text = varBenCCY

            If varOtherID = cPayDescInOtherFreeText199 Or varOtherID = cPayDescInOtherFreeText599 Or varOtherID = cPayDescInOtherFreeText999 Or varOtherID = cPayDescInOtherCancelPayment192 Then
                GrpSendMessage.Visible = True
                ClsIMS.PopulateComboboxWithData(cboSendTo, "select 0,sc_swiftcode from vwdolfinpaymentswiftcodes where sc_source not in ('UPLOAD') group by sc_swiftcode order by sc_swiftcode")
                txtEmailRequest.ReadOnly = False
                CboCCY.Enabled = False
                txtAmount.Enabled = False
                txtComments.Enabled = False
                TabMovement.SelectedTab = RequestTab
            ElseIf varOtherID = cPayDescInOtherBankCharges Then
                ClsPay.SelectedOrder = 1
                ClsPay.PortfolioCode = varPortfolioCode
                ClsPay.OrderAccountCode = varOrderAccountCode
                ClsPay.OrderBrokerCode = varOrderBrokerCode
                CboOrderAccount.SelectedValue = varPortfolioCode
                CboOrderAccount.Text = varPortfolio
                CboOrderName.SelectedValue = varOrderAccountCode
                CboOrderName.Text = varOrderAccount
                PopulateExtraFormDetails()
                TabMovement.SelectedTab = OrderTab
            Else
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, varBenCCYCode, varCustId, varOrderAccountCode, varUserLocationCode)
                    ClsIMS.PopulateFormCombo(CboBenName, varBenCCYCode, varCustId, varBenAccountCode, varUserLocationCode)
                Else
                    ClsIMS.PopulateFormCombo(CboBenAccount, varBenCCYCode, CboOrderName.SelectedValue, CboOrderAccount.SelectedValue, varUserLocationCode)
                    ClsIMS.PopulateFormCombo(CboBenName, varBenCCYCode, varBenAccountCode,, varUserLocationCode)
                End If


                ClsPay.SelectedOrder = 1
                ClsPay.PortfolioCode = varPortfolioCode
                ClsPay.OrderAccountCode = varOrderAccountCode
                ClsPay.OrderBrokerCode = varOrderBrokerCode
                CboOrderAccount.SelectedValue = varPortfolioCode
                CboOrderAccount.Text = varPortfolio
                CboOrderName.SelectedValue = varOrderAccountCode
                CboOrderName.Text = varOrderAccount

                PopulateExtraFormDetails()

                CboBenAccount.SelectedValue = varBenPortfolioCode
                ClsPay.PortfolioCode = varBenPortfolioCode
                CboBenAccount.Text = varBenPortfolio


                ClsPay.OrderAccountCode = -1
                ClsPay.BeneficiaryAccountCode = varBenAccountCode
                ClsPay.BeneficiaryBrokerCode = varBenBrokerCode
                ClsPay.SelectedOrder = 2

                CboBenName.SelectedValue = varBenAccountCode
                CboBenName.Text = varBenAccount

                PopulateExtraFormDetails()
                TabMovement.SelectedTab = OrderTab
            End If

        ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
            CboPortfolio.SelectedValue = varPortfolioCode
            CboPortfolio.Text = varPortfolio
            ClsIMS.PopulateFormCombo(CboCCY, varPortfolioCode,,, varUserLocationCode)
            CboCCY.SelectedValue = varCCYCode
            CboCCY.Text = varCCY
            ClsIMS.PopulateFormCombo(cboBenCCY, varPortfolioCode,,, varUserLocationCode)
            cboBenCCY.SelectedValue = varBenCCYCode
            cboBenCCY.Text = varBenCCY
            ClsIMS.PopulateFormCombo(CboBenAccount, varPortfolioCode, varCCYCode,, varUserLocationCode)
            CboBenAccount.SelectedValue = varBenAccountCode
            CboBenAccount.Text = varBenAccount
            TabMovement.SelectedTab = BeneficiaryTab
        Else
            CboPortfolio.SelectedValue = varPortfolioCode
            CboPortfolio.Text = varPortfolio
            ClsIMS.PopulateFormCombo(CboCCY, varPortfolioCode,,, varUserLocationCode)
            CboCCY.SelectedValue = varCCYCode
            CboCCY.Text = varCCY
            ClsIMS.PopulateFormCombo(cboBenCCY, varPortfolioCode,,, varUserLocationCode)
            cboBenCCY.SelectedValue = varBenCCYCode
            cboBenCCY.Text = varBenCCY

            If varCCYCode <> varBenCCYCode Then
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                lblExchangeRate.Visible = False
                lblExchangeRate.Text = "@ " & ClsPay.ExchangeRate.ToString() & " Exchange Rate"
            End If

            PopulateFormFromPortfolio(varUserLocationCode)

            ClsPay.OrderAccountCode = varOrderAccountCode
            CboOrderAccount.SelectedValue = varOrderAccountCode
            CboOrderAccount.Text = varOrderAccount
            CboOrderName.Text = varOrderName
            TabMovement.SelectedTab = OrderTab
            TabMovement.SelectedTab = BeneficiaryTab

            lblExchangeRate.Text = ""
            txtOrderAddress1.Text = ClsPay.OrderAddress1
            txtOrderAddress2.Text = ClsPay.OrderAddress2
            txtOrderZip.Text = ClsPay.OrderZip
            cboOrderCountry.SelectedValue = ClsPay.OrderCountryID
            cboOrderCountry.Text = ClsPay.OrderCountry
            txtOrderSwift.Text = ClsPay.OrderSwift
            txtOrderIBAN.Text = ClsPay.OrderIBAN

            If CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                ChkBenTemplateEdit.Checked = False
                ClsIMS.PopulateComboboxWithData(CboBenAccount, "select 0,'' as Pa_TemplateName union select 1,Pa_TemplateName from vwDolfinPaymentAddress where Pa_PortfolioCode = " & varPortfolioCode & " and Pa_CCYCode = " & varBenCCYCode & " group by Pa_TemplateName order by Pa_TemplateName")
            End If

            If varBenBrokerCode = 0 And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExClientOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                CboBenAccount.Text = ""
                CboBenAccount.SelectedValue = -1
                CboBenAccount.Text = ""
            End If

            ClsPay.BeneficiaryAccountCode = varBenAccountCode
            CboBenAccount.SelectedValue = varBenAccountCode
            CboBenAccount.Text = varBenAccount
            CboBenName.Text = varBenName

            cboBenType.SelectedValue = ClsPay.BeneficiaryTypeID
            cboBenType.Text = ClsPay.BeneficiaryType
            cbobenrelationship.SelectedValue = ClsPay.BeneficiaryRelationshipID
            cbobenrelationship.Text = ClsPay.BeneficiaryRelationship

            txtbenfirstname.Text = ClsPay.BeneficiaryFirstName
            txtbenmiddlename.Text = ClsPay.BeneficiaryMiddleName
            txtbensurname.Text = ClsPay.BeneficiarySurname
            txtBenAddress1.Text = ClsPay.BeneficiaryAddress1
            txtBenAddress2.Text = ClsPay.BeneficiaryAddress2
            txtbencounty.Text = ClsPay.BeneficiaryCounty
            txtBenZip.Text = ClsPay.BeneficiaryZip
            cboBenCountry.SelectedValue = ClsPay.BeneficiaryCountryID
            cboBenCountry.Text = ClsPay.BeneficiaryCountry
            txtBenSwift.Text = ClsPay.BeneficiarySwift
            txtBenIBAN.Text = ClsPay.BeneficiaryIBAN
            txtBenOther1.Text = ClsPay.BeneficiaryOther
            txtBenBIK.Text = ClsPay.BeneficiaryBIK
            txtBenINN.Text = ClsPay.BeneficiaryINN


            If varBenBankName <> "" Then
                CboBenBankName.Text = varBenBankName
                txtBenBankAddress1.Text = ClsPay.BeneficiaryBankAddress1
                txtBenBankAddress2.Text = ClsPay.BeneficiaryBankAddress2
                txtBenBankZip.Text = ClsPay.BeneficiaryBankZip
                cboBenBankCountry.SelectedValue = ClsPay.BeneficiaryBankCountryID
                cboBenBankCountry.Text = ClsPay.BeneficiaryBankCountry
                txtBenBankSwift.Text = ClsPay.BeneficiaryBankSwift
                txtBenBankIBAN.Text = ClsPay.BeneficiaryBankIBAN
                txtBenBankOther1.Text = ClsPay.BeneficiaryBankOther
                txtBenBankBIK.Text = ClsPay.BeneficiaryBankBIK
                txtBenBankINN.Text = ClsPay.BeneficiaryBankINN
            End If

            If varBeneficiaryTemplateName <> "" Then
                FormControlsEnable(2, False)
                FormControlsEnable(3, False)
                FormControlsEnable(4, False)
                varBeneficiaryAddressID = ClsPay.BeneficiaryAddressID
                varBeneficiarySubBankAddressID = ClsPay.BeneficiarySubBankAddressID
                varBeneficiaryBankAddressID = ClsPay.BeneficiaryBankAddressID
                ClsPay.BeneficiaryTemplateName = ""
            End If

            ChkBenUrgent.Checked = ClsPay.SwiftUrgent
            ChkBenSendRef.Checked = ClsPay.SwiftSendRef

            If varOtherID = cPayDescInOtherNoticetoReceive Or varOtherID = cPayDescInOtherStatementRequest Then
                CboPortfolio.Text = ClsIMS.GetPaymentOtherType(varOtherID)
                CboCCY.SelectedValue = varCCYCode
                CboCCY.Text = varCCY
                cboBenCCY.SelectedValue = varBenCCYCode
                cboBenCCY.Text = varBenCCY
                ClsPay.PortfolioCode = varBenPortfolioCode
                ClsPay.BeneficiaryCCYCode = varBenCCYCode
                ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.CCYCode, varBenPortfolioCode,, varUserLocationCode)
                CboBenAccount.SelectedValue = varBenPortfolioCode
                CboBenAccount.Text = varBenPortfolio

                ClsIMS.PopulateFormCombo(CboBenName, ClsPay.BeneficiaryCCYCode, CboBenAccount.SelectedValue,, varUserLocationCode)
                ClsPay.OrderAccountCode = -1
                ClsPay.SelectedOrder = 2
                ClsPay.BeneficiaryAccountCode = varBenAccountCode
                ClsPay.BeneficiaryBrokerCode = varBenBrokerCode
                PopulateExtraFormDetails()
                CboBenName.SelectedValue = varBenAccountCode
                CboBenName.Text = varBenAccount
                TabMovement.SelectedTab = BeneficiaryTab
                lblBenBalanceInc.Visible = False
                txtBenBalanceInc.Visible = False
                lblBenBalancePostInc.Visible = False
                txtBenBalancePostInc.Visible = False
                lblBenOther1.Text = "Ref:"
                txtBenOther1.Enabled = True
                txtBenOther1.Text = ClsPay.BeneficiaryRef
            Else
                TabMovement.SelectedTab = OrderTab
            End If
        End If

        If varPaymentRequested Then
            txtEmailRequest.Text = varEmailRequest
            'CboBenName.Text = varRequestBeneficiaryName
        End If

        If varBenSubBankName <> "" Then
            ChkBenSubBank.Checked = True
            CboBenSubBankName.Text = varBenSubBankName
            txtBenSubBankAddress1.Text = ClsPay.BeneficiarySubBankAddress1
            txtBenSubBankAddress2.Text = ClsPay.BeneficiarySubBankAddress2
            txtBenSubBankZip.Text = ClsPay.BeneficiarySubBankZip
            cboBenSubBankCountry.SelectedValue = ClsPay.BeneficiarySubBankCountryID
            cboBenSubBankCountry.Text = ClsPay.BeneficiarySubBankCountry
            txtBenSubBankSwift.Text = ClsPay.BeneficiarySubBankSwift
            txtBenSubBankIBAN.Text = ClsPay.BeneficiarySubBankIBAN
            txtBenSubBankOther1.Text = ClsPay.BeneficiarySubBankOther
            txtBenSubBankBIK.Text = ClsPay.BeneficiarySubBankBIK
            txtBenSubbankINN.Text = ClsPay.BeneficiarySubBankINN
        End If

        If ClsIMS.PaymentSendsSwift(CboPayType.Text) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExClientOut) And CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            If CboBenName.Text <> "" And txtBenSubBankSwift.Text <> "" Then
                BeneficiaryGroupBankGroupBox.Visible = True
                BeneficiarySubBankGroupBox.Visible = True
                BeneficiaryBankGroupBox.Visible = False
                ChkBenSubBank.Checked = True
                ChkBenSubBank.Enabled = False
                FormControlsEnable(3, False)
            Else
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
            End If
        End If

        ClsPay.BeneficiaryAddressID = varBeneficiaryAddressID
        ClsPay.BeneficiarySubBankAddressID = varBeneficiarySubBankAddressID
        ClsPay.BeneficiaryBankAddressID = varBeneficiaryBankAddressID
        cboOrderVOCode.SelectedValue = varOrderVOCodeID
        ClsPay.OrderAccountNumber = varOrderAccountNumber

        dtTransDate.Text = ClsPay.TransDate
        dtSettleDate.Text = ClsPay.SettleDate

        If IsNumeric(ClsPay.Amount) Then
            txtAmount.Text = CDbl(ClsPay.Amount).ToString("#,##0.00")
        End If

        txtOrderRef.Text = ClsPay.OrderRef
        txtBenRef.Text = ClsPay.BeneficiaryRef

        txtOrderOther1.Text = ClsPay.OrderOther
        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
            txtOrderOther1.Text = varPaymentRequestedRate
        End If

        If ClsPay.OtherID = cPayDescInOtherFreeText199 Or ClsPay.OtherID = cPayDescInOtherFreeText599 Or ClsPay.OtherID = cPayDescInOtherFreeText999 Or ClsPay.OtherID = cPayDescInOtherCancelPayment192 Then
            txtEmailRequest.Text = ClsPay.Comments
            cboSendTo.Text = ClsPay.OrderOther
            txtRelatedRef.Text = varIMSRef
        Else
            txtComments.Text = ClsPay.Comments
            txtIMSRef.Text = varIMSRef
        End If

        ClsPay.ExchangeRate = varExchangeRate
        txtExchRate.Text = varExchangeRate
        ShowExchangeRate()

        If varBeneficiaryTemplateName <> "" Then
            ChkBenTemplateEdit.Visible = True
            ChkBenTemplateEdit.Checked = False
        End If

        ClsPay.DeleteFileDirectory(TempDirectory)
        RefreshFiles()

        ClsPay.OrderBalance = ClsIMS.GetBalance(varPortfolioCode, varOrderAccountCode)
        ClsPay.BeneficiaryBalance = ClsIMS.GetBalance(varBenPortfolioCode, varBenAccountCode)

        SelectImageSWIFTIBAN(2, "swift", txtBenSwift, PicBenSwift)
        SelectImageSWIFTIBAN(2, "iban", txtBenIBAN, PicBenIBAN)
        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
        SelectImageSWIFTIBAN(3, "iban", txtBenSubBankIBAN, PicBenSubBankIBAN)
        SelectImageSWIFTIBAN(4, "swift", txtBenBankSwift, PicBenBankSwift)
        SelectImageSWIFTIBAN(4, "iban", txtBenBankIBAN, PicBenBankIBAN)

        loadEditForm = False

        Changeform(False)
        ToggleAntiFlicker(False)

    End Sub

    Private Sub cmdAddCash_Click(sender As Object, e As EventArgs) Handles cmdAddCash.Click
        If ValidatedForm() Then
            ClsPay.PaymentType = CboPayType.Text
            ClsPay.PortfolioCode = CboPortfolio.SelectedValue
            ClsPay.Portfolio = CboPortfolio.Text
            ClsPay.TransDate = dtTransDate.Text
            ClsPay.SettleDate = dtSettleDate.Text
            ClsPay.Amount = txtAmount.Text
            ClsPay.CCYCode = CboCCY.SelectedValue
            ClsPay.CCY = CboCCY.Text
            ClsPay.BeneficiaryCCYCode = cboBenCCY.SelectedValue
            ClsPay.BeneficiaryCCY = cboBenCCY.Text
            ClsPay.OrderAccountCode = CboOrderAccount.SelectedValue
            ClsPay.OrderAccount = CboOrderAccount.Text
            ClsPay.OrderName = CboOrderName.Text
            ClsPay.OrderRef = txtOrderRef.Text
            ClsPay.OrderAddress1 = txtOrderAddress1.Text
            ClsPay.OrderAddress2 = txtOrderAddress2.Text
            ClsPay.OrderZip = txtOrderZip.Text
            ClsPay.OrderSwift = txtOrderSwift.Text
            ClsPay.OrderIBAN = txtOrderIBAN.Text

            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                ClsPay.PaymentRequestedRate = IIf(IsNumeric(txtOrderOther1.Text), txtOrderOther1.Text, 0)
            Else
                ClsPay.OrderOther = txtOrderOther1.Text
            End If

            ClsPay.OrderVOCodeID = cboOrderVOCode.SelectedValue
            If IsNumeric(CboBenAccount.SelectedValue) Then
                ClsPay.BeneficiaryAccountCode = CboBenAccount.SelectedValue
            Else
                ClsPay.BeneficiaryAccountCode = 0
            End If
            ClsPay.BeneficiaryAccount = CboBenAccount.Text
            ClsPay.BeneficiaryRelationshipID = cbobenrelationship.SelectedValue
            ClsPay.BeneficiaryTypeID = cboBenType.SelectedValue

            If cboBenType.SelectedValue = 0 Then
                ClsPay.BeneficiaryName = ""
                ClsPay.BeneficiaryFirstName = txtbenfirstname.Text
                ClsPay.BeneficiaryMiddleName = txtbenmiddlename.Text
                ClsPay.BeneficiarySurname = txtbensurname.Text
            Else
                ClsPay.BeneficiaryName = CboBenName.Text
                ClsPay.BeneficiaryFirstName = ""
                ClsPay.BeneficiaryMiddleName = ""
                ClsPay.BeneficiarySurname = ""
            End If

            If txtBenRef.Text = "" Then
                ClsPay.BeneficiaryRef = ClsPay.OrderRef
            Else
                ClsPay.BeneficiaryRef = txtBenRef.Text
            End If

            ClsPay.BeneficiaryAddress1 = txtBenAddress1.Text
            ClsPay.BeneficiaryAddress2 = txtBenAddress2.Text
            ClsPay.BeneficiaryCounty = txtbencounty.Text
            ClsPay.BeneficiaryZip = txtBenZip.Text
            ClsPay.BeneficiarySwift = txtBenSwift.Text
            ClsPay.BeneficiaryIBAN = txtBenIBAN.Text
            ClsPay.BeneficiaryOther = txtBenOther1.Text
            ClsPay.BeneficiaryCountryID = cboBenCountry.SelectedValue
            ClsPay.BeneficiaryCountry = cboBenCountry.Text

            If CboBenSubBankName.Text = "" Then
                ClsPay.BeneficiarySubBankAddressID = 0
                ClsPay.BeneficiarySubBankName = ""
                ClsPay.BeneficiarySubBankAddress1 = ""
                ClsPay.BeneficiarySubBankAddress2 = ""
                ClsPay.BeneficiarySubBankZip = ""
                ClsPay.BeneficiarySubBankSwift = ""
                ClsPay.BeneficiarySubBankIBAN = ""
                ClsPay.BeneficiarySubBankOther = ""
                ClsPay.BeneficiarySubBankBIK = ""
                ClsPay.BeneficiarySubBankINN = ""
                ClsPay.BeneficiarySubBankCountryID = 0
                ClsPay.BeneficiarySubBankCountry = ""
            Else
                ClsPay.BeneficiarySubBankName = CboBenSubBankName.Text
                ClsPay.BeneficiarySubBankAddress1 = txtBenSubBankAddress1.Text
                ClsPay.BeneficiarySubBankAddress2 = txtBenSubBankAddress2.Text
                ClsPay.BeneficiarySubBankZip = txtBenSubBankZip.Text
                ClsPay.BeneficiarySubBankSwift = txtBenSubBankSwift.Text
                ClsPay.BeneficiarySubBankIBAN = txtBenSubBankIBAN.Text
                ClsPay.BeneficiarySubBankOther = txtBenSubBankOther1.Text
                ClsPay.BeneficiarySubBankBIK = txtBenSubBankBIK.Text
                ClsPay.BeneficiarySubBankINN = txtBenSubbankINN.Text
                ClsPay.BeneficiarySubBankCountryID = cboBenSubBankCountry.SelectedValue
                ClsPay.BeneficiarySubBankCountry = cboBenSubBankCountry.Text
            End If

            If CboBenBankName.Text = "" Then
                ClsPay.BeneficiaryBankAddressID = 0
                ClsPay.BeneficiaryBankName = ""
                ClsPay.BeneficiaryBankAddress1 = ""
                ClsPay.BeneficiaryBankAddress2 = ""
                ClsPay.BeneficiaryBankZip = ""
                ClsPay.BeneficiaryBankSwift = ""
                ClsPay.BeneficiaryBankIBAN = ""
                ClsPay.BeneficiaryBankOther = ""
                ClsPay.BeneficiaryBankBIK = ""
                ClsPay.BeneficiaryBankINN = ""
                ClsPay.BeneficiaryBankCountryID = 0
                ClsPay.BeneficiaryBankCountry = ""
            Else
                ClsPay.BeneficiaryBankName = CboBenBankName.Text
                ClsPay.BeneficiaryBankAddress1 = txtBenBankAddress1.Text
                ClsPay.BeneficiaryBankAddress2 = txtBenBankAddress2.Text
                ClsPay.BeneficiaryBankZip = txtBenBankZip.Text
                ClsPay.BeneficiaryBankSwift = txtBenBankSwift.Text
                ClsPay.BeneficiaryBankIBAN = txtBenBankIBAN.Text
                ClsPay.BeneficiaryBankOther = txtBenBankOther1.Text
                ClsPay.BeneficiaryBankBIK = txtBenBankBIK.Text
                ClsPay.BeneficiaryBankINN = txtBenBankINN.Text
                ClsPay.BeneficiaryBankCountryID = cboBenBankCountry.SelectedValue
                ClsPay.BeneficiaryBankCountry = cboBenBankCountry.Text
            End If

            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                ClsPay.PortfolioCode = CboOrderAccount.SelectedValue
                ClsPay.Portfolio = CboOrderAccount.Text
                ClsPay.OrderAccountCode = CboOrderName.SelectedValue
                ClsPay.OrderAccount = CboOrderName.Text
                ClsPay.OrderName = ""

                If IsNumeric(CboPortfolio.SelectedValue) Then
                    If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                        ClsPay.OtherID = CboPortfolio.SelectedValue
                    End If
                End If

                If Not ClsPay.OrderFormData Is Nothing Then
                    If ClsPay.OrderFormData.ContainsKey("B_Name1") Then
                        ClsPay.OrderName = ClsPay.OrderFormData.Item("B_Name1").ToString
                    End If
                End If
                ClsPay.BeneficiaryPortfolioCode = CboBenAccount.SelectedValue
                ClsPay.BeneficiaryPortfolio = CboBenAccount.Text
                ClsPay.BeneficiaryAccountCode = CboBenName.SelectedValue
                ClsPay.BeneficiaryAccount = CboBenName.Text
                ClsPay.BeneficiaryName = ""
                If Not ClsPay.BeneficiaryFormData Is Nothing Then
                    If ClsPay.BeneficiaryFormData.ContainsKey("B_Name1") Then
                        ClsPay.BeneficiaryName = ClsPay.BeneficiaryFormData.Item("B_Name1").ToString
                    End If
                End If
            End If

            ClsPay.Comments = txtComments.Text
            ClsPay.SwiftUrgent = ChkBenUrgent.Checked
            ClsPay.SwiftSendRef = ChkBenSendRef.Checked

            If ClsPay.PortfolioFeeCCY Is Nothing Then
                ClsPay.PortfolioFeeCCY = ""
            End If

            If ClsPay.PortfolioFeeCCYInt Is Nothing Then
                ClsPay.PortfolioFeeCCYInt = ""
            End If

            If ClsPay.SendSwift And ClsIMS.IsAboveThreshold(ClsPay.Amount, ClsPay.CCYCode) Then
                ClsPay.AboveThreshold = True
            Else
                ClsPay.AboveThreshold = False
            End If

            ClsPay.IMSRef = txtIMSRef.Text

            If (ClsPay.OtherID = cPayDescInOtherNoticetoReceive Or ClsPay.OtherID = cPayDescInOtherStatementRequest) And txtBenOther1.Text <> "" Then
                ClsPay.BeneficiaryRef = txtBenOther1.Text
            ElseIf ClsPay.OtherID = cPayDescInOtherFreeText199 Or ClsPay.OtherID = cPayDescInOtherFreeText599 Or ClsPay.OtherID = cPayDescInOtherFreeText999 Or ClsPay.OtherID = cPayDescInOtherCancelPayment192 Then
                ClsPay.OrderOther = cboSendTo.Text
                ClsPay.IMSRef = txtRelatedRef.Text
                ClsPay.Comments = txtEmailRequest.Text
                ClsPay.SendSwift = True
                ClsPay.SendIMS = False
            End If

            'If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
            'OpenTransferFeeConfirm()
            'End If

            If ChkBenTemplate.Checked Then
                ClsPay.BeneficiaryAddressID = 0
                ClsPay.BeneficiarySubBankAddressID = 0
                ClsPay.BeneficiaryBankAddressID = 0
            ElseIf ClsPay.SelectedTemplateName <> "" And ChkBenTemplateEdit.Checked Then
                If Not IsNumeric(ClsPay.SelectedTemplateName) Then
                    ClsPay.BeneficiaryTemplateName = ClsPay.SelectedTemplateName
                End If
            End If

            If ClsPay.OtherID = cPayDescInOtherFXBuy Or ClsPay.OtherID = cPayDescInOtherFXSell Then
                ClsPay.ExchangeRate = txtExchRate.Text
            End If

            If ClsPay.SelectedTransferFeeOption <> 3 Then
                If ClsPay.SelectedPaymentID <> 0 Then
                    ClsPay.ConfirmDirectory()
                    ClsIMS.UpdatePaymentTable(2)
                    ClsPay.SelectedPaymentOption = 4
                    Changeform(True)
                Else
                    ClsIMS.UpdatePaymentTable(1)
                    ClsPay.ConfirmDirectory()
                    Dim FrmOption As New FrmPaymentOption
                    FrmOption.ShowDialog()
                End If

                SelectPaymentOption()
                ClsIMS.RefreshPaymentGrid(dgvCash)
            End If
        End If
    End Sub

    Public Sub Changeform(ByVal varAdd As Boolean)
        ClsPay.DeleteFileDirectory(TempDirectory)
        TempDirectory = ""
        If varAdd Then
            cmdAddCash.Text = "Add Movement"
            lblpaymentTitle.Text = "Cash Movements"
            cmdRefresh.Text = "Refresh Movement Grid"
            Me.BackColor = Color.LightSteelBlue
            txtID.BackColor = Color.LightSteelBlue
            txtOrderBalance.BackColor = Color.LightSteelBlue
            txtOrderBalanceInc.BackColor = Color.LightSteelBlue
            txtOrderBalancePostInc.BackColor = Color.LightSteelBlue
            txtBenBalance.BackColor = Color.LightSteelBlue
            txtBenBalanceInc.BackColor = Color.LightSteelBlue
            txtBenBalancePostInc.BackColor = Color.LightSteelBlue
            TabMovement.BackColor = Color.LightSteelBlue
            OrderGroupBox.BackColor = Color.LightSteelBlue
            BeneficiaryGroupBox.BackColor = Color.LightSteelBlue
            BeneficiaryGroupBankGroupBox.BackColor = Color.LightSteelBlue
            OrderTab.BackColor = Color.LightSteelBlue
            BeneficiaryTab.BackColor = Color.LightSteelBlue
            FileTab.BackColor = Color.LightSteelBlue
            HistoryTab.BackColor = Color.LightSteelBlue
            PreviewTab.BackColor = Color.LightSteelBlue
            RequestTab.BackColor = Color.LightSteelBlue
            dgvHistory.BackgroundColor = Color.LightSteelBlue
            MainTab.BackColor = Color.LightSteelBlue
            BankDetailsTab.BackColor = Color.LightSteelBlue
            txtID.Text = ""
            txtIMSRef.Text = ""
            LoadNewItem()
            RefreshNewItem()
            ClearPaymentDetails()
            ClearOrderBeneficiary()
            CboPortfolio.Enabled = True
        Else
            cmdAddCash.Text = "Click to confirm edit"
            lblpaymentTitle.Text = "Edit Movement"
            cmdRefresh.Text = "New Movement"
            Me.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtID.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtOrderBalance.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtOrderBalanceInc.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtOrderBalancePostInc.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtBenBalance.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtBenBalanceInc.BackColor = Color.FromKnownColor(KnownColor.Control)
            txtBenBalancePostInc.BackColor = Color.FromKnownColor(KnownColor.Control)
            TabMovement.BackColor = Color.FromKnownColor(KnownColor.Control)
            OrderGroupBox.BackColor = Color.FromKnownColor(KnownColor.Control)
            BeneficiaryGroupBox.BackColor = Color.FromKnownColor(KnownColor.Control)
            OrderTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            BeneficiaryTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            FileTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            HistoryTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            PreviewTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            RequestTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            dgvHistory.BackgroundColor = Color.FromKnownColor(KnownColor.Control)
            BeneficiaryGroupBankGroupBox.BackColor = Color.FromKnownColor(KnownColor.Control)
            MainTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            BankDetailsTab.BackColor = Color.FromKnownColor(KnownColor.Control)
            BenMessages()
            OrderMessages()
        End If
    End Sub

    Public Sub OpenTransferFeeConfirm()
        If (ClsPay.SelectedPaymentID = 0 And ClsPay.PortfolioFeeDue) Or (ClsPay.SelectedPaymentID <> 0 And ClsPay.PortfolioFeeDue And Not ClsIMS.PaymentHasFee(ClsPay.SelectedPaymentID)) Then
            Dim FrmTFC As New FrmtransferFeesConfirm
            FrmTFC.ShowDialog()
        End If

        If ClsPay.SelectedPaymentOption = 2 Then
            ClsPay.SelectedTransferFeeOption = 0
            ClsPay.PortfolioFeeID = 0
            ClsPay.PortfolioFee = 0
            ClsPay.PortfolioFeeCCY = ""
            ClsPay.PortfolioFeeInt = 0
            ClsPay.PortfolioFeeCCYInt = ""
        End If
    End Sub


    Private Sub PopulateTemplateFormDetails(ByVal varPortfolioCode As Integer, ByVal varCCYCode As Integer, ByVal varBeneficiaryTemplateName As String)
        Dim Lst As New Dictionary(Of String, String)
        clearBen()
        clearBenSubBank()
        CboBenSubBankName.Text = ""
        ChkBenSubBank.Checked = False
        clearBenBank()
        CboBenBankName.Text = ""
        If varBeneficiaryTemplateName <> "" Then
            Lst = ClsIMS.GetTemplateFormData(varPortfolioCode, varCCYCode, varBeneficiaryTemplateName)
            If Not Lst Is Nothing Then
                If Lst.ContainsKey("Ben Pa_Name") Then
                    CboBenName.Text = Lst.Item("Ben Pa_Name").ToString
                    ClsPay.BeneficiaryAddressID = Lst.Item("Ben Pa_ID")
                    varBeneficiaryAddressID = ClsPay.BeneficiaryAddressID

                    If Lst.ContainsKey("Ben Pa_BeneficiaryTypeID") Then
                        If IsNumeric(Lst.Item("Ben Pa_BeneficiaryTypeID")) Then
                            cboBenType.SelectedValue = IIf(Lst.Item("Ben Pa_BeneficiaryTypeID") = "", 0, Lst.Item("Ben Pa_BeneficiaryTypeID"))
                        End If
                    End If
                    If Lst.ContainsKey("Ben Pa_Firstname") Then
                        txtbenfirstname.Text = Lst.Item("Ben Pa_Firstname").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Middlename") Then
                        txtbenmiddlename.Text = Lst.Item("Ben Pa_Middlename").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Surname") Then
                        txtbensurname.Text = Lst.Item("Ben Pa_Surname").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Address1") Then
                        txtBenAddress1.Text = Lst.Item("Ben Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Address2") Then
                        txtBenAddress2.Text = Lst.Item("Ben Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_County") Then
                        txtbencounty.Text = Lst.Item("Ben Pa_County").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Zip") Then
                        txtBenZip.Text = Lst.Item("Ben Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_CountryID") Then
                        If IsNumeric(Lst.Item("Ben Pa_CountryID")) Then
                            cboBenCountry.SelectedValue = IIf(Lst.Item("Ben Pa_CountryID") = "", 1, Lst.Item("Ben Pa_CountryID"))
                        End If
                    End If
                    If Lst.ContainsKey("Ben Pa_Country") Then
                        cboBenCountry.Text = Lst.Item("Ben Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_Swift") Then
                        txtBenSwift.Text = Lst.Item("Ben Pa_Swift").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_IBAN") Then
                        txtBenIBAN.Text = Lst.Item("Ben Pa_IBAN").ToString()
                    End If
                    If Lst.ContainsKey("Ben Pa_Other") Then
                        txtBenOther1.Text = Lst.Item("Ben Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_BIK") Then
                        txtBenBIK.Text = Lst.Item("Ben Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_INN") Then
                        txtBenINN.Text = Lst.Item("Ben Pa_INN").ToString
                    End If
                    If Lst.ContainsKey("Ben Pa_RelationshipID") Then
                        If IsNumeric(Lst.Item("Ben Pa_RelationshipID")) Then
                            cbobenrelationship.SelectedValue = IIf(Lst.Item("Ben Pa_RelationshipID") = "", 5, Lst.Item("Ben Pa_RelationshipID"))
                        End If
                    End If
                End If

                If Lst.ContainsKey("SubBank Pa_Name") Then
                    CboBenSubBankName.Text = Lst.Item("SubBank Pa_Name").ToString
                    ClsPay.BeneficiarySubBankAddressID = Lst.Item("SubBank Pa_ID")
                    varBeneficiarySubBankAddressID = ClsPay.BeneficiarySubBankAddressID

                    ChkBenSubBank.Checked = True

                    If Lst.ContainsKey("SubBank Pa_Address1") Then
                        txtBenSubBankAddress1.Text = Lst.Item("SubBank Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Address2") Then
                        txtBenSubBankAddress2.Text = Lst.Item("SubBank Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Zip") Then
                        txtBenSubBankZip.Text = Lst.Item("SubBank Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_CountryID") Then
                        If IsNumeric(Lst.Item("SubBank Pa_CountryID")) Then
                            cboBenSubBankCountry.SelectedValue = IIf(Lst.Item("SubBank Pa_CountryID") = "", 1, Lst.Item("SubBank Pa_CountryID"))
                        End If
                    End If
                    If Lst.ContainsKey("SubBank Pa_Country") Then
                        cboBenSubBankCountry.Text = Lst.Item("SubBank Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_Swift") Then
                        txtBenSubBankSwift.Text = Lst.Item("SubBank Pa_Swift").ToString
                        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
                    End If
                    If Lst.ContainsKey("SubBank Pa_IBAN") Then
                        txtBenSubBankIBAN.Text = Lst.Item("SubBank Pa_IBAN").ToString
                        SelectImageSWIFTIBAN(3, "IBAN", txtBenSubBankIBAN, PicBenSubBankIBAN)
                    End If
                    If Lst.ContainsKey("SubBank Pa_Other") Then
                        txtBenSubBankOther1.Text = Lst.Item("SubBank Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_BIK") Then
                        txtBenSubBankBIK.Text = Lst.Item("SubBank Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("SubBank Pa_INN") Then
                        txtBenSubbankINN.Text = Lst.Item("SubBank Pa_INN").ToString
                    End If
                End If

                If Lst.ContainsKey("Bank Pa_Name") Then
                    CboBenBankName.Text = Lst.Item("Bank Pa_Name").ToString
                    ClsPay.BeneficiaryBankAddressID = Lst.Item("Bank Pa_ID")
                    varBeneficiaryBankAddressID = ClsPay.BeneficiaryBankAddressID

                    If Lst.ContainsKey("Bank Pa_Address1") Then
                        txtBenBankAddress1.Text = Lst.Item("Bank Pa_Address1").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Address2") Then
                        txtBenBankAddress2.Text = Lst.Item("Bank Pa_Address2").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Zip") Then
                        txtBenBankZip.Text = Lst.Item("Bank Pa_Zip").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_CountryID") Then
                        If IsNumeric(Lst.Item("Bank Pa_CountryID")) Then
                            cboBenBankCountry.SelectedValue = IIf(Lst.Item("Bank Pa_CountryID") = "", 1, Lst.Item("Bank Pa_CountryID"))
                        End If
                    End If
                    If Lst.ContainsKey("Bank Pa_Country") Then
                        cboBenBankCountry.Text = Lst.Item("Bank Pa_Country").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_Swift") Then
                        txtBenBankSwift.Text = Lst.Item("Bank Pa_Swift").ToString
                        SelectImageSWIFTIBAN(4, "swift", txtBenBankSwift, PicBenBankSwift)
                    End If
                    If Lst.ContainsKey("Bank Pa_IBAN") Then
                        txtBenBankIBAN.Text = Lst.Item("Bank Pa_IBAN").ToString
                        SelectImageSWIFTIBAN(4, "IBAN", txtBenBankIBAN, PicBenBankIBAN)
                    End If
                    If Lst.ContainsKey("Bank Pa_Other") Then
                        txtBenBankOther1.Text = Lst.Item("Bank Pa_Other").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_BIK") Then
                        txtBenBankBIK.Text = Lst.Item("Bank Pa_BIK").ToString
                    End If
                    If Lst.ContainsKey("Bank Pa_INN") Then
                        txtBenBankINN.Text = Lst.Item("Bank Pa_INN").ToString
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub PopulateExtraFormDetails()
        Dim Lst As New Dictionary(Of String, String)

        If ClsPay.SelectedOrder = 1 Then
            If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                    ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                clearOrder()
            Else
                ClearOrderAddress()
            End If

            If IsNumeric(ClsPay.OrderAccountCode) Then
                Lst = ClsPay.OrderFormData

                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                            CboOrderName.Text = Lst.Item("B_Name1").ToString
                        End If
                    End If
                    If Lst.ContainsKey("B_Address") Then
                        txtOrderAddress1.Text = Lst.Item("B_Address").ToString
                    End If
                    If Lst.ContainsKey("B_Area") Then
                        txtOrderAddress2.Text = Lst.Item("B_Area").ToString
                    End If
                    If Lst.ContainsKey("B_Zip") Then
                        txtOrderZip.Text = Lst.Item("B_Zip").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboOrderCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If

                    If Lst.ContainsKey("SC_Country") Then
                        cboOrderCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("B_SwiftAddress") Then
                        txtOrderSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        SelectImageSWIFTIBAN(1, "swift", txtOrderSwift, PicOrderSwift)
                    End If
                    If Lst.ContainsKey("T_IBAN") Then
                        If Lst.Item("T_IBAN").ToString <> "" Then
                            txtOrderIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                            SelectImageSWIFTIBAN(1, "IBAN", txtOrderIBAN, PicOrderIBAN)
                        End If
                    End If
                    If Lst.ContainsKey("B_Other1") Then
                        txtOrderOther1.Text = Lst.Item("B_Other1").ToString
                    End If

                    If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                            ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                        ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Then
                        If Lst.ContainsKey("SC_Swift3rdPartyMessTypeID") Then
                            If IsNumeric(Lst.Item("SC_Swift3rdPartyMessTypeID")) Then
                                If ClsPay.OrderMessageTypeID <> 0 Then
                                    ClsPay.MessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID)
                                ElseIf Lst.Item("SC_Swift3rdPartyMessTypeID") <> 0 Then
                                    ClsPay.OrderMessageTypeID = Lst.Item("SC_Swift3rdPartyMessTypeID")
                                    ClsPay.MessageType = ClsIMS.GetMessageType(Lst.Item("SC_Swift3rdPartyMessTypeID").ToString)
                                End If
                            End If
                        End If
                    ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                        If Lst.ContainsKey("SC_SwiftOtherMessTypeID") Then
                            If IsNumeric(Lst.Item("SC_SwiftOtherMessTypeID")) Then
                                If ClsPay.OrderMessageTypeID <> 0 Then
                                    ClsPay.MessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID)
                                ElseIf Lst.Item("SC_SwiftOtherMessTypeID") <> 0 Then
                                    ClsPay.OrderMessageTypeID = Lst.Item("SC_SwiftOtherMessTypeID")
                                    ClsPay.MessageType = ClsIMS.GetMessageType(Lst.Item("SC_SwiftOtherMessTypeID").ToString)
                                End If
                            End If
                        End If
                    Else
                        If Lst.ContainsKey("SC_SwiftTransferMessTypeID") Then
                            If IsNumeric(Lst.Item("SC_SwiftTransferMessTypeID")) Then
                                If ClsPay.OrderMessageTypeID <> 0 Then
                                    ClsPay.MessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID)
                                ElseIf Lst.Item("SC_SwiftTransferMessTypeID") <> 0 Then
                                    ClsPay.OrderMessageTypeID = Lst.Item("SC_SwiftTransferMessTypeID")
                                    ClsPay.MessageType = ClsIMS.GetMessageType(Lst.Item("SC_SwiftTransferMessTypeID").ToString)
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 2 Then
            If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                clearBen()
            Else
                ClearBenAddress()
            End If
            If IsNumeric(ClsPay.BeneficiaryAccountCode) Then
                Lst = ClsPay.BeneficiaryFormData

                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                            CboBenName.Text = Lst.Item("B_Name1").ToString
                        End If
                    End If
                    If Lst.ContainsKey("B_Address") Then
                        txtBenAddress1.Text = Lst.Item("B_Address").ToString
                    End If
                    If Lst.ContainsKey("B_Area") Then
                        txtBenAddress2.Text = Lst.Item("B_Area").ToString
                    End If
                    If Lst.ContainsKey("B_Zip") Then
                        txtBenZip.Text = Lst.Item("B_Zip").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("B_SwiftAddress") Then
                        txtBenSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        SelectImageSWIFTIBAN(2, "swift", txtBenSwift, PicBenSwift)
                    End If
                    If Lst.ContainsKey("T_IBAN") Then
                        If Lst.Item("T_IBAN").ToString <> "" Then
                            txtBenIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                            SelectImageSWIFTIBAN(2, "IBAN", txtBenIBAN, PicBenIBAN)
                        End If
                    End If
                    If Lst.ContainsKey("B_Other1") Then
                        txtBenOther1.Text = Lst.Item("B_Other1").ToString
                    End If

                    If Lst.ContainsKey("IBankSwiftAddress") Then
                        txtBenSubBankSwift.Text = Lst.Item("IBankSwiftAddress").ToString
                        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 3 Then
            clearBenSubBank()

            If ClsPay.SelectedBeneficiarySubBankSwiftCode <> "" Then
                Lst = ClsPay.BeneficiarySubBankFormData
                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        CboBenSubBankName.Text = Lst.Item("B_Name1").ToString
                    End If
                    If Lst.ContainsKey("B_Address") Then
                        txtBenSubBankAddress1.Text = Lst.Item("B_Address").ToString
                    End If
                    If Lst.ContainsKey("B_Area") Then
                        txtBenSubBankAddress2.Text = Lst.Item("B_Area").ToString
                    End If
                    If Lst.ContainsKey("B_Zip") Then
                        txtBenSubBankZip.Text = Lst.Item("B_Zip").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenSubBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenSubBankCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("B_SwiftAddress") Then
                        txtBenSubBankSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
                    End If
                    If Lst.ContainsKey("T_IBAN") Then
                        txtBenSubBankIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        SelectImageSWIFTIBAN(3, "IBAN", txtBenSubBankIBAN, PicBenSubBankIBAN)
                    End If
                    If Lst.ContainsKey("B_Other1") Then
                        txtBenSubBankOther1.Text = Lst.Item("B_Other1").ToString
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 4 Then
            clearBenBank()

            If ClsPay.SelectedBeneficiaryBankSwiftCode <> "" Then
                Lst = ClsPay.BeneficiaryBankFormData
                If Not Lst Is Nothing Then
                    If Lst.ContainsKey("B_Name1") Then
                        CboBenBankName.Text = Lst.Item("B_Name1").ToString
                    End If
                    If Lst.ContainsKey("B_Address") Then
                        txtBenBankAddress1.Text = Lst.Item("B_Address").ToString
                    End If
                    If Lst.ContainsKey("B_Area") Then
                        txtBenBankAddress2.Text = Lst.Item("B_Area").ToString
                    End If
                    If Lst.ContainsKey("B_Zip") Then
                        txtBenBankZip.Text = Lst.Item("B_Zip").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenBankCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("B_SwiftAddress") Then
                        txtBenBankSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                        SelectImageSWIFTIBAN(4, "swift", txtBenBankSwift, PicBenBankSwift)
                    End If
                    If Lst.ContainsKey("T_IBAN") Then
                        txtBenBankIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        SelectImageSWIFTIBAN(4, "IBAN", txtBenBankIBAN, PicBenBankIBAN)
                    End If
                    If Lst.ContainsKey("B_Other1") Then
                        txtBenBankOther1.Text = Lst.Item("B_Other1").ToString
                    End If
                End If
            End If
        End If
    End Sub

    Private Function PopulateExtraFormDetailsFromSwiftCode() As Boolean
        Dim Lst As New Dictionary(Of String, String)

        PopulateExtraFormDetailsFromSwiftCode = False
        varPopulateExtraFormDetailsFromSwiftCode = True
        If ClsPay.SelectedOrder = 1 Then
            ClearOrderAddressForSwift()

            If ValidSwift(txtOrderSwift.Text) Then
                Lst = ClsIMS.GetFormDataFromSwift(txtOrderSwift.Text)
                If Not Lst Is Nothing Then
                    PopulateExtraFormDetailsFromSwiftCode = True
                    If Lst.ContainsKey("SC_Address1") Then
                        txtOrderAddress1.Text = Lst.Item("SC_Address1").ToString
                    End If
                    If Lst.ContainsKey("SC_City") Then
                        txtOrderAddress2.Text = Lst.Item("SC_City").ToString
                    End If
                    If Lst.ContainsKey("SC_PostCode") Then
                        txtOrderZip.Text = Lst.Item("SC_PostCode").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboOrderCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboOrderCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 2 Then
            ClearBenAddressForSwift()

            If ValidSwift(txtBenSwift.Text) Then
                Lst = ClsIMS.GetFormDataFromSwift(txtBenSwift.Text)

                If Not Lst Is Nothing Then
                    PopulateExtraFormDetailsFromSwiftCode = True
                    If Lst.ContainsKey("SC_Address1") Then
                        txtBenAddress1.Text = Lst.Item("SC_Address1").ToString
                    End If
                    If Lst.ContainsKey("SC_City") Then
                        txtBenAddress2.Text = Lst.Item("SC_City").ToString
                    End If
                    If Lst.ContainsKey("SC_PostCode") Then
                        txtBenZip.Text = Lst.Item("SC_PostCode").ToString
                    End If
                    If Lst.ContainsKey("SC_BIK") Then
                        txtBenBIK.Text = Lst.Item("SC_BIK").ToString
                    End If
                    If Lst.ContainsKey("SC_INN") Then
                        txtBenINN.Text = Lst.Item("SC_INN").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 3 Then
            clearSubBenBankAddressForSwift()

            If ValidSwift(txtBenSubBankSwift.Text) Then
                Lst = ClsIMS.GetFormDataFromSwift(txtBenSubBankSwift.Text, txtBenSwift.Text, CboCCY.SelectedValue)

                If Not Lst Is Nothing Then
                    PopulateExtraFormDetailsFromSwiftCode = True
                    If Lst.ContainsKey("SC_BankName") Then
                        CboBenSubBankName.Text = Lst.Item("SC_BankName").ToString
                    End If
                    If Lst.ContainsKey("SC_Address1") Then
                        txtBenSubBankAddress1.Text = Lst.Item("SC_Address1").ToString
                    End If
                    If Lst.ContainsKey("SC_City") Then
                        txtBenSubBankAddress2.Text = Lst.Item("SC_City").ToString
                    End If
                    If Lst.ContainsKey("SC_PostCode") Then
                        txtBenSubBankZip.Text = Lst.Item("SC_PostCode").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenSubBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenSubBankCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("IBankIBAN") Then
                        txtBenSubBankIBAN.Text = Lst.Item("IBankIBAN").ToString
                    End If
                    If Lst.ContainsKey("IBankBIK") Then
                        txtBenSubBankBIK.Text = Lst.Item("IBankBIK").ToString
                    End If
                    If Lst.ContainsKey("IBankINN") Then
                        txtBenSubbankINN.Text = Lst.Item("IBankINN").ToString
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 4 Then
            clearBenBankAddressForSwift()

            If ValidSwift(txtBenBankSwift.Text) Then
                Lst = ClsIMS.GetFormDataFromSwift(txtBenBankSwift.Text)

                If Not Lst Is Nothing Then
                    PopulateExtraFormDetailsFromSwiftCode = True
                    If Lst.ContainsKey("SC_BankName") Then
                        CboBenBankName.Text = Lst.Item("SC_BankName").ToString
                    End If
                    If Lst.ContainsKey("SC_Address1") Then
                        txtBenBankAddress1.Text = Lst.Item("SC_Address1").ToString
                    End If
                    If Lst.ContainsKey("SC_City") Then
                        txtBenBankAddress2.Text = Lst.Item("SC_City").ToString
                    End If
                    If Lst.ContainsKey("SC_CountryID") Then
                        If IsNumeric(Lst.Item("SC_CountryID")) Then
                            cboBenBankCountry.SelectedValue = Lst.Item("SC_CountryID")
                        End If
                    End If
                    If Lst.ContainsKey("SC_Country") Then
                        cboBenBankCountry.Text = Lst.Item("SC_Country").ToString
                    End If
                    If Lst.ContainsKey("SC_PostCode") Then
                        txtBenBankZip.Text = Lst.Item("SC_PostCode").ToString
                    End If
                    If Lst.ContainsKey("SC_BIK") Then
                        txtBenBIK.Text = Lst.Item("SC_BIK").ToString
                    End If
                    If Lst.ContainsKey("SC_INN") Then
                        txtBenINN.Text = Lst.Item("SC_INN").ToString
                    End If
                End If
            End If
        End If
        varPopulateExtraFormDetailsFromSwiftCode = False
    End Function

    Private Function PopulateExtraFormDetailsTransfer() As Boolean
        Dim Lst As New Dictionary(Of String, String)

        PopulateExtraFormDetailsTransfer = False
        If ClsPay.SelectedOrder = 1 Then
            ClearOrderAddress()

            Lst = ClsIMS.GetFormDataTransfer(CboCCY.SelectedValue, CboOrderName.SelectedValue)
            If Not Lst Is Nothing Then
                PopulateExtraFormDetailsTransfer = True
                If Lst.ContainsKey("B_SwiftAddress") Then
                    txtOrderSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                    SelectImageSWIFTIBAN(1, "swift", txtOrderSwift, PicOrderSwift)
                End If
                If Lst.ContainsKey("T_IBAN") Then
                    If Lst.Item("T_IBAN").ToString <> "" Then
                        txtOrderIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        SelectImageSWIFTIBAN(1, "IBAN", txtOrderIBAN, PicOrderIBAN)
                    End If
                End If
                If Lst.ContainsKey("T_Account") Then
                    ClsPay.OrderAccountNumber = Lst.Item("T_Account").ToString
                End If

                If Lst.ContainsKey("SC_SwiftTransferMessTypeID") Then
                    If IsNumeric(Lst.Item("SC_SwiftTransferMessTypeID")) Then
                        If ClsPay.OrderMessageTypeID <> 0 Then
                            ClsPay.MessageType = ClsIMS.GetMessageType(ClsPay.OrderMessageTypeID)
                        ElseIf Lst.Item("SC_SwiftTransferMessTypeID") <> 0 Then
                            ClsPay.OrderMessageTypeID = Lst.Item("SC_SwiftTransferMessTypeID")
                            ClsPay.MessageType = ClsIMS.GetMessageType(Lst.Item("SC_SwiftTransferMessTypeID").ToString)
                        End If
                    End If
                End If
            End If
        ElseIf ClsPay.SelectedOrder = 2 Then
            ClearBenAddress()

            Lst = ClsIMS.GetFormDataTransfer(cboBenCCY.SelectedValue, CboBenName.SelectedValue)
            If Not Lst Is Nothing Then
                PopulateExtraFormDetailsTransfer = True
                If Lst.ContainsKey("B_SwiftAddress") Then
                    txtBenSwift.Text = Strings.Trim(Lst.Item("B_SwiftAddress").ToString)
                    SelectImageSWIFTIBAN(2, "swift", txtBenSwift, PicBenSwift)
                End If
                If Lst.ContainsKey("T_IBAN") Then
                    If Lst.Item("T_IBAN").ToString <> "" Then
                        txtBenIBAN.Text = Strings.Trim(Lst.Item("T_IBAN").ToString)
                        SelectImageSWIFTIBAN(2, "IBAN", txtBenIBAN, PicBenIBAN)
                    End If
                End If
                If Lst.ContainsKey("T_Account") Then
                    ClsPay.BeneficiaryAccountNumber = Lst.Item("T_Account").ToString
                End If
            End If
        End If
    End Function

    Private Sub cmdSubmit_Click(sender As Object, e As EventArgs) Handles cmdSubmit.Click
        If ClsPay.SelectedPaymentID = 0 Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSubmit) Then
                If ClsPay.SubmitTransactions(dgvCash, cboPayCheck.SelectedValue) Then
                    Changeform(True)
                End If
            Else
                MsgBox("You are not authorised to submit payments", vbCritical, "Cannot submit payments")
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovementsFiles, "Refused entry to submit payments")
            End If
        Else
            MsgBox("You are currently in edit mode. Please save your current transaction or create a new one to submit other transactions", vbInformation, "Complete save transaction")
        End If
    End Sub

    Private Sub PopulateFormFromPortfolio(ByVal varUserLocationCode As Integer)
        ClearOrderMessages()
        ClearBenMessages()
        ClsPay.OrderAccountCode = -1
        ClsPay.BeneficiaryAccountCode = -1
        ClsPay.BeneficaryAccountFiltered = False
        ClsPay.BeneficiaryTemplateName = ""

        If ClsPay.PaymentType <> "" And ClsPay.CCY <> "" Then
            If CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Then
                ClsPay.OrderRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtOrderRef.Text = ClsPay.OrderRef
                txtBenRef.Text = ClsPay.OrderRef
                ClsIMS.PopulateFormCombo(CboOrderAccount, ClsPay.PortfolioCode, ClsPay.CCYCode,, varUserLocationCode)
                ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.BeneficiaryPortfolioCode, ClsPay.BeneficiaryCCYCode,, varUserLocationCode)
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                ClsPay.OrderRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtOrderRef.Text = ClsPay.OrderRef
                txtBenRef.Text = ClsPay.OrderRef
                ClsIMS.PopulateFormCombo(CboOrderAccount, ClsPay.CustomerId, ClsPay.CCYCode,, varUserLocationCode)
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPort) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInOther) Or
                    CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                ClsPay.OrderRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtOrderRef.Text = ClsPay.OrderRef
                txtBenRef.Text = ClsPay.OrderRef
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.OtherID = cPayDescInOtherIntTra Then
                    ClsIMS.PopulateFormCombo(CboOrderAccount, 365, ClsPay.CCYCode,, varUserLocationCode)
                    ClsIMS.PopulateFormCombo(CboOrderName, ClsPay.PortfolioCode, ClsPay.CCYCode,, varUserLocationCode)
                ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.OtherID = cPayDescInOtherNoticetoReceive Or ClsPay.OtherID = cPayDescInOtherStatementRequest Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.CCYCode, ClsPay.BeneficiaryPortfolioCode,, varUserLocationCode)
                Else
                    ClsIMS.PopulateFormCombo(CboOrderAccount, ClsPay.CCYCode,,, varUserLocationCode)
                End If
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientOut) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                ClsPay.OrderRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtOrderRef.Text = ClsPay.OrderRef
                txtBenRef.Text = ClsPay.OrderRef
                ClsIMS.PopulateFormCombo(CboOrderAccount, ClsPay.PortfolioCode, ClsPay.CCYCode,, varUserLocationCode)
                ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.BeneficiaryPortfolioCode, ClsPay.BeneficiaryCCYCode,, varUserLocationCode)
                ClsIMS.PopulateFormCombo(CboBenSubBankName)
                ClsIMS.PopulateFormCombo(CboBenBankName)
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExIn) Then
                ClsPay.BeneficiaryRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtBenRef.Text = ClsPay.BeneficiaryRef
                ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.PortfolioCode, ClsPay.CCYCode,, varUserLocationCode)
                ClsIMS.PopulateFormCombo(CboBenBankName)
            End If
            ClsIMS.PopulateFormCombo(cboOrderCountry)
            ClsIMS.PopulateFormCombo(cboBenCountry)
            ClsIMS.PopulateFormCombo(cboBenSubBankCountry)
            ClsIMS.PopulateFormCombo(cboBenBankCountry)
            ClsIMS.PopulateFormCombo(cboBenType)
            ClsIMS.PopulateFormCombo(cbobenrelationship)
            RunRefreshHistoryGrid()
        Else
            txtOrderBalance.Text = ""
            txtOrderBalanceInc.Text = ""
            txtOrderBalancePostInc.Text = ""
            txtBenBalance.Text = ""
            txtBenBalanceInc.Text = ""
            txtBenBalancePostInc.Text = ""
        End If

        clearOrder()
        clearBen()
        clearBenSubBank()
        clearBenBank()
    End Sub

    Private Sub dgvCash_Disposed(sender As Object, e As EventArgs) Handles dgvCash.Disposed
        NewPaymentForm = Nothing
    End Sub

    Private Sub dgvCash_RowHeaderMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCash.RowHeaderMouseDoubleClick
        Dim FrmAuth As New FrmPaymentAuthorise

        If ClsPay.SelectedPaymentID = 0 Then
            ClsPayAuth = New ClsPayment
            ClsIMS.GetReaderItemIntoAuthPaymentClass(dgvCash.Rows(e.RowIndex).Cells(0).Value)
            FrmAuth.ShowDialog()
            If ClsPayAuth.SelectedAuthoriseOption = 7 Then 'edit
                ClsPay = ClsPayAuth
                EditPayment()
            End If
            ClsIMS.RefreshPaymentGrid(dgvCash)
        Else
            MsgBox("You are currently in edit mode. Please save your current transaction or create a new one to authorise other transactions", vbInformation, "Complete save transaction")
        End If
    End Sub

    Private Sub cmdRefresh_Click(sender As Object, e As EventArgs) Handles cmdRefresh.Click
        Dim varGetFilterWhereClause As String
        If ClsPay.SelectedPaymentID = 0 Then
            Cursor = Cursors.WaitCursor
            ClsIMS.UpdatePendingMovements()
            ClsIMS.UpdateMovementStatus()

            If ChkShowHidden.Checked Then
                varGetFilterWhereClause = "where 1 = 1 "
            Else
                varGetFilterWhereClause = GetFilterWhereClause()
            End If

            ClsIMS.RefreshMovementGrid(dgvCash, varGetFilterWhereClause)
            Cursor = Cursors.Default
        Else
            Dim varResponse = MsgBox("Click OK to cancel editing this transaction?", vbCritical + vbOKCancel, "transaction not saving")
            If varResponse = vbOK Then
                Changeform(True)
            End If
        End If
    End Sub

    Private Sub CboPortfolio_Leave(sender As Object, e As EventArgs) Handles CboPortfolio.Leave
        If Not IsNumeric(CboCCY.SelectedValue) Then
            ClearOrderBeneficiary()
            ClsPay.PortfolioIsFee = False
            ClsPay.PortfolioIsCommission = False
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                If CboPortfolio.SelectedValue = cPayDescInOtherPayFee Or CboPortfolio.SelectedValue = cPayDescInOtherPayServicesFee Or CboPortfolio.SelectedValue = cPayDescInOtherPayVolopaFee Then
                    ClsPay.PortfolioIsFee = True
                ElseIf CboPortfolio.SelectedValue = cPayDescInOtherComFee Or CboPortfolio.SelectedValue = cPayDescInOtherComOrdOnly Or
                    CboPortfolio.SelectedValue = cPayDescInOtherComBenOnly Or CboPortfolio.SelectedValue = cPayDescInOtherFXCommission Then
                    ClsPay.PortfolioIsCommission = True
                ElseIf CboPortfolio.SelectedValue = cPayDescInOtherNoticetoReceive Or CboPortfolio.SelectedValue = cPayDescInOtherStatementRequest Then
                    TabMovement.TabPages.Remove(OrderTab)
                    OrderGroupBox.Visible = False
                    OrderTab.Visible = False
                End If
            End If
        End If
    End Sub

    Private Sub ClearOrderBeneficiary()
        txtOrderRef.Text = ""
        txtBenRef.Text = ""
        CboOrderAccount.DataSource = Nothing
        CboOrderAccount.Enabled = True
        CboBenAccount.DataSource = Nothing
        CboBenAccount.Enabled = True
        CboOrderName.DataSource = Nothing
        CboBenName.DataSource = Nothing
        ClsPay.BeneficiaryTemplateName = ""
        ClsPay.OrderAccountCode = 0
        ClsPay.OrderAccount = ""
        ClsPay.BeneficiaryAccountCode = 0
        ClsPay.BeneficiaryAccount = ""
        txtOrderBalance.Text = Nothing
        txtOrderBalanceInc.Text = Nothing
        txtOrderBalancePostInc.Text = Nothing
        txtBenBalance.Text = Nothing
        txtBenBalanceInc.Text = Nothing
        txtBenBalancePostInc.Text = Nothing
        ClsPay.SelectedOrder = 1
        ClsPay.OrderAccountCode = -1
        PopulateExtraFormDetails()
        ClsPay.SelectedOrder = 2
        ClsPay.BeneficiaryAccountCode = -1
        PopulateExtraFormDetails()
        ClsPay.SelectedOrder = 3
        ClsPay.SelectedBeneficiarySubBankSwiftCode = ""
        PopulateExtraFormDetails()
        CboBenSubBankName.Text = ""
        clearBenSubBank()
        ClsPay.SelectedOrder = 4
        ClsPay.SelectedBeneficiaryBankSwiftCode = ""
        PopulateExtraFormDetails()
        CboBenBankName.Text = ""
        clearBenBank()
        ClearOrderMessages()
        ClearBenMessages()
        ChkBenUrgent.Checked = False
        ChkBenSendRef.Checked = False
        ChkBenTemplate.Checked = False
        ChkBenSubBank.Checked = False
        dgvHistory.DataSource = Nothing
        If ChkAutoComments.Checked Then
            txtComments.Text = ""
        End If
    End Sub

    Private Sub CboPortfolio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboPortfolio.SelectedIndexChanged
        If IsNumeric(CboPortfolio.SelectedValue) Then
            ClsPay.OtherID = 0
            ManageTabs()
            GrpSendMessage.Visible = False
            txtEmailRequest.ReadOnly = True
            txtAmount.Enabled = True
            txtComments.Enabled = True
            CCYDefaults(True)
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                ClsPay.CustomerId = CboPortfolio.SelectedValue
                ClsPay.CustomerName = CboPortfolio.Text
                ClsPay.PortfolioCode = CboPortfolio.SelectedValue
                ClsPay.Portfolio = CboPortfolio.Text
                ClsIMS.PopulateFormCombo(CboCCY, ClsPay.CustomerId)
                ClsIMS.PopulateFormCombo(CboCCY, ClsPay.CustomerId)
                ClsIMS.PopulateFormCombo(cboBenCCY, ClsPay.CustomerId)
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                ClsPay.OtherID = CboPortfolio.SelectedValue
                ClsIMS.PopulateFormCombo(CboCCY)
                ClsIMS.PopulateFormCombo(cboBenCCY)
                If ClsPay.OtherID = cPayDescInOtherNoticetoReceive Or ClsPay.OtherID = cPayDescInOtherStatementRequest Then
                    TabMovement.TabPages.Remove(OrderTab)
                    TabMovement.SelectedTab = BeneficiaryTab
                    ClsPay.SendIMS = False
                    lblBenBalanceInc.Visible = False
                    txtBenBalanceInc.Visible = False
                    lblBenBalancePostInc.Visible = False
                    txtBenBalancePostInc.Visible = False
                    lblBenOther1.Text = "Ref:"
                    txtBenOther1.Enabled = True
                    If ClsPay.OtherID = cPayDescInOtherStatementRequest Then
                        txtAmount.Text = 1
                    End If
                ElseIf ClsPay.OtherID = cPayDescInOtherBankCharges Then
                    TabMovement.TabPages.Remove(BeneficiaryTab)
                    ClsPay.SendSwift = False
                ElseIf ClsPay.OtherID = cPayDescInOtherFreeText199 Or ClsPay.OtherID = cPayDescInOtherFreeText599 Or ClsPay.OtherID = cPayDescInOtherFreeText999 Or ClsPay.OtherID = cPayDescInOtherCancelPayment192 Then
                    TabMovement.TabPages.Remove(OrderTab)
                    TabMovement.TabPages.Remove(BeneficiaryTab)
                    TabMovement.SelectedTab = RequestTab
                    GrpSendMessage.Visible = True
                    ClsIMS.PopulateComboboxWithData(cboSendTo, "select 0,sc_swiftcode from vwdolfinpaymentswiftcodes where sc_source not in ('UPLOAD') group by sc_swiftcode order by sc_swiftcode")
                    txtEmailRequest.ReadOnly = False
                    CboCCY.SelectedValue = 6
                    CboCCY.Enabled = False
                    txtAmount.Text = 0.01
                    txtAmount.Enabled = False
                    txtComments.Enabled = False
                ElseIf ClsPay.OtherID = cPayDescInOtherFXBuy Or ClsPay.OtherID = cPayDescInOtherFXSell Then
                    CCYDefaults(False, ClsPay.OtherID)
                End If
            Else
                ClsPay.PortfolioCode = CboPortfolio.SelectedValue
                ClsPay.Portfolio = CboPortfolio.Text
                If ClsPay.SelectedPaymentID = 0 Or (ClsPay.SelectedPaymentID <> 0 And (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut))) Then
                    ClsPay.BeneficiaryPortfolioCode = CboPortfolio.SelectedValue
                    ClsPay.BeneficiaryPortfolio = CboPortfolio.Text
                End If

                ClsIMS.PopulateFormCombo(CboCCY, ClsPay.PortfolioCode)
                ClsIMS.PopulateFormCombo(cboBenCCY, ClsPay.BeneficiaryPortfolioCode)
            End If

            ClearOrderMessages()
            ClearBenMessages()
        End If
    End Sub

    Private Sub CboCCY_Leave(sender As Object, e As EventArgs) Handles CboCCY.Leave
        OrderMessages()

        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            cboBenType.SelectedValue = 1
            cbobenrelationship.SelectedValue = 5
        End If
        ShowExchangeRate()
    End Sub

    Private Sub PopulateRUBForm()
        If ClsPay.CCY = "RUB" Then
            lblOrderVOCode.Visible = True
            cboOrderVOCode.Visible = True
            ClsIMS.PopulateFormCombo(cboOrderVOCode)
            cboOrderVOCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cboOrderVOCode.AutoCompleteSource = AutoCompleteSource.ListItems
            lblbenBIK.Visible = True
            txtBenBIK.Visible = True
            lblbenINN.Visible = True
            txtBenINN.Visible = True
            lblBenSubBankBIK.Visible = True
            txtBenSubBankBIK.Visible = True
            lblBenSubBankINN.Visible = True
            txtBenSubbankINN.Visible = True
            lblBenBankBIK.Visible = True
            txtBenBankBIK.Visible = True
            lblBenBankINN.Visible = True
            txtBenBankINN.Visible = True
            txtOrderOther1.Enabled = False
        Else
            lblOrderVOCode.Visible = False
            cboOrderVOCode.Visible = False
            'If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Then
            '    lblOrderVOCode.Visible = True
            '    cboOrderVOCode.Visible = True
            '    ClsIMS.PopulateFormCombo(cboOrderVOCode)
            '    cboOrderVOCode.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            '    cboOrderVOCode.AutoCompleteSource = AutoCompleteSource.ListItems
            'Else
            '    cboOrderVOCode.DataSource = Nothing
            'End If
            lblbenBIK.Visible = False
            txtBenBIK.Visible = False
            lblbenINN.Visible = False
            txtBenINN.Visible = False
            lblBenSubBankBIK.Visible = False
            txtBenSubBankBIK.Visible = False
            lblBenSubBankINN.Visible = False
            txtBenSubbankINN.Visible = False
            lblBenBankBIK.Visible = False
            txtBenBankBIK.Visible = False
            lblBenBankINN.Visible = False
            txtBenBankINN.Visible = False
            txtOrderOther1.Enabled = True
        End If
    End Sub


    Private Sub CboCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboCCY.SelectedIndexChanged
        If IsNumeric(CboCCY.SelectedValue) Then
            lblCCYAmt.Text = CboCCY.Text
            ClsPay.CCYCode = CboCCY.SelectedValue
            ClsPay.CCY = CboCCY.Text
            cboBenCCY.Text = CboCCY.Text
            PopulateRUBForm()
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And (ClsPay.PortfolioIsCommission Or ClsPay.OtherID = cPayDescInOtherManagementFeeClearClientOwedFees Or ClsPay.OtherID = cPayDescInOtherCustodyFeeClearClientOwedFees) Then
                lblIMSRef.Visible = True
                txtIMSRef.Visible = True
                If (ClsPay.OtherID = cPayDescInOtherManagementFeeClearClientOwedFees Or ClsPay.OtherID = cPayDescInOtherCustodyFeeClearClientOwedFees) Then
                    txtIMSRef.Text = "INTTRA"
                End If
            Else
                lblIMSRef.Visible = False
                txtIMSRef.Visible = False
            End If

            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                ClsPay.PortfolioCode = 0
                ClsPay.Portfolio = ""
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                    lblOrderOther1.Text = "Rate:"
                End If
            End If

            If (ClsPay.Portfolio <> "" And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And ClsPay.CCY <> "") Or
                (ClsPay.Portfolio = "" And (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                    ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta)) And ClsPay.CCY <> "") Then
                If ClsPay.PortfolioCode = 0 Then
                    varUserLocationCode = ClsIMS.UserLocationCode
                Else
                    varUserLocationCode = ClsIMS.GetSQLItem("select BranchCode from vwdolfinpaymentclientportfolioall where pf_code = " & ClsPay.PortfolioCode)
                End If
                PopulateFormFromPortfolio(varUserLocationCode)
            End If
        End If
    End Sub

    Private Function CheckFee() As Boolean
        CheckFee = False
        If ClsPay.Portfolio <> "" Then
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                If (ClsPay.CCY = "GBP" And ClsPay.PortfolioFeeDue And ClsPay.PortfolioFee > 0) Or (ClsPay.CCY <> "GBP" And ClsPay.PortfolioFeeDue And ClsPay.PortfolioFeeInt > 0) Then
                    CheckFee = True
                End If
            End If
        End If
    End Function

    Private Sub CboPayType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboPayType.SelectedIndexChanged
        Try
            Cursor = Cursors.WaitCursor
            ClsPay.PaymentType = CboPayType.Text
            If ClsPay.SelectedPaymentID = 0 Then
                LoadNewItem()
                ClsPay.PaymentType = CboPayType.Text
                RefreshNewItem()
            End If

            ManageTabs()

            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Then
                TabMovement.SelectedTab = OrderTab
                OrderGroupBox.Visible = True
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
                BeneficiaryBankGroupBox.Visible = False
                ClsIMS.PopulateFormCombo(CboPortfolio)
                lblExchangeRate.Visible = False
                lblportfolio.Text = "Portfolio:"
                CboPortfolio.Text = ""
                CboPortfolio.Enabled = True
                CboCCY.DataSource = Nothing
                cboBenCCY.DataSource = Nothing
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                ChkBenTemplate.Visible = False
                ChkBenTemplateEdit.Visible = False
                lblOrderAccount.Text = "Account:"
                lblOrderName.Text = "Name:"
                lblBenAccount.Text = "Account"
                CboBenAccount.BackColor = Color.White
                lblBenName.Text = "Name:"
                lblBenBalance.Text = "Balance:"
                lblBenBalanceInc.Text = "Balance (inc. pending):"
                lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
                FixBenSwiftForm()
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                TabMovement.SelectedTab = OrderTab
                OrderGroupBox.Visible = True
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
                BeneficiaryBankGroupBox.Visible = False
                ClsIMS.PopulateFormCombo(CboPortfolio)
                lblExchangeRate.Visible = False
                lblportfolio.Text = "    Client:"
                CboPortfolio.Text = ""
                CboPortfolio.Enabled = True
                CboCCY.DataSource = Nothing
                cboBenCCY.DataSource = Nothing
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                ChkBenTemplate.Visible = False
                ChkBenTemplateEdit.Visible = False
                lblOrderAccount.Text = "Portfolio:"
                lblOrderName.Text = "Account:"
                lblBenAccount.Text = "Portfolio:"
                CboBenAccount.BackColor = Color.White
                lblBenName.Text = "Account:"
                lblBenBalance.Text = "Balance:"
                lblBenBalanceInc.Text = "Balance (inc. pending):"
                lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
                FixBenSwiftForm()
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                TabMovement.SelectedTab = OrderTab
                OrderGroupBox.Visible = True
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
                BeneficiaryBankGroupBox.Visible = False
                ClsIMS.PopulateFormCombo(CboPortfolio)
                lblExchangeRate.Visible = False
                lblportfolio.Text = "Fee Type:"
                CboPortfolio.Text = ""
                CboPortfolio.Enabled = True
                ClsIMS.PopulateFormCombo(CboCCY)
                ClsIMS.PopulateFormCombo(cboBenCCY)
                ClsPay.Portfolio = ""
                CboPortfolio.Text = ""
                ChkBenTemplate.Visible = False
                ChkBenTemplateEdit.Visible = False
                lblOrderAccount.Text = "Portfolio:"
                lblOrderName.Text = "Account:"
                lblBenAccount.Text = "Portfolio:"
                CboBenAccount.BackColor = Color.White
                lblBenName.Text = "Account:"
                lblBenBalance.Text = "Balance:"
                lblBenBalanceInc.Text = "Balance (inc. pending):"
                lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
                FixBenSwiftForm()
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                TabMovement.SelectedTab = OrderTab
                OrderGroupBox.Visible = True
                BeneficiaryGroupBankGroupBox.Visible = True
                BeneficiarySubBankGroupBox.Visible = True
                BeneficiaryBankGroupBox.Visible = True
                ClsIMS.PopulateFormCombo(CboPortfolio)
                lblExchangeRate.Visible = False
                lblportfolio.Text = "Portfolio:"
                CboPortfolio.Text = ""
                CboPortfolio.Enabled = True
                CboCCY.DataSource = Nothing
                cboBenCCY.DataSource = Nothing
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                ChkBenTemplate.Visible = True
                ChkBenTemplateEdit.Visible = True
                ChkBenTemplateEdit.Checked = False
                lblOrderAccount.Text = "Account:"
                lblOrderName.Text = "Name:"
                lblBenAccount.Text = "Template:"
                CboBenAccount.BackColor = Color.LightYellow
                lblBenName.Text = "Name:"
                lblBenBalance.Text = ""
                lblBenBalanceInc.Text = ""
                lblBenBalancePostInc.Text = ""
                FixBenSwiftForm()
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn) Then
                ClsIMS.PopulateFormCombo(CboPortfolio)
                TabMovement.TabPages.Remove(OrderTab)
                TabMovement.SelectedTab = BeneficiaryTab
                OrderGroupBox.Visible = False
                OrderTab.Visible = False
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
                BeneficiaryBankGroupBox.Visible = False
                lblExchangeRate.Visible = False
                lblportfolio.Text = "Portfolio:"
                CboPortfolio.Text = ""
                CboPortfolio.Enabled = True
                CboCCY.DataSource = Nothing
                cboBenCCY.DataSource = Nothing
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                ChkBenTemplate.Visible = False
                ChkBenTemplateEdit.Visible = False
                lblOrderAccount.Text = "Account:"
                lblOrderName.Text = "Name:"
                lblBenAccount.Text = "Account:"
                CboBenAccount.BackColor = Color.White
                lblBenName.Text = "Name:"
                lblBenBalance.Text = "Balance:"
                lblBenBalanceInc.Text = "Balance (inc. pending):"
                lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
                FixBenSwiftForm()
            Else
                If ClsPay.PaymentType = "" Then
                    CboPortfolio.Enabled = True
                Else
                    CboPortfolio.Enabled = False
                End If
                TabMovement.SelectedTab = OrderTab
                OrderGroupBox.Visible = True
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
                BeneficiaryBankGroupBox.Visible = False
                ClsIMS.PopulateFormCombo(CboCCY)
                ClsIMS.PopulateFormCombo(cboBenCCY)
                lblBenCCY.Visible = False
                cboBenCCY.Visible = False
                lblExchangeRate.Visible = False
                lblportfolio.Text = "Portfolio:"
                ClsPay.Portfolio = ""
                CboPortfolio.Text = ""
                ChkBenTemplate.Visible = False
                ChkBenTemplateEdit.Visible = False
                lblOrderAccount.Text = "Portfolio"
                lblOrderName.Text = "Account"
                lblBenAccount.Text = "Portfolio"
                CboBenAccount.BackColor = Color.White
                lblBenName.Text = "Account"
                lblBenBalance.Text = "Balance:"
                lblBenBalanceInc.Text = "Balance (inc. pending):"
                lblBenBalancePostInc.Text = "Post Balance (inc. pending):"
                FixBenSwiftForm()
            End If

            ChkBenSubBank.Enabled = True
            CboPortfolio.SelectedValue = -1
            CboCCY.SelectedValue = -1
            cboBenCCY.SelectedValue = -1
            txtOrderRef.Text = ""
            txtBenRef.Text = ""
            ClearOrderBeneficiary()
            If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
                ClsPay.SendSwift = True
            Else
                ClsPay.SendSwift = False
            End If
            Cursor = Cursors.Default
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - CboPayType_SelectedIndexChanged")
        End Try
    End Sub

    Private Sub FixBenSwiftForm()
        FormControlsEnable(1, False)
        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                ChkBenSendRef.Visible = False
                FormControlsEnable(2, False)
            Else
                ChkBenSendRef.Visible = True
                FormControlsEnable(2, True)
            End If
            ChkBenUrgent.Visible = True
            CboOrderName.Enabled = True
            txtOrderOther1.Enabled = True
            txtBenOther1.Enabled = False
            BeneficiaryGroupBox.Text = "Beneficiary Customer Details (59/59A on swift)"

            LabelTextAndPoint(lblOrderSwift, "Swift (52A):", 257, 109)
            LabelTextAndPoint(lblOrderIBAN, "IBAN (50K):", 255, 134)
            LabelTextAndPoint(lblOrderOther1, "Other (72):", 260, 159)

            LabelTextAndPoint(lblBenSwift, "Swift (57A):", 235, 12)
            LabelTextAndPoint(lblBenIBAN, "IBAN (59):", 240, 37)
            LabelTextAndPoint(lblBenOther1, "Other:", 260, 62)

            LabelTextAndPoint(lblBenSubBankSwift, "Swift(56A):", 268, 50)
            LabelTextAndPoint(lblBenSubBankIBAN, "IBAN(57A):", 268, 74)
            LabelTextAndPoint(lblBenSubBankOther1, "Other:", 285, 98)

            LabelTextAndPoint(lblBenBankSwift, "Swift(57A):", 268, 49)
            LabelTextAndPoint(lblBenBankIBAN, "IBAN(59):", 271, 73)
            LabelTextAndPoint(lblBenBankOther1, "Other:", 284, 97)
        Else
            ChkBenUrgent.Visible = False
            ChkBenSendRef.Visible = False
            CboBenName.Enabled = True
            FormControlsEnable(2, False)
            BeneficiaryGroupBox.Text = "Beneficiary Details"
            LabelTextAndPoint(lblOrderSwift, "Swift:", 277, 109)
            LabelTextAndPoint(lblOrderIBAN, "IBAN:", 275, 134)
            LabelTextAndPoint(lblOrderOther1, "Other:", 275, 159)

            LabelTextAndPoint(lblBenSwift, "Swift:", 260, 12)
            LabelTextAndPoint(lblBenIBAN, "IBAN:", 258, 37)
            LabelTextAndPoint(lblBenOther1, "Other:", 258, 62)

            LabelTextAndPoint(lblBenSubBankSwift, "Swift:", 272, 75)
            LabelTextAndPoint(lblBenSubBankIBAN, "IBAN:", 270, 99)
            LabelTextAndPoint(lblBenSubBankOther1, "Other:", 270, 125)

            LabelTextAndPoint(lblBenBankSwift, "Swift:", 272, 49)
            LabelTextAndPoint(lblBenBankIBAN, "IBAN:", 270, 73)
            LabelTextAndPoint(lblBenBankOther1, "Other:", 270, 100)
        End If

    End Sub

    Private Sub PopulateComments()
        If ChkAutoComments.Checked Then
            If CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                txtComments.Text = txtComments.Text
            ElseIf CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInOther) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPort) Or
                 CboPayType.Text = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                txtComments.Text = ClsIMS.PopulateComments(CboPayType.Text, CboPortfolio.Text, CboOrderAccount.Text, CboBenAccount.Text, txtAmount.Text, CboCCY.Text, ClsPayAuth.PaymentRequestedType, CboBenBankName.Text)
            Else
                txtComments.Text = ClsIMS.PopulateComments(CboPayType.Text, "", CboOrderName.Text, CboBenName.Text, txtAmount.Text, CboCCY.Text, ClsPayAuth.PaymentRequestedType, CboBenBankName.Text)
            End If
        End If
    End Sub


    Private Sub OrderMessages()
        Dim varPendingAmount As Double = 0

        ClearOrderMessages()
        If ClsPay.SelectedPaymentID = 0 Or (ClsPay.SelectedPaymentID <> 0 And ClsPay.Status = ClsIMS.GetStatusType(cStatusReady) Or ClsPay.Status = ClsIMS.GetStatusType(cStatusPendingAuth) Or ClsPay.Status = ClsIMS.GetStatusType(cStatusPendingReview)) Then
            If CboCCY.Text <> "" Then
                If CboPortfolio.Text <> "" Then
                    If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
                        OrderMessagesFee()
                    End If
                End If

                OrderMessagesSettle()

                If CboOrderAccount.Text <> "" Then 'Or
                    '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) And CboOrderName.Text <> "") Or
                    '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) And CboOrderName.Text <> "") Or
                    '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And CboOrderName.Text <> "") Or
                    '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) And CboOrderName.Text <> "") Or
                    '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And CboOrderName.Text <> "") Then
                    OrderMessagesCutOff()
                    OrderMessagesFormDetails()
                    OrderMessagesAccountCheck()
                End If

                If CboOrderAccount.Text <> "" And ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PortfolioIsCommission Then
                    OrderMessagesIMSRefCheck()
                End If
            End If
        ElseIf ClsPay.SelectedPaymentID <> 0 Then
            If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
                If ClsPay.SwiftReturned Then
                    If ClsPay.SwiftAcknowledged Then
                        PopulateMessage(True, "Swift:" & ClsPay.SwiftStatus, True, 0, 0, "")
                    ElseIf Not ClsPay.SwiftAcknowledged Then
                        If ClsPay.Status = ClsIMS.GetStatusType(cStatusSwiftRej) Then
                            PopulateMessage(True, ClsPay.SwiftStatus, False, 0, 12, "file:///" & Replace(ClsIMS.GetFilePath("Documents"), "\", "/") & "SwiftTextValidationErrorCodes.docx")
                        ElseIf ClsPay.Status = ClsIMS.GetStatusType(cStatusSwiftErr) Then
                            PopulateMessage(True, Strings.Left(ClsPay.SwiftStatus, 95), False, 0, 12, "file:///" & Replace(ClsIMS.GetFilePath("Documents"), "\", "/") & "SwiftErrors.docx")
                        End If
                    End If
                Else
                    PopulateMessage(True, "Swift:" & ClsPay.SwiftStatus, True, 0, 0, "")
                End If
            End If
            If ClsPay.SendIMS Then
                If ClsPay.IMSReturned Then
                    PopulateMessage(True, "IMS:" & ClsPay.IMSStatus, IIf(ClsPay.IMSAcknowledged, True, False), 0, 0, "")
                End If
            End If
        End If

        If CboOrderAccount.Text <> "" Then
            '(ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) And CboOrderName.Text <> "") _
            '   And ClsPay.OtherID <> cPayDescInOtherIntTra And ClsPay.OtherID <> cPayDescInOtherBankCharges _
            '  And Not ClsPay.SwiftAcknowledged And Not ClsPay.IMSAcknowledged Then
            OrderMessagesBalance(varPendingAmount)
            OrderMessagesBalanceInc(varPendingAmount)
            OrderMessagesPostBalanceInc(varPendingAmount)
            OrderMessageFOPFee()
        End If
        PopulateComments()
        PopulatePreviews()
    End Sub

    Private Sub OrderMessagesFee()
        If ClsPay.SelectedPaymentID <> 0 Then
            Dim FeeId As Double
            ClsIMS.FeeIncurred()
            If ClsIMS.PaymentHasFee(ClsPay.SelectedPaymentID, FeeId) Then
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Then
                    PopulateMessage(True, "This transaction is a fee for an external transfer" & IIf(FeeId <> 0, " ID:" & FeeId, ""), True, 0, 0, "")
                Else
                    PopulateMessage(True, "This client has a transaction fee created for this external transfer" & IIf(FeeId <> 0, " ID:" & FeeId, ""), True, 0, 0, "")
                End If
            ElseIf CheckFee() Then
                PopulateMessage(True, "Fee for this transaction has been waivered/cancelled. On save, you have option to book another fee", True, 0, 0, "")
            End If
        Else
            ClsIMS.FeeIncurred()
            If CheckFee() Then
                If ClsPay.CCY = "GBP" Then
                    PopulateMessage(True, "This client will incur an external transfer fee of " & ClsPay.PortfolioFee & " " & ClsPay.PortfolioFeeCCY & " per transaction", True, 0, 0, "")
                Else
                    PopulateMessage(True, "This client will incur an external international transfer fee of " & ClsPay.PortfolioFeeInt & " " & ClsPay.PortfolioFeeCCYInt & " per transaction", True, 0, 0, "")
                End If
            End If
        End If
    End Sub

    Private Sub OrderMessagesSettle()
        If dtSettleDate.Value.Date < Now.Date Then
            PopulateMessage(True, "The settle date for this payment has passed.", False, 0, 0, "")
        End If
    End Sub

    Private Sub OrderMessagesBalance(ByRef varPendingAmount As Double)
        txtOrderBalance.Text = ClsPay.OrderBalance.ToString("#,##0.00")
        ColorBalanceTextBox(txtOrderBalance)
        Dim PendingAmtMoreThanBalance As Boolean = PendingAmountMoreThanBalance(varPendingAmount)

        'when cross currency required
        'If CboCCY.Text <> cboBenCCY.Text Then
        'varPendingAmount = varPendingAmount * ClsPay.ExchangeRate
        'End If

        If CDbl(IIf(Not IsNumeric(txtAmount.Text), 0, txtAmount.Text)) <= 0 Then
            PopulateMessage(True, "Please enter a movement amount greater than 0", False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        ElseIf PendingAmtMoreThanBalance Then
            PopulateMessage(True, "WARNING: Pending/trans amount is greater than the available balance in the order account", False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        End If
    End Sub

    Private Sub OrderMessagesBalanceInc(ByVal varPendingAmount As Double)
        If IsNumeric(txtOrderBalance.Text) Then
            txtOrderBalanceInc.Text = (varPendingAmount + CDbl(txtOrderBalance.Text)).ToString("#,##0.00")
            ColorBalanceTextBox(txtOrderBalanceInc)
        End If
    End Sub

    Private Sub OrderMessagesPostBalanceInc(ByVal varPendingAmount As Double)
        If txtAmount.Text = "" Then
            If IsNumeric(txtOrderBalance.Text) Then
                txtOrderBalancePostInc.Text = (varPendingAmount + CDbl(txtOrderBalance.Text)).ToString("#,##0.00")
                ColorBalanceTextBox(txtOrderBalancePostInc)
            End If
        ElseIf IsNumeric(txtAmount.Text) And IsNumeric(txtOrderBalance.Text) Then
            txtOrderBalancePostInc.Text = ((-CDbl(txtAmount.Text)) + (varPendingAmount + CDbl(txtOrderBalance.Text))).ToString("#,##0.00")
            ColorBalanceTextBox(txtOrderBalancePostInc)
        End If
    End Sub

    Private Sub OrderMessagesCutOff()
        If CutOffTimePassed() Then
            TimerBroker.Enabled = False
            varLblBroker = PopulateMessage(True, "WARNING: You have now passed the cut off time. The cut off time for this broker was at " & ClsPay.CutOffTimeGMT & " GMT", False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        ElseIf dtSettleDate.Value.Date = Now.Date And ClsPay.CutOffTime <> "" Then
            ts = New TimeSpan(DateDiff(DateInterval.Hour, Now, CDate(dtSettleDate.Value.Date & " " & ClsPay.CutOffTimeGMT)), DateDiff(DateInterval.Minute, Now, CDate(dtSettleDate.Value.Date & " " & ClsPay.CutOffTimeGMT)) Mod 60, DateDiff(DateInterval.Second, Now, CDate(dtSettleDate.Value.Date & " " & ClsPay.CutOffTimeGMT)) Mod 60)
            TimerBroker.Enabled = True
            varLblBroker = PopulateMessage(True, "WARNING: The cut off time today for this broker is at " & ClsPay.CutOffTimeGMT & " GMT", True, 0, 0, "")
        Else
            TimerBroker.Enabled = False
        End If
    End Sub

    Private Sub OrderMessagesFormDetails()
        If ClsPay.OrderAccountNumber = "" Then
            PopulateMessage(True, "The order account number must be populated in IMS for this order account (Free field 1)", False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        ElseIf ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
            If txtOrderSwift.Text = "" Then
                PopulateMessage(True, "An order swift code is required for SWIFT payment", False, 0, 0, "")
                TabMovement.SelectedTab = OrderTab
            ElseIf txtOrderIBAN.Text = "" Then
                PopulateMessage(True, "An order IBAN is required for SWIFT payment", False, 0, 0, "")
                TabMovement.SelectedTab = OrderTab
            End If
        End If
    End Sub

    Private Sub OrderMessagesAccountCheck()
        If ((CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or CboPayType.Text = ClsIMS.GetPaymentType(cPayDescExClientIn)) And CboOrderAccount.Items.Count <= 1 And CboBenAccount.Items.Count = 0) Then
            PopulateMessage(True, "There is only one account available for an internal transfer in " & ClsPay.CCY, False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        End If
    End Sub

    Private Sub OrderMessagesIMSRefCheck()
        If Not ClsIMS.IsIMSRefValid(txtIMSRef.Text) Then
            PopulateMessage(True, "WARNING: The associated IMS reference(s) entered has not been found in IMS", False, 0, 0, "")
            TabMovement.SelectedTab = OrderTab
        End If
    End Sub

    Private Sub OrderMessageFOPFee()
        If ClsPay.OtherID = cPayDescInOtherFOPFee Then
            Dim Lst = ClsIMS.GetDataToList("select top 1 cf_MovementFee,cf_MovementFeeCCY from vwDolfinPaymentCustodyFeeLinksSafekeeping where pf_code = " & CboOrderAccount.SelectedValue)
            If Not Lst Is Nothing Then
                PopulateMessage(True, "FOP Fee from fee schedule is " & Lst("cf_MovementFee") & " " & Lst("cf_MovementFeeCCY"), True, 0, 0, "")
                TabMovement.SelectedTab = OrderTab
            End If
        End If
    End Sub

    Private Sub BenMessages()
        Dim varPendingAmount As Double = 0

        ClearBenMessages()
        varPendingAmount = ClsIMS.GetPendingAmount(ClsPay.SelectedPaymentID, ClsPay.PortfolioCode, ClsPay.BeneficiaryCCYCode, ClsPay.BeneficiaryAccountCode)

        'when cross currency required
        'If CboCCY.Text <> cboBenCCY.Text Then
        ' varPendingAmount = varPendingAmount * ClsPay.ExchangeRate
        'End If

        If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) And CboBenAccount.Text <> "" Then
            BenMessagesFormDetails()
        End If

        If CboBenAccount.Text <> "" Then
            BenMessagesBalance()
            BenMessagesBalanceInc(varPendingAmount)
            BenMessagesPostBalanceInc(varPendingAmount)
        End If

        If ClsPay.SelectedPaymentID <> 0 And (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExIn)) And ClsPay.IMSReturned Then
            PopulateMessage(False, "IMS:" & ClsPay.IMSStatus, IIf(ClsPay.IMSAcknowledged, True, False), 0, 0, "")
        End If
        PopulateComments()
        PopulatePreviews()
    End Sub

    Private Sub BenMessagesFormDetails()
        If ClsIMS.PaymentSendsSwift(CboPayType.Text) And ClsPay.BeneficiaryAccountNumber = "" Then
            PopulateMessage(False, "The beneficiary account number must be populated in IMS for this beneficiary account (Free field 1)", False, 0, 0, "")
            TabMovement.SelectedTab = BeneficiaryTab
        End If
        If (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or ClsIMS.PaymentSendsSwift(CboPayType.Text)) And txtBenSwift.Text = "" Then
            PopulateMessage(False, "A beneficiary swift code is required for SWIFT payment", False, 0, 0, "")
            TabMovement.SelectedTab = BeneficiaryTab
        End If
        If (CboPayType.Text = ClsIMS.GetPaymentType(cPayDescIn) Or ClsIMS.PaymentSendsSwift(CboPayType.Text)) And txtBenIBAN.Text = "" Then
            PopulateMessage(False, "A beneficiary IBAN is required for SWIFT payment", False, 0, 0, "")
            TabMovement.SelectedTab = BeneficiaryTab
        End If
    End Sub

    Private Sub BenMessagesBalance()
        txtBenBalance.Text = ClsPay.BeneficiaryBalance.ToString("#,##0.00")
        ColorBalanceTextBox(txtBenBalance)
        If CDbl(IIf(Not IsNumeric(txtAmount.Text), 0, txtAmount.Text)) <= 0 Then
            PopulateMessage(False, "Please enter a movement amount greater than 0", False, 0, 0, "")
            TabMovement.SelectedTab = BeneficiaryTab
        End If
    End Sub

    Private Sub BenMessagesBalanceInc(ByVal varPendingAmount As Double)
        If IsNumeric(txtBenBalance.Text) Then
            If CboCCY.Text <> cboBenCCY.Text Then
                txtBenBalanceInc.Text = ((varPendingAmount) + CDbl(txtBenBalance.Text)).ToString("#,##0.00")
            Else
                txtBenBalanceInc.Text = (varPendingAmount + CDbl(txtBenBalance.Text)).ToString("#,##0.00")
            End If
            ColorBalanceTextBox(txtBenBalanceInc)
        End If
    End Sub

    Private Sub BenMessagesPostBalanceInc(ByVal varPendingAmount As Double)
        If txtAmount.Text = "" Then
            If IsNumeric(txtBenBalance.Text) Then
                txtBenBalancePostInc.Text = (varPendingAmount + CDbl(txtBenBalance.Text)).ToString("#,##0.00")
                ColorBalanceTextBox(txtBenBalancePostInc)
            End If
        ElseIf IsNumeric(txtAmount.Text) And IsNumeric(txtBenBalance.Text) Then
            txtBenBalancePostInc.Text = ((CDbl(txtAmount.Text)) + (varPendingAmount + CDbl(txtBenBalance.Text))).ToString("#,##0.00")
            ColorBalanceTextBox(txtBenBalancePostInc)
        End If
    End Sub

    Private Sub BenMessagesPostBalance(ByVal varPendingAmount As Double)
        If txtAmount.Text = "" Then
            If IsNumeric(txtBenBalance.Text) Then
                txtBenBalancePostInc.Text = ((varPendingAmount + CDbl(txtBenBalance.Text))).ToString("#,##0.00")
                ColorBalanceTextBox(txtBenBalancePostInc)
            End If
        ElseIf IsNumeric(txtAmount.Text) And IsNumeric(txtBenBalance.Text) Then
            txtBenBalancePostInc.Text = ((CDbl(txtAmount.Text)) + (varPendingAmount + CDbl(txtBenBalance.Text))).ToString("#,##0.00")
            ColorBalanceTextBox(txtBenBalancePostInc)
        End If
    End Sub

    Private Sub CboOrderAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboOrderAccount.SelectedIndexChanged
        If IsNumeric(CboOrderAccount.SelectedValue) And CboOrderAccount.Text <> "" Then
            If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                 ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                 ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                If CboOrderAccount.SelectedValue = ClsPay.BeneficiaryAccountCode Then
                    ClsPay.BeneficiaryAccountCode = -1
                    ClsPay.SelectedOrder = 2
                    PopulateExtraFormDetails()
                End If

                ClsPay.SelectedOrder = 1
                ClsPay.OrderAccountCode = CboOrderAccount.SelectedValue.ToString()
                ClsPay.OrderAccount = CboOrderAccount.Text
                PopulateExtraFormDetails()

                If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) And Not ClsPay.BeneficaryAccountFiltered Then
                    ClsPay.BeneficaryAccountFiltered = True
                    ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.BeneficiaryPortfolioCode, ClsPay.BeneficiaryCCYCode, ClsPay.OrderAccountCode, varUserLocationCode)
                    ClsPay.BeneficaryAccountFiltered = False
                    ClearBenMessages()
                End If
            Else
                ClsIMS.PopulateFormCombo(CboOrderName, ClsPay.CCYCode, CboOrderAccount.SelectedValue,, varUserLocationCode)
                If ClsPay.SelectedPaymentID = 0 Then
                    CboBenAccount.DataSource = Nothing
                    CboBenName.DataSource = Nothing
                End If
                ClearOrderAddress()
                ClearBenAddress()
                ClearOrderMessages()
                ClearBenMessages()
                ClsPay.Portfolio = CboOrderAccount.Text
                ClsPay.PortfolioCode = CboOrderAccount.SelectedValue
                ClsPay.OrderRef = ClsIMS.GenerateRef(ClsPay.PaymentType, ClsPay.SelectedPaymentID, ClsPay.OtherID)
                txtOrderRef.Text = ClsPay.OrderRef
            End If
        End If
    End Sub

    Public Sub RunRefreshHistoryGrid()
        If (ClsPay.SelectedPaymentID <> 0) Or
            ((ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
            ((ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
            ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther)) And CboOrderAccount.Text <> "" And CboCCY.Text <> "") Or
                (ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And CboPortfolio.Text <> "" And CboCCY.Text <> ""))) Then
            If CboOrderName.Text <> "" And ClsPay.OrderAccountCode <> 0 Then
                ClsIMS.RefreshHistoryGrid(dgvHistory, True)
            Else
                ClsIMS.RefreshHistoryGrid(dgvHistory, False)
            End If

            ClsPay.FormatHistoryGrid(dgvHistory)
        End If
    End Sub


    Private Sub CboOrderAccount_Leave(sender As Object, e As EventArgs) Handles CboOrderAccount.Leave
        If (IsNumeric(CboOrderAccount.SelectedValue) And
            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
            ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn)) Then
            OrderMessages()
        End If

        If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientOut) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            BeneficiaryGroupBankGroupBox.Visible = False
            BeneficiarySubBankGroupBox.Visible = False
        End If
        RunRefreshHistoryGrid()
    End Sub


    Private Sub CboBenAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboBenAccount.SelectedIndexChanged
        If IsNumeric(CboBenAccount.SelectedValue) Then
            If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                If ClsPay.BeneficaryAccountFiltered Then
                    ClsPay.BeneficiaryAccountCode = -1
                    ClsPay.SelectedOrder = 2
                    PopulateExtraFormDetails()
                    CboBenAccount.SelectedValue = -1
                Else
                    If CboBenAccount.SelectedValue = ClsPay.OrderAccountCode Then
                        ClsPay.OrderAccountCode = -1
                    End If

                    ClsPay.SelectedOrder = 2
                    ClsPay.BeneficiaryAccountCode = CboBenAccount.SelectedValue.ToString()
                    ClsPay.BeneficiaryAccount = CboBenAccount.Text
                    PopulateExtraFormDetails()
                    ClsPay.BeneficaryAccountFiltered = False
                End If

                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
                    ChkBenTemplateEdit.Focus()
                End If
            ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Then
                ClsIMS.PopulateFormCombo(CboBenName, ClsPay.BeneficiaryCCYCode, CboBenAccount.SelectedValue, CboOrderName.SelectedValue)
            Else
                ClsIMS.PopulateFormCombo(CboBenName, ClsPay.BeneficiaryCCYCode, CboBenAccount.SelectedValue)
                ClearBenAddress()
                ClearBenMessages()
            End If
        End If
    End Sub

    Private Sub FormControlsEnable(ByVal SelectedOrder As Integer, ByVal varEnabled As Boolean)
        If SelectedOrder = 1 Then
            For Each ctrl In Me.OrderGroupBox.Controls
                If ctrl.tag = "ControlAddress" Then
                    ctrl.enabled = varEnabled
                End If
            Next
        ElseIf SelectedOrder = 2 Then
            For Each ctrl In Me.TabBeneficiary.Controls
                If LCase(ctrl.name) = "maintab" Then
                    For Each ctrl2 In Me.MainTab.Controls
                        If ctrl2.tag = "ControlAddressBen" Then
                            ctrl2.enabled = varEnabled
                        End If
                    Next
                ElseIf LCase(ctrl.name) = "bankdetailstab" Then
                    For Each ctrl2 In Me.BankDetailsTab.Controls
                        If ctrl2.tag = "ControlAddressBen" Then
                            ctrl2.enabled = varEnabled
                        End If
                    Next
                End If
            Next
        ElseIf SelectedOrder = 3 Then
            For Each ctrl In Me.BeneficiarySubBankGroupBox.Controls
                If ctrl.tag = "ControlAddressIntermediary" Then
                    ctrl.enabled = varEnabled
                End If
            Next
        ElseIf SelectedOrder = 4 Then
            For Each ctrl In Me.BeneficiaryBankGroupBox.Controls
                If ctrl.tag = "ControlAddressBenBank" Then
                    ctrl.enabled = varEnabled
                End If
            Next
        End If
    End Sub


    Private Sub CboBenAccount_Leave(sender As Object, e As EventArgs) Handles CboBenAccount.Leave
        If IsNumeric(CboBenAccount.SelectedValue) Then
            If ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) And
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                If (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut)) And Not ClsPay.PortfolioIsFee Then
                    If CboPayType.Text <> "" And CboPortfolio.Text <> "" And cboBenCCY.Text <> "" Then
                        ClsPay.SelectedTemplateName = CboBenAccount.Text
                        PopulateTemplateFormDetails(CboPortfolio.SelectedValue, cboBenCCY.SelectedValue, ClsPay.SelectedTemplateName)
                        ChkBenTemplateEdit.Checked = True
                        ChkBenTemplateEdit.Checked = False
                    End If
                End If
                BenMessages()
            End If
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescIn) Then
                If CboBenAccount.Text <> "" And txtBenSubBankSwift.Text <> "" Then
                    BeneficiaryGroupBankGroupBox.Visible = True
                    BeneficiarySubBankGroupBox.Visible = True
                    BeneficiaryBankGroupBox.Visible = False
                    ChkBenSubBank.Checked = True
                    ChkBenSubBank.Enabled = False
                    FormControlsEnable(3, False)
                Else
                    BeneficiaryGroupBankGroupBox.Visible = False
                    BeneficiarySubBankGroupBox.Visible = False
                End If
            ElseIf (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn)) And CboBenName.Text = "" Then
                BeneficiaryGroupBankGroupBox.Visible = False
                BeneficiarySubBankGroupBox.Visible = False
            End If
        Else
            txtBenBalance.Text = ""
            txtBenBalanceInc.Text = ""
            txtBenBalancePostInc.Text = ""
        End If
    End Sub

    Private Sub CboOrderName_Leave(sender As Object, e As EventArgs) Handles CboOrderName.Leave
        If IsNumeric(CboOrderName.SelectedValue) Then
            If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
                ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                ClsPay.SelectedOrder = 1
                ClsPay.PortfolioCode = CboOrderAccount.SelectedValue
                ClsPay.BeneficiaryAccountCode = 0
                ClsPay.OrderAccountCode = CboOrderName.SelectedValue
                PopulateExtraFormDetails()
                OrderMessages()
                If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.CCYCode, CboPortfolio.SelectedValue, CboOrderAccount.SelectedValue)
                ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.OtherID = cPayDescInOtherIntTra Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, 742, ClsPay.CCYCode)
                ElseIf ClsPay.OtherID = cPayDescInOtherFXBuy Or ClsPay.OtherID = cPayDescInOtherFXSell Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, CboOrderAccount.SelectedValue, ClsPay.CCYCode)
                ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Then
                    ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.CCYCode)
                Else
                    ClsIMS.PopulateFormCombo(CboBenAccount, ClsPay.CCYCode, CboOrderName.SelectedValue, CboOrderAccount.SelectedValue)
                End If
                'End If
            Else
                txtOrderBalance.Text = ""
                txtOrderBalanceInc.Text = ""
                txtOrderBalancePostInc.Text = ""
            End If
            RunRefreshHistoryGrid()
        End If
    End Sub

    Private Sub CboBenName_Leave(sender As Object, e As EventArgs) Handles CboBenName.Leave
        If ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan) Or
            ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
            ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientIn) Then
            If IsNumeric(CboBenName.SelectedValue) Then
                ClsPay.SelectedOrder = 2
                ClsPay.PortfolioCode = CboOrderAccount.SelectedValue
                ClsPay.BeneficiaryPortfolioCode = CboBenAccount.SelectedValue
                ClsPay.OrderAccountCode = 0
                ClsPay.BeneficiaryAccountCode = CboBenName.SelectedValue
                ClsPay.OrderAccountCode = CboOrderName.SelectedValue
                PopulateExtraFormDetails()
                BenMessages()
                'End If

                If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then
                    If CboBenName.Text <> "" And txtBenSubBankSwift.Text <> "" Then
                        BeneficiaryGroupBankGroupBox.Visible = True
                        BeneficiarySubBankGroupBox.Visible = True
                        BeneficiaryBankGroupBox.Visible = False
                        ChkBenSubBank.Checked = True
                        'ChkBenSubBank.Enabled = False
                        FormControlsEnable(3, False)
                    Else
                        BeneficiaryGroupBankGroupBox.Visible = False
                        BeneficiarySubBankGroupBox.Visible = False
                    End If
                End If
            Else
                txtBenBalance.Text = ""
                txtBenBalanceInc.Text = ""
                txtBenBalancePostInc.Text = ""
            End If
        End If
        SelectImageSWIFTIBAN(2, "swift", txtBenSwift, PicBenSwift)
        SelectImageSWIFTIBAN(2, "iban", txtBenIBAN, PicBenIBAN)
        PopulateComments()
        PopulatePreviews()
    End Sub

    Private Sub CboBenBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboBenBankName.SelectedIndexChanged
        If CboBenBankName.Text <> "" Then
            ClsPay.SelectedOrder = 4
            ClsPay.SelectedBeneficiaryBankSwiftCode = CboBenBankName.SelectedValue.ToString()
            If Not varPopulateExtraFormDetailsFromSwiftCode Then
                PopulateExtraFormDetails()
            End If
        Else
            clearBenBank()
        End If
        BenMessages()
    End Sub

    Private Sub txtAmount_Leave(sender As Object, e As EventArgs) Handles txtAmount.Leave
        If IsNumeric(txtAmount.Text) Then
            txtAmount.Text = CDbl(txtAmount.Text).ToString("#,##0.00")
            ShowExchangeRate()
        End If
        OrderMessages()
        BenMessages()
    End Sub

    Private Function AmountVsBalance() As Boolean
        AmountVsBalance = True
        If CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExIn) Then
            If IsNumeric(txtAmount.Text) And IsNumeric(txtOrderBalance.Text) Then
                If CDbl(txtAmount.Text) > CDbl(txtOrderBalance.Text) Then
                    AmountVsBalance = False
                End If
            End If
        End If
    End Function

    Private Function PendingAmountMoreThanBalance(Optional ByRef varPendingAmount As Double = 0) As Boolean
        PendingAmountMoreThanBalance = False
        Dim PendingAmount As Double = ClsIMS.GetPendingAmount(ClsPay.SelectedPaymentID, ClsPay.PortfolioCode, ClsPay.CCYCode, ClsPay.OrderAccountCode)
        varPendingAmount = PendingAmount
        If CboPayType.Text <> ClsIMS.GetPaymentType(cPayDescExIn) Then
            If IsNumeric(txtAmount.Text) And IsNumeric(txtOrderBalance.Text) Then
                If CDbl(txtAmount.Text) > Strings.Format(CDbl(PendingAmount) + CDbl(txtOrderBalance.Text), "#,##0.00") Then
                    PendingAmountMoreThanBalance = True
                End If
            End If
        End If
    End Function

    Private Sub dtSettleDate_Leave(sender As Object, e As EventArgs) Handles dtSettleDate.Leave
        OrderMessages()
    End Sub

    Private Sub TimerBroker_Tick(sender As Object, e As EventArgs) Handles TimerBroker.Tick
        ts = ts.Subtract(New TimeSpan(0, 0, 1))
        If CutOffTimePassed() Then
            TimerBroker.Enabled = False
            varLblBroker.Text = "You have now passed the cut off time. The cut off time for this broker was at " & ClsPay.CutOffTimeGMT & " GMT"
        ElseIf dtSettleDate.Value.Date = Now.Date And ClsPay.CutOffTime <> "" Then
            varLblBroker.Text = "The cut off time for this broker is at " & ClsPay.CutOffTimeGMT & " GMT (" & String.Format("{0}", ts) & ")"
        Else
            TimerBroker.Enabled = False
        End If
    End Sub

    Private Sub txtOrderSwift_Leave(sender As Object, e As EventArgs) Handles txtOrderSwift.Leave
        SelectImageSWIFTIBAN(1, "swift", txtOrderSwift, PicOrderSwift)
    End Sub

    Private Sub txtBenBankSwift_Leave(sender As Object, e As EventArgs) Handles txtBenBankSwift.Leave
        SelectImageSWIFTIBAN(4, "swift", txtBenBankSwift, PicBenBankSwift)
        PopulatePreviews()
    End Sub

    Private Sub txtBenIBAN_Leave(sender As Object, e As EventArgs) Handles txtBenIBAN.Leave
        SelectImageSWIFTIBAN(2, "iban", txtBenIBAN, PicBenIBAN)
        PopulatePreviews()
    End Sub

    Private Sub txtOrderIBAN_Leave(sender As Object, e As EventArgs) Handles txtOrderIBAN.Leave
        SelectImageSWIFTIBAN(1, "iban", txtOrderIBAN, PicOrderIBAN)
    End Sub

    Private Sub txtBenBankIBAN_Leave(sender As Object, e As EventArgs) Handles txtBenBankIBAN.Leave
        SelectImageSWIFTIBAN(4, "iban", txtBenBankIBAN, PicBenBankIBAN)
        PopulatePreviews()
    End Sub

    Private Sub SelectImageSWIFTIBAN(ByRef SelectedOrder As Integer, ByRef txtboxtype As String, ByRef txtbox As TextBox, ByRef txtboxPic As PictureBox)
        On Error Resume Next
        If txtbox.Text <> "" Then
            txtbox.Text = Strings.UCase(txtbox.Text)
            If LCase(txtboxtype) = "swift" Then
                ClsPay.SelectedOrder = SelectedOrder
                If PopulateExtraFormDetailsFromSwiftCode() Then
                    txtboxPic.Image = ImageListTickCross.Images(1)
                Else
                    txtboxPic.Image = ImageListTickCross.Images(0)
                End If
            ElseIf LCase(txtboxtype) = "iban" Then
                If ValidIBAN(txtbox.Text) Then
                    txtboxPic.Image = ImageListTickCross.Images(1)
                Else
                    txtboxPic.Image = ImageListTickCross.Images(0)
                End If
            Else
                txtboxPic.Image = Nothing
            End If
        Else
            txtboxPic.Image = Nothing
        End If
    End Sub

    Private Sub CboBenBankName_Leave(sender As Object, e As EventArgs) Handles CboBenBankName.Leave
        SelectImageSWIFTIBAN(4, "swift", txtBenBankSwift, PicBenBankSwift)
        SelectImageSWIFTIBAN(4, "iban", txtBenBankIBAN, PicBenBankIBAN)
        PopulatePreviews()
    End Sub

    Private Sub LoadNewItem()
        TimerRefresh.Enabled = True
        CboCCY.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboCCY.AutoCompleteSource = AutoCompleteSource.ListItems
        CboPortfolio.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboPortfolio.AutoCompleteSource = AutoCompleteSource.ListItems
        CboOrderAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboOrderAccount.AutoCompleteSource = AutoCompleteSource.ListItems
        CboBenAccount.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboBenAccount.AutoCompleteSource = AutoCompleteSource.ListItems
        CboOrderName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboOrderName.AutoCompleteSource = AutoCompleteSource.ListItems
        CboBenName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        CboBenName.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsPay.ClearClass()
        TempDirectory = ""
    End Sub

    Private Sub RefreshNewItem()
        RefreshFiles()
        If CboPayType.Items.Count = 0 Then
            ClsIMS.PopulatecboPaymentTypes(CboPayType)
        End If
        If CboFilterStatus.Items.Count = 0 Then
            ClsIMS.PopulatecbostatusTypes(CboFilterStatus)
        End If
        If cboFilterType.Items.Count = 0 Then
            ClsIMS.PopulatecboPaymentAllTypes(cboFilterType)
        End If
        If cboFilterLocation.Items.Count = 0 Then
            ClsIMS.PopulatecboLocationTypes(cboFilterLocation)
        End If

        If cboPayCheck.Items.Count = 0 Then
            ClsIMS.PopulatecboPayCheck(cboPayCheck)
        End If
        cboPayCheck.SelectedValue = -1
        OrderMessages()
        BenMessages()
    End Sub

    Private Sub RefreshFiles()
        Try
            If ClsPay.PaymentFilePath = "" And TempDirectory <> "" Then
                ClsPay.PaymentFilePath = TempDirectory
            ElseIf Not Directory.Exists(ClsPay.PaymentFilePathRemote) Then
                'ClsPay.GetTempDirectory(ClsIMS.GetFilePath("Default"))
                'TempDirectory = ClsPay.PaymentFilePathRemote
            End If

            ExtractAssociatedIconEx()
            PopulateBrowserTreeView(ClsPay.PaymentFilePathRemote)
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCashMovementsFiles, Err.Description & " - RefreshFiles")
        End Try
    End Sub

    Public Sub PopulateBrowserTreeView(ByVal varPaymentFilePath As String)
        Try
            TreeViewFiles.Nodes.Clear()
            TreeViewFiles.ImageList = ImageListFiles
            TreeViewFiles.Nodes.Add(Path.GetPathRoot(varPaymentFilePath))
            TreeViewFiles.ImageIndex = 1

            Dim filepath As String = Replace(varPaymentFilePath, Path.GetPathRoot(varPaymentFilePath) & "\", "")
            Dim directoryname As String

            Dim rootnode As TreeNode = TreeViewFiles.Nodes(0)
            rootnode.ImageIndex = 0
            rootnode.SelectedImageIndex = 0
            Dim parentnode As TreeNode = TreeViewFiles.Nodes(0)

            Dim NodeLevel As Integer = 0
            While filepath <> ""
                If InStr(1, filepath, "\", vbTextCompare) <> 0 Then
                    directoryname = Strings.Left(filepath, InStr(1, filepath, "\", vbTextCompare) - 1)
                    If NodeLevel = 0 Then
                        parentnode = rootnode.Nodes.Add(directoryname)
                    Else
                        parentnode = parentnode.Nodes.Add(directoryname)
                    End If
                    filepath = Replace(filepath, directoryname & "\", "")
                Else
                    directoryname = filepath
                    If NodeLevel = 0 Then
                        parentnode = rootnode.Nodes.Add(directoryname)
                    Else
                        parentnode = parentnode.Nodes.Add(directoryname)
                    End If
                    filepath = ""
                End If
                parentnode.ImageIndex = 1
                parentnode.SelectedImageIndex = 1
                NodeLevel = NodeLevel + 1
            End While
            TreeViewFiles.ExpandAll()
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameCashMovementsFiles, Err.Description & " - PopulateBrowserTreeView")
        End Try
    End Sub

    Public Sub ExtractAssociatedIconEx()
        Try
            LVFiles.Items.Clear()

            Dim ImageFileList As ImageList
            ImageFileList = New ImageList()

            If Directory.Exists(ClsPay.PaymentFilePathRemote) Then

                LVFiles.SmallImageList = ImageFileList
                LVFiles.View = View.SmallIcon

                ' Get the payment file path directory.
                Dim dir As New DirectoryInfo(ClsPay.PaymentFilePathRemote)

                Dim item As ListViewItem
                LVFiles.BeginUpdate()
                Dim file As FileInfo
                For Each file In dir.GetFiles()
                    ' Set a default icon for the file.
                    Dim iconForFile As Icon = SystemIcons.WinLogo
                    item = New ListViewItem(file.Name, 1)

                    ' Check to see if the image collection contains an image
                    ' for this extension, using the extension as a key.
                    If Not (ImageFileList.Images.ContainsKey(file.Extension)) Then
                        ' If not, add the image to the image list.
                        'iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName)
                        ImageFileList.Images.Add(file.Extension, iconForFile)
                    End If
                    item.ImageKey = file.Extension
                    LVFiles.Items.Add(item)

                Next file
                LVFiles.EndUpdate()
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusWarning, cAuditGroupNameCashMovementsFiles, Err.Description & " - ExtractAssociatedIconEx: ")
        End Try
    End Sub

    Private Sub LVFiles_DragDrop(sender As Object, e As DragEventArgs) Handles LVFiles.DragDrop
        RefreshFiles()
        If Directory.Exists(ClsPay.PaymentFilePathRemote) Then
            If e.Data.GetDataPresent(DataFormats.FileDrop) Then
                Dim MyFiles() As String
                Dim i As Integer

                MyFiles = e.Data.GetData(DataFormats.FileDrop)
                ' Loop through the array and add the files to the list.
                For i = 0 To MyFiles.Length - 1
                    If Not File.Exists(ClsPay.PaymentFilePathRemote & Dir(MyFiles(i))) Then
                        File.Copy(MyFiles(i), ClsPay.PaymentFilePathRemote & Dir(MyFiles(i)))
                    End If
                Next
                ExtractAssociatedIconEx()
            ElseIf e.Data.ToString = "System.Windows.Forms.DataObject" Then
                Try
                    Dim OL As Interop.Outlook.Application = GetObject(, "Outlook.Application")

                    Dim mi As Interop.Outlook.MailItem
                    Dim MsgCount As Integer = 1
                    If OL.ActiveExplorer.Selection.Count > 0 Then
                        For Each mi In OL.ActiveExplorer.Selection()
                            If (TypeOf mi Is Interop.Outlook.MailItem) Then
                                mi.SaveAs(ClsPay.PaymentFilePathRemote & "M" & MsgCount & "_" & mi.SenderName.ToString & "_" & mi.ReceivedTime.ToString("yyyy-MM-dd HHmm") & ".msg")
                                MsgCount = MsgCount + 1
                            End If
                        Next
                        mi = Nothing
                        OL = Nothing
                    End If
                    ExtractAssociatedIconEx()
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub

    Private Sub LVFiles_DragEnter(sender As Object, e As DragEventArgs) Handles LVFiles.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Or e.Data.ToString = "System.Windows.Forms.DataObject" Then
            e.Effect = DragDropEffects.All
        End If
    End Sub

    Private Sub LVFiles_DragLeave(sender As Object, e As EventArgs) Handles LVFiles.DragLeave
        Cursor = Cursors.Default
    End Sub

    Private Sub FrmPayment_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        ClsPay.DeleteFileDirectory(TempDirectory)
        ClsPay.ClearClass()
        TimerRefresh.Enabled = False
    End Sub

    Private Sub LVFiles_ItemMouseHover(sender As Object, e As ListViewItemMouseHoverEventArgs) Handles LVFiles.ItemMouseHover
        LVFiles.ContextMenuStrip = ContextMenuStripFiles
    End Sub

    Private Sub OpenFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            ShellExecute(ClsPay.PaymentFilePathRemote & LVFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub RemoveFileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveFileToolStripMenuItem.Click
        If LVFiles.SelectedItems.Count > 0 Then
            File.Delete(ClsPay.PaymentFilePathRemote & LVFiles.SelectedItems(0).Text)
            ExtractAssociatedIconEx()
        End If
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        ExtractAssociatedIconEx()
    End Sub

    Private Sub FrmPayment_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Cursor = Cursors.WaitCursor
        ClsIMS.RefreshMovementGrid(dgvCash, GetFilterWhereClause())
        Cursor = Cursors.Default
    End Sub

    Private Sub PicPayType_Click(sender As Object, e As EventArgs) Handles PicPayType.Click
        Dim varTip As String

        If CboPayType.SelectedValue = cPayDescInOther Then
            varTip = "Cover Charge - Sends a Swift - allows client to firm and visa versa cover charge booking for swift and IMS (both in and outbound transaction movement)" & vbNewLine & vbNewLine
            varTip = varTip & "Commission Fee - Sends a Swift - allows client to firm and visa versa commission fee booking for swift and IMS (both in and outbound transaction movement)" & vbNewLine & vbNewLine
            varTip = varTip & "Payment Fee - Sends a Swift - allows client to firm and visa versa payment fee booking for swift and IMS (both in and outbound transaction movement)" & vbNewLine & vbNewLine
            varTip = varTip & "Commission (Fee in Price) - Sends a Swift - allows client to firm and visa versa payment fee booking for swift and IMS (both in and outbound swift but only order outbound in IMS)" & vbNewLine & vbNewLine
            varTip = varTip & "Internal Transfer - Sends a Swift - allows money movement between the same currency but different accounts (no FX)" & vbNewLine & vbNewLine
        Else
            varTip = "Internal Client to Client - Does not send a swift - allows internal movement between clients within the same institution" & vbNewLine & vbNewLine
            varTip = varTip & "Internal Institutional Account Move - Sends a swift message for accounts setup for the portfolio selected - From order to beneficiary ('BANK', 'PRIVATE BANKING' and 'Custodian' types can pay out)" & vbNewLine & vbNewLine
            varTip = varTip & "Internal Management Fee - Sends a swift message for accounts setup for internal money movements between the client and the firm" & vbNewLine & vbNewLine
            varTip = varTip & "External Client Account Move - Sends a swift message for accounts setup for the client selected - From client's portfolio to other other portfolio same ccy ('BANK', 'PRIVATE BANKING' and 'Custodian' types can pay out). Movement can be instructed by the client. " & vbNewLine & vbNewLine
            varTip = varTip & "External Client Payment - Sends a swift message to pay an external entity i.e. bank, person, business (Only 'BANK' types can pay out). Similar to External payment but payment can be instructed by the client" & vbNewLine & vbNewLine
            varTip = varTip & "External Payment - Sends a swift message to pay an external entity i.e. bank, person, business (Only 'BANK' types can pay out)" & vbNewLine & vbNewLine
            varTip = varTip & "External Receipt - Does not send a swift - Select for incoming funds into the selected portfolio" & vbNewLine & vbNewLine
        End If

        MsgBox(varTip, vbInformation, "Options for cash movements")
    End Sub

    Private Sub TimerRefresh_Tick(sender As Object, e As EventArgs) Handles TimerRefresh.Tick
        Cursor = Cursors.WaitCursor
        ClsIMS.RefreshMovementGrid(dgvCash, GetFilterWhereClause())
        Cursor = Cursors.Default
    End Sub

    Public Sub UpdateG(ByVal dgv As DataGridView)
        For i As Integer = 0 To dgvCash.RowCount - 1
            If dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusReady) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkGreen
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusPendingAuth) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkGoldenrod
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusPendingReview) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkViolet
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusCancelled) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.Black
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusSent) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkBlue
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusSwiftAck) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.MediumSeaGreen
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusIMSAck) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkSeaGreen
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusSwiftErr) Or dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusSwiftRej) Or dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusIMSErr) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.DarkRed
            ElseIf dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusAwaitingIMS) Or dgv.Rows(i).Cells(1).Value = ClsIMS.GetStatusType(cStatusAwaitingSwift) Then
                dgv.Rows(i).Cells(1).Style.ForeColor = Color.Chocolate
            End If
        Next
    End Sub

    Private Sub lblordermsg1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg1.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg2.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg3.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg4_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg4.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg5_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg5.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg6_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg6.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblordermsg7_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblordermsg7.LinkClicked
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblbenmsg1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblbenmsg2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub lblbenmsg3_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs)
        System.Diagnostics.Process.Start(e.Link.LinkData.ToString)
    End Sub

    Private Sub txtSubBenBankIBAN_Leave(sender As Object, e As EventArgs) Handles txtBenSubBankIBAN.Leave
        SelectImageSWIFTIBAN(3, "iban", txtBenSubBankIBAN, PicBenSubBankIBAN)
        PopulatePreviews()
    End Sub

    Private Sub CboSubBenBankName_Leave(sender As Object, e As EventArgs) Handles CboBenSubBankName.Leave
        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
        SelectImageSWIFTIBAN(3, "iban", txtBenSubBankIBAN, PicBenSubBankIBAN)
        PopulatePreviews()
    End Sub

    Private Sub txtSubBenBankSwift_Leave(sender As Object, e As EventArgs) Handles txtBenSubBankSwift.Leave
        SelectImageSWIFTIBAN(3, "swift", txtBenSubBankSwift, PicBenSubBankSwift)
        PopulatePreviews()
    End Sub

    Private Sub CboBenSubBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboBenSubBankName.SelectedIndexChanged
        If CboBenSubBankName.Text <> "" Then
            ClsPay.SelectedOrder = 3
            ClsPay.SelectedBeneficiarySubBankSwiftCode = CboBenSubBankName.SelectedValue.ToString()
            If Not varPopulateExtraFormDetailsFromSwiftCode Then
                PopulateExtraFormDetails()
            End If
        Else
            clearBenSubBank()
        End If
        BenMessages()
    End Sub

    Public Function GetFilterWhereClause() As String
        GetFilterWhereClause = ""
        If txtFilterID.Text <> "" Then
            If IsNumeric(txtFilterID.Text) Then
                GetFilterWhereClause = GetFilterWhereClause & " and id = " & txtFilterID.Text
            Else
                GetFilterWhereClause = GetFilterWhereClause & " and (OrderRef = '" & txtFilterID.Text & "' or BenRef = '" & txtFilterID.Text & "')"
            End If
        End If
        If cboFilterLocation.Text <> "" Then
            GetFilterWhereClause = GetFilterWhereClause & " and (Branchcode = " & cboFilterLocation.SelectedValue & ") and transdate >= dbo.DolfinPaymentAddBusinessDays(getdate(),-14)"
        End If
        If CboFilterStatus.Text <> "" Then
            GetFilterWhereClause = GetFilterWhereClause & " and (status = '" & CboFilterStatus.Text & "') and transdate >= dbo.DolfinPaymentAddBusinessDays(getdate(),-14)"
        End If
        If cboFilterType.Text <> "" Then
            GetFilterWhereClause = GetFilterWhereClause & " and (paymenttype = '" & cboFilterType.Text & "') and transdate >= dbo.DolfinPaymentAddBusinessDays(getdate(),-14)"
        End If
        If cboPayCheck.SelectedValue >= 0 Then
            GetFilterWhereClause = GetFilterWhereClause & " and cast(transdate as date) = dbo.DolfinPaymentAddBusinessDays(getdate(),-" & IIf(Not IsNumeric(cboPayCheck.SelectedValue), 0, cboPayCheck.SelectedValue) & ")"
        End If
        If GetFilterWhereClause <> "" Then
            GetFilterWhereClause = "Where " & Strings.Right(GetFilterWhereClause, Len(GetFilterWhereClause) - 4)
        End If
    End Function

    Private Sub ChkCashRefresh_CheckedChanged(sender As Object, e As EventArgs) Handles ChkCashRefresh.CheckedChanged
        If ChkCashRefresh.Checked Then
            TimerRefresh.Enabled = True
        Else
            TimerRefresh.Enabled = False
        End If
    End Sub

    Private Sub ChkBenSubBank_CheckedChanged(sender As Object, e As EventArgs) Handles ChkBenSubBank.CheckedChanged
        If Not ChkBenSubBank.Checked Then
            CboBenSubBankName.Text = ""
            clearBenSubBank()
        ElseIf ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExClientOut) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescExVolopaOut) Then
            ChkBenTemplateEdit.Checked = True
        End If
        FormControlsEnable(3, ChkBenSubBank.Checked)
    End Sub

    Private Sub ChkBenTemplate_CheckedChanged(sender As Object, e As EventArgs)
        Dim IsUnique As Boolean = False

        If CboBenName.Enabled Then
            If Not ChkBenTemplate.Checked Then
                ClsPay.BeneficiaryTemplateName = ""
            Else
                While Not IsUnique
                    Dim myTemp As String = InputBox("Please enter a unique template name" & vbNewLine & "(100 chars max)", "Template name required", CboPortfolio.Text & " - " & CboCCY.Text & " - " & CboBenName.Text)
                    If myTemp <> "" Then
                        IsUnique = False
                        If ClsIMS.IsTemplateNameUnique(myTemp) Then
                            ClsPay.BeneficiaryTemplateName = Strings.Left(myTemp, 100)
                            IsUnique = True
                        Else
                            MsgBox("This template name is not unique. Please enter another name or cancel", vbExclamation, "Unique name required")
                        End If
                    Else
                        ChkBenTemplate.Checked = False
                        IsUnique = True
                    End If
                End While
            End If
        Else
            If ChkBenTemplate.Checked Then
                MsgBox("These details are already saved as a template" & vbNewLine & "If you want a new template, select edit populated details", vbExclamation, "Template already saved")
                ChkBenTemplate.Checked = False
            End If
        End If
    End Sub

    Private Sub cboBenCCY_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBenCCY.SelectedIndexChanged
        If IsNumeric(cboBenCCY.SelectedValue) Then
            ClsPay.BeneficiaryCCYCode = cboBenCCY.SelectedValue
            ClsPay.BeneficiaryCCY = cboBenCCY.Text
        End If

        If (ClsPay.Portfolio <> "" And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPort) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInPortLoan)) Or
                ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInMaltaLondon) And ClsPay.PaymentType <> ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
        (ClsPay.Portfolio = "" And (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPort) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInPortLoan))) Or
            ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInMaltaLondon) Or ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInLondonMalta) Or
            (ClsPay.PaymentType = ClsIMS.GetPaymentType(cPayDescInOther) And ClsPay.BeneficiaryCCY <> "") Then
            PopulateFormFromPortfolio(varUserLocationCode)
        End If
    End Sub

    Private Sub cboBenCCY_Leave(sender As Object, e As EventArgs) Handles cboBenCCY.Leave
        ShowExchangeRate()
    End Sub

    Private Sub ShowExchangeRate()
        lblExchangeRate.Visible = False
        If IsNumeric(CboCCY.SelectedValue) And IsNumeric(cboBenCCY.SelectedValue) Then
            If CboCCY.Text <> cboBenCCY.Text And IsNumeric(txtAmount.Text) Then
                Dim varRatestr As String = ClsIMS.GetCCYRate(dtSettleDate.Value, CboCCY.Text, cboBenCCY.Text)
                If IsNumeric(varRatestr) Then
                    Dim varRate As Double = (CDbl(varRatestr)).ToString("0.000000")
                    txtExchRate.Text = varRate
                    ClsPay.ExchangeRate = varRate
                    lblExchangeRate.Visible = True
                    lblExchangeRate.Text = "@ " & ClsPay.ExchangeRate & " = " & (CDbl(txtAmount.Text) * ClsPay.ExchangeRate).ToString("#,##0.00") & " " & cboBenCCY.Text
                End If
            End If
        End If
    End Sub

    Private Sub dgvHistory_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvHistory.CellFormatting
        If e.ColumnIndex = 5 Then
            If IsNumeric(dgvHistory.Rows(e.RowIndex).Cells(5).Value) Then
                If CDbl(dgvHistory.Rows(e.RowIndex).Cells(5).Value) < 0 Then
                    dgvHistory.Rows(e.RowIndex).Cells(5).Style.ForeColor = Color.DarkRed
                Else
                    dgvHistory.Rows(e.RowIndex).Cells(5).Style.ForeColor = Color.Black
                End If
            End If
        End If
    End Sub

    Private Sub dgvCash_CellFormatting(sender As Object, e As DataGridViewCellFormattingEventArgs) Handles dgvCash.CellFormatting
        If dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "Status" Then
            FormatGridStatus(dgvCash, e.RowIndex)
            'ElseIf dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "OrderBalance" Then
            '   If dgvCash.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusReady) Or
            '  dgvCash.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingAuth) Or
            ' dgvCash.Rows(e.RowIndex).Cells("Status").Value = ClsIMS.GetStatusType(cStatusPendingReview) Then
            'FormatGridPaymentBalance(dgvCash, e.RowIndex, e.ColumnIndex)
            'Else
            '   dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Style.ForeColor = Color.Black
            'End If
        End If
    End Sub

    Private Sub ChkBenTemplateEdit_CheckedChanged(sender As Object, e As EventArgs) Handles ChkBenTemplateEdit.CheckedChanged
        If ChkBenTemplateEdit.Checked Then
            CboBenName.Enabled = True
            FormControlsEnable(2, True)
            FormControlsEnable(3, True)
            FormControlsEnable(4, True)
            ClsPay.BeneficiaryAddressID = 0
            ClsPay.BeneficiarySubBankAddressID = 0
            ClsPay.BeneficiaryBankAddressID = 0
        Else
            CboBenName.Enabled = False
            FormControlsEnable(2, False)
            FormControlsEnable(3, False)
            FormControlsEnable(4, False)
            ClsPay.BeneficiaryAddressID = varBeneficiaryAddressID
            ClsPay.BeneficiarySubBankAddressID = varBeneficiarySubBankAddressID
            ClsPay.BeneficiaryBankAddressID = varBeneficiaryBankAddressID
            'PopulateTemplateFormDetails(ClsPay.SelectedTemplateName)
        End If
        'Else
        'ChkBenTemplateEdit.Checked = False
        'End If
    End Sub

    Private Sub PopulatePreviews()

        If Not loadEditForm Then
            PopulatePreviewSWIFT()
        End If

    End Sub

    Private Sub PopulatePreviewSWIFT()

        Dim paymentTypeID As Integer = 0
        Dim otherId As Integer = 0
        Dim benName As String = String.Empty
        Dim clientDetails As New ClientDetails
        Dim paymentTypes As Dictionary(Of String, String)

        Try

            If CboPayType.Text <> "" Then
                paymentTypeID = CboPayType.SelectedValue
                otherId = CboPortfolio.SelectedValue
                If ClsIMS.PaymentSendsSwift(CboPayType.Text) Then

                    'Fetch the Client Details.
                    clientDetails = ClsIMS.FetchClientDetails()

                    'Fetch Swift Payment Types.
                    paymentTypes = ClsIMS.FetchPaymentTypes(ClsPay.SelectedPaymentID, IIf(String.IsNullOrEmpty(ClsPay.OrderSwift), txtOrderSwift.Text, ClsPay.OrderSwift))
                    If Not paymentTypes Is Nothing Then

                        If cboBenType.SelectedValue = 0 Then
                            benName = txtbenfirstname.Text & " " & txtbensurname.Text
                        Else
                            benName = IIf(CboBenName.Text <> "", CboBenName.Text, "")
                        End If

                        Dim _swiftPayload As SwiftPayloadData = New SwiftPayloadData With {
                            .P_ClientSwiftCode = clientDetails.ClientSwiftCode,
                            .P_PaymentId = ClsPay.SelectedPaymentID,
                            .P_PaymentTypeID = CDbl(paymentTypeID),
                            .P_SettleDate = dtSettleDate.Value.Date.ToString("yyyy-MM-ddT00:00:00"),
                            .P_Amount = CDbl(IIf(String.IsNullOrEmpty(txtAmount.Text), 0, txtAmount.Text)),
                            .P_CCY = IIf(CboCCY.Text <> "", CboCCY.Text, ""),
                            .P_OrderRef = txtOrderRef.Text,
                            .P_OrderSwift = txtOrderSwift.Text,
                            .P_OrderIBAN = txtOrderIBAN.Text,
                            .P_OrderOther = txtOrderOther1.Text,
                            .P_OrderVOCode = cboOrderVOCode.Text,
                            .P_OrderAccountNumber = ClsPay.OrderAccountNumber,
                            .P_BenAccountNumber = ClsPay.BeneficiaryAccountNumber,
                            .P_BenRef = IIf(txtBenRef.Text <> "", txtBenRef.Text, ""),
                            .P_BenName = benName,
                            .P_BenAddress1 = txtBenAddress1.Text,
                            .P_BenAddress2 = txtBenAddress2.Text,
                            .P_BenZip = txtBenZip.Text,
                            .P_BenSwift = txtBenSwift.Text,
                            .P_BenIBAN = txtBenIBAN.Text,
                            .P_BenOther = txtBenOther1.Text,
                            .P_BenBIK = txtBenBIK.Text,
                            .P_BenINN = txtBenINN.Text,
                            .P_BenCCY = IIf(cboBenCCY.Text <> "", cboBenCCY.Text, ""),
                            .P_BenSubBankName = IIf(CboBenSubBankName.Text <> "", CboBenSubBankName.Text, ""),
                            .P_BenSubBankAddress1 = txtBenSubBankAddress1.Text,
                            .P_BenSubBankAddress2 = txtBenSubBankAddress2.Text,
                            .P_BenSubBankZip = txtBenSubBankZip.Text,
                            .P_BenSubBankSwift = txtBenSubBankSwift.Text,
                            .P_BenSubBankIBAN = txtBenSubBankIBAN.Text,
                            .P_BenSubBankOther = txtBenSubBankOther1.Text,
                            .P_BenSubBankBIK = txtBenSubBankBIK.Text,
                            .P_BenSubBankINN = txtBenSubbankINN.Text,
                            .P_BenBankName = IIf(CboBenBankName.Text <> "", CboBenBankName.Text, ""),
                            .P_BenBankAddress1 = txtBenBankAddress1.Text,
                            .P_BenBankAddress2 = txtBenBankAddress2.Text,
                            .P_BenBankZip = txtBenBankZip.Text,
                            .P_BenBankSwift = txtBenBankSwift.Text,
                            .P_BenBankIBAN = txtBenBankIBAN.Text,
                            .P_BenBankBIK = txtBenBankBIK.Text,
                            .P_BenBankINN = txtBenBankINN.Text,
                            .P_SwiftUrgent = IIf(ChkBenUrgent.Checked, 1, 0),
                            .P_SwiftSendRef = IIf(ChkBenSendRef.Checked, 1, 0),
                            .P_ExchangeRate = 1,
                            .P_OrderTransferMessTypeID = CInt(paymentTypes("P_OrderTransferMessTypeID")),
                            .P_Order3rdPartyMessTypeID = CInt(paymentTypes("P_Order3rdPartyMessTypeID")),
                            .P_OrderOtherMessTypeID = CInt(paymentTypes("P_OrderOtherMessTypeID")),
                            .P_Other_ID = IIf(paymentTypeID = 7, CInt(otherId), 0),
                            .O_OrderName = clientDetails.ClientName,
                            .O_Address = clientDetails.ClientAddress,
                            .O_City = clientDetails.ClientCity,
                            .O_PostCode = clientDetails.ClientPostCode,
                            .A_ApiKey = My.Settings.ApiKey.ToString,
                            .P_ClientId = My.Settings.ClientId,
                            .E_Email = String.Empty
                            }

                        txtSWIFT.Text = ClsIMS.GetSwiftFileData(_swiftPayload, _filePaths.Find(Function(f) f.PathName = "SwiftApiUri").PathFilePath)
                    Else
                        txtSWIFT.Text = "No SWIFT file sent for " & CboPayType.Text
                    End If
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtBenSwift_Leave(sender As Object, e As EventArgs) Handles txtBenSwift.Leave
        PopulatePreviews()
    End Sub


    Private Sub ChkBenUrgent_CheckedChanged(sender As Object, e As EventArgs) Handles ChkBenUrgent.CheckedChanged
        PopulatePreviews()
    End Sub

    Private Sub ChkBenSendRef_CheckedChanged(sender As Object, e As EventArgs) Handles ChkBenSendRef.CheckedChanged
        PopulatePreviews()
    End Sub

    Private Sub ChkAutoComments_CheckedChanged(sender As Object, e As EventArgs) Handles ChkAutoComments.CheckedChanged
        PopulateComments()
    End Sub

    Private Sub cboOrderVOCode_Leave(sender As Object, e As EventArgs) Handles cboOrderVOCode.Leave
        PopulatePreviews()
    End Sub

    Private Sub txtBenAddress1_Leave(sender As Object, e As EventArgs) Handles txtBenAddress1.Leave
        PopulatePreviews()
    End Sub

    Private Sub txtbencounty_Leave(sender As Object, e As EventArgs) Handles txtbencounty.Leave
        PopulatePreviews()
    End Sub

    Private Sub txtBenAddress2_Leave(sender As Object, e As EventArgs) Handles txtBenAddress2.Leave
        PopulatePreviews()
    End Sub

    Private Sub txtBenZip_Leave(sender As Object, e As EventArgs) Handles txtBenZip.Leave
        PopulatePreviews()
    End Sub

    Private Sub dgvCash_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvCash.CellMouseClick
        If e.ColumnIndex >= 0 And e.RowIndex >= 0 Then
            If Not IsDBNull(dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) Then
                If dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "IMSFile" Then
                    If File.Exists(ClsIMS.GetFilePath("Buffer") & dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                        Process.Start(ClsIMS.GetFilePath("Buffer") & dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                    End If
                ElseIf dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "PaidReceivedInPayFile" Or dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).OwningColumn.Name = "PaidReceivedOutPayFile" Then
                    If File.Exists(ClsIMS.GetFilePath("SWIFTImport") & dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = True Then
                        Process.Start(ClsIMS.GetFilePath("SWIFTImport") & dgvCash.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub txtIMSRef_Leave(sender As Object, e As EventArgs) Handles txtIMSRef.Leave
        OrderMessages()
    End Sub

    Private Sub txtIMSRef_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtIMSRef.KeyPress
        If txtIMSRef.Text <> "" Then
            If Microsoft.VisualBasic.Strings.Len(txtIMSRef.Text) Mod 10 = 0 Then
                'txtIMSRef.Text = txtIMSRef.Text & ","
            End If
        End If
    End Sub

    Private Sub RunFilters()
        Cursor = Cursors.WaitCursor
        If CboFilterStatus.Text = "" And cboPayCheck.Text = "" And cboFilterLocation.Text = "" And cboFilterType.Text = "" Then
            ClsIMS.RefreshMovementGrid(dgvCash, "")
        Else
            ClsIMS.RefreshMovementGrid(dgvCash, GetFilterWhereClause())
        End If
        Cursor = Cursors.Default
    End Sub

    Private Sub CboFilterStatus_Leave(sender As Object, e As EventArgs) Handles CboFilterStatus.Leave
        RunFilters()
    End Sub

    Private Sub LVFiles_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles LVFiles.MouseDoubleClick
        If LVFiles.SelectedItems.Count > 0 Then
            ShellExecute(ClsPay.PaymentFilePathRemote & LVFiles.SelectedItems(0).Text)
        End If
    End Sub

    Private Sub cboBenType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBenType.SelectedIndexChanged
        txtbenfirstname.Enabled = False
        txtbenmiddlename.Enabled = False
        txtbensurname.Enabled = False
        If IsNumeric(cboBenType.SelectedValue) Then
            If cboBenType.SelectedValue = 0 Then
                CboBenName.Enabled = False
                CboBenName.Text = ""
                txtbenfirstname.Enabled = True
                txtbenmiddlename.Enabled = True
                txtbensurname.Enabled = True
            Else
                CboBenName.Enabled = True
                txtbenfirstname.Text = ""
                txtbenmiddlename.Text = ""
                txtbensurname.Text = ""
                cbobenrelationship.SelectedValue = 5
            End If
        End If
    End Sub

    Private Sub CCYDefaults(ByVal varDefault As Boolean, Optional ByVal varBuySellType As Integer = 0)
        CboCCY.Enabled = True
        lblExchangeRate.Visible = False
        ClsPay.ExchangeRate = 1
        lblExchRate.Visible = Not varDefault
        txtExchRate.Visible = Not varDefault
        lblBenCCY.Visible = Not varDefault
        cboBenCCY.Visible = Not varDefault
        lblCCYAmt.Visible = varDefault
        lblCCY.Text = "CCY:"
        lblBenCCY.Text = "to CCY:"
        If varBuySellType = cPayDescInOtherFXBuy Or varBuySellType = cPayDescInOtherFXSell Then
            If varBuySellType = cPayDescInOtherFXBuy Then
                lblCCY.Text = "Buy:"
                lblBenCCY.Text = "    Sell:"
            ElseIf varBuySellType = cPayDescInOtherFXSell Then
                lblCCY.Text = "Sell:"
                lblBenCCY.Text = "     Buy:"
            End If
        End If
    End Sub

    Private Sub cboPayCheck_Leave(sender As Object, e As EventArgs) Handles cboPayCheck.Leave
        RunFilters()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvCash, 0)
        Cursor = Cursors.Default
    End Sub


    Private Sub CmdSubmitBatch_Click(sender As Object, e As EventArgs) Handles CmdSubmitBatch.Click
        If ClsPay.SelectedPaymentID = 0 Then
            If ClsIMS.CanUser(ClsIMS.FullUserName, cUserSubmit) Then
                If ClsPay.SubmitTransactionsBatch(dgvCash) Then
                    Changeform(True)
                End If
            Else
                MsgBox("You are not authorised to submit batch payments", vbCritical, "Cannot submit batch payments")
                ClsIMS.AddAudit(cAuditStatusBlocked, cAuditGroupNameCashMovementsFiles, "Refused entry to submit batch payments")
            End If
        Else
            MsgBox("You are currently in edit mode. Please save your current transaction or create a new one to submit other batch transactions", vbInformation, "Complete save transaction")
        End If
    End Sub

    Private Sub ToggleAntiFlicker(ByVal _enable As Boolean)

        enableFormLevelDoubleBuffering = _enable
        Me.MaximizeBox = True

    End Sub

    Private Sub FrmPayment_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        ToggleAntiFlicker(False)

    End Sub

End Class