﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmSwiftIntermediaryCodes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmSwiftIntermediaryCodes))
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ViewGroupBox = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboCCY = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboIB = New System.Windows.Forms.ComboBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboSC = New System.Windows.Forms.ComboBox()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.CmdFilter = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboSCBankName = New System.Windows.Forms.ComboBox()
        Me.dgvSwiftIntermediaryCodes = New System.Windows.Forms.DataGridView()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.ViewGroupBox.SuspendLayout()
        CType(Me.dgvSwiftIntermediaryCodes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label13.Location = New System.Drawing.Point(12, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(304, 24)
        Me.Label13.TabIndex = 49
        Me.Label13.Text = "Intermediary Bank Management"
        '
        'ViewGroupBox
        '
        Me.ViewGroupBox.Controls.Add(Me.Label3)
        Me.ViewGroupBox.Controls.Add(Me.cboCCY)
        Me.ViewGroupBox.Controls.Add(Me.Label1)
        Me.ViewGroupBox.Controls.Add(Me.cboIB)
        Me.ViewGroupBox.Controls.Add(Me.CmdExportToExcel)
        Me.ViewGroupBox.Controls.Add(Me.Label8)
        Me.ViewGroupBox.Controls.Add(Me.cboSC)
        Me.ViewGroupBox.Controls.Add(Me.CmdRefreshView)
        Me.ViewGroupBox.Controls.Add(Me.CmdFilter)
        Me.ViewGroupBox.Controls.Add(Me.Label2)
        Me.ViewGroupBox.Controls.Add(Me.cboSCBankName)
        Me.ViewGroupBox.Location = New System.Drawing.Point(12, 45)
        Me.ViewGroupBox.Name = "ViewGroupBox"
        Me.ViewGroupBox.Size = New System.Drawing.Size(639, 86)
        Me.ViewGroupBox.TabIndex = 48
        Me.ViewGroupBox.TabStop = False
        Me.ViewGroupBox.Text = "Filter Criteria"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(345, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 58
        Me.Label3.Text = "CCY"
        '
        'cboCCY
        '
        Me.cboCCY.FormattingEnabled = True
        Me.cboCCY.Location = New System.Drawing.Point(379, 24)
        Me.cboCCY.Name = "cboCCY"
        Me.cboCCY.Size = New System.Drawing.Size(75, 21)
        Me.cboCCY.TabIndex = 57
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(215, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(118, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Swift Intermediary Code"
        '
        'cboIB
        '
        Me.cboIB.FormattingEnabled = True
        Me.cboIB.Location = New System.Drawing.Point(339, 51)
        Me.cboIB.Name = "cboIB"
        Me.cboIB.Size = New System.Drawing.Size(115, 21)
        Me.cboIB.TabIndex = 55
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(588, 19)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 54
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(11, 54)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 13)
        Me.Label8.TabIndex = 43
        Me.Label8.Text = "Swift Code"
        '
        'cboSC
        '
        Me.cboSC.FormattingEnabled = True
        Me.cboSC.Location = New System.Drawing.Point(75, 51)
        Me.cboSC.Name = "cboSC"
        Me.cboSC.Size = New System.Drawing.Size(115, 21)
        Me.cboSC.TabIndex = 42
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(470, 53)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(112, 23)
        Me.CmdRefreshView.TabIndex = 29
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'CmdFilter
        '
        Me.CmdFilter.Location = New System.Drawing.Point(470, 19)
        Me.CmdFilter.Name = "CmdFilter"
        Me.CmdFilter.Size = New System.Drawing.Size(112, 32)
        Me.CmdFilter.TabIndex = 31
        Me.CmdFilter.Text = "Filter Data Grid"
        Me.CmdFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(63, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Bank Name"
        '
        'cboSCBankName
        '
        Me.cboSCBankName.FormattingEnabled = True
        Me.cboSCBankName.Location = New System.Drawing.Point(75, 24)
        Me.cboSCBankName.Name = "cboSCBankName"
        Me.cboSCBankName.Size = New System.Drawing.Size(264, 21)
        Me.cboSCBankName.TabIndex = 29
        '
        'dgvSwiftIntermediaryCodes
        '
        Me.dgvSwiftIntermediaryCodes.AllowUserToOrderColumns = True
        Me.dgvSwiftIntermediaryCodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSwiftIntermediaryCodes.Location = New System.Drawing.Point(12, 138)
        Me.dgvSwiftIntermediaryCodes.Name = "dgvSwiftIntermediaryCodes"
        Me.dgvSwiftIntermediaryCodes.Size = New System.Drawing.Size(1280, 660)
        Me.dgvSwiftIntermediaryCodes.TabIndex = 53
        '
        'FrmSwiftIntermediaryCodes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Linen
        Me.ClientSize = New System.Drawing.Size(1299, 801)
        Me.Controls.Add(Me.dgvSwiftIntermediaryCodes)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ViewGroupBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmSwiftIntermediaryCodes"
        Me.Text = "SWIFT Intermediary Codes"
        Me.ViewGroupBox.ResumeLayout(False)
        Me.ViewGroupBox.PerformLayout()
        CType(Me.dgvSwiftIntermediaryCodes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label13 As Label
    Friend WithEvents ViewGroupBox As GroupBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents cboSC As ComboBox
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents CmdFilter As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents cboSCBankName As ComboBox
    Friend WithEvents dgvSwiftIntermediaryCodes As DataGridView
    Friend WithEvents Label3 As Label
    Friend WithEvents cboCCY As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cboIB As ComboBox
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
