﻿Public Class FrmSwiftCodes
    Dim SCConn As SqlClient.SqlConnection
    Dim dgvdata As SqlClient.SqlDataAdapter
    Dim dgvdataBank As SqlClient.SqlDataAdapter
    Dim dgvdataIBank As SqlClient.SqlDataAdapter
    Dim cboSwiftTransferColumn As DataGridViewComboBoxColumn
    Dim cboSwift3rdPartyColumn As DataGridViewComboBoxColumn
    Dim cboSwiftRACountry As DataGridViewComboBoxColumn
    Dim ds As New DataSet

    Private Sub SCLoadGrid(Optional ByVal varFilterWhereClause As String = "")
        Dim varSQL As String = "", varColSQL As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If varFilterWhereClause = "" Then
                varSQL = "SELECT SC_ID, SC_SwiftCode,SC_BankName,SC_Branch,SC_Address1,SC_City,SC_PostCode,SC_Country,SC_CountryID,SC_INN,SC_LastModifiedDate,SC_Source,SC_SwiftTransferMessTypeID,SC_Swift3rdPartyMessTypeID,SC_SwiftOtherMessTypeID 
                            from vwDolfinPaymentSwiftCodes where SC_Source not in ('UPLOAD') and isnull(sc_bankName,'') <> '' order by SC_SwiftCode"
            Else
                varSQL = "SELECT SC_ID, SC_SwiftCode,SC_BankName,SC_Branch,SC_Address1,SC_City,SC_PostCode,SC_Country,SC_CountryID,SC_INN,SC_LastModifiedDate,SC_Source,SC_SwiftTransferMessTypeID,SC_Swift3rdPartyMessTypeID,SC_SwiftOtherMessTypeID from vwDolfinPaymentSwiftCodes " & varFilterWhereClause & " order by SC_SwiftCode"
            End If

            dgvSWIFT.DataSource = Nothing
            SCConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, SCConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentSwiftCodes")
            dgvSWIFT.AutoGenerateColumns = True
            dgvSWIFT.DataSource = ds.Tables("vwDolfinPaymentSwiftCodes")

            dgvSWIFT.Columns("SC_ID").Visible = False
            dgvSWIFT.Columns("SC_SwiftCode").HeaderText = "Swift Code"
            dgvSWIFT.Columns("SC_SwiftCode").Width = 140
            dgvSWIFT.Columns("SC_BankName").HeaderText = "Bank Name"
            dgvSWIFT.Columns("SC_BankName").Width = 250
            dgvSWIFT.Columns("SC_Branch").HeaderText = "Branch"
            dgvSWIFT.Columns("SC_Branch").Width = 100
            dgvSWIFT.Columns("SC_Address1").HeaderText = "Address"
            dgvSWIFT.Columns("SC_Address1").Width = 300
            dgvSWIFT.Columns("SC_City").HeaderText = "City"
            dgvSWIFT.Columns("SC_City").Width = 100
            dgvSWIFT.Columns("SC_PostCode").HeaderText = "Post Code"
            dgvSWIFT.Columns("SC_PostCode").Width = 100
            dgvSWIFT.Columns("SC_Country").HeaderText = "Country"
            dgvSWIFT.Columns("SC_Country").Width = 150
            dgvSWIFT.Columns("SC_CountryID").HeaderText = "Country ID"
            dgvSWIFT.Columns("SC_CountryID").Width = 50
            dgvSWIFT.Columns("SC_INN").HeaderText = "INN Code"
            dgvSWIFT.Columns("SC_INN").Width = 50
            dgvSWIFT.Columns("SC_LastModifiedDate").HeaderText = "Last Modified Date"
            dgvSWIFT.Columns("SC_LastModifiedDate").Width = 100
            dgvSWIFT.Columns("SC_Source").HeaderText = "Source"
            dgvSWIFT.Columns("SC_Source").Width = 100

            varColSQL = "Select 0 as SC_RARangeCountryID,'' as Ctry_Name union select Ctry_ID as SC_CountryID, Ctry_Name FROM DolfinPaymentRARangeCountry"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SCConn)
            dgvdataBank.Fill(ds, "vwDolfinPaymentRACountry")
            cboSwiftRACountry = New DataGridViewComboBoxColumn
            cboSwiftRACountry.HeaderText = "Risk Assessment Country"
            cboSwiftRACountry.DataPropertyName = "SC_CountryID"
            cboSwiftRACountry.DataSource = ds.Tables("vwDolfinPaymentRACountry")
            cboSwiftRACountry.ValueMember = ds.Tables("vwDolfinPaymentRACountry").Columns(0).ColumnName
            cboSwiftRACountry.DisplayMember = ds.Tables("vwDolfinPaymentRACountry").Columns(1).ColumnName
            cboSwiftRACountry.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboSwiftRACountry.FlatStyle = FlatStyle.Flat
            dgvSWIFT.Columns.Insert(8, cboSwiftRACountry)
            dgvSWIFT.Columns(8).Width = 200

            varColSQL = "Select 0 as SC_SwiftTransferMessTypeID,NULL as M_MessageType union select M_ID as SC_SwiftTransferMessTypeID, M_MessageType FROM vwDolfinPaymentMessageType where case when M_ID = 78 then 0 else M_Incoming end = 0"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SCConn)
            dgvdataBank.Fill(ds, "vwDolfinPaymentMessageType")
            cboSwiftTransferColumn = New DataGridViewComboBoxColumn
            cboSwiftTransferColumn.HeaderText = "Send Swift Msg (Transfer)"
            cboSwiftTransferColumn.DataPropertyName = "SC_SwiftTransferMessTypeID"
            cboSwiftTransferColumn.DataSource = ds.Tables("vwDolfinPaymentMessageType")
            cboSwiftTransferColumn.ValueMember = ds.Tables("vwDolfinPaymentMessageType").Columns(0).ColumnName
            cboSwiftTransferColumn.DisplayMember = ds.Tables("vwDolfinPaymentMessageType").Columns(1).ColumnName
            cboSwiftTransferColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboSwiftTransferColumn.FlatStyle = FlatStyle.Flat
            dgvSWIFT.Columns.Insert(9, cboSwiftTransferColumn)

            varColSQL = "Select 0 as SC_Swift3rdPartyMessTypeID,NULL as M_MessageType union select M_ID as SC_Swift3rdPartyMessTypeID, M_MessageType FROM vwDolfinPaymentMessageType where case when M_ID = 78 then 0 else M_Incoming end = 0"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SCConn)
            dgvdataBank.Fill(ds, "vwDolfinPaymentMessageType2")
            cboSwift3rdPartyColumn = New DataGridViewComboBoxColumn
            cboSwift3rdPartyColumn.HeaderText = "Send Swift Msg (3rd Party)"
            cboSwift3rdPartyColumn.DataPropertyName = "SC_Swift3rdPartyMessTypeID"
            cboSwift3rdPartyColumn.DataSource = ds.Tables("vwDolfinPaymentMessageType2")
            cboSwift3rdPartyColumn.ValueMember = ds.Tables("vwDolfinPaymentMessageType2").Columns(0).ColumnName
            cboSwift3rdPartyColumn.DisplayMember = ds.Tables("vwDolfinPaymentMessageType2").Columns(1).ColumnName
            cboSwift3rdPartyColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboSwift3rdPartyColumn.FlatStyle = FlatStyle.Flat
            dgvSWIFT.Columns.Insert(10, cboSwift3rdPartyColumn)

            varColSQL = "Select 0 as SC_SwiftOtherMessTypeID,NULL as M_MessageType union select M_ID as SC_SwiftOtherMessTypeID, M_MessageType FROM vwDolfinPaymentMessageType where case when M_ID = 78 then 0 else M_Incoming end = 0"
            dgvdataBank = New SqlClient.SqlDataAdapter(varColSQL, SCConn)
            dgvdataBank.Fill(ds, "vwDolfinPaymentMessageType3")
            cboSwift3rdPartyColumn = New DataGridViewComboBoxColumn
            cboSwift3rdPartyColumn.HeaderText = "Send Swift Msg (Other)"
            cboSwift3rdPartyColumn.DataPropertyName = "SC_SwiftOtherMessTypeID"
            cboSwift3rdPartyColumn.DataSource = ds.Tables("vwDolfinPaymentMessageType3")
            cboSwift3rdPartyColumn.ValueMember = ds.Tables("vwDolfinPaymentMessageType3").Columns(0).ColumnName
            cboSwift3rdPartyColumn.DisplayMember = ds.Tables("vwDolfinPaymentMessageType3").Columns(1).ColumnName
            cboSwift3rdPartyColumn.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboSwift3rdPartyColumn.FlatStyle = FlatStyle.Flat
            dgvSWIFT.Columns.Insert(11, cboSwift3rdPartyColumn)

            dgvSWIFT.Columns("SC_SwiftTransferMessTypeID").Visible = False
            dgvSWIFT.Columns("SC_Swift3rdPartyMessTypeID").Visible = False
            dgvSWIFT.Columns("SC_SwiftOtherMessTypeID").Visible = False
            dgvSWIFT.Columns("SC_CountryID").Visible = False

            'dgvSWIFT.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvSWIFT.Refresh()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvSWIFT.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSwiftCodes, Err.Description & " - Problem With viewing swift codes grid")
        End Try
    End Sub

    Private Sub FrmSwiftCodes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ModRMS.DoubleBuffered(dgvSWIFT, True)
        ClsIMS.PopulateComboboxWithData(cboSCBankName, "Select 1, SC_BankName from vwDolfinPaymentSwiftCodes group by SC_BankName order by SC_BankName")
        cboSCBankName.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSCBankName.AutoCompleteSource = AutoCompleteSource.ListItems
        ClsIMS.PopulateComboboxWithData(cboSC, "Select 1, SC_SwiftCode from vwDolfinPaymentSwiftCodes group by SC_SwiftCode order by SC_SwiftCode")
        cboSC.AutoCompleteMode = AutoCompleteMode.SuggestAppend
        cboSC.AutoCompleteSource = AutoCompleteSource.ListItems
        SCLoadGrid()
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvSWIFT, 0)
        Cursor = Cursors.Default
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        cboSCBankName.Text = ""
        cboSC.Text = ""
        ChknonValidSC.Checked = False
        SCLoadGrid()
    End Sub

    Private Sub CmdFilter_Click(sender As Object, e As EventArgs) Handles CmdFilter.Click
        Dim varWhereClauseFilter As String = ""

        Cursor = Cursors.WaitCursor

        If Not ChknonValidSC.Checked Then
            varWhereClauseFilter = "where isnull(sc_bankName,'') <> '' "
        Else
            varWhereClauseFilter = "where SC_SwiftCode like '%' "
        End If

        If Not ChkAllSwifts.Checked Then
            varWhereClauseFilter = varWhereClauseFilter & " and SC_Source not in ('UPLOAD') "
        End If


        If cboSCBankName.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and SC_BankName ='" & cboSCBankName.Text & "'"
        End If

        If cboSC.Text <> "" Then
            varWhereClauseFilter = varWhereClauseFilter & " and SC_SwiftCode ='" & cboSC.Text & "'"
        End If

        If varWhereClauseFilter <> "where" Then
            SCLoadGrid(Replace(varWhereClauseFilter, "where and ", "where "))
        Else
            SCLoadGrid()
        End If
        Cursor = Cursors.Default
    End Sub


    Public Function ValidSwiftDetails(ByVal vRow As Integer) As Boolean
        ValidSwiftDetails = True
        If dgvSWIFT.Rows(vRow).Cells(1).Value <> "" Or dgvSWIFT.Rows(vRow).Cells(2).Value <> "" Or dgvSWIFT.Rows(vRow).Cells(3).Value <> "" Or
            dgvSWIFT.Rows(vRow).Cells(4).Value <> "" Or dgvSWIFT.Rows(vRow).Cells(5).Value <> "" Then
            ValidSwiftDetails = False
        End If
    End Function


    Private Sub dgvSwiftCodes_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvSWIFT.RowLeave
        Dim varId As Integer = 0
        Dim varSwiftCode As String = ""
        Dim varCCY As String = ""
        Dim varIBankSwiftCode As String = ""
        Dim varSwiftDetails As New Dictionary(Of String, String)
        Dim varBankName As String = ""
        Dim varCtryID As Integer = 0

        If dgvSWIFT.EndEdit Then
            Try
                varId = IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_ID").Value), 0, dgvSWIFT.Rows(e.RowIndex).Cells("SC_ID").Value)
                varSwiftCode = IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftCode").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftCode").Value)
                varBankName = IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_BankName").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_BankName").Value)
                varCtryID = IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_CountryID").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_CountryID").Value)
                varSwiftCode = Strings.Left(Replace(Replace(varSwiftCode, " ", ""), "-", ""), 11)

                If varId = 0 Then
                    If ValidSwift(varSwiftCode) Then
                        If varBankName <> "" And varCtryID <> 0 Then
                            Dim CheckID As String = ClsIMS.GetSQLItem("select SC_ID from vwDolfinPaymentSwiftCodes where SC_ID = " & varId)
                            If CheckID = "" Then
                                Dim varSQL As String = "insert into DolfinPaymentSwiftCodes(SC_SwiftCode,SC_BankName,SC_Branch,SC_Address1,SC_City,SC_PostCode,SC_Country,SC_CountryCode,SC_INN,SC_SwiftTransferMessTypeID,SC_Swift3rdPartyMessTypeID,SC_SwiftOtherMessTypeID,SC_RARangeCountryID,SC_LastModifiedDate,SC_Source) values (" &
                                   "'" & varSwiftCode & "'," &
                                  "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(2).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(2).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(3).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(3).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(4).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(4).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(5).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(5).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(6).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(6).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(7).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(7).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(8).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(8).Value) & "'," &
                                 "'" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(9).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(9).Value) & "',"

                                If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(12).Value) Then
                                    varSQL = varSQL & "0,"
                                Else
                                    varSQL = varSQL & "convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells(12).Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells(12).Value) & "'),"
                                End If
                                If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(13).Value) Then
                                    varSQL = varSQL & "0,"
                                Else
                                    varSQL = varSQL & "convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells(13).Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells(13).Value) & "'),"
                                End If
                                If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(14).Value) Then
                                    varSQL = varSQL & "0,"
                                Else
                                    varSQL = varSQL & "convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells(14).Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells(14).Value) & "'),"
                                End If
                                If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(15).Value) Then
                                    varSQL = varSQL & "0,"
                                Else
                                    varSQL = varSQL & "convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells(15).Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells(15).Value) & "'),"
                                End If

                                varSQL = varSQL & "getdate(),'USER')"

                                ClsIMS.ExecuteString(SCConn, varSQL)
                                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodes, varSQL)
                                MsgBox("The Bank details has been updated. Please refresh to see update", vbInformation, "Bank updated")
                            Else
                                MsgBox("This BIC is already in the system. Please use this BIC", vbInformation, "Bank not updated")
                            End If
                        Else
                            MsgBox("Please enter a valid bank name and valid country for entry", vbInformation, "Bank not updated")
                        End If
                    Else
                        MsgBox("Please enter a valid swift for entry", vbInformation, "Bank not updated")
                    End If
                Else
                    If ValidSwift(varSwiftCode) Then
                        If varBankName <> "" And varCtryID <> 0 Then
                            Dim varSQL As String = "update DolfinPaymentSwiftCodes set " &
                                            "SC_SwiftCode = '" & varSwiftCode & "'," &
                                            "SC_BankName = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_BankName").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_BankName").Value) & "'," &
                                            "SC_Branch = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_Branch").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_Branch").Value) & "'," &
                                            "SC_Address1 = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_Address1").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_Address1").Value) & "'," &
                                            "SC_City = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_City").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_City").Value) & "'," &
                                            "SC_PostCode = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_PostCode").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_PostCode").Value) & "'," &
                                            "SC_Country = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_Country").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_Country").Value) & "'," &
                                            "SC_CountryCode = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(8).Value), "", dgvSWIFT.Rows(e.RowIndex).Cells(8).Value) & "'," &
                                            "SC_INN = '" & IIf(IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells("SC_INN").Value), "", dgvSWIFT.Rows(e.RowIndex).Cells("SC_INN").Value.ToString) & "',"

                            If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(12).Value) Then
                                varSQL = varSQL & "SC_SwiftTransferMessTypeID = 0,"
                            Else
                                varSQL = varSQL & "SC_SwiftTransferMessTypeID = convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftTransferMessTypeID").Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftTransferMessTypeID").Value) & "'),"
                            End If
                            If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(13).Value) Then
                                varSQL = varSQL & "SC_Swift3rdPartyMessTypeID = 0,"
                            Else
                                varSQL = varSQL & "SC_Swift3rdPartyMessTypeID = convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells("SC_Swift3rdPartyMessTypeID").Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells("SC_Swift3rdPartyMessTypeID").Value) & "'),"
                            End If
                            If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(14).Value) Then
                                varSQL = varSQL & "SC_SwiftOtherMessTypeID = 0,"
                            Else
                                varSQL = varSQL & "SC_SwiftOtherMessTypeID = convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftOtherMessTypeID").Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells("SC_SwiftOtherMessTypeID").Value) & "'),"
                            End If
                            If IsDBNull(dgvSWIFT.Rows(e.RowIndex).Cells(15).Value) Then
                                varSQL = varSQL & "SC_RARangeCountryID = 0,"
                            Else
                                varSQL = varSQL & "SC_RARangeCountryID = convert(int,'" & IIf(CStr(dgvSWIFT.Rows(e.RowIndex).Cells(8).Value) = "", "0", dgvSWIFT.Rows(e.RowIndex).Cells(8).Value) & "'),"
                            End If
                            varSQL = varSQL & "SC_LastModifiedDate=getdate(),SC_Source='USER' where SC_ID = " & varId
                            ClsIMS.ExecuteString(SCConn, varSQL)

                            varSQL = "update DolfinPaymentSwiftCodes set SC_RARangeCountryID = @RACtryID where [SC_ID] = " & varId
                            ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameTransferFees, varSQL)
                        Else
                            MsgBox("Please enter a valid bank name and valid country for update", vbInformation, "Bank not updated")
                        End If
                    Else
                        MsgBox("Please enter a valid swift for update", vbInformation, "Bank not updated")
                    End If
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameSwiftCodes, Err.Description & " - Problem with updating SWIFT Code: " & varSwiftCode)
            End Try
        End If
    End Sub

    Private Sub FrmSwiftCodes_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        If SCConn.State = ConnectionState.Open Then
            SCConn.Close()
        End If
    End Sub

    Private Sub clearLookup()
        txtLookBankName.Text = ""
        txtLookAddress1.Text = ""
        txtLookBranch.Text = ""
        txtLookCity.Text = ""
        txtLookCountry.Text = ""
        txtLookPostCode.Text = ""
        txtLookCountryCode.Text = ""
        PicLookSwift.Image = Nothing
    End Sub


    Private Sub txtLookSWIFT_Leave(sender As Object, e As EventArgs) Handles txtLookSWIFT.Leave
        Dim varSwiftDetails As Dictionary(Of String, String)

        clearLookup()
        If txtLookSWIFT.Text <> "" Then
            If ValidSwift(txtLookSWIFT.Text) Then
                Dim CheckSC As String = ClsIMS.GetSQLItem("Select SC_SwiftCode from vwDolfinPaymentSwiftCodes where SC_SwiftCode = '" & txtLookSWIFT.Text & "'")
                If CheckSC = "" Then
                    varSwiftDetails = GetSwiftDetails(txtLookSWIFT.Text)
                    If Not varSwiftDetails Is Nothing Then
                        If varSwiftDetails.Count > 0 Then
                            txtLookBankName.Text = varSwiftDetails.Item("bank").ToString
                            txtLookCity.Text = varSwiftDetails.Item("city").ToString
                            txtLookBranch.Text = varSwiftDetails.Item("branch").ToString
                            txtLookAddress1.Text = varSwiftDetails.Item("address").ToString
                            txtLookPostCode.Text = varSwiftDetails.Item("postcode").ToString
                            txtLookCountry.Text = varSwiftDetails.Item("country").ToString
                            txtLookCountryCode.Text = varSwiftDetails.Item("countrycode").ToString
                            PicLookSwift.Image = ImageListTickCross.Images(0)
                        Else
                            PicLookSwift.Image = ImageListTickCross.Images(1)
                            MsgBox("This SWIFT code has not been found. Please enter a valid SWIFT code for retrieveing bank details", vbInformation, "Enter all details")
                        End If
                    Else
                        PicLookSwift.Image = ImageListTickCross.Images(1)
                        MsgBox("This SWIFT code is not valid. Please enter a valid SWIFT code for retrieveing bank details", vbInformation, "Enter all details")
                    End If
                Else
                    MsgBox("This BIC is already in the list. Please use this BIC" & vbNewLine & "If you cannot find the code in the grid, include 'non valid Swift Codes' in your serach criteria", vbInformation, "BIC already available")
                End If
            Else
                PicLookSwift.Image = ImageListTickCross.Images(1)
                MsgBox("This SWIFT code is not valid. Please enter a valid SWIFT code for retrieveing bank details", vbInformation, "Enter all details")
            End If
        End If
    End Sub

    Private Sub cmdSWIFT_Click(sender As Object, e As EventArgs) Handles cmdSWIFT.Click
        If txtLookSWIFT.Text <> "" Then
            If ValidSwift(txtLookSWIFT.Text) And txtLookBankName.Text <> "" Then
                Dim varSQL As String = "insert into DolfinPaymentSwiftCodes(SC_SwiftCode,SC_BankName,SC_City,SC_Branch,SC_Address1,SC_PostCode,SC_Country,SC_CountryCode,SC_LastModifiedDate,SC_Source) values ("
                varSQL = varSQL & "'" & UCase(txtLookSWIFT.Text) & "',"
                varSQL = varSQL & "'" & UCase(txtLookBankName.Text) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookCity.Text, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookBranch.Text, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookAddress1.Text, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookPostCode.Text, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookCountry.Text, "'", "")) & "',"
                varSQL = varSQL & "'" & UCase(Replace(txtLookCountryCode.Text, "'", "")) & "',"
                varSQL = varSQL & "getdate(),'USER')"

                ClsIMS.ExecuteString(SCConn, varSQL)
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameSwiftCodes, varSQL)
                MsgBox("The SWIFT details have been added", vbInformation, "SWIFT Success")
                txtLookSWIFT.Text = ""
                clearLookup()
                SCLoadGrid()
            End If
        End If
    End Sub

    Private Sub dgvSWIFT_UserDeletingRow(sender As Object, e As DataGridViewRowCancelEventArgs) Handles dgvSWIFT.UserDeletingRow
        Dim varID As Integer = 0
        Try
            varID = IIf(IsDBNull(dgvSWIFT.SelectedRows(0).Cells(0).Value), 0, dgvSWIFT.SelectedRows(0).Cells(0).Value)
            Dim varResponse As Integer = MsgBox("Are you sure you want to delete SWIFT detail " & IIf(IsDBNull(dgvSWIFT.SelectedRows(0).Cells(1).Value), 0, dgvSWIFT.SelectedRows(0).Cells(1).Value) & "?", vbYesNo, "Are you sure")
            If vbYes Then
                ClsIMS.ExecuteString(SCConn, "delete from vwDolfinPaymentSwiftCodes where SC_ID = " & varID)
                ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameUsers, "user deleted - delete from vwDolfinPaymentSwiftCodes varid " & varID)
            End If
        Catch ex As Exception
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameUsers, Err.Description & " - Problem with deleting from vwDolfinPaymentSwiftCodes varid " & varID)
        End Try
    End Sub

    Private Sub cboSCBankName_Leave(sender As Object, e As EventArgs) Handles cboSCBankName.Leave
        If Not IsNothing(cboSCBankName.Text) Then
            If cboSCBankName.Text <> "" Then
                ChkAllSwifts.Checked = True
            Else
                ChkAllSwifts.Checked = False
            End If
        Else
            ChkAllSwifts.Checked = False
        End If
    End Sub

    Private Sub cboSC_Leave(sender As Object, e As EventArgs) Handles cboSC.Leave
        If Not IsNothing(cboSC.Text) Then
            If cboSC.Text <> "" Then
                ChkAllSwifts.Checked = True
            Else
                ChkAllSwifts.Checked = False
            End If
        Else
            ChkAllSwifts.Checked = False
        End If
    End Sub
End Class