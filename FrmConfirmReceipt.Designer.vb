﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConfirmReceipt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmConfirmReceipt))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.CmdRunPassword = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CmdExcelPR = New System.Windows.Forms.Button()
        Me.ChkRefresh = New System.Windows.Forms.CheckBox()
        Me.cmdPRSearch = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboPROrderRef = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboPRCCY = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboPRPortfolio = New System.Windows.Forms.ComboBox()
        Me.dgvPR = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmdPRRefresh = New System.Windows.Forms.Button()
        Me.cmdNewPR = New System.Windows.Forms.Button()
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.TabCRM = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.CmdExcelFOP = New System.Windows.Forms.Button()
        Me.CmdFOPRefresh = New System.Windows.Forms.Button()
        Me.dgvFOP = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.CmdExcelCER = New System.Windows.Forms.Button()
        Me.Cmd = New System.Windows.Forms.Button()
        Me.ChkShowAll = New System.Windows.Forms.CheckBox()
        Me.cmdCERRefresh = New System.Windows.Forms.Button()
        Me.dgvCER = New System.Windows.Forms.DataGridView()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvPR, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabCRM.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvFOP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvCER, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CmdRunPassword)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.dgvPR)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdPRRefresh)
        Me.GroupBox1.Controls.Add(Me.cmdNewPR)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1612, 488)
        Me.GroupBox1.TabIndex = 45
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Request Details"
        '
        'CmdRunPassword
        '
        Me.CmdRunPassword.Location = New System.Drawing.Point(190, 46)
        Me.CmdRunPassword.Name = "CmdRunPassword"
        Me.CmdRunPassword.Size = New System.Drawing.Size(174, 31)
        Me.CmdRunPassword.TabIndex = 80
        Me.CmdRunPassword.Text = "Client Password Generator"
        Me.CmdRunPassword.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.OldLace
        Me.GroupBox2.Controls.Add(Me.CmdExcelPR)
        Me.GroupBox2.Controls.Add(Me.ChkRefresh)
        Me.GroupBox2.Controls.Add(Me.cmdPRSearch)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.cboPROrderRef)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cboPRCCY)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cboPRPortfolio)
        Me.GroupBox2.Location = New System.Drawing.Point(867, 10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(731, 69)
        Me.GroupBox2.TabIndex = 79
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Search Previous Payments Criteria"
        '
        'CmdExcelPR
        '
        Me.CmdExcelPR.Image = CType(resources.GetObject("CmdExcelPR.Image"), System.Drawing.Image)
        Me.CmdExcelPR.Location = New System.Drawing.Point(686, 9)
        Me.CmdExcelPR.Name = "CmdExcelPR"
        Me.CmdExcelPR.Size = New System.Drawing.Size(39, 32)
        Me.CmdExcelPR.TabIndex = 84
        Me.CmdExcelPR.UseVisualStyleBackColor = True
        '
        'ChkRefresh
        '
        Me.ChkRefresh.AutoSize = True
        Me.ChkRefresh.Checked = True
        Me.ChkRefresh.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkRefresh.ForeColor = System.Drawing.Color.Maroon
        Me.ChkRefresh.Location = New System.Drawing.Point(585, 46)
        Me.ChkRefresh.Name = "ChkRefresh"
        Me.ChkRefresh.Size = New System.Drawing.Size(146, 17)
        Me.ChkRefresh.TabIndex = 81
        Me.ChkRefresh.Text = "Auto Refresh Grid On/Off"
        Me.ChkRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ChkRefresh.UseVisualStyleBackColor = True
        '
        'cmdPRSearch
        '
        Me.cmdPRSearch.Location = New System.Drawing.Point(405, 21)
        Me.cmdPRSearch.Name = "cmdPRSearch"
        Me.cmdPRSearch.Size = New System.Drawing.Size(174, 37)
        Me.cmdPRSearch.TabIndex = 83
        Me.cmdPRSearch.Text = "Search Previous Payments"
        Me.cmdPRSearch.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(160, 45)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "ID/OrderRf:"
        '
        'cboPROrderRef
        '
        Me.cboPROrderRef.FormattingEnabled = True
        Me.cboPROrderRef.Location = New System.Drawing.Point(227, 42)
        Me.cboPROrderRef.Name = "cboPROrderRef"
        Me.cboPROrderRef.Size = New System.Drawing.Size(165, 21)
        Me.cboPROrderRef.TabIndex = 81
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 80
        Me.Label3.Text = "CCY:"
        '
        'cboPRCCY
        '
        Me.cboPRCCY.FormattingEnabled = True
        Me.cboPRCCY.Location = New System.Drawing.Point(69, 43)
        Me.cboPRCCY.Name = "cboPRCCY"
        Me.cboPRCCY.Size = New System.Drawing.Size(85, 21)
        Me.cboPRCCY.TabIndex = 79
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(18, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 78
        Me.Label2.Text = "Portfolio:"
        '
        'cboPRPortfolio
        '
        Me.cboPRPortfolio.FormattingEnabled = True
        Me.cboPRPortfolio.Location = New System.Drawing.Point(69, 16)
        Me.cboPRPortfolio.Name = "cboPRPortfolio"
        Me.cboPRPortfolio.Size = New System.Drawing.Size(323, 21)
        Me.cboPRPortfolio.TabIndex = 77
        '
        'dgvPR
        '
        Me.dgvPR.AllowUserToAddRows = False
        Me.dgvPR.AllowUserToDeleteRows = False
        Me.dgvPR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPR.Location = New System.Drawing.Point(6, 85)
        Me.dgvPR.Name = "dgvPR"
        Me.dgvPR.ReadOnly = True
        Me.dgvPR.Size = New System.Drawing.Size(1592, 397)
        Me.dgvPR.TabIndex = 75
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(183, 24)
        Me.Label1.TabIndex = 43
        Me.Label1.Text = "Payment Requests"
        '
        'cmdPRRefresh
        '
        Me.cmdPRRefresh.Location = New System.Drawing.Point(10, 46)
        Me.cmdPRRefresh.Name = "cmdPRRefresh"
        Me.cmdPRRefresh.Size = New System.Drawing.Size(174, 31)
        Me.cmdPRRefresh.TabIndex = 73
        Me.cmdPRRefresh.Text = "Refresh Movements"
        Me.cmdPRRefresh.UseVisualStyleBackColor = True
        '
        'cmdNewPR
        '
        Me.cmdNewPR.Location = New System.Drawing.Point(370, 46)
        Me.cmdNewPR.Name = "cmdNewPR"
        Me.cmdNewPR.Size = New System.Drawing.Size(174, 31)
        Me.cmdNewPR.TabIndex = 76
        Me.cmdNewPR.Text = "New Payment Request"
        Me.cmdNewPR.UseVisualStyleBackColor = True
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Enabled = True
        Me.TimerRefresh.Interval = 120000
        '
        'TabCRM
        '
        Me.TabCRM.Controls.Add(Me.TabPage1)
        Me.TabCRM.Controls.Add(Me.TabPage2)
        Me.TabCRM.Location = New System.Drawing.Point(12, 507)
        Me.TabCRM.Name = "TabCRM"
        Me.TabCRM.SelectedIndex = 0
        Me.TabCRM.Size = New System.Drawing.Size(1612, 384)
        Me.TabCRM.TabIndex = 46
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPage1.Controls.Add(Me.CmdExcelFOP)
        Me.TabPage1.Controls.Add(Me.CmdFOPRefresh)
        Me.TabPage1.Controls.Add(Me.dgvFOP)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1604, 358)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Free of Payments"
        '
        'CmdExcelFOP
        '
        Me.CmdExcelFOP.Image = CType(resources.GetObject("CmdExcelFOP.Image"), System.Drawing.Image)
        Me.CmdExcelFOP.Location = New System.Drawing.Point(1559, 38)
        Me.CmdExcelFOP.Name = "CmdExcelFOP"
        Me.CmdExcelFOP.Size = New System.Drawing.Size(39, 37)
        Me.CmdExcelFOP.TabIndex = 76
        Me.CmdExcelFOP.UseVisualStyleBackColor = True
        '
        'CmdFOPRefresh
        '
        Me.CmdFOPRefresh.Location = New System.Drawing.Point(1443, 6)
        Me.CmdFOPRefresh.Name = "CmdFOPRefresh"
        Me.CmdFOPRefresh.Size = New System.Drawing.Size(155, 26)
        Me.CmdFOPRefresh.TabIndex = 73
        Me.CmdFOPRefresh.Text = "Refresh Free of Payments"
        Me.CmdFOPRefresh.UseVisualStyleBackColor = True
        '
        'dgvFOP
        '
        Me.dgvFOP.AllowUserToAddRows = False
        Me.dgvFOP.AllowUserToDeleteRows = False
        Me.dgvFOP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFOP.Location = New System.Drawing.Point(6, 6)
        Me.dgvFOP.Name = "dgvFOP"
        Me.dgvFOP.Size = New System.Drawing.Size(1431, 346)
        Me.dgvFOP.TabIndex = 75
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FloralWhite
        Me.TabPage2.Controls.Add(Me.CmdExcelCER)
        Me.TabPage2.Controls.Add(Me.Cmd)
        Me.TabPage2.Controls.Add(Me.ChkShowAll)
        Me.TabPage2.Controls.Add(Me.cmdCERRefresh)
        Me.TabPage2.Controls.Add(Me.dgvCER)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1604, 358)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Incoming Funds"
        '
        'CmdExcelCER
        '
        Me.CmdExcelCER.Image = CType(resources.GetObject("CmdExcelCER.Image"), System.Drawing.Image)
        Me.CmdExcelCER.Location = New System.Drawing.Point(1559, 93)
        Me.CmdExcelCER.Name = "CmdExcelCER"
        Me.CmdExcelCER.Size = New System.Drawing.Size(39, 37)
        Me.CmdExcelCER.TabIndex = 80
        Me.CmdExcelCER.UseVisualStyleBackColor = True
        '
        'Cmd
        '
        Me.Cmd.Location = New System.Drawing.Point(1443, 6)
        Me.Cmd.Name = "Cmd"
        Me.Cmd.Size = New System.Drawing.Size(155, 26)
        Me.Cmd.TabIndex = 79
        Me.Cmd.Text = "Add Funds"
        Me.Cmd.UseVisualStyleBackColor = True
        '
        'ChkShowAll
        '
        Me.ChkShowAll.AutoSize = True
        Me.ChkShowAll.Location = New System.Drawing.Point(1443, 70)
        Me.ChkShowAll.Name = "ChkShowAll"
        Me.ChkShowAll.Size = New System.Drawing.Size(155, 17)
        Me.ChkShowAll.TabIndex = 78
        Me.ChkShowAll.Text = "Show all confirmed receipts"
        Me.ChkShowAll.UseVisualStyleBackColor = True
        '
        'cmdCERRefresh
        '
        Me.cmdCERRefresh.Location = New System.Drawing.Point(1443, 38)
        Me.cmdCERRefresh.Name = "cmdCERRefresh"
        Me.cmdCERRefresh.Size = New System.Drawing.Size(155, 26)
        Me.cmdCERRefresh.TabIndex = 77
        Me.cmdCERRefresh.Text = "Refresh Receipts"
        Me.cmdCERRefresh.UseVisualStyleBackColor = True
        '
        'dgvCER
        '
        Me.dgvCER.AllowUserToAddRows = False
        Me.dgvCER.AllowUserToDeleteRows = False
        Me.dgvCER.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCER.Location = New System.Drawing.Point(6, 6)
        Me.dgvCER.Name = "dgvCER"
        Me.dgvCER.Size = New System.Drawing.Size(1431, 346)
        Me.dgvCER.TabIndex = 75
        '
        'FrmConfirmReceipt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1632, 903)
        Me.Controls.Add(Me.TabCRM)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmConfirmReceipt"
        Me.Text = "Payment Requests/Confirm External Receipts"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvPR, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabCRM.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgvFOP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvCER, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cmdNewPR As Button
    Friend WithEvents dgvPR As DataGridView
    Friend WithEvents cmdPRRefresh As Button
    Friend WithEvents TimerRefresh As Timer
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cmdPRSearch As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents cboPROrderRef As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboPRCCY As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cboPRPortfolio As ComboBox
    Friend WithEvents CmdRunPassword As Button
    Friend WithEvents ChkRefresh As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TabCRM As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents CmdFOPRefresh As Button
    Friend WithEvents dgvFOP As DataGridView
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Cmd As Button
    Friend WithEvents ChkShowAll As CheckBox
    Friend WithEvents cmdCERRefresh As Button
    Friend WithEvents dgvCER As DataGridView
    Friend WithEvents CmdExcelPR As Button
    Friend WithEvents CmdExcelFOP As Button
    Friend WithEvents CmdExcelCER As Button
End Class
