﻿Public Class ClsConfirmReceipt
    Private varID As Double = 0
    Private varStatus As String = ""
    Private varType As String = ""
    Private varPortfolioCode As Double = 0
    Private varPortfolioName As String = ""
    Private varCCYCode As Double = 0
    Private varCCYName As String = ""
    Private varOrderRef As String = ""
    Private varAmount As Double = 0
    Private varSenderName As String = ""
    Private varTradeDate As Date = Now
    Private varSettleDate As Date = Now
    Private varFilePath As String = ""
    Private varSwiftCode As String = ""
    Private varBankName As String = ""
    Private varNotes As String = ""

    Property ID() As Double
        Get
            Return varID
        End Get
        Set(value As Double)
            varID = value
        End Set
    End Property

    Property Status() As String
        Get
            Return varStatus
        End Get
        Set(value As String)
            varStatus = value
        End Set
    End Property

    Property Type() As String
        Get
            Return varType
        End Get
        Set(value As String)
            varType = value
        End Set
    End Property

    Property PortfolioCode() As Double
        Get
            Return varPortfolioCode
        End Get
        Set(value As Double)
            varPortfolioCode = value
        End Set
    End Property

    Property PortfolioName() As String
        Get
            Return varPortfolioName
        End Get
        Set(value As String)
            varPortfolioName = value
        End Set
    End Property

    Property CCYCode() As Double
        Get
            Return varCCYCode
        End Get
        Set(value As Double)
            varCCYCode = value
        End Set
    End Property

    Property CCYName() As String
        Get
            Return varCCYName
        End Get
        Set(value As String)
            varCCYName = value
        End Set
    End Property

    Property OrderRef() As String
        Get
            Return varOrderRef
        End Get
        Set(value As String)
            varOrderRef = value
        End Set
    End Property

    Property Amount() As Double
        Get
            Return varAmount
        End Get
        Set(value As Double)
            varAmount = value
        End Set
    End Property

    Property SenderName() As String
        Get
            Return varSenderName
        End Get
        Set(value As String)
            varSenderName = value
        End Set
    End Property

    Property TradeDate() As Date
        Get
            Return varTradeDate
        End Get
        Set(value As Date)
            varTradeDate = value
        End Set
    End Property

    Property SettleDate() As Date
        Get
            Return varSettleDate
        End Get
        Set(value As Date)
            varSettleDate = value
        End Set
    End Property

    Property SwiftCode() As String
        Get
            Return varSwiftCode
        End Get
        Set(value As String)
            varSwiftCode = value
        End Set
    End Property

    Property BankName() As String
        Get
            Return varBankName
        End Get
        Set(value As String)
            varBankName = value
        End Set
    End Property

    Property FilePath() As String
        Get
            Return varFilePath
        End Get
        Set(value As String)
            varFilePath = value
        End Set
    End Property

    Property Notes() As String
        Get
            Return varNotes
        End Get
        Set(value As String)
            varNotes = value
        End Set
    End Property

End Class
