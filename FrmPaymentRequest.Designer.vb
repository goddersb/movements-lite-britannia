﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmPaymentRequest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.CmdBrowseFiles = New System.Windows.Forms.Button()
        Me.txtPaymentFilesPath = New System.Windows.Forms.TextBox()
        Me.CmdFilePayment = New System.Windows.Forms.Button()
        Me.LVFiles1 = New System.Windows.Forms.ListView()
        Me.GrpPRCriteria = New System.Windows.Forms.GroupBox()
        Me.TabRequest = New System.Windows.Forms.TabControl()
        Me.TabRequestPayment = New System.Windows.Forms.TabPage()
        Me.LblNewBalanceCCY = New System.Windows.Forms.Label()
        Me.lblFeeBalanceCCY = New System.Windows.Forms.Label()
        Me.lblPostBalanceCCY = New System.Windows.Forms.Label()
        Me.lblBalanceCCY = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cboFeeType = New System.Windows.Forms.ComboBox()
        Me.lblFeeBalance = New System.Windows.Forms.Label()
        Me.lblFeeBalancetitle = New System.Windows.Forms.Label()
        Me.lblPostBalance = New System.Windows.Forms.Label()
        Me.lblPostBalancetitle = New System.Windows.Forms.Label()
        Me.txtRef = New System.Windows.Forms.TextBox()
        Me.lblRef = New System.Windows.Forms.Label()
        Me.txtPRRate = New System.Windows.Forms.TextBox()
        Me.lblTemBenPayPortfolio = New System.Windows.Forms.Label()
        Me.cboTemBenPayPortfolio = New System.Windows.Forms.ComboBox()
        Me.lblNewBalance = New System.Windows.Forms.Label()
        Me.lblNewBalancetitle = New System.Windows.Forms.Label()
        Me.txtPRAmountFee = New System.Windows.Forms.TextBox()
        Me.lblPRAmountFee = New System.Windows.Forms.Label()
        Me.cboTemFeeCCY = New System.Windows.Forms.ComboBox()
        Me.lblTemFeeCCY = New System.Windows.Forms.Label()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.lblBalancetitle = New System.Windows.Forms.Label()
        Me.txtPRAmount = New System.Windows.Forms.TextBox()
        Me.lblPRAmount = New System.Windows.Forms.Label()
        Me.cboTemFindCCY = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboTemFindPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabRequestMM = New System.Windows.Forms.TabPage()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.cboMMBuySell = New System.Windows.Forms.ComboBox()
        Me.lblHeldEndDate = New System.Windows.Forms.Label()
        Me.dtHeldEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lblHeldStartDate = New System.Windows.Forms.Label()
        Me.dtHeldStartDate = New System.Windows.Forms.DateTimePicker()
        Me.LblMMNewBalanceCCY = New System.Windows.Forms.Label()
        Me.lblMMInstrumentBalanceCCY = New System.Windows.Forms.Label()
        Me.lblMMPostBalanceCCY = New System.Windows.Forms.Label()
        Me.lblMMBalanceCCY = New System.Windows.Forms.Label()
        Me.lblMMHeldType = New System.Windows.Forms.Label()
        Me.cboMMHeldType = New System.Windows.Forms.ComboBox()
        Me.lblMMInstrumentBalance = New System.Windows.Forms.Label()
        Me.lblMMInstrumentBalancetitle = New System.Windows.Forms.Label()
        Me.lblMMPostBalance = New System.Windows.Forms.Label()
        Me.lblMMPostBalancetitle = New System.Windows.Forms.Label()
        Me.lblMMInstrument = New System.Windows.Forms.Label()
        Me.cboMMInstrument = New System.Windows.Forms.ComboBox()
        Me.lblMMNewBalance = New System.Windows.Forms.Label()
        Me.lblMMNewBalancetitle = New System.Windows.Forms.Label()
        Me.lblMMBalance = New System.Windows.Forms.Label()
        Me.lblMMBalancetitle = New System.Windows.Forms.Label()
        Me.txtMMAmount = New System.Windows.Forms.TextBox()
        Me.lblMMAmount = New System.Windows.Forms.Label()
        Me.cboMMCCY = New System.Windows.Forms.ComboBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.cboMMPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.TabRequestFOP = New System.Windows.Forms.TabPage()
        Me.dgvFOP = New System.Windows.Forms.DataGridView()
        Me.cboFOPPortfolio = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.cboPayType = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TabPayment = New System.Windows.Forms.TabControl()
        Me.TabBeneficiary = New System.Windows.Forms.TabPage()
        Me.cboTemFindName = New System.Windows.Forms.ComboBox()
        Me.TabBeneficiaryDetails = New System.Windows.Forms.TabControl()
        Me.TabMain = New System.Windows.Forms.TabPage()
        Me.cbobenrelationship = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtbensurname = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtbenmiddlename = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtbenfirstname = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.cboBenCountry = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtbenpostcode = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtbencounty = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtbencity = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cboBenType = New System.Windows.Forms.ComboBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtbenaddress1 = New System.Windows.Forms.TextBox()
        Me.txtBenBusinessName = New System.Windows.Forms.TextBox()
        Me.lblTemBenPayName = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblFinancePay = New System.Windows.Forms.Label()
        Me.txtTemSubBankCtry = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.CboTemBenSubBankName = New System.Windows.Forms.ComboBox()
        Me.txtTemSubBankIBAN = New System.Windows.Forms.TextBox()
        Me.txtTemSubBankSwift = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTemBankCtry = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.CboTemBenBankName = New System.Windows.Forms.ComboBox()
        Me.txtTemBankIBAN = New System.Windows.Forms.TextBox()
        Me.txtTemBankSwift = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.TabBank = New System.Windows.Forms.TabPage()
        Me.GrpBankDetails = New System.Windows.Forms.GroupBox()
        Me.cboLookBankCountry = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtLookBankName = New System.Windows.Forms.TextBox()
        Me.GrpLookup = New System.Windows.Forms.GroupBox()
        Me.cboLookSWIFT = New System.Windows.Forms.ComboBox()
        Me.cmdValidateBank = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtLookAccountNo = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtLookSortCode = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.TabKYC = New System.Windows.Forms.TabPage()
        Me.GrpKYC = New System.Windows.Forms.GroupBox()
        Me.cmdBrowse = New System.Windows.Forms.Button()
        Me.txtFilePath = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtKYCIssuesDisclosed = New System.Windows.Forms.TextBox()
        Me.ChkKYCCDD = New System.Windows.Forms.CheckBox()
        Me.ChkKYCPEP = New System.Windows.Forms.CheckBox()
        Me.ChkKYCsanctions = New System.Windows.Forms.CheckBox()
        Me.TabSearch = New System.Windows.Forms.TabPage()
        Me.GrpSearch = New System.Windows.Forms.GroupBox()
        Me.cmdPRSearch2 = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPRGoogleSearch2 = New System.Windows.Forms.TextBox()
        Me.cmdPRSearch = New System.Windows.Forms.Button()
        Me.txtPRGoogleSearch = New System.Windows.Forms.TextBox()
        Me.CmdPRSend = New System.Windows.Forms.Button()
        Me.GrpPREmail = New System.Windows.Forms.GroupBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.ContextMenuStripFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStripFiles1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ChkCallback = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GrpPRCriteria.SuspendLayout()
        Me.TabRequest.SuspendLayout()
        Me.TabRequestPayment.SuspendLayout()
        Me.TabRequestMM.SuspendLayout()
        Me.TabRequestFOP.SuspendLayout()
        CType(Me.dgvFOP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPayment.SuspendLayout()
        Me.TabBeneficiary.SuspendLayout()
        Me.TabBeneficiaryDetails.SuspendLayout()
        Me.TabMain.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabBank.SuspendLayout()
        Me.GrpBankDetails.SuspendLayout()
        Me.GrpLookup.SuspendLayout()
        Me.TabKYC.SuspendLayout()
        Me.GrpKYC.SuspendLayout()
        Me.TabSearch.SuspendLayout()
        Me.GrpSearch.SuspendLayout()
        Me.GrpPREmail.SuspendLayout()
        Me.ContextMenuStripFiles.SuspendLayout()
        Me.ContextMenuStripFiles1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.SteelBlue
        Me.Label1.Location = New System.Drawing.Point(275, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 24)
        Me.Label1.TabIndex = 44
        Me.Label1.Text = "Payment Request"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox7)
        Me.GroupBox1.Controls.Add(Me.GrpPRCriteria)
        Me.GroupBox1.Controls.Add(Me.CmdPRSend)
        Me.GroupBox1.Controls.Add(Me.GrpPREmail)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 36)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(671, 833)
        Me.GroupBox1.TabIndex = 46
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Request Details"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.CmdBrowseFiles)
        Me.GroupBox7.Controls.Add(Me.txtPaymentFilesPath)
        Me.GroupBox7.Controls.Add(Me.CmdFilePayment)
        Me.GroupBox7.Controls.Add(Me.LVFiles1)
        Me.GroupBox7.Location = New System.Drawing.Point(7, 651)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(655, 147)
        Me.GroupBox7.TabIndex = 49
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Compliance && Payment SSI File Attachments"
        '
        'CmdBrowseFiles
        '
        Me.CmdBrowseFiles.Location = New System.Drawing.Point(551, 18)
        Me.CmdBrowseFiles.Name = "CmdBrowseFiles"
        Me.CmdBrowseFiles.Size = New System.Drawing.Size(95, 23)
        Me.CmdBrowseFiles.TabIndex = 167
        Me.CmdBrowseFiles.Text = "Browse Files"
        Me.CmdBrowseFiles.UseVisualStyleBackColor = True
        '
        'txtPaymentFilesPath
        '
        Me.txtPaymentFilesPath.Enabled = False
        Me.txtPaymentFilesPath.Location = New System.Drawing.Point(5, 19)
        Me.txtPaymentFilesPath.Name = "txtPaymentFilesPath"
        Me.txtPaymentFilesPath.Size = New System.Drawing.Size(540, 20)
        Me.txtPaymentFilesPath.TabIndex = 166
        '
        'CmdFilePayment
        '
        Me.CmdFilePayment.Location = New System.Drawing.Point(551, 45)
        Me.CmdFilePayment.Name = "CmdFilePayment"
        Me.CmdFilePayment.Size = New System.Drawing.Size(95, 23)
        Me.CmdFilePayment.TabIndex = 43
        Me.CmdFilePayment.Text = "Refresh Files"
        Me.CmdFilePayment.UseVisualStyleBackColor = True
        '
        'LVFiles1
        '
        Me.LVFiles1.AllowDrop = True
        Me.LVFiles1.BackColor = System.Drawing.SystemColors.Window
        Me.LVFiles1.FullRowSelect = True
        Me.LVFiles1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.LVFiles1.HideSelection = False
        Me.LVFiles1.Location = New System.Drawing.Point(6, 45)
        Me.LVFiles1.MultiSelect = False
        Me.LVFiles1.Name = "LVFiles1"
        Me.LVFiles1.Size = New System.Drawing.Size(539, 96)
        Me.LVFiles1.TabIndex = 42
        Me.LVFiles1.UseCompatibleStateImageBehavior = False
        '
        'GrpPRCriteria
        '
        Me.GrpPRCriteria.Controls.Add(Me.TabRequest)
        Me.GrpPRCriteria.Controls.Add(Me.cboPayType)
        Me.GrpPRCriteria.Controls.Add(Me.Label14)
        Me.GrpPRCriteria.Controls.Add(Me.TabPayment)
        Me.GrpPRCriteria.Location = New System.Drawing.Point(7, 19)
        Me.GrpPRCriteria.Name = "GrpPRCriteria"
        Me.GrpPRCriteria.Size = New System.Drawing.Size(655, 522)
        Me.GrpPRCriteria.TabIndex = 48
        Me.GrpPRCriteria.TabStop = False
        '
        'TabRequest
        '
        Me.TabRequest.Controls.Add(Me.TabRequestPayment)
        Me.TabRequest.Controls.Add(Me.TabRequestMM)
        Me.TabRequest.Controls.Add(Me.TabRequestFOP)
        Me.TabRequest.Location = New System.Drawing.Point(6, 42)
        Me.TabRequest.Name = "TabRequest"
        Me.TabRequest.SelectedIndex = 0
        Me.TabRequest.Size = New System.Drawing.Size(640, 198)
        Me.TabRequest.TabIndex = 130
        '
        'TabRequestPayment
        '
        Me.TabRequestPayment.BackColor = System.Drawing.Color.BlanchedAlmond
        Me.TabRequestPayment.Controls.Add(Me.LblNewBalanceCCY)
        Me.TabRequestPayment.Controls.Add(Me.lblFeeBalanceCCY)
        Me.TabRequestPayment.Controls.Add(Me.lblPostBalanceCCY)
        Me.TabRequestPayment.Controls.Add(Me.lblBalanceCCY)
        Me.TabRequestPayment.Controls.Add(Me.Label32)
        Me.TabRequestPayment.Controls.Add(Me.cboFeeType)
        Me.TabRequestPayment.Controls.Add(Me.lblFeeBalance)
        Me.TabRequestPayment.Controls.Add(Me.lblFeeBalancetitle)
        Me.TabRequestPayment.Controls.Add(Me.lblPostBalance)
        Me.TabRequestPayment.Controls.Add(Me.lblPostBalancetitle)
        Me.TabRequestPayment.Controls.Add(Me.txtRef)
        Me.TabRequestPayment.Controls.Add(Me.lblRef)
        Me.TabRequestPayment.Controls.Add(Me.txtPRRate)
        Me.TabRequestPayment.Controls.Add(Me.lblTemBenPayPortfolio)
        Me.TabRequestPayment.Controls.Add(Me.cboTemBenPayPortfolio)
        Me.TabRequestPayment.Controls.Add(Me.lblNewBalance)
        Me.TabRequestPayment.Controls.Add(Me.lblNewBalancetitle)
        Me.TabRequestPayment.Controls.Add(Me.txtPRAmountFee)
        Me.TabRequestPayment.Controls.Add(Me.lblPRAmountFee)
        Me.TabRequestPayment.Controls.Add(Me.cboTemFeeCCY)
        Me.TabRequestPayment.Controls.Add(Me.lblTemFeeCCY)
        Me.TabRequestPayment.Controls.Add(Me.lblBalance)
        Me.TabRequestPayment.Controls.Add(Me.lblBalancetitle)
        Me.TabRequestPayment.Controls.Add(Me.txtPRAmount)
        Me.TabRequestPayment.Controls.Add(Me.lblPRAmount)
        Me.TabRequestPayment.Controls.Add(Me.cboTemFindCCY)
        Me.TabRequestPayment.Controls.Add(Me.Label7)
        Me.TabRequestPayment.Controls.Add(Me.cboTemFindPortfolio)
        Me.TabRequestPayment.Controls.Add(Me.Label10)
        Me.TabRequestPayment.Location = New System.Drawing.Point(4, 23)
        Me.TabRequestPayment.Name = "TabRequestPayment"
        Me.TabRequestPayment.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRequestPayment.Size = New System.Drawing.Size(632, 171)
        Me.TabRequestPayment.TabIndex = 0
        Me.TabRequestPayment.Text = "Payment"
        '
        'LblNewBalanceCCY
        '
        Me.LblNewBalanceCCY.AutoSize = True
        Me.LblNewBalanceCCY.Location = New System.Drawing.Point(598, 114)
        Me.LblNewBalanceCCY.Name = "LblNewBalanceCCY"
        Me.LblNewBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.LblNewBalanceCCY.TabIndex = 203
        Me.LblNewBalanceCCY.Text = "GBP"
        Me.LblNewBalanceCCY.Visible = False
        '
        'lblFeeBalanceCCY
        '
        Me.lblFeeBalanceCCY.AutoSize = True
        Me.lblFeeBalanceCCY.Location = New System.Drawing.Point(598, 86)
        Me.lblFeeBalanceCCY.Name = "lblFeeBalanceCCY"
        Me.lblFeeBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblFeeBalanceCCY.TabIndex = 202
        Me.lblFeeBalanceCCY.Text = "GBP"
        Me.lblFeeBalanceCCY.Visible = False
        '
        'lblPostBalanceCCY
        '
        Me.lblPostBalanceCCY.AutoSize = True
        Me.lblPostBalanceCCY.Location = New System.Drawing.Point(598, 57)
        Me.lblPostBalanceCCY.Name = "lblPostBalanceCCY"
        Me.lblPostBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblPostBalanceCCY.TabIndex = 201
        Me.lblPostBalanceCCY.Text = "GBP"
        Me.lblPostBalanceCCY.Visible = False
        '
        'lblBalanceCCY
        '
        Me.lblBalanceCCY.AutoSize = True
        Me.lblBalanceCCY.Location = New System.Drawing.Point(598, 26)
        Me.lblBalanceCCY.Name = "lblBalanceCCY"
        Me.lblBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblBalanceCCY.TabIndex = 200
        Me.lblBalanceCCY.Text = "GBP"
        Me.lblBalanceCCY.Visible = False
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(23, 85)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(54, 14)
        Me.Label32.TabIndex = 199
        Me.Label32.Text = "Fee Type:"
        Me.Label32.Visible = False
        '
        'cboFeeType
        '
        Me.cboFeeType.BackColor = System.Drawing.Color.White
        Me.cboFeeType.FormattingEnabled = True
        Me.cboFeeType.Location = New System.Drawing.Point(83, 82)
        Me.cboFeeType.Name = "cboFeeType"
        Me.cboFeeType.Size = New System.Drawing.Size(64, 22)
        Me.cboFeeType.TabIndex = 178
        Me.cboFeeType.Visible = False
        '
        'lblFeeBalance
        '
        Me.lblFeeBalance.AutoSize = True
        Me.lblFeeBalance.Location = New System.Drawing.Point(504, 85)
        Me.lblFeeBalance.Name = "lblFeeBalance"
        Me.lblFeeBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblFeeBalance.TabIndex = 198
        Me.lblFeeBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblFeeBalance.Visible = False
        '
        'lblFeeBalancetitle
        '
        Me.lblFeeBalancetitle.AutoSize = True
        Me.lblFeeBalancetitle.Location = New System.Drawing.Point(427, 86)
        Me.lblFeeBalancetitle.Name = "lblFeeBalancetitle"
        Me.lblFeeBalancetitle.Size = New System.Drawing.Size(71, 14)
        Me.lblFeeBalancetitle.TabIndex = 197
        Me.lblFeeBalancetitle.Text = "Fee CCY Bal:"
        Me.lblFeeBalancetitle.Visible = False
        '
        'lblPostBalance
        '
        Me.lblPostBalance.AutoSize = True
        Me.lblPostBalance.Location = New System.Drawing.Point(504, 57)
        Me.lblPostBalance.Name = "lblPostBalance"
        Me.lblPostBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblPostBalance.TabIndex = 196
        Me.lblPostBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblPostBalance.Visible = False
        '
        'lblPostBalancetitle
        '
        Me.lblPostBalancetitle.AutoSize = True
        Me.lblPostBalancetitle.Location = New System.Drawing.Point(425, 57)
        Me.lblPostBalancetitle.Name = "lblPostBalancetitle"
        Me.lblPostBalancetitle.Size = New System.Drawing.Size(73, 14)
        Me.lblPostBalancetitle.TabIndex = 195
        Me.lblPostBalancetitle.Text = "Post Balance:"
        Me.lblPostBalancetitle.Visible = False
        '
        'txtRef
        '
        Me.txtRef.Location = New System.Drawing.Point(83, 137)
        Me.txtRef.Name = "txtRef"
        Me.txtRef.Size = New System.Drawing.Size(540, 20)
        Me.txtRef.TabIndex = 183
        '
        'lblRef
        '
        Me.lblRef.AutoSize = True
        Me.lblRef.Location = New System.Drawing.Point(23, 140)
        Me.lblRef.Name = "lblRef"
        Me.lblRef.Size = New System.Drawing.Size(54, 14)
        Me.lblRef.TabIndex = 194
        Me.lblRef.Text = "Loan Ref:"
        '
        'txtPRRate
        '
        Me.txtPRRate.Location = New System.Drawing.Point(153, 82)
        Me.txtPRRate.Name = "txtPRRate"
        Me.txtPRRate.Size = New System.Drawing.Size(53, 20)
        Me.txtPRRate.TabIndex = 179
        Me.txtPRRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPRRate.Visible = False
        '
        'lblTemBenPayPortfolio
        '
        Me.lblTemBenPayPortfolio.AutoSize = True
        Me.lblTemBenPayPortfolio.Location = New System.Drawing.Point(7, 114)
        Me.lblTemBenPayPortfolio.Name = "lblTemBenPayPortfolio"
        Me.lblTemBenPayPortfolio.Size = New System.Drawing.Size(71, 14)
        Me.lblTemBenPayPortfolio.TabIndex = 193
        Me.lblTemBenPayPortfolio.Text = "Ben Portfolio:"
        '
        'cboTemBenPayPortfolio
        '
        Me.cboTemBenPayPortfolio.BackColor = System.Drawing.Color.White
        Me.cboTemBenPayPortfolio.FormattingEnabled = True
        Me.cboTemBenPayPortfolio.Location = New System.Drawing.Point(83, 111)
        Me.cboTemBenPayPortfolio.Name = "cboTemBenPayPortfolio"
        Me.cboTemBenPayPortfolio.Size = New System.Drawing.Size(337, 22)
        Me.cboTemBenPayPortfolio.TabIndex = 182
        '
        'lblNewBalance
        '
        Me.lblNewBalance.AutoSize = True
        Me.lblNewBalance.Location = New System.Drawing.Point(504, 114)
        Me.lblNewBalance.Name = "lblNewBalance"
        Me.lblNewBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblNewBalance.TabIndex = 192
        Me.lblNewBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblNewBalance.Visible = False
        '
        'lblNewBalancetitle
        '
        Me.lblNewBalancetitle.AutoSize = True
        Me.lblNewBalancetitle.Location = New System.Drawing.Point(423, 114)
        Me.lblNewBalancetitle.Name = "lblNewBalancetitle"
        Me.lblNewBalancetitle.Size = New System.Drawing.Size(75, 14)
        Me.lblNewBalancetitle.TabIndex = 191
        Me.lblNewBalancetitle.Text = "New Balance:"
        Me.lblNewBalancetitle.Visible = False
        '
        'txtPRAmountFee
        '
        Me.txtPRAmountFee.Location = New System.Drawing.Point(246, 82)
        Me.txtPRAmountFee.Name = "txtPRAmountFee"
        Me.txtPRAmountFee.Size = New System.Drawing.Size(75, 20)
        Me.txtPRAmountFee.TabIndex = 180
        Me.txtPRAmountFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPRAmountFee.Visible = False
        '
        'lblPRAmountFee
        '
        Me.lblPRAmountFee.AutoSize = True
        Me.lblPRAmountFee.Location = New System.Drawing.Point(212, 85)
        Me.lblPRAmountFee.Name = "lblPRAmountFee"
        Me.lblPRAmountFee.Size = New System.Drawing.Size(28, 14)
        Me.lblPRAmountFee.TabIndex = 190
        Me.lblPRAmountFee.Text = "Fee:"
        Me.lblPRAmountFee.Visible = False
        '
        'cboTemFeeCCY
        '
        Me.cboTemFeeCCY.BackColor = System.Drawing.Color.White
        Me.cboTemFeeCCY.FormattingEnabled = True
        Me.cboTemFeeCCY.Location = New System.Drawing.Point(363, 83)
        Me.cboTemFeeCCY.Name = "cboTemFeeCCY"
        Me.cboTemFeeCCY.Size = New System.Drawing.Size(57, 22)
        Me.cboTemFeeCCY.TabIndex = 181
        Me.cboTemFeeCCY.Visible = False
        '
        'lblTemFeeCCY
        '
        Me.lblTemFeeCCY.AutoSize = True
        Me.lblTemFeeCCY.Location = New System.Drawing.Point(326, 86)
        Me.lblTemFeeCCY.Name = "lblTemFeeCCY"
        Me.lblTemFeeCCY.Size = New System.Drawing.Size(31, 14)
        Me.lblTemFeeCCY.TabIndex = 189
        Me.lblTemFeeCCY.Text = "CCY:"
        Me.lblTemFeeCCY.Visible = False
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.Location = New System.Drawing.Point(504, 26)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblBalance.TabIndex = 188
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblBalance.Visible = False
        '
        'lblBalancetitle
        '
        Me.lblBalancetitle.AutoSize = True
        Me.lblBalancetitle.Location = New System.Drawing.Point(420, 26)
        Me.lblBalancetitle.Name = "lblBalancetitle"
        Me.lblBalancetitle.Size = New System.Drawing.Size(78, 14)
        Me.lblBalancetitle.TabIndex = 187
        Me.lblBalancetitle.Text = "Open Balance:"
        Me.lblBalancetitle.Visible = False
        '
        'txtPRAmount
        '
        Me.txtPRAmount.Location = New System.Drawing.Point(199, 55)
        Me.txtPRAmount.Name = "txtPRAmount"
        Me.txtPRAmount.Size = New System.Drawing.Size(122, 20)
        Me.txtPRAmount.TabIndex = 176
        Me.txtPRAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPRAmount
        '
        Me.lblPRAmount.AutoSize = True
        Me.lblPRAmount.Location = New System.Drawing.Point(164, 58)
        Me.lblPRAmount.Name = "lblPRAmount"
        Me.lblPRAmount.Size = New System.Drawing.Size(29, 14)
        Me.lblPRAmount.TabIndex = 186
        Me.lblPRAmount.Text = "Amt:"
        '
        'cboTemFindCCY
        '
        Me.cboTemFindCCY.BackColor = System.Drawing.Color.White
        Me.cboTemFindCCY.FormattingEnabled = True
        Me.cboTemFindCCY.Location = New System.Drawing.Point(363, 55)
        Me.cboTemFindCCY.Name = "cboTemFindCCY"
        Me.cboTemFindCCY.Size = New System.Drawing.Size(57, 22)
        Me.cboTemFindCCY.TabIndex = 177
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(326, 57)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 14)
        Me.Label7.TabIndex = 185
        Me.Label7.Text = "CCY:"
        '
        'cboTemFindPortfolio
        '
        Me.cboTemFindPortfolio.BackColor = System.Drawing.Color.White
        Me.cboTemFindPortfolio.FormattingEnabled = True
        Me.cboTemFindPortfolio.Location = New System.Drawing.Point(83, 23)
        Me.cboTemFindPortfolio.Name = "cboTemFindPortfolio"
        Me.cboTemFindPortfolio.Size = New System.Drawing.Size(337, 22)
        Me.cboTemFindPortfolio.TabIndex = 175
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(28, 26)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 14)
        Me.Label10.TabIndex = 184
        Me.Label10.Text = "Portfolio:"
        '
        'TabRequestMM
        '
        Me.TabRequestMM.BackColor = System.Drawing.Color.LemonChiffon
        Me.TabRequestMM.Controls.Add(Me.Label35)
        Me.TabRequestMM.Controls.Add(Me.cboMMBuySell)
        Me.TabRequestMM.Controls.Add(Me.lblHeldEndDate)
        Me.TabRequestMM.Controls.Add(Me.dtHeldEndDate)
        Me.TabRequestMM.Controls.Add(Me.lblHeldStartDate)
        Me.TabRequestMM.Controls.Add(Me.dtHeldStartDate)
        Me.TabRequestMM.Controls.Add(Me.LblMMNewBalanceCCY)
        Me.TabRequestMM.Controls.Add(Me.lblMMInstrumentBalanceCCY)
        Me.TabRequestMM.Controls.Add(Me.lblMMPostBalanceCCY)
        Me.TabRequestMM.Controls.Add(Me.lblMMBalanceCCY)
        Me.TabRequestMM.Controls.Add(Me.lblMMHeldType)
        Me.TabRequestMM.Controls.Add(Me.cboMMHeldType)
        Me.TabRequestMM.Controls.Add(Me.lblMMInstrumentBalance)
        Me.TabRequestMM.Controls.Add(Me.lblMMInstrumentBalancetitle)
        Me.TabRequestMM.Controls.Add(Me.lblMMPostBalance)
        Me.TabRequestMM.Controls.Add(Me.lblMMPostBalancetitle)
        Me.TabRequestMM.Controls.Add(Me.lblMMInstrument)
        Me.TabRequestMM.Controls.Add(Me.cboMMInstrument)
        Me.TabRequestMM.Controls.Add(Me.lblMMNewBalance)
        Me.TabRequestMM.Controls.Add(Me.lblMMNewBalancetitle)
        Me.TabRequestMM.Controls.Add(Me.lblMMBalance)
        Me.TabRequestMM.Controls.Add(Me.lblMMBalancetitle)
        Me.TabRequestMM.Controls.Add(Me.txtMMAmount)
        Me.TabRequestMM.Controls.Add(Me.lblMMAmount)
        Me.TabRequestMM.Controls.Add(Me.cboMMCCY)
        Me.TabRequestMM.Controls.Add(Me.Label52)
        Me.TabRequestMM.Controls.Add(Me.cboMMPortfolio)
        Me.TabRequestMM.Controls.Add(Me.Label53)
        Me.TabRequestMM.Location = New System.Drawing.Point(4, 23)
        Me.TabRequestMM.Name = "TabRequestMM"
        Me.TabRequestMM.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRequestMM.Size = New System.Drawing.Size(632, 171)
        Me.TabRequestMM.TabIndex = 1
        Me.TabRequestMM.Text = "Money Market"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(19, 56)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(49, 14)
        Me.Label35.TabIndex = 236
        Me.Label35.Text = "Buy/Sell:"
        '
        'cboMMBuySell
        '
        Me.cboMMBuySell.BackColor = System.Drawing.Color.White
        Me.cboMMBuySell.FormattingEnabled = True
        Me.cboMMBuySell.Location = New System.Drawing.Point(75, 54)
        Me.cboMMBuySell.Name = "cboMMBuySell"
        Me.cboMMBuySell.Size = New System.Drawing.Size(82, 22)
        Me.cboMMBuySell.TabIndex = 235
        '
        'lblHeldEndDate
        '
        Me.lblHeldEndDate.AutoSize = True
        Me.lblHeldEndDate.Location = New System.Drawing.Point(191, 145)
        Me.lblHeldEndDate.Name = "lblHeldEndDate"
        Me.lblHeldEndDate.Size = New System.Drawing.Size(21, 14)
        Me.lblHeldEndDate.TabIndex = 234
        Me.lblHeldEndDate.Text = "To:"
        Me.lblHeldEndDate.Visible = False
        '
        'dtHeldEndDate
        '
        Me.dtHeldEndDate.Location = New System.Drawing.Point(218, 140)
        Me.dtHeldEndDate.Name = "dtHeldEndDate"
        Me.dtHeldEndDate.Size = New System.Drawing.Size(200, 20)
        Me.dtHeldEndDate.TabIndex = 233
        Me.dtHeldEndDate.Visible = False
        '
        'lblHeldStartDate
        '
        Me.lblHeldStartDate.AutoSize = True
        Me.lblHeldStartDate.Location = New System.Drawing.Point(178, 121)
        Me.lblHeldStartDate.Name = "lblHeldStartDate"
        Me.lblHeldStartDate.Size = New System.Drawing.Size(34, 14)
        Me.lblHeldStartDate.TabIndex = 232
        Me.lblHeldStartDate.Text = "From:"
        '
        'dtHeldStartDate
        '
        Me.dtHeldStartDate.Location = New System.Drawing.Point(218, 118)
        Me.dtHeldStartDate.Name = "dtHeldStartDate"
        Me.dtHeldStartDate.Size = New System.Drawing.Size(200, 20)
        Me.dtHeldStartDate.TabIndex = 231
        '
        'LblMMNewBalanceCCY
        '
        Me.LblMMNewBalanceCCY.AutoSize = True
        Me.LblMMNewBalanceCCY.Location = New System.Drawing.Point(600, 121)
        Me.LblMMNewBalanceCCY.Name = "LblMMNewBalanceCCY"
        Me.LblMMNewBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.LblMMNewBalanceCCY.TabIndex = 230
        Me.LblMMNewBalanceCCY.Text = "GBP"
        Me.LblMMNewBalanceCCY.Visible = False
        '
        'lblMMInstrumentBalanceCCY
        '
        Me.lblMMInstrumentBalanceCCY.AutoSize = True
        Me.lblMMInstrumentBalanceCCY.Location = New System.Drawing.Point(600, 88)
        Me.lblMMInstrumentBalanceCCY.Name = "lblMMInstrumentBalanceCCY"
        Me.lblMMInstrumentBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblMMInstrumentBalanceCCY.TabIndex = 229
        Me.lblMMInstrumentBalanceCCY.Text = "GBP"
        Me.lblMMInstrumentBalanceCCY.Visible = False
        '
        'lblMMPostBalanceCCY
        '
        Me.lblMMPostBalanceCCY.AutoSize = True
        Me.lblMMPostBalanceCCY.Location = New System.Drawing.Point(600, 57)
        Me.lblMMPostBalanceCCY.Name = "lblMMPostBalanceCCY"
        Me.lblMMPostBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblMMPostBalanceCCY.TabIndex = 228
        Me.lblMMPostBalanceCCY.Text = "GBP"
        Me.lblMMPostBalanceCCY.Visible = False
        '
        'lblMMBalanceCCY
        '
        Me.lblMMBalanceCCY.AutoSize = True
        Me.lblMMBalanceCCY.Location = New System.Drawing.Point(600, 26)
        Me.lblMMBalanceCCY.Name = "lblMMBalanceCCY"
        Me.lblMMBalanceCCY.Size = New System.Drawing.Size(28, 14)
        Me.lblMMBalanceCCY.TabIndex = 227
        Me.lblMMBalanceCCY.Text = "GBP"
        Me.lblMMBalanceCCY.Visible = False
        '
        'lblMMHeldType
        '
        Me.lblMMHeldType.AutoSize = True
        Me.lblMMHeldType.Location = New System.Drawing.Point(23, 121)
        Me.lblMMHeldType.Name = "lblMMHeldType"
        Me.lblMMHeldType.Size = New System.Drawing.Size(50, 14)
        Me.lblMMHeldType.TabIndex = 226
        Me.lblMMHeldType.Text = "Duration:"
        '
        'cboMMHeldType
        '
        Me.cboMMHeldType.BackColor = System.Drawing.Color.White
        Me.cboMMHeldType.FormattingEnabled = True
        Me.cboMMHeldType.Location = New System.Drawing.Point(75, 118)
        Me.cboMMHeldType.Name = "cboMMHeldType"
        Me.cboMMHeldType.Size = New System.Drawing.Size(82, 22)
        Me.cboMMHeldType.TabIndex = 207
        '
        'lblMMInstrumentBalance
        '
        Me.lblMMInstrumentBalance.AutoSize = True
        Me.lblMMInstrumentBalance.Location = New System.Drawing.Point(506, 88)
        Me.lblMMInstrumentBalance.Name = "lblMMInstrumentBalance"
        Me.lblMMInstrumentBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblMMInstrumentBalance.TabIndex = 225
        Me.lblMMInstrumentBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMMInstrumentBalance.Visible = False
        '
        'lblMMInstrumentBalancetitle
        '
        Me.lblMMInstrumentBalancetitle.AutoSize = True
        Me.lblMMInstrumentBalancetitle.Location = New System.Drawing.Point(429, 88)
        Me.lblMMInstrumentBalancetitle.Name = "lblMMInstrumentBalancetitle"
        Me.lblMMInstrumentBalancetitle.Size = New System.Drawing.Size(68, 14)
        Me.lblMMInstrumentBalancetitle.TabIndex = 224
        Me.lblMMInstrumentBalancetitle.Text = "Inst Bal held:"
        Me.lblMMInstrumentBalancetitle.Visible = False
        '
        'lblMMPostBalance
        '
        Me.lblMMPostBalance.AutoSize = True
        Me.lblMMPostBalance.Location = New System.Drawing.Point(506, 57)
        Me.lblMMPostBalance.Name = "lblMMPostBalance"
        Me.lblMMPostBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblMMPostBalance.TabIndex = 223
        Me.lblMMPostBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMMPostBalance.Visible = False
        '
        'lblMMPostBalancetitle
        '
        Me.lblMMPostBalancetitle.AutoSize = True
        Me.lblMMPostBalancetitle.Location = New System.Drawing.Point(427, 57)
        Me.lblMMPostBalancetitle.Name = "lblMMPostBalancetitle"
        Me.lblMMPostBalancetitle.Size = New System.Drawing.Size(73, 14)
        Me.lblMMPostBalancetitle.TabIndex = 222
        Me.lblMMPostBalancetitle.Text = "Post Balance:"
        Me.lblMMPostBalancetitle.Visible = False
        '
        'lblMMInstrument
        '
        Me.lblMMInstrument.AutoSize = True
        Me.lblMMInstrument.Location = New System.Drawing.Point(9, 88)
        Me.lblMMInstrument.Name = "lblMMInstrument"
        Me.lblMMInstrument.Size = New System.Drawing.Size(60, 14)
        Me.lblMMInstrument.TabIndex = 221
        Me.lblMMInstrument.Text = "Instrument:"
        '
        'cboMMInstrument
        '
        Me.cboMMInstrument.BackColor = System.Drawing.Color.White
        Me.cboMMInstrument.FormattingEnabled = True
        Me.cboMMInstrument.Location = New System.Drawing.Point(75, 85)
        Me.cboMMInstrument.Name = "cboMMInstrument"
        Me.cboMMInstrument.Size = New System.Drawing.Size(345, 22)
        Me.cboMMInstrument.TabIndex = 211
        '
        'lblMMNewBalance
        '
        Me.lblMMNewBalance.AutoSize = True
        Me.lblMMNewBalance.Location = New System.Drawing.Point(506, 121)
        Me.lblMMNewBalance.Name = "lblMMNewBalance"
        Me.lblMMNewBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblMMNewBalance.TabIndex = 220
        Me.lblMMNewBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMMNewBalance.Visible = False
        '
        'lblMMNewBalancetitle
        '
        Me.lblMMNewBalancetitle.AutoSize = True
        Me.lblMMNewBalancetitle.Location = New System.Drawing.Point(425, 121)
        Me.lblMMNewBalancetitle.Name = "lblMMNewBalancetitle"
        Me.lblMMNewBalancetitle.Size = New System.Drawing.Size(75, 14)
        Me.lblMMNewBalancetitle.TabIndex = 219
        Me.lblMMNewBalancetitle.Text = "New Balance:"
        Me.lblMMNewBalancetitle.Visible = False
        '
        'lblMMBalance
        '
        Me.lblMMBalance.AutoSize = True
        Me.lblMMBalance.Location = New System.Drawing.Point(506, 26)
        Me.lblMMBalance.Name = "lblMMBalance"
        Me.lblMMBalance.Size = New System.Drawing.Size(0, 14)
        Me.lblMMBalance.TabIndex = 216
        Me.lblMMBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMMBalance.Visible = False
        '
        'lblMMBalancetitle
        '
        Me.lblMMBalancetitle.AutoSize = True
        Me.lblMMBalancetitle.Location = New System.Drawing.Point(422, 26)
        Me.lblMMBalancetitle.Name = "lblMMBalancetitle"
        Me.lblMMBalancetitle.Size = New System.Drawing.Size(78, 14)
        Me.lblMMBalancetitle.TabIndex = 215
        Me.lblMMBalancetitle.Text = "Open Balance:"
        Me.lblMMBalancetitle.Visible = False
        '
        'txtMMAmount
        '
        Me.txtMMAmount.Location = New System.Drawing.Point(198, 54)
        Me.txtMMAmount.Name = "txtMMAmount"
        Me.txtMMAmount.Size = New System.Drawing.Size(122, 20)
        Me.txtMMAmount.TabIndex = 205
        Me.txtMMAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMMAmount
        '
        Me.lblMMAmount.AutoSize = True
        Me.lblMMAmount.Location = New System.Drawing.Point(163, 57)
        Me.lblMMAmount.Name = "lblMMAmount"
        Me.lblMMAmount.Size = New System.Drawing.Size(29, 14)
        Me.lblMMAmount.TabIndex = 214
        Me.lblMMAmount.Text = "Amt:"
        '
        'cboMMCCY
        '
        Me.cboMMCCY.BackColor = System.Drawing.Color.White
        Me.cboMMCCY.FormattingEnabled = True
        Me.cboMMCCY.Location = New System.Drawing.Point(362, 54)
        Me.cboMMCCY.Name = "cboMMCCY"
        Me.cboMMCCY.Size = New System.Drawing.Size(57, 22)
        Me.cboMMCCY.TabIndex = 206
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(326, 56)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(31, 14)
        Me.Label52.TabIndex = 213
        Me.Label52.Text = "CCY:"
        '
        'cboMMPortfolio
        '
        Me.cboMMPortfolio.BackColor = System.Drawing.Color.White
        Me.cboMMPortfolio.FormattingEnabled = True
        Me.cboMMPortfolio.Location = New System.Drawing.Point(75, 22)
        Me.cboMMPortfolio.Name = "cboMMPortfolio"
        Me.cboMMPortfolio.Size = New System.Drawing.Size(344, 22)
        Me.cboMMPortfolio.TabIndex = 204
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(20, 25)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(49, 14)
        Me.Label53.TabIndex = 212
        Me.Label53.Text = "Portfolio:"
        '
        'TabRequestFOP
        '
        Me.TabRequestFOP.BackColor = System.Drawing.Color.Beige
        Me.TabRequestFOP.Controls.Add(Me.dgvFOP)
        Me.TabRequestFOP.Controls.Add(Me.cboFOPPortfolio)
        Me.TabRequestFOP.Controls.Add(Me.Label40)
        Me.TabRequestFOP.Location = New System.Drawing.Point(4, 23)
        Me.TabRequestFOP.Name = "TabRequestFOP"
        Me.TabRequestFOP.Padding = New System.Windows.Forms.Padding(3)
        Me.TabRequestFOP.Size = New System.Drawing.Size(632, 171)
        Me.TabRequestFOP.TabIndex = 2
        Me.TabRequestFOP.Text = "FOP Request"
        '
        'dgvFOP
        '
        Me.dgvFOP.AllowUserToResizeColumns = False
        Me.dgvFOP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFOP.Location = New System.Drawing.Point(6, 34)
        Me.dgvFOP.Name = "dgvFOP"
        Me.dgvFOP.Size = New System.Drawing.Size(620, 131)
        Me.dgvFOP.TabIndex = 242
        '
        'cboFOPPortfolio
        '
        Me.cboFOPPortfolio.BackColor = System.Drawing.Color.White
        Me.cboFOPPortfolio.FormattingEnabled = True
        Me.cboFOPPortfolio.Location = New System.Drawing.Point(282, 6)
        Me.cboFOPPortfolio.Name = "cboFOPPortfolio"
        Me.cboFOPPortfolio.Size = New System.Drawing.Size(344, 22)
        Me.cboFOPPortfolio.TabIndex = 237
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(227, 9)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(49, 14)
        Me.Label40.TabIndex = 241
        Me.Label40.Text = "Portfolio:"
        '
        'cboPayType
        '
        Me.cboPayType.BackColor = System.Drawing.Color.White
        Me.cboPayType.FormattingEnabled = True
        Me.cboPayType.Location = New System.Drawing.Point(270, 19)
        Me.cboPayType.Name = "cboPayType"
        Me.cboPayType.Size = New System.Drawing.Size(374, 22)
        Me.cboPayType.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(210, 22)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 14)
        Me.Label14.TabIndex = 129
        Me.Label14.Text = "Pay Type:"
        '
        'TabPayment
        '
        Me.TabPayment.Controls.Add(Me.TabBeneficiary)
        Me.TabPayment.Controls.Add(Me.TabBank)
        Me.TabPayment.Controls.Add(Me.TabKYC)
        Me.TabPayment.Controls.Add(Me.TabSearch)
        Me.TabPayment.Location = New System.Drawing.Point(5, 246)
        Me.TabPayment.Name = "TabPayment"
        Me.TabPayment.SelectedIndex = 0
        Me.TabPayment.Size = New System.Drawing.Size(642, 270)
        Me.TabPayment.TabIndex = 59
        '
        'TabBeneficiary
        '
        Me.TabBeneficiary.BackColor = System.Drawing.Color.OldLace
        Me.TabBeneficiary.Controls.Add(Me.cboTemFindName)
        Me.TabBeneficiary.Controls.Add(Me.TabBeneficiaryDetails)
        Me.TabBeneficiary.Controls.Add(Me.Label30)
        Me.TabBeneficiary.Location = New System.Drawing.Point(4, 23)
        Me.TabBeneficiary.Name = "TabBeneficiary"
        Me.TabBeneficiary.Size = New System.Drawing.Size(634, 243)
        Me.TabBeneficiary.TabIndex = 2
        Me.TabBeneficiary.Text = "Beneficiary"
        '
        'cboTemFindName
        '
        Me.cboTemFindName.BackColor = System.Drawing.Color.White
        Me.cboTemFindName.FormattingEnabled = True
        Me.cboTemFindName.Location = New System.Drawing.Point(98, 16)
        Me.cboTemFindName.Name = "cboTemFindName"
        Me.cboTemFindName.Size = New System.Drawing.Size(529, 22)
        Me.cboTemFindName.TabIndex = 10
        '
        'TabBeneficiaryDetails
        '
        Me.TabBeneficiaryDetails.Controls.Add(Me.TabMain)
        Me.TabBeneficiaryDetails.Controls.Add(Me.TabPage2)
        Me.TabBeneficiaryDetails.Location = New System.Drawing.Point(6, 44)
        Me.TabBeneficiaryDetails.Name = "TabBeneficiaryDetails"
        Me.TabBeneficiaryDetails.SelectedIndex = 0
        Me.TabBeneficiaryDetails.Size = New System.Drawing.Size(625, 196)
        Me.TabBeneficiaryDetails.TabIndex = 122
        '
        'TabMain
        '
        Me.TabMain.BackColor = System.Drawing.Color.OldLace
        Me.TabMain.Controls.Add(Me.cbobenrelationship)
        Me.TabMain.Controls.Add(Me.Label29)
        Me.TabMain.Controls.Add(Me.txtbensurname)
        Me.TabMain.Controls.Add(Me.Label28)
        Me.TabMain.Controls.Add(Me.txtbenmiddlename)
        Me.TabMain.Controls.Add(Me.Label27)
        Me.TabMain.Controls.Add(Me.txtbenfirstname)
        Me.TabMain.Controls.Add(Me.Label26)
        Me.TabMain.Controls.Add(Me.cboBenCountry)
        Me.TabMain.Controls.Add(Me.Label25)
        Me.TabMain.Controls.Add(Me.Label24)
        Me.TabMain.Controls.Add(Me.txtbenpostcode)
        Me.TabMain.Controls.Add(Me.Label23)
        Me.TabMain.Controls.Add(Me.txtbencounty)
        Me.TabMain.Controls.Add(Me.Label22)
        Me.TabMain.Controls.Add(Me.txtbencity)
        Me.TabMain.Controls.Add(Me.Label21)
        Me.TabMain.Controls.Add(Me.cboBenType)
        Me.TabMain.Controls.Add(Me.Label20)
        Me.TabMain.Controls.Add(Me.txtbenaddress1)
        Me.TabMain.Controls.Add(Me.txtBenBusinessName)
        Me.TabMain.Controls.Add(Me.lblTemBenPayName)
        Me.TabMain.Location = New System.Drawing.Point(4, 23)
        Me.TabMain.Name = "TabMain"
        Me.TabMain.Padding = New System.Windows.Forms.Padding(3)
        Me.TabMain.Size = New System.Drawing.Size(617, 169)
        Me.TabMain.TabIndex = 0
        Me.TabMain.Text = "Main"
        '
        'cbobenrelationship
        '
        Me.cbobenrelationship.BackColor = System.Drawing.Color.White
        Me.cbobenrelationship.FormattingEnabled = True
        Me.cbobenrelationship.Location = New System.Drawing.Point(397, 5)
        Me.cbobenrelationship.Name = "cbobenrelationship"
        Me.cbobenrelationship.Size = New System.Drawing.Size(213, 22)
        Me.cbobenrelationship.TabIndex = 12
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(323, 8)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(68, 14)
        Me.Label29.TabIndex = 141
        Me.Label29.Text = "Relationship:"
        '
        'txtbensurname
        '
        Me.txtbensurname.Location = New System.Drawing.Point(93, 85)
        Me.txtbensurname.Name = "txtbensurname"
        Me.txtbensurname.Size = New System.Drawing.Size(516, 20)
        Me.txtbensurname.TabIndex = 16
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(34, 85)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(53, 14)
        Me.Label28.TabIndex = 138
        Me.Label28.Text = "Surname:"
        '
        'txtbenmiddlename
        '
        Me.txtbenmiddlename.Location = New System.Drawing.Point(379, 59)
        Me.txtbenmiddlename.Name = "txtbenmiddlename"
        Me.txtbenmiddlename.Size = New System.Drawing.Size(230, 20)
        Me.txtbenmiddlename.TabIndex = 15
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(303, 62)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(70, 14)
        Me.Label27.TabIndex = 136
        Me.Label27.Text = "Middle Name:"
        '
        'txtbenfirstname
        '
        Me.txtbenfirstname.Location = New System.Drawing.Point(93, 59)
        Me.txtbenfirstname.Name = "txtbenfirstname"
        Me.txtbenfirstname.Size = New System.Drawing.Size(204, 20)
        Me.txtbenfirstname.TabIndex = 14
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(26, 62)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(61, 14)
        Me.Label26.TabIndex = 134
        Me.Label26.Text = "First Name:"
        '
        'cboBenCountry
        '
        Me.cboBenCountry.BackColor = System.Drawing.Color.White
        Me.cboBenCountry.FormattingEnabled = True
        Me.cboBenCountry.Location = New System.Drawing.Point(424, 137)
        Me.cboBenCountry.Name = "cboBenCountry"
        Me.cboBenCountry.Size = New System.Drawing.Size(184, 22)
        Me.cboBenCountry.TabIndex = 21
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(370, 140)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(48, 14)
        Me.Label25.TabIndex = 133
        Me.Label25.Text = "Country:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(217, 140)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 14)
        Me.Label24.TabIndex = 131
        Me.Label24.Text = "Post Code:"
        '
        'txtbenpostcode
        '
        Me.txtbenpostcode.Location = New System.Drawing.Point(281, 137)
        Me.txtbenpostcode.Name = "txtbenpostcode"
        Me.txtbenpostcode.Size = New System.Drawing.Size(84, 20)
        Me.txtbenpostcode.TabIndex = 20
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(42, 140)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(44, 14)
        Me.Label23.TabIndex = 129
        Me.Label23.Text = "County:"
        '
        'txtbencounty
        '
        Me.txtbencounty.Location = New System.Drawing.Point(92, 137)
        Me.txtbencounty.Name = "txtbencounty"
        Me.txtbencounty.Size = New System.Drawing.Size(118, 20)
        Me.txtbencounty.TabIndex = 19
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(390, 114)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(28, 14)
        Me.Label22.TabIndex = 127
        Me.Label22.Text = "City:"
        '
        'txtbencity
        '
        Me.txtbencity.Location = New System.Drawing.Point(424, 111)
        Me.txtbencity.Name = "txtbencity"
        Me.txtbencity.Size = New System.Drawing.Size(185, 20)
        Me.txtbencity.TabIndex = 18
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(34, 114)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(52, 14)
        Me.Label21.TabIndex = 125
        Me.Label21.Text = "Address:"
        '
        'cboBenType
        '
        Me.cboBenType.BackColor = System.Drawing.Color.White
        Me.cboBenType.FormattingEnabled = True
        Me.cboBenType.Location = New System.Drawing.Point(92, 5)
        Me.cboBenType.Name = "cboBenType"
        Me.cboBenType.Size = New System.Drawing.Size(225, 22)
        Me.cboBenType.TabIndex = 11
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(31, 8)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(55, 14)
        Me.Label20.TabIndex = 124
        Me.Label20.Text = "Ben Type:"
        '
        'txtbenaddress1
        '
        Me.txtbenaddress1.Location = New System.Drawing.Point(92, 111)
        Me.txtbenaddress1.Name = "txtbenaddress1"
        Me.txtbenaddress1.Size = New System.Drawing.Size(273, 20)
        Me.txtbenaddress1.TabIndex = 17
        '
        'txtBenBusinessName
        '
        Me.txtBenBusinessName.Location = New System.Drawing.Point(93, 33)
        Me.txtBenBusinessName.Name = "txtBenBusinessName"
        Me.txtBenBusinessName.Size = New System.Drawing.Size(516, 20)
        Me.txtBenBusinessName.TabIndex = 13
        '
        'lblTemBenPayName
        '
        Me.lblTemBenPayName.AutoSize = True
        Me.lblTemBenPayName.Location = New System.Drawing.Point(2, 36)
        Me.lblTemBenPayName.Name = "lblTemBenPayName"
        Me.lblTemBenPayName.Size = New System.Drawing.Size(85, 14)
        Me.lblTemBenPayName.TabIndex = 120
        Me.lblTemBenPayName.Text = "Business Name:"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.OldLace
        Me.TabPage2.Controls.Add(Me.lblFinancePay)
        Me.TabPage2.Controls.Add(Me.txtTemSubBankCtry)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.CboTemBenSubBankName)
        Me.TabPage2.Controls.Add(Me.txtTemSubBankIBAN)
        Me.TabPage2.Controls.Add(Me.txtTemSubBankSwift)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.txtTemBankCtry)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.CboTemBenBankName)
        Me.TabPage2.Controls.Add(Me.txtTemBankIBAN)
        Me.TabPage2.Controls.Add(Me.txtTemBankSwift)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Location = New System.Drawing.Point(4, 23)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(617, 169)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Bank Details"
        '
        'lblFinancePay
        '
        Me.lblFinancePay.AutoSize = True
        Me.lblFinancePay.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFinancePay.ForeColor = System.Drawing.Color.DarkRed
        Me.lblFinancePay.Location = New System.Drawing.Point(85, 16)
        Me.lblFinancePay.Name = "lblFinancePay"
        Me.lblFinancePay.Size = New System.Drawing.Size(536, 14)
        Me.lblFinancePay.TabIndex = 125
        Me.lblFinancePay.Text = "FINANCE TO MAKE THIS PAYMENT (Only authorise this to this mail when payment has b" &
    "een made)"
        Me.lblFinancePay.Visible = False
        '
        'txtTemSubBankCtry
        '
        Me.txtTemSubBankCtry.Enabled = False
        Me.txtTemSubBankCtry.Location = New System.Drawing.Point(85, 127)
        Me.txtTemSubBankCtry.Name = "txtTemSubBankCtry"
        Me.txtTemSubBankCtry.Size = New System.Drawing.Size(225, 20)
        Me.txtTemSubBankCtry.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 132)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 12)
        Me.Label2.TabIndex = 124
        Me.Label2.Text = "SubBank Ctry:"
        '
        'CboTemBenSubBankName
        '
        Me.CboTemBenSubBankName.Enabled = False
        Me.CboTemBenSubBankName.FormattingEnabled = True
        Me.CboTemBenSubBankName.Location = New System.Drawing.Point(85, 47)
        Me.CboTemBenSubBankName.Name = "CboTemBenSubBankName"
        Me.CboTemBenSubBankName.Size = New System.Drawing.Size(225, 22)
        Me.CboTemBenSubBankName.TabIndex = 22
        '
        'txtTemSubBankIBAN
        '
        Me.txtTemSubBankIBAN.Enabled = False
        Me.txtTemSubBankIBAN.Location = New System.Drawing.Point(85, 101)
        Me.txtTemSubBankIBAN.Name = "txtTemSubBankIBAN"
        Me.txtTemSubBankIBAN.Size = New System.Drawing.Size(225, 20)
        Me.txtTemSubBankIBAN.TabIndex = 24
        '
        'txtTemSubBankSwift
        '
        Me.txtTemSubBankSwift.Enabled = False
        Me.txtTemSubBankSwift.Location = New System.Drawing.Point(85, 75)
        Me.txtTemSubBankSwift.Name = "txtTemSubBankSwift"
        Me.txtTemSubBankSwift.Size = New System.Drawing.Size(225, 20)
        Me.txtTemSubBankSwift.TabIndex = 23
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 106)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(70, 12)
        Me.Label5.TabIndex = 120
        Me.Label5.Text = "SubBank IBAN:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(10, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 12)
        Me.Label6.TabIndex = 119
        Me.Label6.Text = "SubBank Swift:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 12)
        Me.Label8.TabIndex = 118
        Me.Label8.Text = "SubBank Name:"
        '
        'txtTemBankCtry
        '
        Me.txtTemBankCtry.Enabled = False
        Me.txtTemBankCtry.Location = New System.Drawing.Point(386, 127)
        Me.txtTemBankCtry.Name = "txtTemBankCtry"
        Me.txtTemBankCtry.Size = New System.Drawing.Size(225, 20)
        Me.txtTemBankCtry.TabIndex = 29
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(329, 132)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 12)
        Me.Label13.TabIndex = 116
        Me.Label13.Text = "Bank Ctry:"
        '
        'CboTemBenBankName
        '
        Me.CboTemBenBankName.Enabled = False
        Me.CboTemBenBankName.FormattingEnabled = True
        Me.CboTemBenBankName.Location = New System.Drawing.Point(386, 47)
        Me.CboTemBenBankName.Name = "CboTemBenBankName"
        Me.CboTemBenBankName.Size = New System.Drawing.Size(225, 22)
        Me.CboTemBenBankName.TabIndex = 26
        '
        'txtTemBankIBAN
        '
        Me.txtTemBankIBAN.Enabled = False
        Me.txtTemBankIBAN.Location = New System.Drawing.Point(386, 101)
        Me.txtTemBankIBAN.Name = "txtTemBankIBAN"
        Me.txtTemBankIBAN.Size = New System.Drawing.Size(225, 20)
        Me.txtTemBankIBAN.TabIndex = 28
        '
        'txtTemBankSwift
        '
        Me.txtTemBankSwift.Enabled = False
        Me.txtTemBankSwift.Location = New System.Drawing.Point(386, 75)
        Me.txtTemBankSwift.Name = "txtTemBankSwift"
        Me.txtTemBankSwift.Size = New System.Drawing.Size(225, 20)
        Me.txtTemBankSwift.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(326, 106)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(54, 12)
        Me.Label9.TabIndex = 112
        Me.Label9.Text = "Bank IBAN:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(327, 80)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 12)
        Me.Label11.TabIndex = 111
        Me.Label11.Text = "Bank Swift:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(323, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 12)
        Me.Label12.TabIndex = 110
        Me.Label12.Text = "Bank Name:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(10, 19)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(82, 14)
        Me.Label30.TabIndex = 121
        Me.Label30.Text = "Template Name:"
        '
        'TabBank
        '
        Me.TabBank.BackColor = System.Drawing.Color.OldLace
        Me.TabBank.Controls.Add(Me.GrpBankDetails)
        Me.TabBank.Controls.Add(Me.GrpLookup)
        Me.TabBank.Location = New System.Drawing.Point(4, 23)
        Me.TabBank.Name = "TabBank"
        Me.TabBank.Padding = New System.Windows.Forms.Padding(3)
        Me.TabBank.Size = New System.Drawing.Size(634, 243)
        Me.TabBank.TabIndex = 1
        Me.TabBank.Text = "Bank Lookup"
        '
        'GrpBankDetails
        '
        Me.GrpBankDetails.BackColor = System.Drawing.Color.Wheat
        Me.GrpBankDetails.Controls.Add(Me.cboLookBankCountry)
        Me.GrpBankDetails.Controls.Add(Me.Label31)
        Me.GrpBankDetails.Controls.Add(Me.Label16)
        Me.GrpBankDetails.Controls.Add(Me.txtLookBankName)
        Me.GrpBankDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrpBankDetails.ForeColor = System.Drawing.Color.Black
        Me.GrpBankDetails.Location = New System.Drawing.Point(6, 94)
        Me.GrpBankDetails.Name = "GrpBankDetails"
        Me.GrpBankDetails.Size = New System.Drawing.Size(622, 143)
        Me.GrpBankDetails.TabIndex = 90
        Me.GrpBankDetails.TabStop = False
        Me.GrpBankDetails.Text = "Please confirm and tick the following to continue payment request"
        '
        'cboLookBankCountry
        '
        Me.cboLookBankCountry.BackColor = System.Drawing.Color.White
        Me.cboLookBankCountry.FormattingEnabled = True
        Me.cboLookBankCountry.Location = New System.Drawing.Point(78, 46)
        Me.cboLookBankCountry.Name = "cboLookBankCountry"
        Me.cboLookBankCountry.Size = New System.Drawing.Size(343, 21)
        Me.cboLookBankCountry.TabIndex = 35
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(16, 49)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(56, 13)
        Me.Label31.TabIndex = 92
        Me.Label31.Text = "Bank Ctry:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 13)
        Me.Label16.TabIndex = 90
        Me.Label16.Text = "Bank Name:"
        '
        'txtLookBankName
        '
        Me.txtLookBankName.Location = New System.Drawing.Point(78, 19)
        Me.txtLookBankName.Name = "txtLookBankName"
        Me.txtLookBankName.Size = New System.Drawing.Size(534, 20)
        Me.txtLookBankName.TabIndex = 34
        '
        'GrpLookup
        '
        Me.GrpLookup.BackColor = System.Drawing.Color.OldLace
        Me.GrpLookup.Controls.Add(Me.cboLookSWIFT)
        Me.GrpLookup.Controls.Add(Me.cmdValidateBank)
        Me.GrpLookup.Controls.Add(Me.Label19)
        Me.GrpLookup.Controls.Add(Me.txtLookAccountNo)
        Me.GrpLookup.Controls.Add(Me.Label18)
        Me.GrpLookup.Controls.Add(Me.txtLookSortCode)
        Me.GrpLookup.Controls.Add(Me.Label17)
        Me.GrpLookup.Controls.Add(Me.Label15)
        Me.GrpLookup.Location = New System.Drawing.Point(6, 6)
        Me.GrpLookup.Name = "GrpLookup"
        Me.GrpLookup.Size = New System.Drawing.Size(622, 82)
        Me.GrpLookup.TabIndex = 89
        Me.GrpLookup.TabStop = False
        Me.GrpLookup.Text = "Lookup"
        '
        'cboLookSWIFT
        '
        Me.cboLookSWIFT.FormattingEnabled = True
        Me.cboLookSWIFT.Location = New System.Drawing.Point(10, 37)
        Me.cboLookSWIFT.Name = "cboLookSWIFT"
        Me.cboLookSWIFT.Size = New System.Drawing.Size(131, 22)
        Me.cboLookSWIFT.TabIndex = 30
        '
        'cmdValidateBank
        '
        Me.cmdValidateBank.Location = New System.Drawing.Point(429, 37)
        Me.cmdValidateBank.Name = "cmdValidateBank"
        Me.cmdValidateBank.Size = New System.Drawing.Size(184, 23)
        Me.cmdValidateBank.TabIndex = 33
        Me.cmdValidateBank.Text = "Validate Bank"
        Me.cmdValidateBank.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(301, 23)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(104, 14)
        Me.Label19.TabIndex = 95
        Me.Label19.Text = "UK Account Number"
        '
        'txtLookAccountNo
        '
        Me.txtLookAccountNo.Location = New System.Drawing.Point(290, 39)
        Me.txtLookAccountNo.Name = "txtLookAccountNo"
        Me.txtLookAccountNo.Size = New System.Drawing.Size(124, 20)
        Me.txtLookAccountNo.TabIndex = 32
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(198, 23)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 14)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "UK Sort Code"
        '
        'txtLookSortCode
        '
        Me.txtLookSortCode.Location = New System.Drawing.Point(199, 39)
        Me.txtLookSortCode.Name = "txtLookSortCode"
        Me.txtLookSortCode.Size = New System.Drawing.Size(85, 20)
        Me.txtLookSortCode.TabIndex = 31
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(147, 33)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 26)
        Me.Label17.TabIndex = 91
        Me.Label17.Text = "OR"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(50, 23)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 14)
        Me.Label15.TabIndex = 90
        Me.Label15.Text = "Swift Code:"
        '
        'TabKYC
        '
        Me.TabKYC.BackColor = System.Drawing.Color.OldLace
        Me.TabKYC.Controls.Add(Me.GrpKYC)
        Me.TabKYC.Location = New System.Drawing.Point(4, 23)
        Me.TabKYC.Name = "TabKYC"
        Me.TabKYC.Padding = New System.Windows.Forms.Padding(3)
        Me.TabKYC.Size = New System.Drawing.Size(634, 243)
        Me.TabKYC.TabIndex = 3
        Me.TabKYC.Text = "KYC"
        '
        'GrpKYC
        '
        Me.GrpKYC.BackColor = System.Drawing.Color.Wheat
        Me.GrpKYC.Controls.Add(Me.ChkCallback)
        Me.GrpKYC.Controls.Add(Me.cmdBrowse)
        Me.GrpKYC.Controls.Add(Me.txtFilePath)
        Me.GrpKYC.Controls.Add(Me.Label34)
        Me.GrpKYC.Controls.Add(Me.Label3)
        Me.GrpKYC.Controls.Add(Me.txtKYCIssuesDisclosed)
        Me.GrpKYC.Controls.Add(Me.ChkKYCCDD)
        Me.GrpKYC.Controls.Add(Me.ChkKYCPEP)
        Me.GrpKYC.Controls.Add(Me.ChkKYCsanctions)
        Me.GrpKYC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrpKYC.ForeColor = System.Drawing.Color.Black
        Me.GrpKYC.Location = New System.Drawing.Point(6, 6)
        Me.GrpKYC.Name = "GrpKYC"
        Me.GrpKYC.Size = New System.Drawing.Size(622, 231)
        Me.GrpKYC.TabIndex = 91
        Me.GrpKYC.TabStop = False
        Me.GrpKYC.Text = "Please confirm and tick the following to continue payment request"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.Location = New System.Drawing.Point(564, 17)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(52, 23)
        Me.cmdBrowse.TabIndex = 167
        Me.cmdBrowse.Text = "Browse"
        Me.cmdBrowse.UseVisualStyleBackColor = True
        '
        'txtFilePath
        '
        Me.txtFilePath.Location = New System.Drawing.Point(66, 19)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.Size = New System.Drawing.Size(492, 20)
        Me.txtFilePath.TabIndex = 165
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(9, 22)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(51, 13)
        Me.Label34.TabIndex = 166
        Me.Label34.Text = "File Path:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(430, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(183, 13)
        Me.Label3.TabIndex = 90
        Me.Label3.Text = "Issues Disclosed/Payment Rationale:"
        '
        'txtKYCIssuesDisclosed
        '
        Me.txtKYCIssuesDisclosed.Location = New System.Drawing.Point(14, 144)
        Me.txtKYCIssuesDisclosed.Multiline = True
        Me.txtKYCIssuesDisclosed.Name = "txtKYCIssuesDisclosed"
        Me.txtKYCIssuesDisclosed.Size = New System.Drawing.Size(599, 81)
        Me.txtKYCIssuesDisclosed.TabIndex = 39
        '
        'ChkKYCCDD
        '
        Me.ChkKYCCDD.AutoSize = True
        Me.ChkKYCCDD.Location = New System.Drawing.Point(12, 94)
        Me.ChkKYCCDD.Name = "ChkKYCCDD"
        Me.ChkKYCCDD.Size = New System.Drawing.Size(353, 17)
        Me.ChkKYCCDD.TabIndex = 38
        Me.ChkKYCCDD.Text = "Confirmed CDD Customer Due Diligence checks has been completed"
        Me.ChkKYCCDD.UseVisualStyleBackColor = True
        '
        'ChkKYCPEP
        '
        Me.ChkKYCPEP.AutoSize = True
        Me.ChkKYCPEP.Location = New System.Drawing.Point(12, 71)
        Me.ChkKYCPEP.Name = "ChkKYCPEP"
        Me.ChkKYCPEP.Size = New System.Drawing.Size(361, 17)
        Me.ChkKYCPEP.TabIndex = 37
        Me.ChkKYCPEP.Text = "Confirmed PEP (Politically Exposed Person check) has been completed"
        Me.ChkKYCPEP.UseVisualStyleBackColor = True
        '
        'ChkKYCsanctions
        '
        Me.ChkKYCsanctions.AutoSize = True
        Me.ChkKYCsanctions.Location = New System.Drawing.Point(12, 48)
        Me.ChkKYCsanctions.Name = "ChkKYCsanctions"
        Me.ChkKYCsanctions.Size = New System.Drawing.Size(390, 17)
        Me.ChkKYCsanctions.TabIndex = 36
        Me.ChkKYCsanctions.Text = "Confirmed payee and country of payee does not have sanctions against them"
        Me.ChkKYCsanctions.UseVisualStyleBackColor = True
        '
        'TabSearch
        '
        Me.TabSearch.Controls.Add(Me.GrpSearch)
        Me.TabSearch.Location = New System.Drawing.Point(4, 23)
        Me.TabSearch.Name = "TabSearch"
        Me.TabSearch.Padding = New System.Windows.Forms.Padding(3)
        Me.TabSearch.Size = New System.Drawing.Size(634, 243)
        Me.TabSearch.TabIndex = 4
        Me.TabSearch.Text = "Search"
        Me.TabSearch.UseVisualStyleBackColor = True
        '
        'GrpSearch
        '
        Me.GrpSearch.BackColor = System.Drawing.Color.Wheat
        Me.GrpSearch.Controls.Add(Me.cmdPRSearch2)
        Me.GrpSearch.Controls.Add(Me.Label33)
        Me.GrpSearch.Controls.Add(Me.Label4)
        Me.GrpSearch.Controls.Add(Me.txtPRGoogleSearch2)
        Me.GrpSearch.Controls.Add(Me.cmdPRSearch)
        Me.GrpSearch.Controls.Add(Me.txtPRGoogleSearch)
        Me.GrpSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GrpSearch.ForeColor = System.Drawing.Color.Black
        Me.GrpSearch.Location = New System.Drawing.Point(6, 6)
        Me.GrpSearch.Name = "GrpSearch"
        Me.GrpSearch.Size = New System.Drawing.Size(622, 231)
        Me.GrpSearch.TabIndex = 92
        Me.GrpSearch.TabStop = False
        Me.GrpSearch.Text = "Search"
        '
        'cmdPRSearch2
        '
        Me.cmdPRSearch2.Location = New System.Drawing.Point(531, 145)
        Me.cmdPRSearch2.Name = "cmdPRSearch2"
        Me.cmdPRSearch2.Size = New System.Drawing.Size(85, 23)
        Me.cmdPRSearch2.TabIndex = 177
        Me.cmdPRSearch2.Text = "Search OC"
        Me.cmdPRSearch2.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(9, 131)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(132, 13)
        Me.Label33.TabIndex = 176
        Me.Label33.Text = "Search Type (Other Crime)"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(148, 13)
        Me.Label4.TabIndex = 175
        Me.Label4.Text = "Search Type (Financial Crime)"
        '
        'txtPRGoogleSearch2
        '
        Me.txtPRGoogleSearch2.Location = New System.Drawing.Point(12, 147)
        Me.txtPRGoogleSearch2.Multiline = True
        Me.txtPRGoogleSearch2.Name = "txtPRGoogleSearch2"
        Me.txtPRGoogleSearch2.Size = New System.Drawing.Size(513, 78)
        Me.txtPRGoogleSearch2.TabIndex = 41
        '
        'cmdPRSearch
        '
        Me.cmdPRSearch.Location = New System.Drawing.Point(531, 41)
        Me.cmdPRSearch.Name = "cmdPRSearch"
        Me.cmdPRSearch.Size = New System.Drawing.Size(85, 23)
        Me.cmdPRSearch.TabIndex = 40
        Me.cmdPRSearch.Text = "Search FC"
        Me.cmdPRSearch.UseVisualStyleBackColor = True
        '
        'txtPRGoogleSearch
        '
        Me.txtPRGoogleSearch.Location = New System.Drawing.Point(12, 43)
        Me.txtPRGoogleSearch.Multiline = True
        Me.txtPRGoogleSearch.Name = "txtPRGoogleSearch"
        Me.txtPRGoogleSearch.Size = New System.Drawing.Size(513, 77)
        Me.txtPRGoogleSearch.TabIndex = 39
        '
        'CmdPRSend
        '
        Me.CmdPRSend.Location = New System.Drawing.Point(473, 804)
        Me.CmdPRSend.Name = "CmdPRSend"
        Me.CmdPRSend.Size = New System.Drawing.Size(189, 23)
        Me.CmdPRSend.TabIndex = 43
        Me.CmdPRSend.Text = "Send Payment Request"
        Me.CmdPRSend.UseVisualStyleBackColor = True
        '
        'GrpPREmail
        '
        Me.GrpPREmail.Controls.Add(Me.txtEmail)
        Me.GrpPREmail.Location = New System.Drawing.Point(7, 547)
        Me.GrpPREmail.Name = "GrpPREmail"
        Me.GrpPREmail.Size = New System.Drawing.Size(655, 98)
        Me.GrpPREmail.TabIndex = 8
        Me.GrpPREmail.TabStop = False
        Me.GrpPREmail.Text = "Email Request"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(6, 19)
        Me.txtEmail.Multiline = True
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtEmail.Size = New System.Drawing.Size(642, 69)
        Me.txtEmail.TabIndex = 40
        '
        'ContextMenuStripFiles
        '
        Me.ContextMenuStripFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenFileToolStripMenuItem, Me.RemoveFileToolStripMenuItem, Me.RefreshToolStripMenuItem})
        Me.ContextMenuStripFiles.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles.Size = New System.Drawing.Size(139, 70)
        '
        'OpenFileToolStripMenuItem
        '
        Me.OpenFileToolStripMenuItem.Name = "OpenFileToolStripMenuItem"
        Me.OpenFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.OpenFileToolStripMenuItem.Text = "&Open File"
        '
        'RemoveFileToolStripMenuItem
        '
        Me.RemoveFileToolStripMenuItem.Name = "RemoveFileToolStripMenuItem"
        Me.RemoveFileToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RemoveFileToolStripMenuItem.Text = "&Remove File"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ContextMenuStripFiles1
        '
        Me.ContextMenuStripFiles1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.ToolStripMenuItem3})
        Me.ContextMenuStripFiles1.Name = "ContextMenuStripFiles"
        Me.ContextMenuStripFiles1.Size = New System.Drawing.Size(139, 70)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem1.Text = "&Open File"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem2.Text = "&Remove File"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(138, 22)
        Me.ToolStripMenuItem3.Text = "Refresh"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ChkCallback
        '
        Me.ChkCallback.AutoSize = True
        Me.ChkCallback.Location = New System.Drawing.Point(12, 117)
        Me.ChkCallback.Name = "ChkCallback"
        Me.ChkCallback.Size = New System.Drawing.Size(369, 17)
        Me.ChkCallback.TabIndex = 168
        Me.ChkCallback.Text = "RM call back to confirm new payment details (template is not being used)"
        Me.ChkCallback.UseVisualStyleBackColor = True
        '
        'FrmPaymentRequest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(695, 875)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmPaymentRequest"
        Me.Text = "Payment Request"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GrpPRCriteria.ResumeLayout(False)
        Me.GrpPRCriteria.PerformLayout()
        Me.TabRequest.ResumeLayout(False)
        Me.TabRequestPayment.ResumeLayout(False)
        Me.TabRequestPayment.PerformLayout()
        Me.TabRequestMM.ResumeLayout(False)
        Me.TabRequestMM.PerformLayout()
        Me.TabRequestFOP.ResumeLayout(False)
        Me.TabRequestFOP.PerformLayout()
        CType(Me.dgvFOP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPayment.ResumeLayout(False)
        Me.TabBeneficiary.ResumeLayout(False)
        Me.TabBeneficiary.PerformLayout()
        Me.TabBeneficiaryDetails.ResumeLayout(False)
        Me.TabMain.ResumeLayout(False)
        Me.TabMain.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabBank.ResumeLayout(False)
        Me.GrpBankDetails.ResumeLayout(False)
        Me.GrpBankDetails.PerformLayout()
        Me.GrpLookup.ResumeLayout(False)
        Me.GrpLookup.PerformLayout()
        Me.TabKYC.ResumeLayout(False)
        Me.GrpKYC.ResumeLayout(False)
        Me.GrpKYC.PerformLayout()
        Me.TabSearch.ResumeLayout(False)
        Me.GrpSearch.ResumeLayout(False)
        Me.GrpSearch.PerformLayout()
        Me.GrpPREmail.ResumeLayout(False)
        Me.GrpPREmail.PerformLayout()
        Me.ContextMenuStripFiles.ResumeLayout(False)
        Me.ContextMenuStripFiles1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GrpPREmail As GroupBox
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents CmdPRSend As Button
    Friend WithEvents GrpPRCriteria As GroupBox
    Friend WithEvents ContextMenuStripFiles As ContextMenuStrip
    Friend WithEvents OpenFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoveFileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TabPayment As TabControl
    Friend WithEvents TabBank As TabPage
    Friend WithEvents GrpBankDetails As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents txtLookBankName As TextBox
    Friend WithEvents GrpLookup As GroupBox
    Friend WithEvents cmdValidateBank As Button
    Friend WithEvents Label19 As Label
    Friend WithEvents txtLookAccountNo As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents txtLookSortCode As TextBox
    Friend WithEvents Label17 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents TabBeneficiary As TabPage
    Friend WithEvents cboTemFindName As ComboBox
    Friend WithEvents TabBeneficiaryDetails As TabControl
    Friend WithEvents TabMain As TabPage
    Friend WithEvents cbobenrelationship As ComboBox
    Friend WithEvents Label29 As Label
    Friend WithEvents txtbensurname As TextBox
    Friend WithEvents Label28 As Label
    Friend WithEvents txtbenmiddlename As TextBox
    Friend WithEvents Label27 As Label
    Friend WithEvents txtbenfirstname As TextBox
    Friend WithEvents Label26 As Label
    Friend WithEvents cboBenCountry As ComboBox
    Friend WithEvents Label25 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents txtbenpostcode As TextBox
    Friend WithEvents Label23 As Label
    Friend WithEvents txtbencounty As TextBox
    Friend WithEvents Label22 As Label
    Friend WithEvents txtbencity As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents cboBenType As ComboBox
    Friend WithEvents Label20 As Label
    Friend WithEvents txtbenaddress1 As TextBox
    Friend WithEvents txtBenBusinessName As TextBox
    Friend WithEvents lblTemBenPayName As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents txtTemSubBankCtry As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents CboTemBenSubBankName As ComboBox
    Friend WithEvents txtTemSubBankIBAN As TextBox
    Friend WithEvents txtTemSubBankSwift As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtTemBankCtry As TextBox
    Friend WithEvents Label13 As Label
    Friend WithEvents CboTemBenBankName As ComboBox
    Friend WithEvents txtTemBankIBAN As TextBox
    Friend WithEvents txtTemBankSwift As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents cboLookBankCountry As ComboBox
    Friend WithEvents cboLookSWIFT As ComboBox
    Friend WithEvents TabKYC As TabPage
    Friend WithEvents GrpKYC As GroupBox
    Friend WithEvents ChkKYCCDD As CheckBox
    Friend WithEvents ChkKYCPEP As CheckBox
    Friend WithEvents ChkKYCsanctions As CheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtKYCIssuesDisclosed As TextBox
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents LVFiles1 As ListView
    Friend WithEvents ContextMenuStripFiles1 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents cboPayType As ComboBox
    Friend WithEvents Label14 As Label
    Friend WithEvents TabSearch As TabPage
    Friend WithEvents GrpSearch As GroupBox
    Friend WithEvents txtPRGoogleSearch As TextBox
    Friend WithEvents cmdPRSearch As Button
    Friend WithEvents cmdPRSearch2 As Button
    Friend WithEvents Label33 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtPRGoogleSearch2 As TextBox
    Friend WithEvents cmdBrowse As Button
    Friend WithEvents txtFilePath As TextBox
    Friend WithEvents Label34 As Label
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents lblFinancePay As Label
    Friend WithEvents TabRequest As TabControl
    Friend WithEvents TabRequestPayment As TabPage
    Friend WithEvents TabRequestMM As TabPage
    Friend WithEvents LblNewBalanceCCY As Label
    Friend WithEvents lblFeeBalanceCCY As Label
    Friend WithEvents lblPostBalanceCCY As Label
    Friend WithEvents lblBalanceCCY As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents cboFeeType As ComboBox
    Friend WithEvents lblFeeBalance As Label
    Friend WithEvents lblFeeBalancetitle As Label
    Friend WithEvents lblPostBalance As Label
    Friend WithEvents lblPostBalancetitle As Label
    Friend WithEvents txtRef As TextBox
    Friend WithEvents lblRef As Label
    Friend WithEvents txtPRRate As TextBox
    Friend WithEvents lblTemBenPayPortfolio As Label
    Friend WithEvents cboTemBenPayPortfolio As ComboBox
    Friend WithEvents lblNewBalance As Label
    Friend WithEvents lblNewBalancetitle As Label
    Friend WithEvents txtPRAmountFee As TextBox
    Friend WithEvents lblPRAmountFee As Label
    Friend WithEvents cboTemFeeCCY As ComboBox
    Friend WithEvents lblTemFeeCCY As Label
    Friend WithEvents lblBalance As Label
    Friend WithEvents lblBalancetitle As Label
    Friend WithEvents txtPRAmount As TextBox
    Friend WithEvents lblPRAmount As Label
    Friend WithEvents cboTemFindCCY As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents cboTemFindPortfolio As ComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents LblMMNewBalanceCCY As Label
    Friend WithEvents lblMMInstrumentBalanceCCY As Label
    Friend WithEvents lblMMPostBalanceCCY As Label
    Friend WithEvents lblMMBalanceCCY As Label
    Friend WithEvents lblMMHeldType As Label
    Friend WithEvents cboMMHeldType As ComboBox
    Friend WithEvents lblMMInstrumentBalance As Label
    Friend WithEvents lblMMInstrumentBalancetitle As Label
    Friend WithEvents lblMMPostBalance As Label
    Friend WithEvents lblMMPostBalancetitle As Label
    Friend WithEvents lblMMInstrument As Label
    Friend WithEvents cboMMInstrument As ComboBox
    Friend WithEvents lblMMNewBalance As Label
    Friend WithEvents lblMMNewBalancetitle As Label
    Friend WithEvents lblMMBalance As Label
    Friend WithEvents lblMMBalancetitle As Label
    Friend WithEvents txtMMAmount As TextBox
    Friend WithEvents lblMMAmount As Label
    Friend WithEvents cboMMCCY As ComboBox
    Friend WithEvents Label52 As Label
    Friend WithEvents cboMMPortfolio As ComboBox
    Friend WithEvents Label53 As Label
    Friend WithEvents lblHeldEndDate As Label
    Friend WithEvents dtHeldEndDate As DateTimePicker
    Friend WithEvents lblHeldStartDate As Label
    Friend WithEvents dtHeldStartDate As DateTimePicker
    Friend WithEvents Label35 As Label
    Friend WithEvents cboMMBuySell As ComboBox
    Friend WithEvents TabRequestFOP As TabPage
    Friend WithEvents cboFOPPortfolio As ComboBox
    Friend WithEvents Label40 As Label
    Friend WithEvents dgvFOP As DataGridView
    Friend WithEvents CmdFilePayment As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents CmdBrowseFiles As Button
    Friend WithEvents txtPaymentFilesPath As TextBox
    Friend WithEvents ChkCallback As CheckBox
End Class
