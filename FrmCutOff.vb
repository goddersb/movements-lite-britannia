﻿Public Class FrmCutOff
    Dim CutoffConn As New SqlClient.SqlConnection
    Dim dgvdata As New SqlClient.SqlDataAdapter
    Dim dgvdataTimes As New SqlClient.SqlDataAdapter
    Dim ds As New DataSet
    Dim cboColTime As New DataGridViewComboBoxColumn
    Dim cboColTimeZone As New DataGridViewComboBoxColumn

    Private Sub CufOffBrokerLoadGrid(Optional ByVal varFilter As String = "")
        Dim varSQL As String = "", varSQLTimes As String = "", varSQLTimeZone As String = ""

        Try
            Cursor = Cursors.WaitCursor
            If varFilter = "" Then
                varSQL = "select * from vwDolfinPaymentCutOffTimesLinks order by b_Name1, CR_Name1"
            Else
                varSQL = "select * from vwDolfinPaymentCutOffTimesLinks where b_Code in (" & varFilter & ") order by b_Name1, CR_Name1"
            End If

            dgvBrokerCutOff.DataSource = Nothing
            CutoffConn = ClsIMS.GetNewOpenConnection

            dgvdata = New SqlClient.SqlDataAdapter(varSQL, CutoffConn)
            ds = New DataSet
            dgvdata.Fill(ds, "vwDolfinPaymentCutOffTimesLinks")
            dgvBrokerCutOff.DataSource = ds.Tables("vwDolfinPaymentCutOffTimesLinks")

            Dim varStartTime As Date = "01/01/2001 00:00"
            Dim varNextTime As Date = varStartTime

            varSQLTimes = "Select '" & varNextTime.ToString("HH:mm") & "' union "
            For i = 1 To (24 * 4) - 1
                varNextTime = DateAdd(DateInterval.Minute, 15, varNextTime)
                varSQLTimes = varSQLTimes & "Select '" & varNextTime.ToString("HH:mm") & "' union "
            Next

            varSQLTimes = Strings.Left(varSQLTimes, Len(varSQLTimes) - 7)

            dgvdataTimes = New SqlClient.SqlDataAdapter(varSQLTimes & " as CO_CutOffTime", CutoffConn)
            dgvdataTimes.Fill(ds, "CutOffTime")
            cboColTime = New DataGridViewComboBoxColumn
            cboColTime.HeaderText = "CutOffTime"
            cboColTime.DataPropertyName = "CO_CutOffTime"
            cboColTime.DataSource = ds.Tables("CutOffTime")
            cboColTime.DisplayMember = ds.Tables("CutOffTime").Columns(0).ColumnName
            cboColTime.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboColTime.FlatStyle = FlatStyle.Flat

            dgvBrokerCutOff.Columns.RemoveAt(4)
            dgvBrokerCutOff.Columns.Insert(4, cboColTime)

            varSQLTimeZone = "Select 'Central European Standard Time' union Select 'GMT Standard Time' union Select 'Eastern Standard Time' as CO_CutOffTimeZone"
            dgvdataTimes = New SqlClient.SqlDataAdapter(varSQLTimeZone, CutoffConn)
            dgvdataTimes.Fill(ds, "CutOffTimeZone")
            cboColTimeZone = New DataGridViewComboBoxColumn
            cboColTimeZone.HeaderText = "CutOffTimeZone"
            cboColTimeZone.DataPropertyName = "CO_CutOffTimeZone"
            cboColTimeZone.DataSource = ds.Tables("CutOffTimeZone")
            cboColTimeZone.DisplayMember = ds.Tables("CutOffTimeZone").Columns(0).ColumnName
            cboColTimeZone.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            cboColTimeZone.FlatStyle = FlatStyle.Flat

            dgvBrokerCutOff.Columns.RemoveAt(5)
            dgvBrokerCutOff.Columns.Insert(5, cboColTimeZone)

            dgvBrokerCutOff.Columns(0).Visible = False
            dgvBrokerCutOff.Columns(1).HeaderText = "Broker"
            dgvBrokerCutOff.Columns(1).Width = 400
            dgvBrokerCutOff.Columns(1).ReadOnly = True
            dgvBrokerCutOff.Columns(2).Visible = False
            dgvBrokerCutOff.Columns(3).HeaderText = "CCY"
            dgvBrokerCutOff.Columns(3).ReadOnly = True
            dgvBrokerCutOff.Columns(4).HeaderText = "Cut Off Time"
            dgvBrokerCutOff.Columns(5).HeaderText = "Cut Off Time Zone"
            dgvBrokerCutOff.Columns(5).Width = 200
            dgvBrokerCutOff.Columns(6).Visible = False
            dgvBrokerCutOff.Columns(7).Visible = False
            dgvBrokerCutOff.Columns(8).HeaderText = "Modified By"
            dgvBrokerCutOff.Columns(8).ReadOnly = True
            dgvBrokerCutOff.Columns(9).HeaderText = "Modified Time"
            dgvBrokerCutOff.Columns(9).ReadOnly = True

            dgvBrokerCutOff.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells
            dgvBrokerCutOff.Refresh()

            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            dgvBrokerCutOff.DataSource = Nothing
            ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCutOff, Err.Description & " - Problem with viewing cut off grid")
        End Try
    End Sub

    Private Sub FrmCutOff_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        CufOffBrokerLoadGrid()
    End Sub

    Private Sub FrmCutOff_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        Try
            If CutoffConn.State = ConnectionState.Open Then
                CutoffConn.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FrmCutOff_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ClsIMS.PopulateFormCombo(cboCutOffBroker)
    End Sub

    Private Sub cboCutOffBroker_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCutOffBroker.SelectedIndexChanged
        If IsNumeric(cboCutOffBroker.SelectedValue) Then
            CufOffBrokerLoadGrid(cboCutOffBroker.SelectedValue.ToString)
        End If
    End Sub

    Private Sub dgvBrokerCutOff_RowLeave(sender As Object, e As DataGridViewCellEventArgs) Handles dgvBrokerCutOff.RowLeave
        Dim varID As Integer, varCCYCode As Integer

        If dgvBrokerCutOff.IsCurrentRowDirty And dgvBrokerCutOff.EndEdit Then
            Try
                If IsDate(dgvBrokerCutOff.Rows(e.RowIndex).Cells(6).Value) And Not IsDBNull(dgvBrokerCutOff.Rows(e.RowIndex).Cells(7).Value) Then
                    varID = dgvBrokerCutOff.Rows(e.RowIndex).Cells(0).Value
                    varCCYCode = dgvBrokerCutOff.Rows(e.RowIndex).Cells(2).Value
                    Dim CheckID As String = ClsIMS.GetSQLItem("select CO_BrokerCode from vwDolfinPaymentCutOffTimes where CO_BrokerCode = " & varID & " and CO_CCYCode = " & varCCYCode)
                    If CheckID = "" Then
                        ClsIMS.ExecuteString(CutoffConn, "insert into vwDolfinPaymentCutOffTimes(CO_BrokerCode,CO_CCYCode) values (" & varID & "," & varCCYCode & ")")
                    End If

                    Dim varSQL As String = "update vwDolfinPaymentCutOffTimes set CO_BrokerCode = " & varID & ", CO_CCYCode = " & varCCYCode
                    Dim CutOffTime As String = dgvBrokerCutOff.Rows(e.RowIndex).Cells(6).Value
                    Dim CutOffTimeZone As String = dgvBrokerCutOff.Rows(e.RowIndex).Cells(7).Value
                    varSQL = varSQL & ", CO_CutOffTime = '" & CutOffTime & "', CO_CutOffTimeZone = '" & CutOffTimeZone & "',CO_UserModifiedBy = '" & ClsIMS.FullUserName & "',CO_UserModifiedTime = getdate() where CO_BrokerCode = " & varID & " and CO_CCYCode = " & varCCYCode
                    ClsIMS.ExecuteString(CutoffConn, varSQL)

                    ClsIMS.AddAudit(cAuditStatusSuccess, cAuditGroupNameCutOff, varSQL)
                End If
            Catch ex As Exception
                ClsIMS.AddAudit(cAuditStatusError, cAuditGroupNameCutOff, Err.Description & " - Problem with updating Cutoff times ID: " & varID)
            End Try
        End If
    End Sub

    Private Sub CmdRefreshView_Click(sender As Object, e As EventArgs) Handles CmdRefreshView.Click
        CufOffBrokerLoadGrid()
        cboCutOffBroker.Text = ""
    End Sub

    Private Sub CmdExportToExcel_Click(sender As Object, e As EventArgs) Handles CmdExportToExcel.Click
        Cursor = Cursors.WaitCursor
        ExportGridToExcelFromForm(dgvBrokerCutOff, 0)
        Cursor = Cursors.Default
    End Sub
End Class