﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmCorpAction
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCorpAction))
        Me.TabCA = New System.Windows.Forms.TabControl()
        Me.TabVoluntary = New System.Windows.Forms.TabPage()
        Me.dgvCAVoluntary = New System.Windows.Forms.DataGridView()
        Me.TabMandatory = New System.Windows.Forms.TabPage()
        Me.dgvCAMandatory = New System.Windows.Forms.DataGridView()
        Me.TabPayment = New System.Windows.Forms.TabPage()
        Me.dgvCAPayment = New System.Windows.Forms.DataGridView()
        Me.TabBlacklist = New System.Windows.Forms.TabPage()
        Me.dgvCABlacklist = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.ChkArchive = New System.Windows.Forms.CheckBox()
        Me.CmdExportToExcel = New System.Windows.Forms.Button()
        Me.cboCAPayID = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboCAStatus = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CmdRefreshView = New System.Windows.Forms.Button()
        Me.dtCADate = New System.Windows.Forms.DateTimePicker()
        Me.cboCADate = New System.Windows.Forms.ComboBox()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboCAName = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboCACustodian = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboCAEvent = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboCAISIN = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboCAID = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblfeeTitle = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.TabCA.SuspendLayout()
        Me.TabVoluntary.SuspendLayout()
        CType(Me.dgvCAVoluntary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabMandatory.SuspendLayout()
        CType(Me.dgvCAMandatory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPayment.SuspendLayout()
        CType(Me.dgvCAPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabBlacklist.SuspendLayout()
        CType(Me.dgvCABlacklist, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabCA
        '
        Me.TabCA.Controls.Add(Me.TabVoluntary)
        Me.TabCA.Controls.Add(Me.TabMandatory)
        Me.TabCA.Controls.Add(Me.TabPayment)
        Me.TabCA.Controls.Add(Me.TabBlacklist)
        Me.TabCA.Location = New System.Drawing.Point(16, 138)
        Me.TabCA.Name = "TabCA"
        Me.TabCA.SelectedIndex = 0
        Me.TabCA.Size = New System.Drawing.Size(1556, 723)
        Me.TabCA.TabIndex = 41
        '
        'TabVoluntary
        '
        Me.TabVoluntary.Controls.Add(Me.dgvCAVoluntary)
        Me.TabVoluntary.Location = New System.Drawing.Point(4, 22)
        Me.TabVoluntary.Name = "TabVoluntary"
        Me.TabVoluntary.Padding = New System.Windows.Forms.Padding(3)
        Me.TabVoluntary.Size = New System.Drawing.Size(1548, 697)
        Me.TabVoluntary.TabIndex = 0
        Me.TabVoluntary.Text = "Voluntary"
        Me.TabVoluntary.UseVisualStyleBackColor = True
        '
        'dgvCAVoluntary
        '
        Me.dgvCAVoluntary.AllowUserToAddRows = False
        Me.dgvCAVoluntary.AllowUserToDeleteRows = False
        Me.dgvCAVoluntary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCAVoluntary.Location = New System.Drawing.Point(3, 3)
        Me.dgvCAVoluntary.MultiSelect = False
        Me.dgvCAVoluntary.Name = "dgvCAVoluntary"
        Me.dgvCAVoluntary.ReadOnly = True
        Me.dgvCAVoluntary.Size = New System.Drawing.Size(1535, 685)
        Me.dgvCAVoluntary.TabIndex = 38
        '
        'TabMandatory
        '
        Me.TabMandatory.Controls.Add(Me.dgvCAMandatory)
        Me.TabMandatory.Location = New System.Drawing.Point(4, 22)
        Me.TabMandatory.Name = "TabMandatory"
        Me.TabMandatory.Padding = New System.Windows.Forms.Padding(3)
        Me.TabMandatory.Size = New System.Drawing.Size(1548, 697)
        Me.TabMandatory.TabIndex = 1
        Me.TabMandatory.Text = "Mandatory"
        Me.TabMandatory.UseVisualStyleBackColor = True
        '
        'dgvCAMandatory
        '
        Me.dgvCAMandatory.AllowUserToAddRows = False
        Me.dgvCAMandatory.AllowUserToDeleteRows = False
        Me.dgvCAMandatory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCAMandatory.Location = New System.Drawing.Point(3, 3)
        Me.dgvCAMandatory.MultiSelect = False
        Me.dgvCAMandatory.Name = "dgvCAMandatory"
        Me.dgvCAMandatory.ReadOnly = True
        Me.dgvCAMandatory.Size = New System.Drawing.Size(1535, 685)
        Me.dgvCAMandatory.TabIndex = 39
        '
        'TabPayment
        '
        Me.TabPayment.Controls.Add(Me.dgvCAPayment)
        Me.TabPayment.Location = New System.Drawing.Point(4, 22)
        Me.TabPayment.Name = "TabPayment"
        Me.TabPayment.Size = New System.Drawing.Size(1548, 697)
        Me.TabPayment.TabIndex = 2
        Me.TabPayment.Text = "Payment"
        Me.TabPayment.UseVisualStyleBackColor = True
        '
        'dgvCAPayment
        '
        Me.dgvCAPayment.AllowUserToAddRows = False
        Me.dgvCAPayment.AllowUserToDeleteRows = False
        Me.dgvCAPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCAPayment.Location = New System.Drawing.Point(3, 3)
        Me.dgvCAPayment.MultiSelect = False
        Me.dgvCAPayment.Name = "dgvCAPayment"
        Me.dgvCAPayment.ReadOnly = True
        Me.dgvCAPayment.Size = New System.Drawing.Size(1535, 685)
        Me.dgvCAPayment.TabIndex = 39
        '
        'TabBlacklist
        '
        Me.TabBlacklist.Controls.Add(Me.dgvCABlacklist)
        Me.TabBlacklist.Location = New System.Drawing.Point(4, 22)
        Me.TabBlacklist.Name = "TabBlacklist"
        Me.TabBlacklist.Size = New System.Drawing.Size(1548, 697)
        Me.TabBlacklist.TabIndex = 3
        Me.TabBlacklist.Text = "Blacklist"
        Me.TabBlacklist.UseVisualStyleBackColor = True
        '
        'dgvCABlacklist
        '
        Me.dgvCABlacklist.AllowUserToOrderColumns = True
        Me.dgvCABlacklist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCABlacklist.Location = New System.Drawing.Point(7, 6)
        Me.dgvCABlacklist.MultiSelect = False
        Me.dgvCABlacklist.Name = "dgvCABlacklist"
        Me.dgvCABlacklist.Size = New System.Drawing.Size(1535, 685)
        Me.dgvCABlacklist.TabIndex = 40
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Location = New System.Drawing.Point(1301, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(262, 103)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Legend"
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.MistyRose
        Me.TextBox4.Location = New System.Drawing.Point(7, 15)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(250, 20)
        Me.TextBox4.TabIndex = 3
        Me.TextBox4.Text = "Check Action Details"
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.MediumAquamarine
        Me.TextBox3.Location = New System.Drawing.Point(7, 79)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(250, 20)
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Text = "Check ex-date with custodian"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.PaleTurquoise
        Me.TextBox2.Location = New System.Drawing.Point(7, 58)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(250, 20)
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Text = "Check deadline date with custodian"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.Wheat
        Me.TextBox1.Location = New System.Drawing.Point(7, 37)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(250, 20)
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "Changes from previous SWIFT"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ChkArchive)
        Me.GroupBox3.Controls.Add(Me.CmdExportToExcel)
        Me.GroupBox3.Controls.Add(Me.cboCAPayID)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.cboCAStatus)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.CmdRefreshView)
        Me.GroupBox3.Controls.Add(Me.dtCADate)
        Me.GroupBox3.Controls.Add(Me.cboCADate)
        Me.GroupBox3.Controls.Add(Me.cmdRefresh)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.cboCAName)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.cboCACustodian)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.cboCAEvent)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.cboCAISIN)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.cboCAID)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(16, 44)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1279, 84)
        Me.GroupBox3.TabIndex = 43
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Corporate Action Filters"
        '
        'ChkArchive
        '
        Me.ChkArchive.AutoSize = True
        Me.ChkArchive.Location = New System.Drawing.Point(1148, 19)
        Me.ChkArchive.Name = "ChkArchive"
        Me.ChkArchive.Size = New System.Drawing.Size(125, 17)
        Me.ChkArchive.TabIndex = 73
        Me.ChkArchive.Text = "Show Archived items"
        Me.ChkArchive.UseVisualStyleBackColor = True
        '
        'CmdExportToExcel
        '
        Me.CmdExportToExcel.Image = CType(resources.GetObject("CmdExportToExcel.Image"), System.Drawing.Image)
        Me.CmdExportToExcel.Location = New System.Drawing.Point(1148, 40)
        Me.CmdExportToExcel.Name = "CmdExportToExcel"
        Me.CmdExportToExcel.Size = New System.Drawing.Size(39, 37)
        Me.CmdExportToExcel.TabIndex = 72
        Me.CmdExportToExcel.UseVisualStyleBackColor = True
        '
        'cboCAPayID
        '
        Me.cboCAPayID.FormattingEnabled = True
        Me.cboCAPayID.Location = New System.Drawing.Point(62, 51)
        Me.cboCAPayID.Name = "cboCAPayID"
        Me.cboCAPayID.Size = New System.Drawing.Size(91, 21)
        Me.cboCAPayID.TabIndex = 70
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(11, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 71
        Me.Label9.Text = "PAY ID:"
        '
        'cboCAStatus
        '
        Me.cboCAStatus.FormattingEnabled = True
        Me.cboCAStatus.Location = New System.Drawing.Point(210, 51)
        Me.cboCAStatus.Name = "cboCAStatus"
        Me.cboCAStatus.Size = New System.Drawing.Size(300, 21)
        Me.cboCAStatus.TabIndex = 68
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(166, 54)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 69
        Me.Label8.Text = "Status:"
        '
        'CmdRefreshView
        '
        Me.CmdRefreshView.Location = New System.Drawing.Point(1010, 59)
        Me.CmdRefreshView.Name = "CmdRefreshView"
        Me.CmdRefreshView.Size = New System.Drawing.Size(121, 19)
        Me.CmdRefreshView.TabIndex = 65
        Me.CmdRefreshView.Text = "Reset Filter"
        Me.CmdRefreshView.UseVisualStyleBackColor = True
        '
        'dtCADate
        '
        Me.dtCADate.Location = New System.Drawing.Point(710, 52)
        Me.dtCADate.Name = "dtCADate"
        Me.dtCADate.Size = New System.Drawing.Size(200, 20)
        Me.dtCADate.TabIndex = 64
        '
        'cboCADate
        '
        Me.cboCADate.FormattingEnabled = True
        Me.cboCADate.Location = New System.Drawing.Point(586, 51)
        Me.cboCADate.Name = "cboCADate"
        Me.cboCADate.Size = New System.Drawing.Size(118, 21)
        Me.cboCADate.TabIndex = 62
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Location = New System.Drawing.Point(1010, 11)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(121, 41)
        Me.cmdRefresh.TabIndex = 57
        Me.cmdRefresh.Text = "Filter Data Grid"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(542, 54)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 13)
        Me.Label7.TabIndex = 63
        Me.Label7.Text = "Date:"
        '
        'cboCAName
        '
        Me.cboCAName.FormattingEnabled = True
        Me.cboCAName.Location = New System.Drawing.Point(765, 22)
        Me.cboCAName.Name = "cboCAName"
        Me.cboCAName.Size = New System.Drawing.Size(233, 21)
        Me.cboCAName.TabIndex = 60
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(721, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 61
        Me.Label6.Text = "Name:"
        '
        'cboCACustodian
        '
        Me.cboCACustodian.FormattingEnabled = True
        Me.cboCACustodian.Location = New System.Drawing.Point(586, 22)
        Me.cboCACustodian.Name = "cboCACustodian"
        Me.cboCACustodian.Size = New System.Drawing.Size(119, 21)
        Me.cboCACustodian.TabIndex = 58
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(523, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 59
        Me.Label5.Text = "Custodian:"
        '
        'cboCAEvent
        '
        Me.cboCAEvent.FormattingEnabled = True
        Me.cboCAEvent.Location = New System.Drawing.Point(380, 22)
        Me.cboCAEvent.Name = "cboCAEvent"
        Me.cboCAEvent.Size = New System.Drawing.Size(130, 21)
        Me.cboCAEvent.TabIndex = 55
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(336, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 56
        Me.Label4.Text = "Event:"
        '
        'cboCAISIN
        '
        Me.cboCAISIN.FormattingEnabled = True
        Me.cboCAISIN.Location = New System.Drawing.Point(210, 21)
        Me.cboCAISIN.Name = "cboCAISIN"
        Me.cboCAISIN.Size = New System.Drawing.Size(116, 21)
        Me.cboCAISIN.TabIndex = 53
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(173, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "ISIN:"
        '
        'cboCAID
        '
        Me.cboCAID.FormattingEnabled = True
        Me.cboCAID.Location = New System.Drawing.Point(62, 22)
        Me.cboCAID.Name = "cboCAID"
        Me.cboCAID.Size = New System.Drawing.Size(91, 21)
        Me.cboCAID.TabIndex = 51
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(18, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "CA ID:"
        '
        'lblfeeTitle
        '
        Me.lblfeeTitle.AutoSize = True
        Me.lblfeeTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfeeTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.lblfeeTitle.Location = New System.Drawing.Point(16, 9)
        Me.lblfeeTitle.Name = "lblfeeTitle"
        Me.lblfeeTitle.Size = New System.Drawing.Size(177, 24)
        Me.lblfeeTitle.TabIndex = 115
        Me.lblfeeTitle.Text = "Corporate Actions"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Maroon
        Me.Label10.Location = New System.Drawing.Point(987, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(308, 13)
        Me.Label10.TabIndex = 116
        Me.Label10.Text = "Double click on row divider to select corporate actions available"
        '
        'FrmCorpAction
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FloralWhite
        Me.ClientSize = New System.Drawing.Size(1576, 866)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblfeeTitle)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TabCA)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCorpAction"
        Me.Text = "FrmCorpAction"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TabCA.ResumeLayout(False)
        Me.TabVoluntary.ResumeLayout(False)
        CType(Me.dgvCAVoluntary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabMandatory.ResumeLayout(False)
        CType(Me.dgvCAMandatory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPayment.ResumeLayout(False)
        CType(Me.dgvCAPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabBlacklist.ResumeLayout(False)
        CType(Me.dgvCABlacklist, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabCA As TabControl
    Friend WithEvents TabVoluntary As TabPage
    Friend WithEvents dgvCAVoluntary As DataGridView
    Friend WithEvents TabMandatory As TabPage
    Friend WithEvents dgvCAMandatory As DataGridView
    Friend WithEvents TabPayment As TabPage
    Friend WithEvents dgvCAPayment As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents ChkArchive As CheckBox
    Friend WithEvents CmdExportToExcel As Button
    Friend WithEvents cboCAPayID As ComboBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cboCAStatus As ComboBox
    Friend WithEvents Label8 As Label
    Friend WithEvents CmdRefreshView As Button
    Friend WithEvents dtCADate As DateTimePicker
    Friend WithEvents cboCADate As ComboBox
    Friend WithEvents cmdRefresh As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents cboCAName As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents cboCACustodian As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents cboCAEvent As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents cboCAISIN As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents cboCAID As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lblfeeTitle As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents TabBlacklist As TabPage
    Friend WithEvents dgvCABlacklist As DataGridView
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
End Class
