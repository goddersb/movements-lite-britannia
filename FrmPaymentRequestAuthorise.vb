﻿Public Class FrmPaymentrequestAuthorise

    Private Sub FrmPaymentrequestAuthorise_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtPRAuthID.Text = ClsPR.ID
        txtPRAuthPortfolio.Text = ClsPR.PortfolioName
        txtPRAuthCCY.Text = ClsPR.CCYName
        txtPRAuthOrderRef.Text = ClsPR.OrderRef
        txtPRAuthAmount.Text = ClsPR.Amount
    End Sub

    Private Sub cmdContinue_Click(sender As Object, e As EventArgs) Handles cmdContinue.Click
        Dim varResponse As Integer
        Dim varDesc As String = ""

        ClsPR.SelectedAuthoriseOption = 0
        If PRRb1.Checked Then
            If ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingCompliance) And ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) And ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingReview) Then
                ClsPR.SelectedAuthoriseOption = 1
                Dim FrmPRVS As New FrmPaymentRequestViewSwift
                FrmPRVS.ShowDialog()
            Else
                MsgBox("No outgoing payment swift is available for this payment yet", vbInformation)
            End If
        ElseIf PRRb2.Checked Then
            If ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingCompliance) And ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) And ClsPR.Status <> ClsIMS.GetStatusType(cStatusPendingReview) Then
                ClsPR.SelectedAuthoriseOption = 2
                Dim FrmPRVS As New FrmPaymentRequestViewSwift
                FrmPRVS.ShowDialog()
            Else
                MsgBox("No confirmation swift is available for this payment yet", vbInformation)
            End If
        ElseIf PRRb3.Checked Then
            If ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingAuth) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingCompliance) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Or
                ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingReview) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusComplianceRejected) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusPaymentsTeamRejected) Or
                ClsPR.Status = ClsIMS.GetStatusType(cStatusPaymentError) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusAllocatingFunds) Or
                ClsPR.Status = ClsIMS.GetStatusType(cStatusAutoReady) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusReady) Or
                ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingAccountMgt) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingComplianceLevel2) Or
                ClsPR.Status = ClsIMS.GetStatusType(cStatusAboveThreshold) Then
                varResponse = MsgBox("Are you sure you want to cancel this payment request?", vbQuestion + vbYesNo, "Cancel payment request?")
                If varResponse = vbYes Then
                    ClsPR.SelectedAuthoriseOption = 3
                    varDesc = "Cancel payment from FLOW"
                End If
            Else
                MsgBox("You can only cancel payment requests when on pending statuses", vbExclamation, "Cannot cancel payment request")
            End If
        ElseIf PRRb4.Checked Then
            ClsPR = New ClsPaymentRequest
            ClsPR.SelectedAuthoriseOption = 7
            ClsIMS.GetReaderItemIntoPRClass(txtPRAuthID.Text)
            Dim NewPR = New FrmPaymentRequest
            NewPR.ShowDialog()
        ElseIf PRRb5.Checked Then
            ClsPR = New ClsPaymentRequest
            Dim NewPR = New FrmPaymentRequest
            ClsPR.SelectedAuthoriseOption = 6
            ClsIMS.GetReaderItemIntoPRClass(txtPRAuthID.Text)
            NewPR.ShowDialog()
        ElseIf PRRb7.Checked Then
            If (ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingAccountMgt) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgt)) Or
                (ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingAccountMgtLevel2) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseAccountMgtLevel2)) Or
                ((ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingCompliance) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusComplianceRejected)) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseCompliance)) Or
                ((ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingPaymentsTeam) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusPaymentsTeamRejected)) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorisePaymentsTeam)) Or
                ((ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingComplianceLevel2) Or ClsPR.Status = ClsIMS.GetStatusType(cStatusComplianceRejected)) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthoriseComplianceLevel2)) Then
                varResponse = MsgBox("Are you sure you want to authorise this payment?", vbQuestion + vbYesNo, "Authorise Payment")
                If varResponse = vbYes Then
                    ClsPR.SelectedAuthoriseOption = 4
                    varDesc = "Authorise payments team payment from FLOW"
                End If
            ElseIf ClsPR.Status = ClsIMS.GetStatusType(cStatusPendingAuth) And ClsIMS.CanUser(ClsIMS.FullUserName, cUserAuthorise3rdPartyPayments) Then
                varResponse = MsgBox("Are you sure you want to authorise this payment?", vbQuestion + vbYesNo, "Authorise Payment")
                If varResponse = vbYes Then
                    ClsPR.SelectedAuthoriseOption = 12
                    varDesc = "Authorise finance payment from FLOW"
                End If
            Else
                MsgBox("You do not have permissions to authorise this payment request or the status is not ready to be authorised. Please request finance, accounts management or payments team to authorise this request", vbExclamation, "Cannot authorise payment")
            End If
        ElseIf PRRb6.Checked Then
            ClsPR = New ClsPaymentRequest
            Dim NewPR = New FrmPaymentRequest
            ClsPR.SelectedAuthoriseOption = 7
            ClsIMS.GetReaderItemIntoPRClass(txtPRAuthID.Text)
            ClsGV.GridOption = 0
            ClsGV.ID = ClsPR.ID
            Dim FrmAuditView As New FrmGridView
            FrmAuditView.ShowDialog()
        ElseIf PRRb8.Checked Then
            ClsIMS.GetReaderItemIntoAuthPaymentClass(txtPRAuthID.Text)
            FrmEmail.ShowDialog()
        End If

        If ClsPR.SelectedAuthoriseOption = 3 Or ClsPR.SelectedAuthoriseOption = 4 Or ClsPR.SelectedAuthoriseOption = 12 Then
            ClsIMS.ActionMovementPR(ClsPR.SelectedAuthoriseOption, ClsPR.ID, ClsIMS.FullUserName, varDesc)
        End If

        If varResponse = vbYes Then
            Me.Close()
        End If
    End Sub

End Class